{*******************************************************}
{                                                       }
{       Logger                                          }
{                                                       }
{       Реализация логгера, записей, списков.           }
{                                                       }
{       Перефикс: SANLog                                }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SANLoggerImpl;

interface

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  SysUtils, Classes,
  {$ENDREGION}

  {$REGION 'SANCore'}
  SANCoreIntfs, SANCoreImpl,
  {$ENDREGION}

  {$REGION 'SANLogger'}
  SANLoggerIntfs, SANLoggerContexts;
  {$ENDREGION}
{$ENDREGION}

{$REGION 'resourcestring'}
resourcestring
  sSANLoggerImplLogNotFound =
    'Log "%s" not found';
{$ENDREGION}

type
  /// <summary>
  ///   Базовое исключение для лога.
  /// </summary>
  ESANLogImplError = class(Exception);

  /// <summary>
  /// Одна запись лога
  /// </summary>
  TSANLogRecord = class(TSANCoreInterfacedRefPersistent,
    ISANCorePersistent,  ISANCorePersistentSafe,
    ISANLogRecord)
  private
    FID        : TSANLogRecordID;
    FDateTime  : TDateTime;
    FKind      : TSANLogRecordKind;
    FSubType   : TSANLogRecordSubType;
    FText      : TSANLogRecordText;

  protected
    {$REGION 'ISANLogRecord'}
    function GetID : TSANLogRecordID; safecall;
    procedure SetID(const AID : TSANLogRecordID); safecall;
    function GetDateTime : TDateTime; safecall;
    procedure SetDateTime(const ADateTime : TDateTime); safecall;
    function GetKind : TSANLogRecordKind; safecall;
    procedure SetKind(const AKind : TSANLogRecordKind); safecall;
    function GetSubType : TSANLogRecordSubType; safecall;
    procedure SetSubType(const ASubType : TSANLogRecordSubType); safecall;
    function GetText : TSANLogRecordText; safecall;
    procedure SetText(const AText : TSANLogRecordText); safecall;
    procedure Clear; safecall;
    function From(const AID : TSANLogRecordID; const ADateTime : TDateTime;
      const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      const AText : TSANLogRecordText): ISANLogRecord; safecall;
    function IsEquals(const ALogRecord : ISANLogRecord) : WordBool; safecall;
    procedure Assign(const ASource : ISANLogRecord); safecall;
    function Clone : ISANLogRecord; safecall;
    {$ENDREGION}

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); override;
    procedure DoAssign(const ASource : IInterface); override;
    procedure DoClone(out AClone); override;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property ID        : TSANLogRecordID       read GetID        write SetID;
    property DateTime  : TDateTime             read GetDateTime  write SetDateTime;
    property Kind      : TSANLogRecordKind     read GetKind      write SetKind;
    property SubType   : TSANLogRecordSubType  read GetSubType   write SetSubType;
    property Text      : TSANLogRecordText     read GetText      write SetText;
  end;

  /// <summary>
  ///   Набор записей в логе.
  /// </summary>
  TSANLogRecords = class(TSANCoreInterfacedRefPersistent,
    ISANCorePersistent, ISANCorePersistentSafe, ISANLogRecords)
  private
    FRecords, FNotifiers : IInterfaceListEx;

  protected
    {$REGION 'ISANLogRecords'}
    function GetCount : integer; safecall;
    function GetRecords(const AIndex : integer) : ISANLogRecord; safecall;
    procedure Lock; safecall;
    procedure UnLock; safecall;
    procedure Clear; safecall;
    function TryFindByID(const AID : TSANLogRecordID; out ALogRecord : ISANLogRecord;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;
    function ExtractByID(const AID : TSANLogRecordID) : ISANLogRecord; safecall;
    function RemoveByID(const AID : TSANLogRecordID) : WordBool; safecall;
    procedure Delete(const AIndex : integer); safecall;
    function Add(const ALogRecord : ISANLogRecord) : ISANLogRecord; safecall;
    function AddClone(const ALogRecord : ISANLogRecord) : ISANLogRecord; safecall;
    function AddFrom(const AID : TSANLogRecordID; const ADateTime : TDateTime;
      const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      const AText : TSANLogRecordText) : ISANLogRecord; safecall;
    function Filter(AFilter : ISANLogRecordsFilter) : integer; safecall;
    procedure ExportRecords(const ARecords : ISANLogRecords;
      const AReWrite, AUseClone : WordBool;
      AFilter : ISANLogRecordsFilter = nil); safecall;
    procedure ImportRecords(const ARecords : ISANLogRecords;
      const AReWrite, AUseClone : WordBool;
      AFilter : ISANLogRecordsFilter = nil); safecall;
    function AddNotifier(const ANotifier : ISANLogRecordsNotifier) : TSANCoreNotifierHandle; safecall;
    function RemoveNotifier(const ANotifierHandle : TSANCoreNotifierHandle) : WordBool; safecall;
    function IsEquals(const AOther : ISANLogRecords) : WordBool; safecall;
    procedure Assign(const ASource : ISANLogRecords); safecall;
    function Clone : ISANLogRecords; safecall;
    {$ENDREGION}

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); override;
    procedure DoAssign(const ASource : IInterface); override;
    procedure DoClone(out AClone); override;

    procedure NotifyAllDestroying;
    procedure NotifayAllFiltered(AFiltered : ISANLogRecords);
    procedure NotifayAllCleared;
    procedure NotifayAllExtracted(const ARec : ISANLogRecord);
    procedure NotifayAllRemoving(const ARec : ISANLogRecord);
    procedure NotifayAllAdded(const ARec : ISANLogRecord);

  public
    constructor Create;
    procedure BeforeDestruction; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property Count : integer  read GetCount;
    property Records[const AIndex : integer] : ISANLogRecord  read GetRecords; default;
  end;

  /// <summary>
  ///   Cписок хранилищ.
  /// </summary>
  TSANLogsStoragesList = class(TSANCoreInterfacedRefPersistent,
    ISANCorePersistent, ISANCorePersistentSafe, ISANLogsStoragesList)
  private
    FStorages, FNotifiers : IInterfaceListEx;

    procedure SetStorages(const Index: integer; const Value: ISANLogsStorage);

  protected
    {$REGION 'ISANLogsStoragesList'}
    function GetCount : integer; safecall;
    function GetStorages(const Index : integer) : ISANLogsStorage; safecall;
    procedure Lock; safecall;
    procedure UnLock; safecall;
    procedure Clear; safecall;
    function TryFindByName(const AName : TSANLogName; out AStorage : ISANLogsStorage;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;
    function ExtractByName(const AName : TSANLogName) : ISANLogsStorage; safecall;
    function RemoveByName(const AName : TSANLogName) : WordBool; safecall;
    function Add(const AStorage : ISANLogsStorage) : ISANLogsStorage; safecall;
    function AddNotifier(const ANotifier : ISANLogsStoragesListNotifier) : TSANCoreNotifierHandle; safecall;
    function RemoveNotifier(const ANotifierHandle : TSANCoreNotifierHandle) : WordBool; safecall;
    {$ENDREGION}

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); override;
    procedure DoAssign(const ASource : IInterface); override;
    procedure DoClone(out AClone); override;

    procedure NotifyAllDestroying;
    procedure NotifyAllAdded(const AStorage : ISANLogsStorage);
    procedure NotifyAllExtracted(const AStorage : ISANLogsStorage);
    procedure NotifyAllRemoving(const AStorage : ISANLogsStorage);
    procedure NotifyAllCleared;

  public
    constructor Create;
    procedure BeforeDestruction; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property Count : integer  read GetCount;
    property Storages[const Index : integer] : ISANLogsStorage  read GetStorages  write SetStorages; default;
  end;

  /// <summary>
  ///   Синглтон управления логированием.
  /// </summary>
  TSANLogger = class(TInterfacedObject,
    ITermination, ITerminationSafe,
    ISANLogsSet, ISANLogger, ISANLoggerMaker
  )
  private
    FStorages: ISANLogsStoragesList;
    FOuterTermination : ITerminationSafe;

  protected
    {$REGION 'ITermination'}
    function GetOuterTerminated : WordBool;
    {$ENDREGION}

    {$REGION 'ITerminationSafe'}
    function ITerminationSafe.GetOuterTerminated = GetOuterTerminatedSafe;
    function GetOuterTerminatedSafe : WordBool; safecall;
    {$ENDREGION}

    {$REGION 'ISANLogsSet'}
    function GetLogsCount : integer; safecall;
    function GetLogs(const Index : integer) : ISANLog; safecall;
    function GetLogByName(const AName : TSANLogName) : ISANLog; safecall;
    function TryFindByName(const AName : TSANLogName; out ALog : ISANLog;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;
    {$ENDREGION}

    {$REGION 'ISANLogger'}
    function GetStorages : ISANLogsStoragesList; safecall;
    function GetOuterTermination : ITerminationSafe; safecall;
    procedure SetOuterTermination(const AOuterTermination : ITerminationSafe); safecall;
    {$ENDREGION}

    {$REGION 'ISANLoggerMaker'}
    function MakeLogRecord : ISANLogRecord; safecall;
    function MakeLogsRecords : ISANLogRecords; safecall;
    {$ENDREGION}

  public
    constructor Create;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property LogsCount : integer  read GetLogsCount;
    property Logs[const Index : integer] : ISANLog  read GetLogs;
    property LogByName[const AName : TSANLogName] : ISANLog  read GetLogByName;

    property Storages : ISANLogsStoragesList  read GetStorages;

    property OuterTerminated : WordBool  read GetOuterTerminated;

  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  ActiveX, ComObj;
  {$ENDREGION}
{$ENDREGION}

//==============================================================================
{$REGION 'TSANLogRecord'}
procedure TSANLogRecord.Assign(const ASource: ISANLogRecord);
begin
  DoAssign(ASource);
end;

procedure TSANLogRecord.Clear;
begin
  FID        := 0;
  FDateTime  := 0;
  FKind      := 0;
  FSubType   := EmptyWideStr;
  FText      := EmptyWideStr;
end;

function TSANLogRecord.Clone: ISANLogRecord;
begin
  DoClone(Result);
end;

procedure TSANLogRecord.DoAssign(const ASource: IInterface);
var
  Source : ISANLogRecord;
begin

  if not Supports(ASource, ISANLogRecord, Source) then
    inherited;

  FID        := Source.ID;
  FDateTime  := Source.DateTime;
  FKind      := Source.Kind;
  FSubType   := Source.SubType;
  FText      := Source.Text;
end;

procedure TSANLogRecord.DoClone(out AClone);
var
  LogRecord : TSANLogRecord;
begin
  LogRecord := TSANLogRecord.Create;
  LogRecord.Assign(Self);

  ISANLogRecord(AClone) := LogRecord;
end;

procedure TSANLogRecord.DoIsEquals(const AOther: IInterface;
  out AIsEquals: WordBool);
var
  Other : ISANLogRecord;
begin
  AIsEquals := Supports(AOther, ISANLogRecord, Other)
           and (FID        = Other.ID)
           and (FDateTime  = Other.DateTime)
           and (FKind      = Other.Kind)
           and WideSameStr(FSubType,  Other.SubType)
           and WideSameStr(FText,     Other.Text)
end;

function TSANLogRecord.From(const AID: TSANLogRecordID;
  const ADateTime: TDateTime; const AKind: TSANLogRecordKind;
  const ASubType: TSANLogRecordSubType; const AText: TSANLogRecordText) : ISANLogRecord;
begin
  FID        := AID;
  FDateTime  := ADateTime;
  FKind      := AKind;
  FSubType   := ASubType;
  FText      := AText;

  Result := Self;
end;

function TSANLogRecord.GetDateTime: TDateTime;
begin
  Result := FDateTime;
end;

function TSANLogRecord.GetID: TSANLogRecordID;
begin
  Result := FID;
end;

function TSANLogRecord.GetKind: TSANLogRecordKind;
begin
  Result := FKind;
end;

function TSANLogRecord.GetSubType: TSANLogRecordSubType;
begin
  Result := FSubType;
end;

function TSANLogRecord.GetText: TSANLogRecordText;
begin
  Result := FText;
end;

function TSANLogRecord.IsEquals(const ALogRecord: ISANLogRecord): WordBool;
begin
  DoIsEquals(ALogRecord, Result);
end;

function TSANLogRecord.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogRecord,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

procedure TSANLogRecord.SetDateTime(const ADateTime: TDateTime);
begin
  FDateTime := ADateTime;
end;

procedure TSANLogRecord.SetID(const AID: TSANLogRecordID);
begin
  FID := AID;
end;

procedure TSANLogRecord.SetKind(const AKind: TSANLogRecordKind);
begin
  FKind := AKind;
end;

procedure TSANLogRecord.SetSubType(const ASubType: TSANLogRecordSubType);
begin
  FSubType := ASubType;
end;

procedure TSANLogRecord.SetText(const AText: TSANLogRecordText);
begin
  FText := AText;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogRecords'}
function TSANLogRecords.AddFrom(const AID: TSANLogRecordID;
  const ADateTime: TDateTime; const AKind: TSANLogRecordKind;
  const ASubType: TSANLogRecordSubType;
  const AText: TSANLogRecordText): ISANLogRecord;
begin
  Result := TSANLogRecord.Create;
  Result.From(AID, ADateTime, AKind, ASubType, AText);
  FRecords.Add(Result);
  NotifayAllAdded(Result);
end;

function TSANLogRecords.Add(const ALogRecord: ISANLogRecord): ISANLogRecord;
begin
  Result := ALogRecord;
  FRecords.Add(Result);
  NotifayAllAdded(Result);
end;

function TSANLogRecords.AddClone(
  const ALogRecord: ISANLogRecord): ISANLogRecord;
begin
  Result := TSANLogRecord.Create;
  Result.Assign(ALogRecord);
  FRecords.Add(Result);
  NotifayAllAdded(Result);
end;

function TSANLogRecords.AddNotifier(
  const ANotifier: ISANLogRecordsNotifier): TSANCoreNotifierHandle;
begin
  Result := TSANCoreNotifierHandle(ANotifier);
  FNotifiers.Add(ANotifier);
end;

procedure TSANLogRecords.Assign(const ASource: ISANLogRecords);
begin
  DoAssign(ASource);
end;

procedure TSANLogRecords.BeforeDestruction;
begin
  NotifyAllDestroying;

  inherited;
end;

procedure TSANLogRecords.Clear;
begin
  FRecords.Clear;
  NotifayAllCleared;
end;

function TSANLogRecords.Clone: ISANLogRecords;
begin
  DoClone(Result);
end;

constructor TSANLogRecords.Create;
begin
  inherited Create;

  FRecords    := TInterfaceList.Create;
  FNotifiers  := TInterfaceList.Create;
end;

procedure TSANLogRecords.Delete(const AIndex: integer);
begin
  FRecords.Delete(AIndex);
end;

procedure TSANLogRecords.DoAssign(const ASource: IInterface);
var
  Source : ISANLogRecords;
  i : integer;
begin

  if not Supports(ASource, ISANLogRecords, Source) then
    inherited;

  FRecords.Lock;
  try

    Source.Lock;
    try

      for i := 0 to Source.Count - 1 do
        if i >= FRecords.Count then
          AddClone(Source[i])
        else
          Records[i].Assign(Source[i]);

      for i := FRecords.Count - 1 downto Source.Count - 1 do
        FRecords.Delete(i);

    finally
      Source.UnLock;
    end;

  finally
    FRecords.Unlock;
  end;
end;

procedure TSANLogRecords.DoClone(out AClone);
var
  vClone : ISANLogRecords;
begin
  vClone := TSANLogRecords.Create;
  vClone.Assign(Self);
  ISANLogRecords(AClone) := vClone;
end;

procedure TSANLogRecords.DoIsEquals(const AOther: IInterface;
  out AIsEquals: WordBool);
var
  Other : ISANLogRecords;
  i : integer;
begin
  AIsEquals := False;

  if not Supports(AOther, ISANLogRecords, Other) then
    Exit;

  FRecords.Lock;
  try

    Other.Lock;
    try

      if Other.Count <> Count then
        Exit;

      for i := 0 to Count - 1 do
        if not Records[i].IsEquals(Other[i]) then
          Exit;

    finally
      Other.UnLock;
    end;

  finally
    FRecords.Unlock;
  end;

  AIsEquals := True;
end;

procedure TSANLogRecords.ExportRecords(const ARecords: ISANLogRecords;
  const AReWrite, AUseClone : WordBool;
  AFilter: ISANLogRecordsFilter);
var
  i : integer;
begin

  ARecords.Lock;
  try

    if AReWrite then
      ARecords.Clear;

    FRecords.Lock;
    try

      for i := 0 to Count - 1 do
        if not (Assigned(AFilter) and AFilter.Filtered(Records[i])) then
          if AUseClone then
            ARecords.AddClone(Records[i])
          else
            ARecords.Add(Records[i])

    finally
      FRecords.Unlock;
    end;

  finally
    ARecords.UnLock;
  end;

end;

procedure TSANLogRecords.ImportRecords(const ARecords: ISANLogRecords;
  const AReWrite, AUseClone : WordBool; AFilter: ISANLogRecordsFilter);
var
  i : integer;
begin
  FRecords.Lock;
  try

    if AReWrite then
    begin
      FRecords.Clear;
      NotifayAllCleared;
    end;

    ARecords.Lock;
    try

      for i := 0 to ARecords.Count - 1 do
        if not (Assigned(AFilter) and AFilter.Filtered(ARecords[i])) then
          if AUseClone then
            AddClone(ARecords[i])
          else
            ARecords.Add(Records[i]);

    finally
      ARecords.UnLock;
    end;

  finally
    FRecords.Unlock;
  end;
end;

function TSANLogRecords.ExtractByID(const AID: TSANLogRecordID): ISANLogRecord;
var
  i : integer;
begin
  FRecords.Lock;
  try

    if TryFindByID(AID, Result, @i) then
    begin
      FRecords.Delete(i);

      NotifayAllExtracted(Result);
    end;

  finally
    FRecords.Unlock;
  end;
end;

function TSANLogRecords.RemoveByID(const AID: TSANLogRecordID): WordBool;
var
  Rec : ISANLogRecord;
  i : integer;
begin
  FRecords.Lock;
  try

    Result := TryFindByID(AID, Rec, @i);

    if Result then
    begin
      NotifayAllRemoving(Rec);

      FRecords.Delete(i);
    end;

  finally
    FRecords.Unlock;
  end;
end;

function TSANLogRecords.Filter(AFilter: ISANLogRecordsFilter): integer;
var
  i : integer;
  Filtered : TSANLogRecords;
begin
  Filtered := TSANLogRecords.Create;

  FRecords.Lock;
  try

    for i := Count - 1 downto 0 do
      if AFilter.Filtered(Records[i]) then
      begin
        Filtered.FRecords.Insert(0, Records[i]);
        FRecords.Delete(i);
      end;

  finally
    FRecords.Unlock;
  end;

  Result := Filtered.Count;

  NotifayAllFiltered(Filtered);
end;

function TSANLogRecords.GetCount: integer;
begin
  Result := FRecords.Count;
end;

function TSANLogRecords.GetRecords(const AIndex: integer): ISANLogRecord;
begin
  Result := ISANLogRecord(FRecords[AIndex]);
end;

function TSANLogRecords.IsEquals(const AOther: ISANLogRecords): WordBool;
begin
  DoIsEquals(AOther, Result);
end;

procedure TSANLogRecords.Lock;
begin
  FRecords.Lock;
end;

procedure TSANLogRecords.NotifayAllAdded(const ARec: ISANLogRecord);
var
  Cx : ISANLogRecordsOneRecordContext;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogRecordsOneRecordContext.Create(Self, ARec);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogRecordsNotifier(FNotifiers[i]).Added(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogRecords.NotifayAllCleared;
var
  Cx : ISANCoreNotificationContextSafe;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANCoreSimpleNotificationContext.Create(Self);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogRecordsNotifier(FNotifiers[i]).Cleared(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogRecords.NotifayAllExtracted(const ARec: ISANLogRecord);
var
  Cx : ISANLogRecordsOneRecordContext;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogRecordsOneRecordContext.Create(Self, ARec);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogRecordsNotifier(FNotifiers[i]).Extracted(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogRecords.NotifayAllFiltered(AFiltered: ISANLogRecords);
var
  Cx : ISANLogRecordsFilteredContext;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogRecordsFilteredContext.Create(Self, AFiltered);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogRecordsNotifier(FNotifiers[i]).Filtered(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogRecords.NotifayAllRemoving(const ARec: ISANLogRecord);
var
  Cx : ISANLogRecordsOneRecordContext;
  i : integer;
begin
  FNotifiers.Lock;
  try
    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogRecordsOneRecordContext.Create(Self, ARec);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogRecordsNotifier(FNotifiers[i]).Removing(Cx);
  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogRecords.NotifyAllDestroying;
var
  Cx : ISANCoreNotificationContextSafe;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANCoreSimpleNotificationContext.Create(Self);

    for i := 0 to FNotifiers.Count - 1 do
      ISANCoreNotifierSafe(FNotifiers[i]).Destroying(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

function TSANLogRecords.RemoveNotifier(
  const ANotifierHandle: TSANCoreNotifierHandle): WordBool;
begin
  if FNotifiers.Count = 0 then
    Exit;

  Result := FNotifiers.Remove(IInterface(ANotifierHandle)) > -1;
end;

function TSANLogRecords.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogRecords,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

function TSANLogRecords.TryFindByID(const AID: TSANLogRecordID;
  out ALogRecord: ISANLogRecord; const AIndexPtr: PInteger): WordBool;
var
  i : integer;
begin
  FRecords.Lock;
  try

    for i := 0 to Count - 1 do
      if Records[i].ID = AID then
      begin
        ALogRecord := Records[i];

        if Assigned(AIndexPtr) then
          AIndexPtr^ := i;

        Exit(True);
      end;

  finally
    FRecords.Unlock
  end;

  ALogRecord := nil;
  Result := False;
end;

procedure TSANLogRecords.UnLock;
begin
  FRecords.UnLock;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogsStoragesList'}
function TSANLogsStoragesList.Add(const AStorage: ISANLogsStorage): ISANLogsStorage;
begin
  FStorages.Add(AStorage);
  NotifyAllAdded(AStorage);
end;

function TSANLogsStoragesList.AddNotifier(
  const ANotifier: ISANLogsStoragesListNotifier): TSANCoreNotifierHandle;
begin
  Result := TSANCoreNotifierHandle(ANotifier);
  FNotifiers.Add(ANotifier);
end;

procedure TSANLogsStoragesList.BeforeDestruction;
begin
  NotifyAllDestroying;

  inherited;
end;

function TSANLogsStoragesList.RemoveNotifier(
  const ANotifierHandle: TSANCoreNotifierHandle): WordBool;
begin
  Result := FNotifiers.Remove(IInterface(ANotifierHandle)) > -1;
end;

procedure TSANLogsStoragesList.Clear;
begin
  FStorages.Clear;
  NotifyAllCleared;
end;

constructor TSANLogsStoragesList.Create;
begin
  inherited Create;

  FStorages   := TInterfaceList.Create;
  FNotifiers  := TInterfaceList.Create;
end;

procedure TSANLogsStoragesList.DoAssign(const ASource: IInterface);
var
  Source : ISANLogsStoragesList;
  i : integer;
begin

  if not Supports(ASource, ISANLogsStoragesList, Source) then
    inherited;

  FStorages.Lock;
  try

    Source.Lock;
    try

      for i := 0 to Source.Count - 1 do
        if i >= FStorages.Count then
          Add(Source[i])
        else
          Storages[i] := Source[i];

      for i := FStorages.Count - 1 downto Source.Count - 1 do
        FStorages.Delete(i);

    finally
      Source.UnLock;
    end;

  finally
    FStorages.Unlock;
  end;
end;

procedure TSANLogsStoragesList.DoClone(out AClone);
var
  vClone : ISANLogsStoragesList;
begin
  vClone := TSANLogsStoragesList.Create;
  (vClone as ISANCorePersistent).Assign(Self);
  ISANLogsStoragesList(AClone) := vClone;
end;

procedure TSANLogsStoragesList.DoIsEquals(const AOther: IInterface;
  out AIsEquals: WordBool);
var
  Other : ISANLogsStoragesList;
  i : integer;
begin
  AIsEquals := False;

  if not Supports(AOther, ISANLogsStoragesList, Other) then
    Exit;

  FStorages.Lock;
  try

    Other.Lock;
    try

      if Other.Count <> Count then
        Exit;

      for i := 0 to Count - 1 do
        if not Storages[i].IsEquals(Other[i]) then
          Exit;

    finally
      Other.UnLock;
    end;

  finally
    FStorages.Unlock;
  end;

  AIsEquals := True;
end;

function TSANLogsStoragesList.ExtractByName(
  const AName: TSANLogName): ISANLogsStorage;
var
  i : integer;
begin
  FStorages.Lock;
  try
    if TryFindByName(AName, Result, @i) then
    begin
      FStorages.Delete(i);

      NotifyAllExtracted(Result);
    end;
  finally
    FStorages.Unlock;
  end;
end;

function TSANLogsStoragesList.RemoveByName(const AName: TSANLogName): WordBool;
var
  Storage : ISANLogsStorage;
  i : integer;
begin
  FStorages.Lock;
  try
    Result := TryFindByName(AName, Storage, @i);

    if Result then
    begin
      NotifyAllRemoving(Storage);

      FStorages.Delete(i);
    end;

  finally
    FStorages.Unlock;
  end;
end;

function TSANLogsStoragesList.GetCount: integer;
begin
  Result := FStorages.Count;
end;

function TSANLogsStoragesList.GetStorages(const Index: integer): ISANLogsStorage;
begin
  Result := ISANLogsStorage(FStorages[Index]);
end;

procedure TSANLogsStoragesList.Lock;
begin
  FStorages.Lock;
end;

procedure TSANLogsStoragesList.NotifyAllDestroying;
var
  Cx : ISANCoreNotificationContextSafe;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANCoreSimpleNotificationContext.Create(Self);

    for i := 0 to FNotifiers.Count - 1 do
      ISANCoreNotifierSafe(FNotifiers[i]).Destroying(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogsStoragesList.NotifyAllAdded(const AStorage: ISANLogsStorage);
var
  Cx : ISANLogsStoragesOneStorageContext;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogsStoragesOneStorageContext.Create(Self, AStorage);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogsStoragesListNotifier(FNotifiers[i]).Added(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogsStoragesList.NotifyAllCleared;
var
  Cx : ISANCoreNotificationContextSafe;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANCoreSimpleNotificationContext.Create(Self);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogsStoragesListNotifier(FNotifiers[i]).Cleared(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogsStoragesList.NotifyAllExtracted(
  const AStorage: ISANLogsStorage);
var
  Cx : ISANLogsStoragesOneStorageContext;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogsStoragesOneStorageContext.Create(Self, AStorage);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogsStoragesListNotifier(FNotifiers[i]).Extracted(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogsStoragesList.NotifyAllRemoving(
  const AStorage: ISANLogsStorage);
var
  Cx : ISANLogsStoragesOneStorageContext;
  i : integer;
begin
  FNotifiers.Lock;
  try
    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogsStoragesOneStorageContext.Create(Self, AStorage);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogsStoragesListNotifier(FNotifiers[i]).Removing(Cx);
  finally
    FNotifiers.Unlock;
  end;
end;

function TSANLogsStoragesList.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogsStoragesList,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

procedure TSANLogsStoragesList.SetStorages(const Index: integer;
  const Value: ISANLogsStorage);
begin
  FStorages[Index] := Value;
end;

function TSANLogsStoragesList.TryFindByName(const AName: TSANLogName;
  out AStorage: ISANLogsStorage; const AIndexPtr: PInteger): WordBool;
var
  i : integer;
begin

  FStorages.Lock;
  try

    for i := 0 to Count - 1 do
      if WideSameText(Storages[i].Name, AName) then
      begin
        AStorage := Storages[i];

        if Assigned(AIndexPtr) then
          AIndexPtr^ := i;

        Exit(True);
      end;

  finally
    FStorages.Unlock;
  end;

  AStorage := nil;
  Result := False;
end;

procedure TSANLogsStoragesList.UnLock;
begin
  FStorages.Unlock;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogger'}
constructor TSANLogger.Create;
begin
  inherited Create;

  FStorages := TSANLogsStoragesList.Create;
end;

function TSANLogger.GetLogByName(const AName: TSANLogName): ISANLog;
begin
  if not TryFindByName(AName, Result) then
    raise ESANLogImplError.CreateResFmt(@sSANLoggerImplLogNotFound, [AName]);
end;

function TSANLogger.GetLogs(const Index: integer): ISANLog;
var
  i, vIdx : integer;
begin
  vIdx := Index;

  for i := 0 to Storages.Count - 1 do
    if vIdx < Storages[i].LogsCount then
    begin
      Result := Storages[i].Logs[vIdx];
      Exit;
    end

    else
      Dec(vIdx, Storages[i].LogsCount);
end;

function TSANLogger.GetLogsCount: integer;
var
  i : integer;
begin
  Result := 0;

  for i := 0 to Storages.Count - 1 do
    Inc(Result, Storages[i].LogsCount);
end;

function TSANLogger.GetOuterTerminated: WordBool;
begin
  Result := Assigned(FOuterTermination) and FOuterTermination.OuterTerminated;
end;

function TSANLogger.GetOuterTerminatedSafe: WordBool;
begin
  Result := GetOuterTerminated;
end;

function TSANLogger.GetOuterTermination: ITerminationSafe;
begin
  Result := FOuterTermination;
end;

function TSANLogger.GetStorages: ISANLogsStoragesList;
begin
  Result := FStorages;
end;

function TSANLogger.MakeLogRecord: ISANLogRecord;
begin
  Result := TSANLogRecord.Create;
end;

function TSANLogger.MakeLogsRecords: ISANLogRecords;
begin
  Result := TSANLogRecords.Create;
end;

function TSANLogger.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogger,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

procedure TSANLogger.SetOuterTermination(const AOuterTermination: ITerminationSafe);
begin
  FOuterTermination := AOuterTermination;
end;

function TSANLogger.TryFindByName(const AName: TSANLogName; out ALog: ISANLog;
  const AIndexPtr: PInteger): WordBool;
var
  i : integer;
begin

  for i := 0 to Storages.Count - 1 do
    if Storages[i].TryFindByName(AName, ALog, AIndexPtr) then
      Exit(True);

  ALog := nil;
  Result := False;
end;
{$ENDREGION}
//==============================================================================

end.
