{*******************************************************}
{                                                       }
{       Logger                                          }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SANLogger;

interface

uses
  SysUtils, Classes, WideStrings;

const
  /// <summary>
  ///   Признак новой записи.
  /// </summary>
  LOG_REC_MARKER = '#@$';

{$REGION 'LogRecordKind constants'}
const
  /// <summary>
  ///   Разновидность записи в логе: информация.
  /// </summary>
  LRK_INFO     = 0;

  /// <summary>
  ///   Разновидность записи в логе: информация.
  /// </summary>
  LRK_WARNING  = 1;

  /// <summary>
  ///   Разновидность записи в логе: информация.
  /// </summary>
  LRK_ERROR    = 2;

  /// <summary>
  ///   Разновидность записи в логе: информация.
  /// </summary>
  LRK_FATAL    = 3;
{$ENDREGION}

{$REGION 'resourcestring'}
resourcestring
  sSANLoggerRecordClosedErr =
    'Запись закрыта. Действия над записью запрещены';

  sSANLoggerTargetReccordMissingErr =
    'Не задан экземпляр записи для передачи данных';

  sSANLoggerLogNotFoundFmtErr =
    'Лог "%s" не найден';

  sSANLoggerLogAlreadyExistsFmtErr =
    'Лог "%s" уже существует';
{$ENDREGION}

type
  /// <summary>
  ///   Базовое исключение для лога.
  /// </summary>
  ESANLogError = class(Exception);

  {$REGION 'BaseTypes'}
  /// <summary>
  ///   Тип данных типа лога.
  /// </summary>
  TSANLogRecordKind     = Longword;

  /// <summary>
  ///   Тип данных текстового подтипа лога.
  /// </summary>
  TSANLogRecordSubType  = WideString;

  /// <summary>
  ///   Тип данных текста лога.
  /// </summary>
  TSANLogRecordText     = WideString;

  /// <summary>
  ///   Тип данных имен.
  /// </summary>
  TSANLogName           = WideString;

  /// <summary>
  ///   Тип данных расположения.
  /// </summary>
  TSANLocation          = WideString;
  {$ENDREGION}

  {$REGION 'Record'}
  /// <summary>
  ///   Интерфейс вывода лога.
  /// </summary>
  ISANLogOutput = interface(IInterface)
  ['{03E130F6-AA97-4E08-8F0A-4AB03169495F}']
    {$REGION 'Gets&Sets'}
    function GetDateTimeFormat : TSANLogRecordText;
    {$ENDREGION}
    /// <summary>
    ///   Заголовок записи.
    /// </summary>
    /// <param name="AKind">
    ///   Целочисленный тип записи.
    /// </param>
    /// <param name="ASubType">
    ///   Строка подтипа записи.
    /// </param>
    /// <param name="ADateTime">
    ///   Дата и время записи. Если ADateTime = 0, берется Now.
    /// </param>
    procedure Header(const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      ADateTime : TDateTime = 0);

    /// <summary>
    ///   Добавить строку в буфер.
    /// </summary>
    /// <param name="AText">
    ///   Добавляемый текст.
    /// </param>
    procedure Write(const AText : TSANLogRecordText);

    /// <summary>
    ///   Добавить строку в буфер с добавлением #13#10.
    /// </summary>
    /// <param name="AText">
    ///   Добавляемый текст.
    /// </param>
    procedure Writeln(const AText : TSANLogRecordText);

    /// <summary>
    ///   Добавить отчеркивание.
    /// </summary>
    /// <param name="ALineItem">
    ///   Элементарная строка, из которых строится линия.
    /// </param>
    /// <param name="ALineItemsCount">
    ///   Количество элементарных строк в линии.
    /// </param>
    /// <param name="AUseLn">
    ///   Производится ли перевод строки после линии.
    /// </param>
    procedure Line(const ALineItem : TSANLogRecordText; const ALineItemsCount : Word = 255;
      const AUseLn : boolean = True);

    /// <summary>
    ///   Формат даты и времени.
    /// </summary>
    property DateTimeFormat : TSANLogRecordText  read GetDateTimeFormat;
  end;

  /// <summary>
  ///   Интерфейс одной записи лога.
  /// </summary>
  ISANLogRecord = interface(ISANLogOutput)
  ['{AA68A6F1-BD10-4A89-A107-719A4F992A7F}']
    {$REGION 'Gets&Sets'}
    function GetBuffer : TSANLogRecordText;
    procedure SetBuffer(const ABuffer : TSANLogRecordText);
    function GetAutoFlush : boolean;
    procedure SetAutoFlush(const AAutoFlush : boolean);
    function GetClosed : boolean;
    {$ENDREGION}

    /// <summary>
    ///   Вписать все накопленные данные в файл.
    /// </summary>
    procedure Flush;

    /// <summary>
    ///   Закончить работу с записью.
    /// </summary>
    procedure Close;

    /// <summary>
    ///   Накопленные данные.
    /// </summary>
    property Buffer : TSANLogRecordText  read GetBuffer  write SetBuffer;

    /// <summary>
    ///   Выполнять команду Flush автоматически при каждом изменении Buffer.
    /// </summary>
    property AutoFlush : boolean  read GetAutoFlush  write SetAutoFlush;

    /// <summary>
    ///   Признак закрытой записи.
    /// </summary>
    property Closed : boolean  read GetClosed;
  end;

  /// <summary>
  ///   Интерфейс с перенаправляемым вводом.
  /// </summary>
  ISANLogRecordTargetProvider = interface(IInterface)
  ['{906DD15D-7CE9-4256-A4DB-3419851E495A}']
    {$REGION 'Gets&Sets'}
    function GetTargetRec : ISANLogRecord;
    procedure SetTargetRec(const ATargetRec : ISANLogRecord);
    {$ENDREGION}

    /// <summary>
    ///   Запись, в которую перенаправляется ввод.
    /// </summary>
    property TargetRec : ISANLogRecord  read GetTargetRec  write SetTargetRec;
  end;
  {$ENDREGION}

  {$REGION 'Log'}
  /// <summary>
  ///   Полноценный лог.
  /// </summary>
  ISANLog = interface(ISANLogOutput)
  ['{C3BF5BEE-84A2-4BD2-8720-36FB3F6E89A1}']
    {$REGION 'Gets&Sets'}
    function GetName : TSANLogName;
    function GetLocation : TSANLocation;
    procedure SetLocation(const ALocation : TSANLocation);
    function GetDateTimeFormat : WideString;
    procedure SetDateTimeFormat(const ADateTimeFormat : WideString);
    {$ENDREGION}

    /// <summary>
    ///   Создать запись.
    /// </summary>
    /// <returns>
    ///   Открытая запись.
    /// </returns>
    function NewRecord : ISANLogRecord;

    /// <summary>
    ///   Уникальное имя лога, однозначно определяющее его
    ///   в списке логов.
    /// </summary>
    property Name : TSANLogName  read GetName;

    /// <summary>
    ///   Расположение лога.
    /// </summary>
    property Location : TSANLocation  read GetLocation  write SetLocation;

    /// <summary>
    ///   Формат даты и времени для каждой записи в логе. По умолчанию: yyyy.mm.dd hh:nn:ss.zzz
    /// </summary>
    property DateTimeFormat : WideString  read GetDateTimeFormat  write SetDateTimeFormat;
  end;
  {$ENDREGION}

  {$REGION 'Table'}
  /// <summary>
  ///   Колонка таблицы лога.
  /// </summary>
  ISANLogTableColumn = interface(IInterface)
  ['{F65F05C7-AF2D-42E6-AA01-326C0B5392D6}']
    {$REGION 'Gets&Sets'}
    function GetWidth : Word;
    procedure SetWidth(const AWidth : Word);
    function GetAlignment : TAlignment;
    procedure SetAlignment(const AAlignment : TAlignment);
    function GetStrings : TWideStrings;
    {$ENDREGION}

    /// <summary>
    ///   Создать запись.
    /// </summary>
    /// <returns>
    ///   Открытая запись.
    /// </returns>
    function NewRecord : ISANLogRecord;

    /// <summary>
    ///   Ширина колонки
    /// </summary>
    property Width      : Word          read GetWidth      write SetWidth;

    /// <summary>
    ///   Выравнивание текста колонки
    /// </summary>
    property Alignment  : TAlignment    read GetAlignment  write SetAlignment;
    /// <summary>
    ///   Буфер позволяющий сначала назначить текст каждой колонки,
    ///   а затем вывести их все
    /// </summary>
    property Strings    : TWideStrings  read GetStrings;
  end;

  /// <summary>
  ///   Колонки таблицы лога.
  /// </summary>
  ISANLogTableColumns = interface(IInterface)
  ['{C9CEF962-F56C-47F6-9D37-1E18A32B09F9}']
    {$REGION 'Gets&Sets'}
    function GetCount : integer;
    function GetItems(const AIndex: Integer): ISANLogTableColumn;
    function GetMaxRowCount : integer;
    function GetTotalWidth : integer;
    {$ENDREGION}

    /// <summary>
    ///   Заблокировать доступ других потоков.
    /// </summary>
    procedure Lock;

    /// <summary>
    ///   Разблокировать доступ других потоков.
    /// </summary>
    procedure UnLock;

    /// <summary>
    ///   Добавить колонку.
    /// </summary>
    function Add : ISANLogTableColumn;

    /// <summary>
    ///   Первая колонка.
    /// </summary>
    function First: ISANLogTableColumn;

    /// <summary>
    ///   Последняя колонка.
    /// </summary>
    function Last: ISANLogTableColumn;

    /// <summary>
    ///   Задать кол-во и ширину колонок таблицы.
    /// </summary>
    /// <param name="AColWidths">
    ///   Массив ширин колонок.
    /// </param>
    procedure InitColsByWidth(const AColWidths : array of Word);

    /// <summary>
    ///   Очистить текст всех колонок.
    /// </summary>
    procedure AllStringsClear;

    /// <summary>
    ///   Количество колонок.
    /// </summary>
    property Count : integer  read GetCount;

    /// <summary>
    ///   Колонки по индексу.
    /// </summary>
    property Items[const AIndex: Integer]: ISANLogTableColumn read GetItems; default;

    /// <summary>
    ///   Максимальное кол-во строк в тексте среди всех колонок таблицы.
    /// </summary>
    property MaxRowCount : integer  read GetMaxRowCount;

    /// <summary>
    ///   Полная ширина одной строки таблицы с текущими настройками.
    /// </summary>
    property TotalWidth : integer  read GetTotalWidth;
  end;

  /// <summary>
  ///   Таблица лога.
  /// </summary>
  ISANLogTable = interface(ISANLogRecordTargetProvider)
  ['{DCEC4894-8F66-489C-BBD4-4B9E6279BB5D}']
    {$REGION 'Gets&Sets'}
    function GetColumns : ISANLogTableColumns;
    function GetVLineChar : Char;
    procedure SetVLineChar(const AVLineChar : Char);
    function GetHLineChar : Char;
    procedure SetHLineChar(const AHLineChar : Char);
    function GetCrossChar : Char;
    procedure SetCrossChar(const ACrossChar : Char);
    {$ENDREGION}

    /// <summary>
    ///   Вывести горизонтальную линию таблицы.
    /// </summary>
    /// <param name="AShowColsSep">
    ///   True, показывать разделители колонок.
    /// </param>
    procedure WtHrzLine(const AShowColsSep : boolean = True);

    /// <summary>
    ///   Вывод многострочного заголовка таблицы (без деления на колонки)
    ///   из массива строк.
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание заголовка.
    /// </param>
    /// <param name="AStrArr">
    ///   Массив строк заголовка. Каждая строка массива выводится с новой строки.
    /// </param>
    procedure WtHeader(const AAlignment : TAlignment; const AStrArr : array of WideString); overload;

    /// <summary>
    ///   Вывод многострочного заголовка таблицы (без деления на колонки)
    ///   из списка строк
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание заголовка.
    /// </param>
    /// <param name="AStrings">
    ///   Список строк заголовка. Каждая строка списка выводится с новой строки.
    /// </param>
    procedure WtHeader(const AAlignment : TAlignment; const AStrings : TStrings); overload;

    /// <summary>
    ///   Вывод многострочного заголовка таблицы (без деления на колонки)
    ///   из списка строк
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание заголовка.
    /// </param>
    /// <param name="AStrings">
    ///   Список строк заголовка. Каждая строка списка выводится с новой строки.
    /// </param>
    procedure WtHeader(const AAlignment : TAlignment; const AWideStrings : TWideStrings); overload;

    /// <summary>
    ///   Вывод многострочного заголовка таблицы (без деления на колонки)
    ///   из строки разделением по столбцам строки строки ASeparatedStr
    ///   подстрокой ASeparator
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание заголовка.
    /// </param>
    /// <param name="ASeparatedStr">
    ///   Строки заголовка в виде единой строки.
    /// </param>
    /// <param name="ASeparator">
    ///   Разделитель строк, обеспечивающий перенос при выводе.
    /// </param>
    procedure WtHeader(const AAlignment : TAlignment; const ASeparatedStr : WideString;
      const ASeparator : WideString = sLineBreak); overload;

    /// <summary>
    ///   Вывод номеров колонок.
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание.
    /// </param>
    procedure WtColsNumbers(const AAlignment : TAlignment = taLeftJustify);

    /// <summary>
    ///   Вывод строки таблицы из массива строк.
    /// </summary>
    procedure WtRow(const AStrArr : array of WideString); overload;

    /// <summary>
    ///   Вывод строки таблицы из списка строк.
    /// </summary>
    /// <param name="AStrings">
    ///   Текст колонок по строкам.
    /// </param>
    procedure WtRow(const AStrings : TStrings); overload;

    /// <summary>
    ///   Вывод строки таблицы из списка строк.
    /// </summary>
    /// <param name="AStrings">
    ///   Текст колонок по строкам.
    /// </param>
    procedure WtRow(const AStrings : TWideStrings); overload;

    /// <summary>
    ///   Вывод строки таблицы, c разделением по столбцам строки ASeparatedStr
    ///   подстрокой ASeparator.
    /// </summary>
    /// <param name="ASeparatedStr">
    ///   Строки колонок в виде единой строки.
    /// </param>
    /// <param name="ASeparator">
    ///   Разделитель строк, обеспечивающий деление на колонки.
    /// </param>
    procedure WtRow(const ASeparatedStr : WideString; const ASeparator : WideString = '|'); overload;

    /// <summary>
    ///   Вывод строки таблицы из списков строк колонок.
    /// </summary>
    procedure WtColumnsText;

    /// <summary>
    ///   Список колонок таблицы
    /// </summary>
    property Columns : ISANLogTableColumns  read GetColumns;

    /// <summary>
    ///   Символ, вертикальной линии. По умолчанию "|"
    /// </summary>
    property VLineChar : Char  read GetVLineChar  write SetVLineChar;
    /// <summary>
    ///   Символ, горизонтальной линии. По умолчанию "-"
    /// </summary>
    property HLineChar : Char  read GetHLineChar  write SetHLineChar;
    /// <summary>
    ///   Символ, пересечения линиий. По умолчанию "+"
    /// </summary>
    property CrossChar : Char  read GetCrossChar  write SetCrossChar;
  end;
  {$ENDREGION}

  {$REGION 'Tree'}
  ISANLogTree = interface(ISANLogRecordTargetProvider)
  ['{CF91216E-E8B2-4D94-8438-448ACAA8C326}']
    {$REGION 'Gets&Sets'}
    function GetLevelsCount : integer;
    function GetLevelsNext(const ALevel : integer) : boolean;
    function GetVLineChar : Char;
    procedure SetVLineChar(const AVLineChar : Char);
    function GetHLineChar : Char;
    procedure SetHLineChar(const AHLineChar : Char);
    function GetCrossChar : Char;
    procedure SetCrossChar(const ACrossChar : Char);
    function GetVIndent : byte;
    procedure SetVIndent(const AVIndent : byte);
    function GetHIndent : byte;
    procedure SetHIndent(const AHIndent : byte);
    function GetSubIndent : byte;
    procedure SetSubIndent(const ASubIndent : byte);
    function GetLeftIndent : byte;
    procedure SetLeftIndent(const ALeftIndent : byte);
    function GetVIndentIfEmpty : boolean;
    procedure SetVIndentIfEmpty(const AVIndentIfEmpty : boolean);
    {$ENDREGION}

    /// <summary>
    ///   Начать новый узел
    ///   Св-во LevelsNext[] показывает какие ветки продолжаются (при True)
    ///   Если ALevel в пределах [0, LevelsCount - 1] и LevelsNext[ALevel] = True,
    ///   то добавляется подузел уровня ALevel, иначе подузел уровня LevelsCount
    ///   AHasNextSibling - новое значение для LevelsNext[ALevel]
    /// </summary>
    function NewNode(const AHasNextSibling : boolean; const ALevel :integer = -1) : ISANLogRecord;

    /// <summary>
    ///   Завершить текущий узел, отрисовать линии старших узлов (если они есть)
    /// </summary>
    procedure EndNode;

    /// <summary>
    ///   Простой узел с текстом
    /// </summary>
    procedure SimpleNode(const ANodeText : TSANLogRecordText; const AHasNextSibling : boolean;
      const ALevel : Integer = -1);

    /// <summary>
    ///   Кол-во уровней открытых в данный момент
    /// </summary>
    property LevelsCount : integer  read GetLevelsCount;

    /// <summary>
    ///   True - для уровней на которых ожидается следующий,
    ///   False - для уровней на нет следующего узла
    /// </summary>
    property LevelsNext [const ALevel : integer] : boolean  read GetLevelsNext;

    /// <summary>
    /// Символ, вертикальной линии. По умолчанию "|"
    /// </summary>
    property VLineChar : Char  read GetVLineChar  write SetVLineChar;
    /// <summary>
    /// Символ, горизонтальной линии. По умолчанию "-"
    /// </summary>
    property HLineChar : Char  read GetHLineChar  write SetHLineChar;
    /// <summary>
    /// Символ, пересечения линиий. По умолчанию "+"
    /// </summary>
    property CrossChar : Char  read GetCrossChar  write SetCrossChar;

    /// <summary>
    /// Вертикальный отступ.
    /// </summary>
    property VIndent     : byte  read GetVIndent     write SetVIndent;
    /// <summary>
    /// Горизогтальный отступ.
    /// </summary>
    property HIndent     : byte  read GetHIndent     write SetHIndent;
    /// <summary>
    /// Отступ для подэлемента.
    /// </summary>
    property SubIndent   : byte  read GetSubIndent   write SetSubIndent;
    /// <summary>
    /// Отступ слева.
    /// </summary>
    property LeftIndent  : byte  read GetLeftIndent  write SetLeftIndent;

    /// <summary>
    /// Применять ли вертикальный отступ для пустого дерева.
    /// </summary>
    property VIndentIfEmpty : boolean  read GetVIndentIfEmpty  write SetVIndentIfEmpty;
  end;
  {$ENDREGION}

  {$REGION 'Logger'}
  ISANLogger = interface(IInterface)
  ['{069FD74B-16EB-4328-9448-EC1D63A79369}']
    {$REGION 'Gets&Sets'}
    function GetLogsCount : integer;
    function GetLogs(const AIndex : integer) : ISANLog;
    function GetLogByName(const AName : TSANLogName) : ISANLog;
    {$ENDREGION}

    /// <summary>
    ///   Заблокировать доступ других потоков.
    /// </summary>
    procedure Lock;

    /// <summary>
    ///   Разблокировать доступ других потоков.
    /// </summary>
    procedure UnLock;

    /// <summary>
    ///   Попытка поиск лога по имени.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <param name="ALog">
    ///   Найденый лог, если поиск успешен.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, поиск успешен.
    /// </returns>
    function TryFindByName(const AName : TSANLogName; out ALog : ISANLog;
      const AIndexPtr : PInteger = nil) : boolean;

    /// <summary>
    ///   Проверка существования лога по имени.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <returns>
    ///   True, лог cуществует.
    /// </returns>
    function ExistsByName(const AName : TSANLogName) : boolean;

    /// <summary>
    ///   Добавить лог.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <param name="ALocation">
    ///   Расположение лога.
    /// </param>
    /// <returns>
    ///   Экземпляр нового лога.
    /// </returns>
    function Add(const AName : TSANLogName; const ALocation : TSANLocation) : ISANLog;

    /// <summary>
    ///   Создать экземпляр табличного вывода.
    /// </summary>
    function MakeTable : ISANLogTable;

    /// <summary>
    ///   Создать экземпляр древовидного вывода.
    /// </summary>
    function MakeTree : ISANLogTree;

    /// <summary>
    ///   Количество логов в наборе.
    /// </summary>
    property LogsCount : integer  read GetLogsCount;

    /// <summary>
    ///   Лог по индексу.
    /// </summary>
    property Logs[const AIndex : integer] : ISANLog  read GetLogs;

    /// <summary>
    ///   Лог по индексу.
    /// </summary>
    property LogByName[const AName : TSANLogName] : ISANLog  read GetLogByName; default;
  end;
  {$ENDREGION}

/// <summary>
///   Синглтон логгера.
/// </summary>
function Logger : ISANLogger;

implementation

uses
  StrUtils, Math, DateUtils, Types,
  SyncObjs, Generics.Collections;

/// <summary>
///    Строковое представление типа записи лога
/// </summary>
function LogRecordKindToStr(const AKind : TSANLogRecordKind) : string;
begin
  case AKind of
  LRK_INFO     : Result := 'INFO';
  LRK_WARNING  : Result := 'WARNING';
  LRK_ERROR    : Result := 'ERROR';
  LRK_FATAL    : Result := 'FATAL';
  else
    Result := Format('KIND_%d', [AKind]);
  end;
end;

//==============================================================================
{$REGION 'BaseClasses'}
type
  /// <summary>
  ///   Интерфейс расширенной записи лога.
  /// </summary>
  TSANLogOutput = class abstract(TInterfacedObject, ISANLogOutput)
  protected
    {$REGION 'ISANLogOutput'}
    function GetDateTimeFormat : TSANLogRecordText;
    procedure Header(const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      ADateTime : TDateTime = 0);
    procedure Write(const AText : TSANLogRecordText);
    procedure Writeln(const AText : TSANLogRecordText);
    procedure Line(const ALineItem : TSANLogRecordText; const ALineItemsCount : Word = 255;
      const AUseLn : boolean = True);
    {$ENDREGION}

    /// <summary>
    ///   Считать формат даты и времени заголовка.
    /// </summary>
    function DoGetDateTimeFormat : string; virtual; abstract;

    /// <summary>
    ///   Непосредственная запись.
    /// </summary>
    procedure DoWrite(const AText : string); virtual; abstract;

  public
    property DateTimeFormat : TSANLogRecordText  read GetDateTimeFormat;
  end;

  /// <summary>
  ///   Абстрактная запись лога.
  /// </summary>
  TSANLogCustomRecord = class abstract(TSANLogOutput, ISANLogRecord)
  private
    FAutoFlush : boolean;
    FBufferCS : TCriticalSection;
    FClosed : boolean;

  protected
    {$REGION 'ISANLogRecord'}
    function GetBuffer : TSANLogRecordText;
    procedure SetBuffer(const ABuffer : TSANLogRecordText);
    function GetAutoFlush : boolean;
    procedure SetAutoFlush(const AAutoFlush : boolean);
    function GetClosed : boolean;
    procedure Flush;
    procedure Close;
    {$ENDREGION}

    procedure DoWrite(const AText : string); override;
    procedure ValidateClosed; inline;
    procedure DoFlush; virtual; abstract;
    procedure DoClose; virtual;
    function DoGetBuffer : TSANLogRecordText; virtual; abstract;
    procedure DoSetBuffer(const ABuffer : TSANLogRecordText); virtual; abstract;

  public
    constructor Create;
    destructor Destroy; override;

    property Buffer : TSANLogRecordText  read GetBuffer  write SetBuffer;
    property AutoFlush : boolean  read GetAutoFlush  write SetAutoFlush;
  end;

{$REGION 'TSANLogOutput'}
function TSANLogOutput.GetDateTimeFormat: TSANLogRecordText;
begin
  Result := DoGetDateTimeFormat;
end;

procedure TSANLogOutput.Header(const AKind: TSANLogRecordKind;
  const ASubType: TSANLogRecordSubType; ADateTime: TDateTime);
begin

  if CompareValue(ADateTime, 0, OneMillisecond) = EqualsValue then
    ADateTime := Now();

  DoWrite
  (
    Format
    (
      '%s %s %s %s',
      [
        LOG_REC_MARKER,
        FormatDateTime(DateTimeFormat, ADateTime),
        LogRecordKindToStr(AKind),
        ASubType
      ]
    ) + sLineBreak
  );

end;

procedure TSANLogOutput.Line(const ALineItem: TSANLogRecordText;
  const ALineItemsCount: Word; const AUseLn: boolean);
begin
  DoWrite(DupeString(ALineItem, ALineItemsCount) + IfThen(AUseLn, sLineBreak));
end;

procedure TSANLogOutput.Write(const AText: TSANLogRecordText);
begin
  DoWrite(AText);
end;

procedure TSANLogOutput.Writeln(const AText: TSANLogRecordText);
begin
  DoWrite(AText + sLineBreak);
end;
{$ENDREGION}

{$REGION 'TSANLogCustomRecord'}
procedure TSANLogCustomRecord.Close;
begin
  ValidateClosed;

  DoClose;

  FClosed := True;
end;

constructor TSANLogCustomRecord.Create;
begin
  inherited;

  FBufferCS := TCriticalSection.Create;
end;

destructor TSANLogCustomRecord.Destroy;
begin

  if not FClosed then
    Close;

  FreeANdNil(FBufferCS);

  inherited;
end;

procedure TSANLogCustomRecord.DoClose;
begin
end;

procedure TSANLogCustomRecord.DoWrite(const AText: string);
begin
  Buffer := Buffer + AText;
end;

procedure TSANLogCustomRecord.Flush;
begin
  ValidateClosed;

  FBufferCS.Enter;
  try

    DoFlush;

  finally
    FBufferCS.Leave;
  end;
end;

function TSANLogCustomRecord.GetAutoFlush: boolean;
begin
  Result := FAutoFlush;
end;

function TSANLogCustomRecord.GetBuffer: TSANLogRecordText;
begin
  FBufferCS.Enter;
  try

    Result := DoGetBuffer;

  finally
    FBufferCS.Leave;
  end;
end;

function TSANLogCustomRecord.GetClosed: boolean;
begin
  Result := FClosed;
end;

procedure TSANLogCustomRecord.SetAutoFlush(const AAutoFlush: boolean);
begin
  FAutoFlush := AAutoFlush;
end;

procedure TSANLogCustomRecord.SetBuffer(const ABuffer: TSANLogRecordText);
begin
  ValidateClosed;

  FBufferCS.Enter;
  try

    DoSetBuffer(ABuffer);

    if FAutoFlush then
    begin
      DoFlush;
      DoSetBuffer('');
    end;

  finally
    FBufferCS.Leave;
  end;

end;

procedure TSANLogCustomRecord.ValidateClosed;
begin
  if FClosed then
    raise ESANLogError.CreateRes(@sSANLoggerRecordClosedErr);
end;
{$ENDREGION}
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'Table'}
type
  TSANLogTableColumn  = class;
  TSANLogTable        = class;

  /// <summary>
  ///   Одна запись колонки таблицы лога.
  /// </summary>
  TSANLogTableColumnRecord = class(TSANLogCustomRecord)
  private
    FColumn : TSANLogTableColumn;

  protected
    function DoGetBuffer : TSANLogRecordText; override;
    procedure DoSetBuffer(const ABuffer : TSANLogRecordText); override;
    procedure DoFlush; override;
    procedure DoClose; override;
    function DoGetDateTimeFormat : string; override;

  public
    constructor Create(const AColumn : TSANLogTableColumn);
  end;

  /// <summary>
  ///   Колонка таблицы лога.
  /// </summary>
  TSANLogTableColumn = class(TInterfacedObject, ISANLogTableColumn)
  private
    FTable       : TSANLogTable;
    FOpenRecord  : TSANLogTableColumnRecord;
    FWidth       : Word;
    FAlignment   : TAlignment;
    FStrings     : TWideStrings;

  protected
    {$REGION 'ISANLogTableColumn'}
    function GetWidth : Word;
    procedure SetWidth(const AWidth : Word);
    function GetAlignment : TAlignment;
    procedure SetAlignment(const AAlignment : TAlignment);
    function GetStrings : TWideStrings;
    function NewRecord : ISANLogRecord;
    {$ENDREGION}

  public
    constructor Create(const ATable : TSANLogTable; const AWidth : Word = 0;
      const AAlignment : TAlignment = Low(TAlignment));
    destructor Destroy; override;

    /// <summary>
    ///   Ширина колонки
    /// </summary>
    property Width      : Word          read GetWidth      write SetWidth;

    /// <summary>
    ///   Выравнивание текста колонки
    /// </summary>
    property Alignment  : TAlignment    read GetAlignment  write SetAlignment;
    /// <summary>
    ///   Буфер позволяющий сначала назначить текст каждой колонки,
    ///   а затем вывести их все
    /// </summary>
    property Strings    : TWideStrings  read GetStrings;
  end;

  /// <summary>
  ///   Колонки таблицы лога.
  /// </summary>
  TSANLogTableColumns = class(TInterfaceList, ISANLogTableColumns)
  private
    FTable : TSANLogTable;

  protected
    {$REGION 'ISANLogTableColumns'}
    function GetItems(const AIndex: Integer): ISANLogTableColumn;
    function GetMaxRowCount : integer;
    function GetTotalWidth : integer;
    function Add : ISANLogTableColumn;
    function First: ISANLogTableColumn;
    function Last: ISANLogTableColumn;
    procedure InitColsByWidth(const AColWidths : array of Word);
    procedure AllStringsClear;
    {$ENDREGION}

  public
    constructor Create(const ATable : TSANLogTable);
    property Items[const AIndex: Integer]: ISANLogTableColumn read GetItems; default;
    property MaxRowCount : integer  read GetMaxRowCount;
  end;

  /// <summary>
  ///   Таблица лога.
  /// </summary>
  TSANLogTable = class(TInterfacedObject, ISANLogRecordTargetProvider, ISANLogTable)
  private
    FColumns : ISANLogTableColumns;
    FVLineChar, FHLineChar, FCrossChar : Char;
    FTargetRec : ISANLogRecord;

  protected
    {$REGION 'ISANLogRecordTargetProvider'}
    function GetTargetRec : ISANLogRecord;
    procedure SetTargetRec(const ATargetRec : ISANLogRecord);
    {$ENDREGION}

    {$REGION 'ISANLogTable'}
    function GetColumns : ISANLogTableColumns;
    function GetVLineChar : Char;
    procedure SetVLineChar(const AVLineChar : Char);
    function GetHLineChar : Char;
    procedure SetHLineChar(const AHLineChar : Char);
    function GetCrossChar : Char;
    procedure SetCrossChar(const ACrossChar : Char);
    procedure WtHrzLine(const AShowColsSep : boolean = True);
    procedure WtHeader(const AAlignment : TAlignment; const AStrArr : array of WideString); overload;
    procedure WtHeader(const AAlignment : TAlignment; const AStrings : TStrings); overload;
    procedure WtHeader(const AAlignment : TAlignment; const AWideStrings : TWideStrings); overload;
    procedure WtHeader(const AAlignment : TAlignment; const ASeparatedStr : WideString;
      const ASeparator : WideString = sLineBreak); overload;
    procedure WtColsNumbers(const AAlignment : TAlignment = taLeftJustify);
    procedure WtRow(const AStrArr : array of WideString); overload;
    procedure WtRow(const AStrings : TStrings); overload;
    procedure WtRow(const AStrings : TWideStrings); overload;
    procedure WtRow(const ASeparatedStr : WideString; const ASeparator : WideString = '|'); overload;
    procedure WtColumnsText;
    {$ENDREGION}

    /// <summary>
    ///   Форматировать текст AFullCellText в соответствии с параметрами столбца с индексом AColIndex,
    ///   игнорировать выравнивание колонок.
    /// </summary>
    function CellText(const AFullCellText : string; const AColIndex : integer; const AAlignment : TAlignment) : string; overload;

    /// <summary>
    ///   Форматировать текст AFullCellText в соответствии с параметрами столбца с индексом AColIndex.
    /// </summary>
    function CellText(const AFullCellText : string; const AColIndex : integer) : string; overload;

    /// <summary>
    ///   Проверить целевую запись.
    /// </summary>
    procedure ValidateTargetRec; inline;

    /// <summary>
    ///   Передача текста в целевую запись.
    /// </summary>
    procedure WriteToTargetRec(const AText : string); inline;

    /// <summary>
    ///   Передача одной строки заголовка в целевую запись.
    /// </summary>
    procedure WriteHeaderRowToTargetRec(const AAlignment : TAlignment;
      const AHeaderRow : string; const AWidth : integer);

  public
    constructor Create;

    property TargetRec : ISANLogRecord  read GetTargetRec  write SetTargetRec;

    property Columns : ISANLogTableColumns  read GetColumns;

    property VLineChar : Char  read GetVLineChar  write SetVLineChar;
    property HLineChar : Char  read GetHLineChar  write SetHLineChar;
    property CrossChar : Char  read GetCrossChar  write SetCrossChar;
  end;

{$REGION 'TSANLogTableColumnRecord'}
constructor TSANLogTableColumnRecord.Create(const AColumn: TSANLogTableColumn);
begin
  inherited Create;

  FColumn := AColumn;
end;

procedure TSANLogTableColumnRecord.DoClose;
begin
  FColumn.FOpenRecord := nil;
end;

procedure TSANLogTableColumnRecord.DoFlush;
begin
  FColumn.FTable.WtColumnsText;
end;

function TSANLogTableColumnRecord.DoGetBuffer: TSANLogRecordText;
begin
  Result := FColumn.FStrings.Text;
end;

function TSANLogTableColumnRecord.DoGetDateTimeFormat: string;
begin
  FColumn.FTable.ValidateTargetRec;
  Result := FColumn.FTable.TargetRec.DateTimeFormat;
end;

procedure TSANLogTableColumnRecord.DoSetBuffer(
  const ABuffer: TSANLogRecordText);
begin
  FColumn.Strings.Text := ABuffer;
end;
{$ENDREGION}

{$REGION 'TSANLogTableColumn'}
constructor TSANLogTableColumn.Create(const ATable : TSANLogTable;
  const AWidth : Word; const AAlignment : TAlignment);
begin
  inherited Create;

  FTable      := ATable;
  FWidth      := AWidth;
  FAlignment  := AAlignment;

  FStrings    := TWideStringList.Create;
end;

destructor TSANLogTableColumn.Destroy;
begin
  FreeAndNil(FStrings);

  inherited;
end;

function TSANLogTableColumn.GetAlignment: TAlignment;
begin
  Result := FAlignment;
end;

function TSANLogTableColumn.GetStrings: TWideStrings;
begin
  Result := FStrings;
end;

function TSANLogTableColumn.GetWidth: Word;
begin
  Result := FWidth;
end;

function TSANLogTableColumn.NewRecord: ISANLogRecord;
begin
  if Assigned(FOpenRecord) then
    FOpenRecord.Close;

  FOpenRecord := TSANLogTableColumnRecord.Create(Self);
  Result := FOpenRecord;
end;

procedure TSANLogTableColumn.SetAlignment(const AAlignment: TAlignment);
begin
  FAlignment := AAlignment;
end;

procedure TSANLogTableColumn.SetWidth(const AWidth: Word);
begin
  FWidth := AWidth;
end;
{$ENDREGION}

{$REGION 'TSANLogTableColumns'}
function TSANLogTableColumns.Add: ISANLogTableColumn;
begin
  Result := TSANLogTableColumn.Create(FTable);
  inherited Add(Result);
end;

procedure TSANLogTableColumns.AllStringsClear;
var
  i : integer;
begin

  Lock;
  try

    for i := 0 to Count - 1 do
      Items[i].Strings.Clear;

  finally
    Unlock;
  end;

end;

constructor TSANLogTableColumns.Create(const ATable: TSANLogTable);
begin
  inherited Create;

  FTable := ATable;
end;

function TSANLogTableColumns.First: ISANLogTableColumn;
begin
  Result := ISANLogTableColumn(inherited First);
end;

function TSANLogTableColumns.GetItems(const AIndex: Integer): ISANLogTableColumn;
begin
  Result := ISANLogTableColumn(inherited Items[AIndex]);
end;

function TSANLogTableColumns.GetMaxRowCount: integer;
var
  i : integer;
begin

  Lock;
  try

    Result := 0;

    for i := 0 to Count - 1 do
      Result := MAX(Result, Items[i].Strings.Count);

  finally
    Unlock;
  end;

end;

function TSANLogTableColumns.GetTotalWidth: integer;
var
  i : integer;
begin

  Lock;
  try

    Result := 0;

    for i := 0 to Count - 1 do
      Inc(Result, Items[i].Width);

    Inc(Result, Count*3 + 1);

  finally
    Unlock;
  end;

end;

procedure TSANLogTableColumns.InitColsByWidth(const AColWidths: array of Word);
var
  i : integer;
begin
  Lock;
  try

    for i := Low(AColWidths) to High(AColWidths) do
      if i < Count then
        Items[i].Width := AColWidths[i]
      else
        Add.Width := AColWidths[i];

    for i := Count - 1 downto High(AColWidths) + 1 do
      Delete(i);

  finally
    Unlock;
  end;

end;

function TSANLogTableColumns.Last: ISANLogTableColumn;
begin
  Result := ISANLogTableColumn(inherited Last);
end;
{$ENDREGION}

{$REGION 'TSANLogTable'}
function TSANLogTable.CellText(const AFullCellText: string;
  const AColIndex: integer; const AAlignment: TAlignment): string;
var
  Column : ISANLogTableColumn;
  DLgth : integer;
begin
  Column := Columns[AColIndex];
  DLgth := Column.Width - Length(AFullCellText);

  if DLgth <= 0 then
    Result := Copy(AFullCellText, 1, Column.Width)

  else
    case AAlignment of
      taLeftJustify:
        Result := AFullCellText + DupeString(' ', DLgth);

      taRightJustify:
        Result := DupeString(' ', DLgth) + AFullCellText;

      taCenter:
        Result := DupeString(' ', DLgth div 2) +
                  AFullCellText +
                  DupeString(' ', DLgth div 2) +
                  IfThen(Odd(DLgth), ' ');
    end;

  Result := ' ' + Result + ' ' + VLineChar;
end;

function TSANLogTable.CellText(const AFullCellText: string;
  const AColIndex: integer): string;
begin
  Result := CellText(AFullCellText, AColIndex, Columns[AColIndex].Alignment);
end;

constructor TSANLogTable.Create;
begin
  inherited Create;

  FColumns := TSANLogTableColumns.Create(Self);

  FVLineChar := '|';
  FHLineChar := '-';
  FCrossChar := '+';
end;

function TSANLogTable.GetColumns: ISANLogTableColumns;
begin
  Result := FColumns;
end;

function TSANLogTable.GetCrossChar: Char;
begin
  Result := FCrossChar;
end;

function TSANLogTable.GetHLineChar: Char;
begin
  Result := FHLineChar;
end;

function TSANLogTable.GetTargetRec: ISANLogRecord;
begin
  Result := FTargetRec;
end;

function TSANLogTable.GetVLineChar: Char;
begin
  Result := FVLineChar;
end;

procedure TSANLogTable.SetCrossChar(const ACrossChar: Char);
begin
  FCrossChar := ACrossChar;
end;

procedure TSANLogTable.SetHLineChar(const AHLineChar: Char);
begin
  FHLineChar := AHLineChar;
end;

procedure TSANLogTable.SetTargetRec(const ATargetRec: ISANLogRecord);
begin
  FTargetRec := ATargetRec;
end;

procedure TSANLogTable.SetVLineChar(const AVLineChar: Char);
begin
  FVLineChar := AVLineChar;
end;

procedure TSANLogTable.ValidateTargetRec;
begin
  if not Assigned(FTargetRec) then
    raise ESANLogError.CreateRes(@sSANLoggerTargetReccordMissingErr);
end;

procedure TSANLogTable.WriteHeaderRowToTargetRec(const AAlignment : TAlignment;
  const AHeaderRow: string; const AWidth: integer);
var
  dWidth : integer;
  S : string;
begin
  dWidth := AWidth - Length(AHeaderRow);

  case AAlignment of
    taLeftJustify:
      S := AHeaderRow + DupeString(' ', dWidth);

    taRightJustify:
      S := DupeString(' ', dWidth) + AHeaderRow;

    taCenter:
      S := DupeString(' ', dWidth div 2) + AHeaderRow + DupeString(' ', dWidth div 2 + ORD(Odd(dWidth)));

  end;

  WriteToTargetRec(VLineChar + ' ' + S + ' ' + VLineChar);
end;

procedure TSANLogTable.WriteToTargetRec(const AText: string);
begin
  ValidateTargetRec;

  FTargetRec.Writeln(AText);
end;

procedure TSANLogTable.WtColsNumbers(const AAlignment: TAlignment);
var
  i : integer;
  Row : string;
begin
  Row := VLineChar;

  for i := 0 to Columns.Count - 1 do
    Row := Row + CellText(IntToStr(i + 1), i, AAlignment);

  WriteToTargetRec(Row);
end;

procedure TSANLogTable.WtColumnsText;
var
  i, RowIndex, RowCount : integer;
  Row : string;
begin
  Columns.Lock;
  try

    RowCount := Columns.MaxRowCount;

    for RowIndex := 0 to RowCount - 1 do
    begin
      Row := VLineChar;

      for i := 0 to Columns.Count - 1 do
        with Columns[i].Strings do
          if RowIndex < Count then
            Row := Row + CellText(Strings[RowIndex], i)
          else
            Row := Row + CellText('', i);

      WriteToTargetRec(Row);
    end;

    Columns.AllStringsClear;

  finally
    Columns.UnLock;
  end;
end;

procedure TSANLogTable.WtHeader(const AAlignment: TAlignment;
  const AWideStrings: TWideStrings);
var
  i, Width : integer;
begin

  if AWideStrings.Count = 0 then
    Exit;

  Width := Columns.TotalWidth - 4;

  for i := 0 to AWideStrings.Count - 1 do
    WriteHeaderRowToTargetRec(AAlignment, AWideStrings[i], Width);
end;

procedure TSANLogTable.WtHeader(const AAlignment: TAlignment;
  const AStrings: TStrings);
var
  i, Width : integer;
begin

  if AStrings.Count = 0 then
    Exit;

  Width := Columns.TotalWidth - 4;

  for i := 0 to AStrings.Count - 1 do
    WriteHeaderRowToTargetRec(AAlignment, AStrings[i], Width);
end;

procedure TSANLogTable.WtHeader(const AAlignment: TAlignment;
  const AStrArr: array of WideString);
var
  i, Width : integer;
begin

  if Length(AStrArr) = 0 then
    Exit;

  Width := Columns.TotalWidth - 4;

  for i := Low(AStrArr) to High(AStrArr) do
    WriteHeaderRowToTargetRec(AAlignment, AStrArr[i], Width);
end;

procedure TSANLogTable.WtHeader(const AAlignment: TAlignment;
  const ASeparatedStr, ASeparator: WideString);
var
  Width, Pos, Offset, L : integer;
begin
  Width := Columns.TotalWidth - 4;
  L := Length(ASeparator);
  
  Offset  := 1;

  repeat
    Pos := PosEx(ASeparator, ASeparatedStr, Offset);

    if Pos = 0 then
      Break;

    WriteHeaderRowToTargetRec(AAlignment, Copy(ASeparatedStr, Offset, Pos - Offset - L + 1), Width);

    Offset := Pos + L;
  until False;

  WriteHeaderRowToTargetRec(AAlignment, Copy(ASeparatedStr, Offset, Length(ASeparatedStr) - Offset + 1), Width);
end;

procedure TSANLogTable.WtHrzLine(const AShowColsSep: boolean);
var
  S : string;
//------------------------------------------------------------------------------
  procedure AddColumnChars(const AColIndex : integer; const AEndChar : Char);
  begin
    S := S +
         DupeString(HLineChar, Columns[AColIndex].Width + 2) +
         AEndChar;
  end;
//------------------------------------------------------------------------------
var
  i : integer;
  ColsSep : Char;
begin
  S := CrossChar;

  if AShowColsSep then
    ColsSep := CrossChar
  else
    ColsSep := HLineChar;

  Columns.Lock;
  try

    for i := 0 to Columns.Count - 2 do
      AddColumnChars(i, ColsSep);

    AddColumnChars(Columns.Count - 1, CrossChar);

  finally
    Columns.UnLock;
  end;

  WriteToTargetRec(S);
end;

procedure TSANLogTable.WtRow(const AStrings: TWideStrings);
var
  i : integer;
  Row : string;
begin
  Row := VLineChar;

  for i := 0 to MIN(AStrings.Count - 1, Columns.Count - 1) do
    Row := Row + CellText(AStrings[i], i);

  for i := AStrings.Count to Columns.Count - 1 do
    Row := Row + CellText('', i);

  WriteToTargetRec(Row);
end;

procedure TSANLogTable.WtRow(const ASeparatedStr, ASeparator: WideString);
var
  Pos, Offset, L, Cols, i : integer;
  Row : string;
begin
  Offset  := 1;
  Cols    := 0;
  L       := Length(ASeparator);
  Row     := VLineChar;

  repeat
    Pos := PosEx(ASeparator, ASeparatedStr, Offset);

    if Pos = 0 then
      Break;

    Row := Row + CellText(Copy(ASeparatedStr, Offset, Pos - Offset - L + 1), Cols);

    Offset := Pos + L;
    Inc(Cols);
  until Cols >= Columns.Count;

  if Cols < Columns.Count then
    Row := Row + CellText(Copy(ASeparatedStr, Offset, Length(ASeparatedStr) - Offset + 1), Cols);
  
  for I := Cols + 1 to Columns.Count - 1 do
    Row := Row + CellText('', i);

  WriteToTargetRec(Row);
end;

procedure TSANLogTable.WtRow(const AStrArr: array of WideString);
var
  i : integer;
  Row : string;
begin
  Row := VLineChar;

  for i := 0 to MIN(High(AStrArr), Columns.Count - 1) do
    Row := Row + CellText(AStrArr[i], i);

  for i := Length(AStrArr) to Columns.Count - 1 do
    Row := Row + CellText('', i);

  WriteToTargetRec(Row);
end;

procedure TSANLogTable.WtRow(const AStrings: TStrings);
var
  i : integer;
  Row : string;
begin
  Row := VLineChar;

  for i := 0 to MIN(AStrings.Count - 1, Columns.Count - 1) do
    Row := Row + CellText(AStrings[i], i);

  for i := AStrings.Count to Columns.Count - 1 do
    Row := Row + CellText('', i);

  WriteToTargetRec(Row);
end;
{$ENDREGION}
//------------------------------------------------------------------------------
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'Tree'}
type
  TSANLogTree = class;

  /// <summary>
  ///   Одна запись дерева лога.
  /// </summary>
  TSANLogTreeRecord = class(TSANLogCustomRecord, ISANLogRecord)
  private
    FTree : TSANLogTree;
    FBuffer : TSANLogRecordText;

  protected
    {$REGION 'ISANLogRecord'}
    procedure SetAutoFlush(const AAutoFlush : boolean);
    {$ENDREGION}

    function DoGetBuffer : TSANLogRecordText; override;
    procedure DoSetBuffer(const ABuffer : TSANLogRecordText); override;
    procedure DoFlush; override;
    procedure DoClose; override;
    function DoGetDateTimeFormat : string; override;

  public
    constructor Create(const ATree : TSANLogTree);
  end;

  /// <summary>
  ///   Таблица лога.
  /// </summary>
  TSANLogTree = class(TInterfacedObject, ISANLogRecordTargetProvider, ISANLogTree)
  private
    FTargetRec : ISANLogRecord;
    FOpenRecord  : TSANLogTreeRecord;

    FLevels : TList<Boolean>;

    FVLineChar, FHLineChar, FCrossChar : Char;
    FVIndent, FHIndent, FSubIndent, FLeftIndent : byte;

    FFirstLine, FNotAddIndentStr, FVIndentIfEmpty : boolean;

  strict private
    /// <summary>
    ///   Внутрення передача текста в целевую запись.
    /// </summary>
    procedure InnerWriteToTargetRec(const AText: string; const AIndex, ACount : Integer); //inline;

  protected
    {$REGION 'ISANLogRecordTargetProvider'}
    function GetTargetRec : ISANLogRecord;
    procedure SetTargetRec(const ATargetRec : ISANLogRecord);
    {$ENDREGION}

    {$REGION 'ISANLogTree'}
    function GetLevelsCount : integer;
    function GetLevelsNext(const ALevel : integer) : boolean;
    function GetVLineChar : Char;
    procedure SetVLineChar(const AVLineChar : Char);
    function GetHLineChar : Char;
    procedure SetHLineChar(const AHLineChar : Char);
    function GetCrossChar : Char;
    procedure SetCrossChar(const ACrossChar : Char);
    function GetVIndent : byte;
    procedure SetVIndent(const AVIndent : byte);
    function GetHIndent : byte;
    procedure SetHIndent(const AHIndent : byte);
    function GetSubIndent : byte;
    procedure SetSubIndent(const ASubIndent : byte);
    function GetLeftIndent : byte;
    procedure SetLeftIndent(const ALeftIndent : byte);
    function GetVIndentIfEmpty : boolean;
    procedure SetVIndentIfEmpty(const AVIndentIfEmpty : boolean);
    function NewNode(const AHasNextSibling : boolean; const ALevel :integer = -1) : ISANLogRecord;
    procedure EndNode;
    procedure SimpleNode(const ANodeText : TSANLogRecordText; const AHasNextSibling : boolean;
      const ALevel : Integer = -1);
    {$ENDREGION}

    /// <summary>
    ///   Проверить целевую запись.
    /// </summary>
    procedure ValidateTargetRec; inline;

    /// <summary>
    ///   Передача текста в целевую запись.
    /// </summary>
    procedure WriteToTargetRec(const AText : string); inline;

    /// <summary>
    ///   Cформировать строку отступа.
    /// </summary>
    function IndentStr(const AForceVrtLine : boolean = False) : string;

  public
    constructor Create;
    destructor Destroy; override;

    property TargetRec : ISANLogRecord  read GetTargetRec  write SetTargetRec;

    property LevelsCount : integer  read GetLevelsCount;
    property LevelsNext [const ALevel : integer] : boolean  read GetLevelsNext;

    property VLineChar : Char  read GetVLineChar  write SetVLineChar;
    property HLineChar : Char  read GetHLineChar  write SetHLineChar;
    property CrossChar : Char  read GetCrossChar  write SetCrossChar;

    property VIndent     : byte  read GetVIndent     write SetVIndent;
    property HIndent     : byte  read GetHIndent     write SetHIndent;
    property SubIndent   : byte  read GetSubIndent   write SetSubIndent;
    property LeftIndent  : byte  read GetLeftIndent  write SetLeftIndent;
    property VIndentIfEmpty : boolean  read GetVIndentIfEmpty  write SetVIndentIfEmpty;
  end;

{$REGION 'TSANLogTreeRecord'}
constructor TSANLogTreeRecord.Create(const ATree: TSANLogTree);
begin
  inherited Create();

  FAutoFlush := True;
  FTree := ATree;
end;

procedure TSANLogTreeRecord.SetAutoFlush(const AAutoFlush: boolean);
begin
end;

procedure TSANLogTreeRecord.DoClose;
begin
  FTree.FOpenRecord := nil;
end;

procedure TSANLogTreeRecord.DoFlush;
begin
  FTree.WriteToTargetRec(FBuffer);
end;

function TSANLogTreeRecord.DoGetBuffer: TSANLogRecordText;
begin
  Result := FBuffer;
end;

function TSANLogTreeRecord.DoGetDateTimeFormat: string;
begin
  FTree.ValidateTargetRec;
  Result := FTree.TargetRec.DateTimeFormat;
end;

procedure TSANLogTreeRecord.DoSetBuffer(const ABuffer: TSANLogRecordText);
begin
  FBuffer := ABuffer;
end;
{$ENDREGION}

{$REGION 'TSANLogTree'}
constructor TSANLogTree.Create;
begin
  inherited Create;

  FLevels := TList<Boolean>.Create;

  FVLineChar := '|';
  FHLineChar := '-';
  FCrossChar := '+';

  FHIndent     := 2;
  FVIndent     := 1;
  FSubIndent   := 2;
  FLeftIndent  := 2;
end;

destructor TSANLogTree.Destroy;
begin
  FreeAndNil(FLevels);

  inherited;
end;

function TSANLogTree.GetCrossChar: Char;
begin
  Result := FCrossChar;
end;

function TSANLogTree.GetHIndent: byte;
begin
  Result := FHIndent;
end;

function TSANLogTree.GetHLineChar: Char;
begin
  Result := FHLineChar;
end;

function TSANLogTree.GetLeftIndent: byte;
begin
  Result := FLeftIndent;
end;

function TSANLogTree.GetLevelsCount: integer;
begin
  Result := FLevels.Count;
end;

function TSANLogTree.GetLevelsNext(const ALevel: integer): boolean;
begin
  Result := FLevels[ALevel];
end;

function TSANLogTree.GetSubIndent: byte;
begin
  Result := FSubIndent;
end;

function TSANLogTree.GetTargetRec: ISANLogRecord;
begin
  Result := FTargetRec;
end;

function TSANLogTree.GetVIndent: byte;
begin
  Result := FVIndent;
end;

function TSANLogTree.GetVIndentIfEmpty: boolean;
begin
  Result := FVIndentIfEmpty;
end;

function TSANLogTree.GetVLineChar: Char;
begin
  Result := FVLineChar;
end;

procedure TSANLogTree.SetCrossChar(const ACrossChar: Char);
begin
  FCrossChar := ACrossChar;
end;

procedure TSANLogTree.SetHIndent(const AHIndent: byte);
begin
  FHIndent := AHIndent;
end;

procedure TSANLogTree.SetHLineChar(const AHLineChar: Char);
begin
  FHLineChar := AHLineChar;
end;

procedure TSANLogTree.SetLeftIndent(const ALeftIndent: byte);
begin
  FLeftIndent := ALeftIndent;
end;

procedure TSANLogTree.SetSubIndent(const ASubIndent: byte);
begin
  FSubIndent := ASubIndent;
end;

procedure TSANLogTree.SetTargetRec(const ATargetRec: ISANLogRecord);
begin
  FTargetRec := ATargetRec;
end;

procedure TSANLogTree.SetVIndent(const AVIndent: byte);
begin
  FVIndent := AVIndent;
end;

procedure TSANLogTree.SetVIndentIfEmpty(const AVIndentIfEmpty: boolean);
begin
  FVIndentIfEmpty := AVIndentIfEmpty;
end;

procedure TSANLogTree.SetVLineChar(const AVLineChar: Char);
begin
  FVLineChar := AVLineChar;
end;

function TSANLogTree.IndentStr(const AForceVrtLine: boolean): string;
var
  i : integer;
begin

  if LevelsCount > 0 then
    Result := DupeString(' ', FLeftIndent)
  else
    Result := '';

  for i := 0 to LevelsCount - 2 do
    Result := Result
            + IfThen(LevelsNext[i], VLineChar, ' ')
            + DupeString(' ', FHIndent + FSubIndent);

  Result := Result
          + IfThen(FFirstLine, CrossChar, IfThen(FLevels.Last or AForceVrtLine, VLineChar, ' ') )
          + DupeString( IfThen(FFirstLine, HLineChar, ' '), FHIndent);
end;

function TSANLogTree.NewNode(const AHasNextSibling : boolean;
  const ALevel :integer = -1) : ISANLogRecord;
var
  RowIndex : byte;
  EmptyTree : boolean;
begin
  EmptyTree := (LevelsCount = 0);

  if (ALevel < 0) or (ALevel > LevelsCount - 1) or (not LevelsNext[ALevel]) then
    FLevels.Add(AHasNextSibling)

  else
  begin
    FLevels[ALevel] := AHasNextSibling;
    FLevels.Count := MIN(FLevels.Count, ALevel + 1)
  end;


  if not Assigned(FOpenRecord) then
    FOpenRecord := TSANLogTreeRecord.Create(Self);

  Result := FOpenRecord;


  if (not EmptyTree) or (FVIndentIfEmpty) then
  begin
    ValidateTargetRec;

    for RowIndex := 1 to VIndent do
    begin
      FNotAddIndentStr := True;
      FOpenRecord.Writeln(IndentStr(True));
    end;
  end;

  FFirstLine := True;
end;

procedure TSANLogTree.EndNode;
begin

  with FLevels do
    while (Count > 0) and not FLevels.Last do
      Count := Count - 1;

  if Assigned(FOpenRecord) and (FLevels.Count = 0) then
    FOpenRecord.Close;
end;

procedure TSANLogTree.SimpleNode(const ANodeText: TSANLogRecordText;
  const AHasNextSibling: boolean; const ALevel: Integer);
begin
  NewNode(AHasNextSibling, ALevel);
  try
    FOpenRecord.Write(ANodeText);
  finally
    EndNode;
  end;
end;

procedure TSANLogTree.ValidateTargetRec;
begin
  if not Assigned(FTargetRec) then
    raise ESANLogError.CreateRes(@sSANLoggerTargetReccordMissingErr);
end;

procedure TSANLogTree.InnerWriteToTargetRec(const AText: string; const AIndex,
  ACount: Integer);
begin
  if FNotAddIndentStr then
    FTargetRec.Write(Copy(AText, AIndex, ACount))

  else
  begin
    FTargetRec.Write(IndentStr + Copy(AText, AIndex, ACount));
    FNotAddIndentStr := True;
  end;

  FFirstLine := False;
end;

procedure TSANLogTree.WriteToTargetRec(const AText: string);
var
  Pos, Offset : integer;
begin
  ValidateTargetRec;

  Offset := 1;
  repeat
    Pos := PosEx(sLineBreak, AText, Offset);

    if Pos = 0 then
      Break;

    InnerWriteToTargetRec(AText, Offset, Pos - Offset + 2);
    FNotAddIndentStr := False;

    Offset := Pos + 2;
  until FALSE;

  if Length(AText) > Offset + 1 then
    InnerWriteToTargetRec(AText, Offset, Length(AText) - Offset + 1);

  FFirstLine := False;
end;
{$ENDREGION}

{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'Log'}
type
  TSANLog = class;

  /// <summary>
  ///   Одна запись лога.
  /// </summary>
  TSANLogRecord = class(TSANLogCustomRecord)
  private
    FBuffer : TSANLogRecordText;
    FTestFile : System.Text;
    FLog : TSANLog;
    FOpenSuccessfully : boolean;

  protected
    function DoGetBuffer : TSANLogRecordText; override;
    procedure DoSetBuffer(const ABuffer : TSANLogRecordText); override;
    procedure DoFlush; override;
    procedure DoClose; override;
    function DoGetDateTimeFormat : string; override;

  public
    constructor Create(const ALog : TSANLog);
  end;

  /// <summary>
  ///   Лог.
  /// </summary>
  TSANLog = class(TSANLogOutput, ISANLog)
  private
    FWriteRecCS : TCriticalSection;
    FName : TSANLogName;
    FDateTimeFormat : WideString;
    FLocation : TSANLocation;
    FOpenRecord : TSANLogRecord;

  protected
    {$REGION 'ISANLog'}
    function GetName : TSANLogName;
    function GetLocation : TSANLocation;
    procedure SetLocation(const ALocation : TSANLocation);
    function GetDateTimeFormat : WideString;
    procedure SetDateTimeFormat(const ADateTimeFormat : WideString);
    function NewRecord : ISANLogRecord;
    {$ENDREGION}

    /// <summary>
    ///   Развернуть Location c учетом форматированных дат внутри '|'.
    /// </summary>
    function ExpandLocation : string;

    function DoGetDateTimeFormat : string; override;
    procedure DoWrite(const AText : string); override;

  public
    constructor Create(const AName : TSANLogName);
    destructor Destroy; override;

    property Name : TSANLogName  read GetName;
    property Location : TSANLocation  read GetLocation  write SetLocation;

  end;

{$REGION 'TSANLogRecord'}
procedure TSANLogRecord.DoClose;
begin

  if FOpenSuccessfully then
    CloseFile(FTestFile);

  FLog.FOpenRecord := nil;

  FLog.FWriteRecCS.Leave;
end;

constructor TSANLogRecord.Create(const ALog : TSANLog);
//------------------------------------------------------------------------------
  function Open : boolean;
  begin
    {$I-}
    Append(FTestFile);

    if IOResult = 0 then
      Exit(True);

    {$I+}

    Rewrite(FTestFile);

    Result := True;
  end;
//------------------------------------------------------------------------------
begin
  FLog := ALog;

  inherited Create;

  FAutoFlush := True;
  
  AssignFile(FTestFile, FLog.ExpandLocation);

  FOpenSuccessfully := Open;
end;

procedure TSANLogRecord.DoFlush;
begin
  System.Write(FTestFile, FBuffer);
end;

function TSANLogRecord.DoGetBuffer: TSANLogRecordText;
begin
  Result := FBuffer;
end;

function TSANLogRecord.DoGetDateTimeFormat: string;
begin
  Result := FLog.FDateTimeFormat;
end;

procedure TSANLogRecord.DoSetBuffer(const ABuffer: TSANLogRecordText);
begin
  FBuffer := ABuffer;
end;
{$ENDREGION}

{$REGION 'TSANLog'}
constructor TSANLog.Create(const AName : TSANLogName);
begin
  inherited Create;

  FDateTimeFormat := 'yyyy.mm.dd hh:nn:ss.zzz';

  FName := AName;

  FWriteRecCS := TCriticalSection.Create;
end;

destructor TSANLog.Destroy;
begin
  FreeAndNil(FWriteRecCS);

  inherited;
end;

function TSANLog.DoGetDateTimeFormat: string;
begin
  Result := FDateTimeFormat;
end;

procedure TSANLog.DoWrite(const AText: string);
begin
  NewRecord.Buffer := AText;
end;

function TSANLog.GetName: TSANLogName;
begin
  Result := FName;
end;

function TSANLog.NewRecord: ISANLogRecord;
begin
  FWriteRecCS.Enter;

  if Assigned(FOpenRecord) then
    FOpenRecord.Close;

  FOpenRecord := TSANLogRecord.Create(Self);
  Result := FOpenRecord;
end;

function TSANLog.GetDateTimeFormat: WideString;
begin
  Result := FDateTimeFormat;
end;

procedure TSANLog.SetDateTimeFormat(const ADateTimeFormat: WideString);
begin
  FDateTimeFormat := ADateTimeFormat;
end;

function TSANLog.GetLocation: TSANLocation;
begin
  Result := FLocation;
end;

procedure TSANLog.SetLocation(const ALocation: TSANLocation);
begin
  FLocation := ALocation;
end;

function TSANLog.ExpandLocation: string;
var
  Now : TDateTime;
  S, DS : string;
  UseFmt : boolean;
  Pos, Offset : integer;
begin
  Result  := '';
  S       := FLocation;
  UseFmt  := False;
  Offset  := 1;
  Now     := SysUtils.Now();

  repeat
    Pos := PosEx('|', S, Offset);

    if Pos = 0 then
      Break;

    DS := Copy(S, Offset, Pos - Offset);

    if UseFmt then
      Result := Result + FormatDateTime(DS, Now)
    else
      Result := Result + DS;

    UseFmt := not UseFmt;
    Offset := Pos + 1;
  until False;

  Result := Result + Copy(S, Offset, Length(S) - Offset + 1);
end;
{$ENDREGION}
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogger'}
type
  TSANLogger = class(TInterfaceList, ISANLogger)
  protected
    {$REGION 'ISANLogger'}
    // procedure Lock;                                              <- inherited
    // procedure UnLock;                                            <- inherited
    function GetLogsCount : integer;
    function GetLogs(const AIndex : integer) : ISANLog;
    function GetLogByName(const AName : TSANLogName) : ISANLog;
    function TryFindByName(const AName : TSANLogName; out ALog : ISANLog;
      const AIndexPtr : PInteger = nil) : boolean;
    function ExistsByName(const AName : TSANLogName) : boolean;
    function Add(const AName : TSANLogName; const ALocation : TSANLocation) : ISANLog;
    function MakeTable : ISANLogTable;
    function MakeTree : ISANLogTree;
    {$ENDREGION}

  public
    /// <summary>
    ///   Количество логов в наборе.
    /// </summary>
    property LogsCount : integer  read GetLogsCount;

    /// <summary>
    ///   Лог по индексу.
    /// </summary>
    property Logs[const AIndex : integer] : ISANLog  read GetLogs;

    /// <summary>
    ///   Лог по индексу.
    /// </summary>
    property LogByName[const AName : TSANLogName] : ISANLog  read GetLogByName; default;
  end;

function TSANLogger.Add(const AName: TSANLogName;
  const ALocation: TSANLocation): ISANLog;
begin

  if ExistsByName(AName) then
    raise ESANLogError.CreateResFmt(@sSANLoggerLogAlreadyExistsFmtErr, [AName]);

  Result := TSANLog.Create(AName);

  Result.Location := ALocation;

  inherited Add(Result);
end;

function TSANLogger.ExistsByName(const AName: TSANLogName): boolean;
var
  Dummy : ISANLog;
begin
  Result := TryFindByName(AName, Dummy);
end;

function TSANLogger.GetLogByName(const AName: TSANLogName): ISANLog;
begin
  if not TryFindByName(AName, Result) then
    raise ESANLogError.CreateResFmt(@sSANLoggerLogNotFoundFmtErr, [AName]);
end;

function TSANLogger.GetLogs(const AIndex: integer): ISANLog;
begin
  Result := ISANLog(Items[AIndex]);
end;

function TSANLogger.GetLogsCount: integer;
begin
  Result := Count;
end;

function TSANLogger.MakeTable: ISANLogTable;
begin
  Result := TSANLogTable.Create;
end;

function TSANLogger.MakeTree: ISANLogTree;
begin
  Result := TSANLogTree.Create;
end;

function TSANLogger.TryFindByName(const AName: TSANLogName; out ALog: ISANLog;
  const AIndexPtr: PInteger): boolean;
var
  i : integer;
begin

  for i := 0 to Count - 1 do
    if WideSameText(AName, Logs[i].Name) then
    begin
      ALog := Logs[i];

      if Assigned(AIndexPtr) then
        AIndexPtr^ := i;

      Exit(True);
    end;

  ALog    := nil;
  Result  := False;
end;
{$ENDREGION}
//==============================================================================

var
  InnerLogger : ISANLogger;

function Logger : ISANLogger;
begin
  Result := InnerLogger;
end;

initialization
  InnerLogger := TSANLogger.Create;

end.
