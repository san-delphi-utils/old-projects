{*******************************************************}
{                                                       }
{       Logger                                          }
{                                                       }
{       Хранилище в виде файлов.                        }
{                                                       }
{       Перефикс: SANLog                                }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SANLoggerStorageFiles;

interface

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  Windows, Messages, SysUtils, Classes, Variants, Math, DateUtils, SyncObjs,
  {$ENDREGION}

  {$REGION 'SANCore'}
  SANCoreIntfs, SANCoreImpl,
  {$ENDREGION}

  {$REGION 'SANLogger'}
  SANLoggerIntfs, SANLoggerContexts, SANLoggerCustom;
  {$ENDREGION}
{$ENDREGION}

{$REGION 'resourcestring'}
resourcestring
  sSANLoggerStorageFilesInvalidLocationParamsCountFmt =
    'Invalid location params count "%d". Need one parameter';

  sSANLoggerStorageFilesInvalidLocationParamNeedFileNameFmt =
    'Invalid location param "%s". Need "%s"';
{$ENDREGION}

const
  /// <summary>
  ///   Параметр: Имя файла.
  /// </summary>
  LOGGER_FILES_PARAM_FILE_NAME = 'FileName';

type
  /// <summary>
  ///   Исключение файлового лога.
  /// </summary>
  ESANLogFilesError = class(ESANLogCustomError);

  TSANLogsStorageFiles = class;

  /// <summary>
  ///   Лог в файле.
  /// </summary>
  TSANLogFiles = class(TSANLogCustom,
    ISANCorePersistent, ISANCorePersistentSafe,
    ISANLogCustom, ISANLog, ISANLoggerMaker, ISANLogCustomNotification)

  private
    function GetStorage: TSANLogsCustomStorage;

  protected
    procedure DoSetLocation(var ALocation : ISANLogLocation;
      const ANewLocation: ISANLogLocation); override;

    property Storage : TSANLogsCustomStorage  read GetStorage;
  end;

  /// <summary>
  ///   Хранилище логов в файле.
  /// </summary>
  TSANLogsStorageFiles = class(TSANLogsCustomStorage,
    ISANCorePersistent, ISANCorePersistentSafe,
    ISANCoreModuleHandleProvider, ISANCoreModuleHandleProviderSafe,
    ISANLogsSet, ISANLogsStorage, ISANLoggerMaker)

  protected
    function DoCreateLog(const AName : TSANLogName) : ISANLog; override;
    procedure DoCheckLocation(const ALocation : ISANLogLocation;
      out AErrors : TSANLogRecordText; out AIsCorrect : WordBool); override;
    procedure DoIOThreadCommand(const AIOContext : ISANLogIOContext); override;

  end;


implementation

//==============================================================================
{$REGION 'TSANLogFiles'}
procedure TSANLogFiles.DoSetLocation(var ALocation: ISANLogLocation;
  const ANewLocation: ISANLogLocation);
begin
  inherited;

end;

function TSANLogFiles.GetStorage: TSANLogsCustomStorage;
begin
  Result := (inherited Storage) as TSANLogsCustomStorage;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogsStorageFiles'}
procedure TSANLogsStorageFiles.DoCheckLocation(const ALocation: ISANLogLocation;
  out AErrors: TSANLogRecordText; out AIsCorrect: WordBool);
begin
  AIsCorrect := False;

  if ALocation.ParamsCount <> 1 then
  begin
    AErrors := Format
    (
      LoadResString(@sSANLoggerStorageFilesInvalidLocationParamsCountFmt),
      [ALocation.ParamsCount]
    );

    Exit;
  end;

  if not WideSameText(ALocation.Params[0].Name, LOGGER_FILES_PARAM_FILE_NAME) then
  begin
    AErrors := Format
    (
      LoadResString(@sSANLoggerStorageFilesInvalidLocationParamNeedFileNameFmt),
      [ALocation.Params[0].Name, LOGGER_FILES_PARAM_FILE_NAME]
    );

    Exit;
  end;

  AIsCorrect := True;
end;

function TSANLogsStorageFiles.DoCreateLog(const AName: TSANLogName): ISANLog;
begin
  Result := TSANLogFiles.Create(Self, AName);
end;

procedure TSANLogsStorageFiles.DoIOThreadCommand(
  const AIOContext: ISANLogIOContext);
begin
  inherited;

end;
{$ENDREGION}
//==============================================================================

(*
  ISANLogIOWriteRecordContext = interface(ISANLogIOCustomWriteRecordContext)
  ISANLogIOBeginWriteContext = interface(ISANLogIOCustomWriteRecordContext)
  ISANLogIOPartialWriteContext = interface(ISANLogIOCustomWriteRecordContext)
  ISANLogIOEndWriteContext = interface(ISANLogIOCustomWriteRecordContext)
  ISANLogIOWriteRecordsContext = interface(ISANLogIOContext)
  ISANLogIOReadContext = interface(ISANLogIOContext)
*)

end.
