unit UformLoggerAppMain;

interface

uses
  SANLogger,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TformLoggerTest = class(TForm)
    btn: TButton;
    btn2: TButton;
    btnTable: TButton;
    btn1: TButton;
    btnTree: TButton;
    btnTbTrTb: TButton;
    procedure btnClick(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btnTableClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btnTreeClick(Sender: TObject);
    procedure btnTbTrTbClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formLoggerTest: TformLoggerTest;

implementation

{$R *.dfm}

procedure TformLoggerTest.btn1Click(Sender: TObject);
var
  Log : ISANLog;
  LR : ISANLogRecord;
  T1, T11, T12, T13 : ISANLogTable;
begin
  Log := Logger.Add('btn1', 'Log.txt');

  LR := Log.NewRecord;

  LR.Header(LRK_INFO, '123');

  T1 := Logger.MakeTable;

  T1.TargetRec := LR;

  T1.Columns.InitColsByWidth([13, 30, 45]);
  T1.WtHrzLine;

  T11 := Logger.MakeTable;
  T11.TargetRec := T1.Columns[0].NewRecord;
  T11.Columns.InitColsByWidth([5, 3, 4]);
  T11.WtHrzLine;
  T11.WtRow('a|b|c');
  T11.WtHrzLine;

  T12 := Logger.MakeTable;
  T12.TargetRec := T1.Columns[1].NewRecord;
  T12.Columns.InitColsByWidth([4, 7, 3]);
  T12.WtHrzLine;
  T12.WtRow('z|x|y');
  T12.WtHrzLine;

  T13 := Logger.MakeTable;
  T13.TargetRec := T1.Columns[2].NewRecord;
  T13.Columns.InitColsByWidth([8, 6, 2]);
  T13.WtHrzLine;
  T13.WtRow('jb|kn|lm');
  T13.WtHrzLine;

  T1.WtColumnsText;

  T1.WtHrzLine;
end;

procedure TformLoggerTest.btn2Click(Sender: TObject);
var
  R : ISANLogRecord;
begin
  R := Logger['L2'].NewRecord();
  R.Close;
  R := Logger['L2'].NewRecord();

  R.Buffer := '123'#13#10;
  R.Flush;
  R.Buffer := 'ghsklgjekgn''o543qtjqKRNGKJ''W'#13#10;
  R.Flush;
  R.Line('=');
  R.Flush;
end;

procedure TformLoggerTest.btnClick(Sender: TObject);
var
  Log, L2 : ISANLog;
begin
  Log := Logger.Add('Default', 'Log.txt');

  L2 := Logger.Add('L2', 'Log.txt');

  Log.Header(LRK_INFO, 'asd');
  Log.Writeln('123');

  Log.Writeln('паыв');

  L2.Header(LRK_FATAL, 'JKL', NOw - 1);
  L2.Writeln('gfe');

  Logger.Add('formatted', '|yyyy.mm.dd|_|zz.nn.ss|_Log.txt').Write('qwertyuiop');
end;

procedure TformLoggerTest.btnTableClick(Sender: TObject);
var
  t : ISANLogTable;
begin
  t := Logger.MakeTable;

  t.TargetRec := Logger['L2'].NewRecord();
  t.TargetRec.AutoFlush := False;

  t.Columns.InitColsByWidth([10, 20, 6]);

  t.WtHrzLine(False);
  t.WtHeader(taCenter, 'Таблица с заголовком'#13#10'из двух строк');
  t.WtHrzLine(True);

  t.WtRow(['СЛОВО', 'число', '64']);
  t.WtRow(['СЛОВО', 'число', '64']);
  t.WtRow(['СЛОВО', 'число', '64']);
  t.WtHrzLine(True);
  t.WtRow('СЛОВО|число|0123456789');
  t.WtRow('СЛОВО|число0123456789');
  t.WtRow(['СЛОВО', 'число', '64']);
  t.WtHrzLine(True);

  t.TargetRec.Flush;
end;

procedure TformLoggerTest.btnTreeClick(Sender: TObject);
var
  Log : ISANLog;
  Tree : ISANLogTree;
begin

  if not Logger.TryFindByName('Tree', Log) then
    Log := Logger.Add('Tree', 'Log.txt');

  Tree := Logger.MakeTree;

  Tree.TargetRec := Log.NewRecord;

  with Tree.NewNode(True) do
  try
    Write('1234');
    Writeln('5678');

    with Tree.NewNode(False) do
    try

      Write('qwer');
      Writeln('tyui');

    finally
      Tree.EndNode;
    end;

    with Tree.NewNode(False) do
    try

      Write('jgd');
      Writeln(';khfg');

      with Tree.NewNode(False) do
      try

        Writeln('^&*(*)');

      finally
        Tree.EndNode;
      end;

    finally
      Tree.EndNode;
    end;

  finally
    Tree.EndNode;
  end;

  with Tree.NewNode(False, 0) do
  try
    Writeln('<End');
    Writeln('>');
  finally
    Tree.EndNode;
  end;

//  Tree.SimpleNode('<End'#13#10, False, 0);
end;

procedure TformLoggerTest.btnTbTrTbClick(Sender: TObject);
var
  Log : ISANLog;
  Tb1, Tb2 : ISANLogTable;
  Tree : ISANLogTree;
begin

  if not Logger.TryFindByName('TbTrTb', Log) then
    Log := Logger.Add('TbTrTb', 'Log.txt');

  Tb1  := Logger.MakeTable;
  Tree := Logger.MakeTree;
  Tb2  := Logger.MakeTable;

  Tb1.TargetRec := Log.NewRecord;

  Tb1.TargetRec.Header(LRK_WARNING, 'Table->Tree->Table');

  Tb1.Columns.InitColsByWidth([50, 30, 10]);
  Tb2.Columns.InitColsByWidth([20, 6, 4, 2]);


  Tb1.WtHrzLine;
  Tree.TargetRec := Tb1.Columns[0].NewRecord;

  with Tree.NewNode(True) do
  try

    Write('Level1');

    with Tree.NewNode(True) do
    try

      Write('Level2');

      with Tree.NewNode(False) do
      try

        Write('Level3');

      finally
        Tree.EndNode;
      end;

    finally
      Tree.EndNode;
    end;

    Tb2.TargetRec := Tree.NewNode(False, 1);
    try

      Tb2.TargetRec.Writeln('Table:');
      Tb2.WtHrzLine;
      Tb2.WtColsNumbers(taCenter);
      Tb2.WtHrzLine;
      Tb2.WtRow(['ASD', 'FGH', 'THR']);
      Tb2.WtRow(['gffj', 'fsdh', '134']);
      Tb2.WtHrzLine;

    finally
      Tree.EndNode;
    end;

  finally
    Tree.EndNode;
  end;

  with Tree.NewNode(False, 0) do
  try

    Write('Gihi');

  finally
    Tree.EndNode;
  end;

  Tree.TargetRec.Flush;

  Tb1.WtHrzLine;

  Tb1.TargetRec.Flush;
end;


end.
