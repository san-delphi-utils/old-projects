program LoggerApp;

uses
  Forms,
  UformLoggerAppMain in 'UformLoggerAppMain.pas' {formLoggerTest};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TformLoggerTest, formLoggerTest);
  Application.Run;
end.
