object formLoggerTest: TformLoggerTest
  Left = 0
  Top = 0
  Caption = 'formLoggerTest'
  ClientHeight = 243
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btn: TButton
    Left = 24
    Top = 24
    Width = 75
    Height = 25
    Caption = 'btn'
    TabOrder = 0
    OnClick = btnClick
  end
  object btn2: TButton
    Left = 120
    Top = 24
    Width = 57
    Height = 25
    Caption = 'btn2'
    TabOrder = 1
    OnClick = btn2Click
  end
  object btnTable: TButton
    Left = 120
    Top = 80
    Width = 75
    Height = 25
    Caption = 'btnTable'
    TabOrder = 2
    OnClick = btnTableClick
  end
  object btn1: TButton
    Left = 272
    Top = 24
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 3
    OnClick = btn1Click
  end
  object btnTree: TButton
    Left = 384
    Top = 16
    Width = 75
    Height = 25
    Caption = 'btnTree'
    TabOrder = 4
    OnClick = btnTreeClick
  end
  object btnTbTrTb: TButton
    Left = 24
    Top = 144
    Width = 75
    Height = 25
    Caption = 'btnTbTrTb'
    TabOrder = 5
    OnClick = btnTbTrTbClick
  end
end
