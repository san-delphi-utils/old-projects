{*******************************************************}
{                                                       }
{       Core                                            }
{                                                       }
{       Имплементации интерфейсов абстрактного ядра.    }
{                                                       }
{       Перефикс: SANCore                               }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SANCoreImpl;

interface

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  SysUtils, Classes,
  {$ENDREGION}

  {$REGION 'SANCore'}
  SANCoreIntfs;
  {$ENDREGION}
{$ENDREGION}

type
  {$REGION 'Exceptions'}
  /// <summary>
  ///   Ошибки абстрактного ядра.
  /// </summary>
  ESANCoreError = class(Exception);

  /// <summary>
  ///   Ошибки нотификации.
  /// </summary>
  ESANCoreNotifiacationError = class(ESANCoreError);
  {$ENDREGION}
//==============================================================================
  {$REGION 'Contained&Aggregated'}
  /// <summary>
  ///   Внутренний объект в виде интерфейсной ссылки.
  /// </summary>
  TSANCoreInterfacedContained = class(TInterfacedObject, IInterface,
    ISANCoreOwnerProvider, ISANCoreOwnerProviderSafe)
  private
    FController: Pointer;  // weak reference to controller

    function GetController: IInterface;

  protected
    {$REGION 'IInterface'}
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    {$ENDREGION}

    {$REGION 'ISANCoreOwnerProvider'}
    function ISANCoreOwnerProvider.GetOwner = SCOPGetOwner;
    function SCOPGetOwner : IInterface;
    {$ENDREGION}

    {$REGION 'ISANCoreOwnerProviderSafe'}
    function ISANCoreOwnerProviderSafe.GetOwner = SCOPSGetOwner;
    function SCOPSGetOwner : IInterface; safecall;
    {$ENDREGION}

  public
    constructor Create(const AController: IInterface);

    procedure OutController(out AController);

    property Controller: IInterface read GetController;

  end;

  /// <summary>
  ///   Объект для агригации в виде интерфейсной ссылки.
  /// </summary>
  TSANCoreInterfacedAggregated = class(TSANCoreInterfacedContained, IInterface)
  protected
    {$REGION 'IInterface'}
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    {$ENDREGION}
  end;
  {$ENDREGION Contained&Aggregated}
//==============================================================================
  {$REGION 'Persistent'}
  /// <summary>
  ///   Поддерживает учет ссылок и перситентность.
  ///  /// </summary>
  TSANCoreInterfacedRefPersistent = class(TInterfacedObject,
    ISANCorePersistent, ISANCorePersistentSafe)
  protected
    // ISANCorePersistent
    function  ISANCorePersistent.IsEquals  = SCPIsEquals;
    procedure ISANCorePersistent.Assign    = SCPAssign;
    procedure ISANCorePersistent.Clone     = SCPClone;
    function  SCPIsEquals(const AOther : ISANCorePersistent) : WordBool;
    procedure SCPAssign(const ASource : ISANCorePersistent);
    procedure SCPClone(out AClone);

    // ISANCorePersistentSafe
    function  ISANCorePersistentSafe.IsEquals  = SCPSIsEquals;
    procedure ISANCorePersistentSafe.Assign    = SCPSAssign;
    procedure ISANCorePersistentSafe.Clone     = SCPSClone;
    function  SCPSIsEquals(const AOther : ISANCorePersistentSafe) : WordBool; safecall;
    procedure SCPSAssign(const ASource : ISANCorePersistentSafe); safecall;
    procedure SCPSClone(out AClone); safecall;

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); virtual;
    procedure DoAssign(const ASource : IInterface); virtual;
    procedure DoClone(out AClone); virtual;
  end;

  /// <summary>
  ///   <para>
  ///      Внутренний объект в виде интерфейсной ссылки.
  ///   </para>
  ///   <para>
  ///     Поддерживает перситентность.
  ///   </para>
  /// </summary>
  TSANCoreInterfacedContainedPersistent = class(TSANCoreInterfacedContained,
    ISANCorePersistent, ISANCorePersistentSafe)
  protected
    // ISANCorePersistent
    function  ISANCorePersistent.IsEquals  = SCPIsEquals;
    procedure ISANCorePersistent.Assign    = SCPAssign;
    procedure ISANCorePersistent.Clone     = SCPClone;
    function  SCPIsEquals(const AOther : ISANCorePersistent) : WordBool;
    procedure SCPAssign(const ASource : ISANCorePersistent);
    procedure SCPClone(out AClone);

    // ISANCorePersistentSafe
    function  ISANCorePersistentSafe.IsEquals  = SCPSIsEquals;
    procedure ISANCorePersistentSafe.Assign    = SCPSAssign;
    procedure ISANCorePersistentSafe.Clone     = SCPSClone;
    function  SCPSIsEquals(const AOther : ISANCorePersistentSafe) : WordBool; safecall;
    procedure SCPSAssign(const ASource : ISANCorePersistentSafe); safecall;
    procedure SCPSClone(out AClone); safecall;

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); virtual;
    procedure DoAssign(const ASource : IInterface); virtual;
    procedure DoClone(out AClone); virtual;
  end;
  {$ENDREGION Persistent}
//==============================================================================
  {$REGION 'Notification'}
  /// <summary>
  ///   Контекст простого события.
  /// </summary>
  TSANCoreSimpleNotificationContext = class(TInterfacedObject,
    ISANCoreNotificationContext, ISANCoreNotificationContextSafe)
  private
    FSender : IInterface;

  protected
    // ISANCoreNotificationContext
    function GetSender : IInterface;

    // ISANCoreNotificationContextSafe
    function ISANCoreNotificationContextSafe.GetSender = GetSenderSafe;
    function GetSenderSafe : IInterface; safecall;

  public
    constructor Create(const ASender : IInterface);
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property Sender : IInterface  read GetSender;
  end;
  {$ENDREGION Notification}

implementation

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  ActiveX, ComObj;
  {$ENDREGION}
{$ENDREGION}

//==============================================================================
{$REGION 'TSANCoreInterfacedContained'}
constructor TSANCoreInterfacedContained.Create(const AController: IInterface);
begin
  inherited Create;

  FController := Pointer(AController);
end;

function TSANCoreInterfacedContained.GetController: IInterface;
begin
  OutController(Result);
end;

procedure TSANCoreInterfacedContained.OutController(out AController);
begin
  IInterface(AController) := IInterface(FController);
end;

function TSANCoreInterfacedContained.SCOPGetOwner: IInterface;
begin
  OutController(Result);
end;

function TSANCoreInterfacedContained.SCOPSGetOwner: IInterface;
begin
  OutController(Result);
end;

function TSANCoreInterfacedContained._AddRef: Integer;
begin

  if (RefCount > 0) and Assigned(FController) then
    Controller._AddRef;

  Result := inherited;
end;

function TSANCoreInterfacedContained._Release: Integer;
begin

  if (RefCount > 1) and Assigned(FController) then
    Controller._Release;

  Result := inherited;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANCoreInterfacedAggregated'}
function TSANCoreInterfacedAggregated.QueryInterface(const IID: TGUID;
  out Obj): HResult;
begin
  Result := Controller.QueryInterface(IID, Obj);
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANCoreInterfacedRefPersistent'}
procedure TSANCoreInterfacedRefPersistent.DoAssign(const ASource: IInterface);
begin
  raise ESANCoreError.CreateFmt
  (
    'Can`t assign %s class from this interface',
    [
      Self.ClassName
    ]
  );
end;

procedure TSANCoreInterfacedRefPersistent.DoClone(out AClone);
begin
  IInterface(AClone) := nil;
end;

procedure TSANCoreInterfacedRefPersistent.DoIsEquals(const AOther: IInterface;
  out AIsEquals: WordBool);
begin
  AIsEquals := False;
end;

procedure TSANCoreInterfacedRefPersistent.SCPAssign(
  const ASource: ISANCorePersistent);
begin
  DoAssign(ASource);
end;

procedure TSANCoreInterfacedRefPersistent.SCPClone(out AClone);
begin
  DoClone(AClone);
end;

function TSANCoreInterfacedRefPersistent.SCPIsEquals(
  const AOther: ISANCorePersistent): WordBool;
begin
  DoIsEquals(AOther, Result);
end;

procedure TSANCoreInterfacedRefPersistent.SCPSAssign(
  const ASource: ISANCorePersistentSafe);
begin
  DoAssign(ASource);
end;

procedure TSANCoreInterfacedRefPersistent.SCPSClone(out AClone);
begin
  DoClone(AClone);
end;

function TSANCoreInterfacedRefPersistent.SCPSIsEquals(
  const AOther: ISANCorePersistentSafe): WordBool;
begin
  DoIsEquals(AOther, Result);
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANCoreInterfacedContainedPersistent'}
procedure TSANCoreInterfacedContainedPersistent.DoAssign(const ASource: IInterface);
begin
  raise ESANCoreError.CreateFmt
  (
    'Can`t assign %s class from this interface',
    [
      Self.ClassName
    ]
  );
end;

procedure TSANCoreInterfacedContainedPersistent.DoClone(out AClone);
begin
  IInterface(AClone) := nil;
end;

procedure TSANCoreInterfacedContainedPersistent.DoIsEquals(
  const AOther: IInterface; out AIsEquals: WordBool);
begin
  AIsEquals := False;
end;

procedure TSANCoreInterfacedContainedPersistent.SCPAssign(
  const ASource: ISANCorePersistent);
begin
  DoAssign(ASource);
end;

procedure TSANCoreInterfacedContainedPersistent.SCPClone(out AClone);
begin
  DoClone(AClone);
end;

function TSANCoreInterfacedContainedPersistent.SCPIsEquals(
  const AOther: ISANCorePersistent): WordBool;
begin
  DoIsEquals(AOther, Result);
end;

procedure TSANCoreInterfacedContainedPersistent.SCPSAssign(
  const ASource: ISANCorePersistentSafe);
begin
  DoAssign(ASource);
end;

procedure TSANCoreInterfacedContainedPersistent.SCPSClone(out AClone);
begin
  DoClone(AClone);
end;

function TSANCoreInterfacedContainedPersistent.SCPSIsEquals(
  const AOther: ISANCorePersistentSafe): WordBool;
begin
  DoIsEquals(AOther, Result);
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANCoreSimpleNotificationContext'}
constructor TSANCoreSimpleNotificationContext.Create(const ASender: IInterface);
begin
  inherited Create;

  FSender := ASender;
end;

destructor TSANCoreSimpleNotificationContext.Destroy;
begin

  inherited;
end;

function TSANCoreSimpleNotificationContext.GetSender: IInterface;
begin
  Result := FSender;
end;

function TSANCoreSimpleNotificationContext.GetSenderSafe: IInterface;
begin
  Result := FSender;
end;

function TSANCoreSimpleNotificationContext.SafeCallException(
  ExceptObject: TObject; ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANCoreNotificationContextSafe,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
{$ENDREGION}
//==============================================================================

end.
