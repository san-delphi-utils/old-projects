{*******************************************************}
{                                                       }
{       Logger                                          }
{                                                       }
{       Реализация контекстов событий логгера.          }
{                                                       }
{       Перефикс: SANLog                                }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SANLoggerContexts;

interface

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  SysUtils, Classes,
  {$ENDREGION}

  {$REGION 'SANCore'}
  SANCoreIntfs, SANCoreImpl,
  {$ENDREGION}

  {$REGION 'SANLogger'}
  SANLoggerIntfs;
  {$ENDREGION}
{$ENDREGION}

type
  /// <summary>
  ///   Контекст события фильтрации набора записей в логе.
  /// </summary>
  TSANLogRecordsFilteredContext = class(TSANCoreSimpleNotificationContext,
    ISANCoreNotificationContextSafe, ISANLogRecordsFilteredContext)
  private
    FFiltered : ISANLogRecords;

  protected
    {$REGION 'ISANLogRecordsFilteredContext'}
    function GetFiltered : ISANLogRecords; safecall;
    {$ENDREGION}

  public
    constructor Create(const ASender : IInterface; const AFiltered : ISANLogRecords);
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  ///   Контекст событий одной записи набора записей в логе.
  /// </summary>
  TSANLogRecordsOneRecordContext = class(TSANCoreSimpleNotificationContext,
    ISANCoreNotificationContextSafe, ISANLogRecordsOneRecordContext)
  private
    FLogRecord : ISANLogRecord;

  protected
    {$REGION 'ISANLogRecordsOneRecordContext'}
    function GetLogRecord : ISANLogRecord; safecall;
    {$ENDREGION}

  public
    constructor Create(const ASender : IInterface; const ALogRecord : ISANLogRecord);
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  ///   Контекст событий одного лога в хранилище.
  /// </summary>
  TSANLogsStorageOneLogContext = class(TSANCoreSimpleNotificationContext,
    ISANCoreNotificationContextSafe, ISANLogsStorageOneLogContext)
  private
    FLog : ISANLog;

  protected
    {$REGION 'ISANLogsStorageOneLogContext'}
    function GetLog : ISANLog; safecall;
    {$ENDREGION}

  public
    constructor Create(const ASender : IInterface; const ALog : ISANLog);
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  ///   Контекст событий одного лога в хранилище.
  /// </summary>
  TSANLogsStoragesOneStorageContext = class(TSANCoreSimpleNotificationContext,
    ISANCoreNotificationContextSafe, ISANLogsStoragesOneStorageContext)
  private
    FStorage : ISANLogsStorage;

  protected
    {$REGION 'ISANLogsStoragesOneStorageContext'}
    function GetStorage : ISANLogsStorage; safecall;
    {$ENDREGION}

  public
    constructor Create(const ASender : IInterface; const AStorage : ISANLogsStorage);
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  ActiveX, ComObj;
  {$ENDREGION}
{$ENDREGION}

//==============================================================================
{$REGION 'TSANLogRecordsFilteredContext'}
constructor TSANLogRecordsFilteredContext.Create(const ASender : IInterface;
  const AFiltered : ISANLogRecords);
begin
  inherited Create(ASender);

  FFiltered := AFiltered;
end;

function TSANLogRecordsFilteredContext.GetFiltered : ISANLogRecords;
begin
  Result := FFiltered;
end;

function TSANLogRecordsFilteredContext.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogRecordsFilteredContext,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogRecordsOneRecordContext'}
constructor TSANLogRecordsOneRecordContext.Create(const ASender: IInterface;
  const ALogRecord: ISANLogRecord);
begin
  inherited Create(ASender);

  FLogRecord := ALogRecord;
end;

function TSANLogRecordsOneRecordContext.GetLogRecord: ISANLogRecord;
begin
  Result := FLogRecord;
end;

function TSANLogRecordsOneRecordContext.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogRecordsOneRecordContext,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogsStorageOneLogContext'}
constructor TSANLogsStorageOneLogContext.Create(const ASender: IInterface;
  const ALog: ISANLog);
begin
  inherited Create(ASender);

  FLog := ALog;
end;

function TSANLogsStorageOneLogContext.GetLog: ISANLog;
begin
  Result := FLog;
end;

function TSANLogsStorageOneLogContext.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogsStorageOneLogContext,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogsStoragesOneStorageContext'}
constructor TSANLogsStoragesOneStorageContext.Create(const ASender: IInterface;
  const AStorage: ISANLogsStorage);
begin
  inherited Create(ASender);
  FStorage := AStorage;
end;

function TSANLogsStoragesOneStorageContext.GetStorage: ISANLogsStorage;
begin
  Result := FStorage;
end;

function TSANLogsStoragesOneStorageContext.SafeCallException(
  ExceptObject: TObject; ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogsStoragesOneStorageContext,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
{$ENDREGION}
//==============================================================================


end.
