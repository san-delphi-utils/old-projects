{*******************************************************}
{                                                       }
{       Core                                            }
{                                                       }
{       Интерфейсы абстрактного ядра.                   }
{                                                       }
{       Перефикс: SANCore                               }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SANCoreIntfs;

interface

type
//==============================================================================
{$REGION 'ISANCorePersistent'}
  /// <summary>
  ///   Поддерживающий сравнение, передачу свойств и клонирование.
  /// </summary>
  ISANCorePersistent = interface(IInterface)
  ['{71C84C12-BA95-4EF0-874B-E4FF5E38CA62}']

    /// <summary>
    ///   Сравнение свойств с другим интерфейсом.
    /// </summary>
    /// <param name="AOther">
    ///   Другой интерфейс для сравнения.
    /// </param>
    /// <returns>
    ///   True, если объекты имеют одинаковые свойства.
    /// </returns>
    function IsEquals(const AOther : ISANCorePersistent) : WordBool;

    /// <summary>
    ///   Скопировать свойства из ASource.
    /// </summary>
    /// <param name="ASource">
    ///   ASource источник для копирования свойств.
    /// </param>
    procedure Assign(const ASource : ISANCorePersistent);

    /// <summary>
    ///   Создать копию с теми же значениями свойств.
    /// </summary>
    /// <param name="AClone">
    ///   Копия экземпляра.
    /// </param>
    procedure Clone(out AClone);
  end;

  /// <summary>
  ///   Safecall. Поддерживающий сравнение, передачу свойств и клонирование.
  /// </summary>
  ISANCorePersistentSafe = interface(IInterface)
  ['{481ED422-A362-4E32-8ECF-0E5803C5CC37}']

    /// <summary>
    ///   Сравнение свойств с другим интерфейсом.
    /// </summary>
    /// <param name="AOther">
    ///   Другой интерфейс для сравнения.
    /// </param>
    /// <returns>
    ///   True, если объекты имеют одинаковые свойства.
    /// </returns>
    function IsEquals(const AOther : ISANCorePersistentSafe) : WordBool; safecall;

    /// <summary>
    ///   Скопировать свойства из ASource.
    /// </summary>
    /// <param name="ASource">
    ///   ASource источник для копирования свойств.
    /// </param>
    procedure Assign(const ASource : ISANCorePersistentSafe); safecall;

    /// <summary>
    ///   Создать копию с теми же значениями свойств.
    /// </summary>
    /// <param name="AClone">
    ///   Копия экземпляра.
    /// </param>
    procedure Clone(out AClone); safecall;
  end;

  /// <summary>
  ///   Сравнение свойств двух интерфейсов.
  /// </summary>
  function IsEquals(const AFirst, ASecond : IInterface) : Boolean;
{$ENDREGION}
//==============================================================================
{$REGION 'ISANCoreOwnerProvider'}
type
  /// <summary>
  ///   Обладатель свойства "Владелец"
  /// </summary>
  ISANCoreOwnerProvider = interface(IInterface)
  ['{754A240D-5421-42F3-BA63-CB142FFBEE32}']
    function GetOwner : IInterface;

    /// <summary>
    ///   Владелец. Weak link.
    /// </summary>
    property Owner : IInterface  read GetOwner;
  end;

  /// <summary>
  ///   Safecall.  Обладатель свойства "Владелец"
  /// </summary>
  ISANCoreOwnerProviderSafe = interface(IInterface)
  ['{733BEFFF-B69B-4F40-8846-D99ED5DBFA79}']
    function GetOwner : IInterface; safecall;

    /// <summary>
    ///   Владелец. Weak link.
    /// </summary>
    property Owner : IInterface  read GetOwner;
  end;
{$ENDREGION}
//==============================================================================
{$REGION 'ISANCoreModuleHandleProvider'}
  /// <summary>
  ///   Обладатель свойства "Описатель модуля"
  /// </summary>
  ISANCoreModuleHandleProvider = interface(IInterface)
  ['{312354D4-D093-409B-8DB3-95155766DE64}']
    function GetModuleHandle : HMODULE;

    /// <summary>
    ///   Описатель модуля.
    /// </summary>
    property ModuleHandle : HMODULE  read GetModuleHandle;
  end;

  /// <summary>
  ///   Safecall.  Обладатель свойства "ModuleHandle"
  /// </summary>
  ISANCoreModuleHandleProviderSafe = interface(IInterface)
  ['{0A4F7306-BA1B-44E5-AD95-5552FBF79ACC}']
    function GetModuleHandle : HMODULE; safecall;

    /// <summary>
    ///   Описатель модуля.
    /// </summary>
    property ModuleHandle : HMODULE  read GetModuleHandle;
  end;
{$ENDREGION}
//==============================================================================
{$REGION 'ISANCoreInt64IDProvider'}
  /// <summary>
  ///   Обладатель свойства "Целочисленный идентификатор"
  /// </summary>
  ISANCoreInt64IDProvider = interface(IInterface)
  ['{209E00E5-744B-4901-B455-BA8C833810E5}']
    function GetID : Int64;

    /// <summary>
    ///   Целочисленный идентификатор.
    /// </summary>
    property ID : Int64  read GetID;
  end;

  /// <summary>
  ///   Safecall. Обладатель свойства "Целочисленный идентификатор"
  /// </summary>
  ISANCoreInt64IDProviderSafe = interface(IInterface)
  ['{EC9D9157-251D-49B3-A1A9-76864D9E1DB9}']
    function GetID : Int64; safecall;

    /// <summary>
    ///   Целочисленный идентификатор.
    /// </summary>
    property ID : Int64  read GetID;
  end;

  /// <summary>
  ///   Список обладателей свойства "Целочисленный идентификатор"
  /// </summary>
  ISANCoreInt64IDProvidersList = interface(IInterface)
  ['{88A444BF-C1CF-41FB-8C99-E9C0749F3BEE}']

    /// <summary>
    ///   Поиск элемента списка по значению свойства "Целочисленный идентификатор".
    /// </summary>
    /// <param name="AID">
    ///   Целочисленный идентификатор.
    /// </param>
    /// <param name="AIDProvider">
    ///   Интерфейс найденного элемента, в случае успеха.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, в случае успеха.
    /// </returns>
    function TryFind(const AID : Int64; out AIDProvider;
      const AIndexPtr : PInteger = nil) : WordBool;

  end;

  /// <summary>
  ///   Safecall. Список обладателей свойства "Целочисленный идентификатор"
  /// </summary>
  ISANCoreInt64IDProvidersListSafe = interface(IInterface)
  ['{19525DDB-75A2-489E-99BB-21A02F7657CC}']

    /// <summary>
    ///   Поиск элемента списка по значению свойства "Целочисленный идентификатор".
    /// </summary>
    /// <param name="AID">
    ///   Целочисленный идентификатор.
    /// </param>
    /// <param name="AIDProvider">
    ///   Интерфейс найденного элемента, в случае успеха.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, в случае успеха.
    /// </returns>
    function TryFind(const AID : Int64; out AIDProvider;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;
  end;
{$ENDREGION}
//==============================================================================
{$REGION 'ISANCoreWideNameProvider'}
  /// <summary>
  ///   Обладатель свойства "Имя"
  /// </summary>
  ISANCoreWideNameProvider = interface(IInterface)
  ['{DFD337EB-39C8-4EFD-9778-CECE4EC5C6AA}']
    function GetName : Int64;

    /// <summary>
    ///   Целочисленный идентификатор.
    /// </summary>
    property Name : Int64  read GetName;
  end;

  /// <summary>
  ///   Safecall. Обладатель свойства "Имя"
  /// </summary>
  ISANCoreWideNameProviderSafe = interface(IInterface)
  ['{F6456A9E-BDB6-47FA-963D-C4C416503DAA}']
    function GetName : Int64; safecall;

    /// <summary>
    ///   Целочисленный идентификатор.
    /// </summary>
    property Name : Int64  read GetName;
  end;

  /// <summary>
  ///   Список обладателей свойства "Имя"
  /// </summary>
  ISANCoreWideNameProvidersList = interface(IInterface)
  ['{01C08242-7101-48E2-A1E9-F4ECAF51A1A7}']

    /// <summary>
    ///   Поиск элемента списка по значению свойства "Имя".
    /// </summary>
    /// <param name="AName">
    ///   Имя.
    /// </param>
    /// <param name="AWideNameProvider">
    ///   Интерфейс найденного элемента, в случае успеха.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, в случае успеха.
    /// </returns>
    function TryFind(const AName : Int64; out AWideNameProvider;
      const AIndexPtr : PInteger = nil) : WordBool;

  end;

  /// <summary>
  ///   Safecall. Список обладателей свойства "Имя"
  /// </summary>
  ISANCoreWideNameProvidersListSafe = interface(IInterface)
  ['{D2897C73-5E81-4D02-9A87-6295333EF43A}']

    /// <summary>
    ///   Поиск элемента списка по значению свойства "Имя".
    /// </summary>
    /// <param name="AName">
    ///   Имя.
    /// </param>
    /// <param name="AWideNameProvider">
    ///   Интерфейс найденного элемента, в случае успеха.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, в случае успеха.
    /// </returns>
    function TryFind(const AName : Int64; out AWideNameProvider;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;
  end;
{$ENDREGION}
//==============================================================================
{$REGION 'Notification'}
  /// <summary>
  ///   Описатель перехватчика событий.
  /// </summary>
  TSANCoreNotifierHandle = PNativeInt;

  /// <summary>
  ///   Контекст события.
  /// </summary>
  ISANCoreNotificationContext = interface(IInterface)
    ['{5CFB33D4-40BE-4226-892F-DFCBE7F7E480}']
    function GetSender : IInterface;

    /// <summary>
    ///   Интерфейс объекта, в котором произошло событие.
    /// </summary>
    property Sender : IInterface  read GetSender;
  end;

  /// <summary>
  ///   Safecall. Контекст события.
  /// </summary>
  ISANCoreNotificationContextSafe = interface(IInterface)
    ['{B8BED765-B798-41FE-B17C-36F37FC9FE09}']
    function GetSender : IInterface; safecall;

    /// <summary>
    ///   Интерфейс объекта, в котором произошло событие.
    /// </summary>
    property Sender : IInterface  read GetSender;
  end;

  /// <summary>
  ///   Перехватчик события.
  /// </summary>
  ISANCoreNotifier = interface(IInterface)
    ['{BE5B25CB-E442-4FEC-B788-32B4805073EC}']
    /// <summary>
    ///   Разрущение Sender-а. Необходимо снятие нотификации.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Destroying(const AContext : ISANCoreNotificationContext);
  end;

  /// <summary>
  ///   Safecall. Перехватчик события.
  /// </summary>
  ISANCoreNotifierSafe = interface(IInterface)
    ['{A2654C49-EB9D-436A-AEBF-A1E108EF2B34}']

    /// <summary>
    ///   Разрущение Sender-а. Необходимо снятие нотификации.
    /// </summary>
    procedure Destroying(const AContext : ISANCoreNotificationContextSafe); safecall;
  end;
{$ENDREGION}
//==============================================================================
{$REGION 'Termination'}
  /// <summary>
  ///   Проверка завершения извне.
  /// </summary>
  ITermination = interface(IInterface)
  ['{3D3351D4-8851-404D-A782-DB4F1E1D3079}']
    function GetOuterTerminated : WordBool;

    /// <summary>
    ///   Признак завершения извне.
    /// </summary>
    property OuterTerminated : WordBool  read GetOuterTerminated;
  end;

  /// <summary>
  ///   Safe. Проверка завершения извне.
  /// </summary>
  ITerminationSafe = interface(IInterface)
  ['{26845EE1-5CB6-47DE-AFA9-82E10687EE6B}']
    function GetOuterTerminated : WordBool; safecall;

    /// <summary>
    ///   Признак завершения извне.
    /// </summary>
    property OuterTerminated : WordBool  read GetOuterTerminated;
  end;
{$ENDREGION}
//==============================================================================

implementation

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  SysUtils;
  {$ENDREGION}
{$ENDREGION}

//==============================================================================
function IsEquals(const AFirst, ASecond : IInterface) : Boolean;

  function IsEqualsAsISANCorePersistent(out AIsEquals : boolean) : boolean;
  var
    P1, P2 : ISANCorePersistent;
  begin
    Result := Supports(AFirst, ISANCorePersistent, P1)
          and Supports(ASecond, ISANCorePersistent, P2);

    if Result then
      AIsEquals := P1.IsEquals(P2);
  end;

  function IsEqualsAsISANCorePersistentSafe(out AIsEquals : boolean) : boolean;
  var
    P1, P2 : ISANCorePersistentSafe;
  begin
    Result := Supports(AFirst, ISANCorePersistentSafe, P1)
          and Supports(ASecond, ISANCorePersistentSafe, P2);

    if Result then
      AIsEquals := P1.IsEquals(P2);
  end;

begin

  case Ord(Assigned(AFirst)) + Ord(Assigned(ASecond)) of
    0:
      Result := True;

    1:
      Result := False;

  else

    if IsEqualsAsISANCorePersistent(Result) then
      Exit;

    if IsEqualsAsISANCorePersistentSafe(Result) then
      Exit;

    Result := False;
  end;

end;
//==============================================================================


end.
