{*******************************************************}
{                                                       }
{       Logger                                          }
{                                                       }
{       Интерфейсы логгера.                             }
{                                                       }
{       Перефикс: SANLog                                }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SANLoggerIntfs;

interface

{$REGION 'uses'}
uses
  {$REGION 'SANCore'}
  SANCoreIntfs;
  {$ENDREGION}
{$ENDREGION}

type
  {$REGION 'BaseTypes'}
  /// <summary>
  ///   Базовый интерфейс для всех остальных.
  /// </summary>
  ISANLogBaseIntf       = IInterface;

  /// <summary>
  ///   Тип данных идентификатора записи лога.
  /// </summary>
  TSANLogRecordID       = Int64;

  /// <summary>
  ///   Тип данных типа лога.
  /// </summary>
  TSANLogRecordKind     = Longword;

  /// <summary>
  ///   Тип данных текстового подтипа лога.
  /// </summary>
  TSANLogRecordSubType  = WideString;

  /// <summary>
  ///   Тип данных текста лога.
  /// </summary>
  TSANLogRecordText     = WideString;

  /// <summary>
  ///   Тип данных имен.
  /// </summary>
  TSANLogName           = WideString;
  {$ENDREGION}

  {$REGION 'Forwards'}
  ISANLogRecords   = interface;
  ISANLogsStorage  = interface;
  {$ENDREGION}

  {$REGION 'Records'}
  /// <summary>
  ///   Одна запись лога.
  /// </summary>
  ISANLogRecord = interface(ISANLogBaseIntf)
  ['{EF259211-3F70-4BB3-962D-70F78DC215EE}']
    function GetID : TSANLogRecordID; safecall;
    procedure SetID(const AID : TSANLogRecordID); safecall;
    function GetDateTime : TDateTime; safecall;
    procedure SetDateTime(const ADateTime : TDateTime); safecall;
    function GetKind : TSANLogRecordKind; safecall;
    procedure SetKind(const AKind : TSANLogRecordKind); safecall;
    function GetSubType : TSANLogRecordSubType; safecall;
    procedure SetSubType(const ASubType : TSANLogRecordSubType); safecall;
    function GetText : TSANLogRecordText; safecall;
    procedure SetText(const AText : TSANLogRecordText); safecall;

    /// <summary>
    ///   Метод очистки.
    /// </summary>
    procedure Clear; safecall;

    /// <summary>
    ///   Метод задающий набор свойств.
    /// </summary>
    /// <param name="AID">
    ///   Идентификатор записи.
    /// </param>
    /// <param name="ADateTime">
    ///   Дата и время записи.
    /// </param>
    /// <param name="AKind">
    ///   Тип сообщения.
    /// </param>
    /// <param name="ASubType">
    ///   Текстовый подтип сообщения.
    /// </param>
    /// <param name="AText">
    ///   Текст сообщения.
    /// </param>
    /// <returns>
    ///   Ссылка на Self.
    /// </returns>
    function From(const AID : TSANLogRecordID; const ADateTime : TDateTime;
      const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      const AText : TSANLogRecordText): ISANLogRecord; safecall;

    /// <summary>
    ///   Сравнение двух записей на идентичность.
    /// </summary>
    /// <param name="ALogRecord">
    ///   Запись, с которой идет сравнение.
    /// </param>
    /// <returns>
    ///   True, записи идентичны.
    /// </returns>
    function IsEquals(const ALogRecord : ISANLogRecord) : WordBool; safecall;

    /// <summary>
    ///   Скопировать данные из другой записи
    /// </summary>
    /// <param name="ASource">
    ///   Запись лога, источник данных для копирования
    /// </param>
    procedure Assign(const ASource : ISANLogRecord); safecall;

    /// <summary>
    ///   Создать копию записи с теми же значениями свойств.
    /// </summary>
    /// <returns>
    ///   Копия записи.
    /// </returns>
    function Clone : ISANLogRecord; safecall;

    /// <summary>
    ///   Идентификатор записи, зависит от типа хранения в логе.
    ///   Значение не может быть отрицательным.
    /// </summary>
    property ID : TSANLogRecordID  read GetID  write SetID;

    /// <summary>
    ///   Дата и время записи.
    /// </summary>
    property DateTime : TDateTime  read GetDateTime  write SetDateTime;

    /// <summary>
    ///   Тип сообщения.
    /// </summary>
    property Kind : TSANLogRecordKind  read GetKind  write SetKind;

    /// <summary>
    ///   Текстовый подтип сообщения.
    /// </summary>
    property SubType : TSANLogRecordSubType  read GetSubType  write SetSubType;

    /// <summary>
    ///   Текст сообщения.
    /// </summary>
    property Text : TSANLogRecordText  read GetText  write SetText;
  end;

  /// <summary>
  ///   Фильтр записей лога.
  /// </summary>
  ISANLogRecordsFilter = interface(ISANLogBaseIntf)
  ['{FDDFAF09-3A39-4058-9BD4-CE2A8C4658B7}']

    /// <summary>
    ///   Проверка одной записи.
    /// </summary>
    /// <returns>
    ///   True, запись должна быть отфильтрована.
    /// </returns>
    function Filtered(const ALogRecord : ISANLogRecord) : WordBool; safecall;
  end;

  /// <summary>
  ///   Контекст события фильтрации набора записей в логе.
  /// </summary>
  ISANLogRecordsFilteredContext = interface(ISANCoreNotificationContextSafe)
  ['{3A7DA0E2-7658-470D-A663-4818880D32E5}']
    function GetFiltered : ISANLogRecords; safecall;

    /// <summary>
    ///   Список записей отсеченных фильтром.
    /// </summary>
    property Filtered : ISANLogRecords  read GetFiltered;
  end;

  /// <summary>
  ///   Контекст событий одной записи набора записей в логе.
  /// </summary>
  ISANLogRecordsOneRecordContext = interface(ISANCoreNotificationContextSafe)
  ['{D90C9D41-EF8A-4A34-A660-482E4BCF2CE6}']
    function GetLogRecord : ISANLogRecord; safecall;

     /// <summary>
     ///   Запись связанная с событием.
     /// </summary>
    property LogRecord : ISANLogRecord  read GetLogRecord;
  end;

  /// <summary>
  ///   Перехватчик событий набора записей в логе.
  /// </summary>
  ISANLogRecordsNotifier = interface(ISANCoreNotifierSafe)
  ['{9D6D99D8-EC1F-43AF-B50E-0A724C33F555}']

    /// <summary>
    ///   Набор записей отфильтрован.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Filtered(const AContext : ISANLogRecordsFilteredContext); safecall;

    /// <summary>
    ///   Набор записей очищен.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Cleared(const AContext : ISANCoreNotificationContextSafe); safecall;

    /// <summary>
    ///   Из набора извлечена запись.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Extracted(const AContext : ISANLogRecordsOneRecordContext); safecall;

    /// <summary>
    ///   Из набора удаляется запись.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Removing(const AContext : ISANLogRecordsOneRecordContext); safecall;

    /// <summary>
    ///   В набор добавлена запись.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Added(const AContext : ISANLogRecordsOneRecordContext); safecall;
  end;

  /// <summary>
  ///   Набор записей в логе.
  /// </summary>
  ISANLogRecords = interface(ISANLogBaseIntf)
  ['{447E019D-E8C8-4499-B709-CDB2C91BDFB4}']
    function GetCount : integer; safecall;
    function GetRecords(const AIndex : integer) : ISANLogRecord; safecall;

    /// <summary>
    ///   Заблокировать доступ других потоков.
    /// </summary>
    procedure Lock; safecall;

    /// <summary>
    ///   Разблокировать доступ других потоков.
    /// </summary>
    procedure UnLock; safecall;

    /// <summary>
    ///   Очистить.
    /// </summary>
    procedure Clear; safecall;

    /// <summary>
    ///   Попытка поиска записи по ID.
    /// </summary>
    /// <param name="AID">
    ///   ID записи для поиска.
    /// </param>
    /// <param name="ALogRecord">
    ///   Найденая запись, если поиск успешен.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, поиск успешен.
    /// </returns>
    function TryFindByID(const AID : TSANLogRecordID; out ALogRecord : ISANLogRecord;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;

    /// <summary>
    ///   Извлечь запись из набора по ID.
    /// </summary>
    /// <param name="AID">
    ///   ID извлекаемой записи.
    /// </param>
    /// <returns>
    ///   Извлеченная запись или nil в случае неудачи.
    /// </returns>
    function ExtractByID(const AID : TSANLogRecordID) : ISANLogRecord; safecall;

    /// <summary>
    ///   Удалить запись из набора по ID.
    /// </summary>
    /// <param name="AID">
    ///   ID удаляемой записи.
    /// </param>
    /// <returns>
    ///   True, удаление успешно.
    /// </returns>
    function RemoveByID(const AID : TSANLogRecordID) : WordBool; safecall;

    /// <summary>
    ///   Удалить запись из набора по индексу.
    /// </summary>
    /// <param name="AIndex">
    ///   Индекс удаляемой записи.
    /// </param>
    procedure Delete(const AIndex : integer); safecall;

    /// <summary>
    ///   Добавить запись в набор.
    /// </summary>
    /// <param name="ALogRecord">
    ///   Добавляемая запись.
    /// </param>
    /// <returns>
    ///   Ссылка на добавленную запись.
    /// </returns>
    function Add(const ALogRecord : ISANLogRecord) : ISANLogRecord; safecall;


    /// <summary>
    ///   Добавить запись в набор, как копию другой записи.
    /// </summary>
    /// <param name="ALogRecord">
    ///   Запись лога, по которой происходит добавление.
    /// </param>
    /// <returns>
    ///   Созданная запись.
    /// </returns>
    function AddClone(const ALogRecord : ISANLogRecord) : ISANLogRecord; safecall;

    /// <summary>
    ///   Добавить запись в набор, как копию другой записи.
    /// </summary>
    /// <param name="AID">
    ///   Идентификатор записи.
    /// </param>
    /// <param name="ADateTime">
    ///   Дата и время записи.
    /// </param>
    /// <param name="AKind">
    ///   Тип сообщения.
    /// </param>
    /// <param name="ASubType">
    ///   Текстовый подтип сообщения.
    /// </param>
    /// <param name="AText">
    ///   Текст сообщения.
    /// </param>
    /// <returns>
    ///   Созданая запись.
    /// </returns>
    function AddFrom(const AID : TSANLogRecordID; const ADateTime : TDateTime;
      const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      const AText : TSANLogRecordText) : ISANLogRecord; safecall;

    /// <summary>
    ///   Фильтрация
    /// </summary>
    /// <param name="AFilter">
    ///   <para>
    ///     Фильтр, производящий фильтрацию.
    ///   </para>
    ///   <para>
    ///     Передача интерфейса по значению может быть использована
    ///     для освобождения памяти, если интерфейс ни где
    ///     более не используется.
    ///   </para>
    /// </param>
    /// <returns>
    ///   Количество отфильтрованых записей.
    /// </returns>
    function Filter(AFilter : ISANLogRecordsFilter) : integer; safecall;

    /// <summary>
    ///   Экспорт записей в другой список с возможностью фильтрации.
    /// </summary>
    /// <param name="ARecords">
    ///   Список записей в который ведется экспорт.
    /// </param>
    /// <param name="AReWrite">
    ///   True - удалить старые записи.
    /// </param>
    /// <param name="AUseClone">
    ///   True - добавлять копии запеисей / False - добавлять ссылки на те же записи.
    /// </param>
    /// <param name="AFilter">
    ///   <para>
    ///     Фильтр.
    ///   </para>
    ///   <para>
    ///     Передача интерфейса по значению может быть использована
    ///     для освобождения памяти, если интерфейс ни где
    ///     более не используется.
    ///   </para>
    /// </param>
    procedure ExportRecords(const ARecords : ISANLogRecords;
      const AReWrite, AUseClone : WordBool;
      AFilter : ISANLogRecordsFilter = nil); safecall;

    /// <summary>
    ///   Импорт записей из другого списка с возможностью фильтрации.
    /// </summary>
    /// <param name="ARecords">
    ///   Безопасный массив в вариантной переменной.
    ///   После импорта переменная очищается.
    /// </param>
    /// <param name="AReWrite">
    ///   True - удалить старые записи.
    /// </param>
    /// <param name="AUseClone">
    ///   True - добавлять копии запеисей / False - добавлять ссылки на те же записи.
    /// </param>
    /// <param name="AFilter">
    ///   <para>
    ///     Фильтр.
    ///   </para>
    ///   <para>
    ///     Передача интерфейса по значению может быть использована
    ///     для освобождения памяти, если интерфейс ни где
    ///     более не используется.
    ///   </para>
    /// </param>
    procedure ImportRecords(const ARecords : ISANLogRecords;
      const AReWrite, AUseClone : WordBool; AFilter : ISANLogRecordsFilter = nil); safecall;

    /// <summary>
    ///   Добавить перехватчик событий.
    /// </summary>
    /// <param name="ANotifier">
    ///   Перехватчик событий.
    /// </param>
    /// <returns>
    ///   Описатель для передачи в RemoveNotifier.
    /// </returns>
    function AddNotifier(const ANotifier : ISANLogRecordsNotifier) : TSANCoreNotifierHandle; safecall;

    /// <summary>
    ///   Удалить перехватчик событий.
    /// </summary>
    /// <param name="ANotifierHandle">
    ///   Описатель перехватчика событий, полученный из AddNotifier.
    /// </param>
    /// <returns>
    ///   True, в случае успеха.
    /// </returns>
    function RemoveNotifier(const ANotifierHandle : TSANCoreNotifierHandle) : WordBool; safecall;

    /// <summary>
    ///   Сравнение свойств с другим списком.
    /// </summary>
    /// <param name="AOther">
    ///   Другой интерфейс для сравнения.
    /// </param>
    /// <returns>
    ///   True, если объекты имеют одинаковые свойства.
    /// </returns>
    function IsEquals(const AOther : ISANLogRecords) : WordBool; safecall;

    /// <summary>
    ///   Скопировать свойства из ASource.
    /// </summary>
    /// <param name="ASource">
    ///   ASource источник для копирования свойств.
    /// </param>
    procedure Assign(const ASource : ISANLogRecords); safecall;

    /// <summary>
    ///   Создать копию с теми же значениями свойств.
    /// </summary>
    /// <returns>
    ///   Копия экземпляра.
    /// </returns>
    function Clone : ISANLogRecords; safecall;

    /// <summary>
    ///   Количество записей.
    /// </summary>
    property Count : integer  read GetCount;

    /// <summary>
    ///   Запись по индексу.
    /// </summary>
    property Records[const AIndex : integer] : ISANLogRecord  read GetRecords; default;
  end;
  {$ENDREGION}

  {$REGION 'Logs'}
  /// <summary>
  ///   Упрощенный лог (только запись).
  /// </summary>
  ISANLogCustom  = interface(ISANLogBaseIntf)
  ['{F87D086B-9D4F-46C9-89EF-580286070F0B}']

    /// <summary>
    ///   Одна законченная запись.
    /// </summary>
    /// <param name="AKind">
    ///   Целочисленный тип записи.
    /// </param>
    /// <param name="ASubType">
    ///   Строка подтипа записи.
    /// </param>
    /// <param name="AText">
    ///   Строка текста записи.
    /// </param>
    /// <param name="ADateTime">
    ///   Дата и время записи. Если ADateTime = 0, берется Now.
    /// </param>
    /// <returns>
    ///   ID новой записи, если это возможно, иниче - 0.
    /// </returns>
    function Write(const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      const AText : TSANLogRecordText = ''; ADateTime : TDateTime = 0) : TSANLogRecordID; safecall;

    /// <summary>
    ///   <para>
    ///     Запись в лог в режиме, когда сообщение записывается по частям.
    ///   </para>
    ///   <para>
    ///     Начало записи.
    ///   </para>
    /// </summary>
    /// <param name="AKind">
    ///   Целочисленный тип записи.
    /// </param>
    /// <param name="ASubType">
    ///   Строка подтипа записи.
    /// </param>
    /// <param name="ADateTime">
    ///   Дата и время записи. Если ADateTime = 0, берется Now.
    /// </param>
    /// <returns>
    ///   ID новой записи, если это возможно, иниче - 0.
    /// </returns>
    function BeginWrite(const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      ADateTime : TDateTime = 0) : TSANLogRecordID; safecall;

    /// <summary>
    ///   <para>
    ///     Запись в лог в режиме, когда сообщение записывается по частям.
    ///   </para>
    ///   <para>
    ///     Записать часть сообщения.
    ///   </para>
    /// </summary>
    /// <param name="APartialMessage">
    /// Часть сообщения записи.
    /// </param>
    procedure PartialWrite(const APartialMessage : TSANLogRecordText); safecall;

    /// <summary>
    ///   <para>
    ///     Запись в лог в режиме, когда сообщение записывается по частям.
    ///   </para>
    ///   <para>
    ///     Завершить запись сообщения.
    ///   </para>
    /// </summary>
    procedure EndWrite; safecall;

  end;

  /// <summary>
  ///   Параметр расположения лога.
  /// </summary>
  ISANLogLocationParam = interface(ISANLogBaseIntf)
  ['{035CC781-33FE-4F47-AD54-F1FBD76B3C84}']
    function GetName : TSANLogName; safecall;
    function GetValue : OleVariant; safecall;
    procedure SetValue(const AValue : OleVariant); safecall;

    /// <summary>
    ///   Сравнение свойств с другим параметром расположения.
    /// </summary>
    /// <param name="AOther">
    ///   Другой параметр для сравнения.
    /// </param>
    /// <returns>
    ///   True, если объекты имеют одинаковые свойства.
    /// </returns>
    function IsEquals(const AOther : ISANLogLocationParam) : WordBool; safecall;

    /// <summary>
    ///   Скопировать свойства из ASource.
    /// </summary>
    /// <param name="ASource">
    ///   ASource источник для копирования свойств.
    /// </param>
    procedure Assign(const ASource : ISANLogLocationParam); safecall;

    /// <summary>
    ///   Создать копию с теми же значениями свойств.
    /// </summary>
    /// <returns>
    ///   Копия экземпляра.
    /// </returns>
    function Clone : ISANLogLocationParam; safecall;

    /// <summary>
    ///   Имя параметра.
    /// </summary>
    property Name : TSANLogName  read GetName;

    /// <summary>
    ///   Значение параметра.
    /// </summary>
    property Value : OleVariant  read GetValue  write SetValue;
  end;

  /// <summary>
  ///   Расположение лога.
  /// </summary>
  ISANLogLocation = interface(ISANLogBaseIntf)
  ['{7414F300-92DF-40B9-A0B7-591B51951155}']
    function GetStorage : ISANLogsStorage; safecall;
    function GetParamsCount : integer; safecall;
    function GetParams(const AIndex : integer) : ISANLogLocationParam; safecall;
    function GetParamByName(const AName : TSANLogName) : ISANLogLocationParam; safecall;

    /// <summary>
    ///   Попытка поиска параметра расположения по имени.
    /// </summary>
    /// <param name="AName">
    ///   Имя параметра для поиска.
    /// </param>
    /// <param name="AParam">
    ///   Найденый параметр, если поиск успешен.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, поиск успешен.
    /// </returns>
    function TryFindParamByName(const AName : TSANLogName;
      out AParam : ISANLogLocationParam; const AIndexPtr : PInteger = nil) : WordBool; safecall;

    /// <summary>
    ///   Сравнение свойств с другим расположением.
    /// </summary>
    /// <param name="AOther">
    ///   Другой интерфейс для сравнения.
    /// </param>
    /// <returns>
    ///   True, если объекты имеют одинаковые свойства.
    /// </returns>
    function IsEquals(const AOther : ISANLogLocation) : WordBool; safecall;

    /// <summary>
    ///   Скопировать свойства из ASource.
    /// </summary>
    /// <param name="ASource">
    ///   ASource источник для копирования свойств.
    /// </param>
    procedure Assign(const ASource : ISANLogLocation); safecall;

    /// <summary>
    ///   Создать копию с теми же значениями свойств.
    /// </summary>
    /// <returns>
    ///   Копия экземпляра.
    /// </returns>
    function Clone : ISANLogLocation; safecall;

    /// <summary>
    ///   Хранилище в котором расположен лог.
    /// </summary>
    property Storage : ISANLogsStorage  read GetStorage;

    /// <summary>
    ///   Количество параметров расположения.
    /// </summary>
    property ParamsCount : integer  read GetParamsCount;

    /// <summary>
    ///   Параметр расположения по индексу.
    /// </summary>
    property Params[const AIndex : integer] : ISANLogLocationParam  read GetParams; default;

    /// <summary>
    ///   Параметр расположения по имени.
    /// </summary>
    property ParamByName[const AName : TSANLogName] : ISANLogLocationParam  read GetParamByName;

  end;

  /// <summary>
  ///   Перехватчик событий лога.
  /// </summary>
  ISANLogNotifier = interface(ISANCoreNotifierSafe)
  ['{47C54506-9546-4E84-AADD-C5C8B786B949}']

    /// <summary>
    ///   В лог записана запись.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure RecordWrited(const AContext : ISANLogRecordsOneRecordContext); safecall;
  end;

  /// <summary>
  ///   Полноценный лога.
  /// </summary>
  ISANLog = interface(ISANLogCustom)
  ['{C3BF5BEE-84A2-4BD2-8720-36FB3F6E89A1}']
    function GetName : TSANLogName; safecall;
    function GetLocation : ISANLogLocation; safecall;
    procedure SetLocation(const ALocation : ISANLogLocation); safecall;
    function GetAsynchronous : WordBool; safecall;
    procedure SetAsynchronous(const AAsynchronous : WordBool); safecall;

    /// <summary>
    ///   Вписать в лог запись.
    /// </summary>
    /// <param name="ALogRecord">
    ///   Запись, которую надо вписать в лог.
    /// </param>
    procedure WriteRecord(const ALogRecord : ISANLogRecord); safecall;

    /// <summary>
    ///   Вписать в лог набор записей.
    /// </summary>
    /// <param name="ALogRecords">
    ///   Набор записей, которые надо вписать в лог.
    /// </param>
    /// <param name="AReWrite">
    ///   True - очистить старые данные, False - добавить новые данные в конец.
    /// </param>
    procedure WriteRecords(const ALogRecords : ISANLogRecords; const AReWrite : WordBool = False); safecall;

    /// <summary>
    ///   Прочитать записи из лога.
    /// </summary>
    /// <param name="ALogRecords">
    ///   Набор записей, в который производится чтение.
    /// </param>
    /// <param name="AFilter">
    ///   Фильтр, которым можно отсеч лишние записи.
    /// </param>
    procedure Read(const ALogRecords : ISANLogRecords;
      AFilter : ISANLogRecordsFilter = nil); safecall;

    /// <summary>
    ///   Сравнение двух логов.
    /// </summary>
    /// <param name="ALog">
    ///   Лог, с которым идет сравнение.
    /// </param>
    /// <returns>
    ///   True, логи идентичны.
    /// </returns>
    /// <remarks>
    ///   Сравнение имен и расположений.
    /// </remarks>
    function IsEquals(const ALog : ISANLog) : WordBool; safecall;

    /// <summary>
    ///   Добавить перехватчик событий.
    /// </summary>
    /// <param name="ANotifier">
    ///   Перехватчик событий.
    /// </param>
    /// <returns>
    ///   Описатель для передачи в RemoveNotifier.
    /// </returns>
    function AddNotifier(const ANotifier : ISANLogNotifier) : TSANCoreNotifierHandle; safecall;

    /// <summary>
    ///   Удалить перехватчик событий.
    /// </summary>
    /// <param name="ANotifierHandle">
    ///   Описатель перехватчика событий, полученный из AddNotifier.
    /// </param>
    /// <returns>
    ///   True, в случае успеха.
    /// </returns>
    function RemoveNotifier(const ANotifierHandle : TSANCoreNotifierHandle) : WordBool; safecall;

    /// <summary>
    ///   Уникальное имя лога, однозначно определяющее его
    ///   в списке логов.
    /// </summary>
    property Name : TSANLogName  read GetName;

    /// <summary>
    ///   Расположение лога.
    /// </summary>
    property Location : ISANLogLocation  read GetLocation  write SetLocation;

    /// <summary>
    ///   При TRUE, управление возвращается немедленно, а запись ведется асинхронно.
    /// </summary>
    property Asynchronous : WordBool  read GetAsynchronous  write SetAsynchronous;
  end;
  {$ENDREGION}

  {$REGION 'Storages'}
  /// <summary>
  ///   Набор логов.
  /// </summary>
  ISANLogsSet = interface(IInterface)
  ['{A2760418-3EF7-4828-A56D-84134254AEB6}']
    function GetLogsCount : integer; safecall;
    function GetLogs(const Index : integer) : ISANLog; safecall;
    function GetLogByName(const AName : TSANLogName) : ISANLog; safecall;

    /// <summary>
    ///   Попытка поиск лога по имени.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <param name="ALog">
    ///   Найденый лог, если поиск успешен.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, поиск успешен.
    /// </returns>
    function TryFindByName(const AName : TSANLogName; out ALog : ISANLog;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;

    /// <summary>
    ///   Количество логов в наборе.
    /// </summary>
    property LogsCount : integer  read GetLogsCount;

    /// <summary>
    ///   Лог по индексу.
    /// </summary>
    property Logs[const Index : integer] : ISANLog  read GetLogs;

    /// <summary>
    ///   Лог по индексу.
    /// </summary>
    property LogByName[const AName : TSANLogName] : ISANLog  read GetLogByName;
  end;

  /// <summary>
  ///   Контекст событий одного лога в хранилище.
  /// </summary>
  ISANLogsStorageOneLogContext = interface(ISANCoreNotificationContextSafe)
  ['{93B3A70F-7CE6-4387-9E22-E523AFB9A94E}']
    function GetLog : ISANLog; safecall;

     /// <summary>
     ///   Лог, связанный с событием.
     /// </summary>
    property Log : ISANLog  read GetLog;
  end;

  /// <summary>
  ///   Перехватчик событий хранилища.
  /// </summary>
  ISANLogsStorageNotifier = interface(ISANCoreNotifierSafe)
  ['{7AFAFB66-C6A1-420B-8ED9-55006574C251}']

    /// <summary>
    /// В хранилище добавлен лог.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure LogAdded(const AContext : ISANLogsStorageOneLogContext); safecall;

    /// <summary>
    /// Из хранилища удаляется лог.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure LogRemoving(const AContext : ISANLogsStorageOneLogContext); safecall;
  end;

  /// <summary>
  ///   Хранилище логов.
  /// </summary>
  ISANLogsStorage = interface(ISANLogsSet)
  ['{3960B0D2-EA50-4C31-88B6-BE0826F9B518}']
    function GetName : TSANLogName; safecall;

    /// <summary>
    ///   Заблокировать доступ других потоков.
    /// </summary>
    procedure Lock; safecall;

    /// <summary>
    ///   Разблокировать доступ других потоков.
    /// </summary>
    procedure UnLock; safecall;

    /// <summary>
    ///   Получить указатель на новый экземпляр расположения, для заполнения.
    /// </summary>
    /// <returns>
    ///   Интерфейс расположения.
    /// </returns>
    function CreateLocation : ISANLogLocation; safecall;

    /// <summary>
    ///   Проверить корректность расположения для этого хранилища.
    /// </summary>
    /// <param name="ALocation">
    ///   Расположение.
    /// </param>
    /// <param name="AErrors">
    ///   Сообщения об ошибках, если расположение не корректно.
    /// </param>
    /// <returns>
    ///   True, расположение корректно.
    /// </returns>
    function CheckLocation(const ALocation : ISANLogLocation;
      out AErrors : TSANLogRecordText) : WordBool; safecall;

    /// <summary>
    ///   <para>
    ///     Проверить корректность расположения для этого хранилища.
    ///   </para>
    ///   <para>
    ///     Возбудить исключение, если расположение не корректно.
    ///   </para>
    /// </summary>
    /// <param name="ALocation">
    ///   Расположение.
    /// </param>
    procedure ValidateLocation(const ALocation : ISANLogLocation); safecall;

    /// <summary>
    ///   Добавить новый лог
    /// </summary>
    /// <param name="AName">
    ///   Уникальное имя лога
    /// </param>
    /// <param name="ALocation">
    ///   Расположение.
    /// </param>
    /// <returns>
    ///   Интерфейс лога.
    /// </returns>
    function AddLog(const AName : TSANLogName; const ALocation : ISANLogLocation) : ISANLog; safecall;

    /// <summary>
    ///   Удалить объект лога
    /// </summary>
    /// <param name="AName">
    ///   Уникальное имя лога
    /// </param>
    /// <returns>
    ///   True, успешно.
    /// </returns>
    function RemoveLog(const AName : TSANLogName) : WordBool; safecall;

    /// <summary>
    ///   Добавить перехватчик событий.
    /// </summary>
    /// <param name="ANotifier">
    ///   Перехватчик событий.
    /// </param>
    /// <returns>
    ///   Описатель для передачи в RemoveNotifier.
    /// </returns>
    function AddNotifier(const ANotifier : ISANLogsStorageNotifier) : TSANCoreNotifierHandle; safecall;

    /// <summary>
    ///   Удалить перехватчик событий.
    /// </summary>
    /// <param name="ANotifierHandle">
    ///   Описатель перехватчика событий, полученный из AddNotifier.
    /// </param>
    /// <returns>
    ///   True, в случае успеха.
    /// </returns>
    function RemoveNotifier(const ANotifierHandle : TSANCoreNotifierHandle) : WordBool; safecall;

    /// <summary>
    ///   Сравнение двух хранилищ логов на идентичность.
    /// </summary>
    /// <param name="AStorage">
    ///   Хранилища логов, с которым идет сравнение.
    /// </param>
    /// <returns>
    ///   True, хранилища идентичны.
    /// </returns>
    function IsEquals(const AStorage : ISANLogsStorage) : WordBool; safecall;

    /// <summary>
    /// Уникальное имя хранилища, однозначно определяющее его
    /// в списке хранилищ.
    /// </summary>
    property Name : TSANLogName  read GetName;
  end;

  /// <summary>
  ///   Контекст событий одного хранилища в списке хранилищ.
  /// </summary>
  ISANLogsStoragesOneStorageContext = interface(ISANCoreNotificationContextSafe)
  ['{D6B5A963-89EA-4BE5-95BE-C593A2DD49F3}']
    function GetStorage : ISANLogsStorage; safecall;

     /// <summary>
     ///   Хранилище, связанное с событием.
     /// </summary>
    property Storage : ISANLogsStorage  read GetStorage;
  end;

  /// <summary>
  ///   Перехватчик событий списка хранилищ.
  /// </summary>
  ISANLogsStoragesListNotifier = interface(ISANCoreNotifierSafe)
  ['{DA800EBC-D1B8-4536-BA56-829C106FC2C4}']

    /// <summary>
    ///   Вызывается при добавлении хранилища в список.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Added(const AContext : ISANLogsStoragesOneStorageContext); safecall;

    /// <summary>
    ///   Список очищен.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Cleared(const AContext : ISANCoreNotificationContextSafe); safecall;

    /// <summary>
    ///   Из списка извлечена запись.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Extracted(const AContext : ISANLogsStoragesOneStorageContext); safecall;

    /// <summary>
    ///   Вызывается перед удалением хранилища из списка.
    /// </summary>
    /// <param name="AContext">
    ///   Контекст события.
    /// </param>
    procedure Removing(const AContext : ISANLogsStoragesOneStorageContext); safecall;
  end;

  /// <summary>
  ///   Список хранилищ.
  /// </summary>
  ISANLogsStoragesList = interface(IInterface)
  ['{8F0510A6-EF3F-483F-A7F5-C4930275EBE5}']
    function GetCount : integer; safecall;
    function GetStorages(const Index : integer) : ISANLogsStorage; safecall;

    /// <summary>
    ///   Заблокировать доступ других потоков.
    /// </summary>
    procedure Lock; safecall;

    /// <summary>
    ///   Разблокировать доступ других потоков.
    /// </summary>
    procedure UnLock; safecall;

    /// <summary>
    /// Очистка списка хранилищ.
    /// </summary>
    procedure Clear; safecall;

    /// <summary>
    ///   Попытка поиска хранилища по имени.
    /// </summary>
    /// <param name="AName">
    ///   Имя хранилища.
    /// </param>
    /// <param name="AStorage">
    ///   Найденое хранилище, если поиск успешен.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, поиск успешен.
    /// </returns>
    function TryFindByName(const AName : TSANLogName; out AStorage : ISANLogsStorage;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;

    /// <summary>
    ///   Извлечь хранилище из списка.
    /// </summary>
    /// <param name="AName">
    ///   Имя хранилища.
    /// </param>
    /// <returns>
    ///   Извлеченная запись или nil в случае неудачи.
    /// </returns>
    function ExtractByName(const AName : TSANLogName) : ISANLogsStorage; safecall;

    /// <summary>
    ///   Удалить хранилище из списка.
    /// </summary>
    /// <param name="AName">
    ///   Имя хранилища.
    /// </param>
    /// <returns>
    ///   True, удаление успешно.
    /// </returns>
    function RemoveByName(const AName : TSANLogName) : WordBool; safecall;

    /// <summary>
    ///   Добавить хранилище в список.
    /// </summary>
    /// <param name="AStorage">
    ///   Хранилище.
    /// </param>
    /// <returns>
    ///   Интерфейс хранилища.
    /// </returns>
    function Add(const AStorage : ISANLogsStorage) : ISANLogsStorage; safecall;

    /// <summary>
    ///   Добавить перехватчик событий.
    /// </summary>
    /// <param name="ANotifier">
    ///   Перехватчик событий.
    /// </param>
    /// <returns>
    ///   Описатель для передачи в RemoveNotifier.
    /// </returns>
    function AddNotifier(const ANotifier : ISANLogsStoragesListNotifier) : TSANCoreNotifierHandle; safecall;

    /// <summary>
    ///   Удалить перехватчик событий.
    /// </summary>
    /// <param name="ANotifierHandle">
    ///   Описатель перехватчика событий, полученный из AddNotifier.
    /// </param>
    /// <returns>
    ///   True, в случае успеха.
    /// </returns>
    function RemoveNotifier(const ANotifierHandle : TSANCoreNotifierHandle) : WordBool; safecall;

    /// <summary>
    /// Количество хранилищ.
    /// </summary>
    property Count : integer  read GetCount;

    /// <summary>
    /// Получение интерфейса хранилища.
    /// </summary>
    property Storages[const Index : integer] : ISANLogsStorage  read GetStorages; default;
  end;

  /// <summary>
  ///   Синглтон управления логированием.
  /// </summary>
  ISANLogger = interface(ISANLogsSet)
  ['{E03F99FF-49C4-498D-AA97-9EC0DB9F2636}']
    function GetStorages : ISANLogsStoragesList; safecall;
    function GetOuterTermination : ITerminationSafe; safecall;
    procedure SetOuterTermination(const AOuterTermination : ITerminationSafe); safecall;

    /// <summary>
    /// Список хранилищ
    /// </summary>
    property Storages : ISANLogsStoragesList  read GetStorages;

    /// <summary>
    ///   Проверка завершения извне.
    /// </summary>
    property OuterTermination : ITerminationSafe
      read GetOuterTermination  write SetOuterTermination;
  end;

  /// <summary>
  ///   Создание экземпляров.
  /// </summary>
  ISANLoggerMaker = interface(IInterface)
  ['{6795F232-4888-44B6-B40A-C705F6E57363}']
    /// <summary>
    ///   Создать объект записи лога.
    /// </summary>
    function MakeLogRecord : ISANLogRecord; safecall;

    /// <summary>
    ///   Создать объект набора записей лога.
    /// </summary>
    function MakeLogsRecords : ISANLogRecords; safecall;
  end;

  /// <summary>
  ///   Прототип точки вход расширения логгера.
  /// </summary>
  TSANLoggerLoad = function(const ALogger : ISANLogger) : HRESULT; stdcall;
  /// <summary>
  ///   Прототип точки вход расширения логгера. Safe.
  /// </summary>
  TSANLoggerLoadSafe = procedure(const ALogger : ISANLogger); safecall;
  {$ENDREGION}

implementation

end.
