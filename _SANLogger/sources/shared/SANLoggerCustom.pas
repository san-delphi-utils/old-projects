{*******************************************************}
{                                                       }
{       Logger                                          }
{                                                       }
{       Абстрактные классы логгера.                     }
{                                                       }
{       Перефикс: SANLog                                }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SANLoggerCustom;

interface

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  Windows, Messages, SysUtils, Classes, Variants, Math, DateUtils, SyncObjs,
  {$ENDREGION}

  {$REGION 'SANCore'}
  SANCoreIntfs, SANCoreImpl,
  {$ENDREGION}

  {$REGION 'SANLogger'}
  SANLoggerIntfs, SANLoggerContexts;
  {$ENDREGION}
{$ENDREGION}

{$REGION 'resourcestring'}
resourcestring
  sSANLoggerCustomLocationParamNotFound =
    'Location param "%s" not found';

  sSANLoggerCustomCurRecordAlreadySettedUp =
    'Current record already setted up';

  sSANLoggerCustomCurRecordAlreadyDroppedDown =
    'Current record already dropped down';

  sSANLoggerLogWithNameNotFoundFmt =
    'Log with name %s not found';

  sSANLoggerLogWithNameAlreadyExistsFmt =
    'Log with name %s already exists';

  sSANLoggerLocationFromOtherModuleFmt =
    'Location from module %d. Storage module is %d';

  sSANLoggerInvalidLocationFmt =
    'Invalid location: %s';
{$ENDREGION}

{$REGION 'const'}
const
  {$REGION 'TSANLogsCustomStorage messages'}
  /// <summary>
  ///   Исключение в потоке.
  /// </summary>
  CM_IO_THREAD_EXCEPTION    = WM_USER + 1;
  /// <summary>
  ///   Асинхронное событие лога.
  /// </summary>
  CM_IO_THREAD_ASYNC_EVENT  = WM_USER + 2;
  {$ENDREGION}

  {$REGION 'TSANLogsCustomStorage IO flags'}
  /// <summary>
  ///   Флаг перезаписи.
  /// </summary>
  FLAG_IO_REWRITE        = $0001;

  /// <summary>
  ///   Флаг начала частичной записи.
  /// </summary>
  FLAG_IO_BEG_PARTIAL    = $0002;

  /// <summary>
  ///   Флаг частичной записи.
  /// </summary>
  FLAG_IO_PARTIAL        = $0003;

  /// <summary>
  ///   Флаг окончания частичной записи.
  /// </summary>
  FLAG_IO_END_PARTIAL    = $0004;

  /// <summary>
  ///   Флаг начала частичной записи.
  /// </summary>
  FLAG_IO_MULTI_RECORDS  = $0005;
  {$ENDREGION}

{$ENDREGION}

type
  /// <summary>
  ///   Базовое исключение для лога.
  /// </summary>
  ESANLogCustomError = class(Exception);

  /// <summary>
  ///   Параметр расположения лога.
  /// </summary>
  TSANLogLocationParam = class(TSANCoreInterfacedContainedPersistent,
    ISANCorePersistent, ISANCorePersistentSafe, ISANLogLocationParam)
  private
    FName : TSANLogName;
    FValue : OleVariant;
    FValueCS : TCriticalSection;

  protected
    {$REGION 'ISANLogLocationParam'}
    function GetName : TSANLogName; safecall;
    function GetValue : OleVariant; safecall;
    procedure SetValue(const AValue : OleVariant); safecall;
    function IsEquals(const AOther : ISANLogLocationParam) : WordBool; safecall;
    procedure Assign(const ASource : ISANLogLocationParam); safecall;
    function Clone : ISANLogLocationParam; safecall;
    {$ENDREGION}

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); override;
    procedure DoAssign(const ASource : IInterface); override;
    procedure DoClone(out AClone); override;

  public
    constructor Create(const AController: ISANLogLocation; const AName : TSANLogName);
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property Name : TSANLogName  read GetName;
    property Value : OleVariant  read GetValue  write SetValue;

  end;

  /// <summary>
  ///   Расположение лога.
  /// </summary>
  TSANLogLocation = class(TSANCoreInterfacedContainedPersistent,
    ISANCorePersistent, ISANCorePersistentSafe,
    ISANCoreModuleHandleProvider, ISANCoreModuleHandleProviderSafe,
    ISANLogLocation)
  private
    FParams : IInterfaceListEx;

  protected
     {$REGION 'ISANCoreModuleHandleProvider'}
    function GetModuleHandle : HMODULE;
    {$ENDREGION}

    {$REGION 'ISANCoreModuleHandleProviderSafe'}
    function ISANCoreModuleHandleProviderSafe.GetModuleHandle = GetModuleHandleSafe;
    function GetModuleHandleSafe : HMODULE; safecall;
    {$ENDREGION}


    {$REGION 'ISANLogLocation'}
    function GetStorage : ISANLogsStorage; safecall;
    function GetParamsCount : integer; safecall;
    function GetParams(const AIndex : integer) : ISANLogLocationParam; safecall;
    function GetParamByName(const AName : TSANLogName) : ISANLogLocationParam; safecall;
    function TryFindParamByName(const AName : TSANLogName;
      out AParam : ISANLogLocationParam; const AIndexPtr : PInteger = nil) : WordBool; safecall;
    function IsEquals(const AOther : ISANLogLocation) : WordBool; safecall;
    procedure Assign(const ASource : ISANLogLocation); safecall;
    function Clone : ISANLogLocation; safecall;
    {$ENDREGION}

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); override;
    procedure DoAssign(const ASource : IInterface); override;
    procedure DoClone(out AClone); override;

    property ParamsList : IInterfaceListEx  read FParams;

  public
    constructor Create(const AController: ISANLogsStorage);
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property Storage : ISANLogsStorage  read GetStorage;
    property ParamsCount : integer  read GetParamsCount;
    property Params[const AIndex : integer] : ISANLogLocationParam  read GetParams; default;
    property ParamByName[const AName : TSANLogName] : ISANLogLocationParam  read GetParamByName;

  end;

  {$REGION 'IOContexts'}
  /// <summary>
  ///   Базовый контекст ввода/вывода.
  /// </summary>
  ISANLogIOContext = interface(IInterface)
  ['{51F24110-33D8-4FE8-84C7-7D498172EB99}']
    function GetLog : ISANLog;
    function GetCompletedEvent : THandle;

    /// <summary>
    ///   Действие завершено.
    /// </summary>
    procedure Completed;

    /// <summary>
    ///   Лог, для которого выполняется действие.
    /// </summary>
    property Log : ISANLog  read GetLog;

    /// <summary>
    ///   Описатель события завершения действия.
    /// </summary>
    property CompletedEvent : THandle  read GetCompletedEvent;
  end;

  /// <summary>
  ///   Контекст ввода с одной записью.
  /// </summary>
  ISANLogIOCustomWriteRecordContext = interface(ISANLogIOContext)
  ['{67A5B22B-73B7-4BF9-8EB2-03CE62254B89}']
    function GetRec : ISANLogRecord;

    /// <summary>
    ///   Записываемая запись.
    /// </summary>
    property Rec : ISANLogRecord read GetRec;
  end;

  /// <summary>
  ///   Контекст команды WriteRecord.
  /// </summary>
  ISANLogIOWriteRecordContext = interface(ISANLogIOCustomWriteRecordContext)
  ['{87CA291E-2C6B-4428-B88C-F9CDA85BFA56}']
  end;

  /// <summary>
  ///   Контекст команды BeginWrite.
  /// </summary>
  ISANLogIOBeginWriteContext = interface(ISANLogIOCustomWriteRecordContext)
  ['{964747D7-235B-4E4F-BB45-90A160264BFC}']
  end;

  /// <summary>
  ///   Контекст команды PartialWrite.
  /// </summary>
  ISANLogIOPartialWriteContext = interface(ISANLogIOCustomWriteRecordContext)
  ['{AF53341C-F44C-4EB5-BFC7-10265E39105D}']
    function GetPartialMessage : string;

    /// <summary>
    ///   Сообщение для частичной записи.
    /// </summary>
    property PartialMessage : string  read GetPartialMessage;
  end;

  /// <summary>
  ///   Контекст команды EndWrite.
  /// </summary>
  ISANLogIOEndWriteContext = interface(ISANLogIOCustomWriteRecordContext)
  ['{46F8F96E-CD97-4753-A0A6-ABAE3AFCA5F8}']
  end;

  /// <summary>
  ///   Контекст команды WriteRecords.
  /// </summary>
  ISANLogIOWriteRecordsContext = interface(ISANLogIOContext)
  ['{B8A9698E-CFC0-42A8-AB00-B016CE0823EC}']
    function GetRecs : ISANLogRecords;
    function GetReWrite : boolean;

    /// <summary>
    ///   Записываемая запись.
    /// </summary>
    property Recs : ISANLogRecords read GetRecs;

    property ReWrite : boolean  read GetReWrite;
  end;

  /// <summary>
  ///   Контекст команды Read.
  /// </summary>
  ISANLogIOReadContext = interface(ISANLogIOContext)
  ['{67A5B22B-73B7-4BF9-8EB2-03CE62254B89}']
    function GetRecs : ISANLogRecords;
    function GetFilter : ISANLogRecordsFilter;

    /// <summary>
    ///   Список читакмых записей.
    /// </summary>
    property Recs : ISANLogRecords read GetRecs;

    /// <summary>
    ///   Список читакмых записей.
    /// </summary>
    property Filter : ISANLogRecordsFilter read GetFilter;
  end;
  {$ENDREGION}

  /// <summary>
  ///   Нотификация лога.
  /// </summary>
  ISANLogCustomNotification = interface(IInterface)
    ['{BEA775A7-A86C-42B0-9D60-5A0DCB4B2902}']

    /// <summary>
    ///   Возбудить событие при разрушении.
    /// </summary>
    procedure NotifyAllDestroying;

    /// <summary>
    ///   Возбудить событие одной записи в лог.
    /// </summary>
    /// <param name="ARec">
    ///   Запись.
    /// </param>
    procedure NotifyAllRecordWrited(const ARec : ISANLogRecord);
  end;


  TSANLogsCustomStorage = class;

  /// <summary>
  ///   Абстрактный лог.
  /// </summary>
  TSANLogCustom = class abstract(TSANCoreInterfacedContainedPersistent,
    ISANCorePersistent, ISANCorePersistentSafe,
    ISANLogCustom, ISANLog, ISANLoggerMaker, ISANLogCustomNotification)
  private
    FName : TSANLogName;
    FLocation : ISANLogLocation;
    FNotifiers : IInterfaceListEx;
    FAsynchronous : WordBool;
    FPartialWriteRec : ISANLogRecord;
    FPartialWriteCS, FLocationCS : TCriticalSection;
    FStorage : TSANLogsCustomStorage;

  protected
    {$REGION 'ISANLogCustom'}
    function Write(const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      const AText : TSANLogRecordText = ''; ADateTime : TDateTime = 0) : TSANLogRecordID; safecall;
    function BeginWrite(const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      ADateTime : TDateTime = 0) : TSANLogRecordID; safecall;
    procedure PartialWrite(const APartialMessage : TSANLogRecordText); safecall;
    procedure EndWrite; safecall;
    {$ENDREGION}

    {$REGION 'ISANLog'}
    function GetName : TSANLogName; safecall;
    function GetLocation : ISANLogLocation; safecall;
    procedure SetLocation(const ALocation : ISANLogLocation); safecall;
    function GetAsynchronous : WordBool; safecall;
    procedure SetAsynchronous(const AAsynchronous : WordBool); safecall;
    procedure WriteRecord(const ALogRecord : ISANLogRecord); safecall;
    procedure WriteRecords(const ALogRecords : ISANLogRecords; const AReWrite : WordBool = False); safecall;
    procedure Read(const ALogRecords : ISANLogRecords;
      AFilter : ISANLogRecordsFilter = nil); safecall;
    function IsEquals(const ALog : ISANLog) : WordBool; safecall;
    function AddNotifier(const ANotifier : ISANLogNotifier) : TSANCoreNotifierHandle; safecall;
    function RemoveNotifier(const ANotifierHandle : TSANCoreNotifierHandle) : WordBool; safecall;
    {$ENDREGION}

    {$REGION 'ISANLoggerMaker'}
    function MakeLogRecord : ISANLogRecord; safecall;
    function MakeLogsRecords : ISANLogRecords; safecall;
    {$ENDREGION}

    {$REGION 'ISANLogCustomNotification'}
    procedure NotifyAllDestroying;
    procedure NotifyAllRecordWrited(const ARec : ISANLogRecord);
    {$ENDREGION}

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); override;

    procedure DoWrite(const ARecord : ISANLogRecord); virtual;
    procedure DoBeginWrite; virtual;
    procedure DoPartialWrite(const APartialMessage : TSANLogRecordText); virtual;
    procedure DoEndWrite; virtual;

    procedure DoSetLocation(var ALocation : ISANLogLocation;
      const ANewLocation: ISANLogLocation); virtual;
    function DoGetAsynchronous : WordBool; virtual;
    procedure DoSetAsynchronous(const AAsynchronous : WordBool); virtual;
    procedure DoWriteRecords(const ARecords : ISANLogRecords; const AReWrite : WordBool); virtual;
    procedure DoRead(const ALogRecords : ISANLogRecords;
      const AFilter : ISANLogRecordsFilter); virtual;

    property Storage : TSANLogsCustomStorage  read FStorage;

  public
    constructor Create(const AStorage : TSANLogsCustomStorage; const AName : TSANLogName = '');
    procedure BeforeDestruction; override;
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property Name : TSANLogName  read GetName;
    property Location : ISANLogLocation  read GetLocation  write SetLocation;
    property Asynchronous : WordBool  read GetAsynchronous  write SetAsynchronous;

    /// <summary>
    ///   Свойство доступно только в момент записи. Создаваемая запись.
    /// </summary>
    property CurWritingRecord : ISANLogRecord  read FPartialWriteRec;
  end;

  /// <summary>
  ///   Поток ввода/вывода абстрактного хранилища.
  /// </summary>
  TSANLogsCustomStorageIOThread = class(TThread)
  private
    FStorage : TSANLogsCustomStorage;

  protected
    procedure Execute; override;

  public
    constructor Create(const AStorage : TSANLogsCustomStorage);
  end;

  /// <summary>
  ///   Абстрактное хранилище логов.
  /// </summary>
  TSANLogsCustomStorage = class abstract(TSANCoreInterfacedContainedPersistent,
    ISANCorePersistent, ISANCorePersistentSafe,
    ISANCoreModuleHandleProvider, ISANCoreModuleHandleProviderSafe,
    ISANLogsSet, ISANLogsStorage, ISANLoggerMaker)
  private
    FName : TSANLogName;
    FLogs, FNotifiers : IInterfaceListEx;
    FMsgWnd : HWND;
    FIOThread : TThread;
    FIOEvents : array[0..1] of THandle;
    FCommandQueue : IInterfaceListEx;

    procedure IOThreadException(var AMessage: TMessage); message CM_IO_THREAD_EXCEPTION;
    procedure IOThreadAsyncEvent(var AMessage: TMessage); message CM_IO_THREAD_ASYNC_EVENT;

  protected
    {$REGION 'ISANCoreModuleHandleProvider'}
    function GetModuleHandle : HMODULE;
    {$ENDREGION}

    {$REGION 'ISANCoreModuleHandleProviderSafe'}
    function ISANCoreModuleHandleProviderSafe.GetModuleHandle = GetModuleHandleSafe;
    function GetModuleHandleSafe : HMODULE; safecall;
    {$ENDREGION}

    {$REGION 'ISANLogsSet'}
    function GetLogsCount : integer; safecall;
    function GetLogs(const Index : integer) : ISANLog; safecall;
    function GetLogByName(const AName : TSANLogName) : ISANLog; safecall;
    function TryFindByName(const AName : TSANLogName; out ALog : ISANLog;
      const AIndexPtr : PInteger = nil) : WordBool; safecall;
    {$ENDREGION}

    {$REGION 'ISANLogsStorage'}
    function GetName : TSANLogName; safecall;
    procedure Lock; safecall;
    procedure UnLock; safecall;
    function CreateLocation : ISANLogLocation; safecall;
    function CheckLocation(const ALocation : ISANLogLocation;
      out AErrors : TSANLogRecordText) : WordBool; safecall;
    procedure ValidateLocation(const ALocation : ISANLogLocation); safecall;
    function AddLog(const AName : TSANLogName; const ALocation : ISANLogLocation) : ISANLog; safecall;
    function RemoveLog(const AName : TSANLogName) : WordBool; safecall;
    function AddNotifier(const ANotifier : ISANLogsStorageNotifier) : TSANCoreNotifierHandle; safecall;
    function RemoveNotifier(const ANotifierHandle : TSANCoreNotifierHandle) : WordBool; safecall;
    function IsEquals(const AStorage : ISANLogsStorage) : WordBool; safecall;
    {$ENDREGION}

    {$REGION 'ISANLoggerMaker'}
    function MakeLogRecord : ISANLogRecord; safecall;
    function MakeLogsRecords : ISANLogRecords; safecall;
    {$ENDREGION}

    procedure DoIsEquals(const AOther : IInterface; out AIsEquals : WordBool); override;
    function DoCreateLocation : ISANLogLocation; virtual; abstract;
    function DoCreateLog(const AName : TSANLogName) : ISANLog; virtual; abstract;
    procedure DoCheckLocation(const ALocation : ISANLogLocation;
      out AErrors : TSANLogRecordText; out AIsCorrect : WordBool); virtual; abstract;

    procedure NotifyAllDestroying;
    procedure NotifyAllLogAdded(const ALog : ISANLog);
    procedure NotifyAllLogRemoving(const ALog : ISANLog);

    procedure MessagesDispatch(var AMessage: TMessage);

    procedure IOThreadExecute; virtual;
    procedure IOThreadLoop; virtual;
    procedure IOThreadInit; virtual;
    procedure IOThreadFinal; virtual;
    procedure IOThreadCommandQueue; virtual;
    procedure IOThreadCommand(const AIOContext : ISANLogIOContext); virtual;
    procedure DoIOThreadCommand(const AIOContext : ISANLogIOContext); virtual; abstract;

    procedure TerminateIOThread;

    procedure IOCommand(const AContext : ISANLogIOContext);

    procedure IONotify(const AContext : ISANLogIOContext);

    property IOThread : TThread  read FIOThread;

  public
    constructor Create(const AController: ISANLogger; const AName : TSANLogName = '');
    procedure BeforeDestruction; override;
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property LogsCount : integer  read GetLogsCount;
    property Logs[const Index : integer] : ISANLog  read GetLogs;
    property LogByName[const AName : TSANLogName] : ISANLog  read GetLogByName;
    property Name : TSANLogName  read GetName;

  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'Std'}
  ActiveX, ComObj;
  {$ENDREGION}
{$ENDREGION}

//==============================================================================
{$REGION 'TSANLogLocationParam'}
procedure TSANLogLocationParam.Assign(const ASource: ISANLogLocationParam);
begin
  DoAssign(ASource);
end;

function TSANLogLocationParam.Clone: ISANLogLocationParam;
begin
  DoClone(Result);
end;

constructor TSANLogLocationParam.Create(const AController: ISANLogLocation;
  const AName: TSANLogName);
begin
  inherited Create(AController);

  FName := AName;

  FValueCS := TCriticalSection.Create;
end;

destructor TSANLogLocationParam.Destroy;
begin
  FreeAndNil(FValueCS);

  inherited;
end;

procedure TSANLogLocationParam.DoAssign(const ASource: IInterface);
var
  Source : ISANLogLocationParam;
begin

  if not Supports(ASource, ISANLogLocationParam, Source) then
    inherited;

  FName := Source.Name;
  Value := Source.Value;
end;

procedure TSANLogLocationParam.DoClone(out AClone);
var
  vClone : ISANLogLocationParam;
begin
  vClone := TSANLogLocationParam.Create(Controller as ISANLogLocation, FName);
  vClone.Assign(Self);
  ISANLogLocationParam(AClone) := vClone;
end;

procedure TSANLogLocationParam.DoIsEquals(const AOther: IInterface;
  out AIsEquals: WordBool);
var
  Other : ISANLogLocationParam;
begin
  AIsEquals := Supports(AOther, ISANLogLocationParam, Other)
           and WideSameText(Other.Name, Name)
           and (VarCompareValue(Other.Value, Value) = vrEqual)
end;

function TSANLogLocationParam.GetName: TSANLogName;
begin
  Result := FName;
end;

function TSANLogLocationParam.GetValue: OleVariant;
begin
  FValueCS.Enter;
  try
    Result := FValue;
  finally
    FValueCS.Leave;
  end;
end;

function TSANLogLocationParam.IsEquals(const AOther: ISANLogLocationParam): WordBool;
begin
  DoIsEquals(AOther, Result);
end;

function TSANLogLocationParam.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogLocationParam,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

procedure TSANLogLocationParam.SetValue(const AValue: OleVariant);
begin
  FValueCS.Enter;
  try
    FValue := AValue;
  finally
    FValueCS.Leave;
  end;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogLocation'}
procedure TSANLogLocation.Assign(const ASource: ISANLogLocation);
begin
  DoAssign(ASource);
end;

function TSANLogLocation.Clone: ISANLogLocation;
begin
  DoClone(Result);
end;

constructor TSANLogLocation.Create(const AController: ISANLogsStorage);
begin
  inherited Create(AController);
  FParams := TInterfaceList.Create;
end;

procedure TSANLogLocation.DoAssign(const ASource: IInterface);
var
  Source : ISANLogLocation;
  i : integer;
begin

  if not Supports(ASource, ISANLogLocation, Source) then
    inherited;

  FParams.Lock;
  try

    for i := 0 to Source.ParamsCount - 1 do
      if i >= FParams.Count then
        FParams.Add(Source.Params[i].Clone)
      else
        Params[i].Assign(Source[i]);

    for i := FParams.Count - 1 downto Source.ParamsCount - 1 do
      FParams.Delete(i);

  finally
    FParams.Unlock;
  end;
end;

procedure TSANLogLocation.DoClone(out AClone);
var
  vClone : ISANLogLocation;
begin
  vClone := TSANLogLocation.Create(Storage);
  vClone.Assign(Self);
  ISANLogLocation(AClone) := vClone;
end;

procedure TSANLogLocation.DoIsEquals(const AOther: IInterface;
  out AIsEquals: WordBool);
var
  Other : ISANLogLocation;
  i : integer;
begin
  AIsEquals := False;

  FParams.Lock;
  try

    if not Supports(AOther, ISANLogLocation, Other)
    or (Other.ParamsCount <> ParamsCount)
    then
      Exit;

    for i := 0 to ParamsCount - 1 do
      if not Params[i].IsEquals(Other[i]) then
        Exit;

  finally
    FParams.Unlock;
  end;

  AIsEquals := True;
end;

function TSANLogLocation.GetModuleHandle: HMODULE;
begin
  Result := HInstance;
end;

function TSANLogLocation.GetModuleHandleSafe: HMODULE;
begin
  Result := GetModuleHandle;
end;

function TSANLogLocation.GetParamByName(const AName: TSANLogName): ISANLogLocationParam;
begin
  if not TryFindParamByName(AName, Result) then
    raise ESANLogCustomError.CreateResFmt(@sSANLoggerCustomLocationParamNotFound, [AName]);
end;

function TSANLogLocation.GetParams(const AIndex: integer): ISANLogLocationParam;
begin
  Result := ISANLogLocationParam(FParams[AIndex]);
end;

function TSANLogLocation.GetParamsCount: integer;
begin
  Result := FParams.Count;
end;

function TSANLogLocation.GetStorage: ISANLogsStorage;
begin
  Result := Controller as ISANLogsStorage;
end;

function TSANLogLocation.IsEquals(const AOther: ISANLogLocation): WordBool;
begin
  DoIsEquals(AOther, Result);
end;

function TSANLogLocation.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogLocation,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

function TSANLogLocation.TryFindParamByName(const AName: TSANLogName;
  out AParam: ISANLogLocationParam; const AIndexPtr: PInteger): WordBool;
var
  i : integer;
begin
  FParams.Lock;
  try

    for i := 0 to ParamsCount - 1 do
      if WideSameText(Params[i].Name, AName) then
      begin
        AParam := Params[i];

        if Assigned(AIndexPtr) then
          AIndexPtr^ := i;

        Exit(True);
      end;

  finally
    FParams.Unlock;
  end;

  AParam := nil;
  Result := False;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogIOCustomContext'}
type
  TSANLogIOCustomContext = class(TInterfacedObject, ISANLogIOContext)
  private
    FLog : ISANLog;
    FCompleteEvent : THandle;
  protected
    {$REGION 'ISANLogIOContext'}
    function GetLog : ISANLog;
    function GetCompletedEvent : THandle;
    procedure Completed;
    {$ENDREGION}

  public
    constructor Create(const ALog : ISANLog);
    destructor Destroy; override;
  end;

procedure TSANLogIOCustomContext.Completed;
begin
  SetEvent(FCompleteEvent);
end;

constructor TSANLogIOCustomContext.Create(const ALog: ISANLog);
begin
  inherited Create;
  FLog := ALog;

  FCompleteEvent := CreateEvent(nil, True, False, nil);
  if FCompleteEvent = 0 then
    RaiseLastOSError;
end;

destructor TSANLogIOCustomContext.Destroy;
begin
  CloseHandle(FCompleteEvent);

  inherited;
end;

function TSANLogIOCustomContext.GetCompletedEvent: THandle;
begin
  Result := FCompleteEvent;
end;

function TSANLogIOCustomContext.GetLog: ISANLog;
begin
  Result := FLog;
end;
{$ENDREGION}

{$REGION 'TSANLogIOCustomRecContext'}
type
  TSANLogIOCustomRecContext = class(TSANLogIOCustomContext,
    ISANLogIOCustomWriteRecordContext)
  private
    FRec : ISANLogRecord;

  protected
    {$REGION 'ISANLogIOCustomWriteRecordContext'}
    function GetRec : ISANLogRecord;
    {$ENDREGION}

  public
    constructor Create(const ALog : ISANLog; const ARec : ISANLogRecord);

  end;

constructor TSANLogIOCustomRecContext.Create(const ALog: ISANLog;
  const ARec: ISANLogRecord);
begin
  inherited Create(ALog);
  FRec := ARec;
end;

function TSANLogIOCustomRecContext.GetRec: ISANLogRecord;
begin
  Result := FRec;
end;
{$ENDREGION}

{$REGION 'TSANLogIOWriteRecordContext'}
type
  TSANLogIOWriteRecordContext = class(TSANLogIOCustomRecContext,
    ISANLogIOContext, ISANLogIOCustomWriteRecordContext, ISANLogIOWriteRecordContext)
  end;
{$ENDREGION}

{$REGION 'TSANLogIOBeginWriteContext'}
type
  TSANLogIOBeginWriteContext = class(TSANLogIOCustomRecContext,
    ISANLogIOContext, ISANLogIOCustomWriteRecordContext, ISANLogIOBeginWriteContext)
  end;
{$ENDREGION}

{$REGION 'TSANLogIOPartialWriteContext'}
type
  TSANLogIOPartialWriteContext = class(TSANLogIOCustomRecContext,
    ISANLogIOContext, ISANLogIOCustomWriteRecordContext, ISANLogIOPartialWriteContext)
  private
    FPartialMessage : string;

  protected
    {$REGION 'ISANLogIOPartialWriteContext'}
    function GetPartialMessage : string;
    {$ENDREGION}

  public
    constructor Create(const ALog : ISANLog; const ARec : ISANLogRecord;
      const APartialMessage : string);
  end;

constructor TSANLogIOPartialWriteContext.Create(const ALog: ISANLog;
  const ARec: ISANLogRecord; const APartialMessage: string);
begin
  inherited Create(ALog, ARec);

  FPartialMessage := APartialMessage;
end;

function TSANLogIOPartialWriteContext.GetPartialMessage: string;
begin
  Result := FPartialMessage;
end;
{$ENDREGION}

{$REGION 'TSANLogIOEndWriteContext'}
type
  TSANLogIOEndWriteContext = class(TSANLogIOCustomRecContext,
    ISANLogIOContext, ISANLogIOCustomWriteRecordContext, ISANLogIOEndWriteContext)
  end;
{$ENDREGION}

{$REGION 'TSANLogIOWriteRecordsContext'}
type
  TSANLogIOWriteRecordsContext = class(TSANLogIOCustomContext,
    ISANLogIOContext, ISANLogIOWriteRecordsContext)
  private
    FRecs : ISANLogRecords;
    FReWrite: boolean;

  protected
    {$REGION 'ISANLogIOWriteRecordsContext'}
    function GetRecs : ISANLogRecords;
    function GetReWrite : boolean;
    {$ENDREGION}

    constructor Create(const ALog : ISANLog; const ARecs : ISANLogRecords;
      const AReWrite: boolean);
  end;

constructor TSANLogIOWriteRecordsContext.Create(const ALog: ISANLog;
  const ARecs: ISANLogRecords; const AReWrite: boolean);
begin
  inherited Create(ALog);

  FRecs     := ARecs;
  FReWrite  := AReWrite;
end;

function TSANLogIOWriteRecordsContext.GetRecs: ISANLogRecords;
begin
  Result := FRecs;
end;
function TSANLogIOWriteRecordsContext.GetReWrite: boolean;
begin
  Result := FReWrite;
end;
{$ENDREGION}

{$REGION 'TSANLogIOReadContext'}
type
  TSANLogIOReadContext = class(TSANLogIOCustomContext,
    ISANLogIOContext, ISANLogIOReadContext)
  private
    FRecs : ISANLogRecords;
    FFilter : ISANLogRecordsFilter;
  protected
    {$REGION 'ISANLogIOReadContext'}
    function GetRecs : ISANLogRecords;
    function GetFilter : ISANLogRecordsFilter;
    {$ENDREGION}

  public
    constructor Create(const ALog : ISANLog; const ARecs : ISANLogRecords;
      const AFilter : ISANLogRecordsFilter);

  end;

constructor TSANLogIOReadContext.Create(const ALog: ISANLog;
  const ARecs: ISANLogRecords; const AFilter: ISANLogRecordsFilter);
begin
  inherited Create(ALog);

  FRecs    := ARecs;
  FFilter  := AFilter;
end;

function TSANLogIOReadContext.GetFilter: ISANLogRecordsFilter;
begin
  Result := FFilter;
end;

function TSANLogIOReadContext.GetRecs: ISANLogRecords;
begin
  Result := FRecs;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogCustom'}
function TSANLogCustom.AddNotifier(const ANotifier: ISANLogNotifier): TSANCoreNotifierHandle;
begin
  FNotifiers.Add(ANotifier);
  Result := TSANCoreNotifierHandle(ANotifier);
end;

function TSANLogCustom.RemoveNotifier(const ANotifierHandle: TSANCoreNotifierHandle): WordBool;
begin
  Result := FNotifiers.Remove(IInterface(ANotifierHandle)) > -1;
end;

procedure TSANLogCustom.BeforeDestruction;
begin
  NotifyAllDestroying;

  inherited;
end;

function TSANLogCustom.BeginWrite(const AKind: TSANLogRecordKind;
  const ASubType: TSANLogRecordSubType; ADateTime: TDateTime): TSANLogRecordID;
begin

  if CompareValue(ADateTime, 0, OneMillisecond) = 0 then
    ADateTime := Now;

  FPartialWriteCS.Enter;
  try

    FPartialWriteRec := MakeLogRecord.From(0, ADateTime, AKind, ASubType, '');

    DoBeginWrite;

  except
    FPartialWriteCS.Leave;
    raise;
  end;
end;

procedure TSANLogCustom.PartialWrite(const APartialMessage: TSANLogRecordText);
begin
  DoPartialWrite(APartialMessage);
end;

procedure TSANLogCustom.EndWrite;
begin
  try

    try

      DoEndWrite;

    finally
      FPartialWriteRec := nil;
    end;

  finally
    FPartialWriteCS.Leave;
  end;
end;

constructor TSANLogCustom.Create(const AStorage : TSANLogsCustomStorage;
  const AName: TSANLogName);
begin
  inherited Create(AStorage);
  FStorage := AStorage;
  FName := AName;

  FPartialWriteCS  := TCriticalSection.Create;
  FLocationCS      := TCriticalSection.Create;
end;

destructor TSANLogCustom.Destroy;
begin
  FreeAndNil(FPartialWriteCS);
  FreeAndNil(FLocationCS);

  inherited;
end;

function TSANLogCustom.DoGetAsynchronous : WordBool;
begin
  Result := FAsynchronous;
end;

procedure TSANLogCustom.DoIsEquals(const AOther: IInterface;
  out AIsEquals: WordBool);
var
  Other : ISANLog;
begin
  AIsEquals := Supports(AOther, ISANLog, Other)
           and WideSameText(Name, Other.Name)
           and SANCoreIntfs.IsEquals(Self, Other);

  inherited;
end;

procedure TSANLogCustom.DoSetAsynchronous(const AAsynchronous: WordBool);
begin
  FAsynchronous := AAsynchronous;
end;

procedure TSANLogCustom.DoSetLocation(var ALocation: ISANLogLocation;
  const ANewLocation: ISANLogLocation);
begin
  ALocation := ANewLocation;
end;

procedure TSANLogCustom.DoWrite(const ARecord: ISANLogRecord);
begin
  FStorage.IOCommand(TSANLogIOWriteRecordContext.Create(Self, ARecord));
end;

procedure TSANLogCustom.DoWriteRecords(const ARecords: ISANLogRecords;
  const AReWrite: WordBool);
begin
  FStorage.IOCommand
  (
    TSANLogIOWriteRecordsContext.Create(Self, ARecords, AReWrite)
  );
end;

procedure TSANLogCustom.DoBeginWrite;
begin
  FStorage.IOCommand(TSANLogIOBeginWriteContext.Create(Self, FPartialWriteRec));
end;

procedure TSANLogCustom.DoPartialWrite(const APartialMessage: TSANLogRecordText);
begin
  FStorage.IOCommand(TSANLogIOPartialWriteContext.Create(Self, FPartialWriteRec, APartialMessage));
  FPartialWriteRec.Text := FPartialWriteRec.Text + APartialMessage;
end;

procedure TSANLogCustom.DoEndWrite;
begin
  FStorage.IOCommand(TSANLogIOEndWriteContext.Create(Self, FPartialWriteRec));
end;

procedure TSANLogCustom.DoRead(const ALogRecords: ISANLogRecords;
  const AFilter: ISANLogRecordsFilter);
begin
  FStorage.IOCommand(TSANLogIOReadContext.Create(Self, ALogRecords, AFilter));
end;

function TSANLogCustom.GetAsynchronous: WordBool;
begin
  Result := DoGetAsynchronous;
end;

function TSANLogCustom.GetLocation: ISANLogLocation;
begin
  FLocationCS.Enter;
  try
    Result := FLocation;
  finally
    FLocationCS.Leave;
  end;
end;

function TSANLogCustom.GetName: TSANLogName;
begin
  Result := FName;
end;

function TSANLogCustom.IsEquals(const ALog: ISANLog): WordBool;
begin
  DoIsEquals(ALog, Result);
end;

function TSANLogCustom.MakeLogRecord: ISANLogRecord;
begin
  Result := (Controller as ISANLoggerMaker).MakeLogRecord;
end;

function TSANLogCustom.MakeLogsRecords: ISANLogRecords;
begin
  Result := (Controller as ISANLoggerMaker).MakeLogsRecords;
end;

procedure TSANLogCustom.NotifyAllDestroying;
var
  Cx : ISANCoreNotificationContextSafe;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANCoreSimpleNotificationContext.Create(Self);

    for i := 0 to FNotifiers.Count - 1 do
      ISANCoreNotifierSafe(FNotifiers[i]).Destroying(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogCustom.NotifyAllRecordWrited(const ARec: ISANLogRecord);
var
  Cx : ISANLogRecordsOneRecordContext;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogRecordsOneRecordContext.Create(Self, ARec);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogNotifier(FNotifiers[i]).RecordWrited(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogCustom.Read(const ALogRecords: ISANLogRecords;
  AFilter: ISANLogRecordsFilter);
begin
  DoRead(ALogRecords, AFilter);
end;

function TSANLogCustom.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLog,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

procedure TSANLogCustom.SetAsynchronous(const AAsynchronous: WordBool);
begin
  DoSetAsynchronous(AAsynchronous);
end;

procedure TSANLogCustom.SetLocation(const ALocation: ISANLogLocation);
begin
  FLocationCS.Enter;
  try

    FStorage.ValidateLocation(ALocation);

    DoSetLocation(FLocation, ALocation);

  finally
    FLocationCS.Leave;
  end;
end;

function TSANLogCustom.Write(const AKind: TSANLogRecordKind;
  const ASubType: TSANLogRecordSubType; const AText: TSANLogRecordText;
  ADateTime: TDateTime): TSANLogRecordID;
var
  Rec : ISANLogRecord;
begin

  if CompareValue(ADateTime, 0, OneMillisecond) = 0 then
    ADateTime := Now;

  Rec := MakeLogRecord.From(0, ADateTime, AKind, ASubType, AText);

  DoWrite(Rec);
end;

procedure TSANLogCustom.WriteRecord(const ALogRecord: ISANLogRecord);
begin
  DoWrite(ALogRecord);
end;

procedure TSANLogCustom.WriteRecords(const ALogRecords: ISANLogRecords;
  const AReWrite: WordBool);
begin
  DoWriteRecords(ALogRecords, AReWrite);
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogsCustomStorageIOThread'}
constructor TSANLogsCustomStorageIOThread.Create(const AStorage: TSANLogsCustomStorage);
begin
  FStorage := AStorage;

  inherited Create(False);
end;

procedure TSANLogsCustomStorageIOThread.Execute;
begin
  FStorage.IOThreadExecute;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANLogsCustomStorage'}
function TSANLogsCustomStorage.AddLog(const AName: TSANLogName;
  const ALocation: ISANLogLocation): ISANLog;
begin

  if TryFindByName(AName, Result) then
    raise ESANLogCustomError.CreateResFmt(@sSANLoggerLogWithNameAlreadyExistsFmt, [AName]);

  Result := DoCreateLog(AName);
  Result.Location := ALocation;
  FLogs.Add(Result);
end;

function TSANLogsCustomStorage.AddNotifier(
  const ANotifier: ISANLogsStorageNotifier): TSANCoreNotifierHandle;
begin
  FNotifiers.Add(ANotifier);
  Result := TSANCoreNotifierHandle(ANotifier);
end;

function TSANLogsCustomStorage.RemoveNotifier(
  const ANotifierHandle: TSANCoreNotifierHandle): WordBool;
begin
  Result := FNotifiers.Remove(IInterface(ANotifierHandle)) > -1;
end;

function TSANLogsCustomStorage.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANLogsStorage,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

function TSANLogsCustomStorage.CheckLocation(const ALocation: ISANLogLocation;
  out AErrors: TSANLogRecordText): WordBool;
var
  MHP : ISANCoreModuleHandleProviderSafe;
begin
  MHP := ALocation as ISANCoreModuleHandleProviderSafe;

  if MHP.ModuleHandle <> Self.GetModuleHandle then
  begin
    Result := False;

    AErrors := Format
    (
      LoadResString(@sSANLoggerLocationFromOtherModuleFmt),
      [MHP.ModuleHandle, Self.GetModuleHandle]
    );
  end

  else
    DoCheckLocation(ALocation, AErrors, Result);
end;

constructor TSANLogsCustomStorage.Create(const AController: ISANLogger;
  const AName: TSANLogName);
begin
  inherited Create(AController);

  FName := AName;

  FLogs       := TInterfaceList.Create;
  FNotifiers  := TInterfaceList.Create;

  FMsgWnd := AllocateHWnd(MessagesDispatch);

  FIOThread := TSANLogsCustomStorageIOThread.Create(Self);

  FCommandQueue  := TInterfaceList.Create;
end;

procedure TSANLogsCustomStorage.BeforeDestruction;
begin
  NotifyAllDestroying;

  inherited;
end;

destructor TSANLogsCustomStorage.Destroy;
begin
  TerminateIOThread;
  FIOThread.WaitFor;
  FreeAndNil(FIOThread);

  DeallocateHWnd(FMsgWnd);

  inherited;
end;

function TSANLogsCustomStorage.CreateLocation: ISANLogLocation;
begin
  Result := DoCreateLocation;
end;

procedure TSANLogsCustomStorage.IOThreadExecute;
begin
  try

    IOThreadInit;
    try

      IOThreadLoop;

    finally
      IOThreadFinal;
    end;

  except
    PostMessage(FMsgWnd, CM_IO_THREAD_EXCEPTION, 0, 0);
    raise;
  end;
end;

procedure TSANLogsCustomStorage.IOThreadInit;
var
  i : integer;
begin

  for i := Low(FIOEvents) to High(FIOEvents) do
  begin
    FIOEvents[i] := CreateEvent(nil, True, False, nil);
    if FIOEvents[i] = 0 then
      RaiseLastOSError;
  end;

end;

procedure TSANLogsCustomStorage.IOThreadFinal;
var
  i : integer;
begin
  for i := Low(FIOEvents) to High(FIOEvents) do
    CloseHandle(FIOEvents[i]);
end;

procedure TSANLogsCustomStorage.IOThreadLoop;
var
  WtRes : Cardinal;
begin

  repeat
    WtRes := WaitForMultipleObjects(Length(FIOEvents), @FIOEvents, False, 100);

    case WtRes of
      WAIT_OBJECT_0 + 0:
        Break;

      WAIT_OBJECT_0 + 1:
        IOThreadCommandQueue;

      WAIT_TIMEOUT:
        Sleep(1);

    else
      RaiseLastOSError;
    end;

  until TSANLogsCustomStorageIOThread(IOThread).Terminated;

end;

procedure TSANLogsCustomStorage.IOThreadCommand(
  const AIOContext: ISANLogIOContext);
begin
  try

    DoIOThreadCommand(AIOContext);

  finally

    if AIOContext.Log.Asynchronous then
      PostMessage(FMsgWnd, CM_IO_THREAD_ASYNC_EVENT, NativeInt(AIOContext), 0);

  end;
end;

procedure TSANLogsCustomStorage.IOThreadCommandQueue;
var
  i : integer;
  Cx : ISANLogIOContext;
begin
  FCommandQueue.Lock;
  try

    for I := 0 to FCommandQueue.Count - 1 do
    begin
      Cx := ISANLogIOContext(FCommandQueue[I]);

      // Пропускаем события, которые уже отработали, но остались в очереди.
      case WaitForSingleObject(Cx.CompletedEvent, 0) of
        WAIT_OBJECT_0:
          Continue;

        WAIT_TIMEOUT:
          IOThreadCommand(Cx);

      else
        RaiseLastOSError;
      end;

    end;

  finally
    FCommandQueue.Unlock;
  end;
end;

procedure TSANLogsCustomStorage.TerminateIOThread;
begin
  IOThread.Terminate;
  SetEvent(FIOEvents[0]);
end;

procedure TSANLogsCustomStorage.DoIsEquals(const AOther: IInterface;
  out AIsEquals: WordBool);
var
  OtherStorage : ISANLogsStorage;
  MHP : ISANCoreModuleHandleProvider;
begin
  AIsEquals := Supports(AOther, ISANLogsStorage, OtherStorage)
           and SameText(OtherStorage.Name, Name)
           and Supports(AOther, ISANCoreModuleHandleProvider, MHP)
           and (GetModuleHandle = MHP.ModuleHandle);
end;

function TSANLogsCustomStorage.GetLogByName(const AName: TSANLogName): ISANLog;
begin
  if TryFindByName(AName, Result) then
    raise ESANLogCustomError.CreateResFmt(@sSANLoggerLogWithNameNotFoundFmt, [AName]);
end;

function TSANLogsCustomStorage.GetLogs(const Index: integer): ISANLog;
begin
  Result := ISANLog(FLogs[Index]);
end;

function TSANLogsCustomStorage.GetLogsCount: integer;
begin
  Result := FLogs.Count;
end;

function TSANLogsCustomStorage.GetModuleHandle: HMODULE;
begin
  Result := HInstance;
end;

function TSANLogsCustomStorage.GetModuleHandleSafe: HMODULE;
begin
  Result := GetModuleHandle;
end;

function TSANLogsCustomStorage.GetName: TSANLogName;
begin
  Result := FName;
end;

procedure TSANLogsCustomStorage.IOCommand(const AContext : ISANLogIOContext);

  procedure AddCommand;
  begin
    FCommandQueue.Add(AContext);
    SetEvent(FIOEvents[1]);
  end;

  function OuterTermination : boolean;
  begin
    Result := (Controller as ITermination).OuterTerminated;
  end;
var
  WtRes : Cardinal;
begin

  AddCommand;

  if AContext.Log.Asynchronous then
    Exit;

  try

    repeat

      WtRes := WaitForSingleObject(AContext.CompletedEvent, 100);
      case WtRes of
        WAIT_OBJECT_0:
          Break;

        WAIT_TIMEOUT:
          Sleep(1);
      else
        RaiseLastOSError;
      end;

    until OuterTermination;

    IONotify(AContext);

  finally
    FCommandQueue.Remove(AContext);
  end;

end;

procedure TSANLogsCustomStorage.IONotify(const AContext: ISANLogIOContext);
var
  LCN   : ISANLogCustomNotification;
  WRC   : ISANLogIOCustomWriteRecordContext;
  WRsC  : ISANLogIOWriteRecordsContext;
  i     : integer;
begin
  LCN := AContext.Log as ISANLogCustomNotification;

  if Supports(AContext, ISANLogIOWriteRecordContext, WRC)
  or Supports(AContext, ISANLogIOEndWriteContext, WRC)
  then
    LCN.NotifyAllRecordWrited(WRC.Rec)

  else
  if Supports(AContext, ISANLogIOWriteRecordsContext, WRsC) then
  begin

    WRsC.Recs.Lock;
    try

      for i := 0 to WRsC.Recs.Count - 1 do
        LCN.NotifyAllRecordWrited(WRsC.Recs[i]);

    finally
      WRsC.Recs.UnLock;
    end;

  end;

end;

procedure TSANLogsCustomStorage.IOThreadAsyncEvent(var AMessage: TMessage);
var
  Cx : ISANLogIOContext;
begin
  Cx := ISANLogIOContext(AMessage.WParam);
  try

    IONotify(Cx);

  finally
    FCommandQueue.Remove(Cx);
  end;
end;

procedure TSANLogsCustomStorage.IOThreadException(var AMessage: TMessage);
var
  IOTE : Exception;
begin
  IOThread.WaitFor;
  Assert(Assigned(IOThread.FatalException));

  IOTE := IOThread.FatalException as Exception;

  raise ExceptClass(IOTE.ClassType).Create(IOTE.ToString);
end;

function TSANLogsCustomStorage.IsEquals(const AStorage: ISANLogsStorage): WordBool;
begin
  DoIsEquals(AStorage, Result);
end;

procedure TSANLogsCustomStorage.Lock;
begin
  FLogs.Lock;
end;

function TSANLogsCustomStorage.MakeLogRecord: ISANLogRecord;
begin
  Result := (Controller as ISANLoggerMaker).MakeLogRecord;
end;

function TSANLogsCustomStorage.MakeLogsRecords: ISANLogRecords;
begin
  Result := (Controller as ISANLoggerMaker).MakeLogsRecords;
end;

procedure TSANLogsCustomStorage.MessagesDispatch(var AMessage: TMessage);
begin
  Dispatch(AMessage);
end;

procedure TSANLogsCustomStorage.NotifyAllDestroying;
var
  Cx : ISANCoreNotificationContextSafe;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANCoreSimpleNotificationContext.Create(Self);

    for i := 0 to FNotifiers.Count - 1 do
      ISANCoreNotifierSafe(FNotifiers[i]).Destroying(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogsCustomStorage.NotifyAllLogAdded(const ALog: ISANLog);
var
  Cx : ISANLogsStorageOneLogContext;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogsStorageOneLogContext.Create(Self, ALog);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogsStorageNotifier(FNotifiers[i]).LogAdded(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

procedure TSANLogsCustomStorage.NotifyAllLogRemoving(const ALog: ISANLog);
var
  Cx : ISANLogsStorageOneLogContext;
  i : integer;
begin
  FNotifiers.Lock;
  try

    if FNotifiers.Count = 0 then
      Exit;

    Cx := TSANLogsStorageOneLogContext.Create(Self, ALog);

    for i := 0 to FNotifiers.Count - 1 do
      ISANLogsStorageNotifier(FNotifiers[i]).LogRemoving(Cx);

  finally
    FNotifiers.Unlock;
  end;
end;

function TSANLogsCustomStorage.RemoveLog(const AName: TSANLogName): WordBool;
var
  Log : ISANLog;
  i : integer;
begin
  FLogs.Lock;
  try

    Result := TryFindByName(AName, Log, @i);

    if Result then
    begin
      NotifyAllLogRemoving(Log);
      FLogs.Delete(i);
    end;

  finally
    FLogs.Unlock;
  end;
end;

function TSANLogsCustomStorage.TryFindByName(const AName: TSANLogName;
  out ALog: ISANLog; const AIndexPtr: PInteger): WordBool;
var
  i : integer;
begin

  FLogs.Lock;
  try

    for i := 0 to LogsCount - 1 do
      if WideSameText(Logs[i].Name, AName) then
      begin
        ALog := Logs[i];

        if Assigned(AIndexPtr) then
          AIndexPtr^ := i;

        Exit(True);
      end;

  finally
    FLogs.Unlock;
  end;

  ALog := nil;
  Result := False;
end;

procedure TSANLogsCustomStorage.UnLock;
begin
  FLogs.Unlock;
end;

procedure TSANLogsCustomStorage.ValidateLocation(const ALocation: ISANLogLocation);
var
  IsCorrect : WordBool;
  Errors : WideString;
begin
  DoCheckLocation(ALocation, Errors, IsCorrect);

  if not IsCorrect then
    raise ESANLogCustomError.CreateResFmt(@sSANLoggerInvalidLocationFmt, [Errors]);
end;
{$ENDREGION}
//==============================================================================



end.
