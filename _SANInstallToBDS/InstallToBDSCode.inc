function ProcessIsRunS(const AProcessName : PAnsiChar) : boolean; 
external 'ProcessIsRun@{tmp}\ProcessUtils.dll stdcall delayload setuponly';

function ProcessIsRunU(const AProcessName : PAnsiChar) : boolean; 
external 'ProcessIsRun@{app}\..\DLL\uninstall.dll stdcall delayload uninstallonly';

function ProcessIsRun(const AProcessName : PAnsiChar) : boolean;
begin
  if IsUninstaller then
    Result := ProcessIsRunU(AProcessName)
  else  
    Result := ProcessIsRunS(AProcessName)
end;

const
  BDS_KNOWN_PACKAGES_KEY  = 'Software\Embarcadero\BDS\8.0\Known Packages';
  SAN_KEY                 = 'Software\SAN'; 
  SAN_FOR_BDS_KEY         = SAN_KEY + '\ForBDS'; 

  MESSAGE_BDS_IS_RUNNED       = 'Среда разработки DBS запущена. Закройте ее, для продолжения.'#13#10'Повторить проверку?';
  MESSAGE_SAN_PKG_REQUIRED    = 'Для установки пакета необходим набор пакетов: '#13#10;
  MESSAGE_SAN_PKG_USED        = 'Пакет используется другими пакетами, продолжить удаление?';
  MESSAGE_SAN_PKG_USED_ERROR  = 'Ошибка чтения информации из реестра. Возможно пакет используется другими пакетами, продолжить удаление?';
  
function CheckBDSRuned : boolean;
begin

  while ProcessIsRun('bds.exe') do
    if MsgBox(MESSAGE_BDS_IS_RUNNED, mbConfirmation, MB_YESNO) = IDNO then
    begin
      Result := False;
      Exit;   
    end;
     
  Result := True;
end;

function BPLDir : string;
begin
  Result := ExtractFileDir(ExpandConstant('{app}')) + '\BPL';
end;

function DLLDir : string;
begin
  Result := ExtractFileDir(ExpandConstant('{app}')) + '\DLL';
end;

function CheckBDSRunedForSetup : boolean;
begin
  ExtractTemporaryFile('ProcessUtils.dll');
  
  Result := CheckBDSRuned;  
  
  UnloadDLL(ExpandConstant('{tmp}\ProcessUtils.dll'));
end;

function CheckBDSRunedForUninstall : boolean;
begin
  Result := CheckBDSRuned;  
  UnloadDLL(DLLDir + '\uninstall.dll');
end;

function RegisterBDSPackage(const ABPLName, ABPLDesc : string) : boolean;
begin
  Result := RegWriteStringValue(HKEY_CURRENT_USER, BDS_KNOWN_PACKAGES_KEY, BPLDir + '\' + ExpandConstant(ABPLName), ExpandConstant(ABPLDesc));
end;
 
function UnRegisterBDSPackage(const ABPLName : string) : boolean;
begin
  Result := RegDeleteValue(HKEY_CURRENT_USER, BDS_KNOWN_PACKAGES_KEY, BPLDir + '\' + ExpandConstant(ABPLName));
end;

function SANPackageCanInstall(ARequreds : string) : boolean;
var
  vReqStrs : TStringList; 
  i : integer;
begin
  ARequreds := ExpandConstant(ARequreds);
  
  if ARequreds = '' then
  begin
    Result := True;
    Exit;
  end;
  
  vReqStrs := TStringList.Create;
  try
    StringChangeEx(ARequreds, ',', #13#10, True);  
    vReqStrs.Text := ARequreds;

    i := 0;
    while i < vReqStrs.Count do
      if RegValueExists(HKEY_CURRENT_USER, SAN_FOR_BDS_KEY, vReqStrs[i]) then
        vReqStrs.Delete(i)
      else
        Inc(i);      

    Result := vReqStrs.Count = 0;
    
    if not Result then
      MsgBox(MESSAGE_SAN_PKG_REQUIRED + vReqStrs.Text, mbError, MB_OK);
      
  finally
    vReqStrs.Free; 
  end;
  
end;

function SANPackageCanUnInstall(APkgName : string) : boolean;
var
  S : string;
  i, j : integer;
  vRegErrors : boolean;
  vNames: TArrayOfString;
  vReqStrs : TStringList; 
begin
  Result := True;
  
  APkgName := ExpandConstant(APkgName);
  
  // Не удалось прочитать или нет значений. Странно, но разрешаем удаление.
  if not RegGetValueNames(HKEY_CURRENT_USER, SAN_FOR_BDS_KEY, vNames)
  or (GetArrayLength(vNames) = 0)
  then 
    Exit;
  
  vRegErrors := False;
  
  vReqStrs := TStringList.Create;
  try
  
    for i := 0 to GetArrayLength(vNames) - 1 do
      if not RegQueryStringValue(HKEY_CURRENT_USER, SAN_FOR_BDS_KEY, vNames[i], S) then
        vRegErrors := True
      else        
        begin
          StringChangeEx(S, ',', #13#10, True);  
          vReqStrs.Text := S;
        
          Result := not vReqStrs.Find(APkgName, j);
          if not Result then
            Break;
        end;
  
  finally
    vReqStrs.Free; 
  end;
  
  
  if not Result then
    begin
      if (MsgBox(MESSAGE_SAN_PKG_USED, mbError, MB_YESNO + MB_DEFBUTTON2) = IDYES) then
        Result := True;
    end
  else
    if vRegErrors then
      Result := MsgBox(MESSAGE_SAN_PKG_USED_ERROR, mbError, MB_YESNO + MB_DEFBUTTON2) = IDYES;
end;

function SANPackageRegister(APkgName, ARequreds : string) : boolean;
begin
  APkgName   := ExpandConstant(APkgName);
  ARequreds  := ExpandConstant(ARequreds);

  Result := RegWriteStringValue(HKEY_CURRENT_USER, SAN_FOR_BDS_KEY, APkgName, ARequreds); 
end;

function SANPackageUnRegister(APkgName : string) : boolean;
begin
  APkgName   := ExpandConstant(APkgName);
  RegDeleteValue(HKEY_CURRENT_USER, SAN_FOR_BDS_KEY, APkgName);

  RegDeleteKeyIfEmpty(HKEY_CURRENT_USER, SAN_FOR_BDS_KEY);  
  RegDeleteKeyIfEmpty(HKEY_CURRENT_USER, SAN_KEY);  
end;

procedure DeleteDirs();
var
  vAppDir : string;
begin
  RemoveDir(DLLDir);
  RemoveDir(BPLDir);

  vAppDir := ExtractFileDir(ExpandConstant('{app}'));
  
  while RemoveDir(vAppDir) do
  begin  
    vAppDir := ExtractFileDir(vAppDir);
    Sleep(1000);
  end;  
   
end;