function InitializeSetup(): Boolean;
begin
  Result := CheckBDSRunedForSetup and SANPackageCanInstall('{#BPLRequireds}');  
end;

function InitializeUninstall(): Boolean;
begin
  Result := CheckBDSRunedForUninstall and SANPackageCanUnInstall('{#BPLName}');
end;

procedure CurStepChanged(CurStep: TSetupStep); 
begin
  if CurStep = ssPostInstall then
  begin
    RegisterBDSPackage('{#BPLName}', '{#BPLDesc}');
    SANPackageRegister('{#BPLName}', '{#BPLRequireds}');
  end;       
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  case CurUninstallStep of
    
    usUninstall:
      begin
        UnRegisterBDSPackage('{#BPLName}');
        SANPackageUnRegister('{#BPLName}');
      end;  
    
    usPostUninstall:
      DeleteDirs;
        
  end;    
end;
