{*******************************************************}
{                                                       }
{       SANDesignReg                                    }
{       Отображение логотипа пакета при загрузке IDE.   }
{       Связь с настоящим ToolsAPI.                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignReg;
{$I NX.INC}
INTERFACE

procedure Register;

IMPLEMENTATION

USES
  Windows,
  SANDesign_ToolsAPI,
  ToolsAPI;

procedure Register;
begin
//  SANDesign_ToolsAPI.BorlandIDEServices := ToolsAPI.BorlandIDEServices;
//  SANDesign_ToolsAPI.LibraryWizardProc  := ToolsAPI.LibraryWizardProc;

  ToolsAPI.SplashScreenServices.AddPluginBitmap
  (
    'SAN Design Utils Library',
    LoadBitmap(hInstance, 'SAN_DESIGN_LOGO'),
    False,
    '',
    {$I SANDesignUtilsBPL.textversion}
  );

end;

initialization
  SANDesign_ToolsAPI.InitSANToolsAPI(ToolsAPI.BorlandIDEServices, ToolsAPI.LibraryWizardProc);

finalization
  SANDesign_ToolsAPI.FinalSANToolsAPI;

END.
