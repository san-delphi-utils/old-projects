{*******************************************************}
{                                                       }
{       SANDesignCreaters                               }
{       Классы создания модулей IDE.                    }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignCreaters;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI, SANDesign_DesignIntf,
  Windows, SysUtils, Classes,
  SANDesignReplacer;

CONST
  STATIC_CREATOR_EXISTING     = $0001;
  STATIC_CREATOR_UNNAMED      = $0002;
  STATIC_CREATOR_MAIN_FORM    = $0004;
  STATIC_CREATOR_SHOW_FORM    = $0008;
  STATIC_CREATOR_SHOW_SOURCE  = $0010;

  STATIC_CREATOR_DEFAULT_FLAGS = STATIC_CREATOR_SHOW_FORM
                              or STATIC_CREATOR_SHOW_SOURCE
                              or STATIC_CREATOR_UNNAMED;

TYPE
  /// <summary>
  /// Абстрактный класс создания навого модуля в IDE.
  /// Функция класса CreateModule позволяет сразу создать модуль IDE.
  /// </summary>
  TSANCustomModuleCreator = class(TNotifierObject, IOTACreator, IOTAModuleCreator)
  public
    type TCreatorEnumType = (ctUnit, ctForm, ctText);

  protected
    // IOTACreator
    function GetCreatorType: string; dynamic;
    // Return False if this is a new module
    function GetExisting: Boolean; virtual;
    // Return the File system IDString that this module uses for reading/writing
    function GetFileSystem: string; virtual;
    // Return the Owning module, if one exists (for a project module, this would
    //  be a project; for a project this is a project group)
    function GetOwner: IOTAModule; dynamic;
    // Return true, if this item is to be marked as un-named.  This will force the
    //  save as dialog to appear the first time the user saves.
    function GetUnnamed: Boolean; virtual;

    // IOTAModuleCreator
    function GetAncestorName: string; virtual;
    function GetImplFileName: string; virtual;
    function GetIntfFileName: string; virtual;
    function GetFormName: string; virtual;
    function GetMainForm: Boolean; virtual;
    function GetShowForm: Boolean; virtual;
    function GetShowSource: Boolean; virtual;
    function NewFormFile(const FormIdent, AncestorIdent: string): IOTAFile; virtual;
    function NewImplSource(const ModuleIdent, FormIdent, AncestorIdent: string): IOTAFile; virtual;
    function NewIntfSource(const ModuleIdent, FormIdent, AncestorIdent: string): IOTAFile; virtual;
    procedure FormCreated(const FormEditor: IOTAFormEditor); dynamic;

    function CreatorEnumType : TCreatorEnumType; virtual; abstract;

  public
    constructor Create; virtual;

    class function CreateModule : IOTAModule;
  end;

  /// <summary>
  /// Класс статического создания навого модуля в IDE.
  /// Все необходимые параметры передаются в конструктор в качестве параметров.
  /// Функция класса CreateModule позволяет сразу создать модуль IDE,
  /// передав параметры конструктора в качестве аргументов.
  /// </summary>
  TSANStaticModuleCreator = class(TSANCustomModuleCreator)
  private
    FReplacer : TSANReplacer;
    FCreatorType : TSANCustomModuleCreator.TCreatorEnumType;
    FFlags : integer;
    FNewFormFile_Source,  FNewImplSource_Source, FFormName, FFileName, FAncestorName  : string;
    FNewFormFile_Age,     FNewImplSource_Age     : TDateTime;

  protected
    function DoNewFileOrSource(ASource : string; const AFormIdent, AAncestorIdent: string;
      const AAge : TDateTime; const AModuleIdent : string = ''): IOTAFile;

    function GetExisting: Boolean; override;
    function GetUnnamed: Boolean; override;
    function GetAncestorName: string; override;
    function GetImplFileName: string; override;
    function GetFormName: string; override;
    function GetMainForm: Boolean; override;
    function GetShowForm: Boolean; override;
    function GetShowSource: Boolean; override;

    function NewFormFile(const FormIdent, AncestorIdent: string ): IOTAFile; override;
    function NewImplSource(const ModuleIdent, FormIdent, AncestorIdent: string): IOTAFile; override;

    function CreatorEnumType : TSANCustomModuleCreator.TCreatorEnumType; override;

  public
    constructor Create(const ACreatorType : TSANCustomModuleCreator.TCreatorEnumType;
      const ABaseName, AAncestorName, AFormNameFormat, AUnitNameFormat,
      ANewFormFile_Source, ANewImplSource_Source : string;
      const AReplacer : TSANReplacer = nil;
      const AFlags : integer = STATIC_CREATOR_DEFAULT_FLAGS;
      const ANewFormFile_Age : TDateTime = -1; const ANewImplSource_Age : TDateTime = -1); reintroduce; virtual;

    class function CreateModule(const ACreatorType : TSANCustomModuleCreator.TCreatorEnumType;
      const ABaseName, AAncestorName, AFormNameFormat, AUnitNameFormat,
      ANewFormFile_Source, ANewImplSource_Source : string;
      const AReplacer : TSANReplacer = nil;
      const AFlags : integer = STATIC_CREATOR_DEFAULT_FLAGS;
      const ANewFormFile_Age : TDateTime = -1; const ANewImplSource_Age : TDateTime = -1) : IOTAModule;
  end;


  /// <summary>
  /// Абстрактный класс создания навого проекта.
  /// Функция класса CreateProject позволяет сразу создать модуль IDE.
  /// </summary>
  TSANCustomProjectCreator = class(TNotifierObject, IOTACreator, IOTAProjectCreator,
    IOTAProjectCreator50, IOTAProjectCreator80)
  public
    type TCreatorEnumType = (ctApplication, ctLibrary, ctConsole, ctPackage, ctStaticLibrary, ctOptionSet);

  protected
    // IOTACreator
    function GetCreatorType: string; dynamic;
    // Return False if this is a new module
    function GetExisting: Boolean; virtual;
    // Return the File system IDString that this module uses for reading/writing
    function GetFileSystem: string; virtual;
    // Return the Owning module, if one exists (for a project module, this would
    //  be a project; for a project this is a project group)
    function GetOwner: IOTAModule; dynamic;
    // Return true, if this item is to be marked as un-named.  This will force the
    //  save as dialog to appear the first time the user saves.
    function GetUnnamed: Boolean; virtual;

    // IOTAProjectCreator
    { Return the project filename. NOTE: This *must* be a fully qualified file name. }
    function GetFileName: string; virtual; abstract;
    { Deprecated!! Return the option file name (C++ .bpr, .bpk, etc...) }
    function GetOptionFileName: string; dynamic;
    { Return True to show the source }
    function GetShowSource: Boolean; virtual;
    { Deprecated!! Called to create a new default module for this project.
      Please implement and use the method on IOTAProjectCreator50. }
    procedure NewDefaultModule; dynamic;
    { Deprecated!! Create and return the project option source. (C++) }
    function NewOptionSource(const ProjectName: string): IOTAFile; dynamic;
    { Called to indicate when to create/modify the project resource file }
    procedure NewProjectResource(const Project: IOTAProject); virtual;
    { Create and return the Project source file }
    function NewProjectSource(const ProjectName: string): IOTAFile; virtual; abstract;

    // IOTAProjectCreator50
    { Called to create a new default module(s) for the given project.  This
      interface method is the preferred mechanism. }
    procedure NewDefaultProjectModule(const Project: IOTAProject); virtual;

    // IOTAProjectCreator80
    { Implement this interface and return the correct personality of the project
      to create.  The CreatorType function should return any sub-types that this
      personality can create.  For instance, in the Delphi.Personality, returning
      'Package' from CreatorType will create a proper package project. }
    function GetProjectPersonality: string; dynamic;

    function CreatorEnumType : TCreatorEnumType; virtual; abstract;

  public
    constructor Create; virtual;

    class function CreateProject : IOTAProject;
  end;

  /// <summary>
  /// Класс статического создания навого проекта.
  /// Все необходимые параметры передаются в конструктор в качестве параметров.
  /// Функция класса CreateProject позволяет сразу создать проект,
  /// передав параметры конструктора в качестве аргументов.
  /// </summary>
  TSANStaticProjectCreator = class(TSANCustomProjectCreator)
  private
    FReplacer : TSANReplacer;
    FCreatorType : TSANCustomProjectCreator.TCreatorEnumType;
    FFlags : integer;
    FFileName, FNewProject_Source : string;
    FNewProject_Age : TDateTime;

  protected
    function GetExisting: Boolean; override;
    function GetUnnamed: Boolean; override;
    function GetFileName: string; override;
    function GetShowSource: Boolean; override;
    function NewProjectSource(const ProjectName: string): IOTAFile; override;
    function CreatorEnumType : TSANCustomProjectCreator.TCreatorEnumType; override;

  public
    constructor Create(const ACreatorType : TSANCustomProjectCreator.TCreatorEnumType;
      const AFileName, ANewProject_Source : string;
      const AReplacer : TSANReplacer = nil;
      const AFlags : integer = STATIC_CREATOR_DEFAULT_FLAGS;
      const ANewProject_Age : TDateTime = -1); reintroduce; virtual;

    class function CreateModule(const ACreatorType : TSANCustomProjectCreator.TCreatorEnumType;
      const AFileName, ANewProject_Source : string;
      const AReplacer : TSANReplacer = nil;
      const AFlags : integer = STATIC_CREATOR_DEFAULT_FLAGS;
      const ANewProject_Age : TDateTime = -1) : IOTAProject;
  end;


  /// <summary>
  /// Абстрактный класс создания навой группы проектов.
  /// Функция класса CreateProjectGroup позволяет сразу создать группу проектов.
  /// </summary>
  TSANCustomProjectGroupCreator = class(TNotifierObject, IOTACreator, IOTAProjectGroupCreator)
  protected
    // IOTACreator
    function GetCreatorType: string; dynamic;
    // Return False if this is a new module
    function GetExisting: Boolean; virtual;
    // Return the File system IDString that this module uses for reading/writing
    function GetFileSystem: string; virtual;
    // Return the Owning module, if one exists (for a project module, this would
    //  be a project; for a project this is a project group)
    function GetOwner: IOTAModule; dynamic;
    // Return true, if this item is to be marked as un-named.  This will force the
    //  save as dialog to appear the first time the user saves.
    function GetUnnamed: Boolean; virtual;

    // IOTAProjectGroupCreator
    { Return the project group file name }
    function GetFileName: string; virtual; abstract;
    { Return True to show the source }
    function GetShowSource: Boolean; virtual;
    { Deprecated/never called.  Create and return the project group source }
    function NewProjectGroupSource(const ProjectGroupName: string): IOTAFile; dynamic;

  public
    constructor Create; virtual;

    class function CreateProjectGroup : IOTAProjectGroup;
  end;

  /// <summary>
  /// Класс статического создания группы проектов.
  /// Все необходимые параметры передаются в конструктор в качестве параметров.
  /// Функция класса CreateProjectGroup позволяет сразу создать группу проект,
  /// передав параметры конструктора в качестве аргументов.
  /// </summary>
  TSANStaticProjectGroupCreator = class(TSANCustomProjectGroupCreator)
  private
    FFlags : integer;
    FFileName : string;

  protected
    function GetExisting: Boolean; override;
    function GetUnnamed: Boolean; override;
    function GetFileName: string; override;
    function GetShowSource: Boolean; override;

  public
    constructor Create(const AFileName : string; const AFlags : integer = STATIC_CREATOR_DEFAULT_FLAGS); reintroduce; virtual;

    class function CreateProjectGroup(const AFileName : string;
      const AFlags : integer = STATIC_CREATOR_DEFAULT_FLAGS) : IOTAProjectGroup;
  end;

IMPLEMENTATION

//==============================================================================
// TSANCustomModuleCreator
// protected
function TSANCustomModuleCreator.GetCreatorType: string;
begin
  case CreatorEnumType of
    ctUnit:  Result := sUnit;
    ctForm:  Result := sForm;
    else     Result := sText;
  end;
end;

function TSANCustomModuleCreator.GetExisting: Boolean;
begin
  Result := False;
end;

function TSANCustomModuleCreator.GetFileSystem: string;
begin
  Result := '';
end;

function TSANCustomModuleCreator.GetOwner: IOTAModule;
var
  vModuleServices: IOTAModuleServices;
  vModule, vNewModule: IOTAModule;
begin
  // You may prefer to return the project group's ActiveProject instead
  Result := nil;
  vModuleServices := (BorlandIDEServices as IOTAModuleServices);
  vModule := vModuleServices.CurrentModule;

  if Assigned(vModule) then
  begin

    if vModule.QueryInterface(IOTAProject, vNewModule) = S_OK then
      Result := vNewModule

    else
    if vModule.OwnerModuleCount > 0 then
    begin
      vNewModule := vModule.OwnerModules[0];
      if vNewModule <> nil then
        if vNewModule.QueryInterface(IOTAProject, Result) <> S_OK then
          Result := nil;
    end;

  end;

end;

function TSANCustomModuleCreator.GetUnnamed: Boolean;
begin
  Result := True;
end;

function TSANCustomModuleCreator.GetAncestorName: string;
begin
  Result := '';
end;

function TSANCustomModuleCreator.GetFormName: string;
begin
  Result := '';
end;

function TSANCustomModuleCreator.GetImplFileName: string;
begin
  Result := '';
end;

function TSANCustomModuleCreator.GetIntfFileName: string;
begin
  Result := '';
end;

function TSANCustomModuleCreator.GetMainForm: Boolean;
begin
  Result := False;
end;

function TSANCustomModuleCreator.GetShowForm: Boolean;
begin
  Result := True;
end;

function TSANCustomModuleCreator.GetShowSource: Boolean;
begin
  Result := True;
end;

function TSANCustomModuleCreator.NewFormFile(const FormIdent, AncestorIdent: string): IOTAFile;
begin
  Result := nil;
end;

function TSANCustomModuleCreator.NewImplSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  Result := nil;
end;

function TSANCustomModuleCreator.NewIntfSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  Result := nil;
end;

procedure TSANCustomModuleCreator.FormCreated(const FormEditor: IOTAFormEditor);
begin
end;

// public
constructor TSANCustomModuleCreator.Create;
begin
  inherited Create;
end;

class function TSANCustomModuleCreator.CreateModule: IOTAModule;
begin
  Result := (BorlandIDEServices as IOTAModuleServices).CreateModule
  (
    Self.Create
  );
end;
//==============================================================================

//==============================================================================
// TSANStaticModuleCreator
// protected
function TSANStaticModuleCreator.DoNewFileOrSource(ASource : string;
  const AFormIdent, AAncestorIdent: string; const AAge : TDateTime;
  const AModuleIdent : string): IOTAFile;
var
  vReplacer : TSANReplacer;
begin

  if ASource = '' then
    Exit;

  vReplacer := TSANReplacer.Create;
  try

    if Assigned(FReplacer) then
      vReplacer.Assign(FReplacer);

    vReplacer.InsertPair(0, 'AncestorIdent',  AAncestorIdent);
    vReplacer.InsertPair(0, 'FormIdent',      AFormIdent);
    vReplacer.InsertPair(0, 'ModuleIdent',    AModuleIdent);

    ASource := vReplacer.Replace(ASource);

    Result := TOTAFile.Create(ASource, AAge);
  finally
    FreeAndNil(vReplacer);
  end;

end;

function TSANStaticModuleCreator.GetExisting: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_EXISTING = STATIC_CREATOR_EXISTING;
end;

function TSANStaticModuleCreator.GetUnnamed: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_UNNAMED = STATIC_CREATOR_UNNAMED;
end;

function TSANStaticModuleCreator.GetAncestorName: string;
begin
  Result := FAncestorName;
end;

function TSANStaticModuleCreator.GetImplFileName: string;
begin
  Result := FFileName;
end;

function TSANStaticModuleCreator.GetFormName: string;
begin
  Result := FFormName;
end;

function TSANStaticModuleCreator.GetMainForm: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_MAIN_FORM = STATIC_CREATOR_MAIN_FORM;
end;

function TSANStaticModuleCreator.GetShowForm: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_SHOW_FORM = STATIC_CREATOR_SHOW_FORM;
end;

function TSANStaticModuleCreator.GetShowSource: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_SHOW_SOURCE = STATIC_CREATOR_SHOW_SOURCE;
end;

function TSANStaticModuleCreator.NewFormFile(const FormIdent, AncestorIdent: string): IOTAFile;
begin
//  ShowMessageFmt('%s %s %s', ['NewFormFile', FormIdent, AncestorIdent]);
  Result := DoNewFileOrSource(FNewFormFile_Source, FormIdent, AncestorIdent, FNewFormFile_Age);
end;

function TSANStaticModuleCreator.NewImplSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
//  ShowMessageFmt('%s %s %s %s', ['NewImplSource', ModuleIdent, FormIdent, AncestorIdent]);
  Result := DoNewFileOrSource(FNewImplSource_Source, FormIdent, AncestorIdent, FNewImplSource_Age, ModuleIdent);
end;

function TSANStaticModuleCreator.CreatorEnumType: TSANCustomModuleCreator.TCreatorEnumType;
begin
  Result := FCreatorType;
end;

// public
constructor TSANStaticModuleCreator.Create(
  const ACreatorType : TSANCustomModuleCreator.TCreatorEnumType;
  const ABaseName, AAncestorName, AFormNameFormat, AUnitNameFormat,
    ANewFormFile_Source, ANewImplSource_Source : string;
  const AReplacer : TSANReplacer;
  const AFlags : integer;
  const ANewFormFile_Age, ANewImplSource_Age : TDateTime);
var
  vUnitIdent, vClassName, vFileName : string;
begin
  inherited Create;

  FReplacer := AReplacer;

  FCreatorType := ACreatorType;

  (BorlandIDEServices as IOTAModuleServices).GetNewModuleAndClassName
  (
    ABaseName, vUnitIdent, vClassName, vFileName
  );

  FFormName := Format(AFormNameFormat, [ABaseName]);
  FFileName := ExtractFilePath(vFileName) + Format(AUnitNameFormat, [ABaseName]) + '.pas';

  FAncestorName := AAncestorName;

  FNewFormFile_Source    := ANewFormFile_Source;
  FNewImplSource_Source  := ANewImplSource_Source;
  FNewFormFile_Age       := ANewFormFile_Age;
  FNewImplSource_Age     := ANewImplSource_Age;
end;

class function TSANStaticModuleCreator.CreateModule(
  const ACreatorType : TSANCustomModuleCreator.TCreatorEnumType;
  const ABaseName, AAncestorName, AFormNameFormat, AUnitNameFormat,
    ANewFormFile_Source, ANewImplSource_Source : string;
  const AReplacer : TSANReplacer;
  const AFlags : integer;
  const ANewFormFile_Age, ANewImplSource_Age : TDateTime): IOTAModule;
var
  vCreater : TSANStaticModuleCreator;
begin
  vCreater := Self.Create
  (
    ACreatorType,
    ABaseName,
    AAncestorName,
    AFormNameFormat,
    AUnitNameFormat,
    ANewFormFile_Source,
    ANewImplSource_Source,
    AReplacer,
    AFlags,
    ANewFormFile_Age,
    ANewImplSource_Age
  );

  Result := (BorlandIDEServices as IOTAModuleServices).CreateModule(vCreater);
end;
//==============================================================================

//==============================================================================
// TSANCustomProjectCreator
// protected
function TSANCustomProjectCreator.GetCreatorType: string;
begin
  case CreatorEnumType of
    ctApplication:    Result := sApplication;
    ctLibrary:        Result := sLibrary;
    ctConsole:        Result := sConsole;
    ctPackage:        Result := sPackage;
    ctStaticLibrary:  Result := sStaticLibrary;
    ctOptionSet:      Result := sOptionSet;
  end;
end;

function TSANCustomProjectCreator.GetExisting: Boolean;
begin
  Result := False;
end;

function TSANCustomProjectCreator.GetFileSystem: string;
begin
  Result := '';
end;

function TSANCustomProjectCreator.GetOwner: IOTAModule;
var
  vModuleServices: IOTAModuleServices;
  vModule, vNewModule: IOTAModule;
begin
  // You may prefer to return the project group's ActiveProject instead
  Result := nil;
  vModuleServices := (BorlandIDEServices as IOTAModuleServices);
  vModule := vModuleServices.CurrentModule;

  if Assigned(vModule) then
  begin

    if vModule.QueryInterface(IOTAProjectGroup, vNewModule) = S_OK then
      Result := vNewModule

    else
    if vModule.OwnerModuleCount > 0 then
    begin
      vNewModule := vModule.OwnerModules[0];
      if vNewModule <> nil then
        if vNewModule.QueryInterface(IOTAProjectGroup, Result) <> S_OK then
          Result := nil;
    end;

  end;

end;

function TSANCustomProjectCreator.GetUnnamed: Boolean;
begin
  Result := True;
end;

function TSANCustomProjectCreator.GetOptionFileName: string;
begin
  Result := '';
end;

function TSANCustomProjectCreator.GetShowSource: Boolean;
begin
  Result := True;
end;

procedure TSANCustomProjectCreator.NewDefaultModule;
begin
end;

function TSANCustomProjectCreator.NewOptionSource(const ProjectName: string): IOTAFile;
begin
end;

procedure TSANCustomProjectCreator.NewProjectResource(const Project: IOTAProject);
begin
end;

procedure TSANCustomProjectCreator.NewDefaultProjectModule(const Project: IOTAProject);
begin
end;

function TSANCustomProjectCreator.GetProjectPersonality: string;
begin
  Result := sDelphiPersonality;
end;

// public
constructor TSANCustomProjectCreator.Create;
begin
  inherited Create;
end;

class function TSANCustomProjectCreator.CreateProject: IOTAProject;
begin
  Result := (BorlandIDEServices as IOTAModuleServices).CreateModule
  (
    Self.Create
  )
  as IOTAProject;
end;
//==============================================================================

//==============================================================================
// TSANStaticProjectCreator
// protected
function TSANStaticProjectCreator.GetExisting: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_EXISTING = STATIC_CREATOR_EXISTING;
end;

function TSANStaticProjectCreator.GetUnnamed: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_UNNAMED = STATIC_CREATOR_UNNAMED;
end;

function TSANStaticProjectCreator.GetFileName: string;
begin
  Result := FFileName;
end;

function TSANStaticProjectCreator.GetShowSource: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_SHOW_SOURCE = STATIC_CREATOR_SHOW_SOURCE;
end;

function TSANStaticProjectCreator.NewProjectSource(const ProjectName: string): IOTAFile;
var
  vReplacer : TSANReplacer;
  vSource : string;
begin

  if FNewProject_Source = '' then
    Exit;

  vReplacer := TSANReplacer.Create;
  try

    if Assigned(FReplacer) then
      vReplacer.Assign(FReplacer);

    vReplacer.InsertPair(0, 'ProjectName',  ProjectName);

    vSource := vReplacer.Replace(FNewProject_Source);

    Result := TOTAFile.Create(vSource, FNewProject_Age);
  finally
    FreeAndNil(vReplacer);
  end;
end;

function TSANStaticProjectCreator.CreatorEnumType: TSANCustomProjectCreator.TCreatorEnumType;
begin
  Result := FCreatorType;
end;

constructor TSANStaticProjectCreator.Create(
  const ACreatorType: TSANCustomProjectCreator.TCreatorEnumType;
  const AFileName, ANewProject_Source: string; const AReplacer: TSANReplacer;
  const AFlags: integer; const ANewProject_Age: TDateTime);
begin
  inherited Create;

  FReplacer           := AReplacer;
  FCreatorType        := ACreatorType;
  FFileName           := AFileName;
  FNewProject_Source  := ANewProject_Source;
  FFlags              := AFlags;
  FNewProject_Age     := ANewProject_Age;
end;

class function TSANStaticProjectCreator.CreateModule(
  const ACreatorType: TSANCustomProjectCreator.TCreatorEnumType;
  const AFileName, ANewProject_Source: string; const AReplacer: TSANReplacer;
  const AFlags: integer; const ANewProject_Age: TDateTime): IOTAProject;
var
  vCreater : TSANStaticProjectCreator;
begin
  vCreater := Self.Create
  (
    ACreatorType,
    AFileName,
    ANewProject_Source,
    AReplacer,
    AFlags,
    ANewProject_Age
  );

  Result := (BorlandIDEServices as IOTAModuleServices).CreateModule
  (
    vCreater
  )
  as IOTAProject;

end;
//==============================================================================

//==============================================================================
// TSANCustomProjectGroupCreator
// protected
function TSANCustomProjectGroupCreator.GetCreatorType: string;
begin
  Result := '';
end;

function TSANCustomProjectGroupCreator.GetExisting: Boolean;
begin
  Result := False;
end;

function TSANCustomProjectGroupCreator.GetFileSystem: string;
begin
  Result := '';
end;

function TSANCustomProjectGroupCreator.GetOwner: IOTAModule;
begin
  Result := nil;
end;

function TSANCustomProjectGroupCreator.GetUnnamed: Boolean;
begin
  Result := True;
end;

function TSANCustomProjectGroupCreator.GetShowSource: Boolean;
begin
  Result := False;
end;

function TSANCustomProjectGroupCreator.NewProjectGroupSource(const ProjectGroupName: string): IOTAFile;
begin
end;

// public
constructor TSANCustomProjectGroupCreator.Create;
begin
  inherited Create;
end;

class function TSANCustomProjectGroupCreator.CreateProjectGroup: IOTAProjectGroup;
begin
  Result := (BorlandIDEServices as IOTAModuleServices).CreateModule
  (
    Self.Create
  )
  as IOTAProjectGroup;
end;
//==============================================================================

//==============================================================================
// TSANStaticProjectGroupCreator
// protected
function TSANStaticProjectGroupCreator.GetExisting: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_EXISTING = STATIC_CREATOR_EXISTING;
end;

function TSANStaticProjectGroupCreator.GetUnnamed: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_UNNAMED = STATIC_CREATOR_UNNAMED;
end;

function TSANStaticProjectGroupCreator.GetFileName: string;
begin
  Result := FFileName;
end;

function TSANStaticProjectGroupCreator.GetShowSource: Boolean;
begin
  Result := FFlags AND STATIC_CREATOR_SHOW_SOURCE = STATIC_CREATOR_SHOW_SOURCE;
end;

// public
constructor TSANStaticProjectGroupCreator.Create(const AFileName: string;
  const AFlags: integer);
begin
  inherited Create;

  FFileName  := AFileName;
  FFlags     := AFlags;
end;

class function TSANStaticProjectGroupCreator.CreateProjectGroup(
  const AFileName: string; const AFlags: integer): IOTAProjectGroup;
var
  vCreater : TSANStaticProjectGroupCreator;
begin
  vCreater := Self.Create(AFileName, AFlags);

  Result := (BorlandIDEServices as IOTAModuleServices).CreateModule
  (
    vCreater
  )
  as IOTAProjectGroup;
end;
//==============================================================================

END.
