{*******************************************************}
{                                                       }
{       SANDesignEvaluater                              }
{       Класс получения значений выражений              }
{       от дебаггера.                                   }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignEvaluater;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  SysUtils, Classes;

TYPE
  EEvaluaterError = class(Exception);

  /// <summary>
  /// Класс получения значений выражений от дебаггера.
  /// </summary>
  TSANEvaluater = class(TInterfacedPersistent, IOTAThreadNotifier)
  private
    FCompleted, FDeferredError: Boolean;
    FDeferredResult: string;

  protected
    // IOTANotifier
    procedure AfterSave;
    procedure BeforeSave;
    procedure Destroyed;
    procedure Modified;

    // IOTAThreadNotifier
    procedure ThreadNotify(Reason: TOTANotifyReason);
    procedure EvaluteComplete(const ExprStr, ResultStr: string; CanModify: Boolean;
      ResultAddress, ResultSize: LongWord; ReturnCode: Integer);
    procedure ModifyComplete(const ExprStr, ResultStr: string; ReturnCode: Integer);

  public
    function Evaluate(const AExpression: string): string;
    function EvaluateUnQuoted(const AExpression: string): string;
    function EvaluateFmt(const AFmtExpression: string; const AArgs : array of const): string;
    function EvaluateFmtUnQuoted(const AFmtExpression: string; const AArgs : array of const): string;

  end;

IMPLEMENTATION

resourcestring
  sErrorMessage = 'Evaluate fail for Expression "%s", result is "%s"';

//==============================================================================
// TSANEvaluater
// protected
procedure TSANEvaluater.AfterSave;
begin
end;

procedure TSANEvaluater.BeforeSave;
begin
end;

procedure TSANEvaluater.Destroyed;
begin
end;

procedure TSANEvaluater.Modified;
begin
end;

procedure TSANEvaluater.ThreadNotify(Reason: TOTANotifyReason);
begin
end;

procedure TSANEvaluater.EvaluteComplete(const ExprStr, ResultStr: string;
  CanModify: Boolean; ResultAddress, ResultSize: LongWord; ReturnCode: Integer);
begin
  FCompleted := True;
  FDeferredResult := ResultStr;
  FDeferredError := ReturnCode <> 0;
end;

procedure TSANEvaluater.ModifyComplete(const ExprStr, ResultStr: string;
  ReturnCode: Integer);
begin
end;

// public
function TSANEvaluater.Evaluate(const AExpression: string): string;
var
  vCurProcess: IOTAProcess;
  vCurThread: IOTAThread;
  vResultStr: array[0..4095] of Char;
  vCanModify: Boolean;
  vResultAddr, ResultSize, ResultVal: LongWord;
  vEvalRes: TOTAEvaluateResult;
  vDebugSvcs: IOTADebuggerServices;
  vNotifierIndex : integer;
begin
  Result := '';

  if Supports(BorlandIDEServices, IOTADebuggerServices, vDebugSvcs) then
    vCurProcess := vDebugSvcs.CurrentProcess;

  if not Assigned(vCurProcess) then
    Exit;

  vCurThread := vCurProcess.CurrentThread;
  if not Assigned(vCurThread) then
    Exit;

  vEvalRes := vCurThread.Evaluate
  (
    AExpression,
    @vResultStr,
    Length(vResultStr),
    vCanModify,
    eseAll,
    '',
    vResultAddr,
    ResultSize,
    ResultVal,
    '',
    0
  );

  case vEvalRes of
    erOK:
      Result := vResultStr;

    erDeferred:
      begin
        FCompleted := False;
        FDeferredResult := '';
        FDeferredError := False;

        vNotifierIndex := vCurThread.AddNotifier(Self);
        try

          while not FCompleted do
            vDebugSvcs.ProcessDebugEvents;

        finally
          vCurThread.RemoveNotifier(vNotifierIndex);
        end;

        if not FDeferredError then
        begin
          if FDeferredResult <> '' then
            Result := FDeferredResult
          else
            Result := vResultStr;
        end;
      end;

    erBusy:
      begin
        vDebugSvcs.ProcessDebugEvents;
        Result := Evaluate(AExpression);
      end;

    else
      raise EEvaluaterError.CreateResFmt(@sErrorMessage, [AExpression, vResultStr]);

  end;//case

end;

function TSANEvaluater.EvaluateUnQuoted(const AExpression: string): string;
var
  L : integer;
begin
  Result := Evaluate(AExpression);

  L := Length(Result);

  if L >= 2 then
    Result := Copy(Result, 2, L - 2);
end;

function TSANEvaluater.EvaluateFmt(const AFmtExpression: string;
  const AArgs: array of const): string;
begin
  Result := Evaluate
  (
    Format(AFmtExpression, AArgs)
  );
end;

function TSANEvaluater.EvaluateFmtUnQuoted(const AFmtExpression: string;
  const AArgs: array of const): string;
var
  L : integer;
begin
  Result := EvaluateFmt(AFmtExpression, AArgs);

  L := Length(Result);

  if L >= 2 then
    Result := Copy(Result, 2, L - 2);
end;
//==============================================================================

END.
