{*******************************************************}
{                                                       }
{       SANDesignGUIDDialog                             }
{       Диалог редактирования GUID.                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignGUIDDialog;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, StdCtrls, ComObj, ActiveX, Mask;

TYPE
  TfrmSANDesignGUIDDialog = class(TForm)
    lblGUID: TLabel;
    sbtnGUID: TSpeedButton;
    shpGUID: TShape;
    medtGUID: TMaskEdit;
    pnlButtons: TPanel;
    btnCancel: TBitBtn;
    btnOK: TBitBtn;
    pnlCustomData: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure sbtnGUIDClick(Sender: TObject);
    procedure edtGUIDChange(Sender: TObject);
    procedure edtGUIDKeyPress(Sender: TObject; var Key: Char);

  private
    FFlagGUID : boolean;

  protected
    procedure SetWarnShape(const AShape : TShape; var AFlag : boolean;
      const AValidateCondition : boolean; const AWarningHint : string);

  public

  end;

function SANGUIDDialog(const ACaption : TCaption; var AGUIDStr : string) : boolean;

IMPLEMENTATION

resourcestring
  sIncorrectGUIDWarningMessage = 'Введен не корректный GUID';

{$R *.dfm}

function SANGUIDDialog(const ACaption : TCaption; var AGUIDStr : string) : boolean;
begin
  with TfrmSANDesignGUIDDialog.Create(nil) do
  try
    Caption := ACaption;

    medtGUID.Text := AGUIDStr;

    Result := (ShowModal = mrOk);

    if Result then
      AGUIDStr := medtGUID.Text;

  finally
    Free
  end;
end;

procedure TfrmSANDesignGUIDDialog.SetWarnShape(const AShape: TShape; var AFlag: boolean;
  const AValidateCondition: boolean; const AWarningHint: string);
begin
  AFlag := AValidateCondition;

  with AShape do
    if AValidateCondition then
      begin
        Brush.Color := clLime;
        Hint := '';
      end
    else
      begin
        Brush.Color := clRed;
        Hint := AWarningHint;
      end;

  btnOK.Enabled := FFlagGUID;
end;

procedure TfrmSANDesignGUIDDialog.FormCreate(Sender: TObject);
begin
  FFlagGUID := True;
end;

procedure TfrmSANDesignGUIDDialog.sbtnGUIDClick(Sender: TObject);
var
  vGUID : TGUID;
begin
  OleCheck(CoCreateGuid(vGUID));

  medtGUID.Text := GUIDToString(vGUID);
end;

procedure TfrmSANDesignGUIDDialog.edtGUIDChange(Sender: TObject);
var
  vGUID : TGUID;
  vHR : HRESULT;
begin
  vHR := CLSIDFromString
  (
    PWideChar
    (
      WideString
      (
        Trim
        (
          (Sender as TMaskEdit).Text
        )
      )
    ),
    vGUID
  );

  SetWarnShape(shpGUID, FFlagGUID, vHR = S_OK, LoadResString(@sIncorrectGUIDWarningMessage));
end;

procedure TfrmSANDesignGUIDDialog.edtGUIDKeyPress(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', 'A'..'F', 'a'..'f', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)]) then
    Key := #0;
end;

END.
