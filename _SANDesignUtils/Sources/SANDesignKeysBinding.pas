{*******************************************************}
{                                                       }
{       SANDesignKeysBinding                            }
{       Классы регистрации горячих клавиш.              }
{                                                       }
{       По мотивам http://www.delphikingdom.com         }
{       /asp/viewitem.asp?catalogid=899                 }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignKeysBinding;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  SysUtils, Classes, ActnList, Dialogs;

TYPE
  /// <summary>
  /// Абстрактный класс регистрации горячей клавиши.
  /// </summary>
  TSANCustomKeyBinding = class(TNotifierObject, IOTAKeyboardBinding)
  private
    FBindIndex: integer;

  protected
    // IOTAKeyboardBinding
    function GetBindingType: TBindingType; dynamic;
    function GetDisplayName: string; virtual; abstract;
    function GetName: string; virtual; abstract;
    procedure BindKeyboard(const BindingServices: IOTAKeyBindingServices); dynamic;

    function GetShortCut: TShortCut; virtual; abstract;
    function GetKeyboard: string; virtual;
    function GetMenuItemName: string; virtual;
    procedure KeyProc(const Context: IOTAKeyContext; KeyCode: TShortCut;
      var BindingResult: TKeyBindingResult); virtual; abstract;

  public
    constructor Create;
    destructor Destroy; override;

    procedure Bind;
    procedure UnBind;

    property BindIndex: integer read FBindIndex;
    property ShortCut: TShortCut  read GetShortCut;
  end;

  /// <summary>
  /// Класс регистрации горячей клавиши привязанный к действию TAction.
  /// </summary>
  TSANKeyBindingAction = class(TSANCustomKeyBinding)
  private
    FAction : TAction;

  protected
    function GetDisplayName: string; override;
    function GetName: string; override;

    function GetShortCut: TShortCut; override;
    function GetKeyboard: string; override;
    function GetMenuItemName: string; override;
    procedure KeyProc(const Context: IOTAKeyContext; KeyCode: TShortCut;
      var BindingResult: TKeyBindingResult); override;

  public
    constructor Create(const AAction : TAction);

    property Action : TAction  read FAction;
  end;

/// <summary>
/// Отменить регистрацию всех горячих клавиш.
/// </summary>
procedure SANKeysBindingUnBindAll;

/// <summary>
/// Зарегистрировать горячую клавишу для действия TAction.
/// </summary>
/// <param name="AAction">
/// Действие, для которого задается горячая клавиша.
/// </param>
procedure SANKeysBindingAddAction(const AAction : TAction);

/// <summary>
/// Отмена регистрации горячей клавиши для действия TAction.
/// </summary>
/// <param name="AAction">
/// Действие, для которого отменяется горячая клавиша.
/// </param>
/// <returns>
/// Boolean. True, если успешно.
/// </returns>
function SANKeysBindingRemoveAction(const AAction : TAction) : boolean;

IMPLEMENTATION

VAR
  SANKeysBindingList : TList;

//==============================================================================
procedure SANKeysBindingUnBindAll;
var
  i : integer;
begin
  for i := SANKeysBindingList.Count - 1 downto 0 do
    TSANCustomKeyBinding(SANKeysBindingList[i]).UnBind;

  if SANKeysBindingList.Count > 0 then
    ShowMessageFmt('SANDesignKeysBinding.SANKeysBindingUnBindAll: Неочищено - %d!', [SANKeysBindingList.Count]);
end;

procedure SANKeysBindingAddAction(const AAction : TAction);
begin
  TSANKeyBindingAction.Create(AAction);
end;

function SANKeysBindingRemoveAction(const AAction : TAction) : boolean;
var
  P : Pointer;
begin

  for P in SANKeysBindingList do
    if  (TObject(P) is TSANKeyBindingAction)
    and (CompareText(TSANKeyBindingAction(P).Action.Name, AAction.Name) = 0)
    then
    begin
      TSANCustomKeyBinding(P).UnBind;
      Exit(True);
    end;

  Result := False;
end;
//==============================================================================

//==============================================================================
// TSANCustomKeyBinding
// protected
function TSANCustomKeyBinding.GetBindingType: TBindingType;
begin
  Result := btPartial;
end;

procedure TSANCustomKeyBinding.BindKeyboard(const BindingServices: IOTAKeyBindingServices);
begin
  BindingServices.AddKeyBinding([ShortCut], KeyProc,
    nil, kfImplicitShift, GetKeyboard, GetMenuItemName)
end;

function TSANCustomKeyBinding.GetKeyboard: string;
begin
  Result := '';
end;

function TSANCustomKeyBinding.GetMenuItemName: string;
begin
  Result := '';
end;

// public
constructor TSANCustomKeyBinding.Create;
begin
  inherited Create;

  SANKeysBindingList.Add(Self);
end;

destructor TSANCustomKeyBinding.Destroy;
begin
  SANKeysBindingList.Extract(Self);

  inherited;
end;

procedure TSANCustomKeyBinding.Bind;
begin
  FBindIndex := (BorlandIDEServices as IOTAKeyboardServices).AddKeyboardBinding(Self);
end;

procedure TSANCustomKeyBinding.UnBind;
begin
  (BorlandIDEServices as IOTAKeyboardServices).RemoveKeyboardBinding(FBindIndex);
end;
//==============================================================================

//==============================================================================
// TSANKeyBindingAction
// protected
function TSANKeyBindingAction.GetDisplayName: string;
begin
  Result := FAction.Caption;
end;

function TSANKeyBindingAction.GetName: string;
begin
  Result := Format('%s.%s', [GetKeyboard, FAction.Name]);
end;

function TSANKeyBindingAction.GetShortCut: TShortCut;
begin
  Result := FAction.ShortCut;
end;

function TSANKeyBindingAction.GetKeyboard: string;
begin
  Result := ClassName;
end;

function TSANKeyBindingAction.GetMenuItemName: string;
begin
  Result := FAction.Name;
end;

procedure TSANKeyBindingAction.KeyProc(const Context: IOTAKeyContext;
  KeyCode: TShortCut; var BindingResult: TKeyBindingResult);
begin
  if FAction.Execute then
    BindingResult := krHandled;
end;

// public
constructor TSANKeyBindingAction.Create(const AAction: TAction);
begin
  inherited Create;

  FAction := AAction;
end;
//==============================================================================

INITIALIZATION
  SANKeysBindingList := TList.Create;

FINALIZATION
  SANKeysBindingUnBindAll;
  FreeAndNil(SANKeysBindingList);

END.
