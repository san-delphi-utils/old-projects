{*******************************************************}
{                                                       }
{       SANDesignVisualisersAddInOptionsFrame           }
{       Управление настройками визуалайзеров.           }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignVisualisersAddInOptionsFrame;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ActnList, Math,
  SANDesignVisualisers;

TYPE
  TframeSANDesignVisualisersAddInOptions = class(TFrame)
    lblVisualisers: TLabel;
    lblSupportedTypes: TLabel;
    lstVis: TListBox;
    lstSupTypes: TListBox;
    pnlVis: TPanel;
    pnlSupTypes: TPanel;
    splVis: TSplitter;
    edtSupType: TEdit;
    pnlSupTypeButtons: TPanel;
    pnlVisButtons: TPanel;
    btnExtOptions: TBitBtn;
    btnAdd: TBitBtn;
    btnReplace: TBitBtn;
    btnDelete: TBitBtn;
    btnMoveUp: TBitBtn;
    btnMoveDown: TBitBtn;
    actlst: TActionList;
    actExtOptions: TAction;
    actAdd: TAction;
    actReplace: TAction;
    actDelete: TAction;
    actMoveUp: TAction;
    actMoveDown: TAction;
    procedure lstVisClick(Sender: TObject);
    procedure lstSupTypesClick(Sender: TObject);
    procedure actExtOptionsExecute(Sender: TObject);
    procedure actExtOptionsUpdate(Sender: TObject);
    procedure actAddExecute(Sender: TObject);
    procedure actAddUpdate(Sender: TObject);
    procedure actReplaceExecute(Sender: TObject);
    procedure actReplaceUpdate(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure actDeleteUpdate(Sender: TObject);
    procedure actMoveUpExecute(Sender: TObject);
    procedure actMoveUpUpdate(Sender: TObject);
    procedure actMoveDownExecute(Sender: TObject);
    procedure actMoveDownUpdate(Sender: TObject);

  private
    function GetCurVis: TSANCustomVisualiser;

  protected
    procedure SaveSupTypes;

  public
    procedure UpdateSupTypes;
    procedure UpdateVisList;

    property CurVis : TSANCustomVisualiser  read GetCurVis;
  end;

IMPLEMENTATION

{$R *.dfm}

type
  TSANCustomVisualiserHack = class(TSANCustomVisualiser);

//==============================================================================
// TframeSANDesignVisualisersAddInOptions
// private
function TframeSANDesignVisualisersAddInOptions.GetCurVis: TSANCustomVisualiser;
begin
  with lstVis do
    if ItemIndex < 0 then
      Result := nil
    else
      Result := TSANCustomVisualiser(Items.Objects[ItemIndex]);
end;

// protected
procedure TframeSANDesignVisualisersAddInOptions.SaveSupTypes;
begin
  TSANCustomVisualiserHack(CurVis).OptionsTypes.Assign(lstSupTypes.Items);
end;

// public
procedure TframeSANDesignVisualisersAddInOptions.UpdateSupTypes;
var
  vVis : TSANCustomVisualiserHack;
begin
  vVis := TSANCustomVisualiserHack(CurVis);

  if not Assigned(vVis) then
    Exit;

  lstSupTypes.Items.Assign(vVis.OptionsTypes);

  lstSupTypes.ItemIndex := MIN(0, lstSupTypes.Items.Count);

  lstSupTypes.OnClick(lstSupTypes);
end;

procedure TframeSANDesignVisualisersAddInOptions.UpdateVisList;
var
  vSL : TStrings;
  i : integer;
  vVis : TSANCustomVisualiserHack;
begin
  vSL := lstVis.Items;

  vSL.BeginUpdate;
  try
    vSL.Clear;

    for i := 0 to VisualisersCount - 1 do
    begin
      vVis := TSANCustomVisualiserHack(Visualisers(i));

      if vVis.ShowInSANDesignVisualisersAddIn then
        vSL.AddObject( vVis.GetVisualizerName, vVis);
    end;

  finally
    vSL.EndUpdate;
  end;

  lstVis.ItemIndex := MIN(0, vSL.Count);
  UpdateSupTypes;

  edtSupType.Enabled := vSL.Count > 0;
end;

// published
procedure TframeSANDesignVisualisersAddInOptions.lstVisClick(Sender: TObject);
begin
  UpdateSupTypes;
end;

procedure TframeSANDesignVisualisersAddInOptions.lstSupTypesClick(Sender: TObject);
begin
  with Sender as TListBox do
    edtSupType.Text := Items[ItemIndex];
end;

procedure TframeSANDesignVisualisersAddInOptions.actExtOptionsExecute(
  Sender: TObject);
begin
  (CurVis as ISANVisualiserExtOptions).ExtOptionsExecute;
end;

procedure TframeSANDesignVisualisersAddInOptions.actExtOptionsUpdate(
  Sender: TObject);
var
  vExtOptions : ISANVisualiserExtOptions;
begin
  (Sender as TAction).Enabled := Supports(CurVis, ISANVisualiserExtOptions, vExtOptions);
end;

procedure TframeSANDesignVisualisersAddInOptions.actAddExecute(Sender: TObject);
begin
  with lstSupTypes do
    ItemIndex := Items.Add(Trim(edtSupType.Text));

  SaveSupTypes;
end;

procedure TframeSANDesignVisualisersAddInOptions.actAddUpdate(Sender: TObject);
var
  S : string;
begin
  S := Trim(edtSupType.Text);

  (Sender as TAction).Enabled := (lstVis.ItemIndex >= 0)
                             and (S <> '')
                             and (lstSupTypes.Items.IndexOf(S) < 0);
end;

procedure TframeSANDesignVisualisersAddInOptions.actReplaceExecute(
  Sender: TObject);
begin
  with lstSupTypes do
    Items[ItemIndex] := Trim(edtSupType.Text);

  SaveSupTypes;
end;

procedure TframeSANDesignVisualisersAddInOptions.actReplaceUpdate(
  Sender: TObject);
var
  S : string;
begin
  S := Trim(edtSupType.Text);

  (Sender as TAction).Enabled := (S <> '')
                             and (lstSupTypes.ItemIndex >= 0)
                             and (CompareStr(lstSupTypes.Items[lstSupTypes.ItemIndex], S) <> 0);
end;

procedure TframeSANDesignVisualisersAddInOptions.actDeleteExecute(
  Sender: TObject);
var
  vIndex : integer;
begin

  with lstSupTypes do
  begin
    vIndex := ItemIndex;
    Items.Delete(vIndex);
    ItemIndex := MIN(vIndex, Items.Count - 1);
  end;

  SaveSupTypes;
end;

procedure TframeSANDesignVisualisersAddInOptions.actDeleteUpdate(
  Sender: TObject);
begin
  (Sender as TAction).Enabled := (lstSupTypes.ItemIndex >= 0);
end;

procedure TframeSANDesignVisualisersAddInOptions.actMoveUpExecute(
  Sender: TObject);
begin
  with lstSupTypes do
    Items.Move(ItemIndex, ItemIndex - 1);

  SaveSupTypes;
end;

procedure TframeSANDesignVisualisersAddInOptions.actMoveUpUpdate(
  Sender: TObject);
begin
  (Sender as TAction).Enabled := (lstSupTypes.ItemIndex > 0);
end;

procedure TframeSANDesignVisualisersAddInOptions.actMoveDownExecute(
  Sender: TObject);
begin
  with lstSupTypes do
    Items.Move(ItemIndex, ItemIndex + 1);

  SaveSupTypes;
end;

procedure TframeSANDesignVisualisersAddInOptions.actMoveDownUpdate(
  Sender: TObject);
begin
  (Sender as TAction).Enabled := (lstSupTypes.ItemIndex < lstSupTypes.Items.Count - 1);
end;
//==============================================================================

END.
