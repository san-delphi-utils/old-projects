object frameSANDesignGroupAddInOptions: TframeSANDesignGroupAddInOptions
  Left = 0
  Top = 0
  Width = 320
  Height = 240
  TabOrder = 0
  object lblTitle: TLabel
    Left = 16
    Top = 16
    Width = 48
    Height = 19
    Caption = 'lblTitle'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblCaption: TLabel
    Left = 16
    Top = 40
    Width = 167
    Height = 13
    Caption = 'Please select an option on the left.'
  end
end
