{*******************************************************}
{                                                       }
{       SANDesignGroupAddInOptionsFrame                 }
{       Управление настройками IDE.                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignGroupAddInOptionsFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

TYPE
  TframeSANDesignGroupAddInOptions = class(TFrame)
    lblTitle: TLabel;
    lblCaption: TLabel;

  private
    function GetTitle: string;
    procedure SetTitle(const Value: string);

  public
    property Title : string  read GetTitle  write SetTitle;
  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeSANDesignGroupAddInOptions
// private
function TframeSANDesignGroupAddInOptions.GetTitle: string;
begin
  Result := lblTitle.Caption;
end;

procedure TframeSANDesignGroupAddInOptions.SetTitle(const Value: string);
begin
  lblTitle.Caption := Value;
end;
//==============================================================================

END.
