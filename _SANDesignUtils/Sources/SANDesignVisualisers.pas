{*******************************************************}
{                                                       }
{       SANDesignVisualisers                            }
{       Базовые классы для создания визуалайзеров.      }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignVisualisers;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  Windows, Messages, SysUtils, Classes, Controls, Forms,
  SANDesignEvaluater, SANDesignDockableForm;

CONST
  CM_REFRESH_VISUALIZER_NEXT_STEP = WM_USER + 1;

TYPE
  ESANVisualiserError = class(Exception);

  /// <summary>
  /// Состояния визуалайзера.
  /// </summary>
  TAvailableState = (asAvailable, asProcRunning, asOutOfScope);

  // Интерфейс дополнительнх опций визуалайзера в настройках IDE.
  ISANVisualiserExtOptions = interface(IInterface)
    ['{16D14FAC-95C1-4CD7-AB75-28B3680C6AC1}']

    /// <summary>
    /// Действие при вызове редактора дополнительных опций.
    /// </summary>
    procedure ExtOptionsExecute;
  end;

  /// <summary>
  /// Абстрактный класс визуалайзера. Регистрация происходит в AfterConstruction.
  /// Поддерживаемые типы собраны в список строк SupportedTypes,
  /// который может быть загружен из файла (входной параметр конструктора
  /// ASupportedTypesFileName), а так же дополнен в конструкторах наследников.
  /// </summary>
  TSANCustomVisualiser = class(TInterfacedObject, IOTADebuggerVisualizer)
  private
    FSupportedTypes, FOptionsTypes : TStrings;
    FFileName : TFileName;

    procedure SupportedTypesChanged(Sender : TObject);

  protected
    //IOTADebuggerVisualizer
    procedure GetSupportedType(Index: Integer; var TypeName: String; var AllDescendents: Boolean);
    function GetSupportedTypeCount: Integer;
    function GetVisualizerDescription: String; virtual; abstract;
    function GetVisualizerIdentifier: String; virtual; abstract;
    function GetVisualizerName: String; virtual; abstract;

    function ShowInSANDesignVisualisersAddIn : boolean; virtual;

    procedure SaveOptionsTypes;

    /// <summary>
    /// Временное хранилище для настройки.
    /// </summary>
    property OptionsTypes : TStrings  read FOptionsTypes;

  public
    constructor Create(const ASupportedTypesFileName : TFileName);
    procedure AfterConstruction; override;
    destructor Destroy; override;

    property SupportedTypes : TStrings  read FSupportedTypes;
  end;

  /// <summary>
  /// Абстрактный класс визуалайзера замены значения.
  /// </summary>
  TSANCustomVisValueReplacer = class(TSANCustomVisualiser, IOTADebuggerVisualizer)
  protected
    // IOTADebuggerVisualizerValueReplacer
    function GetReplacementValue(const Expression, TypeName, EvalResult: string): string; virtual; abstract;
  end;

  /// <summary>
  /// Абстрактный класс фрейма отображения изменений визуалайзера внешнего просмотра.
  /// </summary>
  TSANVisExternalViewerUpdaterFrame = class(TFrame, IOTADebuggerVisualizerExternalViewerUpdater)
  private
    FClosedProc: TOTAVisualizerClosedProcedure;
    FAvailableState : TAvailableState;
    FEvaluater : TSANEvaluater;
    FExpression, FTypeName, FEvalResult: String;

    procedure Set_AvailableState(const Value: TAvailableState);
    function GetForm: TCustomForm;
    procedure RefreshVisualizerNextStep(var AMsg : TMessage); message CM_REFRESH_VISUALIZER_NEXT_STEP;

  protected
    // IOTADebuggerVisualizerExternalViewerUpdater
    procedure CloseVisualizer;
    procedure MarkUnavailable(Reason: TOTAVisualizerUnavailableReason);
    procedure RefreshVisualizer(const Expression, TypeName, EvalResult: String);
    procedure SetClosedCallback(ClosedProc: TOTAVisualizerClosedProcedure);

    procedure SetParent(AParent: TWinControl); override;

    /// <summary>
    /// Реакция на смену состояния визуалайзера.
    /// </summary>
    procedure DoAvailableStateChanged; virtual;
    /// <summary>
    /// Однократная реакция на обновление визуалайзера.
    /// Вызывается при RefreshVisualizer. Позволяет задать пользовательские
    /// параметры AParam1, AParam2 для передачи их в DoRefreshVisualizerStep
    /// через PostMessage.
    /// </summary>
    /// <param name="AParam1">
    /// Первый пользовательский параметр. В начале имеет значение 0.
    /// </param>
    /// <param name="AParam2">
    /// Второй пользовательский параметр. В начале имеет значение 0.
    /// </param>
    procedure DoRefreshVisualizer(var AParam1, AParam2 : integer); virtual;
    /// <summary>
    /// Многократная реакция на обновление визуалайзера. Вызывается, как минимум,
    /// один раз после DoRefreshVisualizerStep через PostMessage.
    /// Если AContinue присвоено значение True, будет вызвано еще раз через PostMessage.
    /// </summary>
    /// <param name="AParam1">
    /// Первый пользовательский параметр.
    /// </param>
    /// <param name="AParam2">
    /// Второй пользовательский параметр.
    /// </param>
    /// <param name="AContinue">
    /// Вызвать DoRefreshVisualizerStep еще раз через PostMessage.
    /// </param>
    procedure DoRefreshVisualizerStep(var AParam1, AParam2 : integer; var AContinue : boolean); virtual;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    property AvailableState : TAvailableState  read FAvailableState  write Set_AvailableState;
    property Evaluater : TSANEvaluater  read FEvaluater;
    property Form : TCustomForm  read GetForm;

    property Expression  : String  read FExpression;
    property TypeName    : String  read FTypeName;
    property EvalResult  : String  read FEvalResult;
  end;
  TSANVisExternalViewerUpdaterFrameClass = class of TSANVisExternalViewerUpdaterFrame;

  /// <summary>
  /// Абстрактный класс визуалайзера внешнего просмотра.
  /// </summary>
  TSANCustomVisExternalViewer = class(TSANCustomVisualiser)
  protected
    // IOTADebuggerVisualizerExternalViewer
    function GetMenuText: String; virtual; abstract;
    function Show(const Expression, TypeName, EvalResult: String;
      SuggestedLeft, SuggestedTop: Integer): IOTADebuggerVisualizerExternalViewerUpdater;

    function GetDockableFormClass : TSANCustomDockableFormClass; virtual;
    function GetViewerUpdaterFrameClass : TSANVisExternalViewerUpdaterFrameClass; virtual; abstract;
    function GetFormCaption(const AExpression, ATypeName, AEvalResult: String) : string; virtual; abstract;
  end;

/// <summary>
/// Отмена регистрации визуалайзера.
/// </summary>
/// <param name="AVisualiser">
/// Пременная интерфейса визуалайзера. Визуалайзер будет разрегистрирован,
/// а переменной будет присвоино значение nil.
/// </param>
procedure UnRegisterVisualiser(var AVisualiser : IOTADebuggerVisualizer);

function VisualisersCount : integer;
function Visualisers(const AIndex : integer) : TSANCustomVisualiser;

procedure Register;

IMPLEMENTATION

uses
  SANDesignAddInOptions,
  SANDesignVisualisersAddInOptionsFrame;

resourcestring
  sInvalidUnavailableReasonErrorMessage = 'Недопустимое значение UnavailableReason';

//==============================================================================
procedure UnRegisterVisualiser(var AVisualiser : IOTADebuggerVisualizer);
var
  vDebuggerServices: IOTADebuggerServices;
begin

  if not Assigned(AVisualiser) then
    Exit;

  if Supports(BorlandIDEServices, IOTADebuggerServices, vDebuggerServices) then
    vDebuggerServices.UnregisterDebugVisualizer(AVisualiser);

  AVisualiser := nil;
end;

var
  VisualisersList : TList;

function VisualisersCount : integer;
begin
  Result := VisualisersList.Count;
end;

function Visualisers(const AIndex : integer) : TSANCustomVisualiser;
begin
  Result := TSANCustomVisualiser(VisualisersList[AIndex]);
end;
//==============================================================================

//==============================================================================
// TSANCustomVisualiser
// private
procedure TSANCustomVisualiser.SupportedTypesChanged(Sender: TObject);
begin
  FOptionsTypes.Assign(Sender as TStrings);
end;

// protected
procedure TSANCustomVisualiser.GetSupportedType(Index: Integer;
  var TypeName: String; var AllDescendents: Boolean);
begin
  TypeName := FSupportedTypes[Index];

  AllDescendents := False;
end;

function TSANCustomVisualiser.GetSupportedTypeCount: Integer;
begin
  Result := FSupportedTypes.Count;
end;

function TSANCustomVisualiser.ShowInSANDesignVisualisersAddIn: boolean;
begin
  Result := True;
end;

procedure TSANCustomVisualiser.SaveOptionsTypes;
begin
  FSupportedTypes.Assign(FOptionsTypes);

  if DirectoryExists(ExtractFilePath(FFileName)) then
    FSupportedTypes.SaveToFile(FFileName);
end;

// public
constructor TSANCustomVisualiser.Create(const ASupportedTypesFileName: TFileName);
begin
  inherited Create;

  FSupportedTypes := TStringList.Create;
  TStringList(FSupportedTypes).Duplicates := dupIgnore;
  TStringList(FSupportedTypes).OnChange := SupportedTypesChanged;

  FOptionsTypes := TStringList.Create;
  TStringList(FOptionsTypes).Duplicates := dupIgnore;

  FFileName := ASupportedTypesFileName;

  if FileExists(FFileName) then
    FSupportedTypes.LoadFromFile(FFileName);

  VisualisersList.Add(Self);
end;

procedure TSANCustomVisualiser.AfterConstruction;
var
  vDebuggerServices: IOTADebuggerServices;
begin
  inherited;

  if Supports(BorlandIDEServices, IOTADebuggerServices, vDebuggerServices) then
    vDebuggerServices.RegisterDebugVisualizer(Self);
end;

destructor TSANCustomVisualiser.Destroy;
begin
  VisualisersList.Extract(Self);

  FreeAndNil(FSupportedTypes);
  FreeAndNil(FOptionsTypes);
  inherited;
end;
//==============================================================================

//==============================================================================
// TSANVisExternalViewerUpdaterFrame
// private
procedure TSANVisExternalViewerUpdaterFrame.Set_AvailableState(
  const Value: TAvailableState);
begin
  if FAvailableState = Value then
    Exit;

  FAvailableState := Value;

  DoAvailableStateChanged;
end;

function TSANVisExternalViewerUpdaterFrame.GetForm: TCustomForm;
var
  vParent : TWinControl;
begin
  vParent := Parent;

  while Assigned(vParent) do
  begin

    if vParent is TCustomForm then
    begin
      Result := vParent as TCustomForm;
      Exit
    end;

    vParent := vParent.Parent;
  end;

  Result := nil;
end;

procedure TSANVisExternalViewerUpdaterFrame.RefreshVisualizerNextStep(var AMsg: TMessage);
var
  vContinue : boolean;
begin
  vContinue := False;

  DoRefreshVisualizerStep(AMsg.WParam, AMsg.LParam, vContinue);

  if vContinue then
    PostMessage(Handle, CM_REFRESH_VISUALIZER_NEXT_STEP, AMsg.WParam, AMsg.LParam);
end;

// protected
procedure TSANVisExternalViewerUpdaterFrame.CloseVisualizer;
var
  vForm : TCustomForm;
begin
  vForm := Form;

  if Assigned(vForm) then
    vForm.Close;
end;

procedure TSANVisExternalViewerUpdaterFrame.MarkUnavailable(
  Reason: TOTAVisualizerUnavailableReason);
begin

  if Reason = ovurProcessRunning then
    AvailableState := asProcRunning
  else
    if Reason = ovurOutOfScope then
      AvailableState := asOutOfScope
    else
      raise ESANVisualiserError.CreateRes(@sInvalidUnavailableReasonErrorMessage);

end;

procedure TSANVisExternalViewerUpdaterFrame.RefreshVisualizer(const Expression,
  TypeName, EvalResult: String);
var
  vParam1, vParam2 : integer;
begin
  vParam1 := 0;
  vParam2 := 0;

  FExpression  := Expression;
  FTypeName    := TypeName;
  FEvalResult  := EvalResult;

  AvailableState := asAvailable;

  DoRefreshVisualizer(vParam1, vParam2);

  PostMessage(Handle, CM_REFRESH_VISUALIZER_NEXT_STEP, vParam1, vParam2);
end;

procedure TSANVisExternalViewerUpdaterFrame.SetClosedCallback(
  ClosedProc: TOTAVisualizerClosedProcedure);
begin
  FClosedProc := ClosedProc;
end;

procedure TSANVisExternalViewerUpdaterFrame.SetParent(AParent: TWinControl);
begin
  if (AParent = nil) and Assigned(FClosedProc) then
    FClosedProc;

  inherited;
end;

procedure TSANVisExternalViewerUpdaterFrame.DoAvailableStateChanged;
begin
end;

procedure TSANVisExternalViewerUpdaterFrame.DoRefreshVisualizer(
  var AParam1, AParam2: integer);
begin
end;

procedure TSANVisExternalViewerUpdaterFrame.DoRefreshVisualizerStep(
  var AParam1, AParam2: integer; var AContinue: boolean);
begin
end;

// public
constructor TSANVisExternalViewerUpdaterFrame.Create(AOwner: TComponent);
begin
  inherited;

  FEvaluater := TSANEvaluater.Create;
end;

destructor TSANVisExternalViewerUpdaterFrame.Destroy;
begin
  FreeAndNil(FEvaluater);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANCustomVisExternalViewer
// protected
function TSANCustomVisExternalViewer.Show(const Expression, TypeName,
  EvalResult: String; SuggestedLeft, SuggestedTop: Integer
  ) : IOTADebuggerVisualizerExternalViewerUpdater;
var
  vDockForm : TSANCustomDockableForm;
  vForm: TCustomForm;
begin

  vDockForm := GetDockableFormClass.Create
  (
    GetViewerUpdaterFrameClass,
    GetFormCaption(Expression, TypeName, EvalResult)
  );

  vForm := vDockForm.CreateDockableForm;
  vForm.Left  := SuggestedLeft;
  vForm.Top   := SuggestedTop;

  Result := vDockForm.Frame as IOTADebuggerVisualizerExternalViewerUpdater;

  Result.RefreshVisualizer(Expression, TypeName, EvalResult);
end;

function TSANCustomVisExternalViewer.GetDockableFormClass: TSANCustomDockableFormClass;
begin
  Result := TSANCustomDockableForm;
end;
//==============================================================================

//==============================================================================
TYPE
  /// <summary>
  /// Настройки визуалайзеров.
  /// </summary>
  TSANVisualisersAddInOptions = class(TSANCustomAddInOptions)
  protected
    function GetCaption: string; override;
    function GetFrameClass: TCustomFrameClass; override;
    procedure FrameCreated(AFrame: TCustomFrame); override;
    procedure DialogClosed(Accepted: Boolean); override;
  end;

// protected
function TSANVisualisersAddInOptions.GetCaption: string;
begin
  Result := 'SAN Design Utils.Visualisers';
end;

function TSANVisualisersAddInOptions.GetFrameClass: TCustomFrameClass;
begin
  Result := TframeSANDesignVisualisersAddInOptions;
end;

procedure TSANVisualisersAddInOptions.FrameCreated(AFrame: TCustomFrame);
begin
  (AFrame as TframeSANDesignVisualisersAddInOptions).UpdateVisList;
end;

procedure TSANVisualisersAddInOptions.DialogClosed(Accepted: Boolean);
var
  i : integer;
  vVis : TSANCustomVisualiser;
begin

  if Accepted then
    for i := 0 to VisualisersCount - 1 do
    begin
      vVis := Visualisers(i);

      if vVis.ShowInSANDesignVisualisersAddIn then
        vVis.SaveOptionsTypes;
    end;

end;
//==============================================================================

VAR
  vVisualisersAddInOptions : INTAAddInOptions;

procedure Register;
begin
  vVisualisersAddInOptions := TSANVisualisersAddInOptions.Create;
end;

procedure UnRegister;
begin
  UnregisterAddInOptions(vVisualisersAddInOptions);
end;


INITIALIZATION
  VisualisersList := TList.Create;

FINALIZATION
  UnRegister;

  FreeAndNil(VisualisersList);

END.
