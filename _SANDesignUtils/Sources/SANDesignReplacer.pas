{*******************************************************}
{                                                       }
{       SANDesignReplacer                               }
{       Класс замены текста модулей и форм.             }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignReplacer;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes;

TYPE
  /// <summary>
  ///  Класс замены текста модулей и форм.
  /// </summary>
  TSANReplacer = class(TPersistent)
  private
    FStrings : TStrings;
    FBegName, FEndName : string;

  protected
    procedure AssignTo(Dest : TPersistent); override;

  public
    constructor Create(const ABegName : string = '$('; const AEndName : string = ')');
    destructor Destroy; override;

    procedure AddPair(const AName, AValue : string);
    procedure InsertPair(const AIndex : integer; const AName, AValue : string);
    procedure AddPairDFM(const AName, AValue : string);
    procedure InsertPairDFM(const AIndex : integer; const AName, AValue : string);

    function Replace(const ATest : string) : string;

    property Strings : TStrings  read FStrings;
    property BegName : string    read FBegName;
    property EndName : string    read FEndName;
  end;

IMPLEMENTATION

uses
  SANDesignUtils;

//==============================================================================
// TSANReplacer
// protected
procedure TSANReplacer.AssignTo(Dest: TPersistent);
begin
  if not (Dest is TSANReplacer) then
    inherited

  else
    with Dest as TSANReplacer do
    begin
      FStrings.Assign(Self.FStrings);
      FBegName := Self.FBegName;
      FEndName := Self.FEndName;
    end;

end;

// public
constructor TSANReplacer.Create(const ABegName, AEndName: string);
begin
  inherited Create;

  FStrings := TStringList.Create;
  TStringList(FStrings).Duplicates := dupError;
  FStrings.LineBreak := 'B2E33D1DADBA4B74BC035A828116BFFD';

  FBegName := ABegName;
  FEndName := AEndName;
end;

destructor TSANReplacer.Destroy;
begin
  FreeAndNil(FStrings);
  inherited;
end;

procedure TSANReplacer.AddPair(const AName, AValue: string);
begin
  FStrings.Add(AName + FStrings.NameValueSeparator + AValue);
end;

procedure TSANReplacer.InsertPair(const AIndex: integer; const AName, AValue: string);
begin
  FStrings.Insert(AIndex, AName + FStrings.NameValueSeparator + AValue);
end;

procedure TSANReplacer.AddPairDFM(const AName, AValue: string);
begin
  FStrings.Add(AName + FStrings.NameValueSeparator + StringToDFMString(AValue));
end;

procedure TSANReplacer.InsertPairDFM(const AIndex: integer; const AName, AValue: string);
begin
  FStrings.Insert(AIndex, AName + FStrings.NameValueSeparator + StringToDFMString(AValue));
end;

function TSANReplacer.Replace(const ATest: string): string;
var
  i : integer;
begin
  Result := ATest;

  for i := 0 to FStrings.Count - 1 do
    Result := StringReplace
    (
      Result,
      FBegName + FStrings.Names[i] + FEndName,
      FStrings.ValueFromIndex[i],
      [rfReplaceAll, rfIgnoreCase]
    );
end;
//==============================================================================

END.
