{*******************************************************}
{                                                       }
{       SANDesignPropertyEditers                        }
{       Редакторы свойств.                              }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignPropertyEditers;
{$I NX.INC}
INTERFACE
(*
USES
  ToolsAPI, DesignIntf, DesignEditors,
  SysUtils, Classes;

TYPE
  /// <summary>
  /// Редактор свойств GUID в виде строки.
  /// </summary>
  TSANGUIDPropertyEditor = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;
*)
IMPLEMENTATION
(*
uses
  SANDesignGUIDDialog;

//==============================================================================
// TSANGUIDPropertyEditor
// public
function TSANGUIDPropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := inherited;
  Include(Result, paReadOnly);
  Include(Result, paDialog);
  Exclude(Result, paMultiSelect);
end;

procedure TSANGUIDPropertyEditor.Edit;
var
  vName, S : string;
begin

  if GetComponent(0) is TComponent then
    vName := (GetComponent(0) as TComponent).Name + ':'
  else
    vName := '';

  S := GetStrValue;

  if not SANGUIDDialog(vName + GetName, S) then
    Exit;

  SetStrValue(S);
end;
//==============================================================================
*)
(*
//==============================================================================
// Property Editors
//==============================================================================
  TSAN_ImageIndex_PropertyEditor = class(TIntegerProperty, ICustomPropertyListDrawing)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    function GetImageList : TCustomImageList; virtual; abstract;

    procedure ListMeasureWidth(const Value: string; ACanvas: TCanvas;
      var AWidth: Integer);
    procedure ListMeasureHeight(const Value: string; ACanvas: TCanvas;
      var AHeight: Integer);
    procedure ListDrawValue(const Value: string; ACanvas: TCanvas;
      const ARect: TRect; ASelected: Boolean);

  end;//TSDE_TabFormVarImageIndex_PropertyEditor
//==============================================================================
  TSAN_ModuleRootClassName_PropertyEditor = class(TStringProperty)
  public
    function ValidRootClass(ARootComponent : TComponent) : boolean; virtual;
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;//TSAN_ModuleRootClassName_PropertyEditor
//==============================================================================
  TSAN_FormClassName_PropertyEditor = class(TSAN_ModuleRootClassName_PropertyEditor)
  public
    function ValidRootClass(ARootComponent : TComponent) : boolean; override;
  end;//TSAN_ModuleRootClassName_PropertyEditor
//==============================================================================
  TSAN_ModuleComponentName_PropertyEditor = class(TStringProperty)
  public
    function Get_RootComponent : TComponent; virtual; abstract;
    function ValidComponent(AComponent : TComponent) : boolean; virtual;
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;//TSAN_ModuleComponentName_PropertyEditor
//==============================================================================
  TSAN_FilteredComponent_PropertyEditor = class(TComponentProperty)
  private
    FStringList : TStringList;
  private
    procedure InnerStrProc(const S : String);

  public
    procedure GetValues(Proc: TGetStrProc); override;
    function ValidComponent(AComponent : TComponent) : boolean; virtual; abstract;
  end;//TSDE_DCRefreshTablesHook_PropertyEditor
//==============================================================================
  TSAN_DataSetFieldsNames_PropertyEditor = class(TStringProperty)
  public
    function GetDataSet : TDataSet; virtual; abstract;

    function ValidField(AField : TField) : boolean; virtual;

    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;//TSDE_FieldNames_PropertyEditor
//==============================================================================
  TSAN_StrFromStrings_Type = (sssStrings, sssNames, sssValues);
//------------------------------------------------------------------------------
  TSAN_StrFromStrings_PropertyEditor = class(TStringProperty)
  public
    function GetStrings : TStrings; virtual; abstract;

    function GetStringsType : TSAN_StrFromStrings_Type; virtual;
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;//TSDE_StrFromStrings_PropertyEditor
//==============================================================================
  TSAN_StrFromInnerStrings_PropertyEditor = class(TSAN_StrFromStrings_PropertyEditor)
  private
    FInnerStrings : TStrings;

  public
    constructor Create(const ADesigner: IDesigner; APropCount: Integer); override;
    destructor Destroy; override;

    function GetStrings : TStrings; override;

  public
    property InnerStrings : TStrings read FInnerStrings;
  end;//TSAN_StrFromInnerStrings_PropertyEditor
//==============================================================================
  TSAN_HKEY_PropertyEditor = class(TInt64Property)
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  end;//TSAN_HKEY_PropertyEditor
//==============================================================================

//==============================================================================
// Property Editors
//==============================================================================

//==============================================================================
// TSAN_ImageIndex_PropertyEditor
//==============================================================================
function TSAN_ImageIndex_PropertyEditor.GetAttributes : TPropertyAttributes;
begin
  Result:=[paValueList]
end;//TSAN_ImageIndex_PropertyEditor.GetAttributes
//==============================================================================
procedure TSAN_ImageIndex_PropertyEditor.GetValues(Proc: TGetStrProc);
var
  i : integer;
begin
  if PropCount > 1 then
    Exit;

  Proc('-1');

  if GetImageList <> nil then
    for i := 0 to GetImageList.Count - 1 do
      Proc(IntToStr(i));
end;//TSAN_ImageIndex_PropertyEditor.GetValues
//==============================================================================
procedure TSAN_ImageIndex_PropertyEditor.ListDrawValue(
  const Value: string; ACanvas: TCanvas; const ARect: TRect;
  ASelected: Boolean);
var
  vImageIndex : integer;
begin
  vImageIndex := StrToInt(Value);

  with ACanvas, ARect do  begin
    FillRect(ARect);

    TextRect(ARect, 4, Top, Value);

    if vImageIndex > -1 then
      GetImageList.Draw(ACanvas, Left + 32, Top, vImageIndex, True);

  end;//with
end;//TSAN_ImageIndex_PropertyEditor.ListDrawValue
//==============================================================================
procedure TSAN_ImageIndex_PropertyEditor.ListMeasureHeight(
  const Value: string; ACanvas: TCanvas; var AHeight: Integer);
begin
  if (GetImageList = nil) or (GetImageList.Height < 14) then
    AHeight := 14
  else
    AHeight := GetImageList.Height
end;//TSAN_ImageIndex_PropertyEditor.ListMeasureHeight
//==============================================================================
procedure TSAN_ImageIndex_PropertyEditor.ListMeasureWidth(
  const Value: string; ACanvas: TCanvas; var AWidth: Integer);
begin
  if (GetImageList = nil) then
    AWidth := ACanvas.TextWidth(Value)
  else
    if GetImageList.Count < 10 then
      AWidth := GetImageList.Width + 4 + ACanvas.TextWidth('-1')
    else
      AWidth := GetImageList.Width + 4 + ACanvas.TextWidth( IntToStr(GetImageList.Count) );
end;//TSAN_ImageIndex_PropertyEditor.ListMeasureWidth
//==============================================================================

//==============================================================================
// TSAN_ModuleRootClassName_PropertyEditor
//==============================================================================
function TSAN_ModuleRootClassName_PropertyEditor.ValidRootClass(
  ARootComponent: TComponent): boolean;
// Допустимый класс
begin
  Result := True;
end;//TSAN_ModuleRootClassName_PropertyEditor.ValidRootClass
//==============================================================================
function TSAN_ModuleRootClassName_PropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result:=[paValueList]
end;//TSAN_ModuleRootClassName_PropertyEditor.GetAttributes
//==============================================================================
procedure TSAN_ModuleRootClassName_PropertyEditor.GetValues(Proc: TGetStrProc);
var
  i, j : integer;
  vProject : IOTAProject;
  vServices : IOTAModuleServices;
  vFIOTAModuleInfo : IOTAModuleInfo;
  vModule : IOTAModule;
  vFIOTAFormEditor : IOTAFormEditor;
  vFIOTAComponent : IOTAComponent;
  vRootComponent : TComponent;
begin
  vProject := GetActiveProject;

  if vProject = nil then
    Exit;

  vServices := BorlandIDEServices as IOTAModuleServices;

  for i := 0 to vProject.GetModuleCount - 1 do  begin
    vFIOTAModuleInfo := vProject.GetModule(i);

    if vFIOTAModuleInfo.GetFormName <> '' then  begin
      vFIOTAModuleInfo.OpenModule;

      vModule := vServices.FindModule(vFIOTAModuleInfo.FileName);

      for j := 0 to vModule.GetModuleFileCount - 1 do
        if Supports(vModule.GetModuleFileEditor(j), IOTAFormEditor, vFIOTAFormEditor) then  begin
          vFIOTAComponent := vFIOTAFormEditor.GetRootComponent;

          if vFIOTAComponent = nil then
            Continue;

          vRootComponent := TComponent(vFIOTAComponent.GetComponentHandle);

          if not ValidRootClass(vRootComponent) then
            Continue;

          Proc(vRootComponent.ClassName);
        end;//if; for
    end;//if
  end;//for

end;//TSAN_ModuleRootClassName_PropertyEditor.GetValues
//==============================================================================

//==============================================================================
// TSAN_FormClassName_PropertyEditor
//==============================================================================
function TSAN_FormClassName_PropertyEditor.ValidRootClass(
  ARootComponent: TComponent): boolean;
begin
  Result := ARootComponent is TForm;
end;//TSAN_FormClassName_PropertyEditor.ValidRootClass
//==============================================================================

//==============================================================================
// TSAN_ModuleComponentName_PropertyEditor
//==============================================================================
function TSAN_ModuleComponentName_PropertyEditor.ValidComponent(
  AComponent: TComponent): boolean;
// Подходит ли компонент
begin
  Result := True;
end;//TSAN_ModuleComponentName_PropertyEditor.ValidComponent
//==============================================================================
function TSAN_ModuleComponentName_PropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result:=[paValueList]
end;//TSAN_ModuleComponentName_PropertyEditor.GetAttributes
//==============================================================================
procedure TSAN_ModuleComponentName_PropertyEditor.GetValues(Proc: TGetStrProc);
var
  i : integer;
  vRootComponent : TComponent;
begin
  if PropCount > 1 then
    Exit;

  vRootComponent := Get_RootComponent;

  if vRootComponent = nil then
    Exit;

  for i := 0 to vRootComponent.ComponentCount - 1 do
    if ValidComponent(vRootComponent.Components[i]) then
      Proc(vRootComponent.Components[i].Name);
end;//TSAN_ModuleComponentName_PropertyEditor.GetValues
//==============================================================================

//==============================================================================
// TSAN_FilteredComponent_PropertyEditor
//==============================================================================
// private
//==============================================================================
procedure TSAN_FilteredComponent_PropertyEditor.InnerStrProc(const S: String);
begin
  FStringList.Add(S);
end;//TSAN_FilteredComponent_PropertyEditor.InnerStrProc
//==============================================================================
//==============================================================================
// public
//==============================================================================
procedure TSAN_FilteredComponent_PropertyEditor.GetValues(Proc: TGetStrProc);
var
  i : integer;
begin
  FStringList := TStringList.Create;
  try
    Designer.GetComponentNames(GetTypeData(GetPropType), InnerStrProc);

    for i := 0 to FStringList.Count - 1 do
      if ValidComponent(Designer.GetComponent(FStringList[i])) then
        Proc(FStringList[i]);

  finally
    FreeAndNil(FStringList);
  end;//t..f
end;//TSAN_FilteredComponent_PropertyEditor.GetValues
//==============================================================================


//==============================================================================
// TSAN_DataSetFieldsNames_PropertyEditor
//==============================================================================
function TSAN_DataSetFieldsNames_PropertyEditor.ValidField(AField: TField): boolean;
begin
  Result := True;
end;//TSAN_DataSetFieldsNames_PropertyEditor.ValidField
//==============================================================================
function TSAN_DataSetFieldsNames_PropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList]
end;//TSAN_DataSetFieldsNames_PropertyEditor.GetAttributes
//==============================================================================
procedure TSAN_DataSetFieldsNames_PropertyEditor.GetValues(Proc: TGetStrProc);
var
  i : integer;
  vDataSet : TDataSet;
begin
  if (PropCount > 1) then
    Exit;

  vDataSet := GetDataSet;

  if vDataSet = nil then
    Exit;

  for i := 0 to vDataSet.FieldCount - 1 do
    if ValidField(vDataSet.Fields[i]) then
      Proc(vDataSet.Fields[i].FieldName);
end;//TSAN_DataSetFieldsNames_PropertyEditor.GetValues
//==============================================================================

//==============================================================================
// TSAN_StrFromStrings_PropertyEditor
//==============================================================================
function TSAN_StrFromStrings_PropertyEditor.GetStringsType: TSAN_StrFromStrings_Type;
begin
  Result := sssStrings;
end;//TSAN_StrFromStrings_PropertyEditor.GetStringsType
//==============================================================================
function TSAN_StrFromStrings_PropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList]
end;//TSAN_StrFromStrings_PropertyEditor.GetAttributes
//==============================================================================
procedure TSAN_StrFromStrings_PropertyEditor.GetValues(Proc: TGetStrProc);
var
  i : integer;
  vStrings : TStrings;
begin
  if (PropCount > 1) then
    Exit;

  vStrings := GetStrings;

  if vStrings = nil then
    Exit;

  for i := 0 to vStrings.Count - 1 do
    case GetStringsType of
      sssStrings:
        Proc(vStrings[i]);

      sssNames:
        Proc(vStrings.Names[i]);

      sssValues:
        Proc(vStrings.ValueFromIndex[i]);
    end;//case; for

end;//TSAN_StrFromStrings_PropertyEditor.GetValues
//==============================================================================

//==============================================================================
// TSAN_StrFromInnerStrings_PropertyEditor
//==============================================================================
constructor TSAN_StrFromInnerStrings_PropertyEditor.Create(const ADesigner: IDesigner;
  APropCount: Integer);
begin
  inherited;

  FInnerStrings := TStringList.Create;
end;//TSAN_StrFromInnerStrings_PropertyEditor.Create
//==============================================================================
destructor TSAN_StrFromInnerStrings_PropertyEditor.Destroy;
begin
  FreeAndNil(FInnerStrings);

  inherited;
end;//TSAN_StrFromInnerStrings_PropertyEditor.Destroy
//==============================================================================
function TSAN_StrFromInnerStrings_PropertyEditor.GetStrings: TStrings;
begin
  Result := FInnerStrings;
end;//TSAN_StrFromInnerStrings_PropertyEditor.GetStrings
//==============================================================================

//==============================================================================
// TSAN_HKEY_PropertyEditor
//==============================================================================
function TSAN_HKEY_PropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList];
end;//TSAN_HKEY_PropertyEditor.GetAttributes
//==============================================================================
procedure TSAN_HKEY_PropertyEditor.GetValues(Proc: TGetStrProc);
begin
  Proc('HKEY_CLASSES_ROOT');
  Proc('HKEY_CURRENT_USER');
  Proc('HKEY_LOCAL_MACHINE');
  Proc('HKEY_USERS');
  Proc('HKEY_PERFORMANCE_DATA');
  Proc('HKEY_CURRENT_CONFIG');
  Proc('HKEY_DYN_DATA');
end;//TSAN_HKEY_PropertyEditor.GetValues
//==============================================================================
function TSAN_HKEY_PropertyEditor.GetValue: string;
begin
  if       GetInt64Value = HKEY_CLASSES_ROOT then
    Result := 'HKEY_CLASSES_ROOT'

  else  if GetInt64Value = HKEY_CURRENT_USER then
    Result := 'HKEY_CURRENT_USER'

  else  if GetInt64Value = HKEY_LOCAL_MACHINE then
    Result := 'HKEY_LOCAL_MACHINE'

  else  if GetInt64Value = HKEY_USERS then
    Result := 'HKEY_USERS'

  else  if GetInt64Value = HKEY_PERFORMANCE_DATA then
    Result := 'HKEY_PERFORMANCE_DATA'

  else  if GetInt64Value = HKEY_CURRENT_CONFIG then
    Result := 'HKEY_CURRENT_CONFIG'

  else  //if GetInt64Value = HKEY_DYN_DATA then
    Result := 'HKEY_DYN_DATA';
end;//TSAN_HKEY_PropertyEditor.GetValue
//==============================================================================
procedure TSAN_HKEY_PropertyEditor.SetValue(const Value: string);
begin
  if       Value = 'HKEY_CLASSES_ROOT' then
    SetInt64Value(HKEY_CLASSES_ROOT)

  else  if Value = 'HKEY_CURRENT_USER' then
    SetInt64Value(HKEY_CURRENT_USER)

  else  if Value = 'HKEY_LOCAL_MACHINE' then
    SetInt64Value(HKEY_LOCAL_MACHINE)

  else  if Value = 'HKEY_USERS' then
    SetInt64Value(HKEY_USERS)

  else  if Value = 'HKEY_PERFORMANCE_DATA' then
    SetInt64Value(HKEY_PERFORMANCE_DATA)

  else  if Value = 'HKEY_CURRENT_CONFIG' then
    SetInt64Value(HKEY_CURRENT_CONFIG)

  else  //if Value = 'HKEY_DYN_DATA' then
    SetInt64Value(HKEY_DYN_DATA);
end;//TSAN_HKEY_PropertyEditor.SetValue
//==============================================================================
*)

END.
