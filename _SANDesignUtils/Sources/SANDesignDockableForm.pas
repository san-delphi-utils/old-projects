{*******************************************************}
{                                                       }
{       SANDesignDockableForm                           }
{       Форма для встраивания в IDE.                    }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignDockableForm;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI, SANDesign_DesignIntf,
  SysUtils, Classes, Graphics, Controls, Forms, Actnlist, ImgList, Menus,
  ComCtrls, IniFiles;

TYPE
  /// <summary>
  /// Объект управления формой IDE.
  /// </summary>
  TSANCustomDockableForm = class(TInterfacedObject, INTACustomDockableForm)
  private
    FCustomFrameClass : TCustomFrameClass;
    FFrame : TCustomFrame;
    FForm : TCustomForm;
    FCaption : string;
    FDestroyed : TNotifyEvent;

  protected
    // INTACustomDockableForm
    function GetCaption: string; virtual;
    function GetFrameClass: TCustomFrameClass;
    procedure FrameCreated(AFrame: TCustomFrame);
    function GetIdentifier: string; virtual;
    function GetMenuActionList: TCustomActionList; virtual;
    function GetMenuImageList: TCustomImageList; virtual;
    procedure CustomizePopupMenu(PopupMenu: TPopupMenu); virtual;
    function GetToolbarActionList: TCustomActionList; virtual;
    function GetToolbarImageList: TCustomImageList; virtual;
    procedure CustomizeToolBar(ToolBar: TToolBar); virtual;
    procedure LoadWindowState(Desktop: TCustomIniFile; const Section: string); virtual;
    procedure SaveWindowState(Desktop: TCustomIniFile; const Section: string; IsProject: Boolean); virtual;
    function GetEditState: TEditState; virtual;
    function EditAction(Action: TEditAction): Boolean; virtual;

  public
    constructor Create(const ACustomFrameClass : TCustomFrameClass;
      const ACaption : TCaption; const ADestroyed : TNotifyEvent = nil); virtual;
    destructor Destroy; override;

    function CreateDockableForm : TCustomForm;

  public
    property Frame : TCustomFrame  read FFrame;
    property Form : TCustomForm  read FForm;
  end;
  TSANCustomDockableFormClass = class of TSANCustomDockableForm;

IMPLEMENTATION

//==============================================================================
// TSANCustomDockableForm
// protected
function TSANCustomDockableForm.GetCaption: string;
begin
  Result := FCaption;
end;

function TSANCustomDockableForm.GetFrameClass: TCustomFrameClass;
begin
  Result := FCustomFrameClass;
end;

procedure TSANCustomDockableForm.FrameCreated(AFrame: TCustomFrame);
begin
  FFrame := AFrame;
end;

function TSANCustomDockableForm.GetIdentifier: string;
begin
  Result := '';
end;

function TSANCustomDockableForm.GetMenuActionList: TCustomActionList;
begin
  Result := nil;
end;

function TSANCustomDockableForm.GetMenuImageList: TCustomImageList;
begin
  Result := nil;
end;

procedure TSANCustomDockableForm.CustomizePopupMenu(PopupMenu: TPopupMenu);
begin
  // no toolbar
end;

function TSANCustomDockableForm.GetToolbarActionList: TCustomActionList;
begin
  Result := nil;
end;

function TSANCustomDockableForm.GetToolbarImageList: TCustomImageList;
begin
  Result := nil;
end;

procedure TSANCustomDockableForm.CustomizeToolBar(ToolBar: TToolBar);
begin
  // no toolbar
end;

procedure TSANCustomDockableForm.LoadWindowState(Desktop: TCustomIniFile;
  const Section: string);
begin
  //no desktop saving
end;

procedure TSANCustomDockableForm.SaveWindowState(Desktop: TCustomIniFile;
  const Section: string; IsProject: Boolean);
begin
  //no desktop saving
end;

function TSANCustomDockableForm.GetEditState: TEditState;
begin
  Result := [];
end;

function TSANCustomDockableForm.EditAction(Action: TEditAction): Boolean;
begin
  Result := False;
end;

// public
constructor TSANCustomDockableForm.Create(const ACustomFrameClass : TCustomFrameClass;
  const ACaption : TCaption; const ADestroyed : TNotifyEvent);
begin
  inherited Create;

  FCustomFrameClass := ACustomFrameClass;
  FCaption := ACaption;
  FDestroyed := ADestroyed;
end;

destructor TSANCustomDockableForm.Destroy;
begin

  if Assigned(FDestroyed) then
    FDestroyed(Self);

  FFrame := nil;
  FForm := nil;

  inherited;
end;

function TSANCustomDockableForm.CreateDockableForm: TCustomForm;
begin
  FForm := (BorlandIDEServices as INTAServices).CreateDockableForm(Self);
  Result := FForm;
end;
//==============================================================================


END.
