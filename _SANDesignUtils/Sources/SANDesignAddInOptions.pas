{*******************************************************}
{                                                       }
{       SANDesignAddInOptions                           }
{       Управление настройками IDE.                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignAddInOptions;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  SysUtils, Classes, Controls, Forms;

TYPE
  /// <summary>
  /// Абстрактный класс узла настроек IDE. Регистрация происходит в AfterConstruction.
  /// </summary>
  TSANCustomAddInOptions = class(TInterfacedObject, INTAAddInOptions)
  protected
    // INTAAddInOptions
    function GetArea: string; dynamic;
    function GetCaption: string; virtual; abstract;
    function GetFrameClass: TCustomFrameClass; virtual; abstract;
    procedure FrameCreated(AFrame: TCustomFrame); virtual;
    procedure DialogClosed(Accepted: Boolean); virtual;
    function ValidateContents: Boolean; virtual;
    function GetHelpContext: Integer; virtual;
    function IncludeInIDEInsight: Boolean; dynamic;

  public
    procedure AfterConstruction; override;

  end;

  /// <summary>
  /// Класс узла настроек IDE без редактируемых параметров.
  /// </summary>
  TSANGroupAddInOptions = class(TSANCustomAddInOptions)
  private
    FCaption, FTitle : string;

  protected
    function GetCaption: string; override;
    function GetFrameClass: TCustomFrameClass; override;
    procedure FrameCreated(AFrame: TCustomFrame); override;

  public
    constructor Create(const ACaption, ATitle : string);

  end;

/// <summary>
/// Отмена регистрации узла настроек IDE.
/// </summary>
/// <param name="AAddInOptions">
/// Пременная интерфейса узла настроек IDE. Узел будет разрегистрирован,
/// а переменной будет присвоино значение nil.
/// </param>
procedure UnregisterAddInOptions(var AAddInOptions: INTAAddInOptions);

IMPLEMENTATION

uses
  SANDesignGroupAddInOptionsFrame;

procedure UnregisterAddInOptions(var AAddInOptions: INTAAddInOptions);
var
  vEnvironmentOptionsServices : INTAEnvironmentOptionsServices;
begin

  if not Assigned(AAddInOptions) then
    Exit;

  if Supports(BorlandIDEServices, INTAEnvironmentOptionsServices, vEnvironmentOptionsServices) then
    vEnvironmentOptionsServices.UnregisterAddInOptions(AAddInOptions);

  AAddInOptions := nil;
end;

//==============================================================================
// TSANCustomAddInOptions
function TSANCustomAddInOptions.GetArea: string;
begin
  Result := '';
end;

procedure TSANCustomAddInOptions.FrameCreated(AFrame: TCustomFrame);
begin
end;

procedure TSANCustomAddInOptions.DialogClosed(Accepted: Boolean);
begin
end;

function TSANCustomAddInOptions.ValidateContents: Boolean;
begin
  Result := True;
end;

function TSANCustomAddInOptions.GetHelpContext: Integer;
begin
  Result := 0;
end;

function TSANCustomAddInOptions.IncludeInIDEInsight: Boolean;
begin
  Result := True;
end;

// public
procedure TSANCustomAddInOptions.AfterConstruction;
var
  vEnvironmentOptionsServices : INTAEnvironmentOptionsServices;
begin
  inherited;

  if Supports(BorlandIDEServices, INTAEnvironmentOptionsServices, vEnvironmentOptionsServices) then
    vEnvironmentOptionsServices.RegisterAddInOptions(Self);
end;
//==============================================================================

//==============================================================================
// TSANGroupAddInOptions
// protected
procedure TSANGroupAddInOptions.FrameCreated(AFrame: TCustomFrame);
begin
  (AFrame as TframeSANDesignGroupAddInOptions).Title := FTitle;
end;

function TSANGroupAddInOptions.GetCaption: string;
begin
  Result := FCaption;
end;

function TSANGroupAddInOptions.GetFrameClass: TCustomFrameClass;
begin
  Result := TframeSANDesignGroupAddInOptions;
end;

// public
constructor TSANGroupAddInOptions.Create(const ACaption, ATitle: string);
begin
  inherited Create;

  FCaption := ACaption;
  FTitle   := ATitle;
end;
//==============================================================================

END.
