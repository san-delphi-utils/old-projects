{*******************************************************}
{                                                       }
{       SANDesignUtils                                  }
{       Набор разнообразных функций и классов           }
{       для разработки расширений IDE.                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignUtils;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI, SANDesign_DesignIntf,
  Windows, SysUtils, Classes, Graphics, Imglist, ActnList, Menus,
  SANDesignKeysBinding, SANDesignAddInOptions;

TYPE
  ESANDesignError = class(Exception);

  /// <summary>
  /// Интерфейс предоставления hInstace расположения объекта.
  /// </summary>
  ISANProvideHInstace = interface(IInterface)
    ['{5133C0F1-18F1-4B0C-8866-42392EBA7557}']
    function Get_HInstace : LongWord; safecall;
    /// <summary>
    /// hInstace расположения объекта.
    /// </summary>
    property HInstace : LongWord  read Get_HInstace;
  end;

  /// <summary>
  /// Функция обратного вызова при перечислении модулей с корневыми компонентами.
  /// </summary>
  /// <param name="AModule">
  /// Модуль.
  /// </param>
  /// <param name="ARootComponent">
  /// Корневой компонент.
  /// </param>
  /// <param name="AParam">
  /// Параметр перечисления.
  /// </param>
  /// <returns>
  /// Boolean. True - завершить перечисление.
  /// </returns>
  TSANEnumRootCompFunc = function (const AModule : IOTAModule; const ARootComponent : TComponent;
    const AParam : Pointer) : boolean;


/// <summary>
/// Перевод текста в формат .dfm. Все одинарные кавычки и русскоязычные символы
/// заменяются кодами с символом #.
/// </summary>
function StringToDFMString(const S : string) : string;

/// <summary>
/// Получение текста редактора кода.
/// </summary>
/// <param name="ASourceEditor">
/// Редактор кода IOTASourceEditor.
/// </param>
/// <returns>
/// String. Текст редактора кода.
/// </returns>
function EditorAsString(const ASourceEditor : IOTASourceEditor) : String;

/// <summary>
/// Добавить текст в редактор кода.
/// </summary>
/// <param name="AWriter">
/// Интерфейс записи.
/// </param>
/// <param name="AIndent">
/// Отступ.
/// </param>
/// <param name="AText">
/// Добавляемый текст.
/// </param>
procedure WriterAddText(const AWriter : IOTAEditWriter; const AIndent : Integer;
  const AText : String);

/// <summary>
/// Полутить интерфейс буфера первого редактора модуля.
/// </summary>
/// <param name="AModule">
/// Модуль.
/// </param>
/// <param name="ABuffer">
/// Найденый буфер, в случае успеха.
/// </param>
/// <returns>
/// Boolean. True - успешно.
/// </returns>
function GetEditBuffer(const AModule: IOTAModule; out ABuffer: IOTAEditBuffer): boolean;

/// <summary>
/// Загрузить картинку, вставить ее в глобальный список изображений и вернуть индекс в списке.
/// </summary>
/// <param name="AResourseName">
/// Имя ресурса картинки.
/// </param>
/// <param name="AMaskColor">
/// Цвет маски.
/// </param>
/// <returns>
/// TImageIndex. Индекс изображения в глобальном списке.
/// </returns>
function AddGlobalImage(const AResourseName : string; const AMaskColor : TColor = clWhite) : TImageIndex;

/// <summary>
/// Добавить действие TAction в глобальный список действий или обновить информацию.
/// </summary>
/// <param name="AActionName">
/// Имя действия.
/// </param>
/// <param name="ACaption">
/// Внешнее имя действия.
/// </param>
/// <param name="AShortCutForBind">
/// Сочетание горячих клавиш. Если пустая строка - горячие клавиши не назначаются.
/// </param>
/// <param name="AOnExecute">
/// Обработчик события OnExecute.
/// </param>
/// <param name="AOnUpdate">
/// Обработчик события OnUpdate.
/// </param>
/// <returns>
/// TAction. Созданное или найденое действие.
/// </returns>
function AddGlobalAction(const AActionName, ACaption, AShortCutForBind : string;
  const AOnExecute : TNotifyEvent; const AOnUpdate : TNotifyEvent = nil) : TAction;

/// <summary>
/// Получить корневой компонент модуля.
/// </summary>
/// <param name="ARootComponent">
/// Корневой компонент, в случае успеха.
/// </param>
/// <param name="AModule">
/// Модуль.
/// </param>
/// <returns>
/// Boolean. True - успешно.
/// </returns>
function GetModuleRootComponent(out ARootComponent; const AModule : IOTAModule) : boolean;

/// <summary>
/// Поиск модуля и корневого компонента по классу корневого компонента.
/// </summary>
/// <param name="AModule">
/// Модуль, в случае успеха.
/// </param>
/// <param name="ARootComponent">
/// Корневой компонент, в случае успеха.
/// </param>
/// <param name="AClass">
/// Класс корневого компонента.
/// </param>
/// <returns>
/// Boolean. True - успешно.
/// </returns>
function FindRootComponentByClass(out AModule : IOTAModule; out ARootComponent;
  const AClass : TComponentClass) : boolean;

/// <summary>
/// Перечисление модулей с корневыми компонентами.
/// </summary>
/// <param name="AEnumFunc">
/// Функция обратного вызова.
/// </param>
/// <param name="AParam">
/// Параметр перечисления.
/// </param>
procedure EnumRootComponentModules(const AEnumFunc : TSANEnumRootCompFunc; const AParam : Pointer);

procedure Register;

IMPLEMENTATION

{$R *.res}

type
  TCustomActionListHack = class(TCustomActionList);

function StringToDFMString(const S : string) : string;
var
  I: Integer;
  vQuoted : boolean;
begin

  if S = '' then
  begin
    Result := #39#39;
    Exit;
  end;

  vQuoted := False;

  SetLength(Result, 0);

  for I := 1 to Length(S) do
    if (S[i] = #39) or (ORD(S[i]) > 255) then
      begin

        if vQuoted then
        begin
          Result := Result + #39;
          vQuoted := False;
        end;

        if S[i] = #39 then
          Result := Result + '#39'
        else
          Result := Result + '#' + IntToStr(Ord(S[i]));
      end
    else
      begin

        if not vQuoted then
        begin
          Result := Result + #39;
          vQuoted := True;
        end;

        Result := Result + S[i];
      end;

  if vQuoted then
    Result := Result + #39;

end;

function EditorAsString(const ASourceEditor : IOTASourceEditor) : String;
// http://www.davidghoyle.co.uk/WordPress/?tag=iotaeditreader
const
  BUFFER_SIZE : Integer = 1024;
var
  Reader : IOTAEditReader;
  vRead, vPosition : integer;
  vBuffer : AnsiString;
begin
  Result := '';

  Reader := ASourceEditor.CreateReader;
  try
    vPosition := 0;

    repeat
      SetLength(vBuffer, BUFFER_SIZE);
      vRead := Reader.GetText(vPosition, PAnsiChar(vBuffer), BUFFER_SIZE);

      SetLength(vBuffer, vRead);
      Result := Result + String(vBuffer);

      Inc(vPosition, vRead);
    until vRead < BUFFER_SIZE;

  finally
    Reader := nil;
  end;

end;

procedure WriterAddText(const AWriter : IOTAEditWriter; const AIndent : Integer;
  const AText : String);
begin
  AWriter.Insert(PAnsiChar(AnsiString(StringOfChar(#32, AIndent) + AText)));
end;

function GetEditBuffer(const AModule: IOTAModule; out ABuffer: IOTAEditBuffer): boolean;
var
  i: integer;
begin

  if Assigned(AModule) then
    for i := 0 to AModule.GetModuleFileCount - 1 do
     if Supports(AModule.GetModuleFileEditor(i), IOTAEditBuffer, ABuffer) then
       Exit(True);

  Result := False;
end;

function AddGlobalImage(const AResourseName : string; const AMaskColor : TColor = clWhite) : TImageIndex;
var
  vSrv : INTAServices;
  vBMP : TBitmap;
begin

  if not Supports(BorlandIDEServices, INTAServices, vSrv) then
    Exit(-1);

  vBMP := TBitmap.Create;
  try
    vBMP.LoadFromResourceName(HInstance, AResourseName);
    Result := vSrv.ImageList.AddMasked(vBMP, AMaskColor);
  finally
    FreeAndNil(vBMP);
  end;

end;

function AddGlobalAction(const AActionName, ACaption, AShortCutForBind : string;
  const AOnExecute, AOnUpdate : TNotifyEvent) : TAction;
var
  vSrv : INTAServices;
  vAL : TCustomActionListHack;
  vShortCut : TShortCut;
begin

  if not Supports(BorlandIDEServices, INTAServices, vSrv) then
    Exit(nil);

  vShortCut := TextToShortCut(AShortCutForBind);

  vAL := TCustomActionListHack(vSrv.ActionList);

  Result := TAction(vAL.FindComponent(AActionName));

  if not Assigned(Result) then
    begin
      Result := TAction.Create(vAL);
      Result.Name := AActionName;
      vAL.AddAction(Result);
    end

  else
    if vShortCut <> Result.ShortCut then
      SANKeysBindingRemoveAction(Result);

  with Result do
  begin
    Caption    := ACaption;
    ShortCut   := vShortCut;
    OnExecute  := AOnExecute;
    OnUpdate   := AOnUpdate;
  end;

  if vShortCut <> 0 then
    SANKeysBindingAddAction(Result);
end;

function GetModuleRootComponent(out ARootComponent; const AModule : IOTAModule) : boolean;
var
  i : integer;
  vFIOTAFormEditor : IOTAFormEditor;
  vFIOTAComponent : IOTAComponent;
begin

  for i := 0 to AModule.GetModuleFileCount - 1 do
    if Supports(AModule.GetModuleFileEditor(i), IOTAFormEditor, vFIOTAFormEditor) then
    begin
      vFIOTAComponent := vFIOTAFormEditor.GetRootComponent;
      if vFIOTAComponent = nil then
        Continue;

      Pointer(ARootComponent) := vFIOTAComponent.GetComponentHandle;
      Exit(True);
    end;

  Result := False;
end;

function FindRootComponentByClass(out AModule : IOTAModule; out ARootComponent;
  const AClass : TComponentClass) : boolean;
var
  i : integer;
  vProject : IOTAProject;
  vFIOTAModuleInfo : IOTAModuleInfo;
  vModule : IOTAModule;
  vRootComponent : TComponent;
begin
  Result := False;

  if not Assigned(AClass) then
    Exit;

  vProject := GetActiveProject;
  if vProject = nil then
    Exit;

  for i := 0 to vProject.GetModuleCount - 1 do
  begin
    vFIOTAModuleInfo := vProject.GetModule(i);

    if vFIOTAModuleInfo.GetFormName = '' then
      Continue;

    vModule := vFIOTAModuleInfo.OpenModule;

    if  GetModuleRootComponent(vRootComponent, vModule)
    and vRootComponent.ClassType.InheritsFrom(AClass)
    then
    begin
      AModule := vModule;
      Pointer(ARootComponent) := vRootComponent;
      Exit(True);
    end;

  end;//for

end;

procedure EnumRootComponentModules(const AEnumFunc : TSANEnumRootCompFunc; const AParam : Pointer);
var
  i : integer;
  vProject : IOTAProject;
  vFIOTAModuleInfo : IOTAModuleInfo;
  vModule : IOTAModule;
  vRootComponent : TComponent;
begin

  vProject := GetActiveProject;
  if vProject = nil then
    Exit;

  for i := 0 to vProject.GetModuleCount - 1 do
  begin
    vFIOTAModuleInfo := vProject.GetModule(i);

    if vFIOTAModuleInfo.GetFormName = '' then
      Continue;

    vModule := vFIOTAModuleInfo.OpenModule;

    if  GetModuleRootComponent(vRootComponent, vModule)
    and AEnumFunc(vModule, vRootComponent, AParam)
    then
      Exit;

  end;//for
end;

var
  vRootSANDesignAddInOpts : INTAAddInOptions;

procedure Register;
begin
  vRootSANDesignAddInOpts := TSANGroupAddInOptions.Create('SAN Design Utils', 'SAN Design Utils Options');
end;

procedure UnRegister;
begin
  UnregisterAddInOptions(vRootSANDesignAddInOpts);
end;

INITIALIZATION

FINALIZATION
  UnRegister;
END.
