{*******************************************************}
{                                                       }
{       SANDesignWizards                                }
{       Базовые классы экспертов.                       }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDesignWizards;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI, SANDesign_DesignIntf,
  Windows, SysUtils, Classes,
  SANDesignUtils;

type
  /// <summary>
  /// Класс статического эксперта. IDString и Name задаются при создании.
  /// </summary>
  TSANStaticWizard = class(TNotifierObject, IOTAWizard, ISANProvideHInstace)
  private
    FInstance : LongWord;
    FIDString, FName : string;

  protected
    // IOTAWizard
    function GetIDString: string; dynamic;
    function GetName: string; dynamic;
    function GetState: TWizardState; virtual;
    procedure Execute; virtual; abstract;

    // ISANProvideHInstace
    function Get_HInstace : LongWord; dynamic; safecall;

  public
    constructor Create(const AInstance : LongWord; const AIDString, AName : string);
  end;

  /// <summary>
  /// Абстрактный класс эксперта репозитория.
  /// </summary>
  TSANCustomRepositoryWizard = class(TNotifierObject, IOTAWizard, IOTARepositoryWizard,
    IOTARepositoryWizard60, IOTARepositoryWizard80)

  protected
    // IOTAWizard
    function GetIDString: string; virtual; abstract;
    function GetName: string; virtual; abstract;
    function GetState: TWizardState; virtual;
    procedure Execute; virtual; abstract;

    // IOTARepositoryWizard
    function GetAuthor: string; virtual; abstract;
    function GetComment: string; virtual; abstract;
    function GetPage: string; virtual; abstract;
    function GetGlyph: Cardinal; virtual;

    // IOTARepositoryWizard60
    function GetDesigner: string; dynamic;

    // IOTARepositoryWizard80
    function GetGalleryCategory: IOTAGalleryCategory; dynamic;
    function GetPersonality: string; dynamic;

  end;

  /// <summary>
  /// Класс статического эксперта репозитория.
  /// IDString, Name, Author, Comment, Page и Glyph задаются при создании.
  /// </summary>
  TSANStaticRepositoryWizard = class(TSANCustomRepositoryWizard)
  private
    FIDString, FName, FAuthor, FComment, FPage : string;
    FGlyph: Cardinal;

  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetAuthor: string; override;
    function GetComment: string; override;
    function GetPage: string; override;
    function GetGlyph: Cardinal; override;

  public
    constructor Create(const AIDString, AName, AAuthor, AComment, APage : string;
      const AGlyph: Cardinal);
  end;

IMPLEMENTATION

//==============================================================================
// TSANStaticWizard
// protected
function TSANStaticWizard.GetIDString: string;
begin
  Result := FIDString;
end;

function TSANStaticWizard.GetName: string;
begin
  Result := FName;
end;

function TSANStaticWizard.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

function TSANStaticWizard.Get_HInstace: LongWord;
begin
  Result := FInstance;
end;

// public
constructor TSANStaticWizard.Create(const AInstance : LongWord; const AIDString, AName: string);
begin
  inherited Create;

  FInstance := AInstance;
  FIDString := AIDString;
  FName := AName;
end;
//==============================================================================

//==============================================================================
// TSANCustomRepositoryWizard
// protected
function TSANCustomRepositoryWizard.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

function TSANCustomRepositoryWizard.GetGlyph: Cardinal;
begin
  Result := 0;
end;

function TSANCustomRepositoryWizard.GetDesigner: string;
begin
  Result := dVCL;
end;

function TSANCustomRepositoryWizard.GetGalleryCategory: IOTAGalleryCategory;
begin
  Result := (BorlandIDEServices as IOTAGalleryCategoryManager).FindCategory('Borland.Delphi.New.Expert');
end;

function TSANCustomRepositoryWizard.GetPersonality: string;
begin
  Result := sDelphiPersonality;
end;
//==============================================================================

//==============================================================================
// TSANStaticRepositoryWizard
// protected
function TSANStaticRepositoryWizard.GetIDString: string;
begin
  Result := FIDString;
end;

function TSANStaticRepositoryWizard.GetName: string;
begin
  Result := FName;
end;

function TSANStaticRepositoryWizard.GetAuthor: string;
begin
  Result := FAuthor;
end;

function TSANStaticRepositoryWizard.GetComment: string;
begin
  Result := FComment;
end;

function TSANStaticRepositoryWizard.GetPage: string;
begin
  Result := FPage;
end;

function TSANStaticRepositoryWizard.GetGlyph: Cardinal;
begin
  Result := FGlyph;
end;

// public
constructor TSANStaticRepositoryWizard.Create(const AIDString, AName, AAuthor,
  AComment, APage : string; const AGlyph: Cardinal);
begin
  inherited Create;

  FIDString      := AIDString;
  FName          := AName;
  FAuthor        := AAuthor;
  FComment       := AComment;
  FPage          := APage;

  FGlyph         := AGlyph;
end;
//==============================================================================

END.
