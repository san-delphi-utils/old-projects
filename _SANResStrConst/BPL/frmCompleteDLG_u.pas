UNIT frmCompleteDLG_u;

INTERFACE

USES
  SRSC_Types_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

TYPE
  TfrmCompleteDLG = class(TForm)
    lblHint: TLabel;
    pnlButtons: TPanel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    lstRess: TListBox;
    lblRes: TLabel;
    pnlForRes: TPanel;
    lblForRes: TLabel;
    edtForRes: TEdit;
    procedure lstRessClick(Sender: TObject);

  private
    function GetCurRes: TSRSC_Res;
    procedure SetCurRes(const Value: TSRSC_Res);
    { Private declarations }

  protected
    procedure FillResList(const AForRes : TSRSC_Res);

  public
    property CurRes : TSRSC_Res  read GetCurRes  write SetCurRes;

  end;//TfrmCompleteDLG

function CompleteDLG(const ACaption : string; const AOwner : TComponent;
  const AForRes : TSRSC_Res; var AFromRes : TSRSC_Res) : boolean;

IMPLEMENTATION

{$R *.dfm}

function CompleteDLG(const ACaption : string; const AOwner : TComponent;
  const AForRes : TSRSC_Res; var AFromRes : TSRSC_Res) : boolean;
begin

  with TfrmCompleteDLG.Create(AOwner) do
  try
    Caption := ACaption;

    edtForRes.Text := AForRes.Name;

    FillResList(AForRes);


    Result := (ShowModal = mrOK);


    if Result then
      AFromRes := CurRes;

  finally
    Free;
  end;

end;


//==============================================================================
// TfrmCompleteDLG
// private
function TfrmCompleteDLG.GetCurRes: TSRSC_Res;
begin
  with lstRess do
    if Count = 0 then
      Result := nil
    else
      Result := (Items.Objects[ItemIndex]) as TSRSC_Res;
end;

procedure TfrmCompleteDLG.SetCurRes(const Value: TSRSC_Res);
begin
  with lstRess do
    ItemIndex := Items.IndexOfObject(Value);
end;

// protected
procedure TfrmCompleteDLG.FillResList(const AForRes: TSRSC_Res);
var
  i : integer;
begin

  lstRess.Items.BeginUpdate;
  try

    for i := 0 to AForRes.OwnerList.Count - 1 do
      if AForRes.OwnerList[i] <> AForRes then
        lstRess.AddItem(AForRes.OwnerList[i].Name, AForRes.OwnerList[i]);

  finally
    lstRess.Items.EndUpdate;
  end;//t..f

end;

// published
procedure TfrmCompleteDLG.lstRessClick(Sender: TObject);
begin
  btnOK.Enabled := ((Sender as TListBox).ItemIndex > -1);
end;
//==============================================================================

END.
