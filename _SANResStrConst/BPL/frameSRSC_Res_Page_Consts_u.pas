UNIT frameSRSC_Res_Page_Consts_u;///////////////////////////////////////////////
// Страница "Константы"
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  SRSC_Types_u,
  frameCustomSRSC_Res_Page_u,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ActnList, StdCtrls, ExtCtrls, ComCtrls, Buttons, SynEdit,
  SynMemo, SynEditHighlighter, SynHighlighterPas, ToolWin, StrUtils, Math,
  SyncObjs, Menus, Clipbrd;
//==============================================================================
TYPE
  // Поток отслеживает изменение временного файла внешним редактором
  TExtEditerWatchThread = class(TThread)
  private
    FCSFileName, FCSEnabled, FCSFile : TCriticalSection;
    FTempDir, FFileName : string;
    FWndHandle : HWND;
    FEnabled : boolean;
    FHFile : THandle;

  private
    function Get_Enabled: boolean;
    procedure Set_Enabled(const Value: boolean);
    function Get_FileName: string;
    procedure Set_FileName(const Value: string);

  protected
    procedure Execute; override;

    procedure CloseHFile;
    procedure CloseHFileCr;

  public
    constructor Create(const ATempDir : string; const AWndHandle : HWND);
    destructor Destroy; override;

    procedure ReadStrings(const AStrings : TStrings);

  public
    property FileName : string  read Get_FileName  write Set_FileName;
    property Enabled : boolean  read Get_Enabled   write Set_Enabled;

  end;//TExtEditerWatchThread
//==============================================================================
  TframeSRSC_Res_Page_Consts = class(TframeCustomSRSC_Res_Page)
    splTree: TSplitter;
    pnlTree: TPanel;
    tlbTree: TToolBar;
    btnTreeAdd: TToolButton;
    btnTreeAddGr: TToolButton;
    btnTreeRename: TToolButton;
    btnTreeDelete: TToolButton;
    tvConsts: TTreeView;
    pnlText: TPanel;
    synmText: TSynMemo;
    pnlTextTop: TPanel;
    cmbxHL: TComboBox;
    pgscrlrExtEditers: TPageScroller;
    tlbExtEditers: TToolBar;
    pnlFuncCode: TPanel;
    lblFuncCode_f: TLabel;
    lblFuncCode_n: TLabel;
    lblFuncCode_ne: TLabel;
    lblFuncCode_t: TLabel;
    lblFuncCode_e: TLabel;
    imgFuncCode_Close: TImage;
    lblFuncCode_h: TLabel;
    syndtFuncCode_Code: TSynEdit;
    edtFuncCode_Prms: TEdit;
    actTreeAdd: TAction;
    actTreeAddGr: TAction;
    actTreeRename: TAction;
    actTreeDelete: TAction;
    synpsynFuncCode: TSynPasSyn;
    ilTree: TImageList;
    ilClose: TImageList;
    pmTree: TPopupMenu;
    miTreeAdd: TMenuItem;
    miTreeAddGr: TMenuItem;
    miTreeRename: TMenuItem;
    miTreeDelete: TMenuItem;
    tlbrFunc: TToolBar;
    btnFuncNotUsed: TToolButton;
    btnFuncHide: TToolButton;
    btnFuncShow: TToolButton;
    ilFunc: TImageList;
    ilFuncGray: TImageList;
    btnTreeAdvanced: TToolButton;
    pmTreeAdvanced: TPopupMenu;
    actTreeAdvComplete: TAction;
    miTreeAdvComplete: TMenuItem;
    miTreeAdvanced: TMenuItem;
    sepTreeAdvanced: TToolButton;
    btnConstCopyName: TToolButton;
    btnConstIsertIntoCode: TToolButton;
    actConstCopyName: TAction;
    actConstInsertIntoCode: TAction;
    miTreeSepAdvanced: TMenuItem;
    miConstCopyName: TMenuItem;
    miConstInsertIntoCode: TMenuItem;
    actTreeAdvCopy: TAction;
    miTreeAdvCopy: TMenuItem;

    procedure tvConstsChange(Sender: TObject; Node: TTreeNode);
    procedure tvConstsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvConstsKeyPress(Sender: TObject; var Key: Char);
    procedure tvConstsEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure tvConstsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure tvConstsDblClick(Sender: TObject);
    procedure tvConstsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvConstsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tvConstsCompare(Sender: TObject; Node1, Node2: TTreeNode;
      Data: Integer; var Compare: Integer);

    procedure actTreeAddExecute(Sender: TObject);
    procedure actTreeAddUpdate(Sender: TObject);
    procedure actTreeAddGrExecute(Sender: TObject);
    procedure actTreeAddGrUpdate(Sender: TObject);
    procedure actTreeRenameExecute(Sender: TObject);
    procedure actTreeRenameUpdate(Sender: TObject);
    procedure actTreeDeleteExecute(Sender: TObject);
    procedure actTreeDeleteUpdate(Sender: TObject);
    procedure actConstCopyNameExecute(Sender: TObject);
    procedure actConstCopyNameUpdate(Sender: TObject);
    procedure btnTreeAdvancedClick(Sender: TObject);
    procedure actTreeAdvCompleteExecute(Sender: TObject);
    procedure actTreeAdvCompleteUpdate(Sender: TObject);

    procedure cmbxHLChange(Sender: TObject);
    procedure btnsFuncClick(Sender: TObject);
    procedure edtFuncCode_PrmsChange(Sender: TObject);
    procedure syndtFuncCode_CodeChange(Sender: TObject);
    procedure imgFuncCode_CloseMouseEnter(Sender: TObject);
    procedure imgFuncCode_CloseMouseLeave(Sender: TObject);
    procedure imgFuncCode_CloseClick(Sender: TObject);
    procedure synmTextChange(Sender: TObject);
    procedure actConstInsertIntoCodeExecute(Sender: TObject);
    procedure actConstInsertIntoCodeUpdate(Sender: TObject);
    procedure actTreeAdvCopyExecute(Sender: TObject);
    procedure actTreeAdvCopyUpdate(Sender: TObject);

  private
    FExtEdChWatch : TExtEditerWatchThread;
    FOldConstUpper : boolean;

  private
    function Get_CurTreeItem: TSRSC_CustomResItem;
    procedure Set_CurTreeItem(const Value: TSRSC_CustomResItem);
    function Get_CurHL: TSynCustomHighlighter;

  protected
    procedure SetParent(AParent: TWinControl); override;

    procedure InitCmbxHL;

    function Tree_FindItemNode(const AItem : TSRSC_CustomResItem) : TTreeNode;
    function Tree_AddNode(const AItem : TSRSC_CustomResItem; const AParentNode : TTreeNode = nil) : TTreeNode;
    procedure Tree_AddItem(const AItemClass : TSRSC_CustomResItemClass; const ATemplate : TSRSC_CustomResItem = nil);
    procedure Tree_LoadConsts;
    procedure Tree_SetUpper;
    procedure Tree_ReEdit(var Msg: TMessage); message WM_USER + 2;

    procedure ExtEditers_Exec(Sender : TObject);
    procedure ExtEditers_Changed(var Msg: TMessage); message WM_USER + 1;
    procedure ExtEditers_ListChange(Sender: TObject);
    procedure ExtEditers_ReFill;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    class function PageName : string; override;
    class function PageCaption : string; override;

    procedure AfterResChange(const AOldRes, ANewRes: TSRSC_Res); override;
    procedure ResUpdated; override;

    procedure PageActivate; override;

    function CanCommit : boolean; override;
    procedure Commit; override;
    procedure Rollback; override;
    procedure SetFindCursor(const AFindCursor : TSRSC_FindCursor); override;



  public
    property CurTreeItem : TSRSC_CustomResItem  read Get_CurTreeItem  write Set_CurTreeItem;

    property CurHL : TSynCustomHighlighter  read Get_CurHL;

  end;//TframeSRSC_Res_Page_Consts
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
uses
  ShlObj, ShellAPI, CommCtrl,
  frmCompleteDLG_u;
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
function GetAssociatedIcon(const AFileOrDir : string; const ALarge : boolean): HIcon;
var
  vSHFILEINFO : TSHFileInfo;
  vFlags : integer;
begin
  vFlags := SHGFI_ICON or (SHGFI_LARGEICON * ORD(ALarge)) or (SHGFI_SMALLICON * ORD(not ALarge));
  if FileExists(AFileOrDir) then
    vFlags := vFlags or SHGFI_USEFILEATTRIBUTES;

  SHGetFileInfo(PChar(AFileOrDir), FILE_ATTRIBUTE_NORMAL, vSHFILEINFO, SizeOf(TSHFileInfo), vFlags);

  Result := vSHFILEINFO.hIcon;
end;//GetAssociatedIcon
//==============================================================================
procedure MassEnabled(const AEnabled : boolean; const AControls : array of TControl);
// Задать Enabled набору контролов
var
  i : integer;
begin
  for i := Low(AControls) to High(AControls) do
    AControls[i].Enabled := AEnabled;
end;//MassEnabled
//==============================================================================

//==============================================================================
// TExtEditerWatchThread
//==============================================================================
// private
//==============================================================================
function TExtEditerWatchThread.Get_FileName: string;
begin
  FCSFileName.Enter;
  try
    Result := FFileName;
  finally
    FCSFileName.Leave;
  end;//t..f
end;//TExtEditerWatchThread.Get_FileName
//==============================================================================
procedure TExtEditerWatchThread.Set_FileName(const Value: string);
begin
  FCSFileName.Enter;
  try
    FFileName := Value;
  finally
    FCSFileName.Leave;
  end;//t..f
end;//TExtEditerWatchThread.Set_FileName
//==============================================================================
function TExtEditerWatchThread.Get_Enabled: boolean;
begin
  FCSEnabled.Enter;
  try
    Result := FEnabled;
  finally
    FCSEnabled.Leave;
  end;//t..f
end;//TExtEditerWatchThread.Get_Enabled
//==============================================================================
procedure TExtEditerWatchThread.Set_Enabled(const Value: boolean);
begin
  FCSEnabled.Enter;
  try
    FEnabled := Value;
  finally
    FCSEnabled.Leave;
  end;//t..f
end;//TExtEditerWatchThread.Set_Enabled
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TExtEditerWatchThread.CloseHFile;
// Закрыть файл, если он был открыт.
begin
  if FHFile <> INVALID_HANDLE_VALUE then  begin
    CloseHandle(FHFile);
    FHFile := INVALID_HANDLE_VALUE;
  end;//if
end;//TExtEditerWatchThread.CloseHFile
//==============================================================================
procedure TExtEditerWatchThread.CloseHFileCr;
// Закрыть файл, если он был открыт.
// Критическая секция.
begin
  FCSFile.Enter;
  try
    CloseHFile;
  finally
    FCSFile.Leave;
  end;//t..f
end;//TExtEditerWatchThread.CloseHFileCr
//==============================================================================
procedure TExtEditerWatchThread.Execute;
var
  vCh : THandle;
  vRes, i : Integer;
  vFileName : string;
begin

  try

    vCh := FindFirstChangeNotification( PChar(FTempDir), False,
      FILE_NOTIFY_CHANGE_FILE_NAME or FILE_NOTIFY_CHANGE_ATTRIBUTES
      or FILE_NOTIFY_CHANGE_SIZE or FILE_NOTIFY_CHANGE_LAST_WRITE or FILE_NOTIFY_CHANGE_SECURITY
    );
    try

      repeat
        vRes := WaitForSingleObject(vCh, 200);

        if (vRes = WAIT_OBJECT_0) and (not Terminated) and Enabled then  try

          vFileName := FFileName;

          // Если есть дескриптор файла, закрыть его
          FCSFile.Enter;
          try

            CloseHFile;

            for i := 1 to 5 do  begin
              Sleep(500);

              FHFile := FileOpen(vFileName, fmOpenRead or fmShareDenyWrite);

              if (FHFile <> INVALID_HANDLE_VALUE) then  begin
                PostMessage(FWndHandle, WM_USER + 1, 0, 0);

                Break;
              end;//if
            end;//for

          finally
            FCSFile.Leave;
          end;//t..f

        finally
          FindNextChangeNotification(vCh);
        end;//t..f; if
        
        Sleep(100);
      until Terminated;

    finally
      FindCloseChangeNotification(vCh);
    end;//t..f


  finally
    CloseHFileCr;
  end;//t..f

end;//TExtEditerWatchThread.Execute
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TExtEditerWatchThread.Create(const ATempDir: string;
  const AWndHandle: HWND);
begin
  FTempDir    := ATempDir;
  FWndHandle  := AWndHandle;

  FCSFileName  := TCriticalSection.Create;
  FCSEnabled   := TCriticalSection.Create;
  FCSFile      := TCriticalSection.Create;

  FHFile := INVALID_HANDLE_VALUE;

  inherited Create(False);
end;//TExtEditerWatchThread.Create
//==============================================================================
destructor TExtEditerWatchThread.Destroy;
begin
  FreeAndNil(FCSFileName);
  FreeAndNil(FCSEnabled);
  FreeAndNil(FCSFile);

  inherited;
end;//TExtEditerWatchThread.Destroy
//==============================================================================
procedure TExtEditerWatchThread.ReadStrings(const AStrings: TStrings);
// Прочитать список строк из файла
var
  vStream: TStream;
begin

  FCSFile.Enter;
  try

    if FHFile <> INVALID_HANDLE_VALUE then
      try

        vStream := THandleStream.Create(FHFile);
        try
          AStrings.LoadFromStream(vStream);
        finally
          FreeAndNil(vStream);
        end;//t..f

      finally
        CloseHFile;
      end;//t..f; if

  finally
    FCSFile.Leave;
  end;//t..f

end;//TExtEditerWatchThread.ReadStrings
//==============================================================================

//==============================================================================
// TframeSRSC_Res_Page_Consts
//==============================================================================
// private
//==============================================================================
function TframeSRSC_Res_Page_Consts.Get_CurTreeItem: TSRSC_CustomResItem;
begin
  with tvConsts do
    if Assigned(Selected) then
      Result := TSRSC_CustomResItem(Selected.Data)
    else
      Result := nil;
end;//TframeSRSC_Res_Page_Consts.Get_CurTreeItem
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.Set_CurTreeItem(const Value: TSRSC_CustomResItem);
begin
  tvConsts.Selected := Tree_FindItemNode(Value);
end;//TframeSRSC_Res_Page_Consts.Set_CurTreeItem
//==============================================================================
function TframeSRSC_Res_Page_Consts.Get_CurHL: TSynCustomHighlighter;
begin

  with cmbxHL do
    if ItemIndex = -1 then
      Result := nil
    else
      Result := TSynCustomHighlighter(Items.Objects[ItemIndex]);

end;//TframeSRSC_Res_Page_Consts.Get_CurHL
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.SetParent(AParent: TWinControl);
begin
  inherited;

  if Assigned(AParent) then
    FExtEdChWatch.FWndHandle := Handle;

end;//TframeSRSC_Res_Page_Consts.SetParent
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.InitCmbxHL;
var
  i : integer;
  vHLNest : TComponent;
begin
  vHLNest := TComponent.Create(Self);
  vHLNest.Name := 'HLNest';

  cmbxHL.Items.BeginUpdate;
  try

    cmbxHL.Clear;

    cmbxHL.AddItem('PlainText', nil);

    with GetPlaceableHighlighters do
      for i := 0 to Count - 1 do
        cmbxHL.AddItem(Items[i].GetLanguageName, Items[i].Create(vHLNest));

    cmbxHL.ItemIndex := 0;

  finally
    cmbxHL.Items.EndUpdate;
  end;//t..f
end;//TframeSRSC_Res_Page_Consts.InitCmbxHL
//==============================================================================
function TframeSRSC_Res_Page_Consts.Tree_FindItemNode(const AItem: TSRSC_CustomResItem): TTreeNode;
// Поиск листа дерева по элемкнту
var
  i : integer;
begin

  with tvConsts do
    for i := 0 to Items.Count - 1 do
      if Items[i].Data = AItem then
        Exit(Items[i]);

  Result := nil;
end;//TframeSRSC_Res_Page_Consts.Tree_FindItemNode
//==============================================================================
function TframeSRSC_Res_Page_Consts.Tree_AddNode(const AItem: TSRSC_CustomResItem; const AParentNode: TTreeNode): TTreeNode;
// Добавить узел в дерево
begin

  if Assigned(AParentNode) then
    Result := tvConsts.Items.AddChildObject(AParentNode, AItem.Name, AItem)
  else
    Result := tvConsts.Items.AddObject(nil, AItem.Name, AItem);

  Result.ImageIndex := ORD(AItem is TSRSC_Group);
  Result.SelectedIndex := Result.ImageIndex;
end;//TframeSRSC_Res_Page_Consts.Tree_AddNode
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.Tree_AddItem(const AItemClass: TSRSC_CustomResItemClass;
  const ATemplate : TSRSC_CustomResItem = nil);
// Добавление элемента в дерево
// Если задан шаблон, делается копия шаблона
var
  vName : string;
  vParentNode : TTreeNode;
  vItem : TSRSC_CustomResItem;
begin

  if not Assigned(ATemplate) then
    vName := Res.GenUniqueName( AItemClass.UniqueBaseName )
  else
    vName := Res.GenUniqueName( ATemplate.Name );


  if Res.ConstUpper then
    vName := AnsiUpperCase(vName);


  vParentNode := tvConsts.Selected;
  while Assigned(vParentNode) and (not (TObject(vParentNode.Data) is TSRSC_Group)) do
    vParentNode := vParentNode.Parent;


  vItem := AItemClass.Create(Res.Items, vName);


  if not Assigned(ATemplate) then
    begin

      if vItem is TSRSC_Const then
        (vItem as TSRSC_Const).LangName := Res.DefLangName;

    end
  else
    begin

      vItem.Assign(ATemplate);
      vItem.Name := vName;

    end;

  Res.AddItem(vItem);


  if Assigned(vParentNode) and (TObject(vParentNode.Data) is TSRSC_Group) then
    begin
      vItem.Group := TSRSC_Group(vParentNode.Data);
      tvConsts.Selected := Tree_AddNode(vItem, vParentNode);
    end
  else
    tvConsts.Selected := Tree_AddNode(vItem);

  tvConsts.AlphaSort(True);

  tvConsts.Selected.EditText;

  ResChange;
end;//TframeSRSC_Res_Page_Consts.Tree_AddItem
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.Tree_LoadConsts;
//------------------------------------------------------------------------------
  procedure ExportToNode(const ANode : TTreeNode; const ACRIList : TSRSC_CustomResItemList);
  // Заполнение одного узла
  var
    i : integer;
    vNode : TTreeNode;
  begin

    for i := 0 to ACRIList.Count - 1 do  begin

      vNode := Tree_AddNode(ACRIList[i], ANode);

      if ACRIList[i] is TSRSC_Group then
        ExportToNode(vNode, (ACRIList[i] as TSRSC_Group).SubItems);

    end;//for

  end;//ExportToNode
//------------------------------------------------------------------------------
var
  i : integer;
  vNode : TTreeNode;
begin
  with tvConsts, Items do  begin

    BeginUpdate;
    try
      Clear;

      if Assigned(Res) then
        for i := 0 to Res.Items.Count - 1 do
          if Res.Items[i].Group = nil then  begin
            vNode := Tree_AddNode(Res.Items[i], nil);

            if Res.Items[i] is TSRSC_Group then
              ExportToNode(vNode, (Res.Items[i] as TSRSC_Group).SubItems);
          end;//if; for; if

       AlphaSort(True);
    finally
      EndUpdate;
    end;//t..f

    if Count > 0 then
      Selected := Item[0]
    else
      OnChange(tvConsts, nil);

  end;//with
end;//TframeSRSC_Res_Page_Consts.Tree_LoadConsts
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.Tree_SetUpper;
var
  i : integer;
begin

  with tvConsts, Items do  begin

    BeginUpdate;
    try

      for i := 0 to Count - 1 do
        if TObject(Item[i].Data) is TSRSC_Const then
          Item[i].Text := AnsiUpperCase(Item[i].Text);

    finally
      EndUpdate;
    end;//t..f

  end;//tvConsts

end;//TframeSRSC_Res_Page_Consts.Tree_SetUpper
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.Tree_ReEdit(var Msg: TMessage);
begin
  tvConsts.Selected.EditText;
end;//TframeSRSC_Res_Page_Consts.Tree_ReEdit
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.ExtEditers_Exec(Sender: TObject);
// Обработчик кнопки внешнего редактора
var
  vEE : TSRSC_ExtEditer;
  vProg, vFN, vPrm : string;
  F : System.Text;
  vStartInfo    : TStartupInfo;
  vProcInfo     : TProcessInformation;
begin

  FExtEdChWatch.Enabled := False;
  try

    // Получить параметры
    with (Sender as TToolButton) do  begin
      vEE := SRSCManager.ExtEditers[Index];

      vProg   := vEE.FileName;

      vFN := SRSCManager.ConstTempFileName(tvConsts.Selected.Text);

      vPrm  := StringReplace(
                 vEE.Params,
                 '$(File)',
                 vFN,
                 [rfReplaceAll, rfIgnoreCase]
               );
    end;//with


    // Сохранить константу во временный файл
    AssignFile(F, vFN);
    Rewrite(F);
    try
      Write(F, (CurTreeItem as TSRSC_Const).Data);
    finally
      CloseFile(F);
    end;//t..f


    // Запуск процесса
    FillChar(vStartInfo, SizeOf(TStartupInfo),        #0);
    FillChar(vProcInfo,  SizeOf(TProcessInformation), #0);
    with vStartInfo do  begin
      cb := SizeOf(TStartUpInfo);
      dwFlags := STARTF_USESHOWWINDOW or STARTF_FORCEONFEEDBACK;
      wShowWindow := SW_SHOWNORMAL;
    end;//with

    CreateProcess(
      nil, PChar(vProg + ' ' + vPrm), nil, nil, False,
      CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS,
      nil, nil, vStartInfo, vProcInfo
    );

  finally
    FExtEdChWatch.Enabled := True;
  end;//t..f
end;//TframeSRSC_Res_Page_Consts.ExtEditers_Exec
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.ExtEditers_Changed(var Msg: TMessage);
begin
  FExtEdChWatch.ReadStrings(synmText.Lines);

  synmText.OnChange(synmText);
end;//TframeSRSC_Res_Page_Consts.ExtEditers_Changed
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.ExtEditers_ReFill;
// Перезаполнение списка картинок внешних редакторов
var
  i : integer;
  vEE : TSRSC_ExtEditer;
  vTBtn : TToolButton;
begin

  while tlbExtEditers.ButtonCount > 0 do
    tlbExtEditers.Buttons[0].Free;

  tlbExtEditers.Images := SRSCManager.ExtEditersImages;
  for i := SRSCManager.ExtEditersCount - 1 downto 0 do  begin

    vEE := SRSCManager.ExtEditers[i];

    vTBtn := TToolButton.Create(Self);
    with vTBtn do  begin
      Name        := Format('tlbtnEE%d', [i]);
      Parent      := tlbExtEditers;
      Width       := tlbExtEditers.ButtonWidth;
      Height      := tlbExtEditers.ButtonHeight;
      ImageIndex  := vEE.ImageIndex;
      Hint        := 'Редактировать в:'                               + #13#10 +
                     '  ' + vEE.FileName                              + #13#10 +
                     '  ' + vEE.Params;

      OnClick     := ExtEditers_Exec;
    end;//with

  end;//for

end;//TframeSRSC_Res_Page_Consts.ExtEditers_ReFill
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.ExtEditers_ListChange(Sender: TObject);
begin
  ExtEditers_ReFill;
end;//TframeSRSC_Res_Page_Consts.ExtEditers_ListChange
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeSRSC_Res_Page_Consts.Create(AOwner: TComponent);

  procedure CopyTreeAdvancedMenu;
  var
    i : integer;
    vMI : TMenuItem;
  begin

    for i := 0 to pmTreeAdvanced.Items.Count - 1 do
    begin
      vMI := TMenuItem.Create(Self);
      vMI.Name    := pmTreeAdvanced.Items[i].Name + '_Copy';
      vMI.Action  := pmTreeAdvanced.Items[i].Action;
      miTreeAdvanced.Add(vMI);
    end;

  end;

begin
  inherited;

  InitCmbxHL;

//  ShowMessage( IntToStr( TextToShortCut('Alt+Shift+L') ) );

  ExtEditers_ReFill;

  lblFuncCode_h.Hint := lblFuncCode_h.Caption;

  ilClose.GetBitmap(0, imgFuncCode_Close.Picture.Bitmap);

  CopyTreeAdvancedMenu;

  FExtEdChWatch := TExtEditerWatchThread.Create(SRSCManager.AppDataTempPath, Handle);

  tvConstsChange(tvConsts, nil);
end;//TframeSRSC_Res_Page_Consts.Create
//==============================================================================
destructor TframeSRSC_Res_Page_Consts.Destroy;
begin

  FExtEdChWatch.Terminate;
  FExtEdChWatch.WaitFor;
  FreeAndNil(FExtEdChWatch);

  inherited;
end;//TframeSRSC_Res_Page_Consts.Destroy
//==============================================================================
class function TframeSRSC_Res_Page_Consts.PageName: string;
begin
  Result := 'Consts';
end;//TframeSRSC_Res_Page_Consts.PageName
//==============================================================================
class function TframeSRSC_Res_Page_Consts.PageCaption: string;
begin
  Result := 'Константы';
end;//TframeSRSC_Res_Page_Consts.PageCaption
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.AfterResChange(const AOldRes, ANewRes: TSRSC_Res);
begin

  if Assigned(Res) then
    begin
      FOldConstUpper := Res.ConstUpper;

      Tree_LoadConsts;
    end
  else
    tvConsts.Items.Clear;

end;//TframeSRSC_Res_Page_Consts.AfterResChange
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.ResUpdated;
begin

  if Assigned(Res) then  begin

    if Res.ConstUpper and not(FOldConstUpper) then
      Tree_SetUpper;


  end;//if

end;//TframeSRSC_Res_Page_Consts.ResUpdated
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.PageActivate;
begin
  tvConsts.SetFocus;
end;//TframeSRSC_Res_Page_Consts.PageActivate
//==============================================================================
function TframeSRSC_Res_Page_Consts.CanCommit: boolean;
begin
  Result := True;
end;//TframeSRSC_Res_Page_Consts.CanCommit
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.Commit;
begin
  inherited;

end;//TframeSRSC_Res_Page_Consts.Commit
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.Rollback;
begin

end;//TframeSRSC_Res_Page_Consts.Rollback
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.SetFindCursor(const AFindCursor: TSRSC_FindCursor);
begin
  CurTreeItem := AFindCursor.SRSCConst;

  if  (foText in AFindCursor.Options)
  and (AFindCursor.TextPos > 0)
  and (AFindCursor.TextPos <= Length(synmText.Text))
  then
    begin
      synmText.SelStart   := AFindCursor.TextPos - 1;
      synmText.SelLength  := Length(AFindCursor.Text);
    end;//if

end;//TframeSRSC_Res_Page_Consts.SetFindCursor
//==============================================================================
//==============================================================================
// published
//==============================================================================
// Tree
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsChange(Sender: TObject;
  Node: TTreeNode);
var
  vItem : TSRSC_CustomResItem;
  vIsConst : boolean;
  vOnChange : TNotifyEvent;
  i : integer;

  //
  vData, vLangName, vFuncParams, vFuncCode : string;
  vFuncAuto : boolean;
//    property Data        : string   read FData        write FData;
//    property LangName    : string   read FLangName    write FLangName;
//    property FuncParams  : string   read FFuncParams  write FFuncParams;
//    property FuncCode    : string   read FFuncCode    write FFuncCode;
//    property FuncAuto    : boolean  read FFuncAuto    write FFuncAuto;

begin

  // В обработчик руками может быть передан nil
  if Assigned(Node) then
    begin
      vItem     := TSRSC_CustomResItem(Node.Data);
      vIsConst  := (vItem is TSRSC_Const);
    end
  else
    begin
      vItem     := nil;
      vIsConst  := False;
    end;//else if


  MassEnabled(vIsConst, [cmbxHL, synmText, edtFuncCode_Prms, syndtFuncCode_Code]);

  for i := 0 to tlbrFunc.ButtonCount - 1 do
    with tlbrFunc.Buttons[i] do  begin
      Enabled := vIsConst;
      Down := False;
    end;//with; for

  for i := 0 to tlbExtEditers.ButtonCount - 1 do
    tlbExtEditers.Buttons[i].Enabled := vIsConst;


  // Подсветка
  with cmbxHL do
    if vIsConst then
      begin
        ItemIndex := MAX(Items.IndexOf( (vItem as TSRSC_Const).LangName ), 0);
        synmText.Highlighter := CurHL;
      end

    else
      ItemIndex := 0;


  // Передача через локальные переменные
  if vIsConst then
    with (vItem as TSRSC_Const) do  begin
      vData        := Data;
      vLangName    := LangName;
      vFuncParams  := FuncParams;
      vFuncCode    := FuncCode;
      vFuncAuto    := FuncAuto;
    end
  else
    begin
      vData        := '';
      vLangName    := '';
      vFuncParams  := '';
      vFuncCode    := '';
      vFuncAuto    := False;
    end;//else if


  // Текст
  vOnChange := synmText.OnChange;
  try
    synmText.OnChange := nil;
    synmText.Text := vData;
  finally
    synmText.OnChange := vOnChange;
  end;//t..f


  // Функция
  if vIsConst then
    with tlbrFunc.Buttons[ ORD(vFuncAuto) ] do  begin
      Down := True;
      Click;
    end
  else
    pnlFuncCode.Hide;


  vOnChange := edtFuncCode_Prms.OnChange;
  try
    edtFuncCode_Prms.OnChange := nil;
    edtFuncCode_Prms.Text    := vFuncParams;
  finally
    edtFuncCode_Prms.OnChange := vOnChange;
  end;//t..f

  vOnChange := syndtFuncCode_Code.OnChange;
  try
    syndtFuncCode_Code.OnChange := nil;
    syndtFuncCode_Code.Text := vFuncCode;
  finally
    syndtFuncCode_Code.OnChange := vOnChange;
  end;//t..f



  // Внешние редакторы
  if vIsConst then
    FExtEdChWatch.FileName := SRSCManager.ConstTempFileName(Node.Text)
  else
    FExtEdChWatch.FileName := '';

end;//TframeSRSC_Res_Page_Consts.tvConstsChange
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  with Sender as TTreeView do
    if not IsEditing then
      case Key of

        VK_INSERT:
          if ssShift in Shift then
            actTreeAddGr.Execute
          else
            actTreeAdd.Execute;

        VK_F2:
          actTreeRename.Execute;

        VK_DELETE:
          actTreeDelete.Execute;

      end;//case; if; with

end;//TframeSRSC_Res_Page_Consts.tvConstsKeyDown
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsKeyPress(Sender: TObject;
  var Key: Char);
begin

  if not CharInSet(Key, LETTERS + DIGITS + EDIT_KEYS) then  begin
    Key := #0;
    MessageBeep(MB_OK);
    Exit;
  end;//if

  if Res.ConstUpper then
    Key := AnsiUpperCase(Key)[1];

end;//TframeSRSC_Res_Page_Consts.tvConstsKeyPress
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsEdited(Sender: TObject;
  Node: TTreeNode; var S: string);
begin

  if Res.Items.Exists(S, CurTreeItem) then
  begin
    MessageBox(
      Handle,
      PChar('Уже есть константа или группа констант с именем «' + S + '»!'),
      'Имя не уникально',
      MB_ICONWARNING
    );

    PostMessage(Handle, WM_USER + 2, 0, 0);

    Exit;
  end;//if


  CurTreeItem.Name := S;

  Res.Items.SortByOrder;
  (Sender as TTreeView).AlphaSort(True);

  ResChange;
end;//TframeSRSC_Res_Page_Consts.tvConstsEdited
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  vNode : TTreeNode;
begin

  if Button = mbRight then
    with Sender as TTreeView do  begin
      vNode := GetNodeAt(X, Y);

      if Assigned(vNode) then
        Selected := vNode;

      with ClientToScreen(Point(X, Y)) do
        pmTree.Popup(X, Y);
    end;//with

end;//TframeSRSC_Res_Page_Consts.tvConstsMouseDown
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsDblClick(Sender: TObject);
begin

  if Assigned(CurTreeItem) and (CurTreeItem is TSRSC_Const) then
    synmText.SetFocus;

end;//TframeSRSC_Res_Page_Consts.tvConstsDblClick
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  vNode : TTreeNode;
begin

  if Sender <> Source then
    Accept := False

  else
    with Sender as TTreeView do  begin
      vNode := GetNodeAt(X, Y);

      if vNode = nil then
        Accept := Assigned(Selected.Parent)

      else
        Accept := not (
                        (vNode = Selected)
                          or
                        vNode.HasAsParent(Selected)
                          or
                        (Selected.Parent = vNode)
                          or
                        (TObject(vNode.Data) is TSRSC_Const)
                      );

    end;//with; else if

end;//TframeSRSC_Res_Page_Consts.tvConstsDragOver
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsDragDrop(Sender, Source: TObject;
  X, Y: Integer);
var
  vNode : TTreeNode;
  vCRI : TSRSC_CustomResItem;
begin
  with (Sender as TTreeView) do  begin
    vNode := GetNodeAt(X, Y);

    vCRI := CurTreeItem;

    if vNode = nil then
      begin
        vCRI.Group := nil;
        Selected.MoveTo(vNode, naAdd);
      end

    else
      begin
        vCRI.Group := TSRSC_Group(vNode.Data);
        Selected.MoveTo(vNode, naAddChild);
      end;//else if

    Res.Items.SortByOrder;
    AlphaSort(True);

    ResChange;
  end;//with
end;//TframeSRSC_Res_Page_Consts.tvConstsDragDrop
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.tvConstsCompare(Sender: TObject; Node1,
  Node2: TTreeNode; Data: Integer; var Compare: Integer);
begin
  Compare := Compare_SRSC_LGCN(Node1.Data, Node2.Data)
end;//TframeSRSC_Res_Page_Consts.tvConstsCompare
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeAddExecute(Sender: TObject);
begin
  Tree_AddItem(TSRSC_Const);
end;//TframeSRSC_Res_Page_Consts.actTreeAddExecute
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeAddUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not tvConsts.IsEditing;
end;//TframeSRSC_Res_Page_Consts.actTreeAddUpdate
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeAddGrExecute(Sender: TObject);
begin
  Tree_AddItem(TSRSC_Group);
end;//TframeSRSC_Res_Page_Consts.actTreeAddGrExecute
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeAddGrUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not tvConsts.IsEditing;
end;//TframeSRSC_Res_Page_Consts.actTreeAddGrUpdate
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeRenameExecute(Sender: TObject);
begin
  tvConsts.Selected.EditText;
end;//TframeSRSC_Res_Page_Consts.actTreeRenameExecute
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeRenameUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(tvConsts.Selected);
end;//TframeSRSC_Res_Page_Consts.actTreeRenameUpdate
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeDeleteExecute(Sender: TObject);
begin

  if MessageBox(
      Handle,
      PChar(
        Format(
          IfThen(
            CurTreeItem is TSRSC_Group,
            'Удалить группу «%s» со всеми дочерними элементами?',
            'Удалить константу «%s»?'
          ),
          [CurTreeItem.Name]
        )
      ),
      'Внимание',
      MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2
    ) = ID_NO
  then
    Exit;


  CurTreeItem.Free;
  tvConsts.Items.Delete(tvConsts.Selected);


  if tvConsts.Items.Count = 0 then
    tvConsts.OnChange(tvConsts, nil);

  ResChange;
end;//TframeSRSC_Res_Page_Consts.actTreeDeleteExecute
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeDeleteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(tvConsts.Selected);
end;//TframeSRSC_Res_Page_Consts.actTreeDeleteUpdate
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actConstCopyNameExecute(Sender: TObject);
begin
  with (CurTreeItem as TSRSC_Const) do
    Clipboard.AsText := Name + IfThen(FuncParams <> '', '()');
end;
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actConstCopyNameUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(tvConsts.Selected) and (CurTreeItem is TSRSC_Const);
end;
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actConstInsertIntoCodeExecute(Sender: TObject);

  procedure CloseParentForm;
  var
    vWCt : TWinControl;
  begin

    vWCt := Parent;
    while Assigned(vWCt) do
    begin

      if vWCt is TCustomForm then
      begin
        (vWCt as TCustomForm).Close;
        Exit;
      end;

      vWCt := vWCt.Parent;
    end;

  end;

var
  C : TSRSC_Const;
begin
  C := CurTreeItem as TSRSC_Const;

  if not Res.OwnerList.ProjectRes.Provider.InsertIntoSource(C.Name + IfThen(C.FuncParams <> '', '()')) then
  begin
    MessageBox(Handle, 'Невозможно вставить текст в код проекта.', 'Сообщение', MB_ICONWARNING);
    Exit;
  end;

  CloseParentForm;
end;
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actConstInsertIntoCodeUpdate(
  Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(tvConsts.Selected) and (CurTreeItem is TSRSC_Const);
end;
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.btnTreeAdvancedClick(Sender: TObject);
var
  vPt : TPoint;
begin

  with (Sender as TControl) do
    vPt := ClientToScreen( Point(0, Height) );

  pmTreeAdvanced.Popup(vPt.X, vPt.Y);
end;//TframeSRSC_Res_Page_Consts.btnTreeAdvancedClick
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeAdvCompleteExecute(Sender: TObject);
var
  vRes : TSRSC_Res;
begin
  vRes := nil;

  if not CompleteDLG((Sender as TAction).Caption, Self, Res, vRes) then
    Exit;

  if not Res.CompleteFrom(vRes) then
    Exit;

  Tree_LoadConsts;

  ResChange;
end;//TframeSRSC_Res_Page_Consts.actTreeAdvCompleteExecute
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeAdvCompleteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(Res) and (Res.OwnerList.Count > 1);
end;//TframeSRSC_Res_Page_Consts.actTreeAdvCompleteUpdate
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeAdvCopyExecute(Sender: TObject);
begin
  Tree_AddItem(TSRSC_Const, CurTreeItem);
end;//TframeSRSC_Res_Page_Consts.actTreeAdvCopyExecute
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.actTreeAdvCopyUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(tvConsts.Selected) and (CurTreeItem is TSRSC_Const);
end;//TframeSRSC_Res_Page_Consts.actTreeAdvCopyUpdate
//==============================================================================
// Text
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.cmbxHLChange(Sender: TObject);
begin
  synmText.Highlighter := CurHL;

  if Assigned(CurTreeItem) and (CurTreeItem is TSRSC_Const) then  begin

    with (Sender as TComboBox) do
      (CurTreeItem as TSRSC_Const).LangName := Items[ItemIndex];

    ResChange;
  end;//if

end;//TframeSRSC_Res_Page_Consts.cmbxHLChange
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.btnsFuncClick(Sender: TObject);
var
  vFuncAuto : boolean;
begin

  with Sender as TToolButton do  begin
    pnlFuncCode.Visible := (Index = 2);

    vFuncAuto := (Index > 0);

    if   Assigned(CurTreeItem)
    and (CurTreeItem is TSRSC_Const)
    and ((CurTreeItem as TSRSC_Const).FuncAuto <> vFuncAuto)
    then  begin
      (CurTreeItem as TSRSC_Const).FuncAuto := vFuncAuto;

      ResChange;
    end;//if

  end;//with

end;//TframeSRSC_Res_Page_Consts.btnsFuncClick
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.synmTextChange(Sender: TObject);
begin

  if Assigned(CurTreeItem) and (CurTreeItem is TSRSC_Const) then  begin

    (CurTreeItem as TSRSC_Const).Data := (Sender as TSynMemo).Text;

    ResChange;
  end;//if

end;//TframeSRSC_Res_Page_Consts.synmTextChange
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.edtFuncCode_PrmsChange(Sender: TObject);
begin

  if Assigned(CurTreeItem) and (CurTreeItem is TSRSC_Const) then  begin

    (CurTreeItem as TSRSC_Const).FuncParams := Trim( (Sender as TEdit).Text );

    ResChange;
  end;//if

end;//TframeSRSC_Res_Page_Consts.edtFuncCode_PrmsChange
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.syndtFuncCode_CodeChange(Sender: TObject);
begin

  if Assigned(CurTreeItem) and (CurTreeItem is TSRSC_Const) then  begin

    (CurTreeItem as TSRSC_Const).FuncCode := Trim( (Sender as TSynEdit).Text );

    ResChange;
  end;//if

end;//TframeSRSC_Res_Page_Consts.syndtFuncCode_CodeChange
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.imgFuncCode_CloseMouseEnter(Sender: TObject);
begin
  with (Sender as TImage), Picture do  begin
    ilClose.GetBitmap(1, Bitmap);
    Refresh;
  end;//with
end;//TframeSRSC_Res_Page_Consts.imgFuncCode_CloseMouseEnter
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.imgFuncCode_CloseMouseLeave(Sender: TObject);
begin
  with (Sender as TImage), Picture do  begin
    ilClose.GetBitmap(0, Bitmap);
    Refresh;
  end;//with
end;//TframeSRSC_Res_Page_Consts.imgFuncCode_CloseMouseLeave
//==============================================================================
procedure TframeSRSC_Res_Page_Consts.imgFuncCode_CloseClick(Sender: TObject);
begin
  btnFuncHide.Down := True;
  btnFuncHide.Click;
end;//TframeSRSC_Res_Page_Consts.imgFuncCode_CloseClick
//==============================================================================


INITIALIZATION//////////////////////////////////////////////////////////////////
  TframeSRSC_Res_Page_Consts.PagesClasses_Reg;
FINALIZATION////////////////////////////////////////////////////////////////////
  TframeSRSC_Res_Page_Consts.PagesClasses_UnReg;
END.

