UNIT frameSRSC_Res_u;///////////////////////////////////////////////////////////
// Фрейм редактирования одного набора ресурсов
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  SRSC_Types_u,
  frameCustomSRSC_Res_Page_u,

  frameSRSC_Res_Page_General_u,
  frameSRSC_Res_Page_Consts_u,

  Windows, Messages, SysUtils, Variants, Classes, Contnrs, Graphics, Controls, Forms,
  Dialogs, ComCtrls;
//==============================================================================
TYPE
  TPageControl = class(ComCtrls.TPageControl)
  private
    procedure WMLButtonDown(var Message: TWMLButtonDown); message WM_LBUTTONDOWN;

  end;//TPageControl
//==============================================================================
  TframeSRSC_Res = class(TFrame, ISRSC_ResEditer)
    pcResPages: TPageControl;
    procedure pcResPagesChange(Sender: TObject);

  private
    FOldPageIndex : integer;

  private
    // Ссылка на набор констант
    FRes : TSRSC_Res;

  private
    function Get_PageCount: integer;
    function Get_Pages(const Index : integer): TframeCustomSRSC_Res_Page;
    function Get_CurPage: TframeCustomSRSC_Res_Page;

  protected
    procedure SetParent(AParent: TWinControl); override;

    // ISRSC_ResEditer
    procedure Set_Res(const Value: TSRSC_Res);
    function Get_Res: TSRSC_Res;
    procedure ResUpdated;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    function CanCommit : boolean;
    procedure Commit;
    procedure Rollback;

    procedure SetFindCursor(const AFindCursor : TSRSC_FindCursor);

  public
    property Res : TSRSC_Res  read Get_Res  write Set_Res;

    property PageCount : integer  read Get_PageCount;
    property Pages[const Index : integer] : TframeCustomSRSC_Res_Page  read Get_Pages;
    property CurPage : TframeCustomSRSC_Res_Page  read Get_CurPage;

  end;//TframeSRSC_Res
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

type
  TWctHack = class(TWinControl);

//==============================================================================
// TPageControl
//==============================================================================
// private
//==============================================================================
procedure TPageControl.WMLButtonDown(var Message: TWMLButtonDown);
var
//  vTabIndex : integer;
  vAllow : boolean;
begin
  vAllow := True;

//  vTabIndex := IndexOfTabAt(Message.XPos, Message.YPos);

  with (Owner as TframeSRSC_Res) do
    Pages[FOldPageIndex].PageDeactivate(vAllow);

//  if (vTabIndex > -1) and PtInRect(TabCloseButtonRect(vTabIndex, vTabIndex = TabIndex), Point(Message.XPos, Message.YPos)) then
//    if AllowCloseButton(vTabIndex) then
//      CloseButtonClick(vTabIndex, vHandled);


  if vAllow then
    inherited;

end;//TPageControl.WMLButtonDown
//==============================================================================

//==============================================================================
// TframeSRSC_Res
//==============================================================================
// private
//==============================================================================
function TframeSRSC_Res.Get_PageCount: integer;
begin
  Result := pcResPages.PageCount;
end;//TframeSRSC_Res.Get_PageCount
//==============================================================================
function TframeSRSC_Res.Get_Pages(const Index : integer): TframeCustomSRSC_Res_Page;
begin
  Result := (pcResPages.Pages[Index].Controls[0] as TframeCustomSRSC_Res_Page)
end;//TframeSRSC_Res.Get_Pages
//==============================================================================
function TframeSRSC_Res.Get_CurPage: TframeCustomSRSC_Res_Page;
begin
  Result := (pcResPages.ActivePage.Controls[0] as TframeCustomSRSC_Res_Page)
end;//TframeSRSC_Res.Get_CurPage
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TframeSRSC_Res.SetParent(AParent: TWinControl);
var
  i : integer;
begin
  inherited;

  if Assigned(AParent) then
    for i := 0 to PageCount - 1 do
      with TWctHack(Pages[i]) do
        SetParent(Parent);

end;//TframeSRSC_Res.SetParent
//==============================================================================
function TframeSRSC_Res.Get_Res: TSRSC_Res;
begin
  Result := FRes
end;//TframeSRSC_Res.Get_Res: TSRSC_Res
//==============================================================================
procedure TframeSRSC_Res.Set_Res(const Value: TSRSC_Res);
var
  vOld : TSRSC_Res;
  i : integer;
begin
  vOld := FRes;


  for i := 0 to PageCount - 1 do
    Pages[i].BeforeResChange(vOld, Value);


  FRes := Value;


  for i := 0 to PageCount - 1 do
    Pages[i].AfterResChange(vOld, Value);

end;//TframeSRSC_Res.Set_Res
//==============================================================================
procedure TframeSRSC_Res.ResUpdated;
var
  i : integer;
begin
  for i := 0 to PageCount - 1 do
    Pages[i].ResUpdated;

  (Owner as ISRSC_ResListEditer).ResChange(Res);
end;//TframeSRSC_Res.ResUpdated
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeSRSC_Res.Create(AOwner: TComponent);
var
  i     : integer;
  vTS   : TTabSheet;
  vPFC  : TframeSRSCResPageClass;
  vPF   : TframeCustomSRSC_Res_Page;
begin
  inherited;

  for i := 0 to TframeCustomSRSC_Res_Page.PagesClassesCount - 1 do  begin
    vPFC := TframeCustomSRSC_Res_Page.PagesClasses(i);

    vTS := TTabSheet.Create(Self);
    vTS.Name    := 'ts' + vPFC.PageName;
    vTS.Caption := vPFC.PageCaption;
    vTS.PageControl := pcResPages;

    vPF := vPFC.Create(Self);
    vPF.Name := 'fm'+ vPFC.PageName;

    vPF.Parent := vTS;

    vPF.Align := alClient;
  end;//for

  pcResPages.ActivePageIndex := 1;
end;//TframeSRSC_Res.Create
//==============================================================================
destructor TframeSRSC_Res.Destroy;
begin

  inherited;
end;//TframeSRSC_Res.Destroy
//==============================================================================
function TframeSRSC_Res.CanCommit: boolean;
// Возможность принять изменения
var
  i : integer;
begin

  for i := 0 to PageCount - 1 do
    if not Pages[i].CanCommit then
      Exit(False);

  Result := True;
end;// TframeSRSC_Res.CanCommit
//==============================================================================
procedure TframeSRSC_Res.Commit;
// Принять изменения
var
  i : integer;
begin

  for i := 0 to PageCount - 1 do
    Pages[i].Commit;

end;//TframeSRSC_Res.Commit
//==============================================================================
procedure TframeSRSC_Res.Rollback;
// Откатить изменения
var
  i : integer;
begin

  for i := 0 to PageCount - 1 do
    Pages[i].Rollback;

end;//TframeSRSC_Res.Rollback
//==============================================================================
procedure TframeSRSC_Res.SetFindCursor(const AFindCursor: TSRSC_FindCursor);
// Выделение найденого элемента
var
  i : integer;
begin

  for i := 0 to PageCount - 1 do
    Pages[i].SetFindCursor(AFindCursor);

  pcResPages.ActivePageIndex := 1;
end;//TframeSRSC_Res.SetFindCursor
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeSRSC_Res.pcResPagesChange(Sender: TObject);
begin

  with Sender as TPageControl do  begin
    CurPage.PageActivate;

    FOldPageIndex := ActivePageIndex;
  end;//with

end;//TframeSRSC_Res.pcResPagesChange
//==============================================================================



END.
