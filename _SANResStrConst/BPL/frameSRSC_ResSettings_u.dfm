object frameSRSC_ResSettings: TframeSRSC_ResSettings
  Left = 0
  Top = 0
  Width = 232
  Height = 164
  TabOrder = 0
  object lblDeclUnitFmt: TLabel
    Left = 8
    Top = 8
    Width = 215
    Height = 26
    Caption = 
      #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1084#1086#1076#1091#1083#1103' '#1076#1086#1089#1090#1091#1087#1072#13#10'  (%s '#1079#1072#1084#1077#1085#1103#1077#1090#1089#1103' '#1085#1072' '#1080#1084#1103' '#1085#1072#1073#1086#1088#1072' '#1088#1077#1089#1091 +
      #1088#1089#1086#1074')'
    WordWrap = True
  end
  object lblResPrefixFmt: TLabel
    Left = 8
    Top = 72
    Width = 215
    Height = 26
    Caption = 
      #1060#1086#1088#1084#1072#1090' '#1087#1088#1077#1092#1080#1082#1089#1072' '#1080#1084#1077#1085'  '#1088#1077#1089#1091#1088#1089#1086#1074#13#10'  (%s '#1079#1072#1084#1077#1085#1103#1077#1090#1089#1103' '#1085#1072' '#1080#1084#1103' '#1085#1072#1073#1086#1088#1072' '#1088 +
      #1077#1089#1091#1088#1089#1086#1074')'
    WordWrap = True
  end
  object edtDeclUnitFmt: TEdit
    Left = 8
    Top = 40
    Width = 215
    Height = 21
    TabOrder = 0
    Text = 'edtDeclUnitFmt'
  end
  object edtResPrefixFmt: TEdit
    Left = 8
    Top = 106
    Width = 215
    Height = 21
    TabOrder = 1
    Text = 'edtResPrefixFmt'
  end
  object chkConstsUpper: TCheckBox
    Left = 8
    Top = 141
    Width = 217
    Height = 17
    Caption = #1058#1086#1083#1100#1082#1086' '#1074#1077#1088#1093#1085#1080#1081' '#1088#1077#1075#1080#1089#1090#1088' '#1080#1084#1077#1085' '#1082#1086#1085#1089#1090#1072#1085#1090
    TabOrder = 2
  end
end
