UNIT frameSRSC_Res_Page_General_u;//////////////////////////////////////////////
// Страница "Общие"
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  SRSC_Types_u,
  frameCustomSRSC_Res_Page_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ActnList, Grids, ValEdit, ExtCtrls, StdCtrls, StrUtils,

  SynEditHighlighter;
//==============================================================================
TYPE
  TframeSRSC_Res_Page_General = class(TframeCustomSRSC_Res_Page)
    vleGnrl: TValueListEditor;
    shpName: TShape;
    shpDeclUnit: TShape;
    procedure vleGnrlStringsChange(Sender: TObject);
    procedure vleGnrlKeyPress(Sender: TObject; var Key: Char);

  private
    // Предидущие значения параметров VLE
    FOldValues : TStrings;

    // Флаги корректности имен
    FResNameCrt, FResDeclUnitCrt : boolean;

  private
    function Get_Vls(const Index: integer): string;
    procedure Set_Vls(const Index: integer; const Value: string);

  protected
    procedure ValidVlsIndex(const Index: integer);
    procedure ValidResName;
    procedure ValidResDeclUnit;

    procedure SetVLEPickList(const AItemIndex, ADefIndex : integer; const APickListArr : array of string);
    procedure InitUnCpPickLists;
    procedure InitVLE;
    procedure FillVLE;
    procedure UpdateVLE;

    procedure PPChange(var Msg : TMessage); message WM_USER + 1;

  public
    class function PageName : string; override;
    class function PageCaption : string; override;

    procedure AfterResChange(const AOldRes, ANewRes: TSRSC_Res); override;
    procedure ResUpdated; override;

    procedure PageDeactivate(var Allow : boolean); override;
    procedure PageActivate; override;

    function CanCommit : boolean; override;
    procedure Commit; override;
    procedure Rollback; override;

    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

  public
    property Vls[const Index : integer] : string  read Get_Vls  write Set_Vls;

  end;//TframeSRSC_Res_Page_General
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

type
  TVLEHack = class(TValueListEditor);

//==============================================================================
const
  G_RES_IN_DLL       : array[boolean] of string = ('В ресурсах проекта', 'В отдельной DLL');
  G_RES_CONST_UPPER  : array[boolean] of string = ('Любой регистр',      'ВЕРХНИЙ РЕГИСТР');
//==============================================================================
function CorrectName(const AName : string) : boolean;
var
  i : integer;
begin

  if (Length(AName) = 0) or not (CharInSet(AName[1], LETTERS)) then
    Exit(False);

  for i := 2 to Length(AName) do
    if not (CharInSet(AName[1], LETTERS + DIGITS)) then
      Exit(False);

  Result := True;
end;//CorrectName
//==============================================================================

//==============================================================================
// TframeSRSC_Res_Page_General
//==============================================================================
// private
//==============================================================================
function TframeSRSC_Res_Page_General.Get_Vls(const Index: integer): string;
begin
  ValidVlsIndex(Index);

  with vleGnrl do
    Result := Cells[1, Index];

end;//TframeSRSC_Res_Page_General.Get_Vls
//==============================================================================
procedure TframeSRSC_Res_Page_General.Set_Vls(const Index: integer; const Value: string);
begin
  ValidVlsIndex(Index);

  with vleGnrl do
    Cells[1, Index] := Value;

end;//TframeSRSC_Res_Page_General.Set_Vls
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TframeSRSC_Res_Page_General.ValidVlsIndex(const Index: integer);
begin

  if (Index < 1) or (Index > vleGnrl.RowCount - 1) then
    RaisePageExcpFmt('Неверный индекс параметра! Допустимо [1..%d].', [vleGnrl.RowCount - 1]);

end;//TframeSRSC_Res_Page_General.ValidVlsIndex
//==============================================================================
procedure TframeSRSC_Res_Page_General.ValidResName;
begin
  FResNameCrt := CorrectName(Res.Name) and not Res.OwnerList.Exists(Res.Name, Res);

  if FResNameCrt then
    begin
      shpName.Brush.Color := clLime;
      shpName.Hint := 'Допустимое имя';
    end
  else
    begin
      shpName.Brush.Color := clRed;

      if Res.Name = '' then
        shpName.Hint := 'Имя не задано'
      else  if not CorrectName(Res.Name) then
        shpName.Hint := 'Имя некорректно'
      else
        shpName.Hint := 'Имя не уникально'
    end;//else if

end;//TframeSRSC_Res_Page_General.ValidResName
//==============================================================================
procedure TframeSRSC_Res_Page_General.ValidResDeclUnit;
begin
  FResDeclUnitCrt := CorrectName(Res.DeclUnit);

  if FResDeclUnitCrt then
    begin
      shpDeclUnit.Brush.Color := clLime;
      shpDeclUnit.Hint := 'Допустимое имя модуля';
    end
  else
    begin
      shpDeclUnit.Brush.Color := clRed;

      if Res.DeclUnit = '' then
        shpDeclUnit.Hint := 'Имя модуля не задано'
      else  if not CorrectName(Res.DeclUnit) then
        shpDeclUnit.Hint := 'Имя модуля некорректно'
      else
        shpDeclUnit.Hint := 'Имя модуля не уникально'
    end;//else if

end;//TframeSRSC_Res_Page_General.ValidResDeclUnit
//==============================================================================
procedure TframeSRSC_Res_Page_General.SetVLEPickList(const AItemIndex,
  ADefIndex: integer; const APickListArr: array of string);
// Установить значения PickList для одного ItemProps
var
  i : integer;
  vDef : string;
begin
  with vleGnrl.ItemProps[AItemIndex] do  begin
    EditStyle := esPickList;

    for i := Low(APickListArr) to High(APickListArr) do
      PickList.Add( APickListArr[i] );

    ReadOnly := True;

    if ADefIndex >= 0 then
      vDef := PickList[ADefIndex]
    else
      vDef := '';

    vleGnrl.Strings.ValueFromIndex[AItemIndex] := vDef;
  end;//with
end;//TframeSRSC_Res_Page_General.SetVLEPickList
//==============================================================================
procedure TframeSRSC_Res_Page_General.InitUnCpPickLists;
// Инициализация выпадающих списков имен модулей и префиксов констант
var
  vOnChange : TNotifyEvent;
  i, j : integer;
  S : string;
begin
  with vleGnrl do
  begin

    vOnChange := OnStringsChange;
    try
      OnStringsChange := nil;

      for j := 1 to 2 do
        with ItemProps[j] do
        begin
          EditStyle := esPickList;

          PickList.BeginUpdate;
          try
            PickList.Clear;

            case j of
              // UnitName
              1:
                for i := 0 to Res.OwnerList.Count - 1 do
                begin
                  S := Trim(Res.OwnerList[i].DeclUnit);

                  if (S <> '') and (PickList.IndexOf(S) = -1) then
                    PickList.Add(S);
                end;//for;1

              // ConstPrefix
              2:
                for i := 0 to Res.OwnerList.Count - 1 do
                begin
                  S := Trim(Res.OwnerList[i].ResPrefix);

                  if (S <> '') and (PickList.IndexOf(S) = -1) then
                    PickList.Add(S);
                end;//for;2

              else
                raise Exception.Create('It`s imposible!');
            end;

          finally
            PickList.EndUpdate;
          end;//t..f

        end;//with; for

    finally
      OnStringsChange := vOnChange;
    end;//t..f

  end;//with
end;//TframeSRSC_Res_Page_General.InitUnCpPickLists
//==============================================================================
procedure TframeSRSC_Res_Page_General.InitVLE;
// Инициализировать список параметров
var
  vOnChange : TNotifyEvent;
  i : integer;
begin

  with vleGnrl do  begin

    vOnChange := OnStringsChange;
    try
      OnStringsChange := nil;

      with Strings do  begin

        BeginUpdate;
        try

          // DefLangName
          with ItemProps[3] do  begin
            EditStyle := esPickList;

          PickList.Add( PLAIN_TEXT );
          with GetPlaceableHighlighters do
            for i := 0 to Count - 1 do
              PickList.Add( Items[i].GetLanguageName );

            ReadOnly := True;

            ValueFromIndex[3] := PLAIN_TEXT;
          end;//with

          // InDll
          SetVLEPickList(4, 0, G_RES_IN_DLL);

          // ConstUpper
          SetVLEPickList(5, 0, G_RES_CONST_UPPER);

          FOldValues.Assign(vleGnrl.Strings);
        finally
          EndUpdate;
        end;//t..f

      end;//with

    finally
      vleGnrl.OnStringsChange := vOnChange;
    end;//t..f

  end;//with

end;//TframeSRSC_Res_Page_General.InitVLE
//==============================================================================
procedure TframeSRSC_Res_Page_General.FillVLE;
// Заполнить список параметров
var
  vOnChange : TNotifyEvent;
  i : integer;
begin

  with vleGnrl do  begin

    vOnChange := OnStringsChange;
    try
      OnStringsChange := nil;

      if Assigned(Res) then
        for i := 1 to vleGnrl.RowCount - 1 do
          case i of
            1: Vls[i] := Res.Name;
            2: Vls[i] := Res.DeclUnit;
            3: Vls[i] := Res.ResPrefix;
            4: Vls[i] := Res.DefLangName;
            5: Vls[i] := G_RES_IN_DLL[Res.InDll];
            6: Vls[i] := G_RES_CONST_UPPER[Res.ConstUpper];
          end//case; for

      else
        for i := 1 to vleGnrl.RowCount - 1 do
          case i of
            1..3:  Vls[i] := '';
            4:     Vls[i] := PLAIN_TEXT;
            5, 6:  Vls[i] := ItemProps[i - 1].PickList[0];
          end;//case; for; else if

    finally
      vleGnrl.OnStringsChange := vOnChange;
    end;//t..f

    FOldValues.Assign(Strings);
  end;//with


  if Assigned(Res) then  begin
    ValidResName;
    ValidResDeclUnit;
  end;//if

end;//TframeSRSC_Res_Page_General.FillVLE
//==============================================================================
procedure TframeSRSC_Res_Page_General.UpdateVLE;
// Изменение списка параметров
var
  i, vPostFlags : integer;

begin

  if not Assigned(Res) then
    Exit;


  with vleGnrl do  begin

    for i := 0 to Strings.Count - 1 do
      if AnsiCompareStr(Strings.ValueFromIndex[i], FOldValues.ValueFromIndex[i]) <> 0 then
        case i of
          0 : begin

            vPostFlags := 0;
            if StrIsFmt(Res.DeclUnit, Res.OwnerList.ProjectRes.ResSettings.DeclUnitFmt, True, [Res.Name]) then
              vPostFlags := vPostFlags or $01;

            if StrIsFmt(Res.ResPrefix, Res.OwnerList.ProjectRes.ResSettings.ResPrefixFmt, True, [Res.Name]) then
              vPostFlags := vPostFlags or $02;

            if vPostFlags > 0 then
              PostMessage(Self.Handle, WM_USER + 1, vPostFlags, 0);


            Res.OwnerList.ProjectRes.EraseOldUncommitedData(Res);

            Res.Name := Strings.ValueFromIndex[i];

            ValidResName;
          end;//0

          1 : begin
            Res.DeclUnit       := Strings.ValueFromIndex[i];

            ValidResDeclUnit;
          end;//1

          2 : Res.ResPrefix  := Strings.ValueFromIndex[i];
          3 : Res.DefLangName    := Strings.ValueFromIndex[i];
          4 : Res.InDll          := (AnsiCompareStr(Strings.ValueFromIndex[i], ItemProps[i].PickList[0]) <> 0);
          5 : Res.ConstUpper     := (AnsiCompareStr(Strings.ValueFromIndex[i], ItemProps[i].PickList[0]) <> 0);
        end;//case

    FOldValues.Assign(Strings);
  end;//with

  ResChange;
end;//TframeSRSC_Res_Page_General.UpdateVLE
//==============================================================================
procedure TframeSRSC_Res_Page_General.PPChange(var Msg: TMessage);
// Изменения имени модуля и префикса ресурса при изменении имени набора
var
  vSelStart, vSelLength : integer;
begin

  with TVLEHack(vleGnrl).InplaceEditor do  begin

    vSelStart  := SelStart;
    vSelLength := SelLength;
    try

      vleGnrl.Strings.BeginUpdate;
      try

        if Msg.WParam and $01 = $01 then
          Vls[2] := Format(Res.OwnerList.ProjectRes.ResSettings.DeclUnitFmt, [Res.Name]);

        if Msg.WParam and $02 = $02 then
          Vls[3] := Format(Res.OwnerList.ProjectRes.ResSettings.ResPrefixFmt, [Res.Name]);

      finally
        vleGnrl.Strings.EndUpdate;
      end;//t..f

    finally
      SelStart  := vSelStart;
      SelLength := vSelLength;
    end;//t..f

  end;//with

end;//TframeSRSC_Res_Page_General.PPChange
//==============================================================================
//==============================================================================
// public
//==============================================================================
class function TframeSRSC_Res_Page_General.PageName: string;
begin
  Result := 'General';
end;//TframeSRSC_Res_Page_General.PageName
//==============================================================================
class function TframeSRSC_Res_Page_General.PageCaption: string;
begin
  Result := 'Общие';
end;//TframeSRSC_Res_Page_General.PageCaption
//==============================================================================
procedure TframeSRSC_Res_Page_General.AfterResChange(const AOldRes,
  ANewRes: TSRSC_Res);
begin
  InitUnCpPickLists;

  FillVLE;
end;//TframeSRSC_Res_Page_General.AfterResChange
//==============================================================================
procedure TframeSRSC_Res_Page_General.ResUpdated;
begin
  FillVLE;
end;//TframeSRSC_Res_Page_General.ResUpdated
//==============================================================================
procedure TframeSRSC_Res_Page_General.PageDeactivate(var Allow: boolean);
begin
  Allow := CanCommit;

  if not Allow then
    MessageBox(Handle, 'Для перехода к другим вкладкам, данные должны быть корректны!', 'Внимание', MB_ICONWARNING);
end;//TframeSRSC_Res_Page_General.PageDeactivate
//==============================================================================
procedure TframeSRSC_Res_Page_General.PageActivate;
begin
  vleGnrl.SetFocus;
end;//TframeSRSC_Res_Page_General.PageActivate
//==============================================================================
function TframeSRSC_Res_Page_General.CanCommit: boolean;
begin
  Result := FResNameCrt and FResDeclUnitCrt;
end;//TframeSRSC_Res_Page_General.CanCommit
//==============================================================================
procedure TframeSRSC_Res_Page_General.Commit;
begin
  InitUnCpPickLists;

  inherited;
end;//TframeSRSC_Res_Page_General.Commit
//==============================================================================
procedure TframeSRSC_Res_Page_General.Rollback;
begin
  inherited;

end;//TframeSRSC_Res_Page_General.Rollback
//==============================================================================
constructor TframeSRSC_Res_Page_General.Create(AOwner: TComponent);
begin
  FOldValues := TStringList.Create;


  inherited;


  InitVLE;

end;//TframeSRSC_Res_Page_General.Create
//==============================================================================
destructor TframeSRSC_Res_Page_General.Destroy;
begin
  FreeAndNil(FOldValues);

  inherited;
end;//TframeSRSC_Res_Page_General.Destroy
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeSRSC_Res_Page_General.vleGnrlStringsChange(Sender: TObject);
begin
  UpdateVLE;
end;//TframeSRSC_Res_Page_General.vleGnrlStringsChange
//==============================================================================
procedure TframeSRSC_Res_Page_General.vleGnrlKeyPress(Sender: TObject; var Key: Char);
var
  S : string;
  vHalt : boolean;
begin

  if not(vleGnrl.Row in [1..vleGnrl.RowCount - 1]) or CharInSet(Key, EDIT_KEYS) then
    Exit;


  with vleGnrl do
    S := Vls[Row];


  case vleGnrl.Row of

    // 0 : Заголовок

    // Имя
    1, 2:
      vHalt := (Pos(Key, FS_EXCP_SYMBOLS) <> 0)
            or (S =  '') and not CharInSet(Key, LETTERS)
            or (S <> '') and not CharInSet(Key, LETTERS + DIGITS);

    else
      vHalt := False;
  end;//case


  if vHalt then  begin
    Key := #0;
    MessageBeep(MB_OK);
  end;//if

end;//TframeSRSC_Res_Page_General.vleGnrlKeyPress
//==============================================================================


INITIALIZATION//////////////////////////////////////////////////////////////////
  TframeSRSC_Res_Page_General.PagesClasses_Reg;
FINALIZATION////////////////////////////////////////////////////////////////////
  TframeSRSC_Res_Page_General.PagesClasses_UnReg;
END.
