UNIT CrossIntfs_u;//////////////////////////////////////////////////////////////
// Модуль независимых интерфейсов
//==============================================================================
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  Controls;
//==============================================================================
CONST
  CROSS_INTF_TOOLS_CONTROL     = '{84B1263E-A449-412E-B0D2-2E6A3701C8A4}';
  CROSS_INTF_PAGE              = '{728B2256-9CEE-45CD-B449-6012F773F10A}';
  CROSS_INTF_INTERACTIVE_PAGE  = '{B40DD6C1-9B52-4BE4-AB05-0E414C8D2341}';
//==============================================================================
TYPE
  // Интерфейс управления настройками
  IToolsControl = interface(IUnknown)
    [CROSS_INTF_TOOLS_CONTROL]

    function Get_ToolsContext : IUnknown;
    procedure Set_ToolsContext(const AToolsContext : IUnknown);
    property ToolsContext : IUnknown  read Get_ToolsContext  write Set_ToolsContext;

    procedure ReadTools;
    procedure WriteTools;

  end;//IToolsControl
//==============================================================================
  // Интерфейс страницы
  IPage = interface(IUnknown)
    [CROSS_INTF_PAGE]

    function PageName : string;
    function PageCaption : string;
    procedure PlaceTo(const AParant : TWinControl);

  end;//IPage
//==============================================================================
  // Интерфейс интерактивной страницы
  IInteractivePage = interface(IPage)
    [CROSS_INTF_INTERACTIVE_PAGE]

    procedure PageDeactivate;
    procedure PageActivate;
  end;//IInteractivePage
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================

END.
