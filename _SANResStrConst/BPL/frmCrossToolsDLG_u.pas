UNIT frmCrossToolsDLG_u;////////////////////////////////////////////////////////
// Независимая многостраничная форма настроек
INTERFACE
//==============================================================================
USES
  CrossIntfs_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls, Buttons, Math;
//==============================================================================
TYPE
  TfrmCrossToolsDLG = class(TForm)
    pnlBottom: TPanel;
    pcMain: TPageControl;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;

    procedure FormDestroy(Sender: TObject);
    procedure pcMainChange(Sender: TObject);

  private
    FOldActivePage : TTabSheet;

  protected
    function AddPage(const APage : IPage) : TTabSheet;
    procedure InteractivePageAction(const APage : TTabSheet; const AIsActivate : boolean);

  public

  end;//TfrmCrossToolsDLG
//==============================================================================
function CrossToolsDLG(const ACaption : string; const AOwner : TComponent;
  const AWidth, AHeigth : integer;
  const AToolsContext : IUnknown; const APages : array of IUnknown) : boolean;
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
function CrossToolsDLG(const ACaption : string; const AOwner : TComponent;
  const AWidth, AHeigth : integer;
  const AToolsContext : IUnknown; const APages : array of IUnknown) : boolean;
var
  i : integer;
begin

  with TfrmCrossToolsDLG.Create(AOwner) do  try
    Caption  := ACaption;
    Width    := AWidth;
    Height   := AHeigth;

    // Создать все
    for i := Low(APages) to High(APages) do  begin
      (APages[i] as IToolsControl).ToolsContext := AToolsContext;
      AddPage(APages[i] as IPage);
    end;//for

    // Прочитать все
    // Т.к. могут быть связи, создание и чтение в разных циклах
    for i := Low(APages) to High(APages) do
      (APages[i] as IToolsControl).ReadTools;


    FOldActivePage := pcMain.ActivePage;


    Result := (ShowModal = mrOk);


    // Записать все
    if Result then
      for i := Low(APages) to High(APages) do
        (APages[i] as IToolsControl).WriteTools;

  finally
    Free;
  end;//t..f

end;//CrossToolsDLG
//==============================================================================

//==============================================================================
// TfrmCrossToolsDLG
//==============================================================================
// protected
//==============================================================================
function TfrmCrossToolsDLG.AddPage(const APage: IPage): TTabSheet;
begin
  Result := TTabSheet.Create(Self);

  with Result do  begin
    Name         := 'ts' + APage.PageName;
    Caption      := APage.PageCaption;
    PageControl  := pcMain;
  end;//with

  APage.PlaceTo(Result);
end;//TfrmCrossToolsDLG.AddPage
//==============================================================================
procedure TfrmCrossToolsDLG.InteractivePageAction(const APage: TTabSheet;
  const AIsActivate: boolean);
var
  vIntf : IUnknown;
  vIPage : IInteractivePage;
begin

  if (APage = nil) or (APage.ControlCount = 0) then
    Exit;

  vIntf := FOldActivePage.Controls[0];

  if vIntf.QueryInterface( StringToGUID(CROSS_INTF_INTERACTIVE_PAGE), vIPage) = S_OK then
    if AIsActivate then
      vIPage.PageActivate
    else
      vIPage.PageDeactivate;

end;//TfrmCrossToolsDLG.InteractivePageAction
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TfrmCrossToolsDLG.FormDestroy(Sender: TObject);
// Все размещенные контролы, не имеющие владельца, уничтожаются
var
  i, j : integer;
begin

  for i := 0 to pcMain.PageCount - 1 do
    with pcMain.Pages[i] do
      for j := ControlCount - 1 downto 0 do
        if Controls[j].Owner = nil then
          Controls[j].Free;

end;//TfrmCrossToolsDLG.FormDestroy
//==============================================================================
procedure TfrmCrossToolsDLG.pcMainChange(Sender: TObject);
begin

  try

    InteractivePageAction(FOldActivePage, False);

    InteractivePageAction((Sender as TPageControl).ActivePage, True);

  finally
    FOldActivePage := (Sender as TPageControl).ActivePage;
  end;//t..f; with

end;//TfrmCrossToolsDLG.pcMainChange
//==============================================================================

END.
