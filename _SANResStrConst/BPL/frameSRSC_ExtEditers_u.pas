UNIT frameSRSC_ExtEditers_u;////////////////////////////////////////////////////
// Настрока внешних редакторов
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CrossIntfs_u, SRSC_Types_u,

  frmSRSC_ExtEditerDLG_u,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, StdCtrls, ImgList, ActnList;
//==============================================================================
TYPE
{
type
TNoHScrollListview = Class( TListview )
private
Procedure WMNCCalcSize( Var msg: TMessage ); message WM_NCCALCSIZE;
end;



procedure TNoHScrollListview.WMNCCalcSize(var msg: TMessage);
var
style: Integer;
begin
style := getWindowLong( handle, GWL_STYLE );
If (style and WS_HSCROLL) <> 0 Then
SetWindowLong( handle, GWL_STYLE, style and not WS_HSCROLL );
inherited;
end;
}
  TListView = class(ComCtrls.TListView)
  private
    procedure WMNCCalcSize( Var msg: TMessage ); message WM_NCCALCSIZE;
  end;//TListView
//==============================================================================
  TframeSRSC_ExtEditers = class(TFrame, IToolsControl, IPage)
    actlstTools: TActionList;
    actEEAdd: TAction;
    actEEEdit: TAction;
    actEEDelete: TAction;
    actEEUp: TAction;
    actEEDown: TAction;
    ilTools: TImageList;
    lblExtEditersHint: TLabel;
    tlbExtEditers: TToolBar;
    btnEEAdd: TToolButton;
    btnEEEdit: TToolButton;
    btnEEDelete: TToolButton;
    btnEEUp: TToolButton;
    btnEEDown: TToolButton;
    lvExtEditers: TListView;
    ilExtEditers: TImageList;

    procedure lvExtEditersDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lvExtEditersDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure lvExtEditersKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure actEEAddExecute(Sender: TObject);
    procedure actEEAddUpdate(Sender: TObject);
    procedure actEEEditExecute(Sender: TObject);
    procedure actEEEditUpdate(Sender: TObject);
    procedure actEEDeleteExecute(Sender: TObject);
    procedure actEEDeleteUpdate(Sender: TObject);
    procedure actEEUpExecute(Sender: TObject);
    procedure actEEUpUpdate(Sender: TObject);
    procedure actEEDownExecute(Sender: TObject);
    procedure actEEDownUpdate(Sender: TObject);

  private
    FToolsContext : ISRSC_ExtEditersHost;

    // Флаг, актуальный на момент редактирования или добавления внешнего редактора
    // True - именно добавление
    FEE_Add : boolean;

  protected
    // IToolsControl
    function Get_ToolsContext : IUnknown;
    procedure Set_ToolsContext(const AToolsContext : IUnknown);
    procedure ReadTools;
    procedure WriteTools;

    // IPage
    function PageName : string;
    function PageCaption : string;
    procedure PlaceTo(const AParant : TWinControl);

    function EE_CheckDuplecate(const AExePath, AParams : string; const AExcludeLI : TListItem = nil) : boolean;
    procedure EE_Test(const AExePath, AParams : string; var IsValid : boolean);
    function EE_Dialog(const ACaption : string; var AExePath, AParams : string) : boolean;
    function EE_Add(const AExePath, AParams : string; const AImageIndex : TImageIndex = -1) : TListItem;

  public
    constructor Create(AOwner : TComponent); override;

  end;//TframeSRSC_ExtEditers
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
uses
  ShellAPI, SHFolder, CommCtrl;
//==============================================================================
{$R *.dfm}
//==============================================================================


//==============================================================================
// TListView
//==============================================================================
// private
//==============================================================================
procedure TListView.WMNCCalcSize(var msg: TMessage);
var
  vStyle: Integer;
begin
  vStyle := getWindowLong( Handle, GWL_STYLE );

  if (vStyle and WS_HSCROLL) <> 0 then
    SetWindowLong( Handle, GWL_STYLE, vStyle and not WS_HSCROLL );

  inherited;
end;//TListView.WMNCCalcSize
//==============================================================================

//==============================================================================
// TframeSRSC_ExtEditers
//==============================================================================
// protected
//==============================================================================
// IToolsControlPage
//==============================================================================
function TframeSRSC_ExtEditers.Get_ToolsContext: IUnknown;
begin
  Result := FToolsContext;
end;//TframeSRSC_ExtEditers.Get_ToolsContext
//==============================================================================
procedure TframeSRSC_ExtEditers.Set_ToolsContext(const AToolsContext : IUnknown);
begin
  FToolsContext := (AToolsContext as ISRSC_ExtEditersHost)
end;//TframeSRSC_ExtEditers.Set_ToolsContext
//==============================================================================
procedure TframeSRSC_ExtEditers.ReadTools;
var
  i : integer;
begin

  ilExtEditers.Assign(FToolsContext.ExtEditersImages);

  with lvExtEditers, Items do  begin

    BeginUpdate;
    try
      Clear;

      for I := 0 to FToolsContext.ExtEditersCount - 1 do
        with FToolsContext.ExtEditers[i] do
          EE_Add(FileName, Params, ImageIndex);

    finally
      EndUpdate;
    end;//t..f

    if Count > 0 then
      Selected := Item[0];
  end;//with

end;//TframeSRSC_ExtEditers.ReadTools
//==============================================================================
procedure TframeSRSC_ExtEditers.WriteTools;
var
  i : integer;
begin
  FToolsContext.ExtEditers_Clear;

  FToolsContext.ExtEditersImages.Assign(ilExtEditers);

  for i := 0 to lvExtEditers.Items.Count - 1 do
    with lvExtEditers.Items[i] do
      FToolsContext.ExtEditers_Add(Caption, SubItems[0], ImageIndex);

  FToolsContext.ExtEditers_Save;
end;//TframeSRSC_ExtEditers.WriteTools
//==============================================================================
// IPage
//==============================================================================
function TframeSRSC_ExtEditers.PageName: string;
begin
  Result := 'ExtEditers'
end;//TframeSRSC_ExtEditers.PageName
//==============================================================================
function TframeSRSC_ExtEditers.PageCaption: string;
begin
  Result := 'Настройки внешних редакторов'
end;//TframeSRSC_ExtEditers.PageCaption
//==============================================================================
procedure TframeSRSC_ExtEditers.PlaceTo(const AParant: TWinControl);
begin
  Parent            := AParant;
  ParentBackground  := False;
  ParentColor       := False;
end;//TframeSRSC_ExtEditers.PlaceTo
//==============================================================================
function TframeSRSC_ExtEditers.EE_CheckDuplecate(const AExePath, AParams : string;
  const AExcludeLI : TListItem) : boolean;
// Поиск дублирующей строки
var
  i : integer;
begin

  with lvExtEditers.Items do
    for i := 0 to Count - 1 do
      if  (Item[i] <> AExcludeLI)
      and (Item[i].Caption      = AExePath)
      and (Item[i].SubItems[0]  = AParams)
      then
        Exit(True);

  Result := False;
end;//TframeSRSC_ExtEditers.EE_CheckDuplecate
//==============================================================================
procedure TframeSRSC_ExtEditers.EE_Test(const AExePath, AParams: string; var IsValid : boolean);
// Событие проверки параметров AExePath и AParams на повтор
begin

  if FEE_Add then
    IsValid := not EE_CheckDuplecate(AExePath, AParams)

  else
    IsValid := not EE_CheckDuplecate(AExePath, AParams, lvExtEditers.Selected);

end;//TframeSRSC_ExtEditers.EE_Test
//==============================================================================
function TframeSRSC_ExtEditers.EE_Dialog(const ACaption : string;
  var AExePath, AParams : string): boolean;
// Диалог с параметрами внешнего редактора
begin
  Result := ExtEditerDLG(ACaption, Self, AExePath, AParams, EE_Test);
end;//TframeSRSC_ExtEditers.EE_Dialog
//==============================================================================
function TframeSRSC_ExtEditers.EE_Add(const AExePath, AParams: string; const AImageIndex : TImageIndex): TListItem;
// Добавить строку внешнего редактора
begin
  Result := lvExtEditers.Items.Add;

  Result.Caption := AExePath;
  Result.SubItems.Add(AParams);

  if AImageIndex < 0 then
    Result.ImageIndex := ImageList_AddIcon(ilExtEditers.Handle, GetAssociatedIcon(AExePath, False))
  else
    Result.ImageIndex := AImageIndex;
end;//TframeSRSC_ExtEditers.EE_Add
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeSRSC_ExtEditers.Create(AOwner: TComponent);
begin
  inherited;

  Name   := 'f' + PageName;
  Align  := alClient;

  with lblExtEditersHint do
    Hint := Caption;

end;//TframeSRSC_ExtEditers.Create
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeSRSC_ExtEditers.lvExtEditersKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  case Key of
    VK_INSERT:
      actEEAdd.Execute;

    VK_F2:
      actEEEdit.Execute;

    VK_DELETE:
      actEEDelete.Execute;

    VK_UP:
      if Shift = [ssCtrl] then  begin
        actEEUp.Execute;
        Key := 0;
      end;//if

    VK_DOWN:
      if Shift = [ssCtrl] then  begin
        actEEDown.Execute;
        Key := 0;
      end;//if

  end;//case

end;//TframeSRSC_ExtEditers.lvExtEditersKeyDown
//==============================================================================
procedure TframeSRSC_ExtEditers.lvExtEditersDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  vLI : TListItem;
begin


   if Sender <> Source then
     Accept := False

   else
     with (Sender as TListView) do  begin
       vLI := GetItemAt(X, Y);

       Accept := (vLI <> Selected);
     end;//with; else if

end;//TframeSRSC_ExtEditers.lvExtEditersDragOver
//==============================================================================
procedure TframeSRSC_ExtEditers.lvExtEditersDragDrop(Sender, Source: TObject;
  X, Y: Integer);
var
  vIndex : integer;
  vLI : TListItem;
begin

  with (Sender as TListView) do  begin

    vLI := GetItemAt(X, Y);
    if Assigned(vLI) then
      vIndex := vLI.Index
    else
      vIndex := Items.Count;

    if Selected.Index = vIndex - 1 then
      Inc(vIndex);

    Items.BeginUpdate;
    try

      vLI := Items.Insert(vIndex);
      vLI.Assign(Selected);
      Selected.Delete;

    finally
      Items.EndUpdate;
    end;

    Selected := vLI;
    Selected.Focused := True;
  end;//with
end;//TframeSRSC_ExtEditers.lvExtEditersDragDrop
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEAddExecute(Sender: TObject);
var
  vExePath, vParams : string;
begin
  FEE_Add := True;

  vExePath  := '';
  vParams   := '';

  if not EE_Dialog((Sender as TAction).Caption, vExePath, vParams) then
    Exit;

  lvExtEditers.Selected := EE_Add(vExePath, vParams);
end;//TframeSRSC_ExtEditers.actEEAddExecute
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEAddUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := True;
end;//TframeSRSC_ExtEditers.actEEAddUpdate
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEEditExecute(Sender: TObject);
var
  vExePath, vParams : string;
begin
  FEE_Add := False;

  with lvExtEditers do  begin
    vExePath  := Selected.Caption;
    vParams   := Selected.SubItems[0];

    if not EE_Dialog((Sender as TAction).Caption, vExePath, vParams) then
      Exit;

    Selected.Caption      := vExePath;
    Selected.SubItems[0]  := vParams;
    Selected.ImageIndex   := ImageList_ReplaceIcon(
      ilExtEditers.Handle,
      Selected.ImageIndex,
      GetAssociatedIcon(Selected.Caption, False)
    );

  end;//with

end;//TframeSRSC_ExtEditers.actEEEditExecute
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEEditUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(lvExtEditers.Selected);
end;//TframeSRSC_ExtEditers.actEEEditUpdate
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEDeleteExecute(Sender: TObject);
begin

  if MessageBox(
      Handle,
      PChar(
        Format(
          'Вы действительно хотите удалить внешний редактор'#13#10'«%s»?',
          [lvExtEditers.Selected.Caption]
        )
      ),
      'Внимание',
      MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2
    ) = ID_NO
  then
    Exit;


  with lvExtEditers do  begin
    Selected.Delete;

    if Items.Count > 0 then
      Selected := Items[0];
  end;//with

end;//TframeSRSC_ExtEditers.actEEDeleteExecute
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEDeleteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(lvExtEditers.Selected);
end;//TframeSRSC_ExtEditers.actEEDeleteUpdate
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEUpExecute(Sender: TObject);
var
  vLI : TListItem;
begin

  with lvExtEditers do  begin

    Items.BeginUpdate;
    try

      vLI := Items.Insert(Selected.Index - 1);
      vLI.Assign(Selected);
      Selected.Delete;

    finally
      Items.EndUpdate;
    end;

    Selected := vLI;
    Selected.Focused := True;
  end;//with

end;//TframeSRSC_ExtEditers.actEEUpExecute
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEUpUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(lvExtEditers.Selected) and (lvExtEditers.Selected.Index > 0);
end;//TframeSRSC_ExtEditers.actEEUpUpdate
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEDownExecute(Sender: TObject);
var
  vLI : TListItem;
begin

  with lvExtEditers do  begin

    Items.BeginUpdate;
    try

      vLI := Items.Insert(Selected.Index + 2);
      vLI.Assign(Selected);
      Selected.Delete;

    finally
      Items.EndUpdate;
    end;

    Selected := vLI;
    Selected.Focused := True;
  end;//with

end;//TframeSRSC_ExtEditers.actEEDownExecute
//==============================================================================
procedure TframeSRSC_ExtEditers.actEEDownUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(lvExtEditers.Selected) and (lvExtEditers.Selected.Index < lvExtEditers.Items.Count - 1);
end;//TframeSRSC_ExtEditers.actEEDownUpdate
//==============================================================================



END.
