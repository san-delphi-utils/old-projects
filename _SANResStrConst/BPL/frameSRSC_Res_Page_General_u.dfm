inherited frameSRSC_Res_Page_General: TframeSRSC_Res_Page_General
  Width = 431
  ExplicitWidth = 431
  object shpName: TShape [0]
    Left = 323
    Top = 29
    Width = 9
    Height = 9
    ParentShowHint = False
    Shape = stCircle
    ShowHint = True
  end
  object shpDeclUnit: TShape [1]
    Left = 323
    Top = 47
    Width = 9
    Height = 9
    ParentShowHint = False
    Shape = stCircle
    ShowHint = True
  end
  object vleGnrl: TValueListEditor [2]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 314
    Height = 234
    Align = alLeft
    DoubleBuffered = False
    DropDownRows = 10
    ParentDoubleBuffered = False
    ScrollBars = ssNone
    Strings.Strings = (
      #1048#1084#1103' '#1085#1072#1073#1086#1088#1072' '#1088#1077#1089#1091#1088#1089#1086#1074'='
      #1052#1086#1076#1091#1083#1100' '#1076#1086#1089#1090#1091#1087#1072'='
      #1055#1088#1077#1092#1080#1082#1089' '#1080#1084#1077#1085' '#1088#1077#1089#1091#1088#1089#1086#1074'='
      #1057#1080#1085#1090#1072#1082#1089#1080#1089' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102'='
      #1056#1072#1079#1084#1077#1097#1077#1085#1080#1077'='
      #1056#1077#1075#1080#1089#1090#1088' '#1080#1084#1077#1085' '#1082#1086#1085#1089#1090#1072#1085#1090'=')
    TabOrder = 0
    TitleCaptions.Strings = (
      #1055#1072#1088#1072#1084#1077#1090#1088
      #1047#1085#1072#1095#1077#1085#1080#1077)
    OnKeyPress = vleGnrlKeyPress
    OnStringsChange = vleGnrlStringsChange
    ColWidths = (
      150
      158)
  end
end
