UNIT SRSC_Entry_u;//////////////////////////////////////////////////////////////
// Модуль точки входа
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  frmCrossToolsDLG_u,

  SRSC_Types_u, SRSC_Storage_File_u, frameSRSC_ProjectRes_u,

  frameSRSC_ResSettings_u, frameSRSC_ExtEditers_u;
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================

//==============================================================================
function SRSCManager_MainToolsDLG : boolean;
begin
  Result := CrossToolsDLG(
    'Общие настройки',
    nil,
    500,
    300,
    SRSCManager,
    [
      TframeSRSC_ResSettings.Create(nil),
      TframeSRSC_ExtEditers.Create(nil)
    ]
  );
end;//SRSCManager_MainToolsDLG
//==============================================================================
procedure InitBPLExpert(out ABPLIntf : IUnknown);
begin
  SRSCManager.ViewerClass       := TframeSRSC_ProjectRes;
  SRSCManager.MainToolsDLGFunc  := @SRSCManager_MainToolsDLG;

  ABPLIntf := SRSCManager;
end;//InitBPLExpert
//==============================================================================
procedure FinalBPLExpert;
begin
  SRSCManager.ViewerClass := nil;
  SRSCManager.ProjectResList.Clear;
end;//FinalBPLExpert
//==============================================================================

EXPORTS
  InitBPLExpert, 
  FinalBPLExpert
  ;

END.
