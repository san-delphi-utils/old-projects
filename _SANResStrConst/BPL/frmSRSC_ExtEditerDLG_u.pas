UNIT frmSRSC_ExtEditerDLG_u;////////////////////////////////////////////////////
// Диалог редактирования внешнего редактора
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  SRSC_Types_u,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;
//==============================================================================
TYPE
  TSRSC_EETestEvent = procedure(const AExePath, AParams : string; var IsValid : boolean) of object;
//------------------------------------------------------------------------------
  TfrmSRSC_ExtEditerDLG = class(TForm)
    lblExePath: TLabel;
    lblParams: TLabel;
    edtExePath: TEdit;
    edtParams: TEdit;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    lblExtEditersHint: TLabel;
    sbtnExePath: TSpeedButton;
    dlgOpenExe: TOpenDialog;
    pnlIcon: TPanel;
    imgIcon: TImage;
    lblWarn: TLabel;
    tmrWarn: TTimer;
    pnlWarn: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure edtExePathChange(Sender: TObject);
    procedure sbtnExePathClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure tmrWarnTimer(Sender: TObject);

  private
    FTestEvent : TSRSC_EETestEvent;

  protected
    procedure UpdateOK;

  public
    { Public declarations }

  end;//TfrmSRSC_ExtEditerDLG
//==============================================================================
function ExtEditerDLG(const ACaption : string; const AOwner : TComponent;
  var AExePath, AParams : string; const ATestEvent : TSRSC_EETestEvent = nil) : boolean;
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
function ExtEditerDLG(const ACaption : string; const AOwner : TComponent;
  var AExePath, AParams : string; const ATestEvent : TSRSC_EETestEvent) : boolean;
begin

  with TfrmSRSC_ExtEditerDLG.Create(AOwner) do  try
    Caption     := ACaption;
    FTestEvent  := ATestEvent;

    edtExePath.Text := AExePath;

    if AParams = '' then
      edtParams.Text := '$(File)'
    else
      edtParams.Text := AParams;


    Result := (ShowModal = mrOk);


    if Result then  begin
      AExePath  := edtExePath.Text;
      AParams   := edtParams.Text;
    end;//if

  finally
    Free;
  end;//t..f

end;//ExtEditerDLG
//==============================================================================

//==============================================================================
// TfrmSRSC_ExtEditerDLG.FormCreate
//==============================================================================
// private
//==============================================================================
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TfrmSRSC_ExtEditerDLG.UpdateOK;
begin
  btnOK.Enabled := FileExists(edtExePath.Text) and (Pos('$(FILE)', UpperCase(edtParams.Text)) > 0);
end;//TfrmSRSC_ExtEditerDLG.UpdateOK
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TfrmSRSC_ExtEditerDLG.FormCreate(Sender: TObject);
begin

  with lblExtEditersHint do
    Hint := Caption;

end;//TfrmSRSC_ExtEditerDLG.FormCreate
//==============================================================================
procedure TfrmSRSC_ExtEditerDLG.edtExePathChange(Sender: TObject);
begin
  UpdateOK;

  if Sender = edtExePath then
    if FileExists(edtExePath.Text) then
      imgIcon.Picture.Icon.Handle := GetAssociatedIcon( (Sender as TEdit).Text, False)
    else
      imgIcon.Picture.Assign(nil);

  (Sender as TEdit).Hint := (Sender as TEdit).Text;
end;//TfrmSRSC_ExtEditerDLG.edtExePathChange
//==============================================================================
procedure TfrmSRSC_ExtEditerDLG.sbtnExePathClick(Sender: TObject);
begin
  (Sender as TSpeedButton).Refresh;

  if dlgOpenExe.Execute(Handle) then
    edtExePath.Text := dlgOpenExe.FileName;

end;//TfrmSRSC_ExtEditerDLG.sbtnExePathClick
//==============================================================================
procedure TfrmSRSC_ExtEditerDLG.btnOKClick(Sender: TObject);
var
  vIsValid : boolean;
begin
  vIsValid := True;

  if Assigned(FTestEvent) then
    FTestEvent(edtExePath.Text, edtParams.Text, vIsValid);


  if vIsValid then
    ModalResult := mrOk

  else  begin
    lblWarn.Font.Color := clRed;
    pnlWarn.Show;
    tmrWarn.Enabled := True;
    MessageBeep(MB_OK);
  end;//else

end;//TfrmSRSC_ExtEditerDLG.btnOKClick
//==============================================================================
procedure TfrmSRSC_ExtEditerDLG.tmrWarnTimer(Sender: TObject);
const
  C_SPEED = 5;
var
  R, G, B : Byte;
begin
  R := GetRValue(lblWarn.Font.Color);
  G := GetGValue(lblWarn.Font.Color);
  B := GetBValue(lblWarn.Font.Color);

  Dec(R, C_SPEED);
  Inc(G, C_SPEED);
  Inc(B, C_SPEED);

  lblWarn.Font.Color := RGB(R, G, B);
  lblWarn.Repaint;

  if (R < G) then  begin
    tmrWarn.Enabled := False;
    pnlWarn.Hide;
  end;//if

end;//TfrmSRSC_ExtEditerDLG.tmrWarnTimer
//==============================================================================


END.
