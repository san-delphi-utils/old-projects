UNIT frameSRSC_ProjectRes_u;////////////////////////////////////////////////////
// Фрейм эксперта строковых констант в DLL ресурсов
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  frmCrossToolsDLG_u,
  SRSC_Types_u,
  frameSRSC_Res_u,
  frameSRSC_ResSettings_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ActnList, ToolWin, ExtCtrls, Buttons, ImgList, StdCtrls,
  StrUtils, Math;
//==============================================================================
TYPE
  TEditMode = (emBrowse, emEdit, emInsert);
//==============================================================================
type
  TframeSRSC_ProjectRes = class(TFrame, ISRSC_ProjectResFrame, ISRSC_ResListEditer)
    tlbMain: TToolBar;
    actlstMain: TActionList;
    tbcRess: TTabControl;
    pnlClient: TPanel;
    actResAdd: TAction;
    actResDelete: TAction;
    actResCommit: TAction;
    actResRollback: TAction;
    btnDllAdd: TToolButton;
    btnDllDelete: TToolButton;
    btnDllCommit: TToolButton;
    btnDllRollback: TToolButton;
    ilMain: TImageList;
    actTools: TAction;
    sepTools: TToolButton;
    btnTools: TToolButton;
    actRefresh: TAction;
    btnRefresh: TToolButton;
    sepRefresh: TToolButton;
    statMain: TStatusBar;
    btnFind: TToolButton;
    sepFind: TToolButton;
    actFind: TAction;
    pnlFind: TPanel;
    ilClose: TImageList;
    imgFind_Close: TImage;
    cmbxFind: TComboBox;
    sbtnFindNext: TSpeedButton;
    sbtnFindPrev: TSpeedButton;
    actFindNext: TAction;
    actFindPrev: TAction;
    chkFind_ThisResOnly: TCheckBox;
    chkFind_CaseSensitive: TCheckBox;
    chkFind_ConstNames: TCheckBox;
    chkFind_ConstText: TCheckBox;

    procedure tbcRessChange(Sender: TObject);
    procedure tbcRessChanging(Sender: TObject; var AllowChange: Boolean);

    procedure actResAddExecute(Sender: TObject);
    procedure actResAddUpdate(Sender: TObject);
    procedure actResDeleteExecute(Sender: TObject);
    procedure actResDeleteUpdate(Sender: TObject);
    procedure actResCommitExecute(Sender: TObject);
    procedure actResCommitUpdate(Sender: TObject);
    procedure actResRollbackExecute(Sender: TObject);
    procedure actResRollbackUpdate(Sender: TObject);
    procedure actRefreshExecute(Sender: TObject);
    procedure actRefreshUpdate(Sender: TObject);
    procedure actFindExecute(Sender: TObject);
    procedure actFindUpdate(Sender: TObject);
    procedure actFindNextExecute(Sender: TObject);
    procedure actFindPrevExecute(Sender: TObject);
    procedure cmbxFindKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cmbxFindKeyPress(Sender: TObject; var Key: Char);
    procedure cmbxFindChange(Sender: TObject);
    procedure imgFind_CloseClick(Sender: TObject);
    procedure imgFind_CloseMouseEnter(Sender: TObject);
    procedure imgFind_CloseMouseLeave(Sender: TObject);
    procedure chkFindSelect(Sender: TObject);
    procedure actToolsExecute(Sender: TObject);


  private
    FProjectRes: TSRSC_ProjectRes;
    FResFrame : TframeSRSC_Res;
    FEditMode : TEditMode;

    FFindCursor : TSRSC_FindCursor;

  private
    function Get_Modified: boolean;

  protected
    // ISRSC_ProjectResFrame
    function Get_ProjectRes : TSRSC_ProjectRes;
    procedure Set_ProjectRes(const AProjectRes : TSRSC_ProjectRes);
    function Get_CurRes: TSRSC_Res;
    procedure Set_CurRes(const Value: TSRSC_Res);
    function TrySelectConst(const APartOfNameToFind : string) : boolean;

    //ISRSC_ResListEditer
    procedure ResChange(const ARes : TSRSC_Res);

    function ParentCustomForm : TCustomForm;

    procedure ParentFormCloseQuery(Sender: TObject;  var CanClose: Boolean);
    procedure SetParent(AParent : TWinControl);  override;


    procedure UpdateCurRes;
    procedure UpdateEditMode;

    function FindInRes(const AText : string;
      const ANameOpt, ATextOpt, ABackOpt, AIgnoreCaseOpt, ANoChangeResOpt : boolean) : boolean;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    procedure LoadRessTabs;

    procedure EmergencyEndEdit;

  public
    property ProjectRes : TSRSC_ProjectRes  read Get_ProjectRes  write Set_ProjectRes;

    property CurRes : TSRSC_Res  read Get_CurRes  write Set_CurRes;

    property ResFrame : TframeSRSC_Res  read FResFrame;

    property Modified : boolean  read Get_Modified;

  end;//TframeSRSC_ProjectRes
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

type
  TCustomFormHack = class(TCustomForm);

//==============================================================================
// TframeSRSC_ProjectRes
//==============================================================================
// private
//==============================================================================
function TframeSRSC_ProjectRes.Get_Modified: boolean;
begin
  Result := (FEditMode <> emBrowse);
end;//TframeSRSC_ProjectRes.Get_Modified
//==============================================================================
//==============================================================================
// protected
//==============================================================================
// ISRSC_ProjectResFrame
//==============================================================================
function TframeSRSC_ProjectRes.Get_ProjectRes: TSRSC_ProjectRes;
begin
  Result := FProjectRes;
end;//TframeSRSC_ProjectRes.Get_ProjectRes
//==============================================================================
procedure TframeSRSC_ProjectRes.Set_ProjectRes(const AProjectRes: TSRSC_ProjectRes);
begin
  FProjectRes := AProjectRes;

  if Assigned(FProjectRes) then  begin

    if not Assigned(FResFrame) then  begin
      FResFrame := TframeSRSC_Res.Create(Self);
      FResFrame.Align := alClient;
    end;//if

    FResFrame.Parent  := tbcRess;

    LoadRessTabs;
  end;//if

end;//TframeSRSC_ProjectRes.Set_ProjectRes
//==============================================================================
function TframeSRSC_ProjectRes.Get_CurRes: TSRSC_Res;
begin
  if tbcRess.TabIndex = -1 then
    Result := nil
  else
    Result := ProjectRes.ResList[tbcRess.TabIndex];
end;//TframeSRSC_ProjectRes.Get_CurRes
//==============================================================================
procedure TframeSRSC_ProjectRes.Set_CurRes(const Value: TSRSC_Res);
begin
  tbcRess.TabIndex := ProjectRes.ResList.IndexOf(Value);

  UpdateCurRes;
end;//TframeSRSC_ProjectRes.Set_CurRes
//==============================================================================
function TframeSRSC_ProjectRes.TrySelectConst(const APartOfNameToFind: string): boolean;
var
  vFindCursor : TSRSC_FindCursor;
begin

  if (APartOfNameToFind = '')
  or (ProjectRes.ResList.Count = 0)
  then
    Exit(False);


  vFindCursor.Clear;
  vFindCursor.SRSCRes  := ProjectRes.ResList.First;
  vFindCursor.Text     := APartOfNameToFind;
  vFindCursor.SetOptions(True, False, False, True, False);

  Result := ProjectRes.FindInRess(vFindCursor);

  if Result then
  begin
    CurRes := vFindCursor.SRSCRes;
    ResFrame.SetFindCursor(vFindCursor);
  end;//if

end;//TframeSRSC_ProjectRes.TrySelectConst
//==============================================================================
//ISRSC_ResListEditer
//==============================================================================
procedure TframeSRSC_ProjectRes.ResChange(const ARes: TSRSC_Res);
begin
  UpdateEditMode;
end;//TframeSRSC_ProjectRes.ResChange
//==============================================================================
//==============================================================================
function TframeSRSC_ProjectRes.ParentCustomForm: TCustomForm;
var
  vWCt : TWinControl;
begin
  vWCt := Self;

  while Assigned(vWCt) and not (vWCt is TCustomForm) do
    vWCt := vWCt.Parent;

  if Assigned(vWCt) then
    Result := (vWCt as TCustomForm)
  else
    Result := nil;
end;//TframeSRSC_ProjectRes.ParentCustomForm
//==============================================================================
procedure TframeSRSC_ProjectRes.ParentFormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin

  if Modified then
    case MessageBox(Self.Handle, 'Есть несохраненные изменения!'#13#10'Сохранить?', 'Внимание', MB_ICONQUESTION or MB_YESNOCANCEL) of

      ID_YES: begin
        actResCommit.Execute;
        CanClose := True;
      end;//ID_YES

      ID_NO: begin

        try
          actResRollback.Execute;
        except
          on E : Exception do
            MessageBox(Self.Handle, PChar(E.Message), 'Ошибка' , MB_ICONSTOP);
        end;//t..f

        CanClose := True;
      end;//ID_NO

      else
        CanClose := False;

    end;//case

end;//TframeSRSC_ProjectRes.ParentFormCloseQuery
//==============================================================================
procedure TframeSRSC_ProjectRes.SetParent(AParent: TWinControl);
begin

  if Assigned(Parent) then
    TCustomFormHack(ParentCustomForm).OnCloseQuery := nil;


  inherited;


  if Assigned(Parent) then
    TCustomFormHack(ParentCustomForm).OnCloseQuery := ParentFormCloseQuery;

end;//TframeSRSC_ProjectRes.SetParent
//==============================================================================
procedure TframeSRSC_ProjectRes.UpdateCurRes;
var
  vCurRes : TSRSC_Res;
begin
  vCurRes := CurRes;

  FProjectRes.CurRes  := vCurRes;
  FResFrame.Res       := vCurRes;
end;//TframeSRSC_ProjectRes.UpdateCurRes
//==============================================================================
procedure TframeSRSC_ProjectRes.UpdateEditMode;
begin

  if FEditMode = emBrowse then
    FEditMode := emEdit;

  // Сохранение несохраненных данных
  ProjectRes.SaveUncommitedData;

end;//TframeSRSC_ProjectRes.UpdateEditMode
//==============================================================================
function TframeSRSC_ProjectRes.FindInRes(const AText : string;
  const ANameOpt, ATextOpt, ABackOpt, AIgnoreCaseOpt, ANoChangeResOpt : boolean): boolean;
// Поиск
var
  i : integer;
begin

  // Некорректный вызов
  if tbcRess.Tabs.Count = 0 then  begin
    Result := False;

    if actFind.Checked then
      actFind.Execute;

    Exit;
  end;//if


  with cmbxFind do  begin

    if AText = '' then  begin
      MessageBeep(MB_OK);
      Exit(False);
    end;//if


    i := Items.IndexOf(AText);

    if i > -1 then
      Items.Move(i, 0)
    else
      Items.Insert(0, AText);

    Text := AText;
  end;//with


  // Параметры поиска
  FFindCursor.Text     := AText;
  FFindCursor.SRSCRes  := CurRes;
  FFindCursor.SetOptions(ANameOpt, ATextOpt, ABackOpt, AIgnoreCaseOpt, ANoChangeResOpt);


  Result := ProjectRes.FindInRess(FFindCursor);

  if Result then
    begin
      CurRes := FFindCursor.SRSCRes;
      ResFrame.SetFindCursor(FFindCursor);
    end
  else
    MessageBox(Handle, 'Больше нет информации'#13#10'соответствующей критериям поиска.', 'Поиск завершен', MB_ICONINFORMATION);

end;//TframeSRSC_ProjectRes.FindInRes
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeSRSC_ProjectRes.Create(AOwner: TComponent);
begin
  inherited;

  actResAdd.Hint       := 'Добавить новый набор'#13#10'констант в ресурсе';
  actResDelete.Hint    := 'Удалить выбранный'#13#10'набор констант в ресурсе';
  actResCommit.Hint    := 'Принять изменения в выбранном'#13#10'наборе констант в ресурсе';
  actResRollback.Hint  := 'Отменить изменения в выбранном'#13#10'наборе констант в ресурсе';

  actRefresh.Hint      := 'Обновить';

  ilClose.GetBitmap(0, imgFind_Close.Picture.Bitmap);

end;//TframeSRSC_ProjectRes.Create
//==============================================================================
destructor TframeSRSC_ProjectRes.Destroy;
begin

  // Аварийная ситуация: проект закрыт, а данные не сохранены.
  // Текущие изменения отменяются, а при последующей загрузке будет использовано
  // веменное хранилище.
  if Modified then
    FProjectRes.RollbackRes(True);

  FProjectRes := nil;


  if Assigned(Parent) then
    TCustomFormHack(ParentCustomForm).OnCloseQuery := nil;


  inherited;
end;//TframeSRSC_ProjectRes.Destroy
//==============================================================================
procedure TframeSRSC_ProjectRes.LoadRessTabs;
var
  i : integer;
begin
  tbcRess.Tabs.Clear;

  for i := 0 to ProjectRes.ResList.Count - 1 do
    tbcRess.Tabs.Add( ProjectRes.ResList[i].Name );

  tbcRess.Visible := tbcRess.Tabs.Count > 0;

  if tbcRess.Tabs.Count > 0 then
    if Assigned(ProjectRes.CurRes) then
      begin

        if ProjectRes.CurRes.IsUnCommited then
          if ProjectRes.CurRes.UnCommitedAdd then
            FEditMode := emInsert
          else
            FEditMode := emEdit;

        CurRes := ProjectRes.CurRes;
      end
    else
      CurRes := ProjectRes.ResList.First;

  with statMain do  begin
    Panels[0].Text := ProjectRes.ProjectFileName;
    Hint := 'Проект:'#13#10 + Panels[0].Text;
  end;//with

end;//TframeSRSC_ProjectRes.LoadRessTabs
//==============================================================================
procedure TframeSRSC_ProjectRes.EmergencyEndEdit;
// Аварийное прекращение редактирования
begin
  FEditMode := emBrowse;
end;//TframeSRSC_ProjectRes.EmergencyEndEdit
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeSRSC_ProjectRes.tbcRessChange(Sender: TObject);
begin
  ResFrame.pcResPages.ActivePageIndex := 1;

  UpdateCurRes;
end;//TframeSRSC_ProjectRes.tbcRessChange
//==============================================================================
procedure TframeSRSC_ProjectRes.tbcRessChanging(Sender: TObject; var AllowChange: Boolean);
begin

  if (FEditMode <> emBrowse) then
    ParentFormCloseQuery(Sender, AllowChange);

end;//TframeSRSC_ProjectRes.tbcRessChanging
//==============================================================================
procedure TframeSRSC_ProjectRes.actResAddExecute(Sender: TObject);
var
  vName : string;
begin
  FEditMode := emInsert;

  vName := ProjectRes.ResList.GenUniqueName('NewRes');

  tbcRess.Tabs.Add(vName);
  CurRes := ProjectRes.AddRes(vName, True);

  tbcRess.Show;

  ResFrame.pcResPages.ActivePageIndex := 0;

  UpdateEditMode;
end;//TframeSRSC_ProjectRes.actResAddExecute
//==============================================================================
procedure TframeSRSC_ProjectRes.actResAddUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not Assigned(CurRes) or (FEditMode = emBrowse);
end;//TframeSRSC_ProjectRes.actResAddUpdate
//==============================================================================
procedure TframeSRSC_ProjectRes.actResDeleteExecute(Sender: TObject);
//var
//  vRes : TSRSC_Res;
begin

  if MessageBox(
      Handle,
      PChar(
        Format(
          'Удалить набор ресурсов «%s»?',
          [CurRes.Name]
        )
      ),
      'Внимание',
      MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2
    ) = ID_NO
  then
    Exit;

  ProjectRes.DeleteRes;


  with tbcRess do  begin
    Tabs.Delete(TabIndex);
    TabIndex := MAX(0, Tabs.Count - 1);

    Visible := Tabs.Count > 0;
  end;//with

  FEditMode := emBrowse;
end;//TframeSRSC_ProjectRes.actResDeleteExecute
//==============================================================================
procedure TframeSRSC_ProjectRes.actResDeleteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(CurRes);
end;//TframeSRSC_ProjectRes.actResDeleteUpdate
//==============================================================================
procedure TframeSRSC_ProjectRes.actResCommitExecute(Sender: TObject);
begin
  ProjectRes.CommitRes;

  with tbcRess do
    Tabs[TabIndex] := CurRes.Name;

  FEditMode := emBrowse;
end;//TframeSRSC_ProjectRes.actResCommitExecute
//==============================================================================
procedure TframeSRSC_ProjectRes.actResCommitUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(CurRes)
                             and (FEditMode <> emBrowse)
                             and (FResFrame.CanCommit);
end;//TframeSRSC_ProjectRes.actResCommitUpdate
//==============================================================================
procedure TframeSRSC_ProjectRes.actResRollbackExecute(Sender: TObject);
//var
//  vRes : TSRSC_Res;
begin
  ProjectRes.RollbackRes;

  if FEditMode = emInsert then
    with tbcRess do  begin
      Tabs.Delete(TabIndex);
      TabIndex := MAX(0, Tabs.Count - 1);
      Visible := Tabs.Count > 0;
    end;//with; if

  UpdateCurRes;

  FEditMode := emBrowse;
end;//TframeSRSC_ProjectRes.actResRollbackExecute
//==============================================================================
procedure TframeSRSC_ProjectRes.actResRollbackUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(CurRes) and (FEditMode <> emBrowse);
end;//TframeSRSC_ProjectRes.actResRollbackUpdate
//==============================================================================
procedure TframeSRSC_ProjectRes.actRefreshExecute(Sender: TObject);
var
  vResName : string;
begin

  if Assigned(CurRes) then
    vResName := CurRes.Name
  else
    vResName := '';

  ProjectRes.LoadResList;
  LoadRessTabs;

  CurRes := ProjectRes.ResList.Find(vResName);

  FFindCursor.Clear;
end;//TframeSRSC_ProjectRes.actRefreshExecute
//==============================================================================
procedure TframeSRSC_ProjectRes.actRefreshUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (FEditMode = emBrowse);
end;//TframeSRSC_ProjectRes.actRefreshUpdate
//==============================================================================
procedure TframeSRSC_ProjectRes.actFindExecute(Sender: TObject);
begin
  pnlFind.Visible := (Sender as TAction).Checked;

  if (Sender as TAction).Checked then  begin
    FFindCursor.Clear;

    cmbxFind.SetFocus;
  end;//if

end;//TframeSRSC_ProjectRes.actFindExecute
//==============================================================================
procedure TframeSRSC_ProjectRes.actFindUpdate(Sender: TObject);
begin

  if Modified then
    begin
      chkFind_ThisResOnly.Checked := True;
      chkFind_ThisResOnly.Enabled := False;
    end
  else
    chkFind_ThisResOnly.Enabled := True;

  (Sender as TAction).Enabled := (tbcRess.Tabs.Count > 0);
end;//TframeSRSC_ProjectRes.actFindUpdate
//==============================================================================
procedure TframeSRSC_ProjectRes.actFindNextExecute(Sender: TObject);
begin
  FindInRes(cmbxFind.Text, chkFind_ConstNames.Checked,
    chkFind_ConstText.Checked, False, not chkFind_CaseSensitive.Checked, chkFind_ThisResOnly.Checked);
end;//TframeSRSC_ProjectRes.actFindNextExecute
//==============================================================================
procedure TframeSRSC_ProjectRes.actFindPrevExecute(Sender: TObject);
begin
  FindInRes(cmbxFind.Text, chkFind_ConstNames.Checked,
    chkFind_ConstText.Checked,  True, not chkFind_CaseSensitive.Checked, chkFind_ThisResOnly.Checked);
end;//TframeSRSC_ProjectRes.actFindPrevExecute
//==============================================================================
procedure TframeSRSC_ProjectRes.cmbxFindKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key = VK_RETURN then  begin
    Key := 0;
    actFindNext.Execute;
  end;//if

end;//TframeSRSC_ProjectRes.cmbxFindKeyDown
//==============================================================================
procedure TframeSRSC_ProjectRes.cmbxFindKeyPress(Sender: TObject;  var Key: Char);
begin

  if ORD(Key) = VK_RETURN then
    Key := #0;

end;//TframeSRSC_ProjectRes.cmbxFindKeyPress
//==============================================================================
procedure TframeSRSC_ProjectRes.cmbxFindChange(Sender: TObject);
begin
  FFindCursor.Clear;
end;//TframeSRSC_ProjectRes.cmbxFindChange
//==============================================================================
procedure TframeSRSC_ProjectRes.imgFind_CloseClick(Sender: TObject);
begin
  actFind.Execute;
end;//TframeSRSC_ProjectRes.imgFind_CloseClick
//==============================================================================
procedure TframeSRSC_ProjectRes.imgFind_CloseMouseEnter(Sender: TObject);
begin
  with (Sender as TImage), Picture do  begin
    ilClose.GetBitmap(1, Bitmap);
    Refresh;
  end;//with
end;//TframeSRSC_ProjectRes.imgFind_CloseMouseEnter
//==============================================================================
procedure TframeSRSC_ProjectRes.imgFind_CloseMouseLeave(Sender: TObject);
begin
  with (Sender as TImage), Picture do  begin
    ilClose.GetBitmap(0, Bitmap);
    Refresh;
  end;//with
end;//TframeSRSC_ProjectRes.imgFind_CloseMouseLeave
//==============================================================================
procedure TframeSRSC_ProjectRes.chkFindSelect(Sender: TObject);
// Хотябы одна галочка должна стоять
begin

  if not (Sender as TCheckBox).Checked then
    if Sender = chkFind_ConstNames then
      chkFind_ConstText.Checked   := True
    else
      chkFind_ConstNames.Checked  := True

end;//TframeSRSC_ProjectRes.chkFindSelect
//==============================================================================
procedure TframeSRSC_ProjectRes.actToolsExecute(Sender: TObject);
//var
//  S : string;
//  i : integer;
begin

  CrossToolsDLG('Параметры ресурсов проекта', Self, 250, 270, FProjectRes, [TframeSRSC_ResSettings.Create(nil)]);

//  S := '';
//  for i := 0 to CurRes.Items.Count - 1 do
//    S := S + CurRes.Items[i].Name + #13#10;
//
//  ShowMessage(S);


end;//TframeSRSC_ProjectRes.actToolsExecute
//==============================================================================
END.
