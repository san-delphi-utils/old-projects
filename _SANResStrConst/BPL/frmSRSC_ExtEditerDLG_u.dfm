object frmSRSC_ExtEditerDLG: TfrmSRSC_ExtEditerDLG
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'frmSRSC_ExtEditerDLG'
  ClientHeight = 163
  ClientWidth = 400
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  DesignSize = (
    400
    163)
  PixelsPerInch = 96
  TextHeight = 13
  object lblExePath: TLabel
    Left = 16
    Top = 16
    Width = 201
    Height = 13
    Caption = #1055#1091#1090#1100' '#1082' '#1080#1089#1087#1086#1083#1085#1103#1077#1084#1086#1084#1091' '#1092#1072#1081#1083#1091' '#1088#1077#1076#1072#1082#1090#1086#1088#1072
  end
  object lblParams: TLabel
    Left = 16
    Top = 72
    Width = 153
    Height = 13
    Caption = #1055#1088#1072#1088#1072#1084#1077#1088#1099' '#1082#1086#1084#1072#1085#1076#1085#1086#1081' '#1089#1090#1088#1086#1082#1080
  end
  object lblExtEditersHint: TLabel
    AlignWithMargins = True
    Left = 16
    Top = 114
    Width = 222
    Height = 10
    Caption = '$(FILE) '#1074' '#1087#1072#1088#1072#1084#1077#1090#1088#1072#1093' '#1079#1072#1084#1077#1085#1103#1077#1090#1089#1103' '#1085#1072' '#1080#1084#1103' '#1074#1088#1077#1084#1077#1085#1085#1086#1075#1086' '#1092#1072#1081#1083#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
  end
  object sbtnExePath: TSpeedButton
    Left = 361
    Top = 31
    Width = 23
    Height = 22
    Anchors = [akTop, akRight]
    Glyph.Data = {
      6A000000424D6A000000000000003E000000280000000A0000000B0000000100
      0100000000002C0000000000000000000000020000000000000000000000FFFF
      FF00FFC00000FFC00000FFC00000FFC00000FFC000003300000033000000FFC0
      0000FFC00000FFC00000FFC00000}
    OnClick = sbtnExePathClick
  end
  object edtExePath: TEdit
    Left = 44
    Top = 32
    Width = 318
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Text = 'edtExePath'
    OnChange = edtExePathChange
    OnDblClick = sbtnExePathClick
  end
  object edtParams: TEdit
    Left = 16
    Top = 88
    Width = 368
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Text = 'edtParams'
    OnChange = edtExePathChange
  end
  object btnOK: TBitBtn
    Left = 228
    Top = 130
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1054#1050
    Default = True
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 2
    OnClick = btnOKClick
  end
  object btnCancel: TBitBtn
    Left = 309
    Top = 130
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    DoubleBuffered = True
    ModalResult = 2
    ParentDoubleBuffered = False
    TabOrder = 3
  end
  object pnlIcon: TPanel
    Left = 16
    Top = 30
    Width = 24
    Height = 24
    BevelOuter = bvLowered
    Caption = 'pnlIcon'
    ShowCaption = False
    TabOrder = 4
    object imgIcon: TImage
      Tag = 4
      Left = 4
      Top = 4
      Width = 16
      Height = 16
    end
  end
  object pnlWarn: TPanel
    Left = 16
    Top = 127
    Width = 202
    Height = 33
    BevelOuter = bvLowered
    Caption = 'pnlWarn'
    ShowCaption = False
    TabOrder = 5
    Visible = False
    object lblWarn: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 3
      Width = 194
      Height = 26
      Margins.Top = 2
      Align = alClient
      Caption = #1059#1078#1077' '#1077#1089#1090#1100' '#1074#1085#1077#1096#1085#1080#1081' '#1088#1077#1076#1072#1082#1090#1086#1088' '#1089' '#1090#1072#1082#1080#1084#1080' '#1087#1072#1088#1072#1084#1077#1090#1088#1072#1084#1080'!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitTop = 15
      ExplicitWidth = 181
    end
  end
  object dlgOpenExe: TOpenDialog
    DefaultExt = '.exe'
    Filter = #1048#1089#1087#1086#1083#1085#1103#1077#1084#1099#1081' '#1092#1072#1081#1083' (*.exe)|*.exe|'#1051#1102#1073#1086#1081' '#1092#1072#1081#1083' (*.*)|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 16
  end
  object tmrWarn: TTimer
    Enabled = False
    Interval = 200
    OnTimer = tmrWarnTimer
    Left = 48
    Top = 120
  end
end
