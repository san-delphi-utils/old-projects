UNIT frameCustomSRSC_Res_Page_u;////////////////////////////////////////////////
// Абстрактная страница редактирования набора ресурсов
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  SRSC_Types_u,

  Windows, Messages, SysUtils, Variants, Classes, Contnrs, Graphics, Controls, Forms,
  Dialogs, ImgList, ActnList;
//==============================================================================
TYPE
  TframeSRSCResPageClass = class of TframeCustomSRSC_Res_Page;
//------------------------------------------------------------------------------
  TframeCustomSRSC_Res_Page = class(TFrame)
    actlstPage: TActionList;
    ilPage: TImageList;
  private
    class var FPagesClasses : TClassList;

  private
    function Get_Res: TSRSC_Res;

  protected
    class procedure PagesClasses_Reg;
    class procedure PagesClasses_UnReg;
    class procedure RaisePageExcp(const AMessage : string);
    class procedure RaisePageExcpFmt(const AMessage : string; const Args : array of const);

    procedure ResChange;

  public
    class function PagesClassesCount : integer;
    class function PagesClasses(const Index : integer) : TframeSRSCResPageClass;

    class function PageName : string; virtual;
    class function PageCaption : string; virtual;

    procedure BeforeResChange(const AOldRes, ANewRes: TSRSC_Res); virtual;
    procedure AfterResChange(const AOldRes, ANewRes: TSRSC_Res); virtual;
    procedure ResUpdated; virtual;

    procedure PageDeactivate(var Allow : boolean); virtual;
    procedure PageActivate; virtual;

    function CanCommit : boolean; virtual;
    procedure Commit; virtual;
    procedure Rollback; virtual;

    procedure SetFindCursor(const AFindCursor : TSRSC_FindCursor); virtual;


  public
    property Res : TSRSC_Res  read Get_Res;

  end;//TframeCustomSRSC_Res_Page
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
// TframeCustomSRSC_Res_Page
//==============================================================================
// private
//==============================================================================
function TframeCustomSRSC_Res_Page.Get_Res: TSRSC_Res;
begin
  Result := (Owner as ISRSC_ResEditer).Res;
end;//TframeCustomSRSC_Res_Page.Get_Res
//==============================================================================
//==============================================================================
// protected
//==============================================================================
class procedure TframeCustomSRSC_Res_Page.PagesClasses_Reg;
begin
  if FPagesClasses.IndexOf(Self) = -1 then
    FPagesClasses.Add(Self);
end;//TframeCustomSRSC_Res_Page.PagesClasses_Reg
//==============================================================================
class procedure TframeCustomSRSC_Res_Page.PagesClasses_UnReg;
begin
  FPagesClasses.Remove(Self);
end;//TframeCustomSRSC_Res_Page.PagesClasses_UnReg
//==============================================================================
class procedure TframeCustomSRSC_Res_Page.RaisePageExcp(const AMessage: string);
begin
  raise EGUIResPageError.Create( ClassName + #13#10 + AMessage );
end;//TframeCustomSRSC_Res_Page.RaisePageExcp
//==============================================================================
class procedure TframeCustomSRSC_Res_Page.RaisePageExcpFmt(const AMessage: string;
  const Args: array of const);
begin
  RaisePageExcp( Format(AMessage, Args) );
end;//TframeCustomSRSC_Res_Page.RaisePageExcpFmt
//==============================================================================
procedure TframeCustomSRSC_Res_Page.ResChange;
begin
  (Owner as ISRSC_ResEditer).ResUpdated;
end;//TframeCustomSRSC_Res_Page.ResChange
//==============================================================================
//==============================================================================
// public
//==============================================================================
class function TframeCustomSRSC_Res_Page.PagesClassesCount: integer;
begin
  Result := FPagesClasses.Count;
end;//TframeCustomSRSC_Res_Page.PagesClassesCount
//==============================================================================
class function TframeCustomSRSC_Res_Page.PagesClasses(const Index: integer): TframeSRSCResPageClass;
begin
  Result := TframeSRSCResPageClass(FPagesClasses[Index]);
end;//TframeCustomSRSC_Res_Page.PagesClasses
//==============================================================================
class function TframeCustomSRSC_Res_Page.PageName: string;
begin
  raise EAbstractError.Create(ClassName + '.PageName not defined!');
end;//TframeCustomSRSC_Res_Page.PageName
//==============================================================================
class function TframeCustomSRSC_Res_Page.PageCaption: string;
begin
  raise EAbstractError.Create(ClassName + '.PageCaption not defined!');
end;//TframeCustomSRSC_Res_Page.PageCaption
//==============================================================================
procedure TframeCustomSRSC_Res_Page.BeforeResChange(const AOldRes, ANewRes: TSRSC_Res);
begin
end;//TframeCustomSRSC_Res_Page.BeforeResChange
//==============================================================================
procedure TframeCustomSRSC_Res_Page.AfterResChange(const AOldRes, ANewRes: TSRSC_Res);
begin
end;//TframeCustomSRSC_Res_Page.AfterResChange
//==============================================================================
procedure TframeCustomSRSC_Res_Page.ResUpdated;
begin
end;//TframeCustomSRSC_Res_Page.ResUpdated
//==============================================================================
procedure TframeCustomSRSC_Res_Page.PageDeactivate(var Allow: boolean);
begin
end;//TframeCustomSRSC_Res_Page.PageDeactivate
//==============================================================================
procedure TframeCustomSRSC_Res_Page.PageActivate;
begin
end;//TframeCustomSRSC_Res_Page.PageActivate
//==============================================================================
function TframeCustomSRSC_Res_Page.CanCommit: boolean;
begin
  Result := True;
end;//TframeCustomSRSC_Res_Page.CanCommit
//==============================================================================
procedure TframeCustomSRSC_Res_Page.Commit;
begin
end;//TframeCustomSRSC_Res_Page.Commit
//==============================================================================
procedure TframeCustomSRSC_Res_Page.Rollback;
begin
end;//TframeCustomSRSC_Res_Page.Rollback
//==============================================================================
procedure TframeCustomSRSC_Res_Page.SetFindCursor(const AFindCursor: TSRSC_FindCursor);
begin
end;//TframeCustomSRSC_Res_Page.SetFindCursor
//==============================================================================




INITIALIZATION//////////////////////////////////////////////////////////////////
  TframeCustomSRSC_Res_Page.FPagesClasses := TClassList.Create;

FINALIZATION////////////////////////////////////////////////////////////////////
  FreeAndNil(TframeCustomSRSC_Res_Page.FPagesClasses);

END.
