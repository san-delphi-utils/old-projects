UNIT frameSRSC_ResSettings_u;///////////////////////////////////////////////////
// Фрейм настроек ресурса
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CrossIntfs_u, SRSC_Types_u,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;
//==============================================================================
type
  TframeSRSC_ResSettings = class(TFrame, IToolsControl, IPage)
    lblDeclUnitFmt: TLabel;
    lblResPrefixFmt: TLabel;
    edtDeclUnitFmt: TEdit;
    edtResPrefixFmt: TEdit;
    chkConstsUpper: TCheckBox;

  private
    FToolsContext : ISRSC_ResSettingsHost;

  protected
    // IToolsControl
    function Get_ToolsContext : IUnknown;
    procedure Set_ToolsContext(const AToolsContext : IUnknown);
    procedure ReadTools;
    procedure WriteTools;

    // IPage
    function PageName : string;
    function PageCaption : string;
    procedure PlaceTo(const AParant : TWinControl);

  public
    constructor Create(AOwner : TComponent); override;

  end;//TframeSRSC_ResSettings
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
// TframeSRSC_ResSettings
//==============================================================================
// protected
//==============================================================================
// IToolsControlPage
//==============================================================================
function TframeSRSC_ResSettings.Get_ToolsContext: IUnknown;
begin
  Result := FToolsContext;
end;//TframeSRSC_ResSettings.Get_ToolsContext
//==============================================================================
procedure TframeSRSC_ResSettings.Set_ToolsContext(const AToolsContext : IUnknown);
begin
  FToolsContext := (AToolsContext as ISRSC_ResSettingsHost)
end;//TframeSRSC_ResSettings.Set_ToolsContext
//==============================================================================
procedure TframeSRSC_ResSettings.ReadTools;
begin
  edtDeclUnitFmt.Text     := FToolsContext.ResSettings.DeclUnitFmt;
  edtResPrefixFmt.Text    := FToolsContext.ResSettings.ResPrefixFmt;
  chkConstsUpper.Checked  := FToolsContext.ResSettings.ConstsUpper;
end;//TframeSRSC_ResSettings.ReadTools
//==============================================================================
procedure TframeSRSC_ResSettings.WriteTools;
var
  R : TSRSC_ResSettingsRec;
begin
  R.From(edtDeclUnitFmt.Text, edtResPrefixFmt.Text, chkConstsUpper.Checked);

  FToolsContext.ResSettings := R;

  FToolsContext.Save_ResSettings;
end;//TframeSRSC_ResSettings.WriteTools
//==============================================================================
// IPage
//==============================================================================
function TframeSRSC_ResSettings.PageName: string;
begin
  Result := 'ResSettings'
end;//TframeSRSC_ResSettings.PageName
//==============================================================================
function TframeSRSC_ResSettings.PageCaption: string;
begin
  Result := 'Настройки набора ресурсов'
end;//TframeSRSC_ResSettings.PageCaption
//==============================================================================
procedure TframeSRSC_ResSettings.PlaceTo(const AParant: TWinControl);
begin
  Parent            := AParant;
  ParentBackground  := False;
  ParentColor       := False;
end;//TframeSRSC_ResSettings.PlaceTo
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeSRSC_ResSettings.Create(AOwner: TComponent);
begin
  inherited;

  Name   := 'f' + PageName;
  Align  := alClient;
end;//TframeSRSC_ResSettings.Create
//==============================================================================


END.
