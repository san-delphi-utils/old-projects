UNIT SRSC_Storage_File_u;///////////////////////////////////////////////////////
// Франилище ресурса в виде файла
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  SRSC_Types_u,

  Windows, SysUtils, Classes, IniFiles, StrUtils,
  Dialogs;
//==============================================================================
TYPE
  TSRSC_Storage_File = class(TInterfacedObject, ISRSC_Res_Storage)
  private
    FFileName : TFileName;
    FFile: System.Text;

  protected
    procedure DoLoadFromFile(const ARes : TSRSC_Res);
    procedure DoSaveToFile(const ARes : TSRSC_Res);
    function CreateSettingsINI(const AProjectRes : TSRSC_ProjectRes) : TIniFile;

    // ISRSC_Res_Storage
    function StorageName : string;
    procedure LoadRes(const ARes : TSRSC_Res; const APath : string; const AAsTemp : boolean);
    procedure SaveRes(const ARes : TSRSC_Res; const APath : string; const AAsTemp : boolean);
    procedure LoadSettings(const AProjectRes : TSRSC_ProjectRes);
    procedure SaveSettings(const AProjectRes : TSRSC_ProjectRes);
    procedure ReadNames(const APath : string; const AStrings : TStrings; const AAsTemp : boolean);
    function EraseRes(const ARes : TSRSC_Res; const APath : string; const AAsTemp : boolean) : boolean;

  public
    class function FileExt(const AAsTemp : boolean = False) : string;

  end;//TSRSC_Storage_File
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================

type
  TSRSC_FileMrk = (srfmResSet, srfmConst, srfmFuncCode, srfmText, srfmEnd);
//==============================================================================
const
  SFM_BEG = '@';
  SFM_MARKS : array [TSRSC_FileMrk] of string = (
    'RES_SET', 'CONST', 'FUNC_CODE', 'TEXT', 'END'
  );//SFM_MARKS

  FILE_STORAGE_NAME = 'File';
//==============================================================================
function SFMText(const AMark : TSRSC_FileMrk) : string;
begin
  Result := SFM_BEG + SFM_MARKS[AMark];
end;//SFMText
//==============================================================================
function Qt(const AStr : string) : string;
begin
  Result := '"' + AStr + '"';
end;//Qt
//==============================================================================
function No1310(const AStr : string) : string;
var
  l : integer;
begin
  l := Length(AStr);

  if (l >= 2) and (AStr[l-1] = #13) and (AStr[l-1] = #13) then
    Result := Copy(AStr, 1, l - 2);
end;//No1310
//==============================================================================
function NextPrmInStr(const AStr : string; var ACharIdx : integer;
  var ASubStr : string) : boolean;
// Попытка извлечь параметр из строки AStr
// ACharIdx - индекс проверяемого символа
// ASubStr - подстрока параметра
// Возвращает True, если удалось извлеч параметр.
// Параметры разделены пробелами, данные в кавычках рассматриваются,
// как один параметр.
var
  vQuotes : boolean;
begin

  // Пропуск пробелов
  while (ACharIdx <= Length(AStr)) and (AStr[ACharIdx] = ' ') do
    Inc(ACharIdx);


  // Строка закончилась
  if ACharIdx > Length(AStr) then
    Exit(False);


  // Чтение параметра
  ASubStr := '';
  vQuotes := False;
  repeat

    case AStr[ACharIdx] of

      // Кавычки
      '"':
        // Воспринять две кавычки идущие подряд, как сивол кавычки
        if vQuotes and (ACharIdx < Length(AStr)) and (AStr[ACharIdx + 1] = '"') then
          Inc(ACharIdx)

        // Проглотить кавычку, начать/закончить режим чтения в кавычках
        else  begin
          vQuotes := not vQuotes;
          Inc(ACharIdx);
          Continue;
        end;//else if

      // Конец параметра
      ' ':
        if not vQuotes then
          Break;

    end;//case

    ASubStr := ASubStr + AStr[ACharIdx];

    Inc(ACharIdx);
  until ACharIdx > Length(AStr);


  // Если режим кавычек не закончен, строка не корректна
  Result := not vQuotes;
end;//NextPrmInStr
//==============================================================================

//==============================================================================
// TSRSC_Storage_File
//==============================================================================
// protected
//==============================================================================
procedure TSRSC_Storage_File.DoLoadFromFile(const ARes: TSRSC_Res);
// Загрузить набор констант из файла (подробности)
var
  vNames : TStringList;
//------------------------------------------------------------------------------
  function CreateConst(const AFullName : string) : TSRSC_Const;
  // Создание константы и промежуточных групп
  var
    i : integer;
    G1, G2 : TSRSC_Group;
  begin

    vNames.Text := AFullName;


    // Константа верхнего уровня
    if vNames.Count = 1 then
      Result := ARes.Items.AddConst(vNames[0], '')

    // В группе
    else  begin

      if not ARes.Items.FindEx(G1, vNames[0]) then
        G1 := ARes.Items.AddGroup(vNames[0]);

      // Промежуточные группы
      for i := 1 to vNames.Count - 2 do  begin

        if not ARes.Items.FindEx(G2, vNames[i]) then  begin
          G2 := ARes.Items.AddGroup(vNames[i]);
          G2.Group := G1;
        end;//if

        G1 := G2;
      end;//for

      // Константа
      Result := ARes.Items.AddConst(vNames[vNames.Count - 1], '');
      Result.Group := G1;
    end;//else if

  end;//CreateConst
//------------------------------------------------------------------------------
var
  S, vSub : string;
  L, i, vPrm : integer;
  vConst : TSRSC_Const;
begin
  L := 0;

  // Поиск начала
  while not EOF(FFile) do  begin
    Readln(FFile, S);
    Inc(L);

    if Pos( SFMText(srfmResSet), S) > 0 then
      Break;
  end;//while


  // Чтение основных параметров
  i := 1;
  for vPrm := 0 to 6 do
    if NextPrmInStr(S, i, vSub) then
      case vPrm of

        0: if CompareStr(SFMText(srfmResSet), vSub) <> 0 then
          raise ESRSCLoadError.CreateFmt('В файле «%s» неверное начало файла! Строка %d.', [FFileName, L]);

        1: if CompareStr(ARes.Name, vSub) <> 0 then
          raise ESRSCLoadError.CreateFmt('В файле «%s» неверное имя набора ресурсов (%s)! Строка %d.', [FFileName, vSub, L]);

        2: ARes.DeclUnit       := vSub;

        3: ARes.ResPrefix      := vSub;

        4: ARes.DefLangName    := vSub;

        5: ARes.InDll          := (CompareStr( BoolToStr(True, True), vSub) = 0);

        6: ARes.ConstUpper     := (CompareStr( BoolToStr(True, True), vSub) = 0);

      end//case
    else
      raise ESRSCLoadError.CreateFmt('В файле «%s» неверное количество параметров набора ресурсов! Строка %d.', [FFileName, L]);


  // Чтение констант
  vNames := TStringList.Create;
  try
    vNames.LineBreak := '\';

    while not EOF(FFile) do  begin
      Readln(FFile, S);
      Inc(L);

      if Pos( SFMText(srfmConst), S) = 0 then
        Continue;


      // Чтение параметров константы
      vConst := nil;
      i := 1;
      for vPrm := 0 to 4 do
        if NextPrmInStr(S, i, vSub) then
          case vPrm of

            0: if CompareStr(SFMText(srfmConst), vSub) <> 0 then
              raise ESRSCLoadError.CreateFmt('В файле «%s» неверное начало константы! Строка %d.', [FFileName, L]);

            1: vConst := CreateConst(vSub);

            2: vConst.LangName     := vSub;

            3: vConst.FuncAuto     := (CompareText(vSub, BoolToStr(True, True)) = 0);

            4: vConst.FuncParams   := vSub;
          end//case
        else
          raise ESRSCLoadError.CreateFmt('В файле «%s» неверное количество параметров константы! Строка %d.', [FFileName, L]);


      // Поиск начала тела функции
      while not EOF(FFile) do  begin
        Readln(FFile, S);
        Inc(L);

        if Pos( SFMText(srfmFuncCode), S) > 0 then
          Break;
      end;//while


      // Чтение тела функции
      vSub := '';
      while not EOF(FFile) do  begin
        Readln(FFile, S);
        Inc(L);

        if Pos( SFMText(srfmText), S) > 0 then
          Break;

        vSub := vSub + IfThen(vSub <> '', #13#10) + S;
      end;//while
      vConst.FuncCode := vSub;


      // Чтение текста константы
      vSub := '';
      while not EOF(FFile) do  begin
        Readln(FFile, S);
        Inc(L);

        if Pos( SFMText(srfmEnd), S) > 0 then
          Break;

        vSub := vSub + IfThen(vSub <> '', #13#10) + S;
      end;//while
      vConst.Data := vSub;

    end;//while

    ARes.Items.SortByOrder;
  finally
    FreeAndNil(vNames);
  end;//t..f

end;//TSRSC_Storage_File.DoLoadFromFile
//==============================================================================
procedure TSRSC_Storage_File.DoSaveToFile(const ARes : TSRSC_Res);
// Сохранить набор констант в файл (подробности)
var
  i : integer;
  vConst : TSRSC_Const;
begin

  // Заголовок
  Write(FFile,
    SFMText(srfmResSet),                                                    ' ',
    ARes.Name,                                                              ' ',
    ARes.DeclUnit,                                                          ' ',
    ARes.ResPrefix,                                                         ' ',
    ARes.DefLangName,                                                       ' ',
    BoolToStr(ARes.InDll, True),                                            ' ',
    BoolToStr(ARes.ConstUpper, True),                                    #13#10
  );


  // Элементы
  if ARes.Items.FirstConst(vConst, @i) then
    repeat

      with vConst do
        Write(FFile,
          SFMText(srfmConst),                                               ' ',
          Path,                                                             ' ',
          LangName,                                                         ' ',
          BoolToStr(FuncAuto, True),                                        ' ',
          Qt(FuncParams),                                                #13#10,
          SFMText(srfmFuncCode),                                         #13#10,
          FuncCode,                                                      #13#10,
          SFMText(srfmText),                                             #13#10,
          Data,                                                          #13#10,
          SFMText(srfmEnd),                                              #13#10
        );

    until not ARes.Items.NextConst(vConst, i + 1, @i);

end;//TSRSC_Storage_File.DoSaveToFile
//==============================================================================
function TSRSC_Storage_File.CreateSettingsINI(const AProjectRes: TSRSC_ProjectRes): TIniFile;
var
  vFileName : TFileName;
begin
  vFileName := AProjectRes.ProjectFileName;
  vFileName := StringReplace(vFileName, ExtractFileExt(vFileName), '.srscp', [rfIgnoreCase]);

  Result := TIniFile.Create(vFileName);
end;//TSRSC_Storage_File.CreateSettingsINI
//==============================================================================
function TSRSC_Storage_File.StorageName: string;
begin
  Result := FILE_STORAGE_NAME;
end;//TSRSC_Storage_File.StorageName
//==============================================================================
procedure TSRSC_Storage_File.LoadRes(const ARes: TSRSC_Res; const APath: string; const AAsTemp : boolean);
begin

  try
    FFileName := APath + ARes.Name + FileExt(AAsTemp);
    AssignFile(FFile, FFileName);
    Reset(FFile);
    try

      DoLoadFromFile(ARes);

    finally
      Close(FFile);
    end;//t..f

  except
    on E:Exception do  begin
      E.Message := Self.ClassName + '.LoadRes( File:«' + FFileName + '»)'#13#10 + E.Message;
      raise;
    end;//on..do
  end;//t..e

end;//TSRSC_Storage_File.LoadRes
//==============================================================================
procedure TSRSC_Storage_File.SaveRes(const ARes: TSRSC_Res; const APath: string; const AAsTemp : boolean);
begin

  try
    FFileName := APath + ARes.Name + FileExt(AAsTemp);
    AssignFile(FFile, FFileName);
    Rewrite(FFile);
    try

      DoSaveToFile(ARes);

    finally
      Close(FFile);
    end;//t..f

  except
    on E:Exception do  begin
      E.Message := Self.ClassName + '.SaveRes'#13#10 + E.Message;
      raise;
    end;//on..do
  end;//t..e

end;//TSRSC_Storage_File.SaveRes
//==============================================================================
procedure TSRSC_Storage_File.ReadNames(const APath: string; const AStrings: TStrings; const AAsTemp : boolean);
var
  vSR : TSearchRec;
  S : string;
begin

  AStrings.BeginUpdate;
  try

    AStrings.Clear;

    if FindFirst(APath + '*' + FileExt(AAsTemp), faAnyFile AND NOT faDirectory, vSR) = 0 then
      try

        repeat

          S := vSR.Name;
          SetLength(S, Length(S) - Length( ExtractFileExt(S) ));

          AStrings.Add(S);

        until FindNext(vSR) > 0;

      finally
        FindClose(vSR);
      end;//t..f

  finally
    AStrings.EndUpdate;
  end;//t..f

end;//TSRSC_Storage_File.ReadNames
//==============================================================================
procedure TSRSC_Storage_File.LoadSettings(const AProjectRes: TSRSC_ProjectRes);
var
  R : TSRSC_ResSettingsRec;
begin

  with CreateSettingsINI(AProjectRes) do  try
    R.DeclUnitFmt   := ReadString('Settings', 'DeclUnitFmt',   AProjectRes.Manager.ResSettings.DeclUnitFmt);
    R.ResPrefixFmt  := ReadString('Settings', 'ResPrefixFmt',  AProjectRes.Manager.ResSettings.ResPrefixFmt);
    R.ConstsUpper   := ReadBool(  'Settings', 'ConstsUpper',   AProjectRes.Manager.ResSettings.ConstsUpper);
  finally
    Free;
  end;//with

  AProjectRes.ResSettings := R;
end;//TSRSC_Storage_File.LoadSettings
//==============================================================================
procedure TSRSC_Storage_File.SaveSettings(const AProjectRes: TSRSC_ProjectRes);
begin

  with CreateSettingsINI(AProjectRes) do  try
    WriteString('Settings', 'DeclUnitFmt',   AProjectRes.ResSettings.DeclUnitFmt);
    WriteString('Settings', 'ResPrefixFmt',  AProjectRes.ResSettings.ResPrefixFmt);
    WriteBool(  'Settings', 'ConstsUpper',   AProjectRes.ResSettings.ConstsUpper);
  finally
    Free;
  end;//with

end;//TSRSC_Storage_File.SaveSettings
//==============================================================================
function TSRSC_Storage_File.EraseRes(const ARes : TSRSC_Res; const APath : string; const AAsTemp : boolean) : boolean;
begin
  Result := DeleteFile(APath + ARes.Name + FileExt(AAsTemp));
end;//TSRSC_Storage_File.EraseRes
//==============================================================================
//==============================================================================
// public
//==============================================================================
class function TSRSC_Storage_File.FileExt(const AAsTemp: boolean): string;
begin
  if AAsTemp then
    Result := '.~srsc'
  else
    Result := '.srsc'
end;//TSRSC_Storage_File.FileExt
//==============================================================================

INITIALIZATION//////////////////////////////////////////////////////////////////
  SRSCStorages.Reg(TSRSC_Storage_File.Create);

FINALIZATION////////////////////////////////////////////////////////////////////
  SRSCStorages.UnReg('File');

END.
