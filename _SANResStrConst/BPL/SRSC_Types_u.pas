UNIT SRSC_Types_u;//////////////////////////////////////////////////////////////
// Типы данных эксперта строковых констант в DLL ресурсов
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  Windows, SysUtils, Classes, Contnrs, Variants, Math, StrUtils, Types,
  Controls, Forms, ImgList, INIFiles;
//==============================================================================
CONST
  FS_EXCP_SYMBOLS = '\/:*?"<>|';

  LETTERS    : TSysCharSet = ['A'..'Z', 'a'..'z', '_'];
  DIGITS     : TSysCharSet = ['0'..'9'];
  EDIT_KEYS  : TSysCharSet = [CHR(VK_BACK), CHR(VK_INSERT), #$3, #$16, #$18];

  PLAIN_TEXT = 'PlainText';
//==============================================================================
TYPE
  ESRSCError        = class(Exception);
  ESRSCLoadError    = class(ESRSCError);
  ESRSCParseError   = class(ESRSCError);
  EGUIError         = class(ESRSCError);
  EGUIResError      = class(EGUIError);
  EGUIResPageError  = class(EGUIResError);
//==============================================================================
  TSRSC_Const              = class;
  TSRSC_Group              = class;
  TSRSC_CustomResItemList  = class;
  TSRSC_Res                = class;
  TSRSC_ResList            = class;
  TSRSC_ProjectRes         = class;
  TSRSC_ProjectResList     = class;
  TSRSC_Manager            = class;
//==============================================================================
  // Внешний редактор
  TSRSC_ExtEditer = record
    FileName, Params : string;
    ImageIndex : TImageIndex;
  end;//TSRSC_ExtEditer
//------------------------------------------------------------------------------
  // Набор настроек ресурса
  TSRSC_ResSettingsRec = record
    DeclUnitFmt, ResPrefixFmt : string;
    ConstsUpper : boolean;
    class operator Equal(A, B: TSRSC_ResSettingsRec): boolean;
    class operator NotEqual(A, B: TSRSC_ResSettingsRec): boolean;
    procedure From(const ADeclUnitFmt, AResPrefixFmt : string; const AConstsUpper : boolean);
  end;//TSRSC_ResSettingsRec
//------------------------------------------------------------------------------
  // Параметры поиска
  TSRSC_FindOption = (foName, foText, foBack, foIgnoreCase, foNoChangeRes);
  TSRSC_FindOptions = set of TSRSC_FindOption;
//------------------------------------------------------------------------------
  // Позиция при поиске
  TSRSC_FindCursor = record
    SRSCRes : TSRSC_Res;
    SRSCConst : TSRSC_Const;
    TextPos : integer;
    Text : string;
    Options : TSRSC_FindOptions;

    procedure Clear;
    procedure SetOptions(const ANameOpt, ATextOpt, ABackOpt, AIgnoreCaseOpt, ANoChangeResOpt : boolean);
  end;//TSRSC_FindCursor
//------------------------------------------------------------------------------
  TSRSC_DialogFunc0 = function : boolean;
//==============================================================================
  // Интерфейс взаимодействия с библиотекой экспертов
  IIDEEXT_MainToolsDLG = interface(IUnknown)
    ['{3C669B59-19D6-420B-9DC6-3A9AC7A2AA0E}']

    function ShowMainToolsDLG : boolean;
  end;//IIDEEXT_MainToolsDLG
//==============================================================================
  ISRSC_ResListEditer = interface(IUnknown)
    ['{A68846B4-D837-4356-B761-ADA9FB11D4E6}']

    procedure ResChange(const ARes : TSRSC_Res);

  end;//ISRSC_ResListEditer
//==============================================================================
  ISRSC_ResEditer = interface(IUnknown)
    ['{F2E2881B-070D-4643-BB4A-EEF18148D8EA}']

    function Get_Res : TSRSC_Res;
    procedure Set_Res(const Value : TSRSC_Res);
    property Res : TSRSC_Res  read Get_Res  write Set_Res;

    procedure ResUpdated;

  end;//ISRSC_ResEditer
//==============================================================================
  // Хранилище набора ресурсов
  ISRSC_Res_Storage = interface(IUnknown)
    ['{B779F1B1-2B02-4D2F-AF09-C4EAC87D124E}']

    // Уникальное имя хранилища
    function StorageName : string;

    // Загрузка/Сохранение одного набора
    procedure LoadRes(const ARes : TSRSC_Res; const APath : string; const AAsTemp : boolean);
    procedure SaveRes(const ARes : TSRSC_Res; const APath : string; const AAsTemp : boolean);

    // Загрузка/Сохранение настроек проекта
    procedure LoadSettings(const AProjectRes : TSRSC_ProjectRes);
    procedure SaveSettings(const AProjectRes : TSRSC_ProjectRes);

    // Получение списка наборов сохраненных по пути APath
    procedure ReadNames(const APath : string; const AStrings : TStrings; const AAsTemp : boolean);

    // Удалить набор из хранилища
    function EraseRes(const ARes : TSRSC_Res; const APath : string; const AAsTemp : boolean) : boolean;

  end;//ISRSC_Res_Storage
//==============================================================================
  // Интерфейс фрейма проекта
  ISRSC_ProjectResFrame = interface(IUnknown)
    ['{AF40A80A-911F-4155-AAA0-61FA75B7071B}']

    function Get_ProjectRes : TSRSC_ProjectRes;
    procedure Set_ProjectRes(const AProjectRes : TSRSC_ProjectRes);
    property ProjectRes : TSRSC_ProjectRes  read Get_ProjectRes  write Set_ProjectRes;

    function Get_CurRes: TSRSC_Res;
    procedure Set_CurRes(const ACurRes : TSRSC_Res);
    property CurRes : TSRSC_Res  read Get_CurRes  write Set_CurRes;

    function TrySelectConst(const APartOfNameToFind : string) : boolean;

  end;//ISRSC_ProjectResFrame
//==============================================================================
  // Интерфейс объекта с настройками ресурса
  ISRSC_ResSettingsHost = interface(IUnknown)
    ['{16C2F53E-8825-47C5-BA12-198A9BA779AE}']

    function Get_ResSettings : TSRSC_ResSettingsRec;
    procedure Set_ResSettings(const AResSettings : TSRSC_ResSettingsRec);
    property ResSettings : TSRSC_ResSettingsRec  read Get_ResSettings  write Set_ResSettings;

    procedure Save_ResSettings;

  end;//ISRSC_ResSettingsHost
//==============================================================================
  // Интерфейс объекта со списком внешних редакторов
  ISRSC_ExtEditersHost = interface(IUnknown)
    ['{B1E9CD62-A93F-4B9C-993B-7D9478DDF653}']

    function Get_ExtEditersCount: integer;
    property ExtEditersCount : integer  read Get_ExtEditersCount;

    function Get_ExtEditers(const Index: integer): TSRSC_ExtEditer;
    property ExtEditers[const Index : integer] : TSRSC_ExtEditer  read Get_ExtEditers;

    function Get_ExtEditersImages: TImageList;
    property ExtEditersImages : TImageList  read Get_ExtEditersImages;

    function ExtEditers_Add(const AFileName, AParams : string; const AImageIndex : TImageIndex = -1) : TSRSC_ExtEditer;
    procedure ExtEditers_Delete(const AIndex : integer);
    procedure ExtEditers_Clear;
    procedure ExtEditers_Save;

  end;//ISRSC_ExtEditersHost
//==============================================================================
  {$I ..\SRSC_Intfs.inc}
//==============================================================================
  // Абстрактный элемент Dll строковых констант
  TSRSC_CustomResItem = class(TPersistent)
  private
    FOwnerList : TSRSC_CustomResItemList;

    FName : string;

    // Родительская группа
    FGroup : TSRSC_Group;

  private
    procedure Set_Group(const Value: TSRSC_Group);
    function Get_Level: integer;

  protected
    procedure AssignTo(Dest : TPersistent); override;

  public
    constructor Create(const AOwnerList : TSRSC_CustomResItemList; const AName : string); virtual;
    destructor Destroy; override;

    class function UniqueBaseName : string; virtual;

    function Path(const ASep : string = '\'; const ASelfName : boolean = True) : string;

  public
    property OwnerList : TSRSC_CustomResItemList  read FOwnerList;

    // Имя элемента
    property Name : string  read FName  write FName;

    property Group : TSRSC_Group  read FGroup  write Set_Group;

    property Level : integer  read Get_Level;

  end;//TSRSC_CustomResItem
  //----------------------------------------------------------------------------
  TSRSC_CustomResItemClass = class of TSRSC_CustomResItem;
//==============================================================================
  // Константа
  TSRSC_Const = class(TSRSC_CustomResItem)
  private
    FData, FLangName, FFuncParams, FFuncCode : string;
    FFuncAuto : boolean;

  protected
    procedure AssignTo(Dest : TPersistent); override;

  public
    constructor Create(const AOwnerList : TSRSC_CustomResItemList; const AName : string); override;

    class function UniqueBaseName : string; override;

    function FindInConstName(const ACurrentPos: TSRSC_FindCursor): boolean;
    function FindInConstText(var ACurrentPos : TSRSC_FindCursor): boolean;


  public
    property Data        : string   read FData        write FData;
    property LangName    : string   read FLangName    write FLangName;
    property FuncParams  : string   read FFuncParams  write FFuncParams;
    property FuncCode    : string   read FFuncCode    write FFuncCode;
    property FuncAuto    : boolean  read FFuncAuto    write FFuncAuto;

  end;//TSRSC_Const
//==============================================================================
  // Логическая группа констант
  TSRSC_Group = class(TSRSC_CustomResItem)
  private
    FSubItems : TSRSC_CustomResItemList;

  public
    constructor Create(const AOwnerList : TSRSC_CustomResItemList; const AName : string); override;
    destructor Destroy; override;

    class function UniqueBaseName : string; override;

  public
    property SubItems : TSRSC_CustomResItemList  read FSubItems;

  end;//TSRSC_Group
//==============================================================================
  // Список элементов
  TSRSC_SearchProc = procedure (const AItem : TSRSC_CustomResItem; const AParam : Pointer;
    var AMatch : boolean) of object;
  //----------------------------------------------------------------------------
  TSRSC_CustomResItemList = class(TObjectList)
  private
    FGroupsCount, FConstsCount : integer;

  private
    function GetItem(const Index: Integer): TSRSC_CustomResItem;
    function Get_Groups(const Index: integer): TSRSC_Group;
    function Get_Consts(const Index: integer): TSRSC_Const;

  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;

    procedure SearchConst(const AItem : TSRSC_CustomResItem; const AParam : Pointer;
      var AMatch : boolean);

  public
    function Extract(Item: TSRSC_CustomResItem): TSRSC_CustomResItem;
    function ExtractItem(Item: TSRSC_CustomResItem; Direction: TList.TDirection): TSRSC_CustomResItem;
    function First: TSRSC_CustomResItem;
    function Last: TSRSC_CustomResItem;

    function FindEx(var AItem; const AName : string; const AExclude : TSRSC_CustomResItem = nil) : boolean;
    function Exists(const AName : string; const AExclude : TSRSC_CustomResItem = nil) : boolean;

    function FirstIF(var AItem; const ASearchProc : TSRSC_SearchProc; const ASearchParam : Pointer = nil; const AInsexPtr : PInteger = nil) : boolean;
    function PrevIF(var AItem; const ASearchProc : TSRSC_SearchProc; const ASearchParam : Pointer = nil; AIndex : integer = -1; const AInsexPtr : PInteger = nil) : boolean;
    function NextIF(var AItem; const ASearchProc : TSRSC_SearchProc; const ASearchParam : Pointer = nil; AIndex : integer = -1; const AInsexPtr : PInteger = nil) : boolean;
    function LastIF(var AItem; const ASearchProc : TSRSC_SearchProc; const ASearchParam : Pointer = nil; const AInsexPtr : PInteger = nil) : boolean;

    function FirstConst(var AItem; const AInsexPtr : PInteger = nil) : boolean;
    function PrevConst(var AItem; const AIndex : integer = -1; const AInsexPtr : PInteger = nil) : boolean;
    function NextConst(var AItem; const AIndex : integer = -1; const AInsexPtr : PInteger = nil) : boolean;
    function LastConst(var AItem; const AInsexPtr : PInteger = nil) : boolean;

    procedure SortByOrder;

    function AddGroup(const AName: string): TSRSC_Group;
    function AddConst(const AName, AData: string): TSRSC_Const;

  public
    property Items[const Index: Integer]: TSRSC_CustomResItem read GetItem; default;

    property GroupsCount : integer read FGroupsCount;
    property Groups[const Index : integer] : TSRSC_Group read Get_Groups;

    property ConstsCount : integer read FConstsCount;
    property Consts[const Index : integer] : TSRSC_Const read Get_Consts;

  end;//TSRSC_CustomResItemList
//==============================================================================
  TSRSC_Res = class(TPersistent)
  private
    FOwnerList : TSRSC_ResList;

    // Несохраненные данные
    FIsUnCommited : boolean;

    // Добавлен впервые и не сохранен
    FUnCommitedAdd : boolean;

    // Имя набора ресурсов; Имя модуля с вызывами; Формат ресурсов; Язык по умолчанию;
    FName, FDeclUnit, FResPrefix, FDefLangName : string;
    // Признак размещения в отдельной DLL; Имена констант в верхнем регистре
    FInDll, FConstUpper : boolean;

    FItems : TSRSC_CustomResItemList;

    // Старые значения Name и DeclUnit
    FOldName, FOldDeclUnit : string;

  private
    procedure Set_Name(const Value: string);
    procedure Set_DeclUnit(const Value: string);
    function Get_Manager: TSRSC_Manager;

  protected

  public
    constructor Create(const AName, ADeclUnit, AResPrefix : string);
    destructor Destroy; override;

    procedure Load(const AStorage : ISRSC_Res_Storage; const APath : string; const AAsTemp : boolean = False);
    procedure Save(const AStorage : ISRSC_Res_Storage; const APath : string; const AAsTemp : boolean = False);
    procedure Erase(const AStorage : ISRSC_Res_Storage; const APath : string; const AAsTemp : boolean = False);
    procedure EraseOld(const AStorage : ISRSC_Res_Storage; const APath : string);

    // Хранилище в текстовом файле
    procedure LoadFromFile(const APath : string);
    procedure SaveToFile(const APath : string);


    // Добавить элемент с пересортировкой
    function AddItem(const AItem : TSRSC_CustomResItem) : integer;


    function GenUniqueName(const ABaseName : string) : string;
    function CanRenameItem(const AItem : TSRSC_CustomResItem; const ANewName : string) : boolean;

    function FindInRes(var ACurrentPos : TSRSC_FindCursor): boolean;


    // Дополнить константами из ресурса
    // True, если есть дополнения
    function CompleteFrom(const AFromRes : TSRSC_Res) : boolean;

  public
    property OwnerList : TSRSC_ResList  read FOwnerList;
    property Manager : TSRSC_Manager  read Get_Manager;

    // Несохраненные данные
    property IsUnCommited : boolean  read FIsUnCommited;

    // Добавлен впервые и не сохранен
    property UnCommitedAdd : boolean  read FUnCommitedAdd;

    // Текущие параметры
    property Name         : string   read FName         write Set_Name;
    property DeclUnit     : string   read FDeclUnit     write Set_DeclUnit;
    property ResPrefix    : string   read FResPrefix    write FResPrefix;
    property DefLangName  : string   read FDefLangName  write FDefLangName;
    property InDll        : boolean  read FInDll        write FInDll;
    property ConstUpper   : boolean  read FConstUpper   write FConstUpper;

    property Items : TSRSC_CustomResItemList  read FItems;

  end;//TSRSC_Res
//==============================================================================
  // Список наборов ресурсов
  TSRSC_ResList = class(TObjectList)
  private
    FProjectRes : TSRSC_ProjectRes;

  private
    function GetItem(const Index: Integer): TSRSC_Res;

  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;

  public
    function Extract(Item: TSRSC_Res): TSRSC_Res;
    function ExtractItem(Item: TSRSC_Res; Direction: TList.TDirection): TSRSC_Res;
    function First: TSRSC_Res;
    function Last: TSRSC_Res;

    function FindEx(var AItem; const AName : string; const AExclude : TSRSC_Res = nil) : boolean;
    function Find(const AName : string; const AExclude : TSRSC_Res = nil) : TSRSC_Res;
    function Exists(const AName : string; const AExclude : TSRSC_Res = nil) : boolean;

    function Next(var AItem; AIndex : integer = -1; const AIndexPtr : PInteger = nil) : boolean;

    function Add(const AName, ADeclUnit, AResPrefix : string): TSRSC_Res;

    function GenUniqueName(const ABaseName : string) : string;

  public
    property Items[const Index: Integer]: TSRSC_Res read GetItem; default;
    property ProjectRes : TSRSC_ProjectRes  read FProjectRes;

  end;//TSRSC_ResList
//==============================================================================
  // Объект соответствубщий одному проекту
  TSRSC_ProjectRes = class(TInterfacedPersistent, ISRSC_ProjectRes, ISRSC_ResSettingsHost)
  private
    FOwnerList : TSRSC_ProjectResList;
    FProvider : ISRSC_ProjectProvider;
    FProviderDestroying : boolean;

    FResList : TSRSC_ResList;

    FResSettings : TSRSC_ResSettingsRec;

    FViewer : TCustomFrame;
    FCurRes : TSRSC_Res;

  private
    function Get_Manager: TSRSC_Manager;
    function Get_ProjectFileName: string;
    function Get_ProjectOutputPath: string;
    function Get_ProjectPath: string;


  protected
    // ISRSC_ProjectRes
    procedure ProviderDestroying;
    procedure Show(const APartOfNameToFind : string = '');
    procedure Hide;

    function GetModuleSource(const AResName, ANewDeclUnitName, AOldDeclUnitName : string) : string;
    procedure ReplaceOldValuesInSource(const AOldName, ANewName, AOldDeclUnit, ANewDeclUnit : string;
      const ASource : TStrings);
    procedure FillModuleSource(const ARes : TSRSC_Res; const ASource : TStrings);
    procedure SetModuleSource(const ADeclUnitName, ASource : string);

    function Get_Provider : ISRSC_ProjectProvider;
    procedure Set_Provider(const AProvider : ISRSC_ProjectProvider);

    // ISRSC_ResSettingsHost
    function Get_ResSettings : TSRSC_ResSettingsRec;
    procedure Set_ResSettings(const AResSettings : TSRSC_ResSettingsRec);
    procedure Save_ResSettings;

  public
    constructor Create(const AProvider : ISRSC_ProjectProvider);
    destructor Destroy; override;

    procedure LoadResList;

    // ISRSC_ProjectRes
    procedure DeleteRes;
    procedure CommitRes(const AEmergencyMode : boolean = False);
    procedure RollbackRes(const AEmergencyMode : boolean = False);

    // Сохранение несохраненных данных
    procedure SaveUncommitedData;
    // Удалить уставевшие данные
    procedure EraseOldUncommitedData(const ARes : TSRSC_Res);

    function AddRes(const AName : string; const AUnCommitedAdd : boolean) : TSRSC_Res;

    // Поиск по списку ресурсов
    function FindInRess(var AFindCursor : TSRSC_FindCursor) : boolean;


  public
    property OwnerList : TSRSC_ProjectResList  read FOwnerList;
    property Manager : TSRSC_Manager  read Get_Manager;

    property Provider : ISRSC_ProjectProvider  read Get_Provider  write Set_Provider;

    property ProjectFileName    : string  read Get_ProjectFileName;
    property ProjectPath        : string  read Get_ProjectPath;
    property ProjectOutputPath  : string  read Get_ProjectOutputPath;

    property ResList : TSRSC_ResList  read FResList;

    property ResSettings : TSRSC_ResSettingsRec  read Get_ResSettings  write Set_ResSettings;

    property Viewer : TCustomFrame  read FViewer;
    property CurRes : TSRSC_Res  read FCurRes  write FCurRes;

  end;//TSRSC_ProjectRes
//==============================================================================
  TSRSC_ProjectResList = class(TObjectList)
  private
    FManager : TSRSC_Manager;

  private
    function GetItem(const Index: Integer): TSRSC_ProjectRes;

  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;

  public
    function Extract(Item: TSRSC_ProjectRes): TSRSC_ProjectRes;
    function ExtractItem(Item: TSRSC_ProjectRes; Direction: TList.TDirection): TSRSC_ProjectRes;
    function First: TSRSC_ProjectRes;
    function Last: TSRSC_ProjectRes;

    function FindEx(var AProjectRes; const AProjectFileName : string) : boolean;
    function Find(const AProjectFileName : string) : TSRSC_ProjectRes;
    function Add(const AProvider : ISRSC_ProjectProvider) : TSRSC_ProjectRes;

  public
    property Items[const Index: Integer]: TSRSC_ProjectRes read GetItem; default;
    property Manager : TSRSC_Manager  read FManager;

  end;//TSRSC_ProjectResList
//==============================================================================
  // Главный объект библиотеки. Экземпляр передается через InitBPLExpert.
  TSRSC_Manager = class(TInterfacedPersistent, ISRSC_Manager, ISRSC_ResSettingsHost,
    IIDEEXT_MainToolsDLG, ISRSC_ExtEditersHost)
  private
    FProjectResList : TSRSC_ProjectResList;

    FExtEditers : TStrings;
    FExtEditersImages : TImageList;

    FResSettings : TSRSC_ResSettingsRec;

    FViewerClass : TCustomFrameClass;

    // Функция вызова главного диалога
    FMainToolsDLGFunc : TSRSC_DialogFunc0;

  private

  protected
    // ISRSC_Manager
    function FindProjectRes(const AProjectFileName : string) : ISRSC_ProjectRes;
    function AddProjectRes(const AProjectProvider : ISRSC_ProjectProvider) : ISRSC_ProjectRes;

    // ISRSC_ResSettingsHost
    function Get_ResSettings : TSRSC_ResSettingsRec;
    procedure Set_ResSettings(const AResSettings : TSRSC_ResSettingsRec);
    procedure Save_ResSettings;

    // IIDEEXT_MainToolsDLG
    function ShowMainToolsDLG : boolean;

    // ISRSC_ExtEditersHost
    function Get_ExtEditersCount: integer;
    function Get_ExtEditers(const Index: integer): TSRSC_ExtEditer;
    function Get_ExtEditersImages: TImageList;
    function ExtEditers_Add(const AFileName, AParams : string;
      const AImageIndex : TImageIndex = -1) : TSRSC_ExtEditer;
    procedure ExtEditers_Delete(const AIndex : integer);
    procedure ExtEditers_Clear;
    procedure ExtEditers_Save;

  public
    constructor Create;
    destructor Destroy; override;

    function AppDataPath : string;
    function AppDataTempPath : string;
    function ConstTempFileName(const AConstName : string) : string;

    procedure LoadConfig;

    function CreateResFile(const ARes : TSRSC_Res) : string;
    function CreateResDLL(const AResFileName : TFileName) : string;

  public
    property ProjectResList : TSRSC_ProjectResList  read FProjectResList;

    property ExtEditersCount : integer  read Get_ExtEditersCount;
    property ExtEditers[const Index : integer] : TSRSC_ExtEditer  read Get_ExtEditers;
    property ExtEditersImages : TImageList  read Get_ExtEditersImages;

    property ResSettings : TSRSC_ResSettingsRec  read Get_ResSettings  write Set_ResSettings;

    property ViewerClass : TCustomFrameClass  read FViewerClass  write FViewerClass;

    property MainToolsDLGFunc : TSRSC_DialogFunc0  read FMainToolsDLGFunc  write FMainToolsDLGFunc;

  end;//TSRSC_Manager
//==============================================================================
  TSRSC_Storages = class(TPersistent)
  private
    // Список хранилищ ресурсов
    FStorages : TInterfaceList;
  private
    function Get_Count: integer;
    function Get_Items(const Index: integer): ISRSC_Res_Storage;
    function Get_ByName(const AStorageName: string): ISRSC_Res_Storage;

  public
    constructor Create;
    destructor Destroy; override;

    // Работа сосписком хранилищ
    procedure Reg(const AStorage : ISRSC_Res_Storage);
    function Find(const AStorageName : string; const AIndexPtr : PInteger = nil) : ISRSC_Res_Storage;
    function UnReg(const AStorageName : string) : boolean;

  public
    property Count : integer  read Get_Count;
    property Items[const Index : integer] : ISRSC_Res_Storage  read Get_Items;
    property ByName[const AStorageName : string] : ISRSC_Res_Storage  read Get_ByName; default;
  end;//TSRSC_Storages
//==============================================================================
function GetAssociatedIcon(const AFileOrDir : string; const ALarge : boolean): HIcon;
function StrIsFmt(const AStr, AFmt : string; const ACaseSens : boolean; const AArgs : array of const) : boolean;
function Compare_SRSC_LGCN(Item1, Item2: Pointer): Integer;
function SRSCStorages : TSRSC_Storages;
function SRSCManager : TSRSC_Manager;
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
uses
  ShellAPI, SHFolder, CommCtrl;
//==============================================================================

//==============================================================================
function GetAssociatedIcon(const AFileOrDir : string; const ALarge : boolean): HIcon;
var
  vSHFILEINFO : TSHFileInfo;
  vFlags : integer;
begin
  vFlags := SHGFI_ICON or (SHGFI_LARGEICON * ORD(ALarge)) or (SHGFI_SMALLICON * ORD(not ALarge));
  if FileExists(AFileOrDir) then
    vFlags := vFlags or SHGFI_USEFILEATTRIBUTES;

  SHGetFileInfo(PChar(AFileOrDir), FILE_ATTRIBUTE_NORMAL, vSHFILEINFO, SizeOf(TSHFileInfo), vFlags);

  Result := vSHFILEINFO.hIcon;
end;//GetAssociatedIcon
//==============================================================================
function GetSpecialFolderPath(Folder : integer) : string;
const
  SHGFP_TYPE_CURRENT = 0;
var
  path: array [0..MAX_PATH] of char;
begin
  if SUCCEEDED(SHGetFolderPath(0,Folder,0,SHGFP_TYPE_CURRENT,@path[0])) then
    Result := path
  else
    Result := '';
end;//GetSpecialFolderPath
//==============================================================================
procedure DeleteDir(ADir : string);
var
  vSR : TSearchRec;
begin

  if (Length(ADir) > 0) and (ADir[Length(ADir)] = '\') then
    SetLength(ADir, Length(ADir) - 1);

  if FindFirst(ADir + '\*.*', faAnyFile, vSR) = 0 then
    try

      repeat

        if AnsiMatchText(vSR.Name, ['.', '..']) then
          Continue;

        if vSR.Attr AND faDirectory = faDirectory then
          DeleteDir(ADir + '\' + vSR.Name)
        else
          DeleteFile(ADir + '\' + vSR.Name);

      until FindNext(vSR) > 0;

    finally
      FindClose(vSR);
    end;//t..f

  RmDir(ADir);
end;//DeleteDir
//==============================================================================
function StrIsFmt(const AStr, AFmt : string; const ACaseSens : boolean; const AArgs : array of const) : boolean;
// Является ли строка AStr строкой формата AFmt
var
  S : string;
begin
  S := Format(AFmt, AArgs);

  if ACaseSens then
    Result := (AnsiCompareStr(AStr, S) = 0)
  else
    Result := (AnsiCompareText(AStr, S) = 0);

end;//StrIsFmt
//==============================================================================
function Compare_SRSC_LGCN(Item1, Item2: Pointer): Integer;
var
  vItem1, vItem2 : TSRSC_CustomResItem;
  L1, L2 : integer;
begin

  if Item1 = Item2 then
    Exit(0);


  vItem1 := TSRSC_CustomResItem(Item1);
  vItem2 := TSRSC_CustomResItem(Item2);


  // Группы раньше констант
  Result := CompareValue( ORD(vItem1 is TSRSC_Const), ORD(vItem2 is TSRSC_Const));
  if Result <> 0 then
    Exit;


  L1 := vItem1.Level;
  L2 := vItem2.Level;


  case CompareValue(L1, L2) of
    LessThanValue:  begin

      while L1 < L2 do  begin
        vItem2 := vItem2.Group;
        Dec(L2);
      end;//while

      if vItem2 = vItem1 then
        Result := -1
      else
        Result := Compare_SRSC_LGCN(vItem1, vItem2);
    end;//LessThanValue

    GreaterThanValue: begin

      while L1 > L2 do  begin
        vItem1 := vItem1.Group;
        Dec(L1);
      end;//while

      if vItem1 = vItem2 then
        Result := 1
      else
        Result := Compare_SRSC_LGCN(vItem1, vItem2);

    end;//GreaterThanValue

  // Сравнение имен
  else
    Result := CompareText(vItem1.Name, vItem2.Name);

  end;//case

end;//Compare_SRSC_LGCN
//==============================================================================
function NOA(const ACnd, ACndExt : boolean) : boolean;
begin
  Result := not ACnd or ACnd and ACndExt;
end;//NOA
//==============================================================================

//==============================================================================
// TSRSC_ResSettingsRec
//==============================================================================
class operator TSRSC_ResSettingsRec.Equal(A, B: TSRSC_ResSettingsRec): boolean;
begin
  Result := (CompareStr(A.DeclUnitFmt,   B.DeclUnitFmt) = 0)
        and (CompareStr(A.ResPrefixFmt,  B.ResPrefixFmt) = 0)
        and (A.ConstsUpper = B.ConstsUpper);
end;//TSRSC_ResSettingsRec.Equal
//==============================================================================
class operator TSRSC_ResSettingsRec.NotEqual(A, B: TSRSC_ResSettingsRec): boolean;
begin
  Result := not(A = B);
end;//TSRSC_ResSettingsRec.NotEqual
//==============================================================================
procedure TSRSC_ResSettingsRec.From(const ADeclUnitFmt, AResPrefixFmt: string;
  const AConstsUpper: boolean);
begin
  Self.DeclUnitFmt   := ADeclUnitFmt;
  Self.ResPrefixFmt  := AResPrefixFmt;
  Self.ConstsUpper   := AConstsUpper;
end;//TSRSC_ResSettingsRec.From
//==============================================================================

//==============================================================================
// TSRSC_FindCursor
//==============================================================================
procedure TSRSC_FindCursor.Clear;
begin
  // Предотвращение утечек памяти при FillChar для непустой строки
  Self.Text := '';

  FillChar(Self, SizeOf(TSRSC_FindCursor), 0);
end;//TSRSC_FindCursor.Clear
//==============================================================================
procedure TSRSC_FindCursor.SetOptions(const ANameOpt, ATextOpt, ABackOpt,
  AIgnoreCaseOpt, ANoChangeResOpt : boolean);
var
  vOpt : TSRSC_FindOption;
  vPBool : PBoolean;
begin
  Self.Options := [];

  for vOpt := Low(TSRSC_FindOption) to High(TSRSC_FindOption) do
  begin

    case vOpt of
      foName:         vPBool := @ANameOpt;
      foText:         vPBool := @ATextOpt;
      foBack:         vPBool := @ABackOpt;
      foIgnoreCase:   vPBool := @AIgnoreCaseOpt;
      foNoChangeRes:  vPBool := @ANoChangeResOpt;
      else            vPBool := nil;
    end;//case

    if vPBool^ then
      Include(Self.Options, vOpt)
    else
      Exclude(Self.Options, vOpt);
  end;//for

end;//TSRSC_FindCursor.SetOptions
//==============================================================================

//==============================================================================
// TSRSC_CustomResItem
//==============================================================================
// private
//==============================================================================
procedure TSRSC_CustomResItem.Set_Group(const Value: TSRSC_Group);
begin

  if FGroup = Value then
    Exit;

  if Assigned(FGroup) then
    FGroup.FSubItems.Extract(Self);

  FGroup := Value;

  if Assigned(FGroup) then
    FGroup.FSubItems.Add(Self);

end;//TSRSC_CustomResItem.Set_Group
//==============================================================================
function TSRSC_CustomResItem.Get_Level: integer;
begin
  if Assigned(Group) then
    Result := Group.Level + 1
  else
    Result := 0;
end;//TSRSC_CustomResItem.Get_Level
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TSRSC_CustomResItem.AssignTo(Dest: TPersistent);
var
  vGroup : TSRSC_CustomResItem;
begin

  if not (Dest is TSRSC_CustomResItem) then
    inherited

  else
    with Dest as TSRSC_CustomResItem do
    begin

      Name := Self.Name;

      // Поиск аналогичной группы
      if  Assigned(Self.Group)
      and Assigned(OwnerList)
      and OwnerList.FindEx(vGroup, Self.Group.Name)
      and (vGroup is TSRSC_Group)
      then
        Group := vGroup as TSRSC_Group;

    end;

end;//TSRSC_CustomResItem.AssignTo
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSRSC_CustomResItem.Create(const AOwnerList : TSRSC_CustomResItemList; const AName: string);
begin
  inherited Create;

  FOwnerList := AOwnerList;
  FName := AName;
end;//TSRSC_CustomResItem.Create
//==============================================================================
destructor TSRSC_CustomResItem.Destroy;
begin

  if Assigned(FOwnerList) then
    FOwnerList.Extract(Self);

  inherited;
end;//TSRSC_CustomResItem.Destroy
//==============================================================================
class function TSRSC_CustomResItem.UniqueBaseName: string;
begin
  Result := 'Item'
end;//TSRSC_CustomResItem.UniqueBaseName
//==============================================================================
function TSRSC_CustomResItem.Path(const ASep: string; const ASelfName: boolean): string;
// Путь из имен групп
var
  vItem : TSRSC_CustomResItem;
begin

  if ASelfName then
    Result := Name
  else
    Result := '';

  vItem := Group;
  while Assigned(vItem) do  begin
    Result := vItem.Name + ASep + Result;
    vItem := vItem.Group;
  end;//while

end;//TSRSC_CustomResItem.Path
//==============================================================================

//==============================================================================
// TSRSC_Const
//==============================================================================
// protected
//==============================================================================
procedure TSRSC_Const.AssignTo(Dest: TPersistent);
begin

  if (Dest is TSRSC_Const) then
    with Dest as TSRSC_Const do
    begin
      Data        := Self.Data;
      LangName    := Self.LangName;
      FuncParams  := Self.FuncParams;
      FuncCode    := Self.FuncCode;
      FuncAuto    := Self.FuncAuto;
    end;

  inherited;
end;//TSRSC_Const.AssignTo
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSRSC_Const.Create(const AOwnerList: TSRSC_CustomResItemList;
  const AName: string);
begin
  inherited;

  FFuncCode := 'Result := $(Const)';
  FLangName := PLAIN_TEXT;
  FFuncAuto := True;
end;//TSRSC_Const.Create
//==============================================================================
class function TSRSC_Const.UniqueBaseName: string;
begin
  Result := 'Const';
end;//TSRSC_Const.UniqueBaseName
//==============================================================================
function TSRSC_Const.FindInConstName(const ACurrentPos: TSRSC_FindCursor): boolean;
// Поиск в имени константы
var
  vName, vText: string;
begin

  if foBack in ACurrentPos.Options then
    begin
      vName := ReverseString(Name);
      vText := ReverseString(ACurrentPos.Text);
    end
  else
    begin
      vName := Name;
      vText := ACurrentPos.Text;
    end;//else if

  if foIgnoreCase in ACurrentPos.Options then  begin
    vName := AnsiUpperCase(vName);
    vText := AnsiUpperCase(vText);
  end;//if

  Result := (Pos(vText, vName) > 0);
end;//TSRSC_Const.FindInConstName
//==============================================================================
function TSRSC_Const.FindInConstText(var ACurrentPos: TSRSC_FindCursor): boolean;
// Поиск в тексте константы
var
  vData, vText : string;
  L, vPos, vOffset : integer;
begin
  L := Length(Data);

  if (ACurrentPos.TextPos < 1) or (ACurrentPos.TextPos > L) then
    ACurrentPos.TextPos := 0;


  if foBack in ACurrentPos.Options then
    begin
      vData := ReverseString(Data);
      vText := ReverseString(ACurrentPos.Text);

      if ACurrentPos.TextPos = 0 then
        vOffset := 1
      else
        vOffset := L - ACurrentPos.TextPos + 2;

    end
  else
    begin
      vData := Data;
      vText := ACurrentPos.Text;

      if ACurrentPos.TextPos = 0 then
        vOffset := 1
      else
        vOffset := ACurrentPos.TextPos + Length(vText);
    end;//else if


  if foIgnoreCase in ACurrentPos.Options then  begin
    vData := AnsiUpperCase(vData);
    vText := AnsiUpperCase(vText);
  end;//if



  vPos := PosEx(vText, vData, vOffset);

  Result := (vPos > 0);

  if not Result then
    Exit;


  if foBack in ACurrentPos.Options then
    ACurrentPos.TextPos := L - (vPos + Length(vText)) + 2
  else
    ACurrentPos.TextPos := vPos;

end;//TSRSC_Const.FindInConstText
//==============================================================================

//==============================================================================
// TSRSC_Group
//==============================================================================
// public
//==============================================================================
constructor TSRSC_Group.Create(const AOwnerList : TSRSC_CustomResItemList; const AName: string);
begin
  inherited;

  FSubItems := TSRSC_CustomResItemList.Create;
  FSubItems.OwnsObjects := False;
end;//TSRSC_Group.Create
//==============================================================================
destructor TSRSC_Group.Destroy;
begin
  FreeAndNil(FSubItems);

  inherited;
end;//TSRSC_Group.Destroy
//==============================================================================
class function TSRSC_Group.UniqueBaseName: string;
begin
  Result := 'Group';
end;//TSRSC_Group.UniqueBaseName
//==============================================================================

//==============================================================================
// TSRSC_CustomResItemList
//==============================================================================
// private
//==============================================================================
function TSRSC_CustomResItemList.GetItem(const Index: Integer): TSRSC_CustomResItem;
begin
  Result := TSRSC_CustomResItem(inherited Items[Index]);
end;//TSRSC_CustomResItemList.GetItem
//==============================================================================
function TSRSC_CustomResItemList.Get_Groups(const Index: integer): TSRSC_Group;
var
  i, vIndex : integer;
begin
  vIndex := 0;

  for i := 0 to Count - 1 do
    if Items[i] is TSRSC_Group then  begin

      if Index = vIndex then  begin
        Result := (Items[i] as TSRSC_Group);
        Exit;
      end;//if

      Inc(vIndex);
    end;//if

  Result := nil;
end;//TSRSC_CustomResItemList.Get_Groups
//==============================================================================
function TSRSC_CustomResItemList.Get_Consts(const Index: integer): TSRSC_Const;
var
  i, vIndex : integer;
begin
  vIndex := 0;

  for i := 0 to Count - 1 do
    if Items[i] is TSRSC_Const then  begin

      if Index = vIndex then  begin
        Result := (Items[i] as TSRSC_Const);
        Exit;
      end;//if

      Inc(vIndex);
    end;//if

  Result := nil;
end;//TSRSC_CustomResItemList.Get_Consts
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TSRSC_CustomResItemList.Notify(Ptr: Pointer; Action: TListNotification);
var
  vPCntr : PInteger;
begin

  if (TObject(Ptr) is TSRSC_Group) then
    vPCntr := @FGroupsCount
  else
    vPCntr := @FConstsCount;

  if Action = lnAdded then
    begin
      Inc(vPCntr^);

      if OwnsObjects and (not Assigned(TSRSC_CustomResItem(Ptr).FOwnerList)) then
        TSRSC_CustomResItem(Ptr).FOwnerList := Self;
    end

  else
    begin
      Dec(vPCntr^);

      TSRSC_CustomResItem(Ptr).FOwnerList := nil;
    end;//else if

  inherited;
end;//TSRSC_CustomResItemList.Notify
//==============================================================================
procedure TSRSC_CustomResItemList.SearchConst(const AItem: TSRSC_CustomResItem;
  const AParam: Pointer; var AMatch: boolean);
begin
  AMatch := (AItem is TSRSC_Const);
end;//TSRSC_CustomResItemList.SearchConst
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TSRSC_CustomResItemList.Extract(Item: TSRSC_CustomResItem): TSRSC_CustomResItem;
begin
  Result := TSRSC_CustomResItem(inherited Extract(Item));
end;//TSRSC_CustomResItemList.Extract
//==============================================================================
function TSRSC_CustomResItemList.ExtractItem(Item: TSRSC_CustomResItem; Direction: TList.TDirection): TSRSC_CustomResItem;
begin
  Result := TSRSC_CustomResItem(inherited ExtractItem(Item, Direction));
end;//TSRSC_CustomResItemList.ExtractItem
//==============================================================================
function TSRSC_CustomResItemList.First: TSRSC_CustomResItem;
begin
  Result := TSRSC_CustomResItem(inherited First);
end;//TSRSC_CustomResItemList.First
//==============================================================================
function TSRSC_CustomResItemList.Last: TSRSC_CustomResItem;
begin
  Result := TSRSC_CustomResItem(inherited Last);
end;//TSRSC_CustomResItemList.Last
//==============================================================================
function TSRSC_CustomResItemList.FindEx(var AItem; const AName: string;
  const AExclude : TSRSC_CustomResItem): boolean;
var
  i : integer;
begin

  for i := 0 to Count - 1 do
    if  (Items[i] <> AExclude)
    and (CompareText(Items[i].Name, AName) = 0)
    then  begin
      TObject(AItem) := Items[i];
      Exit(True);
    end;//if; for

  Result := False;
end;//TSRSC_CustomResItemList.FindEx
//==============================================================================
function TSRSC_CustomResItemList.Exists(const AName: string; const AExclude : TSRSC_CustomResItem): boolean;
var
  vDummy : TObject;
begin
  Result := FindEx(vDummy, AName, AExclude)
end;//TSRSC_CustomResItemList.Exists
//==============================================================================
function TSRSC_CustomResItemList.FirstIF(var AItem; const ASearchProc: TSRSC_SearchProc;
  const ASearchParam : Pointer; const AInsexPtr: PInteger): boolean;
// Найти первый элемент соответствующий условию ASearchProc
var
  i : integer;
begin
  Result := False;

  for i := 0 to Count - 1 do  begin

    ASearchProc(Items[i], ASearchParam, Result);

    if Result then  begin

      TObject(AItem) := Items[i];

      if Assigned(AInsexPtr) then
        AInsexPtr^ := i;

      Exit;
    end;//if

  end;//for

end;//TSRSC_CustomResItemList.FirstIF
//==============================================================================
function TSRSC_CustomResItemList.PrevIF(var AItem; const ASearchProc: TSRSC_SearchProc;
  const ASearchParam : Pointer; AIndex : integer; const AInsexPtr: PInteger): boolean;
// Найти предидующий элемент соответствующий условию ASearchProc
var
  i : integer;
begin

  if AIndex < 0 then  begin
    AIndex := IndexOfItem( TObject(AItem), FromEnd) - 1;

    if AIndex < 0 then
      Exit(False);
  end;//if


  Result := False;


  for i := AIndex downto 0 do  begin

    ASearchProc(Items[i], ASearchParam, Result);

    if Result then  begin

      TObject(AItem) := Items[i];

      if Assigned(AInsexPtr) then
        AInsexPtr^ := i;

      Exit;
    end;//if

  end;//for

end;//TSRSC_CustomResItemList.PrevIF
//==============================================================================
function TSRSC_CustomResItemList.NextIF(var AItem; const ASearchProc: TSRSC_SearchProc;
  const ASearchParam : Pointer; AIndex : integer; const AInsexPtr: PInteger): boolean;
// Найти следующий элемент соответствующий условию ASearchProc
var
  i : integer;
begin

  if AIndex < 0 then  begin
    AIndex := IndexOfItem( TObject(AItem), FromBeginning) + 1;

    if AIndex < 0 then
      Exit(False);
  end;//if


  Result := False;


  for i := AIndex to Count - 1 do  begin

    ASearchProc(Items[i], ASearchParam, Result);

    if Result then  begin

      TObject(AItem) := Items[i];

      if Assigned(AInsexPtr) then
        AInsexPtr^ := i;

      Exit;
    end;//if

  end;//for

end;//TSRSC_CustomResItemList.NextIF
//==============================================================================
function TSRSC_CustomResItemList.LastIF(var AItem; const ASearchProc: TSRSC_SearchProc;
  const ASearchParam : Pointer; const AInsexPtr: PInteger): boolean;
// Найти последний элемент соответствующий условию ASearchProc
var
  i : integer;
begin
  Result := False;

  for i := Count - 1 downto 0 do  begin

    ASearchProc(Items[i], ASearchParam, Result);

    if Result then  begin

      TObject(AItem) := Items[i];

      if Assigned(AInsexPtr) then
        AInsexPtr^ := i;

      Exit;
    end;//if

  end;//for

end;//TSRSC_CustomResItemList.LastIF
//==============================================================================
function TSRSC_CustomResItemList.FirstConst(var AItem; const AInsexPtr: PInteger): boolean;
begin
  Result := FirstIF(AItem, SearchConst, nil, AInsexPtr);
end;//TSRSC_CustomResItemList.FirstConst
//==============================================================================
function TSRSC_CustomResItemList.PrevConst(var AItem; const AIndex: integer;
  const AInsexPtr: PInteger): boolean;
begin
  Result := PrevIF(AItem, SearchConst, nil, AIndex, AInsexPtr);
end;//TSRSC_CustomResItemList.PrevConst
//==============================================================================
function TSRSC_CustomResItemList.NextConst(var AItem; const AIndex: integer;
  const AInsexPtr: PInteger): boolean;
begin
  Result := NextIF(AItem, SearchConst, nil, AIndex, AInsexPtr);
end;//TSRSC_CustomResItemList.NextConst
//==============================================================================
function TSRSC_CustomResItemList.LastConst(var AItem; const AInsexPtr: PInteger): boolean;
begin
  Result := LastIF(AItem, SearchConst, nil, AInsexPtr);
end;//TSRSC_CustomResItemList.LastConst
//==============================================================================
procedure TSRSC_CustomResItemList.SortByOrder;
begin
  Sort(Compare_SRSC_LGCN);
end;//TSRSC_CustomResItemList.SortByOrder
//==============================================================================
function TSRSC_CustomResItemList.AddGroup(const AName: string): TSRSC_Group;
begin

  if Exists(AName) then
    raise ESRSCError.CreateFmt('%s.AddGroup'#13#10'Элемент с именем «%s» уже существует!', [ClassName, AName]);


  Result := TSRSC_Group.Create(Self, AName);


  inherited Add(Result);
end;//TSRSC_CustomResItemList.AddGroup
//==============================================================================
function TSRSC_CustomResItemList.AddConst(const AName, AData: string): TSRSC_Const;
begin

  if Exists(AName) then
    raise ESRSCError.CreateFmt('%s.AddConst'#13#10'Элемент с именем «%s» уже существует!', [ClassName, AName]);


  Result := TSRSC_Const.Create(Self, AName);
  Result.Data := AData;


  inherited Add(Result);
end;//TSRSC_CustomResItemList.AddConst
//==============================================================================

//==============================================================================
// TSRSC_Res
//==============================================================================
// private
//==============================================================================
function TSRSC_Res.Get_Manager: TSRSC_Manager;
begin
  if  Assigned(FOwnerList) and Assigned(FOwnerList.FProjectRes) then
    Result := FOwnerList.FProjectRes.Manager
  else
    Result := nil;
end;//TSRSC_Res.Get_Manager
//==============================================================================
procedure TSRSC_Res.Set_Name(const Value: string);
// Запоминаем старое имя
begin

  if CompareStr(FName, Value) = EqualsValue then
    Exit;


  if (FOldName = '') then
    FOldName := Value;


  FName := Value;
end;//TSRSC_Res.Set_Name
//==============================================================================
procedure TSRSC_Res.Set_DeclUnit(const Value: string);
// Запоминаем старое имя модуля
begin

  if CompareStr(FDeclUnit, Value) = EqualsValue then
    Exit;


  if AnsiMatchStr(FOldDeclUnit, ['', FDeclUnit]) then
    FOldDeclUnit := FDeclUnit;


  FDeclUnit := Value;
end;//TSRSC_Res.Set_DeclUnit
//==============================================================================
//==============================================================================
// protected
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSRSC_Res.Create(const AName, ADeclUnit, AResPrefix : string);
begin
  inherited Create;

  FItems := TSRSC_CustomResItemList.Create;

  Name          := AName;
  DeclUnit      := ADeclUnit;
  FResPrefix    := AResPrefix;
  FDefLangName  := PLAIN_TEXT;
end;//TSRSC_Res.Create
//==============================================================================
destructor TSRSC_Res.Destroy;
begin
  FreeAndNil(FItems);

  inherited;
end;//TSRSC_Res.Destroy
//==============================================================================
procedure TSRSC_Res.Load(const AStorage: ISRSC_Res_Storage; const APath: string; const AAsTemp : boolean);
begin
  Items.Clear;

  AStorage.LoadRes(Self, APath, AAsTemp);

  Items.SortByOrder;
end;//TSRSC_Res.Load
//==============================================================================
procedure TSRSC_Res.Save(const AStorage: ISRSC_Res_Storage; const APath: string; const AAsTemp : boolean);
begin
  AStorage.SaveRes(Self, APath, AAsTemp);
end;//TSRSC_Res.Save
//==============================================================================
procedure TSRSC_Res.Erase(const AStorage: ISRSC_Res_Storage; const APath: string; const AAsTemp : boolean);
begin
  AStorage.EraseRes(Self, APath, AAsTemp);
end;//TSRSC_Res.Erase
//==============================================================================
procedure TSRSC_Res.EraseOld(const AStorage: ISRSC_Res_Storage; const APath: string);
var
  vName : string;
begin

  vName := FName;
  try
    FName := FOldName;

    Erase(AStorage, APath, False);
    Erase(AStorage, APath, True);

  finally
    FName := vName;
  end;//t..f

end;//TSRSC_Res.EraseOld
//==============================================================================
procedure TSRSC_Res.LoadFromFile(const APath: string);
begin
  Load( SRSCStorages['File'], APath);
end;//TSRSC_Res.LoadFromFile
//==============================================================================
procedure TSRSC_Res.SaveToFile(const APath: string);
begin
  Save( SRSCStorages['File'], APath);
end;//TSRSC_Res.SaveToFile
//==============================================================================
function TSRSC_Res.AddItem(const AItem: TSRSC_CustomResItem): integer;
// Добавить элемент с пересортировкой
begin
  Result := FItems.Add(AItem);
  FItems.SortByOrder;
end;//TSRSC_Res.AddItem
//==============================================================================
function TSRSC_Res.GenUniqueName(const ABaseName: string): string;
// Сгенерировать имя, уникальное в рамках всех вложенных списков
var
  i : integer;
begin

  i := 0;
  repeat
    Inc(i);
    Result := Format('%s%d', [ABaseName, i]);
  until not FItems.Exists(Result);

end;//TSRSC_Res.GenUniqueName
//==============================================================================
function TSRSC_Res.CanRenameItem(const AItem: TSRSC_CustomResItem; const ANewName: string): boolean;
begin
  Result := not FItems.Exists(ANewName, AItem);
end;//TSRSC_Res.CanRenameItem
//==============================================================================
function TSRSC_Res.FindInRes(var ACurrentPos : TSRSC_FindCursor): boolean;
// Поиск среди констант в именах или текстах
type
  TConstStepFunc = function(var AItem; const AIndex: integer;
    const AInsexPtr: PInteger): boolean of object;
var
  i, dI : integer;
  vConstStep : TConstStepFunc;
  vConst : TSRSC_Const;
begin

  // Текущий индекс
  i := Items.IndexOf(ACurrentPos.SRSCConst);

  if Assigned(ACurrentPos.SRSCConst) and (i = -1) then
    Exit(False);

  vConst := ACurrentPos.SRSCConst;


  // Учесть направление
  if foBack in ACurrentPos.Options then
    begin
      vConstStep := Items.PrevConst;
      dI := -1;

      if i = -1 then
        i := Items.Count - 1;
    end
  else
    begin
      vConstStep := Items.NextConst;
      dI := 1;
    end;//else if


  // Цикл по списку констант, до первого совпадения
  repeat

    // Поиск по тексту
    if  Assigned(vConst)
    and (foText in ACurrentPos.Options)
    and vConst.FindInConstText(ACurrentPos)
    then  begin
      ACurrentPos.SRSCConst := vConst;
      Exit(True);
    end;//if


    // Поиск следующей константы
    repeat

      if not vConstStep(vConst, i, @i) then
        Exit(False);

      if  (vConst <> ACurrentPos.SRSCConst)
      and (foName in ACurrentPos.Options)
      and vConst.FindInConstName(ACurrentPos)
      then  begin
        ACurrentPos.SRSCConst := vConst;
        Exit(True);
      end;//if

      i := i + dI;
    until foText in ACurrentPos.Options;

  until FALSE;

end;//TSRSC_Res.FindInRes
//==============================================================================
function TSRSC_Res.CompleteFrom(const AFromRes : TSRSC_Res) : boolean;
// Дополнить константами из ресурса
var
  i : integer;
begin
  Result := False;


  if AFromRes = Self then
    Exit;


  // Группы
  for i := 0 to AFromRes.Items.Count - 1 do
    if AFromRes.Items[i] is TSRSC_Group then
      if not Items.Exists(AFromRes.Items[i].Name) then
      begin
        Items.AddGroup(AFromRes.Items[i].Name).Assign(AFromRes.Items[i]);

        Result := True;
      end;

  // Константы
  for i := 0 to AFromRes.Items.Count - 1 do
    if AFromRes.Items[i] is TSRSC_Const then
      if not Items.Exists(AFromRes.Items[i].Name) then
      begin

        Items.AddConst(
          AFromRes.Items[i].Name,
          (AFromRes.Items[i] as TSRSC_Const).Data
        ).Assign(AFromRes.Items[i]);

        Result := True;
      end;

  Items.SortByOrder;
end;//TSRSC_Res.CompleteFrom
//==============================================================================

//==============================================================================
// TSRSC_ResList
//==============================================================================
// private
//==============================================================================
function TSRSC_ResList.GetItem(const Index: Integer): TSRSC_Res;
begin
  Result := TSRSC_Res(inherited Items[Index]);
end;//TSRSC_ResList.GetItem
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TSRSC_ResList.Notify(Ptr: Pointer; Action: TListNotification);
begin

  if OwnsObjects then
    with TSRSC_Res(Ptr) do
      case Action of
        lnAdded:
          if (FOwnerList = nil) then
            FOwnerList := Self;

        lnExtracted, lnDeleted:
          if (FOwnerList = Self) then
            FOwnerList := nil;

      end;//case

  inherited;
end;//TSRSC_ResList.Notify
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TSRSC_ResList.Extract(Item: TSRSC_Res): TSRSC_Res;
begin
  Result := TSRSC_Res(inherited Extract(Item));
end;//TSRSC_ResList.Extract
//==============================================================================
function TSRSC_ResList.ExtractItem(Item: TSRSC_Res; Direction: TList.TDirection): TSRSC_Res;
begin
  Result := TSRSC_Res(inherited ExtractItem(Item, Direction));
end;//TSRSC_ResList.ExtractItem
//==============================================================================
function TSRSC_ResList.First: TSRSC_Res;
begin
  Result := TSRSC_Res(inherited First);
end;//TSRSC_ResList.First
//==============================================================================
function TSRSC_ResList.Last: TSRSC_Res;
begin
  Result := TSRSC_Res(inherited Last);
end;//TSRSC_ResList.Last
//==============================================================================
function TSRSC_ResList.FindEx(var AItem; const AName: string;
  const AExclude: TSRSC_Res): boolean;
var
  i : integer;
begin

  for i := 0 to Count - 1 do
    if  (Items[i] <> AExclude)
    and (CompareText(Items[i].Name, AName) = 0)
    then  begin
      TObject(AItem) := Items[i];
      Exit(True);
    end;//if; for

  Result := False;
end;//TSRSC_ResList.FindEx
//==============================================================================
function TSRSC_ResList.Find(const AName: string; const AExclude: TSRSC_Res): TSRSC_Res;
begin
  if not FindEx(Result, AName, AExclude) then
    Result := nil;
end;//TSRSC_ResList.Find
//==============================================================================
function TSRSC_ResList.Exists(const AName: string;
  const AExclude: TSRSC_Res): boolean;
var
  vDummy : TObject;
begin
  Result := FindEx(vDummy, AName, AExclude)
end;//TSRSC_ResList.Exists
//==============================================================================
function TSRSC_ResList.Next(var AItem; AIndex : integer; const AIndexPtr : PInteger): boolean;
// Следеющий по списку, если есть
begin

  if AIndex < 0 then
    AIndex := IndexOf(TObject(AItem));

  Inc(AIndex);

  Result := (AIndex < Count);

  if not Result then
    Exit;

  TObject(AItem) := Items[AIndex];

  if Assigned(AIndexPtr) then
    AIndexPtr^ := AIndex;
end;//TSRSC_ResList.Next
//==============================================================================
function TSRSC_ResList.Add(const AName, ADeclUnit, AResPrefix : string): TSRSC_Res;
begin

  if Exists(AName) then
    raise ESRSCError.CreateFmt('%s.Add'#13#10'Элемент с именем «%s» уже существует!', [ClassName, AName]);

  Result := TSRSC_Res.Create(AName, ADeclUnit, AResPrefix);

  {Define params}

  inherited Add(Result);
end;//TSRSC_ResList.Add
//==============================================================================
function TSRSC_ResList.GenUniqueName(const ABaseName : string): string;
// Сгенерировать имя, уникальное в рамках списка
var
  i : integer;
begin

  i := 0;
  repeat
    Inc(i);
    Result := Format('%s%d', [ABaseName, i]);
  until not Exists(Result);

end;//TSRSC_ResList.GenUniqueName
//==============================================================================

//==============================================================================
// TSRSC_ProjectRes
//==============================================================================
// private
//==============================================================================
function TSRSC_ProjectRes.Get_Manager: TSRSC_Manager;
begin
  if Assigned(FOwnerList) then
    Result := FOwnerList.Manager
  else
    Result := nil;
end;//TSRSC_ProjectRes.Get_Manager
//==============================================================================
function TSRSC_ProjectRes.Get_ProjectFileName: string;
begin
  Result := FProvider.ProjectFileName;
end;//TSRSC_ProjectRes.Get_ProjectFileName
//==============================================================================
function TSRSC_ProjectRes.Get_ProjectPath: string;
begin
  Result := ExtractFilePath(ProjectFileName);
end;//TSRSC_ProjectRes.Get_ProjectPath
//==============================================================================
function TSRSC_ProjectRes.Get_ProjectOutputPath: string;
begin
  Result := FProvider.ProjectOutputPath;
end;//TSRSC_ProjectRes.Get_ProjectOutputPath
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TSRSC_ProjectRes.ProviderDestroying;
// Унечтожение вместе с провайдером
begin
  FProviderDestroying := True;
  Free;
end;//TSRSC_ProjectRes.ProviderDestroying
//==============================================================================
procedure TSRSC_ProjectRes.Show(const APartOfNameToFind : string);
begin

  if Assigned(FViewer) then
    SetForegroundWindow( FViewer.Parent.Handle )

  else
    begin
      FViewer := FProvider.CreateViewer( FOwnerList.Manager.ViewerClass );
      (FViewer as ISRSC_ProjectResFrame).ProjectRes := Self
    end;//else if


  (FViewer as ISRSC_ProjectResFrame).TrySelectConst(APartOfNameToFind);
end;//TSRSC_ProjectRes.Show
//==============================================================================
procedure TSRSC_ProjectRes.Hide;
begin
  FViewer := nil;
end;//TSRSC_ProjectRes.Hide
//==============================================================================
function TSRSC_ProjectRes.GetModuleSource(const AResName, ANewDeclUnitName,
  AOldDeclUnitName: string): string;
// Получить текст модуля.
// Если модуля нет - создать, если имя изменилось - переименовать.
//------------------------------------------------------------------------------
  function GetDefSource : string;
  // Исходный код модуля доступа по умолчанию
  var
    vSource : TStrings;
  begin

    vSource := TStringList.Create;
    try
      vSource.LoadFromFile(Manager.AppDataPath + 'DefDeclUnit.pas');

      vSource.Text := StringReplace(vSource.Text, '$(ResName)',   AResName,         [rfReplaceAll, rfIgnoreCase]);
      vSource.Text := StringReplace(vSource.Text, '$(UnitName)',  ANewDeclUnitName, [rfReplaceAll, rfIgnoreCase]);

      Result := vSource.Text;
    finally
      FreeAndNil(vSource);
    end;//t..f

  end;//GetDefSource
//------------------------------------------------------------------------------
begin

  if FProvider.ExistsUnit(AOldDeclUnitName) then
    FProvider.RenameUnit(AOldDeclUnitName, ANewDeclUnitName)

  else
    if not FProvider.ExistsUnit(ANewDeclUnitName) then
      FProvider.CreateUnit(ANewDeclUnitName, GetDefSource);


  Result := FProvider.ReadUnitSource(ANewDeclUnitName);
end;//TSRSC_ProjectRes.GetModuleSource
//==============================================================================
procedure TSRSC_ProjectRes.ReplaceOldValuesInSource(const AOldName, ANewName,
  AOldDeclUnit, ANewDeclUnit: string; const ASource: TStrings);
// Заменить старые значения на новые
var
  i : integer;
begin

  if ASource.Count > 0 then
    ASource[0] := StringReplace(ASource[0], AOldDeclUnit, ANewDeclUnit, [rfReplaceAll, rfIgnoreCase]);

  for i := 1 to ASource.Count - 1 do
    ASource[0] := StringReplace(ASource[0], AOldName, ANewName, [rfReplaceAll, rfIgnoreCase]);

end;//TSRSC_ProjectRes.ReplaceOldValuesInSource
//==============================================================================
procedure TSRSC_ProjectRes.FillModuleSource(const ARes: TSRSC_Res; const ASource: TStrings);
// Наполнить текст модуля функциями доступа к константам
var
  l : integer;
//------------------------------------------------------------------------------
  function FindSRSCLine(const AMarkStr : string) : integer;
  // Поиск линии по маркеру
  // True - найдена
  begin

    while l < ASource.Count - 1 do  begin

      if POS(AMarkStr, ASource[l]) > 0 then
        Exit(l);

      Inc(l);
    end;//while

    raise ESRSCParseError.CreateFmt('В модуле «%s» отсутсвует специальная строка «%s»!', [ARes.DeclUnit, AMarkStr]);
  end;//FindSRSCLine
//------------------------------------------------------------------------------
  procedure ClearSection(const ABegin, AEnd : string);
  // Убрать текст секции из исходника
  begin

    FindSRSCLine(ABegin);
    Inc(l);
    while (l < ASource.Count - 1) and (POS(AEnd, ASource[l]) = 0) do
      ASource.Delete(l);

  end;//ReadSection
//------------------------------------------------------------------------------
  procedure WriteSection(const ASection : TStrings; const ABegin, AEnd : string);
  // Прочитаь текст секции, изяв его из исходника
  var
    i : integer;
  begin

    FindSRSCLine(ABegin);
    Inc(l);
    for i := 0 to ASection.Count - 1 do  begin
      ASource.Insert(l, ASection[i]);
      Inc(l);
    end;//for

  end;//WriteSection
//------------------------------------------------------------------------------
  procedure ConstFuncs(const AList : TSRSC_CustomResItemList; const AIntf, AImpl : TStrings);
  // Функции констант
  var
    i, j, b, e : integer;
    C : TSRSC_Const;
    vHeader : string;
  begin

    for i := 0 to AList.Count - 1 do
    begin

      if AList[i] is TSRSC_Group then
        Continue;

      C := AList[i] as TSRSC_Const;

      if not C.FuncAuto then
        Continue;

      vHeader := Format('function %s(%s) : string;', [C.Name, C.FuncParams]);

      AIntf.Add( vHeader );

      if AImpl.Count > 0 then
        AImpl.Add('');

      AImpl.Add( vHeader );
      b := AImpl.Add('begin');
      AImpl.Add(
        StringReplace(
          C.FuncCode,
          '$(Const)',
          'ConstFromRes(''' + ARes.ResPrefix + C.Name + ''')',
          [rfReplaceAll, rfIgnoreCase]
        )
      );
      e := AImpl.Add('end;');

      for j := b + 1 to e - 1 do
        AImpl[j] := '  ' + AImpl[j];
    end;

  end;//ConstFuncs
//------------------------------------------------------------------------------
var
  vDll, vIntf, vImpl : TStrings;
begin

  vDll := TStringList.Create;
  try

    vIntf:= TStringList.Create;
    try

      vImpl:= TStringList.Create;
      try

         // Прочитать секции
         l := 0;
         ClearSection('//SRSC_Dll_Beg',   '//SRSC_Dll_End');
         ClearSection('//SRSC_Intf_Beg',  '//SRSC_Intf_End');
         ClearSection('//SRSC_Impl_Beg',  '//SRSC_Impl_End');


         vDll.Text := IfThen(ARes.InDll, '{$DEFINE SRSC_IN_DLL}');

         ConstFuncs(ARes.Items, vIntf, vImpl);


         // Записать секции
         l := 0;
         WriteSection( vDll,  '//SRSC_Dll_Beg',   '//SRSC_Dll_End');
         WriteSection(vIntf,  '//SRSC_Intf_Beg',  '//SRSC_Intf_End');
         WriteSection(vImpl,  '//SRSC_Impl_Beg',  '//SRSC_Impl_End');


      finally
        FreeAndNil(vImpl);
      end;//t..f

    finally
      FreeAndNil(vIntf);
    end;//t..f

  finally
    FreeAndNil(vDll);
  end;//t..f

end;//TSRSC_ProjectRes.FillModuleSource
//==============================================================================
procedure TSRSC_ProjectRes.SetModuleSource(const ADeclUnitName, ASource: string);
// Сохранить текст модуля
begin
  FProvider.WriteUnitSource(ADeclUnitName, ASource);
end;//TSRSC_ProjectRes.SetModuleSource
//==============================================================================
function TSRSC_ProjectRes.Get_Provider: ISRSC_ProjectProvider;
begin
  Result := FProvider;
end;//TSRSC_ProjectRes.Get_Provider
//==============================================================================
procedure TSRSC_ProjectRes.Set_Provider(const AProvider: ISRSC_ProjectProvider);
begin
  FProvider := AProvider;
end;//TSRSC_ProjectRes.Set_Provider
//==============================================================================
function TSRSC_ProjectRes.Get_ResSettings: TSRSC_ResSettingsRec;
begin
  Result := FResSettings;
end;//TSRSC_ProjectRes.Get_ResSettings
//==============================================================================
procedure TSRSC_ProjectRes.Set_ResSettings(const AResSettings: TSRSC_ResSettingsRec);
begin
  FResSettings := AResSettings;
end;//TSRSC_ProjectRes.Set_ResSettings
//==============================================================================
procedure TSRSC_ProjectRes.Save_ResSettings;
begin
  SRSCStorages['File'].SaveSettings(Self);
end;//TSRSC_ProjectRes.Save_ResSettings
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSRSC_ProjectRes.Create(const AProvider: ISRSC_ProjectProvider);
begin
  inherited Create;

  FProvider := AProvider;
  FProvider.ProjectRes := Self;

  FResList := TSRSC_ResList.Create;
  FResList.FProjectRes := Self;

end;//TSRSC_ProjectRes.Create
//==============================================================================
destructor TSRSC_ProjectRes.Destroy;
begin

  if Assigned(FOwnerList) then
    FOwnerList.Extract(Self);


  if Assigned(FViewer) then
    FreeAndNil(FViewer);


  if FProviderDestroying then
    begin
      FProvider.ProjectRes := nil;
      FProvider := nil;
    end
  else
    if Assigned(FProvider) then
      FProvider.ProjectResDestroying;


  FreeAndNil(FResList);

  inherited;
end;//TSRSC_ProjectRes.Destroy
//==============================================================================
procedure TSRSC_ProjectRes.LoadResList;
// Загрузка списка наборов ресурсов
var
  vPath : string;
  vNames : TStringList;
  i : integer;
begin

  SRSCStorages['File'].LoadSettings(Self);

  vPath := ProjectPath;
  ResList.Clear;

  vNames := TStringList.Create;
  try

    SRSCStorages['File'].ReadNames(vPath, vNames, False);

    for i := 0 to vNames.Count - 1 do
      try
        ResList.Add(vNames[i], '', '').Load(SRSCStorages['File'], vPath);
      except
        ResList.Remove(ResList.Last);
        raise
      end;//t..e

    SRSCStorages['File'].ReadNames(vPath, vNames, True);
    if vNames.Count = 0 then
      FCurRes := nil

    else
      begin

        if not ResList.FindEx(FCurRes, vNames[0]) then  begin
          FCurRes := ResList.Add(vNames[0], '', '');
          FCurRes.FUnCommitedAdd := True;
        end;//if

        FCurRes.FIsUnCommited := True;

        FCurRes.Load(SRSCStorages['File'], vPath, True)
      end;//else if

  finally
    FreeAndNil(vNames);
  end;//t..f

end;//TSRSC_ProjectRes.LoadResList
//==============================================================================
procedure TSRSC_ProjectRes.DeleteRes;
// Удалить набор констант
var
  vRes : TSRSC_Res;
begin
  vRes := CurRes;

  SRSCStorages['File'].EraseRes(vRes, ProjectPath, False);

  ResList.Remove(vRes);
end;//TSRSC_ProjectRes.DeleteRes
//==============================================================================
procedure TSRSC_ProjectRes.CommitRes(const AEmergencyMode: boolean);
// Принять изменения для набора констант
var
  vRes : TSRSC_Res;
  vProjectPath : string;
  vRenamed : boolean;
  vFN : TFileName;
  vDeclSource : TStrings;
begin
  vRes := CurRes;

  vProjectPath := ProjectPath;


  // РАБОТА С ИСХОДНЫМИ ДАННЫМИ
  // Сохранить исходные данные
  vRes.SaveToFile( vProjectPath );

  vRenamed := (CompareStr(vRes.Name, vRes.FOldName) <> 0);

  // Удалить старую версию при переименовании
  if vRenamed then
    vRes.EraseOld( SRSCStorages['File'],  vProjectPath);


  // РАБОТА С РЕСУРСАМИ
  // Создать файл ресурсов
  vFN := Manager.CreateResFile(vRes);

  // Создание DLL
  if vRes.InDll then
    begin
      vFN := Manager.CreateResDLL(vFN);
      MoveFileEx(PChar(vFN), PChar(ProjectOutputPath + ExtractFileName(vFN)), MOVEFILE_COPY_ALLOWED or MOVEFILE_REPLACE_EXISTING);

      // Удалить старую dll при переименовании
      if vRenamed then
        DeleteFile(ProjectOutputPath + vRes.FOldName + '.dll');
    end
  else
    begin
      MoveFileEx(PChar(vFN), PChar(vProjectPath + ExtractFileName(vFN)), MOVEFILE_COPY_ALLOWED or MOVEFILE_REPLACE_EXISTING);

      // Удалить старый res-файл при переименовании
      if vRenamed then
        DeleteFile(vProjectPath + vRes.FOldName + '.res');
    end;//else if


  // РАБОТА С МОДУЛЯМИ
  vDeclSource := TStringList.Create;
  try
    vDeclSource.Text  := GetModuleSource(vRes.Name, vRes.DeclUnit, vRes.FOldDeclUnit);

    // Заменить имя набора ресурсов при переименовании
    if vRenamed then
      ReplaceOldValuesInSource(vRes.FOldName, vRes.Name, vRes.FOldDeclUnit, vRes.DeclUnit, vDeclSource);

    FillModuleSource(vRes, vDeclSource);


    SetModuleSource(vRes.DeclUnit, vDeclSource.Text);
  finally
    FreeAndNil(vDeclSource);
  end;//t..f

//ShowMessage('4');
  vRes.FOldName      := vRes.Name;
  vRes.FOldDeclUnit  := vRes.DeclUnit;


  if not AEmergencyMode then
    vRes.Erase(SRSCStorages['File'], ProjectPath, True);

  vRes.FIsUnCommited  := False;
  vRes.FUnCommitedAdd := False;
end;//TSRSC_ProjectRes.CommitRes
//==============================================================================
procedure TSRSC_ProjectRes.RollbackRes(const AEmergencyMode: boolean);
var
  vRes : TSRSC_Res;
begin
  vRes := CurRes;


  if not AEmergencyMode then
    vRes.Erase(SRSCStorages['File'], ProjectPath, True);


  if vRes.FUnCommitedAdd then
    ResList.Remove(vRes)

  else  begin
    vRes.Name := vRes.FOldName;

    if not AEmergencyMode then
      vRes.Load(SRSCStorages['File'], ProjectPath);

      vRes.FIsUnCommited  := False;
  end;//else if

end;//TSRSC_ProjectRes.RollbackRes
//==============================================================================
procedure TSRSC_ProjectRes.SaveUncommitedData;
// Сохранение несохраненных данных
begin
  CurRes.Save(SRSCStorages['File'], ProjectPath, True);
end;//TSRSC_ProjectRes.SaveUncommitedData
//==============================================================================
procedure TSRSC_ProjectRes.EraseOldUncommitedData(const ARes : TSRSC_Res);
// Удалить уставевшие данные
begin
  ARes.Erase(SRSCStorages['File'], ProjectPath, True);
end;//TSRSC_ProjectRes.EraseOldUncommitedData
//==============================================================================
function TSRSC_ProjectRes.AddRes(const AName: string; const AUnCommitedAdd : boolean): TSRSC_Res;
// Добавление ресурса
begin

  Result := ResList.Add(
    AName,
    Format(ResSettings.DeclUnitFmt,  [AName]),
    Format(ResSettings.ResPrefixFmt, [AName])
  );

  Result.ConstUpper      := ResSettings.ConstsUpper;
  Result.FUnCommitedAdd  := AUnCommitedAdd;

end;//TSRSC_ProjectRes.AddRes
//==============================================================================
function TSRSC_ProjectRes.FindInRess(var AFindCursor: TSRSC_FindCursor): boolean;
// Поиск по списку ресурсов
var
  i : integer;
  vStartRes : TSRSC_Res;
begin

  // Набор ресурсов, с которого начался поиск,
  // если вновь дошли до него - поиск окончен
  vStartRes := AFindCursor.SRSCRes;

  i := -1;
  repeat

    if AFindCursor.SRSCRes.FindInRes(AFindCursor) then
      Exit(True);

    if foNoChangeRes in AFindCursor.Options then
      Exit(False);


    if not ResList.Next(AFindCursor.SRSCRes, i, @i) then
    begin
      AFindCursor.SRSCRes := ResList.First;
      i := 0;
    end;//if

  until AFindCursor.SRSCRes = vStartRes;

  Result := False;
end;//TSRSC_ProjectRes.FindInRess
//==============================================================================

//==============================================================================
// TSRSC_ProjectResList
//==============================================================================
// private
//==============================================================================
function TSRSC_ProjectResList.GetItem(const Index: Integer): TSRSC_ProjectRes;
begin
  Result := TSRSC_ProjectRes(inherited Items[Index]);
end;//TSRSC_ProjectResList.GetItem
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TSRSC_ProjectResList.Notify(Ptr: Pointer; Action: TListNotification);
begin

  if OwnsObjects then
    with TSRSC_ProjectRes(Ptr) do
      case Action of
        lnAdded:
          if (FOwnerList = nil) then
            FOwnerList := Self;

        lnExtracted, lnDeleted:
          if (FOwnerList = Self) then
            FOwnerList := nil;

      end;//case

  inherited;
end;//TSRSC_ProjectResList.Notify
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TSRSC_ProjectResList.Extract(Item: TSRSC_ProjectRes): TSRSC_ProjectRes;
begin
  Result := TSRSC_ProjectRes(inherited Extract(Item));
end;//TSRSC_ProjectResList.Extract
//==============================================================================
function TSRSC_ProjectResList.ExtractItem(Item: TSRSC_ProjectRes; Direction: TList.TDirection): TSRSC_ProjectRes;
begin
  Result := TSRSC_ProjectRes(inherited ExtractItem(Item, Direction));
end;//TSRSC_ProjectResList.ExtractItem
//==============================================================================
function TSRSC_ProjectResList.First: TSRSC_ProjectRes;
begin
  Result := TSRSC_ProjectRes(inherited First);
end;//TSRSC_ProjectResList.First
//==============================================================================
function TSRSC_ProjectResList.Last: TSRSC_ProjectRes;
begin
  Result := TSRSC_ProjectRes(inherited Last);
end;//TSRSC_ProjectResList.Last
//==============================================================================
function TSRSC_ProjectResList.FindEx(var AProjectRes; const AProjectFileName: string): boolean;
var
  i : integer;
begin

  for i := 0 to Count - 1 do
    if CompareText(AProjectFileName, Items[i].ProjectFileName) = 0 then  begin
      TObject(AProjectRes) := Items[i];

      Result := True;

      Exit;
    end;//if; for

  Result := False;
end;//TSRSC_ProjectResList.FindEx
//==============================================================================
function TSRSC_ProjectResList.Find(const AProjectFileName: string): TSRSC_ProjectRes;
begin
  if not FindEx(Result, AProjectFileName) then
    Result := nil;
end;//TSRSC_ProjectResList.Find
//==============================================================================
function TSRSC_ProjectResList.Add(const AProvider : ISRSC_ProjectProvider): TSRSC_ProjectRes;
begin
  Result := TSRSC_ProjectRes.Create(AProvider);

  inherited Add(Result);

  Result.ResSettings := FManager.ResSettings;
end;//TSRSC_ProjectResList.Add
//==============================================================================

//==============================================================================
// TSRSC_Manager
//==============================================================================
// private
//==============================================================================
//==============================================================================
// protected
//==============================================================================
// ISRSC_Manager
//==============================================================================
function TSRSC_Manager.FindProjectRes(const AProjectFileName: string): ISRSC_ProjectRes;
begin
  Result := FProjectResList.Find(AProjectFileName);
end;//TSRSC_Manager.FindProjectRes
//==============================================================================
function TSRSC_Manager.AddProjectRes(const AProjectProvider: ISRSC_ProjectProvider): ISRSC_ProjectRes;
var
  vPR : TSRSC_ProjectRes;
begin
  vPR := FProjectResList.Add(AProjectProvider);

  vPR.LoadResList;

  Result := vPR;
end;//TSRSC_Manager.AddProjectRes
//==============================================================================
function TSRSC_Manager.Get_ResSettings: TSRSC_ResSettingsRec;
begin
  Result := FResSettings;
end;//TSRSC_Manager.Get_ResSettings
//==============================================================================
procedure TSRSC_Manager.Set_ResSettings(const AResSettings: TSRSC_ResSettingsRec);
begin
  FResSettings := AResSettings;
end;//TSRSC_Manager.Set_ResSettings
//==============================================================================
procedure TSRSC_Manager.Save_ResSettings;
begin

  with TINIFile.Create(AppDataPath + 'Config.ini') do  try
    WriteString('Settings', 'DeclUnitFmt',   FResSettings.DeclUnitFmt);
    WriteString('Settings', 'ResPrefixFmt',  FResSettings.ResPrefixFmt);
    WriteBool(  'Settings', 'ConstsUpper',   FResSettings.ConstsUpper);
  finally
    Free;
  end;//with

end;//TSRSC_Manager.Save_ResSettings
//==============================================================================
// IIDEEXT_MainToolsDLG
//==============================================================================
function TSRSC_Manager.ShowMainToolsDLG: boolean;
begin
  Result := FMainToolsDLGFunc;
end;//TSRSC_Manager.ShowMainToolsDLG
//==============================================================================
//==============================================================================
function TSRSC_Manager.Get_ExtEditersCount: integer;
begin
  Result := FExtEditers.Count;
end;//TSRSC_Manager.Get_ExtEditersCount
//==============================================================================
function TSRSC_Manager.Get_ExtEditers(const Index: integer): TSRSC_ExtEditer;
begin

  if (Index < 0) or (Index >= FExtEditers.Count) then
    raise ESRSCError.CreateFmt('Выход индекса за границы списка (%d). Пределы [0..%d]', [Index, FExtEditers.Count - 1]);

  with Result do  begin
    FileName    := FExtEditers.Names[Index];
    Params      := FExtEditers.ValueFromIndex[Index];
    ImageIndex  := TImageIndex(FExtEditers.Objects[Index]);
  end;//with

end;//TSRSC_Manager.Get_ExtEditers
//==============================================================================
function TSRSC_Manager.Get_ExtEditersImages: TImageList;
begin
  Result := FExtEditersImages;
end;//TSRSC_Manager.Get_ExtEditersImages
//==============================================================================
function TSRSC_Manager.ExtEditers_Add(const AFileName, AParams: string;
  const AImageIndex : TImageIndex): TSRSC_ExtEditer;
// Добавить внешний редактор
begin
  Result.FileName    := AFileName;
  Result.Params      := AParams;

  if AImageIndex < 0 then
    Result.ImageIndex := ImageList_AddIcon(FExtEditersImages.Handle, GetAssociatedIcon(AFileName, False))
  else
    Result.ImageIndex := AImageIndex;

  FExtEditers.AddObject(AFileName + FExtEditers.NameValueSeparator + AParams, TObject(Result.ImageIndex));
end;//TSRSC_Manager.ExtEditers_Add
//==============================================================================
procedure TSRSC_Manager.ExtEditers_Delete(const AIndex: integer);
// Удалить внешний редактор
begin
  FExtEditersImages.Delete( Integer(FExtEditers.Objects[AIndex]) );
  FExtEditers.Delete(AIndex);
end;//TSRSC_Manager.ExtEditers_Delete
//==============================================================================
procedure TSRSC_Manager.ExtEditers_Clear;
// Очистить список внешних редакторов
begin
  FExtEditers.Clear;
  FExtEditersImages.Clear;
end;//TSRSC_Manager.ExtEditers_Clear
//==============================================================================
procedure TSRSC_Manager.ExtEditers_Save;
// Сохранить список внешних редакторов
begin
  FExtEditers.SaveToFile(AppDataPath + 'ExtEditers.txt');
end;//TSRSC_Manager.ExtEditers_Save
//==============================================================================
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSRSC_Manager.Create;
begin
  inherited Create;

  FProjectResList := TSRSC_ProjectResList.Create;
  FProjectResList.FManager := Self;

  FExtEditersImages := TImageList.Create(nil);
  FExtEditersImages.ColorDepth := cd32Bit;
  FExtEditers := TStringList.Create;
  FExtEditers.NameValueSeparator := '|';

  ForceDirectories(AppDataTempPath);

  LoadConfig;
end;//TSRSC_Manager.Create
//==============================================================================
destructor TSRSC_Manager.Destroy;
begin
  DeleteDir(AppDataTempPath);

  FreeAndNil(FProjectResList);

  FreeAndNil(FExtEditers);
  FreeAndNil(FExtEditersImages);

  inherited;
end;//TSRSC_Manager.Destroy
//==============================================================================
function TSRSC_Manager.AppDataPath: string;
begin
  Result := GetSpecialFolderPath(CSIDL_COMMON_APPDATA) + '\SAN\ResStrConst\';
end;//TSRSC_Manager.AppDataPath
//==============================================================================
function TSRSC_Manager.AppDataTempPath: string;
begin
  Result := AppDataPath + 'Temp\';
end;//TSRSC_Manager.AppDataTempPath
//==============================================================================
function TSRSC_Manager.ConstTempFileName(const AConstName: string): string;
begin
  Result := AppDataTempPath + AConstName + '.txt';
end;//TSRSC_Manager.ConstTempFileName
//==============================================================================
procedure TSRSC_Manager.LoadConfig;
// Загрузка конфигурации
var
  i : integer;
  vFileName : string;
begin

  vFileName := AppDataPath + 'Config.ini';
  with TINIFile.Create(vFileName) do  try
    FResSettings.DeclUnitFmt   := ReadString('Settings', 'DeclUnitFmt',   '');
    FResSettings.ResPrefixFmt  := ReadString('Settings', 'ResPrefixFmt',  '');
    FResSettings.ConstsUpper   := ReadBool(  'Settings', 'ConstsUpper',   True);
  finally
    Free;
  end;//with


  ExtEditers_Clear;
  vFileName := AppDataPath + 'ExtEditers.txt';
  if FileExists(vFileName) then  begin
    FExtEditers.LoadFromFile(vFileName);
    for i := 0 to FExtEditers.Count - 1 do
      FExtEditers.Objects[i] := TObject( ImageList_AddIcon(FExtEditersImages.Handle, GetAssociatedIcon(FExtEditers.Names[i], False)) );
  end;//if

end;//TSRSC_Manager.LoadConfig
//==============================================================================
function TSRSC_Manager.CreateResFile(const ARes: TSRSC_Res): string;
// Создать res-файл, и вернуть его полное имя во временной папке.
//------------------------------------------------------------------------------
  procedure PrepareListToRC(const AList : TSRSC_CustomResItemList; const ARC : System.Text);
  // Для обного списка
  var
    i : integer;
    vCnstFN : TFileName;
    vCnst : System.Text;
  begin

    for i := 0 to AList.Count - 1 do
      if AList[i] is TSRSC_Group then
        PrepareListToRC( (AList[i] as TSRSC_Group).SubItems, ARC)

      else  begin

        vCnstFN := AppDataTempPath + AList[i].Name + '.txt';
        AssignFile(vCnst, vCnstFN);
        Rewrite(vCnst);
        try

          Write(vCnst, (AList[i] as TSRSC_Const).Data);

        finally
          CloseFile(vCnst);
        end;//t..f

        Writeln(ARC, Format('%s RCDATA "%s"', [ARes.ResPrefix + AList[i].Name, vCnstFN]));
      end;//else if

  end;//PrepareListToRC
//------------------------------------------------------------------------------
var
  vRCFN : TFileName;
  vRC : System.Text;

  vStartInfo    : TStartupInfo;
  vProcInfo     : TProcessInformation;
begin

  // Создать RC
  vRCFN := AppDataTempPath + ARes.Name + '.RC';
  AssignFile(vRC, vRCFN);
  Rewrite(vRC);
  try

    PrepareListToRC(ARes.Items, vRC);

  finally
    CloseFile(vRC);
  end;//t..f


  // Скомпилировать RES
  FillChar(vStartInfo, SizeOf(TStartupInfo),        #0);
  FillChar(vProcInfo,  SizeOf(TProcessInformation), #0);
  with vStartInfo do  begin
    cb := SizeOf(TStartUpInfo);
    dwFlags := STARTF_USESHOWWINDOW or STARTF_FORCEONFEEDBACK;
    wShowWindow := SW_SHOWNORMAL;
  end;//with

  CreateProcess(
    nil, PChar('brcc32.exe "' + vRCFN + '"'), nil, nil, False,
    CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS,
    nil, nil, vStartInfo, vProcInfo
  );
  WaitForSingleObject(vProcInfo.hProcess, INFINITE);


  Result := AppDataTempPath + ARes.Name + '.RES';
end;//TSRSC_Manager.CreateResFile
//==============================================================================
function TSRSC_Manager.CreateResDLL(const AResFileName: TFileName): string;
// Создать Dll, включающую файл ресурсов AResFileName
var
  vDir : string;
  vStartInfo    : TStartupInfo;
  vProcInfo     : TProcessInformation;
begin

  // Скопировать в папку проекта
  MoveFileEx(PChar(AResFileName), PChar(AppDataPath + 'ResDllProj\Data.res'), MOVEFILE_COPY_ALLOWED or MOVEFILE_REPLACE_EXISTING);

  // Скомпилировать Dll
  FillChar(vStartInfo, SizeOf(TStartupInfo),        #0);
  FillChar(vProcInfo,  SizeOf(TProcessInformation), #0);
  with vStartInfo do  begin
    cb := SizeOf(TStartUpInfo);
    dwFlags := STARTF_USESHOWWINDOW or STARTF_FORCEONFEEDBACK;
    wShowWindow := SW_SHOWNORMAL;
  end;//with

  vDir := GetCurrentDir;
  ChDir(AppDataPath + 'ResDllProj');
  try

    CreateProcess(
      nil, PChar(AppDataPath + 'ResDllProj\Compile.cmd'), nil, nil, False,
      CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS,
      nil, nil, vStartInfo, vProcInfo
    );
    WaitForSingleObject(vProcInfo.hProcess, INFINITE);

  finally
    ChDir(vDir);
  end;//t..f

  Result := AppDataPath + 'ResDllProj\ResDllProj.dll';
end;//TSRSC_Manager.CreateResDLL
//==============================================================================

//==============================================================================
// TSRSC_Storages
//==============================================================================
// private
//==============================================================================
function TSRSC_Storages.Get_Count: integer;
begin
  Result := FStorages.Count;
end;//TSRSC_Storages.Get_Count
//==============================================================================
function TSRSC_Storages.Get_Items(const Index: integer): ISRSC_Res_Storage;
begin
  Result := (FStorages[Index] as ISRSC_Res_Storage);
end;//TSRSC_Storages.Get_Items
//==============================================================================
function TSRSC_Storages.Get_ByName(const AStorageName: string): ISRSC_Res_Storage;
begin
  Result := Find(AStorageName);

  if not Assigned(Result) then
    raise ESRSCError.CreateFmt('Не найдено хранилище «%s».', [AStorageName]);

end;//TSRSC_Storages.Get_ByName
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSRSC_Storages.Create;
begin
  inherited Create;

  FStorages := TInterfaceList.Create;
end;//TSRSC_Storages.Create
//==============================================================================
destructor TSRSC_Storages.Destroy;
begin
  FreeAndNil(FStorages);

  inherited;
end;//TSRSC_Storages.Destroy
//==============================================================================
procedure TSRSC_Storages.Reg(const AStorage: ISRSC_Res_Storage);
begin
  FStorages.Add(AStorage);
end;//TSRSC_Storages.Reg
//==============================================================================
function TSRSC_Storages.Find(const AStorageName: string; const AIndexPtr : PInteger): ISRSC_Res_Storage;
// Поиск хранилища по имени
var
  i : integer;
begin

  for i := 0 to FStorages.Count - 1 do
    if CompareText(AStorageName, (FStorages[i] as ISRSC_Res_Storage).StorageName) = 0 then  begin
      Result := (FStorages[i] as ISRSC_Res_Storage);

      if Assigned(AIndexPtr) then
        AIndexPtr^ := i;

      Exit;
    end;//if; for

  Result := nil;
end;//TSRSC_Storages.Find
//==============================================================================
function TSRSC_Storages.UnReg(const AStorageName : string) : boolean;
var
  i : integer;
begin
  Result := Assigned( Find(AStorageName, @i) );

  if Result then
    FStorages.Delete(i);
end;//TSRSC_Storages.UnReg
//==============================================================================


//==============================================================================
VAR
  vSRSCStorages : TSRSC_Storages;
  vSRSCManager : TSRSC_Manager;
//==============================================================================
function SRSCStorages : TSRSC_Storages;
begin
  Result := vSRSCStorages;
end;//SRSCStorages
//==============================================================================
function SRSCManager : TSRSC_Manager;
begin
  Result := vSRSCManager;
end;//SRSCManager
//==============================================================================




INITIALIZATION//////////////////////////////////////////////////////////////////
  vSRSCStorages := TSRSC_Storages.Create;

  vSRSCManager := TSRSC_Manager.Create;

FINALIZATION////////////////////////////////////////////////////////////////////
  FreeAndNil(vSRSCManager);

  FreeAndNil(vSRSCStorages);

END.
