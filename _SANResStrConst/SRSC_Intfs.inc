//==============================================================================
  ISRSC_ProjectProvider = interface;
//==============================================================================
  // Интерфейс ресурсов проекта
  ISRSC_ProjectRes = interface(IUnknown)
    ['{D82B7FE1-0345-47B4-9D45-4545D6E33960}']

    procedure ProviderDestroying;
    procedure Show(const APartOfNameToFind : string = '');
    procedure Hide;

    procedure DeleteRes;
    procedure CommitRes(const AEmergencyMode : boolean = False);
    procedure RollbackRes(const AEmergencyMode : boolean = False);

    function Get_Provider : ISRSC_ProjectProvider;
    procedure Set_Provider(const AProvider : ISRSC_ProjectProvider);
    property Provider : ISRSC_ProjectProvider  read Get_Provider  write Set_Provider;
  end;//ISRSC_ProjectRes
//==============================================================================
  // Интерфейс доступа к проекту
  ISRSC_ProjectProvider = interface(IUnknown)
    ['{613E2AC0-26D9-4526-9D13-E22EAB507129}']

    procedure ProjectResDestroying;

    function ProjectFileName : string;
    function ProjectOutputPath : string;

    function CreateViewer(const AFrameClass : TCustomFrameClass) : TCustomFrame;

    function ExistsUnit(const AUnitName : string) : boolean;
    procedure RenameUnit(const AOldUnitName, ANewUnitName : string);
    procedure CreateUnit(const AUnitName, ASource : string);
    function ReadUnitSource(const AUnitName : string) : string;
    procedure WriteUnitSource(const AUnitName, ASource : string);

    function InsertIntoSource(const AText : string) : boolean;

    function Get_ProjectRes : ISRSC_ProjectRes;
    procedure Set_ProjectRes(const AProjectRes : ISRSC_ProjectRes);
    property ProjectRes : ISRSC_ProjectRes  read Get_ProjectRes  write Set_ProjectRes;
  end;//ISRSC_ProjectProvider
//==============================================================================
  ISRSC_Manager = interface(IUnknown)
    ['{9CF42EEB-ACE2-4961-80C8-A7CF134A351E}']

    function FindProjectRes(const AProjectFileName : string) : ISRSC_ProjectRes;
    function AddProjectRes(const AProjectProvider : ISRSC_ProjectProvider) : ISRSC_ProjectRes;

  end;//ISRSC_Manager
//==============================================================================
