unit frmMain_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, StrUtils,

  // От порядка упоминания зависит порядок в выпадающем списке
  SynHighlighterSQL,
  SynHighlighterPas,
  SynHighlighterHTML,
  SynHighlighterCSS,
  SynHighlighterPHP,
  SynHighlighterJScript,
  SynHighlighterXML,
  SynHighlighterInno
  ;

type
//==============================================================================
  // Интерфейс взаимодействия с библиотекой экспертов
  IIDEEXT_MainToolsDLG = interface(IUnknown)
    ['{3C669B59-19D6-420B-9DC6-3A9AC7A2AA0E}']

    function ShowMainToolsDLG : boolean;
  end;//IIDEEXT_MainToolsDLG
//==============================================================================
  {$I ..\SRSC_Intfs.inc}
//==============================================================================
  TfrmMain = class(TForm)
    pnlTop: TPanel;
    btnStart: TButton;
    btnStop: TButton;
    btnTools: TButton;
    btnShutdown: TButton;
    btnProjClose: TButton;
    btnTest: TButton;
    lblSelText: TLabel;
    edtSelText: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnToolsClick(Sender: TObject);
    procedure btnShutdownClick(Sender: TObject);
    procedure btnProjCloseClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);

  private
    FBPLHandle : THandle;
    FBPLCtrl : IUnknown;

    FPP : TObject;

  protected
    function BPLName : string;

    // Выгрузка BPL
    procedure UnLoadBPL;

    // Финализация BPL
    procedure FinalizeBPL;

  public
    { Public declarations }
  end;//TfrmMain

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

function CutMsgStr(const AMsgStr : string; const AReplaceStr : string = ' ... ';
  const AIndent : integer = 16; const AMaxLength : integer = 64) : string;
// Заменить в строке сообщения столько сиволов на AReplaceStr,
// чтобы ее длина стала меньше либо равна AMaxLength.
// Замена начинается с AIndent.
var
  u : integer;
begin
  Result := AMsgStr;

  u := Length(AMsgStr) - AMaxLength;
  if u <= 0 then
    Exit;

  Delete(Result, AIndent, u + Length(AReplaceStr) - 2);

  Insert(AReplaceStr, Result, AIndent);
end;//CutMsgStr
//==============================================================================


type
  TProjectProvider = class(TInterfacedPersistent, ISRSC_ProjectProvider)
  private
    FProjectRes: ISRSC_ProjectRes;

  protected
    // ISRSC_ProjectProvider
    procedure ProjectResDestroying;

    function ProjectFileName : string;
    function ProjectOutputPath : string;

    function CreateViewer(const AFrameClass : TCustomFrameClass) : TCustomFrame;

    function ExistsUnit(const AUnitName : string) : boolean;
    procedure RenameUnit(const AOldUnitName, ANewUnitName : string);
    procedure CreateUnit(const AUnitName, ASource : string);
    function ReadUnitSource(const AUnitName : string) : string;
    procedure WriteUnitSource(const AUnitName, ASource : string);

    function InsertIntoSource(const AText : string) : boolean;

    function Get_ProjectRes : ISRSC_ProjectRes;
    procedure Set_ProjectRes(const AProjectRes : ISRSC_ProjectRes);
    property ProjectRes : ISRSC_ProjectRes  read Get_ProjectRes  write Set_ProjectRes;

  public
    constructor Create;
    destructor Destroy; override;


  end;//TProjectProvider

procedure TProjectProvider.ProjectResDestroying;
begin
  ProjectRes.Provider := nil;
  ProjectRes := nil;
  Free;
end;

function TProjectProvider.ProjectFileName: string;
begin
  Result := Application.ExeName;
end;

function TProjectProvider.ProjectOutputPath: string;
begin
  Result := ExtractFilePath( Application.ExeName );
end;

function TProjectProvider.CreateViewer(const AFrameClass: TCustomFrameClass): TCustomFrame;
begin

  Result := AFrameClass.Create(frmMain);
  Result.Parent := frmMain;
  Result.Align := alClient;

end;//TProjectProvider.CreateViewer

function TProjectProvider.ExistsUnit(const AUnitName: string): boolean;
begin
  Result := False;
end;

procedure TProjectProvider.RenameUnit(const AOldUnitName, ANewUnitName: string);
begin

end;

procedure TProjectProvider.CreateUnit(const AUnitName, ASource: string);
begin

end;

function TProjectProvider.ReadUnitSource(const AUnitName: string): string;
begin

  with TStringList.Create do  try
  
    LoadFromFile('C:\ProgramData\SAN\ResStrConst\DefDeclUnit.pas');

    Result := Text;
    
  finally
    Free;
  end;//t..f
  
end;

procedure TProjectProvider.WriteUnitSource(const AUnitName, ASource: string);
begin

end;

function TProjectProvider.InsertIntoSource(const AText: string): boolean;
begin
  ShowMessage(AText);

  Result := True;
end;

function TProjectProvider.Get_ProjectRes: ISRSC_ProjectRes;
begin
  Result := FProjectRes;
end;

procedure TProjectProvider.Set_ProjectRes(const AProjectRes: ISRSC_ProjectRes);
begin
  FProjectRes := AProjectRes;
end;

constructor TProjectProvider.Create;
begin
  inherited Create;

end;

destructor TProjectProvider.Destroy;
begin

  if Assigned(ProjectRes) then
    ProjectRes.ProviderDestroying;

  inherited;
end;

// TfrmMain
// protected
function TfrmMain.BPLName: string;
begin
  Result := 'SANResStrConstExp.bpl';
end;


procedure TfrmMain.UnLoadBPL;
// Выгрузка BPL
begin
  UnloadPackage(FBPLHandle);
  FBPLHandle := INVALID_HANDLE_VALUE;
end;

procedure TfrmMain.FinalizeBPL;
// Финализация BPL
type
  TFinalProc = procedure;
var
  vFinalProc : TFinalProc;
begin

  FBPLCtrl := nil;

  @vFinalProc := GetProcAddress(FBPLHandle, 'FinalBPLExpert');
  if Assigned(@vFinalProc) then
    vFinalProc;

end;

// published

procedure TfrmMain.FormCreate(Sender: TObject);
//var
//  vFrame : TframeSRSC_ProjectRes;
begin
  FBPLHandle := INVALID_HANDLE_VALUE;

//  vFrame := TframeSRSC_ProjectRes.Create(Self);
//  vFrame.Parent := Self;
//  vFrame.Align := alClient;

end;//TfrmMain.FormCreate

procedure TfrmMain.FormDestroy(Sender: TObject);
begin

  if FBPLHandle <> INVALID_HANDLE_VALUE then
    btnStop.Click;

end;//

procedure TfrmMain.btnStartClick(Sender: TObject);
type
  TInitProc = procedure(out ABPLIntf : IUnknown);
var
  vInitProc : TInitProc;
begin

  if FBPLHandle <> INVALID_HANDLE_VALUE then
    raise Exception.Create('Эксперт уже доступен!');


  // Загрузка BPL
  FBPLHandle := LoadPackage(BPLName);
  if FBPLHandle = INVALID_HANDLE_VALUE then
    raise Exception.Create('Не удалось загрузить пакет «' + BPLName + '»!'#13#10 + SysErrorMessage(GetLastError));

  try

    // Инициализация BPL
    @vInitProc := GetProcAddress(FBPLHandle, 'InitBPLExpert');
    if not Assigned(@vInitProc) then
      raise Exception.Create('Не найдена процедура «InitBPLExpert»!');

    vInitProc(FBPLCtrl);
    try

      FPP := TProjectProvider.Create;

      (FBPLCtrl as ISRSC_Manager).AddProjectRes( TProjectProvider(FPP) ).Show(edtSelText.Text);

      btnTools.Enabled      := True;
      btnShutdown.Enabled   := True;
      btnProjClose.Enabled  := True;

    except
      FinalizeBPL;
      raise;
    end;//t..f

  except
    UnLoadBPL;
    raise;
  end;//t..e

end;//

procedure TfrmMain.btnStopClick(Sender: TObject);
begin

  try
    FPP := nil;

    btnTools.Enabled      := False;
    btnShutdown.Enabled   := False;
    btnProjClose.Enabled  := False;

    FinalizeBPL;

  finally
    UnLoadBPL;
  end;//t..f

end;//

procedure TfrmMain.btnToolsClick(Sender: TObject);
begin
  (FBPLCtrl as IIDEEXT_MainToolsDLG).ShowMainToolsDLG
end;//

procedure TfrmMain.btnShutdownClick(Sender: TObject);
begin
  OnCloseQuery := nil;
  btnStop.Click;
end;

procedure TfrmMain.btnProjCloseClick(Sender: TObject);
begin
  FreeAndNil(FPP);
end;

procedure TfrmMain.btnTestClick(Sender: TObject);
begin
  MessageBox(Handle, PChar(CutMsgStr('D:\DELPHI\ActiveProjects\SANResStrConst\BPL\SANResStrConstExp.dproj')), '', MB_ICONSTOP);
end;


END.
