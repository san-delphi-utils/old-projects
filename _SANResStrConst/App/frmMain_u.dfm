object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'frmMain'
  ClientHeight = 435
  ClientWidth = 743
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 743
    Height = 41
    Align = alTop
    Caption = 'pnlTop'
    ShowCaption = False
    TabOrder = 0
    DesignSize = (
      743
      41)
    object lblSelText: TLabel
      Left = 448
      Top = 14
      Width = 36
      Height = 13
      Caption = 'SelText'
    end
    object btnStart: TButton
      Left = 15
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = btnStartClick
    end
    object btnStop: TButton
      Left = 96
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 1
      OnClick = btnStopClick
    end
    object btnTools: TButton
      Left = 177
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Tools'
      Enabled = False
      TabOrder = 2
      OnClick = btnToolsClick
    end
    object btnShutdown: TButton
      Left = 656
      Top = 9
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Shutdown'
      TabOrder = 3
      OnClick = btnShutdownClick
    end
    object btnProjClose: TButton
      Left = 258
      Top = 9
      Width = 75
      Height = 25
      Caption = 'ProjClose'
      TabOrder = 4
      OnClick = btnProjCloseClick
    end
    object btnTest: TButton
      Left = 344
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Test'
      TabOrder = 5
      OnClick = btnTestClick
    end
    object edtSelText: TEdit
      Left = 490
      Top = 11
      Width = 121
      Height = 21
      TabOrder = 6
    end
  end
end
