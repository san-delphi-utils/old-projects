program SANResStrConst;

uses
  Forms,
  frmMain_u in 'frmMain_u.pas' {frmMain};

{$R *.res}

begin
  System.ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.HintHidePause := 60000;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
