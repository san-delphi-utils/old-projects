program SANRelPathsCfg;

uses
  Forms,
  UformSANRelPathsCfgMain in 'UformSANRelPathsCfgMain.pas' {formSANRelPathsCfgMain};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TformSANRelPathsCfgMain, formSANRelPathsCfgMain);
  Application.Run;
end.
