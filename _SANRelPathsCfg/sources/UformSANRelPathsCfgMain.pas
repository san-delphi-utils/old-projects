unit UformSANRelPathsCfgMain;

interface

uses
  Windows, Messages, SysUtils, StrUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ImgList, Spin, VirtualTrees, Clipbrd;

type
  TNodeData = class(TObject)
  private
    FPath: string;

  public
    property Path : string  read FPath  write FPath;

  end;

  TformSANRelPathsCfgMain = class(TForm)
    btnedtBasePath: TButtonedEdit;
    ilMain: TImageList;
    gbBasePath: TGroupBox;
    gbDepth: TGroupBox;
    seDepth: TSpinEdit;
    gbPathsTree: TGroupBox;
    gbResults: TGroupBox;
    vstPaths: TVirtualStringTree;
    btnedtResults: TButtonedEdit;
    procedure FormCreate(Sender: TObject);
    procedure vstPathsFreeNode(Sender: TBaseVirtualTree; ANode: PVirtualNode);
    procedure vstPathsGetText(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex; ATextType: TVSTTextType; var ACellText: string);
    procedure btnedtBasePathChange(Sender: TObject);
    procedure btnedtBasePathRightButtonClick(Sender: TObject);
    procedure seDepthChange(Sender: TObject);
    procedure vstPathsFocusChanged(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex);
    procedure vstPathsNodeClick(Sender: TBaseVirtualTree;
      const AHitInfo: THitInfo);
    procedure btnedtResultsRightButtonClick(Sender: TObject);

  private
    FResults : TStrings;

    function GetNodeData(const ANode: PVirtualNode): TNodeData;
    function GetBasePath: string;

  protected
    procedure ReFillTree(const ADepth : integer);
    procedure ResultsChange(Sender: TObject);

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    property NodeData[const ANode: PVirtualNode] : TNodeData  read GetNodeData;
    property BasePath : string  read GetBasePath;

  end;

var
  formSANRelPathsCfgMain: TformSANRelPathsCfgMain;

implementation

uses
  PathUtils,
  ShlObj;

{$R *.dfm}


function BrowseCallbackProc(hwnd: HWND; uMsg: UINT; lParam: LPARAM; lpData: LPARAM): Integer; stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) then
    SendMessage(hwnd, BFFM_SETSELECTION, 1, lpData);

  BrowseCallbackProc:= 0;
end;

function FolderDialog(const AHandle: Integer; const ACaption: string; var AFolder: string): Boolean;
const
  BIF_STATUSTEXT           = $0004;
  BIF_NEWDIALOGSTYLE       = $0040;
  BIF_RETURNONLYFSDIRS     = $0080;
  BIF_SHAREABLE            = $0100;
  BIF_USENEWUI             = BIF_EDITBOX or BIF_NEWDIALOGSTYLE;

var
  BrowseInfo: TBrowseInfo;
  ItemIDList: PItemIDList;
  JtemIDList: PItemIDList;
  Path, DisplayName : string;
begin
  FillChar(BrowseInfo, SizeOf(TBrowseInfo), 0);

  SetLength(Path, MAX_PATH);
  SetLength(DisplayName, MAX_PATH);

  SHGetSpecialFolderLocation(AHandle, CSIDL_DRIVES, JtemIDList);

  with BrowseInfo do
  begin
    hwndOwner:= GetActiveWindow;
    pidlRoot:= JtemIDList;
    SHGetSpecialFolderLocation(hwndOwner, CSIDL_DRIVES, JtemIDList);

    // Возврат названия выбранного элемента
    pszDisplayName := PChar(DisplayName);

    // Установка названия диалога выбора папки
    lpszTitle:= PChar(ACaption);

    // Флаги, контролирующие возврат
    lpfn:= @BrowseCallbackProc;

    // Дополнительная информация, которая отдаётся обратно в обратный вызов (callback)
    lParam:= LongInt(PChar(AFolder));
  end;

  ItemIDList:= SHBrowseForFolder(BrowseInfo);

  Result := (ItemIDList <> nil) and SHGetPathFromIDList(ItemIDList, PChar(Path));

  if Result then
    AFolder := Path;
end;

function FullPath(const APath : string) : string;
const
  BUF_SIZE = 255;
var
  Buffer : array[0..BUF_SIZE - 1] of Char;
  PCr : PChar;
  l : Cardinal;
begin
  l := GetFullPathName(PChar(APath), BUF_SIZE, Buffer, PCr);
  Result := Buffer;
  SetLength(Result, l);
end;

function MaxPathLevel(AAbsPath : string) : integer;
begin
  AAbsPath := FullPath(AAbsPath);

  if AAbsPath[Length(AAbsPath)] = '\' then
    SetLength(AAbsPath, Length(AAbsPath) - 1);

  Result := 0;
  while not SameText(ExtractFileDir(AAbsPath), ExtractFileDrive(AAbsPath)) do
  begin
    Inc(Result);

    AAbsPath := ExtractFileDir(AAbsPath);

    SetLength(AAbsPath, Length(AAbsPath) - 1);
  end;

end;

function UpPathToDepth(const APath : string; const ADepth : integer) : string;
begin
  if ADepth = 0 then
    Result := APath

  else
  if APath[Length(APath)] = '\' then
    Result := UpPathToDepth(ExtractFileDir(APath), ADepth - 1)

  else
    Result := UpPathToDepth(ExtractFilePath(APath), ADepth - 1)

end;

{$REGION 'TformSANRelPathsCfgMain'}
function TformSANRelPathsCfgMain.GetBasePath: string;
begin
  Result := FullPath(btnedtBasePath.Text);
end;

function TformSANRelPathsCfgMain.GetNodeData(const ANode: PVirtualNode): TNodeData;
begin
  Result := TNodeData(vstPaths.GetNodeData(ANode)^);
end;

procedure TformSANRelPathsCfgMain.ReFillTree(const ADepth : integer);
var
  TopPath, BasePath : string;
//------------------------------------------------------------------------------
  procedure RecursionFillTree(const AParentNode : PVirtualNode; const APath : string;
    const ALevel : integer);
  var
    SR : TSearchRec;
    NodeData : TNodeData;
    NodeDataPath : string;
    Node : PVirtualNode;
  begin
 TODO
//    if ALevel < 0 then
//      NodeDataPath := DupeString('..\', -ALevel)
//
//    else
    if SameText(BasePath, APath) then
      NodeDataPath := '.'

    else
    if POS(AnsiUpperCase(BasePath) + '\', AnsiUpperCase(APath)) = 0 then
    begin
      NodeDataPath := APath;
      Delete(NodeDataPath, 1, Length(TopPath));
      NodeDataPath := DupeString('..\', ADepth - 1) + '..' + NodeDataPath;
    end

    else
    begin
      NodeDataPath := APath;
      Delete(NodeDataPath, 1, Length(BasePath) + 1);
    end;

    NodeData := TNodeData.Create;
    try

      NodeData.Path := NodeDataPath;

      Node := vstPaths.AddChild(AParentNode, NodeData);

      if NodeDataPath <> '.' then
      vstPaths.CheckType[Node] := ctCheckBox;

    except
      FreeAndNil(NodeData);
      raise;
    end;


    if FindFirst(APath + '\*.*', faDirectory, SR) = 0 then
    try

      repeat

        if (SR.Attr AND faDirectory <> faDirectory)
        or MatchText(SR.Name, ['.', '..'])
        then
          Continue;

        RecursionFillTree(Node, APath + '\' + SR.Name, ALevel + 1);
      until FindNext(SR) > 0;

    finally
      FindClose(SR);
    end;

  end;
//------------------------------------------------------------------------------
begin
  BasePath := Self.BasePath;
  if BasePath[Length(BasePath)] = '\' then
    SetLength(BasePath, Length(BasePath) - 1);

  TopPath := UpPathToDepth(BasePath, ADepth);
  if TopPath[Length(TopPath)] = '\' then
    SetLength(TopPath, Length(TopPath) - 1);

  vstPaths.BeginUpdate;
  try
    vstPaths.Clear;

    RecursionFillTree(vstPaths.RootNode, TopPath, -ADepth);

    vstPaths.FullExpand;
  finally
    vstPaths.EndUpdate;
  end;
end;

procedure TformSANRelPathsCfgMain.ResultsChange(Sender: TObject);
var
  Ne : TNotifyEvent;
begin
  with Sender as TStringList do
  begin
    Ne := OnChange;

    OnChange := nil;
    try
      Sort;

      Self.btnedtResults.Text := StringReplace(Trim(Text), LineBreak, ';', [rfReplaceAll, rfIgnoreCase]);
    finally
      OnChange := Ne;
    end;
  end;
end;

procedure TformSANRelPathsCfgMain.btnedtBasePathChange(Sender: TObject);
var
  BE : TButtonedEdit;
  Enb : boolean;
begin
  BE := (Sender as TButtonedEdit);

  Enb := DirectoryExists(BE.Text);

  seDepth.Enabled   := Enb;
  vstPaths.Enabled  := Enb;
   seDepth.Value    := 0;

  if Enb then
  begin
    BE.Color := clWindow;

    seDepth.MaxValue := MaxPathLevel(BE.Text);

    ReFillTree(0);
  end

  else
  begin
    BE.Color := clRed;
    vstPaths.Clear;
  end;

end;

procedure TformSANRelPathsCfgMain.btnedtBasePathRightButtonClick(Sender: TObject);
var
  S : string;
begin
  S := btnedtBasePath.Text;

  if not FolderDialog(Handle, 'Основной путь', S) then
    Exit;

  btnedtBasePath.Text := S;
end;

procedure TformSANRelPathsCfgMain.btnedtResultsRightButtonClick(
  Sender: TObject);
begin
  Clipboard.AsText := btnedtResults.Text;
end;

constructor TformSANRelPathsCfgMain.Create(AOwner: TComponent);
begin
  FResults := TStringList.Create;
  TStringList(FResults).OnChange := ResultsChange;

  inherited;
end;

destructor TformSANRelPathsCfgMain.Destroy;
begin
  FreeAndNil(FResults);
  inherited;
end;

procedure TformSANRelPathsCfgMain.FormCreate(Sender: TObject);
begin
  Application.Title := Caption;
  Application.HintHidePause := 30000;

  seDepth.Align := alClient;

  vstPaths.NodeDataSize := SizeOf(TObject);

  btnedtBasePath.Text := ExtractFilePath(Application.ExeName);
end;

procedure TformSANRelPathsCfgMain.seDepthChange(Sender: TObject);
var
  Depth : integer;
begin
  if TryStrToInt((Sender as TCustomEdit).Text, Depth) then
    ReFillTree(Depth);
end;

procedure TformSANRelPathsCfgMain.vstPathsFocusChanged(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AColumn: TColumnIndex);
begin
  btnedtResults.Text := NodeData[ANode].Path;
end;

procedure TformSANRelPathsCfgMain.vstPathsFreeNode(Sender: TBaseVirtualTree;
  ANode: PVirtualNode);
begin
  NodeData[ANode].Free;
end;

procedure TformSANRelPathsCfgMain.vstPathsGetText(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AColumn: TColumnIndex; ATextType: TVSTTextType;
  var ACellText: string);
begin
//  ACellText := ExtractFileName(NodeData[ANode].Path);
//
//  if ACellText = '' then
    ACellText := NodeData[ANode].Path
end;
procedure TformSANRelPathsCfgMain.vstPathsNodeClick(Sender: TBaseVirtualTree;
  const AHitInfo: THitInfo);
begin

  if not (hiOnItemCheckbox in AHitInfo.HitPositions) then
    Exit;

  if Sender.CheckState[AHitInfo.HitNode] = csCheckedNormal then
    FResults.Add(NodeData[AHitInfo.HitNode].Path)
  else
    FResults.Delete(FResults.IndexOf(NodeData[AHitInfo.HitNode].Path));
end;
{$ENDREGION}

end.
