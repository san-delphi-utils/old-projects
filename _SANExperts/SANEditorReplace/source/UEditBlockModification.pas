{*******************************************************}
{                                                       }
{       SANEditorReplace                                }
{                                                       }
{       Работа с текстом редактора                      }
{                                                       }
{       Использован материал:                           }
{   http://www.tempest-sw.com/opentools/NewSort.html    }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit UEditBlockModification;

interface

uses
  ToolsAPI, SysUtils, Classes, StrUtils;

type
  TModificationProc = procedure(const AStrings : TStrings;
    const AFirstLineOffset : integer; const AParam : NativeInt) of object;

procedure EditBlockModification(const AEditor: IOTASourceEditor;
  const AMdfProc: TModificationProc; const AParam : NativeInt = 0);

implementation

resourcestring
  sCloseViews = 'Close all views but one';

//==============================================================================
{$REGION 'TPosList'}
type
  // Keep track of buffer positions for each line of text.
  // An edit writer's positions are relative to the original file.
  // When writing the sorted text, it is important to keep track
  // of the original position for each line.

  TPosList = class(TList)
  public type
    PPosRec = ^TPosRec;
    TPosRec = record
      Left, Right: LongInt;
    end;


  private
    function GetItems(const Index: integer): PPosRec;

  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;

  public
    function AddPos(ALeft, ARight: LongInt) : PPosRec;

  public
    property Items[const Index : integer] : PPosRec  read GetItems; default;
  end;

function TPosList.GetItems(const Index: integer): PPosRec;
begin
  Result := PPosRec(inherited Items[Index]);
end;

procedure TPosList.Notify(Ptr: Pointer; Action: TListNotification);
begin
  if Action = lnDeleted then
    Dispose(PPosRec(Ptr));
end;

function TPosList.AddPos(ALeft, ARight: LongInt) : PPosRec;
begin
  New(Result);

  Result^.Left   := ALeft;
  Result^.Right  := ARight;

  inherited Add(Result);
end;
{$ENDREGION}
//==============================================================================

/// <summary>
///   Изменить выделенный текст редактора методом AMdfProc
/// </summary>
procedure EditBlockModification(const AEditor: IOTASourceEditor;
  const AMdfProc: TModificationProc; const AParam : NativeInt);
var
  vView: IOTAEditView;
//------------------------------------------------------------------------------
  procedure SelectedColumns;
  var
    TopLeft: TOTAEditPos;
    BottomRight: TOTAEditPos;
    Left, Right: TOTAEditPos;
    LeftChar, RightChar: TOTACharPos;
    LeftPos, RightPos: LongInt;
    Reader: IOTAEditReader;
    Writer: IOTAEditWriter;
    Text: AnsiString;
    Strings: TStringList;
    PosList: TPosList;
    Line: LongInt;
    I: Integer;
    StringsTextBefore : string;
  begin
    // Get the corners of the selected region.
    TopLeft     := TOTAEditPos(AEditor.BlockStart);
    BottomRight := TOTAEditPos(AEditor.BlockAfter);

    Strings := TStringList.Create;
    try
      PosList := TPosList.Create;
      try

        // Get the selected text.
        Reader := AEditor.CreateReader;
        Left.Col := TopLeft.Col;
        Right.Col := BottomRight.Col;

        for Line := TopLeft.Line to BottomRight.Line do  begin

          // Convert the edit positions on the current line to
          // buffer positions. Delphi requires the intermediate
          // step of character positions.
          Left.Line := Line;
          Right.Line := Line;
          vView.ConvertPos(True, Left,  LeftChar);
          vView.ConvertPos(True, Right, RightChar);

          // include the character at RightChar
          Inc(RightChar.CharIndex);
          LeftPos  := vView.CharPosToPos(LeftChar);
          RightPos := vView.CharPosToPos(RightChar);
          SetLength(Text, RightPos - LeftPos);
          Reader.GetText(LeftPos, PAnsiChar(Text), Length(Text));

          // If the text includes the end of the line characters,
          // strip the CR and LF.
          while (Length(Text) > 0) and (CharInSet(Text[Length(Text)], [#13, #10])) do  begin
            Delete(Text, Length(Text), 1);
            Dec(RightPos);
          end;//while

          Strings.Add(String(Text));
          PosList.AddPos(LeftPos, RightPos);
        end;//for

        Reader := nil;


        StringsTextBefore := Strings.Text;


        // Модифицировать текст
        AMdfProc(Strings, 0, AParam);


        // Нет изменений
        if (StringsTextBefore = Strings.Text) then
          Exit;


        // And write the sorted text, line by line.
        Writer := AEditor.CreateUndoableWriter;
        I := 0;
        for Line := TopLeft.Line to BottomRight.Line do  begin

          with PosList[I]^ do  begin
            Writer.CopyTo(Left);
            Writer.DeleteTo(Right);
          end;//with

          Writer.Insert(PAnsiChar(AnsiString(Strings[I])));
          Inc(I);
        end;//for

        Writer := nil;

      finally
        PosList.Free;
      end;//t..f

    finally
      Strings.Free;
    end;//t..f

    // Set the cursor to the start of the sorted text.
    vView.CursorPos := TopLeft;

    // Make sure the top of the sorted text is visible.
    // Scroll the edit window if ncessary.
    if (TopLeft.Line < vView.TopPos.Line) or
       (BottomRight.Line >= vView.TopPos.Line + vView.ViewSize.CY)
    then
      vView.TopPos := TopLeft;

    // Select the newly inserted, sorted text.
    AEditor.BlockVisible  := False;
    AEditor.BlockType     := btColumn;
    AEditor.BlockStart    := TOTACharPos(TopLeft);
    AEditor.BlockAfter    := TOTACharPos(BottomRight);
    AEditor.BlockVisible  := True;
  end;
//------------------------------------------------------------------------------
  procedure SelectedLines;
  var
    BlockStart: TOTACharPos;
    BlockAfter: TOTACharPos;
    StartPos, EndPos: LongInt;
    Reader: IOTAEditReader;
    Writer: IOTAEditWriter;
    TopPos, CursorPos: TOTAEditPos;
    BeforeText, AfterText: AnsiString;
    Strings: TStringList;
    vEnd1310 : boolean;
  begin
    // Get the limits of the selected text.
    BlockStart := AEditor.BlockStart;
    BlockAfter := AEditor.BlockAfter;

    // Convert the character positions to buffer positions.
    StartPos := vView.CharPosToPos(BlockStart);
    EndPos   := vView.CharPosToPos(BlockAfter);

    // Get the selected text.
    Reader := AEditor.CreateReader;
    SetLength(BeforeText, EndPos - StartPos);
    Reader.GetText(StartPos, PAnsiChar(BeforeText), Length(BeforeText));
    Reader := nil;


    vEnd1310 := (RightStr(BeforeText, 2) = #13#10);


    Strings := TStringList.Create;
    try
      Strings.Text := String(BeforeText);

      // Модифицировать текст
      AMdfProc(Strings, BlockStart.CharIndex, AParam);

      AfterText := AnsiString(Strings.Text);

      if not vEnd1310 then
        SetLength(AfterText, Length(AfterText) - 2);

    finally
      Strings.Free;
    end;//t..f


    // Если нет изменений
    if BeforeText = AfterText then
      Exit;


    // Replace the selection with the sorted text.
    Writer := AEditor.CreateUndoableWriter;
    Writer.CopyTo(StartPos);
    Writer.DeleteTo(EndPos);
    Writer.Insert(PAnsiChar(AfterText));
    Writer := nil;

    // Set the cursor to the start of the sorted text.
    vView.ConvertPos(False, CursorPos, BlockStart);
    vView.CursorPos := CursorPos;

    // Make sure the top of the sorted text is visible.
    // Scroll the edit window if ncessary.
    if (BlockStart.Line < vView.TopPos.Line)
    or (BlockAfter.Line >= vView.TopPos.Line + vView.ViewSize.CY)
    then  begin
      vView.ConvertPos(False, TopPos, BlockStart);
      vView.TopPos := TopPos;
    end;//if

    // Select the newly inserted, sorted text.
    AEditor.BlockVisible  := False;
    AEditor.BlockType     := btNonInclusive;
    AEditor.BlockStart    := BlockStart;
    AEditor.BlockAfter    := BlockAfter;
    AEditor.BlockVisible  := True;
  end;
//------------------------------------------------------------------------------
begin
  // If the file is not a source file, Editor is nil.
  if (AEditor = nil) or (@AMdfProc = nil) then
    Exit;

  // The expert cannot tell which view is active, so force
  // the user to have only one view at a time.
  if AEditor.EditViewCount > 1 then
    raise Exception.CreateRes(@sCloseViews);

  vView := AEditor.EditViews[0];

  // Columnar selections work entirely differently.
  if AEditor.BlockType = btColumn then
    SelectedColumns
  else
    SelectedLines;

  // Bring the focus back to the source editor window.
  AEditor.Show;
end;
//==============================================================================

END.
