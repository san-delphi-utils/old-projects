{*******************************************************}
{                                                       }
{       SANEditorReplace                                }
{                                                       }
{       Главный модуль эксперта замены текста           }
{       в окне редактора кода.                          }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit USANEditorReplace;

interface
uses
  ToolsAPI,
  Windows, SysUtils, Classes, Graphics, Menus, Actnlist, ImgList, Forms,
  TypInfo, RTTI, Math, StrUtils;

type
  AER = class(TCustomAttribute)
  private
    FCaption : string;
    FImageIndex : integer;
  public
    constructor Create(const ACaption : string; const AImageIndex : integer = -1);
  end;

  {$M+}
  TSANCustomEditorReplace = class(TNotifierObject, IOTAWizard)
  private
    FMenuItem : TMenuItem;
    FRttiContext : TRttiContext;
    function GetCurrentModule: IOTAModule;

  protected
    function GetIDString: string;
    function GetName: string;
    function GetState: TWizardState;
    procedure Execute;

    function LoadResBMPToGlbImageList(const AResName : string; const AMaskColor : TColor = clWhite) : TImageIndex;
    function GetMaxFirstStacesInStrings(const AStrings : TStrings) : integer;

    procedure InitActions(const ANTAServices : INTAServices);
    procedure ActExecute(Sender : TObject);
    procedure ActUpdate(Sender : TObject);

  public
    constructor Create;
    destructor Destroy; override;

    property CurrentModule: IOTAModule  read GetCurrentModule;
  end;
  {$M-}

  TSANEditorReplace = class(TSANCustomEditorReplace, IOTAWizard)
  published
    [AER('AbCdEf -> AB_CD_EF')]
    procedure ERC_InfixToUPPER(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('AB_CD_EF -> AbCdEf')]
    procedure ERC_UPPERToInfix(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('Comment -> Summary')]
    procedure ERC_CommentToSummary(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('Comment -> Remarks')]
    procedure ERC_CommentToRemarks(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('Comment -> Region')]
    procedure ERC_CommentToRegion(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

  end;

procedure Register;

implementation

uses
  UEditBlockModification;

{$R *.res}

procedure Register;
begin
  RegisterPackageWizard(TSANEditorReplace.Create);
end;

{$I ..\..\shared\SANMenu.inc}

type
  TCustomActionListHack = class(TCustomActionList);

const
  MI_SAN_EDITOR_REPLACE_NAME     = '_19122D813EC84F3EBFA359F5F293E6B6';
  MI_SAN_EDITOR_REPLACE_CAPTION  = 'Editor Replace';
  ERC_PREFIX                     = 'ERC_';

//==============================================================================
function GetEditBuffer(const AModule: IOTAModule; out ABuffer: IOTAEditBuffer): boolean;
var
  i : integer;
begin

  if Assigned(AModule) then
    for i := 0 to AModule.ModuleFileCount - 1 do
     if Supports(AModule.ModuleFileEditors[i], IOTAEditBuffer, ABuffer) then
       Exit(True);

  ABuffer := nil;
  Result := False;
end;
//==============================================================================

//==============================================================================
{$REGION 'AER'}
constructor AER.Create(const ACaption: string; const AImageIndex: integer);
begin
  inherited Create;

  FCaption := ACaption;
  FImageIndex := AImageIndex;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANCustomEditorReplace'}
constructor TSANCustomEditorReplace.Create;
var
  NTAServices  : INTAServices;
  miSAN        : TMenuItem;
begin
  inherited;

  if not
    (
     Supports(BorlandIDEServices, IOTAModuleServices)
       and
     Supports(BorlandIDEServices, INTAServices,          NTAServices)
    )
  then
    Abort;

  miSAN := SANMenuCreateOrFind(NTAServices.MainMenu.Items);

  FMenuItem := TMenuItem.Create(miSAN);
  FMenuItem.Name        := MI_SAN_EDITOR_REPLACE_NAME;
  FMenuItem.Caption     := MI_SAN_EDITOR_REPLACE_CAPTION;
  FMenuItem.ImageIndex  := LoadResBMPToGlbImageList('SER_BMP', clFuchsia);
  //FMenuItem.OnClick     := miExec;

  miSAN.Insert(miSAN.Count - 1, FMenuItem);

  FRttiContext := TRttiContext.Create;

  InitActions(NTAServices);
end;

destructor TSANCustomEditorReplace.Destroy;
begin
  FreeAndNil(FMenuItem);

  FRttiContext.Free;

  SANMenuRelease((BorlandIDEServices as INTAServices).MainMenu.Items);

  inherited;
end;

procedure TSANCustomEditorReplace.Execute;
begin
end;

function TSANCustomEditorReplace.GetCurrentModule: IOTAModule;
begin
  Result := (BorlandIDEServices as IOTAModuleServices).CurrentModule;
end;

function TSANCustomEditorReplace.GetIDString: string;
begin
  Result := '{1F528EFE-25B1-422A-B005-519CB6E9020B}'
end;

function TSANCustomEditorReplace.GetMaxFirstStacesInStrings(
  const AStrings: TStrings): integer;
  var
    i, j, vCount : integer;
  begin
    Result := 0;

    for i := 0 to AStrings.Count - 1 do
    begin
      vCount := 0;

      for j := 1 to Length(AStrings[i]) do
        if AStrings[i][j] = ' ' then
          Inc(vCount)
        else
          Break;

      Result := MAX(Result, vCount);
    end;
end;

function TSANCustomEditorReplace.GetName: string;
begin
  Result := MI_SAN_EDITOR_REPLACE_CAPTION;
end;

function TSANCustomEditorReplace.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

procedure TSANCustomEditorReplace.InitActions(const ANTAServices : INTAServices);
var
  Methods  : TArray<TRttiMethod>;
  Method   : TRttiMethod;
  AERAttr  : AER;
  Name     : string;
  act      : TAction;
  mi       : TMenuItem;
begin
  Methods := (FRttiContext.GetType(ClassType) as TRttiInstanceType).GetDeclaredMethods;

  for Method in Methods do
    if  (Method.Visibility = mvPublished)
    and (Pos(ERC_PREFIX, Method.Name) = 1)
    and (Length(Method.GetAttributes) > 0)
    and (Method.GetAttributes[0] is AER)
    then
    begin
      Name := Method.Name;
      Delete(Name, 1, Length(ERC_PREFIX));

      AERAttr := Method.GetAttributes[0] as AER;

      act := TAction.Create(FMenuItem);
      act.Name        := Format('act%s%s', [MI_SAN_EDITOR_REPLACE_NAME, Name]);
      act.Caption     := AERAttr.FCaption;
      act.OnExecute   := ActExecute;
      act.OnUpdate    := ActUpdate;
      act.ImageIndex  := AERAttr.FImageIndex;
      TCustomActionListHack(ANTAServices.ActionList).AddAction(act);

      mi := TMenuItem.Create(FMenuItem);
      mi.Name    := Format('mi%s%s', [MI_SAN_EDITOR_REPLACE_NAME, Name]);
      mi.Action  := act;
      FMenuItem.Add(mi);
    end;

end;

function TSANCustomEditorReplace.LoadResBMPToGlbImageList(const AResName: string;
  const AMaskColor: TColor): TImageIndex;
var
  NTAServices : INTAServices;
  BMP : TBitmap;
begin

  if not Supports(BorlandIDEServices, INTAServices, NTAServices) then  begin
    Result := -1;
    Exit;
  end;//if

  BMP := TBitmap.Create;
  try
    BMP.LoadFromResourceName(HInstance, AResName);
    Result := NTAServices.ImageList.AddMasked(BMP, AMaskColor);
  finally
    FreeAndNil(BMP);
  end;//t..f

end;

procedure TSANCustomEditorReplace.ActExecute(Sender: TObject);
var
  MethodName : string;
  Method : TModificationProc;
  Buffer : IOTAEditBuffer;
begin

  if not GetEditBuffer(CurrentModule, Buffer) then
    Exit;

  MethodName := (Sender as TComponent).Name;
  Delete(MethodName, 1, Length('act' + MI_SAN_EDITOR_REPLACE_NAME));
  MethodName := ERC_PREFIX + MethodName;

  with TMethod(Method) do
  begin
    Code := MethodAddress(MethodName);
    Data := Self;
  end;

  EditBlockModification(Buffer, Method);
end;

procedure TSANCustomEditorReplace.ActUpdate(Sender: TObject);
var
  vBuffer: IOTAEditBuffer;
begin
  (Sender as TAction).Enabled := GetEditBuffer(CurrentModule, vBuffer)
                             and Assigned(vBuffer.EditBlock)
                             and (vBuffer.EditBlock.Text <> '');
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANEditorReplace'}
procedure TSANEditorReplace.ERC_InfixToUPPER(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  vResult : string;
  vChar : Char;
  i, j : integer;
begin

  for j := 0 to AStrings.Count - 1 do
  begin
    vResult := '';

    for i := 1 to Length(AStrings[j]) do
    begin
      vChar := AStrings[j][i];

      if  (i > 1) and (AStrings[j][i - 1] <> ' ')
      and CharInSet(vChar, ['a'..'z', 'A'..'Z', 'а'..'я', 'А'..'Я'])
      and (vChar = AnsiUpperCase(vChar))
      then
        vResult := vResult + '_';

      vResult := vResult + vChar;
    end;

    AStrings[j] := AnsiUpperCase(vResult);
  end;

end;

procedure TSANEditorReplace.ERC_UPPERToInfix(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  vResult : string;
  i, j : integer;
begin

  for j := 0 to AStrings.Count - 1 do
  begin
    vResult := AStrings[j];

    for i := 1 to Length(vResult) do
    begin

      if (i > 1) and (not CharInSet(vResult[i - 1], [' ', '_'])) then
        vResult[i] := AnsiLowerCase(vResult[i])[1];
    end;

    AStrings[j] := StringReplace(vResult, '_', '', [rfReplaceAll]);
  end;

end;

procedure TSANEditorReplace.ERC_CommentToSummary(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  SpcMaxCount : integer;
begin
  SpcMaxCount := GetMaxFirstStacesInStrings(AStrings);

  AStrings.Text := StringReplace(AStrings.Text, '//', '///  ', [rfReplaceAll, rfIgnoreCase]);

  AStrings.Insert(0, DupeString(' ', SpcMaxCount) + '/// <summary>');
  AStrings.Add(      DupeString(' ', SpcMaxCount) + '/// </summary>');
end;

procedure TSANEditorReplace.ERC_CommentToRemarks(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  SpcMaxCount : integer;
begin
  SpcMaxCount := GetMaxFirstStacesInStrings(AStrings);

  AStrings.Text := StringReplace(AStrings.Text, '//', '///  ', [rfReplaceAll, rfIgnoreCase]);

  AStrings.Insert(0, DupeString(' ', SpcMaxCount) + '/// <remarks>');
  AStrings.Add(      DupeString(' ', SpcMaxCount) + '/// </remarks>');
end;

procedure TSANEditorReplace.ERC_CommentToRegion(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
//------------------------------------------------------------------------------
  function CalcSkipEdEmptyFirst : integer;
  var
    i : integer;
  begin
    Result := 0;

    for i := 0 to AStrings.Count - 1 do
      if Trim(AStrings[i]) = EmptyStr then
        Inc(Result)
      else
        Break;
  end;
//------------------------------------------------------------------------------
  function CalcSkipEdEmptyLast : integer;
  var
    i : integer;
  begin
    Result := 0;

    for i := AStrings.Count - 1 downto 0 do
      if Trim(AStrings[i]) = EmptyStr then
        Inc(Result)
      else
        Break;
  end;
//------------------------------------------------------------------------------
  function IsDoubleSlashComment(const AFirstStr : string; out APos : integer;
    out APreparedStr : string) : boolean;
  begin
    APos := POS('//', AFirstStr);

    Result := (APos > 0) and (Trim(Copy(AFirstStr, 1, APos - 1)) = EmptyStr);

    if not Result then
      Exit;

    APreparedStr := TrimRight(AFirstStr);
    Delete(APreparedStr, 1, APos + 2);
  end;
//------------------------------------------------------------------------------
  function IsBraceComment(const AFirstStr : string; out APos : integer;
    out APreparedStr : string) : boolean;
  var
    SubS : string;
  begin
    SubS := Trim(AFirstStr);

    Result := (Length(SubS) > 1) and (SubS[1] = '{') and (SubS[Length(SubS)] = '}');

    if not Result then
      Exit;

    APos := POS('{', AFirstStr);

    APreparedStr := TrimRight(AFirstStr);
    Delete(APreparedStr, 1, APos + 1);
    SetLength(APreparedStr, Length(APreparedStr) - 1);
    APreparedStr := TrimRight(APreparedStr);
  end;
//------------------------------------------------------------------------------
var
  SpcMaxCount, SkipEdEmptyFirst, SkipEdEmptyLast, PosFirstComment : integer;
  S : string;
begin
  SkipEdEmptyFirst := CalcSkipEdEmptyFirst;
  SkipEdEmptyLast  := CalcSkipEdEmptyLast;

  if SkipEdEmptyFirst + SkipEdEmptyLast >= AStrings.Count then
    Exit;

  if IsDoubleSlashComment(AStrings[SkipEdEmptyFirst], PosFirstComment, S)
  or IsBraceComment(AStrings[SkipEdEmptyFirst], PosFirstComment, S)
  then
  begin
    AStrings[SkipEdEmptyFirst] := DupeString(' ', PosFirstComment - 1) +
                                  '{$REGION' +
                                  IfThen(S <> '', ' ''' + S + '''') +
                                  '}';
    AStrings.Insert(AStrings.Count - SkipEdEmptyLast, DupeString(' ', PosFirstComment - 1) + '{$ENDREGION}');
  end

  else
  begin
    SpcMaxCount := GetMaxFirstStacesInStrings(AStrings);

    AStrings.Insert(SkipEdEmptyFirst, DupeString(' ', SpcMaxCount) + '{$REGION}');
    AStrings.Insert(AStrings.Count - SkipEdEmptyLast, DupeString(' ', SpcMaxCount) + '{$ENDREGION}');
  end;
end;
{$ENDREGION}
//==============================================================================

end.
