const
  MI_SAN_NAME        = '_376939024FCD426294F08CBD0C9D4AF3';
  MI_SAN_CAPTION     = 'SAN';
  MI_SAN_AB_NAME     = '_839E56766C6F4E51BAD80B4EA305FAA4';
  MI_SAN_AB_CAPTION  = '&About';

procedure miAboutClick(Self, Sender : TObject);
begin
  Application.MessageBox('These are extentions of the environment from SAN.'#13#10#13#10#169'SAN     September 2011 — May 2013', 'Information', MB_ICONINFORMATION)
end;

function SANMenuCreateOrFind(const ARootMenuItem : TMenuItem) : TMenuItem;
var
  miAbout : TMenuItem;
begin
  Result := ARootMenuItem.FindComponent(MI_SAN_NAME) as TMenuItem;

  if Assigned(Result) then
    Exit;

  Result := TMenuItem.Create(ARootMenuItem);
  try
    Result.Name     := MI_SAN_NAME;
    Result.Caption  := MI_SAN_CAPTION;

    miAbout := TMenuItem.Create(Result);
    try
      miAbout.Name     := MI_SAN_AB_NAME;
      miAbout.Caption  := MI_SAN_AB_CAPTION;
      @miAbout.OnClick := @miAboutClick;
      Result.Add(miAbout);
    except
      FreeAndNil(miAbout);
      raise;
    end;

    ARootMenuItem.Add(Result);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure SANMenuRelease(const ARootMenuItem : TMenuItem);
var
  miSan : TMenuItem;
begin
  miSan := ARootMenuItem.FindComponent(MI_SAN_NAME) as TMenuItem;

  if not Assigned(miSan) then
    Exit;

  if (miSan.Count = 0) or (miSan.Count = 1) and SameText(miSan.Items[0].Caption, MI_SAN_AB_CAPTION) then
    FreeAndNil(miSan);
end;