{*******************************************************}
{                                                       }
{       SANDebugSvc                                     }
{                                                       }
{       Главный модуль эксперта.                        }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit USANDebugSvc;

interface
uses
  ToolsAPI,
  Windows, SysUtils, Classes, Graphics, Menus, Actnlist, ImgList, Forms;

type
  TSANDebugSvc = class(TNotifierObject, IOTAWizard)
  private
    FMenuItem : TMenuItem;

  protected
    function GetIDString: string;
    function GetName: string;
    function GetState: TWizardState;
    procedure Execute;

    function LoadResBMPToGlbImageList(const AResName : string; const AMaskColor : TColor = clWhite) : TImageIndex;

    procedure miExec(Sender : TObject);

  public
    constructor Create;
    destructor Destroy; override;

  end;

procedure Register;

implementation

uses
  UframeSANDebugSvc;

{$R *.res}

const
  MI_SAN_DEBUG_SVC_NAME     = '_74FBCF9C3591460FBD3C04E848BE87F4';

procedure Register;
begin
  RegisterPackageWizard(TSANDebugSvc.Create);
end;

{$I ..\..\shared\SANMenu.inc}

{$REGION 'TSANDebugSvc'}
constructor TSANDebugSvc.Create;
var
  NTAServices          : INTAServices;
  OTAModuleServices    : IOTAModuleServices;
  OTADebuggerServices  : IOTADebuggerServices;

  miSAN : TMenuItem;
begin
  inherited;

  if not
    (
          Supports(BorlandIDEServices, INTAServices,          NTAServices)
      and Supports(BorlandIDEServices, IOTAModuleServices,    OTAModuleServices)
      and Supports(BorlandIDEServices, IOTADebuggerServices,  OTADebuggerServices)
    )
  then
    Abort;

  miSAN := SANMenuCreateOrFind(NTAServices.MainMenu.Items);

  FMenuItem := TMenuItem.Create(miSAN);
  FMenuItem.Name        := MI_SAN_DEBUG_SVC_NAME;
  FMenuItem.Caption     := MI_SAN_DEBUG_SVC_CAPTION;
  FMenuItem.ImageIndex  := LoadResBMPToGlbImageList('SDS_SVC', clFuchsia);
  FMenuItem.OnClick     := miExec;

  miSAN.Insert(miSAN.Count - 1, FMenuItem);
end;

destructor TSANDebugSvc.Destroy;
begin
  FreeAndNil(FMenuItem);

  SANMenuRelease((BorlandIDEServices as INTAServices).MainMenu.Items);

  inherited;
end;

procedure TSANDebugSvc.Execute;
begin
end;

function TSANDebugSvc.GetIDString: string;
begin
  Result := '{DB1CC871-BB60-4BF9-87BA-06D608E45742}'
end;

function TSANDebugSvc.GetName: string;
begin
  Result := MI_SAN_DEBUG_SVC_CAPTION;
end;

function TSANDebugSvc.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

function TSANDebugSvc.LoadResBMPToGlbImageList(const AResName: string;
  const AMaskColor: TColor): TImageIndex;
var
  NTAServices : INTAServices;
  BMP : TBitmap;
begin

  if not Supports(BorlandIDEServices, INTAServices, NTAServices) then  begin
    Result := -1;
    Exit;
  end;//if

  BMP := TBitmap.Create;
  try
    BMP.LoadFromResourceName(HInstance, AResName);
    Result := NTAServices.ImageList.AddMasked(BMP, AMaskColor);
  finally
    FreeAndNil(BMP);
  end;//t..f

end;

procedure TSANDebugSvc.miExec(Sender: TObject);
var
  DebugSvcDockableForm : TSANDebugSvcDockableForm;
begin

  if GetActiveProject = nil then
  begin
    Application.MessageBox('Need active project for use expert', PChar(GetName), MB_ICONWARNING);
    Exit;
  end;


  DebugSvcDockableForm  := TSANDebugSvcDockableForm.Create;
  DebugSvcDockableForm.CreateDockableForm;
end;
{$ENDREGION}

end.
