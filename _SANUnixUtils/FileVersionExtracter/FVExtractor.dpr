program FVExtractor;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  JCLFileUtils;

var
  vFileVersion : TJclFileVersionInfo;
  F : Text;
begin
  try

    if ParamCount < 1 then
    begin
      Writeln('Read file name parameter expected.');
      Exit;
    end;

    if ParamCount < 2 then
    begin
      Writeln('Save file name parameter expected.');
      Exit;
    end;

    vFileVersion := TJclFileVersionInfo.Create( ParamStr(1) );
    try

      AssignFile(F, ParamStr(2) );
      ReWrite(F);
      try
        Write(F, #39, vFileVersion.FileVersion, #39);
      finally
        CloseFile(F);
      end;

    finally
      FreeAndNil(vFileVersion)
    end;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
