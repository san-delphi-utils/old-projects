unit frmConvertationSettingsDLG_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ComObj, ActiveX,
  PasToIDlTypes_u, VirtualTrees;

type
  TfrmConvertationSettingsDLG = class(TForm)
    pnlBottom: TPanel;
    pnlClient: TPanel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    lblTypeLibGUID: TLabel;
    lblTypeLibName: TLabel;
    gbMain: TGroupBox;
    gbIntfTrfList: TGroupBox;
    medtTypeLibGUID: TMaskEdit;
    sbtnTypeLibGUID: TSpeedButton;
    edtTypeLibName: TEdit;
    vstIntfTrf: TVirtualStringTree;
    procedure FormCreate(Sender: TObject);
    procedure sbtnTypeLibGUIDClick(Sender: TObject);
    procedure medtTypeLibGUIDKeyPress(Sender: TObject; var Key: Char);

  private
    FCnvSettings : TConvertationSettings;

  protected
    procedure LoadSettings;
    procedure SaveSettings;

  public

  end;

function ConvertationSettingsDLG(const ACaption : TCaption;
  const ACnvSettings : TConvertationSettings) : boolean;

implementation

{$R *.dfm}

function ConvertationSettingsDLG(const ACaption : TCaption;
  const ACnvSettings : TConvertationSettings) : boolean;
begin

  with TfrmConvertationSettingsDLG.Create(nil) do
  try
    Caption := ACaption;
    FCnvSettings := ACnvSettings;
    LoadSettings;

    Result := ShowModal = mrOk;

    if Result then
      SaveSettings;

  finally
    Free;
  end;

end;

//==============================================================================
// TfrmConvertationSettingsDLG
// protected
procedure TfrmConvertationSettingsDLG.LoadSettings;
begin
  medtTypeLibGUID.Text  := GUIDToString(FCnvSettings.TypeLibGUID);
  edtTypeLibName.Text   := FCnvSettings.TypeLibName;

  vstIntfTrf.RootNodeCount := FCnvSettings.IntfTrfList.Count;
end;

procedure TfrmConvertationSettingsDLG.SaveSettings;
begin
//  FCnvSettings.TypeLibGUID := StringToGUID(medtTypeLibGUID.Text);
//  FCnvSettings.TypeLibName := edtTypeLibName.Text;

end;

procedure TfrmConvertationSettingsDLG.sbtnTypeLibGUIDClick(Sender: TObject);
var
  vGuid : TGUID;
begin
  OleCheck(CreateGUID(vGuid));

  medtTypeLibGUID.Text := GUIDToString(vGuid);
end;

// published
procedure TfrmConvertationSettingsDLG.FormCreate(Sender: TObject);
begin
//  edtGUID.Button.Glyph.LoadFromFile('D:\DELPHI\Ресурсы\DB_Pics\R.bmp');
end;

procedure TfrmConvertationSettingsDLG.medtTypeLibGUIDKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', 'A'..'F', 'a'..'f', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)]) then
    Key := #0;
end;
//==============================================================================

END.
