UNIT Parsing_u;

INTERFACE

USES
  SysUtils, Classes, StrUtils, TypInfo,
  dpp_PascalParser,
  JCLStrings,
  SANMisc, SANListUtils,
  PasToIDlTypes_u;

TYPE
  EParsingError = class(Exception);

procedure Parsing(const AFileName : TFileName; const AIntfs : TIntfs; out ACnvUnitName : string);

IMPLEMENTATION

resourcestring
  sUnexpectedEndOfFileErrorMessage     = 'Unexpected end of file';
  sUnexpectedTokenKindErrorMessage     = 'Unexpected token kind %s at line %d at index %d';
  sUnexpectedTokenValueErrorMessage    = 'Unexpected token value "%s" (expect "%s") at line %d at index %d';
  sIncorrectInterfaceGUIDErrorMessage  = 'Incorrect interface GUID';
  sFuncProcPropExpectedErrorMessage    = '"Function", "Procedure" or "Property" expected, but not token kind %s and value "%s" at line %d at index %d';
(*
procedure Parsing(const AFileName : TFileName; const AIntfs : TIntfs; out ACnvUnitName : string);
//------------------------------------------------------------------------------
  function TokenIs(const AToken : PTokenInfo; const AKind: TTokenKind; const AValue : string = '') : boolean;
  begin
    Result := Assigned(AToken) and (AToken^.Kind = AKind) and NOA(Length(AValue) > 0, CompareText(AToken^.Value, AValue) = 0);
  end;
//------------------------------------------------------------------------------
var
  vParser : TPascalParser;
  vToken, vPredPredToken, vPredToken : PTokenInfo;
  vComments : string;
//------------------------------------------------------------------------------
  function NextToken : boolean;
  begin
    vPredPredToken := vPredToken;
    vPredToken := vToken;
    Result := vParser.GetToken(vToken);

    if Result and (vToken^.Kind = tkComment) then
      if Assigned(vPredToken) and (vPredToken^.Kind <> tkComment) then
        vComments := vToken^.Value
      else
        vComments := vComments + vToken^.Value;
  end;
//------------------------------------------------------------------------------
  function NextTokenNotComment : boolean;
  begin
    Result := NextToken;

    if Result and (vToken^.Kind = tkComment) then
      Result := NextTokenNotComment;
  end;
//------------------------------------------------------------------------------
  function ReadCnvUnitName : boolean;
  begin
    Result := TokenIs(vToken, tkIdent, 'UNIT')
          and NextToken
          and (vToken^.Kind = tkIdent);

    if Result then
      ACnvUnitName := vToken^.Value;
  end;
//------------------------------------------------------------------------------
  function IsBeginIntf(out AIntfName : string) : boolean;
  begin
    Result := TokenIs(vToken,          tkIdent,   'INTERFACE')
          and TokenIs(vPredToken,      tkSymbol,  '=')
          and TokenIs(vPredPredToken,  tkIdent);

    if Result  then
      AIntfName := vPredPredToken^.Value;
  end;
//------------------------------------------------------------------------------
  function IsNotForwardAndHasGUID : boolean;
  begin
    Result := NextToken
          and (vToken^.Kind = tkSymbol)
          and (MatchText(vToken^.Value, ['(', '[']));
  end;
//------------------------------------------------------------------------------
  function ReadParentName(out AParentName : string) : boolean;
  begin

    if (CompareText(vToken^.Value, '(') <> 0) then
    begin
      AParentName := '';
      Exit(True)
    end;

    Result := NextToken
          and NextToken
          and TokenIs(vToken,      tkSymbol,  ')')
          and TokenIs(vPredToken,  tkIdent);

    if Result then
      AParentName := vPredToken^.Value;

    Result := NextToken;
  end;
//------------------------------------------------------------------------------
  function ReadGUID(out AGUID : string) : boolean;
  begin
    Result := NextToken
          and NextToken
          and TokenIs(vToken,      tkSymbol,  ']')
          and TokenIs(vPredToken,  tkString)
          and (Length(vPredToken^.Value) > 2);

    if not Result then
      Exit;

    AGUID := vPredToken^.Value;
    Delete(AGUID, 1, 1);
    SetLength(AGUID, Length(AGUID) - 1);
  end;
//------------------------------------------------------------------------------
var
  vIntfName, vIntfParentName, vIntfGuid : string;
//------------------------------------------------------------------------------
  function IsIntfEnd : boolean;
  begin
    Result := TokenIs(vToken, tkIdent, 'END');
  end;
//------------------------------------------------------------------------------
  function ReadItemName(out AName : string) : boolean;
  begin
    Result := NextToken and (vToken^.Kind = tkIdent);

    if Result then
      AName := vToken^.Value;
  end;
//------------------------------------------------------------------------------
var
  vNames : TStrings;
//------------------------------------------------------------------------------
  function ReadParamsGroup(const AParams : TIntfItemParams; const AEndStr : string) : boolean;

    function IsEndParamsGroup : boolean;
    begin
      Result := (vToken^.Kind = tkSymbol) and MatchText(vToken^.Value, [AEndStr, ';']);
    end;
  //----------------------------------------------------------------------------
    function ReadMode(out AMode : TParamMode) : boolean;
    begin

      if vToken^.Kind <> tkIdent then
        Exit(False);

      if CompareText(vToken^.Value, 'CONST') = 0 then
        AMode := pmConst

      else
      if CompareText(vToken^.Value, 'VAR') = 0 then
        AMode := pmVar

      else
      if CompareText(vToken^.Value, 'OUT') = 0 then
        AMode := pmOut

      else
        AMode := pmValue;

      if AMode <> pmValue then
        if not NextTokenNotComment then
          Exit(False);

      Result := True;
    end;
  //----------------------------------------------------------------------------
    function ReadNames : boolean;
    begin
      Result := False;

      repeat

        if not
        (
              NextTokenNotComment
          and (vPredToken^.Kind = tkIdent)
          and (vToken^.Kind = tkSymbol)
        )
        then
          Exit;

        vNames.Add(vPredToken^.Value);

        if CompareText(vToken^.Value, ',') = 0 then
          if not NextTokenNotComment then
            Exit;

      until MatchText(vToken^.Value, [':', ';']);

      Result := True;
    end;
  //----------------------------------------------------------------------------
    function ReadDataType(out ADataType : string) : boolean;
    begin

      if IsEndParamsGroup then
      begin
        ADataType := '';
        Exit(True);
      end;

      Result := NextTokenNotComment
            and (vToken^.Kind = tkIdent);

      if not Result then
        Exit;

      ADataType := vToken^.Value;

      Result := NextTokenNotComment;
    end;
  //----------------------------------------------------------------------------
    function ReadDefault(out AHasDefault : boolean; out ADefaultValue : string) : boolean;
    begin

      if IsEndParamsGroup then
      begin
        AHasDefault := False;
        ADefaultValue := '';
        Exit(True);
      end;

      Result := False;

      if not
      (
            TokenIs(vToken, tkSymbol, '=')
        and NextTokenNotComment
      )
      then
        Exit;


      // Для отрицательных чисел
      if TokenIs(vToken, tkSymbol, '-') then
        begin

          if not NextTokenNotComment then
            Exit;

          ADefaultValue := '-' + vToken^.Value;
        end
      else
        ADefaultValue := vToken^.Value;

      AHasDefault := True;

      Result := NextTokenNotComment;
    end;
  //----------------------------------------------------------------------------
    var
      vMode : TParamMode;
      vDataType, vDefaultValue : string;
      vHasDefault : boolean;
      i : integer;
  begin
    Result := False;
    vNames.Clear;

    repeat

      if not NextTokenNotComment then
        Exit;

      if IsEndParamsGroup then
        Break;

      if not
      (
            ReadMode(vMode)
        and ReadNames
        and ReadDataType(vDataType)
        and ReadDefault(vHasDefault, vDefaultValue)
      )
      then
        Exit;

    until IsEndParamsGroup;


    for I := 0 to vNames.Count - 1 do
      AParams.Add(vNames[i], vDataType, vDefaultValue, vMode, vHasDefault);


    Result := True;
  end;
//------------------------------------------------------------------------------
  function ReadParams(const AParams : TIntfItemParams; const AEndStr : string) : boolean;
  begin
    Result := False;

    repeat

      if not ReadParamsGroup(AParams, AEndStr) then
        Exit;

    until TokenIs(vToken, tkSymbol, AEndStr);

    Result := NextTokenNotComment;
  end;
//------------------------------------------------------------------------------
  function ReadMethod(const AIntf : TIntf; const AName : string; const AIsFunction : boolean) : boolean;
  var
    vMethod : TMethodIntfItem;
  begin
    Result := False;

    vMethod := TMethodIntfItem.Create(AName, vComments, AIsFunction);
    try
      vComments := '';

      if IsIntfEnd then
        Exit(True);

      if vToken^.Kind <> tkSymbol then
        Exit;

      if (CompareText(vToken.Value, '(') = 0) then
        if not ReadParams(vMethod.Params, ')') then
          Exit;

      if AIsFunction then
      begin

         if not
         (
               (CompareText(vToken.Value, ':') = 0)
           and NextTokenNotComment
           and (vToken^.Kind = tkIdent)
           and NextTokenNotComment
         )
         then
           Exit;

        vMethod.ResultDataType := vPredToken^.Value;
      end;

      if IsIntfEnd then
        Exit(True);

      if not
      (
            TokenIs(vToken, tkSymbol, ';')
        and NextTokenNotComment
        and (vToken.Kind = tkIdent)
      )
      then
        Exit;

      if MatchText(vToken^.Value, ['REGISTER', 'PASCAL', 'STDCALL', 'CDECL', 'SAFECALL']) then
      begin

        if not ( NextTokenNotComment and TokenIs(vToken, tkSymbol, ';')) then
          Exit;

        if CompareText(vPredToken^.Value, 'REGISTER')  = 0 then
          vMethod.CallingConv := ccRegister

        else
        if CompareText(vPredToken^.Value, 'PASCAL')    = 0 then
          vMethod.CallingConv := ccPascal

        else
        if COMPAReText(vPredToken^.Value, 'STDCALL')   = 0 then
          vMethod.CallingConv := ccStdcall

        else
        if COMPAReText(vPredToken^.Value, 'CDECL')     = 0 then
          vMethod.CallingConv := ccCdecl

        else
        if COMPAReText(vPredToken^.Value, 'SAFECALL')  = 0 then
          vMethod.CallingConv := ccSafecall;

        if not NextTokenNotComment then
          Exit;
      end;


      Result := True;
    finally
      if Result then
        AIntf.Items.Add(vMethod)
      else
        FreeAndNil(vMethod);
    end;
  end;
//------------------------------------------------------------------------------
  function ReadProperty(const AIntf : TIntf; const AName : string) : boolean;
  var
    vProperty : TPropertyIntfItem;
  begin
    Result := False;

    vProperty := TPropertyIntfItem.Create(AName, vComments);
    try
      vComments := '';

      if IsIntfEnd or not ((vToken^.Kind = tkSymbol) and MatchText(vToken^.Value, ['[', ':'])) then
        Exit;

      if (CompareText(vToken.Value, '[') = 0) then
        if not ReadParams(vProperty.Params, ']') then
          Exit;

      if not
      (
            (CompareText(vToken^.Value, ':') = 0)
        and NextTokenNotComment
        and (vToken^.Kind = tkIdent)
      )
      then
        Exit;

      vProperty.DataType := vToken^.Value;


      if not
      (
            NextTokenNotComment
        and TokenIs(vToken, tkIdent, 'READ')
        and NextTokenNotComment
        and (vToken^.Kind = tkIdent)
      )
      then
        Exit;

      vProperty.Reader := vToken^.Value;


      if not NextTokenNotComment then
        Exit;

      if TokenIs(vToken, tkIdent, 'WRITE') then
        if  NextTokenNotComment
        and (vPredToken^.Kind = tkIdent)
        and NextTokenNotComment
        then
          vProperty.Writer := vPredToken^.Value
        else
          Exit;


      if IsIntfEnd then
        Exit(True);


      if not
      (
            TokenIs(vToken, tkSymbol, ';')
        and NextTokenNotComment
      )
      then
        Exit;


      if IsIntfEnd then
        Exit(True);


      if TokenIs(vToken, tkIdent, 'DEFAULT') then
      begin
        if not NextTokenNotComment then
          Exit;

        vProperty.IsDefault := True;

        if IsIntfEnd then
          Exit(True);

        if not (TokenIs(vToken, tkSymbol, ';') and NextTokenNotComment) then
          Exit;
      end;

      Result := True;

    finally
      if Result then
        AIntf.Items.Add(vProperty)
      else
        FreeAndNil(vProperty);
    end;
  end;
//------------------------------------------------------------------------------
  function ReadIntfItems(out AIntf : TIntf) : boolean;
  var
    vName : string;
  begin
    Result := False;

    AIntf := TIntf.Create(vIntfName, vIntfParentName, vIntfGuid, vComments);
    try
      vComments := '';

      if not NextTokenNotComment then
        Exit;

      repeat

        if IsIntfEnd then
          Break;


        if (vToken^.Kind = tkIdent)
        and MatchText(vToken^.Value, ['FUNCTION', 'PROCEDURE', 'PROPERTY'])
        then
        begin

          if not (ReadItemName(vName) and NextTokenNotComment) then
            Exit;


          if CompareText(vPredPredToken^.Value, 'FUNCTION') = 0 then
            ReadMethod(AIntf, vName, True)

          else
          if CompareText(vPredPredToken^.Value, 'PROCEDURE') = 0 then
            ReadMethod(AIntf, vName, False)

          else
          if CompareText(vPredPredToken^.Value, 'PROPERTY') = 0 then
            ReadProperty(AIntf, vName)

        end;

      until IsIntfEnd;

      Result := True;

    finally
      if not Result then
        FreeAndNil(AIntf);
    end;

  end;
//------------------------------------------------------------------------------
var
  vIntf : TIntf;
begin
  vPredPredToken := nil;
  vPredToken     := nil;

  vParser := TPascalParser.Create(AFileName, FileToString(AFileName));
  try

    vNames := TStringList.Create;
    try

      while NextToken do
      begin

        if ReadCnvUnitName then
          Continue;

        if IsBeginIntf(vIntfName) then
          if IsNotForwardAndHasGUID then
            if ReadParentName(vIntfParentName) then
              if ReadGUID(vIntfGuid) then
                if ReadIntfItems(vIntf) then
                  AIntfs.Add(vIntf);
      end;

    finally
      FreeAndNil(vNames);
    end;

  finally
    FreeAndNil(vParser);
  end;
end;

*)
TYPE
  TTokenKinds = set of TTokenKind;

  /// <summary>
  /// Базовый класс парсера.
  /// </summary>
  TCustomParser = class(TObject)
  private
    FParser : TPascalParser;
    FTokens : TList;
    FComments : string;
    FNames : TStrings;

    function GetCurToken: PTokenInfo;
    function GetPredPredToken: PTokenInfo;
    function GetPredToken: PTokenInfo;

  protected
    procedure DoClearToken(const APtr : Pointer);

  public
    constructor Create(const AFileName : TFileName);
    destructor Destroy; override;

    procedure TokensClear;

    function TokenValueIs(const AToken : PTokenInfo; const AValue : string) : boolean; overload;
    function TokenValueIs(const AToken : PTokenInfo; const AValues : array of string) : boolean; overload;
    function TokenIs(const AToken : PTokenInfo; const AKind: TTokenKind; const AValue : string = '') : boolean; overload;
    function TokenIs(const AToken : PTokenInfo; const AKinds: TTokenKinds; const AValue : string = '') : boolean; overload;
    function TokenIs(const AToken : PTokenInfo; const AKinds: TTokenKinds; const AValues : array of string) : boolean; overload;

    procedure ValidateToken(const AToken : PTokenInfo; const AKinds: TTokenKinds; const AValue : string = ''); overload;
    procedure ValidateToken(const AToken : PTokenInfo; const AKinds: TTokenKinds; const AValues : array of string); overload;
    procedure ValidateToken(const AToken : PTokenInfo; const AKind: TTokenKind; const AValue : string = ''); overload;

    function TryNextToken(const ACombineComments : boolean = True) : boolean;

    function NextToken(const ACombineComments : boolean = True) : PTokenInfo; overload;
    function NextToken(const AAllowableKinds : TTokenKinds;
      const AAllowableValue : string = ''; const ACombineComments : boolean = True) : PTokenInfo; overload;
    function NextToken(const AAllowableKind : TTokenKind;
      const AAllowableValue : string = ''; const ACombineComments : boolean = True) : PTokenInfo; overload;
    function NextToken(const AAllowableKinds : TTokenKinds;
      const AAllowableValues : array of string; const ACombineComments : boolean = True) : PTokenInfo; overload;

    property CurToken       : PTokenInfo  read GetCurToken;
    property PredToken      : PTokenInfo  read GetPredToken;
    property PredPredToken  : PTokenInfo  read GetPredPredToken;
  end;

  /// <summary>
  /// Высокоуровневый парсер.
  /// </summary>
  TParser = class(TCustomParser)
  public
    function ReadUnitName(out AUnitName : string) : boolean;
    function IsBeginIntf(out AIntfName : string) : boolean;
    procedure ReadParentName(out AParentName : string);
    procedure ReadGUID(out AGUID : string);
    function IsIntfEnd : boolean;
    procedure ReadParams(const AParams : TIntfItemParams; const AEndStr : string);
    procedure ReadMethod(const AIntf : TIntf; const AName : string; const AIsFunction : boolean);
    procedure ReadProperty(const AIntf : TIntf; const AName : string);

    procedure ReadIntfItems(const AIntf : TIntf);

    procedure Parse(const AIntfs : TIntfs; out AUnitName : string);

  end;


//==============================================================================
// TCustomParser
// private
function TCustomParser.GetCurToken: PTokenInfo;
begin
  if FTokens.Count < 1 then
    Result := nil
  else
    Result := PTokenInfo(FTokens.First);
end;

function TCustomParser.GetPredToken: PTokenInfo;
begin
  if FTokens.Count < 2 then
    Result := nil
  else
    Result := PTokenInfo(FTokens[1]);
end;

function TCustomParser.GetPredPredToken: PTokenInfo;
begin
  if FTokens.Count < 3 then
    Result := nil
  else
    Result := PTokenInfo(FTokens[2]);
end;

// protected
procedure TCustomParser.DoClearToken(const APtr: Pointer);
begin
  with PTokenInfo(APtr)^ do
  begin
    Value := '';
  end;

  Dispose(APtr);
end;

// public
constructor TCustomParser.Create(const AFileName: TFileName);
begin
  inherited Create;

  FParser := TPascalParser.Create(AFileName, String(FileToString(AFileName)) );
  FTokens := TList.Create;
  FNames := TStringList.Create;
end;

destructor TCustomParser.Destroy;
begin
  TokensClear;

  FreeAndNil(FParser);
  FreeAndNil(FTokens);
  FreeAndNil(FNames);

  inherited;
end;

procedure TCustomParser.TokensClear;
begin
  if Assigned(FTokens) then
    SANListClear(FTokens, DoClearToken);
end;

function TCustomParser.TokenValueIs(const AToken: PTokenInfo;  const AValue: string): boolean;
begin
  Result := CompareText(AToken^.Value, AValue) = 0;
end;

function TCustomParser.TokenValueIs(const AToken: PTokenInfo;
  const AValues: array of string): boolean;
begin
  Result := MatchText(AToken^.Value, AValues);
end;

function TCustomParser.TokenIs(const AToken: PTokenInfo; const AKind: TTokenKind;
  const AValue: string): boolean;
begin
  Result := Assigned(AToken)
        and (AToken^.Kind = AKind)
        and NOA(Length(AValue) > 0, TokenValueIs(AToken, AValue));
end;

function TCustomParser.TokenIs(const AToken: PTokenInfo; const AKinds: TTokenKinds;
  const AValue: string): boolean;
begin
  Result := Assigned(AToken)
        and (AToken^.Kind in AKinds)
        and NOA(Length(AValue) > 0, TokenValueIs(AToken, AValue));
end;

function TCustomParser.TokenIs(const AToken: PTokenInfo;
  const AKinds: TTokenKinds; const AValues: array of string): boolean;
begin
  Result := Assigned(AToken)
        and (AToken^.Kind in AKinds)
        and TokenValueIs(AToken, AValues);
end;


procedure TCustomParser.ValidateToken(const AToken: PTokenInfo;
  const AKinds: TTokenKinds; const AValue: string);
begin

  if not (AToken^.Kind in AKinds) then
    raise EParsingError.CreateResFmt
    (
      @sUnexpectedTokenKindErrorMessage,
      [
        GetEnumName(TypeInfo(TTokenKind), Ord(AToken^.Kind)),
        AToken^.StartLine,
        AToken^.StartIndex
      ]
    );

  if (AValue <> '') and not TokenValueIs(AToken, AValue) then
    raise EParsingError.CreateResFmt
    (
      @sUnexpectedTokenValueErrorMessage,
      [
        AToken^.Value,
        AValue,
        AToken^.StartLine,
        AToken^.StartIndex
      ]
    );

end;

procedure TCustomParser.ValidateToken(const AToken: PTokenInfo;
  const AKinds: TTokenKinds; const AValues: array of string);
begin

  if not (AToken^.Kind in AKinds) then
    raise EParsingError.CreateResFmt
    (
      @sUnexpectedTokenKindErrorMessage,
      [
        GetEnumName(TypeInfo(TTokenKind), Ord(AToken^.Kind)),
        AToken^.StartLine,
        AToken^.StartIndex
      ]
    );


  if not TokenValueIs(AToken, AValues) then
    raise EParsingError.CreateResFmt
    (
      @sUnexpectedTokenValueErrorMessage,
      [
        AToken^.Value,
        StrArrToStr(AValues, ';'),
        AToken^.StartLine,
        AToken^.StartIndex
      ]
    );

end;

procedure TCustomParser.ValidateToken(const AToken: PTokenInfo;
  const AKind: TTokenKind; const AValue: string);
begin
  ValidateToken(AToken, [AKind], AValue);
end;

function TCustomParser.TryNextToken(const ACombineComments: boolean): boolean;
var
  vToken, vSaveToken : PTokenInfo;
begin
  Result := FParser.GetToken(vToken);

  if not Result then
    Exit;

  New(vSaveToken);

  vSaveToken^ := vToken^;

  FTokens.Insert(0, vSaveToken);

  if (vToken^.Kind = tkComment) then
  begin

    if not Assigned(PredToken) or (PredToken^.Kind <> tkComment) then
      FComments := vToken^.Value
    else
      FComments := FComments + vToken^.Value;

    if ACombineComments then
      Result := TryNextToken(ACombineComments);
  end;
end;

function TCustomParser.NextToken(const ACombineComments : boolean = True): PTokenInfo;
begin
  if not TryNextToken(ACombineComments) then
    raise EParsingError.CreateRes(@sUnexpectedEndOfFileErrorMessage);

  Result := CurToken;
end;

function TCustomParser.NextToken(const AAllowableKinds: TTokenKinds;
  const AAllowableValue: string; const ACombineComments: boolean) : PTokenInfo;
begin
  Result := NextToken(ACombineComments);

  ValidateToken(Result, AAllowableKinds, AAllowableValue);
end;

function TCustomParser.NextToken(const AAllowableKind: TTokenKind;
  const AAllowableValue: string; const ACombineComments: boolean): PTokenInfo;
begin
  Result := NextToken([AAllowableKind], AAllowableValue, ACombineComments);
end;

function TCustomParser.NextToken(const AAllowableKinds: TTokenKinds;
  const AAllowableValues: array of string;
  const ACombineComments: boolean): PTokenInfo;
begin
  Result := NextToken(ACombineComments);

  ValidateToken(Result, AAllowableKinds, AAllowableValues);
end;
//==============================================================================

//==============================================================================
// TParser
// public
function TParser.ReadUnitName(out AUnitName: string): boolean;
begin
  Result := TokenIs(CurToken, tkIdent, 'UNIT')
        and TryNextToken
        and TokenIs(CurToken, tkIdent);

  if Result then
    AUnitName := CurToken^.Value;
end;

function TParser.IsBeginIntf(out AIntfName: string): boolean;
begin
  Result := TokenIs(CurToken,       tkIdent,   'INTERFACE')
        and TokenIs(PredToken,      tkSymbol,  '=')
        and TokenIs(PredPredToken,  tkIdent);

  if Result then
    AIntfName := PredPredToken^.Value;
end;

procedure TParser.ReadParentName(out AParentName: string);
begin

  if not TokenValueIs(CurToken, '(') then
  begin
    AParentName := 'IInterface';
    Exit;
  end;

  AParentName := NextToken(tkIdent)^.Value;

  NextToken(tkSymbol,  ')');

  NextToken;
end;

procedure TParser.ReadGUID(out AGUID: string);

  procedure UnQuotes;
  begin
    Delete(AGUID, 1, 1);
    SetLength(AGUID, Length(AGUID) - 1);
  end;

begin
  ValidateToken(CurToken, tkSymbol, '[');

  AGUID := NextToken(tkString)^.Value;

  NextToken(tkSymbol, ']');

  if Length(AGUID) <> Length(#39'{00000000-0000-0000-0000-000000000000}'#39) then
    raise EParsingError.CreateRes(@sIncorrectInterfaceGUIDErrorMessage);

  UnQuotes;

  NextToken;
end;

function TParser.IsIntfEnd: boolean;
begin
  Result := TokenIs(CurToken, tkIdent, 'END');
end;

procedure TParser.ReadParams(const AParams: TIntfItemParams; const AEndStr: string);
//------------------------------------------------------------------------------
  procedure ReadMode(out AMode : TParamMode);
  begin

    if TokenValueIs(CurToken, ['CONST', 'VAR', 'OUT']) then
      begin
        AMode := StrToParamMode(CurToken^.Value);

        NextToken(tkIdent);
      end
    else
      AMode := pmValue;

  end;
//------------------------------------------------------------------------------
  procedure ReadNames;
  begin

    repeat
      FNames.Add(CurToken^.Value);

      if TokenValueIs( NextToken([tkSymbol], [',', ':']), ':') then
        Break;

      NextToken(tkIdent);
    until FALSE;

    NextToken(tkIdent);
  end;
//------------------------------------------------------------------------------
  procedure ReadDataType(out ADataType : string);
  begin
    ADataType := CurToken^.Value;

    NextToken([tkSymbol], ['=', ';', AEndStr]);
  end;
//------------------------------------------------------------------------------
  procedure ReadDefault(out ADefaultValue : string);
  begin
    NextToken;

    // Для отрицательных чисел
    if TokenIs(CurToken, tkSymbol, '-') then
      ADefaultValue := '-' + NextToken^.Value
    else
      ADefaultValue := CurToken^.Value;

    NextToken;
  end;
//------------------------------------------------------------------------------
var
  vMode : TParamMode;
  vDataType, vDefaultValue : string;
  vHasDefault : boolean;
  i : integer;
begin

  while not TokenIs(CurToken, tkSymbol, AEndStr) do
  begin
    FNames.Clear;

    NextToken(tkIdent);

    ReadMode(vMode);

    ReadNames;

    ReadDataType(vDataType);

    vHasDefault := TokenValueIs(CurToken, '=');

    if vHasDefault then
      ReadDefault(vDefaultValue)
    else
      vDefaultValue := '';

    for I := 0 to FNames.Count - 1 do
      AParams.Add(FNames[i], vDataType, vDefaultValue, vMode, vHasDefault);
  end;

  NextToken;
end;

procedure TParser.ReadMethod(const AIntf: TIntf; const AName: string;
  const AIsFunction: boolean);

  procedure ReadFuncResult(const AMethod : TMethodIntfItem);
  begin
    ValidateToken(CurToken, tkSymbol, ':');

    AMethod.ResultDataType := NextToken(tkIdent)^.Value;

    NextToken;
  end;

  procedure ReadCallingConv(const AMethod : TMethodIntfItem);
  begin
    AMethod.CallingConv := StrToCallingConv(CurToken^.Value);

    NextToken;

    if IsIntfEnd then
      Exit;

    ValidateToken(CurToken, tkSymbol, ';');

    NextToken;
  end;

var
  vMethod : TMethodIntfItem;
begin

  vMethod := TMethodIntfItem.Create(AName, FComments, AIsFunction);
  try

    try
      FComments := '';

      NextToken;

      if IsIntfEnd then
        if not AIsFunction then
          Exit
        else
          raise EParsingError.CreateRes(@sUnexpectedEndOfFileErrorMessage);

      if TokenIs(CurToken, tkSymbol, '(') then
        ReadParams(vMethod.Params, ')');

      if AIsFunction then
        ReadFuncResult(vMethod);

      if IsIntfEnd then
        Exit;

      ValidateToken(CurToken, tkSymbol, ';');

      NextToken;

      if  not IsIntfEnd
      and TokenValueIs(CurToken, ['REGISTER', 'PASCAL', 'STDCALL', 'CDECL', 'SAFECALL'])
      then
        ReadCallingConv(vMethod);

    except
      FreeAndNil(vMethod);
      raise;
    end;

  finally
    if Assigned(vMethod) then
      AIntf.Items.Add(vMethod);
  end;

end;

procedure TParser.ReadProperty(const AIntf: TIntf; const AName: string);
var
  vProperty : TPropertyIntfItem;
begin

  vProperty := TPropertyIntfItem.Create(AName, FComments);
  try

    try
      FComments := '';

      NextToken;

      if IsIntfEnd then
        raise EParsingError.CreateRes(@sUnexpectedEndOfFileErrorMessage);

      if TokenIs(CurToken, tkSymbol, '[') then
        ReadParams(vProperty.Params, ']');

      ValidateToken(CurToken, tkSymbol, ':');
      vProperty.DataType := NextToken(tkIdent)^.Value;

      NextToken(tkIdent, 'READ');
      vProperty.Reader := NextToken(tkIdent)^.Value;
      NextToken;

      if TokenIs(CurToken, tkIdent, 'WRITE') then
      begin
        vProperty.Writer := NextToken(tkIdent)^.Value;
        NextToken;
      end;

      if IsIntfEnd then
        Exit;

      ValidateToken(CurToken, tkSymbol, ';');

      NextToken;

      if TokenIs(CurToken, tkIdent, 'DEFAULT') then
      begin
        vProperty.IsDefault := True;
        NextToken;

        if IsIntfEnd then
          Exit;

        ValidateToken(CurToken, tkSymbol, ';');

        NextToken;
      end;

    except
      FreeAndNil(vProperty);
      raise;
    end;

  finally
    if Assigned(vProperty) then
      AIntf.Items.Add(vProperty)
  end;

end;

procedure TParser.ReadIntfItems(const AIntf: TIntf);
var
  vName : string;
  vToken : PTokenInfo;
begin

  while not IsIntfEnd do
  begin
    vToken := CurToken;


    if not TokenIs(vToken, [tkIdent], ['FUNCTION', 'PROCEDURE', 'PROPERTY']) then
      raise EParsingError.CreateResFmt
      (
        @sFuncProcPropExpectedErrorMessage,
        [
          GetEnumName(TypeInfo(TTokenKind), Ord(vToken^.Kind)),
          vToken^.Value,
          vToken^.StartLine,
          vToken^.StartIndex
        ]
      );


    vName := NextToken(tkIdent)^.Value;


    if TokenValueIs(vToken, 'FUNCTION') then
      ReadMethod(AIntf, vName, True)

    else
    if TokenValueIs(vToken, 'PROCEDURE') then
      ReadMethod(AIntf, vName, False)

    else
    if TokenValueIs(vToken, 'PROPERTY') then
      ReadProperty(AIntf, vName)

  end;

end;

procedure TParser.Parse(const AIntfs : TIntfs; out AUnitName : string);
var
  vIntfName, vIntfParentName, vIntfGUID : string;
  vIntf : TIntf;
begin

  repeat
  until not TryNextToken or ReadUnitName(AUnitName);

  while TryNextToken do
    if IsBeginIntf(vIntfName) then
      if TryNextToken and not TokenIs(CurToken, tkSymbol, ';') then
      begin
        ReadParentName(vIntfParentName);
        ReadGUID(vIntfGUID);

        vIntf := TIntf.Create(vIntfName, vIntfParentName, vIntfGuid, FComments);
        try
          FComments := '';

          ReadIntfItems(vIntf);

          AIntfs.Add(vIntf);
        except
          FreeAndNil(vIntf);
          raise;
        end;
      end;

end;
//==============================================================================

procedure Parsing(const AFileName : TFileName; const AIntfs : TIntfs; out ACnvUnitName : string);
var
  vParser : TParser;
begin
  vParser := TParser.Create(AFileName);
  try

    vParser.Parse(AIntfs, ACnvUnitName);

  finally
    FreeAndNil(vParser);
  end;
end;




END.
