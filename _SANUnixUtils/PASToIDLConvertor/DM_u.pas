unit DM_u;

interface

uses
  SysUtils, Classes, Forms, StrUtils, ActiveX, ComObj,
  JCLStrings,
  dpp_PascalParser,
  SANMisc, SANINIProps,
  PasToIDlTypes_u, Parsing_u, IDLEncoding_u;

type
  TProps = class(TSANCustomINIProps)
  private
    FPasFileName, FIdlFileName: TFileName;

  public
    constructor Create;

  published
    property PasFileName: TFileName read FPasFileName write FPasFileName;
    property IdlFileName: TFileName read FIdlFileName write FIdlFileName;
  end;

  TDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

  private
    FProps : TProps;

    FCnvUnitName : string;

    FIntfs : TIntfs;

    procedure SetCnvUnitName(const Value: string);

  protected
    function ConfigINIFileName : TFileName;

  public
    procedure Parsing;

    procedure CreateIDL;

    property Props : TProps  read FProps;

    property CnvUnitName : string  read FCnvUnitName  write SetCnvUnitName;

    property Intfs : TIntfs  read FIntfs;
  end;

var
  DM: TDM;

implementation

{$R *.dfm}

//==============================================================================
// TProps
// public
constructor TProps.Create;
begin
  inherited;

end;
//==============================================================================

//==============================================================================
// TDM
// private
procedure TDM.SetCnvUnitName(const Value: string);
begin
  FCnvUnitName := Value;

  (Application.MainForm as IMainCaptionUnitName).MainCaption(Value);
end;

// protected
function TDM.ConfigINIFileName: TFileName;
begin
  Result := ExtractFilePath(ParamStr(0))
          + StringReplace( ExtractFileName(ParamStr(0)), '.exe', '.ini', [rfReplaceAll, rfIgnoreCase]);
end;

// public
procedure TDM.Parsing;
var
  vCnvUnitName : string;
begin
  Parsing_u.Parsing(Props.PasFileName, FIntfs, vCnvUnitName);

  FIntfs.RefreshLinks;

  CnvUnitName := vCnvUnitName;
end;

procedure TDM.CreateIDL;
var
  vGUID : TGUID;
  vIDLTest : TStrings;
begin
  OleCheck(CreateGUID(vGUID));

  vIDLTest := TStringList.Create;
  try
    vIDLTest.Text := IDLEncoding_u.IDLEncoding(CnvUnitName, GUIDToString(vGUID), FIntfs);

    vIDLTest.SaveToFile(Props.IdlFileName);
  finally
    FreeAndNil(vIDLTest);
  end;
end;

// published
procedure TDM.DataModuleCreate(Sender: TObject);
begin
  FProps := TProps.Create;
  FProps.LoadFromINI(ConfigINIFileName);

  FIntfs := TIntfs.Create(True);
end;

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FIntfs);

  FProps.SaveToINI(ConfigINIFileName);
  FreeAndNil(FProps);
end;
//==============================================================================



end.


