unit PasToIDlTypes_u;

interface

USES
  SysUtils, Classes, Generics.Collections, StrUtils, ActiveX, ComObj, INIFiles,
  uPSCompiler,
  uPSRuntime,
  uPSUtils;

TYPE
  EPasToIDL = class(Exception);

  TIntf = class;

  /// <summary>
  /// Интерфейс задания заголовка главной формы
  /// </summary>
  IMainCaptionUnitName = interface(IInterface)
    ['{1A53E99E-749A-486D-9535-F2EFE97B49E1}']

    procedure MainCaption(const ACnvUnitName : string);
  end;

  /// <summary>
  /// Интерфейс управления текущим интерфейсом
  /// </summary>
  ICurIntfController = interface
    ['{6D3C459F-80C9-457E-A1C2-5264C33BF073}']

    function CurIntf : TIntf;
  end;

  /// <summary>
  /// Интерфейс зависящего от текущего интерфейса
  /// </summary>
  ICurIntfClient = interface
    ['{6D3C459F-80C9-457E-A1C2-5264C33BF073}']

    procedure CurIntfUpdated;
  end;

//  TIntfItemKind = (iikNone, iikProc, iikFunc, iikProp);

  /// <summary>
  /// Модификатор параметра.
  /// </summary>
  TParamMode = (pmNone, pmValue, pmConst, pmVar, pmOut);


  /// <summary>
  /// Соглашения о вызовах
  /// </summary>
  TCallingConv = (ccRegister, ccPascal, ccStdcall, ccCdecl, ccSafecall);

  /// <summary>
  /// Параметр метода или свойства интерфейса
  /// </summary>
  TIntfItemParam = class(TPersistent)
  private
    FName, FDataType, FDefaultVal : string;
    FMode : TParamMode;
    FHasDefault : boolean;

  public
    constructor Create(const AName, ADataType, ADefaultVal : string;
      const AMode : TParamMode; const AHasDefault : boolean);

    function AsText : string;

    property Name        : string      read FName;
    property DataType    : string      read FDataType;
    property DefaultVal  : string      read FDefaultVal;
    property Mode        : TParamMode  read FMode;
    property HasDefault  : boolean     read FHasDefault;
  end;

  /// <summary>
  /// Список интерфейсов
  /// </summary>
  TIntfItemParams = class(TObjectList<TIntfItemParam>)
  public
    function Add(const AName, ADataType, ADefaultVal : string;
      const AMode : TParamMode; const AHasDefault : boolean) : TIntfItemParam;

    function AsText : string;
  end;

  /// <summary>
  /// Базовый класс элемента интерфейса
  /// </summary>
  TCustomIntfItem = class(TPersistent)
  private
    FName, FComments : string;
    FParams : TIntfItemParams;

  public
    constructor Create(const AName, AComments : string);
    destructor Destroy; override;

    property Name : string  read FName;
    property Params : TIntfItemParams  read FParams;
  end;

  /// <summary>
  /// Список элементов интерфейса
  /// </summary>
  TIntfItems = class(TObjectList<TCustomIntfItem>)
  public
    function FindByName(out AItem; const AName : string) : boolean;
    function GetByName(const AName : string) : TCustomIntfItem;
    procedure RefreshLinks;
  end;

  TPropertyIntfItem = class;

  /// <summary>
  /// Элемента интерфейса: Метод
  /// </summary>
  TMethodIntfItem = class(TCustomIntfItem)
  private
    FIsFunction : boolean;
    FResultDataType : string;
    FCallingConv : TCallingConv;
    FPropLink : TPropertyIntfItem;

  public
    constructor Create(const AName, AComments : string; const AIsFunction : boolean);

    property IsFunction : boolean  read FIsFunction;
    property ResultDataType : string  read FResultDataType  write FResultDataType;
    property CallingConv : TCallingConv  read FCallingConv  write FCallingConv;

    property PropLink : TPropertyIntfItem  read FPropLink;
  end;

  /// <summary>
  /// Элемента интерфейса: Свойство
  /// </summary>
  TPropertyIntfItem = class(TCustomIntfItem)
  private
    FDataType, FReader, FWriter : string;
    FIsDefault : boolean;
    FReaderLink, FWriterLink : TMethodIntfItem;

  public
    property DataType : string  read FDataType  write FDataType;
    property Reader : string  read FReader  write FReader;
    property Writer : string  read FWriter  write FWriter;
    property IsDefault : boolean  read FIsDefault  write FIsDefault;

    property ReaderLink : TMethodIntfItem  read FReaderLink;
    property WriterLink : TMethodIntfItem  read FWriterLink;
  end;

  /// <summary>
  /// Параметры одного интерфейса
  /// </summary>
  TIntf = class(TPersistent)
  private
    FName, FParentName, FNewParentName, FGUID, FComments : string;
    FItems : TIntfItems;

    function GetHelpStr: string;

  public
    constructor Create(const AName, AParentName, AGUID, AComments : string);
    destructor Destroy; override;

    property Name : string  read FName;
    property ParentName : string  read FParentName;
    property NewParentName : string  read FNewParentName  write FNewParentName;
    property GUID : string  read FGUID;
    property Comments : string  read FComments;
    property HelpStr : string  read GetHelpStr;
    property Items : TIntfItems  read FItems;

  end;

  /// <summary>
  /// Список интерфейсов
  /// </summary>
  TIntfs = class(TObjectList<TIntf>)
  public
    function FindByName(out AItem; const AName : string) : boolean;
    function ExistsByName(const AName : string) : boolean;
    procedure RefreshLinks;
    procedure ExportNewParentName(const AStrings : TStrings);
  end;

  TConvertInterfaceNameProc = procedure(const AInIntfsList : boolean; const APasInterface : string; var AIdlInterface : string) of object;
  TConvertParentInterfaceNameProc = procedure(const AInIntfsList : boolean; const APasInterface, APasParentInterface : string; var AIdlParentInterface : string) of object;
  TConvertGUIDProc = procedure(const APasInterface, APasGUID : string; var AIDlGUID : string) of object;
  TConvertMethodNameProc = procedure(const APasInterface, AMethodName : string; var ACnvMethodName : string) of object;
  TConvertDataTypeProc = function(const AParamName, ADataType : string; var ACnvDataType : string) : boolean of object;


  /// <summary>
  /// Параметры конвертации
  /// </summary>
  TConvertationSettings = class(TPersistent)
  private
    FTypeLibGUID : TGUID;
    FTypeLibName : string;
    FIntfTrfList : TStrings;
    FCode : AnsiString;

    FCompiler: TPSPascalCompiler;
    FExec : TPSExec;

    FConvertInterfaceNameProc : TConvertInterfaceNameProc;
    FConvertParentInterfaceNameProc : TConvertParentInterfaceNameProc;
    FConvertGUIDProc : TConvertGUIDProc;
    FConvertMethodNameProc : TConvertMethodNameProc;
    FConvertDataTypeProc : TConvertDataTypeProc;

  public
    constructor Create(const ATypeLibName : string);
    destructor Destroy; override;

    procedure LoadFromFile(const AFileName : TFileName);

    function CompileCode(out AMessages : string) : boolean;

    function ConvertInterfaceName(const AInterfaceName : string) : string;
    function ConvertParentInterfaceName(const APasInterfaceName, APasParentInterfaceName : string) : string;
    function ConvertGUID(const APasInterface, APasGUID : string) : string;
    function ConvertMethodName(const APasInterface, AMethodName : string) : string;

    function ConvertDataType(const AParamName, ADataType : string; out ACnvDataType : string) : boolean;

    property TypeLibName : string  read FTypeLibName;
    property TypeLibGUID : TGUID   read FTypeLibGUID;
    property IntfTrfList : TStrings  read FIntfTrfList;

  end;

function StrToParamMode(const S : string) : TParamMode;
function StrToCallingConv(const S : string) : TCallingConv;
function CreateGUIDStr : string;

IMPLEMENTATION

//==============================================================================
function StrToParamMode(const S : string) : TParamMode;
begin
  if CompareText(S, 'CONST') = 0 then
    Result := pmConst

  else
  if CompareText(S, 'VAR') = 0 then
    Result := pmVar

  else
  if CompareText(S, 'OUT') = 0 then
    Result := pmOut

  else
    Result := pmValue;
end;

function StrToCallingConv(const S : string) : TCallingConv;
begin

  if CompareText(S, 'REGISTER')  = 0 then
   Result := ccRegister

  else
  if CompareText(S, 'PASCAL')    = 0 then
    Result := ccPascal

  else
  if CompareText(S, 'STDCALL')   = 0 then
    Result := ccStdcall

  else
  if CompareText(S, 'CDECL')     = 0 then
    Result := ccCdecl

  else
  if CompareText(S, 'SAFECALL')  = 0 then
    Result := ccSafecall

  else
    raise EPasToIDL.CreateFmt('Invalid calling convention string "%s"', [S]);

end;

function CreateGUIDStr : string;
var
  vGUID : TGUID;
begin
  OleCheck(CreateGUID(vGUID));
  Result := GUIDToString(vGUID);
end;

{$IFDEF UNICODE}
function ScriptOnExportCheck(Sender: TPSPascalCompiler; Proc: TPSInternalProcedure; const ProcDecl: AnsiString): Boolean;
{$ELSE}
function ScriptOnExportCheck(Sender: TPSPascalCompiler; Proc: TPSInternalProcedure; const ProcDecl: string): Boolean;
{$ENDIF}
{
  The OnExportCheck callback function is called for each function in the script
  (Also for the main proc, with '!MAIN' as a Proc^.Name). ProcDecl contains the
  result type and parameter types of a function using this format:
  ProcDecl: ResultType + ' ' + Parameter1 + ' ' + Parameter2 + ' '+Parameter3 + .....
  Parameter: ParameterType+TypeName
  ParameterType is @ for a normal parameter and ! for a var parameter.
  A result type of 0 means no result.
}
begin

  if MatchText(String(Proc.Name), ['ConvertInterfaceName']) then
  begin
    if not ExportCheck(Sender, Proc, [0, btEnum, btUnicodeString, btUnicodeString], [uPSCompiler.pmIn, uPSCompiler.pmIn, uPSCompiler.pmInOut]) then
    begin
      { Something is wrong, so cause an error. }
      Sender.MakeError('', ecTypeMismatch, '');
      Result := False;
      Exit;
    end;
    Result := True;
  end

  else
  if MatchText(String(Proc.Name), ['ConvertParentInterfaceName']) then
  begin
    if not ExportCheck(Sender, Proc, [0, btEnum, btUnicodeString,btUnicodeString, btUnicodeString], [uPSCompiler.pmIn, uPSCompiler.pmIn, uPSCompiler.pmIn, uPSCompiler.pmInOut]) then
    begin
      { Something is wrong, so cause an error. }
      Sender.MakeError('', ecTypeMismatch, '');
      Result := False;
      Exit;
    end;
    Result := True;
  end

  else
  if MatchText(String(Proc.Name), ['ConvertGUID', 'ConvertMethodName']) then
  begin
    if not ExportCheck(Sender, Proc, [0, btUnicodeString, btUnicodeString, btUnicodeString], [uPSCompiler.pmIn, uPSCompiler.pmIn, uPSCompiler.pmInOut]) then
    begin
      { Something is wrong, so cause an error. }
      Sender.MakeError('', ecTypeMismatch, '');
      Result := False;
      Exit;
    end;
    Result := True;
  end

  else
  if MatchText(String(Proc.Name), ['ConvertDataType']) then
  begin
    if not ExportCheck(Sender, Proc, [btEnum, btUnicodeString, btUnicodeString, btUnicodeString], [uPSCompiler.pmIn, uPSCompiler.pmIn, uPSCompiler.pmInOut]) then
    begin
      { Something is wrong, so cause an error. }
      Sender.MakeError('', ecTypeMismatch, '');
      Result := False;
      Exit;
    end;
    Result := True;
  end

  else
    Result := True;

end;

{$IFDEF UNICODE}
function ScriptOnUses(Sender: TPSPascalCompiler; const Name: AnsiString): Boolean;
{$ELSE}
function ScriptOnUses(Sender: TPSPascalCompiler; const Name: string): Boolean;
{$ENDIF}
{ the OnUses callback function is called for each "uses" in the script.
  It's always called with the parameter 'SYSTEM' at the top of the script.
  For example: uses ii1, ii2;
  This will call this function 3 times. First with 'SYSTEM' then 'II1' and then 'II2'.
}
begin
  if Name = 'SYSTEM' then
  begin
    Sender.AddDelphiFunction('function CompareText(const S1, S2: string): Integer');
    Sender.AddDelphiFunction('function CreateGUIDStr : string');

    Result := True;
  end else
    Result := False;
end;
//==============================================================================

//==============================================================================
// TIntfItemParam
// public
constructor TIntfItemParam.Create(const AName, ADataType, ADefaultVal: string;
  const AMode: TParamMode; const AHasDefault: boolean);
begin
  inherited Create;

  FName        := AName;
  FDataType    := ADataType;
  FDefaultVal  := ADefaultVal;
  FMode        := AMode;
  FHasDefault  := AHasDefault;
end;

function TIntfItemParam.AsText: string;
begin

  case FMode of
    pmConst:
      Result := 'const ';

    pmVar:
      Result := 'var ';

    pmOut:
      Result := 'out ';

    else
      Result := ''
  end;

  Result := Result + FName + ' : ' + FDataType + IfThen(FHasDefault, ' = ' + FDefaultVal);
end;
//==============================================================================

//==============================================================================
// TIntfItemParams
// public
function TIntfItemParams.Add(const AName, ADataType, ADefaultVal: string;
  const AMode: TParamMode; const AHasDefault: boolean): TIntfItemParam;
begin
  Result := TIntfItemParam.Create(AName, ADataType, ADefaultVal, AMode, AHasDefault);
  inherited Add(Result);
end;

function TIntfItemParams.AsText: string;
var
  i : integer;
begin

  if Count = 0 then
    Result := ''

  else
    begin
      Result := First.AsText;

      for i := 1 to Count - 1 do
        Result := Result + '; ' + Items[i].AsText;
    end;

end;
//==============================================================================

//==============================================================================
// TCustomIntfItem
// public
constructor TCustomIntfItem.Create(const AName, AComments: string);
begin
  inherited Create;
  FName      := AName;
  FComments  := AComments;

  FParams := TIntfItemParams.Create(True);
end;

destructor TCustomIntfItem.Destroy;
begin
  FreeAndNil(FParams);
  inherited;
end;
//==============================================================================

//==============================================================================
// TIntfItems
// public
function TIntfItems.FindByName(out AItem; const AName: string): boolean;
var
  vItem : TCustomIntfItem;
begin

  for vItem in Self do
    if CompareText(vItem.Name, AName) = 0 then
    begin
      TObject(AItem) := vItem;
      Exit(True);
    end;
    
  Result := False;
end;

function TIntfItems.GetByName(const AName: string): TCustomIntfItem;
begin
  if not FindByName(Result, AName) then
    raise EPasToIDL.CreateFmt('Не найден элемент с именем %s', [AName]);
end;

procedure TIntfItems.RefreshLinks;

  procedure SetLink(const APropItem : TPropertyIntfItem;
    const AMethodName : string; var ALink : TMethodIntfItem);
  begin
    if  (AMethodName <> '')
    and FindByName(ALink, AMethodName)
    then
      ALink.FPropLink := APropItem;
  end;

var
  i : integer;
  vPropItem : TPropertyIntfItem;
begin

  for i := 0 to Count - 1 do
    if Items[i] is TPropertyIntfItem then
    begin
      vPropItem := (Items[i] as TPropertyIntfItem);

      SetLink(vPropItem, vPropItem.FReader, vPropItem.FReaderLink);
      SetLink(vPropItem, vPropItem.FWriter, vPropItem.FWriterLink);
    end;

end;
//==============================================================================

//==============================================================================
// TMethodIntfItem
// public
constructor TMethodIntfItem.Create(const AName, AComments: string;
  const AIsFunction: boolean);
begin
  inherited Create(AName, AComments);

  FIsFunction := AIsFunction;
end;
//==============================================================================

//==============================================================================
// TIntf
// private
function TIntf.GetHelpStr: string;
begin
  Result := '';
end;

// public
constructor TIntf.Create(const AName, AParentName, AGUID, AComments: string);
begin
  inherited Create;

  FName        := AName;
  FParentName  := AParentName;
  FGUID        := AGUID;
  FComments    := AComments;

  FItems := TIntfItems.Create(True);
end;

destructor TIntf.Destroy;
begin
  FreeAndNil(FItems);

  inherited;
end;
//==============================================================================

//==============================================================================
// TIntfs
// public
function TIntfs.FindByName(out AItem; const AName: string): boolean;
var
  i : integer;
begin

  for i := 0 to Count - 1 do
    if CompareStr(Items[i].Name, AName) = 0 then
    begin
      TObject(AItem) := Items[i];
      Exit(True);
    end;

  Result := False;
end;

function TIntfs.ExistsByName(const AName: string): boolean;
var
  vDummy : TObject;
begin
  Result := FindByName(vDummy, AName);
end;

procedure TIntfs.RefreshLinks;
var
  vIntf : TIntf;
begin
  for vIntf in Self do
    vIntf.Items.RefreshLinks;
end;

procedure TIntfs.ExportNewParentName(const AStrings: TStrings);
var
  vIntf : TIntf;
begin
  AStrings.BeginUpdate;
  try

    for vIntf in Self do
      if  (vIntf.NewParentName <> '')
      and (AStrings.IndexOf(vIntf.NewParentName) < 0)
      then
        AStrings.Add(vIntf.NewParentName);

  finally
    AStrings.EndUpdate;
  end;
end;
//==============================================================================

//==============================================================================
// TConvertationSettings
// public
constructor TConvertationSettings.Create(const ATypeLibName: string);
begin
  inherited Create;

  OleCheck(CreateGUID(FTypeLibGUID));

  FTypeLibName := ATypeLibName;

  FIntfTrfList := TStringList.Create;

  FCompiler := TPSPascalCompiler.Create;
  FCompiler.OnUses := ScriptOnUses; // assign the OnUses event.
  FCompiler.OnExportCheck := ScriptOnExportCheck; // Assign the onExportCheck event.
  FCompiler.AllowNoBegin := True;
  FCompiler.AllowNoEnd := True; // AllowNoBegin and AllowNoEnd allows it that begin and end are not required in a script.

  FExec := TPSExec.Create;
  FExec.RegisterDelphiFunction(@CompareText,    'COMPARETEXT',    cdRegister);
  FExec.RegisterDelphiFunction(@CreateGUIDStr,  'CREATEGUIDSTR',  cdRegister);
end;

destructor TConvertationSettings.Destroy;
begin
  FreeAndNil(FCompiler);
  FreeAndNil(FExec);

  FreeAndNil(FIntfTrfList);

  inherited;
end;

procedure TConvertationSettings.LoadFromFile(const AFileName: TFileName);

  function LoadCode(const ASections : TStrings) : string;
  var
    vCodeIdx, vFirstLine, vLastLine, i : integer;
    vSL : TStringList;
    vNextSection : string;
  begin

    vCodeIdx := ASections.IndexOf('Code');
    if vCodeIdx < 0 then
      Exit('');

    if vCodeIdx < ASections.Count - 1 then
      vNextSection := ASections[vCodeIdx + 1]
    else
      vNextSection := '';

    vSL := TStringList.Create;
    try

      vSL.LoadFromFile(AFileName);

      vFirstLine := vSL.IndexOf('[Code]') + 1;
      if vNextSection = '' then
        vLastLine := vSL.Count
      else
        vLastLine := vSL.IndexOf('[' + vNextSection + ']');


      Result := '';
      for i := vFirstLine to vLastLine - 1 do
        Result := Result + vSL[i] + #13#10;

    finally
      FreeAndNil(vSL);
    end;

  end;

var
  vINI : TIniFile;
  vSections : TStringList;
begin
  vINI := TIniFile.Create(AFileName);
  try

    vSections := TStringList.Create;
    try
      vINI.ReadSections(vSections);

      FTypeLibName := Format( vINI.ReadString('Main', 'TypeLibName', '%s'), [FTypeLibName] );
      FTypeLibGUID := StringToGUID(vINI.ReadString('Main', 'TypeLibGUID', '{00000000-0000-0000-0000-000000000000}'));

      vINI.ReadSectionValues('Interfaces', FIntfTrfList);

      FCode := AnsiString( LoadCode(vSections) );

    finally
      FreeAndNil(vSections);
    end;

  finally
    FreeAndNil(vINI);
  end;
end;

function TConvertationSettings.CompileCode(out AMessages : string) : boolean;
var
  i : integer;
  vData : AnsiString;
begin
  AMessages := '';

  Result := FCompiler.Compile(FCode);

  if not Result then
  begin

    for i := 0 to FCompiler.MsgCount - 1 do
      AMessages := AMessages + String(FCompiler.Msg[i].MessageToString);

    AMessages := AMessages + #13#10'Code section compiling faild';

    Exit;
  end;

  FCompiler.GetOutput(vData);

  Result := FExec.LoadData(vData);

  FConvertInterfaceNameProc        := TConvertInterfaceNameProc(FExec.GetProcAsMethodN('ConvertInterfaceName'));
  FConvertParentInterfaceNameProc  := TConvertParentInterfaceNameProc(FExec.GetProcAsMethodN('ConvertParentInterfaceName'));
  FConvertGUIDProc                 := TConvertGUIDProc(FExec.GetProcAsMethodN('ConvertGUID'));
  FConvertMethodNameProc           := TConvertMethodNameProc(FExec.GetProcAsMethodN('ConvertMethodName'));
  FConvertDataTypeProc             := TConvertDataTypeProc(FExec.GetProcAsMethodN('ConvertDataType'));
end;

function TConvertationSettings.ConvertInterfaceName(const AInterfaceName : string): string;
var
  vIndex : integer;
begin

  vIndex := FIntfTrfList.IndexOfName(AInterfaceName);
  if vIndex < 0 then
    Result := AInterfaceName
  else
    Result := FIntfTrfList.ValueFromIndex[vIndex];

  if Assigned(FConvertInterfaceNameProc) then
    FConvertInterfaceNameProc(vIndex >= 0, AInterfaceName, Result);
end;

function TConvertationSettings.ConvertParentInterfaceName(const APasInterfaceName,
  APasParentInterfaceName : string): string;
var
  vIndex : integer;
begin

  vIndex := FIntfTrfList.IndexOfName(APasParentInterfaceName);
  if vIndex < 0 then
    Result := APasParentInterfaceName
  else
    Result := FIntfTrfList.ValueFromIndex[vIndex];

  if Assigned(FConvertParentInterfaceNameProc) then
    FConvertParentInterfaceNameProc(vIndex >= 0, APasInterfaceName, APasParentInterfaceName, Result);
end;

function TConvertationSettings.ConvertGUID(const APasInterface, APasGUID: string): string;
begin
  Result := APasGUID;

  if Assigned(FConvertGUIDProc) then
    FConvertGUIDProc(APasInterface, APasGUID, Result);
end;

function TConvertationSettings.ConvertMethodName(const APasInterface, AMethodName: string): string;
begin
  Result := AMethodName;

  if Assigned(FConvertMethodNameProc) then
    FConvertMethodNameProc(APasInterface, AMethodName, Result);
end;

function TConvertationSettings.ConvertDataType(const AParamName, ADataType : string; out ACnvDataType : string) : boolean;
begin
  if Assigned(FConvertDataTypeProc) then
    Result := FConvertDataTypeProc(AParamName, ADataType, ACnvDataType)
  else
    Result := False;
end;
//==============================================================================

END.
