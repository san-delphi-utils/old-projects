program PASToIDLCnv;

{$APPTYPE CONSOLE}

uses
  Windows,
  SysUtils,
  Classes,
  JCLDebug,
  JclStrings,
  IDLConsts_u in 'IDLConsts_u.pas',
  IDLEncoding_u in 'IDLEncoding_u.pas',
  Parsing_u in 'Parsing_u.pas',
  PasToIDlTypes_u in 'PasToIDlTypes_u.pas',
  PAStoIDLMessages in 'PAStoIDLMessages.pas';

procedure CnvWriteln(const S: string);
var
  NewStr: AnsiString;
begin
  SetLength(NewStr, Length(S));
  CharToOem(PChar(S), PAnsiChar(NewStr));
  Writeln(NewStr);
end;

procedure ShowExceptions(const E : Exception);
var
  vSL : TStringList;
begin
  vSL := TStringList.Create;
  try
    JclLastExceptStackListToStrings(vSL, True, True, True, True);
    vSL.Insert(0, E.Message);
    vSL.Insert(1, '');

    CnvWriteln(vSL.Text);
  finally
    FreeAndNil(vSL);
  end;
end;

var
  vIntfs : TIntfs;
  vCnsSettings : TConvertationSettings;
  vUnitName, S : string;
//  a : string;
BEGIN
  JclStartExceptionTracking;
  try

    try
      WriteLn(LoadResString(@sHeaderMessage));

      if (ParamCount >= 1) and (ParamStr(1) = '/?') then
      begin
        WriteLn(LoadResString(@sParamsMessage));
        Exit;
      end;

      if ParamCount < 3 then
      begin
        WriteLn(LoadResString(@sInvalidParamsErrorMessage));
        Exit;
      end;

      vIntfs := TIntfs.Create(True);
      try

        Parsing_u.Parsing(ParamStr(1), vIntfs, vUnitName);

        vIntfs.RefreshLinks;

        vCnsSettings := TConvertationSettings.Create(vUnitName);
        try
          vCnsSettings.LoadFromFile(ParamStr(3));

          if not vCnsSettings.CompileCode(S) then
          begin
            CnvWriteln(S);
            Exit;
          end;

          StringToFile(ParamStr(2), RawByteString(IDLEncoding(vIntfs, vCnsSettings)));

        finally
          FreeAndNil(vCnsSettings);
        end;

      finally
        FreeAndNil(vIntfs);
      end;

    except
      on E: Exception do
      begin
        ShowExceptions(E);
        ExitCode := 1;
      end;
    end;

  finally
    JclStopExceptionTracking;

//    ReadLn(a);
  end;

END.
