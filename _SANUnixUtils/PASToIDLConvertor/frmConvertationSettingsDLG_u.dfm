object frmConvertationSettingsDLG: TfrmConvertationSettingsDLG
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'frmConvertationSettingsDLG'
  ClientHeight = 353
  ClientWidth = 508
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 312
    Width = 508
    Height = 41
    Align = alBottom
    Caption = 'pnlBottom'
    ShowCaption = False
    TabOrder = 0
    ExplicitLeft = 136
    ExplicitTop = 176
    ExplicitWidth = 185
    DesignSize = (
      508
      41)
    object btnOK: TBitBtn
      Left = 343
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1050
      DoubleBuffered = True
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333333330000333333333333333333F3333333330000333303333333
        33333F3F3333333300003330003333333333F333F33333330000330000033333
        333F33333F33333300003000300033333373333333F333330000300333000333
        337333F7333F33330000333333300033333777337333F3330000333333330003
        3333333337333F33000033333333300033333333337333F30000333333333300
        3333333333373373000033333333333333333333333377330000333333333333
        33333333333333330000}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 0
      ExplicitLeft = 372
    end
    object btnCancel: TBitBtn
      Left = 424
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1090#1084#1077#1085#1072
      DoubleBuffered = True
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333000033333333333333333FF33333FF330000333003333300
        3333733F333733F3000033300033300033337333F37333F30000333300030003
        3333373337333F330000333330000033333333733333F3330000333333000333
        33333337333F33330000333330000033333333733333F3330000333300030003
        3333373337333F33000033300033300033337333F37333F30000333003333300
        3333733F333733F3000033333333333333333773333377330000333333333333
        33333333333333330000}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 1
      ExplicitLeft = 453
    end
  end
  object pnlClient: TPanel
    Left = 0
    Top = 0
    Width = 508
    Height = 312
    Align = alClient
    Caption = 'pnlClient'
    Padding.Left = 7
    Padding.Right = 7
    Padding.Bottom = 7
    ShowCaption = False
    TabOrder = 1
    ExplicitLeft = 128
    ExplicitTop = 136
    ExplicitWidth = 185
    ExplicitHeight = 41
    object gbMain: TGroupBox
      Left = 8
      Top = 1
      Width = 492
      Height = 71
      Align = alTop
      Caption = #1054#1089#1085#1086#1074#1085#1099#1077
      TabOrder = 0
      object lblTypeLibGUID: TLabel
        Left = 16
        Top = 16
        Width = 128
        Height = 13
        Caption = 'GUID '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1080' '#1090#1080#1087#1086#1074': '
      end
      object lblTypeLibName: TLabel
        Left = 16
        Top = 45
        Width = 122
        Height = 13
        Caption = #1048#1084#1103' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1080' '#1090#1080#1087#1086#1074': '
      end
      object sbtnTypeLibGUID: TSpeedButton
        Left = 462
        Top = 12
        Width = 25
        Height = 24
        Hint = #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
        Glyph.Data = {
          46010000424D460100000000000076000000280000001C0000000D0000000100
          040000000000D000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333FFFFF3333000033333707333333333FF3337733330000333700733333
          3333733337333333000033300733333333337333733333330000337073333333
          3337333F3333333300003300333333333337333F3FFFFFF30000330033300000
          3337333F733333F300003300733370003337333F373333F30000337007370000
          33373333F33333F3000033300000007033337333333373F30000333370007330
          33333733333773F3000033333333333333333377777337330000333333333333
          33333333333333330000}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = sbtnTypeLibGUIDClick
      end
      object medtTypeLibGUID: TMaskEdit
        Left = 150
        Top = 12
        Width = 311
        Height = 24
        CharCase = ecUpperCase
        EditMask = '{AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA};1;_'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 38
        ParentFont = False
        TabOrder = 0
        Text = '{00000000-0000-0000-0000-000000000000}'
        OnKeyPress = medtTypeLibGUIDKeyPress
      end
      object edtTypeLibName: TEdit
        Left = 150
        Top = 40
        Width = 337
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = 'edtTypeLibName'
      end
    end
    object gbIntfTrfList: TGroupBox
      Left = 8
      Top = 72
      Width = 492
      Height = 232
      Align = alClient
      Caption = #1058#1088#1072#1085#1089#1092#1086#1088#1084#1072#1094#1080#1103' '#1080#1085#1090#1077#1088#1092#1077#1081#1089#1086#1074
      Padding.Left = 7
      Padding.Right = 7
      Padding.Bottom = 7
      TabOrder = 1
      ExplicitLeft = 96
      ExplicitTop = 136
      ExplicitWidth = 185
      ExplicitHeight = 105
      object vstIntfTrf: TVirtualStringTree
        Left = 9
        Top = 15
        Width = 474
        Height = 208
        Align = alClient
        Header.AutoSizeIndex = 1
        Header.DefaultHeight = 22
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Height = 22
        Header.Options = [hoAutoResize, hoColumnResize, hoDrag, hoShowSortGlyphs, hoVisible]
        TabOrder = 0
        ExplicitHeight = 144
        Columns = <
          item
            Position = 0
            Width = 200
            WideText = #1048#1085#1090#1077#1088#1092#1077#1081#1089' '#1076#1086' '#1082#1086#1085#1074#1077#1088#1090#1072#1094#1080#1080
          end
          item
            Position = 1
            Width = 274
            WideText = #1048#1085#1090#1077#1088#1092#1077#1081#1089' '#1087#1086#1089#1083#1077' '#1082#1086#1085#1074#1077#1088#1090#1072#1094#1080#1080
          end>
      end
    end
  end
end
