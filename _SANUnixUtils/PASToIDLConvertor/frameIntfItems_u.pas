unit frameIntfItems_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, VirtualTrees, ImgList,
  PasToIDlTypes_u;

type
  TframeIntfItems = class(TFrame, ICurIntfClient)
    vstIntfItems: TVirtualStringTree;
    ilIntfsItems: TImageList;

    procedure vstIntfItemsGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure vstIntfItemsGetImageIndexEx(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
      var Ghosted: Boolean; var ImageIndex: Integer;
      var ImageList: TCustomImageList);
    procedure vstIntfItemsFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);

  private
    function GetIntf: TIntf;

  protected
    //ICurIntfClient
    procedure CurIntfUpdated;

  public
    property Intf : TIntf  read GetIntf;
  end;

implementation

{$R *.dfm}
//==============================================================================
// TframeIntfItems
// private
function TframeIntfItems.GetIntf: TIntf;
begin
  Result := (Owner as ICurIntfController).CurIntf;
end;

// protected
procedure TframeIntfItems.CurIntfUpdated;
var
  vIntf : TIntf;
begin
  vIntf := Intf;

  if Assigned(vIntf) then
    vstIntfItems.RootNodeCount := vIntf.Items.Count
  else
    vstIntfItems.RootNodeCount := 0;

  vstIntfItems.Repaint;
end;

// published
procedure TframeIntfItems.vstIntfItemsGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: string);
var
  vIntfItem : TCustomIntfItem;
begin
  vIntfItem := Intf.Items[Node^.Index];

  case Column of
    0:
      if vIntfItem is TPropertyIntfItem then
        CellText := 'property'
      else
        if (vIntfItem as TMethodIntfItem).IsFunction then
          CellText := 'function'
        else
          CellText := 'procedure';

    1:
      CellText := vIntfItem.Name;

    2:
      CellText := vIntfItem.Params.AsText;

    3:
      if vIntfItem is TPropertyIntfItem then
        CellText := (vIntfItem as TPropertyIntfItem).DataType
      else
        with vIntfItem as TMethodIntfItem do
          if IsFunction then
            CellText := ResultDataType
          else
            CellText := '—';

    4:
      if vIntfItem is TPropertyIntfItem then
        CellText := (vIntfItem as TPropertyIntfItem).Reader
      else
        CellText := '—';

    5:
      if (vIntfItem is TPropertyIntfItem) and ((vIntfItem as TPropertyIntfItem).Writer <> '') then
        CellText := (vIntfItem as TPropertyIntfItem).Writer
      else
        CellText := '—';

    6:
      if vIntfItem is TPropertyIntfItem then
        CellText := BoolToStr( (vIntfItem as TPropertyIntfItem).IsDefault, True)
      else
        CellText := '—';

  end;

end;

procedure TframeIntfItems.vstIntfItemsGetImageIndexEx(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
  var Ghosted: Boolean; var ImageIndex: Integer;
  var ImageList: TCustomImageList);
var
  vIntfItem : TCustomIntfItem;
begin

  if Column > 0 then
  begin
    ImageIndex := -1;
    Exit;
  end;


  ImageList := ilIntfsItems;

  vIntfItem := Intf.Items[Node^.Index];

  if vIntfItem is TPropertyIntfItem then
    ImageIndex := 1
  else
    ImageIndex := 0;
end;

procedure TframeIntfItems.vstIntfItemsFocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
begin
  Sender.Selected[Node] := True;
end;
//==============================================================================

end.
