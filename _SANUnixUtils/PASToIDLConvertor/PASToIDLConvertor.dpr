program PASToIDLConvertor;

uses
  FastMM4,
  Forms,
  frmPAStoIDLMain_u in 'frmPAStoIDLMain_u.pas' {frmPAStoIDLMain},
  DM_u in 'DM_u.pas' {DM: TDataModule},
  PasToIDlTypes_u in 'PasToIDlTypes_u.pas',
  frameIntfItems_u in 'frameIntfItems_u.pas' {frameIntfItems: TFrame},
  Parsing_u in 'Parsing_u.pas',
  IDLEncoding_u in 'IDLEncoding_u.pas',
  IDLConsts_u in 'IDLConsts_u.pas',
  frmConvertationSettingsDLG_u in 'frmConvertationSettingsDLG_u.pas' {frmConvertationSettingsDLG};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmPAStoIDLMain, frmPAStoIDLMain);
  Application.Run;
end.
