UNIT IDLEncoding_u;

INTERFACE

USES
  SysUtils, Classes, StrUtils,
  SANMisc,
  PasToIDlTypes_u, IDLConsts_u;

TYPE
  EIDLEncodingError = class(Exception);

function IDLEncoding(const AIntfs : TIntfs; const ASettings : TConvertationSettings) : string;

IMPLEMENTATION

type
  TIDLEncodingParams = record
    Intfs : TIntfs;
    IDs : TStrings;
    Settings : TConvertationSettings;
  end;

function IDLReplaceDataType(const AEncParams : TIDLEncodingParams; const AParamName, ADataType : string) : string;
begin

  if MatchText(ADataType, ['IInterface', 'IUnknown', 'IDispatch'])
  or AEncParams.Intfs.ExistsByName(ADataType)
  then
    Result := AEncParams.Settings.ConvertInterfaceName(ADataType) + '*'

  else
  if MatchText(ADataType, ['integer']) then
    Result := 'long'

  else
  if MatchText(ADataType, ['LongWord']) then
    Result := 'unsigned long'

  else
  if MatchText(ADataType, ['boolean', 'wordbool']) then
    Result := 'VARIANT_BOOL'

  else
  if MatchText(ADataType, ['WideString']) then
    Result := 'BSTR'

  else
  if MatchText(ADataType, ['TDateTime', 'TDate', 'TTime']) then
    Result := 'DATE'

  else
  if MatchText(ADataType, ['OleVariant']) then
    Result := 'VARIANT'

  else
  if AEncParams.Settings.ConvertDataType(AParamName, ADataType, Result) then


  else
    raise EIDLEncodingError.CreateFmt('Тип данных %s не поддерживается', [ADataType]);

end;

function IDLIntfItemParam(const AEncParams : TIDLEncodingParams; const AParam : TIntfItemParam) : string;
var
  vModes : TIDLParamsModes;
begin

  if AParam.Mode in [pmValue, pmConst] then
    vModes := [ipmIn]

  else
  if AParam.Mode = pmVar then
    vModes := [ipmIn, ipmOut]

  else
  if AParam.Mode = pmOut then
    vModes := [ipmOut]

  else
    raise EIDLEncodingError.Create('Недопустимый модификатор параметра.');

  Result := IDL_PARAM
            (
              vModes,
              IDLReplaceDataType(AEncParams, AParam.Name, AParam.DataType) + IfThen(ipmOut in vModes, '*'),
              AParam.Name
            );
end;

function IDLRetValParam(const AEncParams : TIDLEncodingParams; const AResultDataType : string) : string;
const
  RESULT_NAME = 'ResultRetVal';
begin
  Result := IDL_PARAM([ipmOut, ipmRetVal], IDLReplaceDataType(AEncParams, RESULT_NAME, AResultDataType) + '*', RESULT_NAME);
end;

function IDLIntfItemParams(const AEncParams : TIDLEncodingParams;
  const AParams : TIntfItemParams;
  const AHasResult : boolean; const AResultDataType : string) : string;
var
  i : integer;
begin

  if (AParams.Count = 0) then

    if AHasResult then
      Result := IDLRetValParam(AEncParams, AResultDataType)
    else
      Result := 'void'

  else
    begin
      Result := IDLIntfItemParam(AEncParams, AParams.First);

      for i := 1 to AParams.Count - 1 do
        Result := Result + ', ' + IDLIntfItemParam(AEncParams, AParams[i]);

      if AHasResult then
        Result := Result + ', ' + IDLRetValParam(AEncParams, AResultDataType);
    end;

end;

function IDLIntfItems(const AEncParams : TIDLEncodingParams; const AIntf : TIntf) : string;
type
  TMPFunc = function (const AID : integer; const AName, AParams : string) : string;

  procedure AddMtdOrProp(const AFunc : TMPFunc; const AID : integer; const AName : string;
    const AMethodItem : TMethodIntfItem);
  begin
    Result := Result
            + AFunc
              (
                AID,
                AEncParams.Settings.ConvertMethodName(AIntf.Name, AName),
                IDLIntfItemParams
                (
                  AEncParams,
                  AMethodItem.Params,
                  AMethodItem.IsFunction,
                  AMethodItem.ResultDataType
                )
              );
  end;

var
  vIDCounter : integer;

  procedure LoadLastID;
  var
    vParentIndex : integer;
  begin

    vParentIndex := AEncParams.IDs.IndexOf(AIntf.ParentName);
    if vParentIndex < 0 then
      vIDCounter := 200
    else
      vIDCounter := Integer(AEncParams.IDs.Objects[vParentIndex]);

  end;

  procedure SaveLastID;
  begin
    AEncParams.IDs.AddObject(AIntf.Name, TObject(vIDCounter));
  end;

  function NextID : integer;
  begin
    Result := vIDCounter;
    Inc(vIDCounter);
  end;

var
  i, vID : integer;

begin
  Result := '';

  LoadLastID;
  try

    for i := 0 to AIntf.Items.Count - 1 do
      if AIntf.Items[i] is TMethodIntfItem then
        with AIntf.Items[i] as TMethodIntfItem do
        begin

          if Assigned(PropLink) then
            Continue;

          AddMtdOrProp(@IDL_METHOD, NextID, Name, AIntf.Items[i] as TMethodIntfItem);
        end

      else
      if AIntf.Items[i] is TPropertyIntfItem then
        with AIntf.Items[i] as TPropertyIntfItem do
        begin
          vID := NextID;

          if Assigned(ReaderLink) then
            AddMtdOrProp(@IDL_PROPGET, vID, Name, ReaderLink);

          if Assigned(WriterLink) then
            AddMtdOrProp(@IDL_PROPPUT, vID, Name, WriterLink);
        end

      else
        raise EIDLEncodingError.Create('Некорректный класс элемента интерфейса');


  finally
    SaveLastID;
  end;

end;

function IDLShortDecl(const AEncParams : TIDLEncodingParams) : string;
var
  vIntf : TIntf;
begin
  Result := '';

  for vIntf in AEncParams.Intfs do
    Result := Result + IDL_SHORT_DECL_INTF( AEncParams.Settings.ConvertInterfaceName(vIntf.Name) );
end;

function IDLFullDecl(const AEncParams : TIDLEncodingParams) : string;
var
  vIntf : TIntf;
begin
  Result := '';

  for vIntf in AEncParams.Intfs do
    Result := Result
            + IDL_FULL_DECL_INTF
              (
                AEncParams.Settings.ConvertGUID(vIntf.Name, vIntf.GUID),
                vIntf.HelpStr,
                AEncParams.Settings.ConvertInterfaceName(vIntf.Name),
                AEncParams.Settings.ConvertParentInterfaceName(vIntf.Name, vIntf.ParentName),
                StrShift
                (
                  IDLIntfItems(AEncParams, vIntf),
                  2
                )
              );
end;

function IDLEncoding(const AIntfs : TIntfs; const ASettings : TConvertationSettings) : string;
var
  vEncParams : TIDLEncodingParams;
begin
  vEncParams.IDs := TStringList.Create;
  try
    vEncParams.Intfs := AIntfs;
    vEncParams.Settings := ASettings;

    Result := IDL_BASE
    (
      GUIDToString(ASettings.TypeLibGUID),
      ASettings.TypeLibName,
      StrShift(IDLShortDecl(vEncParams), 2),
      StrShift(IDLFullDecl(vEncParams), 2)
    );

  finally
    FreeAndNil(vEncParams.IDs);
  end;
end;

END.
