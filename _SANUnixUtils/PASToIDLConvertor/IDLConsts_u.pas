// Do not delete or modify specific comments SRSC_XXX_Beg, SRSC_XXX_End.
// The code between these comments are automatically recreated when you save.
// To write a specific function code:
//   1. Write function declaration after SRSC_Intf_End.
//   2. Write function code after the SRSC_Impl_End.
//   3. Disable the automatic generation of code for that function.

UNIT IDLConsts_u;
INTERFACE

//SRSC_Dll_Beg
//SRSC_Dll_End

USES
  Windows, SysUtils, TypInfo, StrUtils;

TYPE
  TIDLParamsMode = (ipmIn, ipmOut, ipmRetVal, ipmOptional, ipmLCID, ipmHasDefault);
  TIDLParamsModes = set of TIDLParamsMode;

//SRSC_Intf_Beg
function IDL_BASE(const ATypeLibGUID, ATypeLibName, AShortDecl, AFullDecl : string) : string;
function IDL_FULL_DECL_INTF(const AIntfGUID, AIntfHlpStr, AIntfName, AIntfParentName, AIntfItems : string) : string;
function IDL_METHOD(const AID : integer; const AName, AParams : string) : string;
function IDL_PROPGET(const AID : integer; const AName, AParams : string) : string;
function IDL_PROPPUT(const AID : integer; const AName, AParams : string) : string;
function IDL_SHORT_DECL_INTF(const AIntfName : string) : string;
//SRSC_Intf_End

function IDL_PARAM(const AModes : TIDLParamsModes; const ADataType, AName : string) : string;

IMPLEMENTATION

{$IFDEF SRSC_IN_DLL}
var vHDLL : THandle;
{$ELSE}
{$R IDLConsts.res}
{$ENDIF}

function ConstFromRes(const AResName: string) : string;
var
  vText : AnsiString;
  vResInstance : THandle;
  HRes : HRSRC;
begin
  vResInstance := FindResourceHinstance(
{$IFDEF SRSC_IN_DLL}
    vHDLL
{$ELSE}
    hInstance
{$ENDIF}
  );

  HRes := FindResource(vResInstance, PWideChar(AResName), RT_RCDATA);


  if (HRes = 0) then  begin
    Result := '';
	  Exit;
  end;//if

  vText := PAnsiChar(
    LockResource(
      LoadResource(vResInstance, HRes)
    )
  );

  SetLength(vText, SizeOfResource(vResInstance, HRes));

  Result := String(vText);
end;

//SRSC_Impl_Beg
function IDL_BASE(const ATypeLibGUID, ATypeLibName, AShortDecl, AFullDecl : string) : string;
begin
  Result := Format(ConstFromRes('IDLConstsIDL_BASE'), [ATypeLibGUID, ATypeLibName, AShortDecl, AFullDecl]);
end;

function IDL_FULL_DECL_INTF(const AIntfGUID, AIntfHlpStr, AIntfName, AIntfParentName, AIntfItems : string) : string;
begin
  Result := Format(ConstFromRes('IDLConstsIDL_FULL_DECL_INTF'), [AIntfGUID, AIntfHlpStr, AIntfName, AIntfParentName, AIntfItems]);
end;

function IDL_METHOD(const AID : integer; const AName, AParams : string) : string;
begin
  Result := Format(ConstFromRes('IDLConstsIDL_METHOD'), [IntToHex(AID, 8), AName, AParams]);
end;

function IDL_PROPGET(const AID : integer; const AName, AParams : string) : string;
begin
  Result := Format(ConstFromRes('IDLConstsIDL_PROPGET'), [IntToHex(AID, 8), AName, AParams]);
end;

function IDL_PROPPUT(const AID : integer; const AName, AParams : string) : string;
begin
  Result := Format(ConstFromRes('IDLConstsIDL_PROPPUT'), [IntToHex(AID, 8), AName, AParams]);
end;

function IDL_SHORT_DECL_INTF(const AIntfName : string) : string;
begin
  Result := Format(ConstFromRes('IDLConstsIDL_SHORT_DECL_INTF'), [AIntfName]);
end;
//SRSC_Impl_End

function IDL_PARAM(const AModes : TIDLParamsModes; const ADataType, AName : string) : string;
var
  vMode : string;
  vPM : TIDLParamsMode;
begin

  vMode := '';
  for vPM := Low(TIDLParamsMode) to High(TIDLParamsMode) do
    if vPM in AModes then
      vMode := vMode
             + IfThen(vMode <> '', ', ')
             + Copy
               (
                 GetEnumName
                 (
                   TypeInfo(TIDLParamsMode),
                   Ord(vPM)
                 ),
                 4,
                 MaxInt
               );

  Result := Format(ConstFromRes('IDLConstsIDL_PARAM'), [LowerCase(vMode), ADataType, AName]);
end;

{$IFDEF SRSC_IN_DLL}
INITIALIZATION
  vHDLL := LoadLibrary('IDLConsts.dll');
FINALIZATION
  FreeLibrary(vHDLL);
{$ENDIF}
END.



