unit PAStoIDLMessages;

interface

resourcestring
  sHeaderMessage               = 'Љ®­ўҐав жЁп Ё­вҐадҐ©б®ў, ®ЇЁб ­­ле ў PAS-д ©«Ґ ў RIDL-д ©«,'#13#10
                               + '¤«п б®§¤ ­Ёп ЎЁЎ«Ё®вҐЄЁ вЁЇ®ў.'#13#10 
                               + #13#10 
                               + '‘в ­ж® Ђ.‚., ­®пЎам 2012'#13#10
                               + #13#10;
              
  sParamsMessage               = 'PASToIDLCnv.exe PasFileName RIDLFileName ConfigFileName'#13#10 
                               + '   PASFileName     - Ё¬п PAS-д ©«  б ®ЇЁб ­ЁҐ¬ Ё­вҐадҐ©б®ў.'#13#10 
                               + '   RIDLFileName    - Ё¬п б®§¤ ў Ґ¬®Ј® RIDL-д ©« .'#13#10 
                               + '   ConfigFileName  - Ё¬п д ©«  ­ бва®ҐЄ Є®­ўҐав жЁЁ.'#13#10
                               + #13#10 
                               + '   ‘вагЄвга  INI-д ©«  ­ бва®ҐЄ:'#13#10
                               + '     ђ §¤Ґ« Main:'#13#10
                               + '       ‘®¤Ґа¦Ёв ®б­®ў­лҐ Ї а ¬Ґвал.'#13#10
                               + '       TypeLibName  - Ё¬п ЎЁЎ«Ё®вҐЄЁ вЁЇ®ў, Є®в®а®Ґ ¬®¦Ґв б®¤Ґа¦ вм Ї а ¬Ґва %s.'#13#10
                               + '                      %s § ¬Ґ­пҐвбп ­  Ё¬п ¬®¤г«п Ё§ PASFileName.'#13#10
                               + '       TypeLibGUID  - GUID ЎЁЎ«Ё®вҐЄЁ вЁЇ®ў.'#13#10
                               + #13#10 
                               + '     ђ §¤Ґ« Interfaces (®ЇжЁ®­ «м­®):'#13#10
                               + '       ‘®¤Ґа¦Ёв Ї ал Ё­вҐадҐ©б®ў, а §¤Ґ«Ґ­­лҐ §­ Є®¬ а ўҐ­бвў . ‹Ґўл© Ё­вҐадҐ©б Ўг¤Ґв § ¬Ґ­Ґ­ ­  Їа ўл©.'#13#10
                               + '       ЏаЁ¬Ґа:'#13#10
                               + '         IInterface=IUnknown  - ўбҐ Ё­вҐадҐ©бл IInterface § ¬Ґ­Ёвм ­  IUnknown.'#13#10
                               + #13#10 
                               + '     ђ §¤Ґ« Code (®ЇжЁ®­ «м­®):'#13#10
                               + '       ‘®¤Ґа¦Ёв Їа®жҐ¤гал Ё дг­ЄжЁЁ ­  п§лЄҐ Pascal Script, Ї®§ў®«пойЁҐ ЈЁЎЄ® гЇа ў«пвм Є®­ўҐав жЁҐ©.'#13#10
                               + #13#10 
                               + '       procedure ConvertInterfaceName(const AInIntfsList : boolean; const APasInterface : string; var AIdlInterface : string);'#13#10
                               + '         ‚л§лў Ґвбп ¤«п Є ¦¤®Ј® Ё§ Ё­вҐадҐ©б®ў Ё Ї®§ў®«пҐв § ¤ вм ҐЈ® Ё¬п Ї®б«Ґ Є®­ўҐав жЁЁ.'#13#10
                               + '         „«п Ё¬Ґ­Ё а®¤ЁвҐ«мбЄ®Ј® Ё­вҐадҐ©б  ЁбЇ®«м§гҐвбп ®в¤Ґ«м­ п Їа®жҐ¤га  ConvertParentInterfaceName.'#13#10
                               + '         AInIntfsList   - Ї®Є §лў Ґв, Ўл«  «Ё Їа®Ё§ўҐ¤Ґ­  § ¬Ґ­  § бзҐв бҐЄжЁЁ Interfaces.'#13#10
                               + '         APasInterface  - ®аЁЈЁ­ «м­®Ґ Ё¬п.'#13#10
                               + '         AIdlInterface  - Ё¬п Ї®б«Ґ Є®­ўҐав жЁЁ.'#13#10
                               + #13#10 
                               + '       procedure ConvertParentInterfaceName(const AInIntfsList : boolean; const APasInterface, APasParentInterface : string; var AIdlParentInterface : string);'#13#10
                               + '         ‚л§лў Ґвбп ¤«п Є ¦¤®Ј® Ё§ Ё­вҐадҐ©б®ў-а®¤ЁвҐ«Ґ© Ё Ї®§ў®«пҐв § ¤ вм ҐЈ® Ё¬п.'#13#10
                               + '         AInIntfsList         - Ї®Є §лў Ґв, Ўл«  «Ё Їа®Ё§ўҐ¤Ґ­  § ¬Ґ­  § бзҐв бҐЄжЁЁ Interfaces.'#13#10
                               + '         APasInterface        - Ё¬п Ё­вҐадҐ©б , ¤«п Є®в®а®Ј® ®ЇаҐ¤Ґ«пҐвбп а®¤ЁвҐ«мбЄЁ© Ё­вҐадҐ©б.'#13#10
                               + '         APasParentInterface  - ®аЁЈЁ­ «м­®Ґ Ё¬п.'#13#10
                               + '         AIdlParentInterface  - Ё¬п Ї®б«Ґ Є®­ўҐав жЁЁ.'#13#10
                               + #13#10 
                               + '       procedure ConvertGUID(const APasInterface, APasGUID : string; var AIDlGUID : string);'#13#10
                               + '         ‚л§лў Ґвбп ¤«п Є ¦¤®Ј® GUID Ё­вҐадҐ©б  Ё Ї®§ў®«пҐв § ¤ вм §­ зҐ­ЁҐ GUID.'#13#10
                               + '         APasInterface  - Ё­вҐадҐ©б, ¤«п Є®в®а®Ј® § ¤ Ґвбп GUID.'#13#10
                               + '         APasGUID       - ®аЁЈЁ­ «м­л© GUID.'#13#10
                               + '         AIDlGUID       - GUID Ї®б«Ґ Є®­ўҐав жЁЁ.'#13#10
                               + #13#10 
                               + '       procedure ConvertMethodName(const APasInterface, AMethodName : string; var ACnvMethodName : string);'#13#10
                               + '         ‚л§лў Ґвбп ¤«п Є ¦¤®Ј® ¬Ґв®¤  Ё­вҐадҐ©б  Ё Ї®§ў®«пҐв Ё§¬Ґ­Ёвм ҐЈ® Ё¬п.'#13#10
                               + '         APasInterface   - Ё­вҐадҐ©б, ¤«п Є®в®а®Ј® § ¤ Ґвбп Ё¬п ¬Ґв®¤ .'#13#10
                               + '         AMethodName     - ®аЁЈЁ­ «м­®Ґ Ё¬п ¬Ґв®¤ .'#13#10
                               + '         ACnvMethodName  - Ё¬п ¬Ґв®¤  Ї®б«Ґ Є®­ўҐав жЁЁ.'#13#10
                               + #13#10 
                               + '       function ConvertDataType(const AParamName, ADataType : string; var ACnvDataType : string) : boolean;'#13#10
                               + '         ‚л§лў Ґвбп ¤«п Є ¦¤®Ј® Ё§ вЁЇ®ў ¤ ­­ле Ї а ¬Ґва®ў Ё аҐ§г«мв в®ў дг­ЄжЁ©'#13#10
                               + '         ў б«гз Ґ, Ґб«Ё Їа®Ја ¬¬  ­Ґ §­ Ґв, Є Є®¬г вЁЇг IDL б®®вўҐвбвўгҐв ¤ ­­л© вЁЇ Ї бЄ «п.'#13#10
                               + '         ЌҐ ўл§лў Ґвбп ¤«п Ё­вҐадҐ©б­ле вЁЇ®ў ¤ ­­ле (ўбҐ Ё­вҐадҐ©б­лҐ Ї а ¬Ґвал ®Ўа Ў влў овбп Їа®жҐ¤га®© ConvertInterfaceName).'#13#10
                               + '         …б«Ё дг­ЄжЁп ­Ґ ®Ўа Ў®в «  вЁЇ Ё ў®§ўа й Ґв False, ў®§­ЁЄ Ґв ЁбЄ«озҐ­ЁҐ.'#13#10
                               + '         AParamName    - Ё¬п Ї а ¬Ґва , ¤«п Є®в®а®Ј® ЇаҐ®Ўа §гҐвбп вЁЇ.'#13#10
                               + '         ADataType     - §­ зҐ­ЁҐ вЁЇ  ¤«п ЇаҐ®Ўа §®ў ­Ёп.'#13#10
                               + '         ACnvDataType  - ЇаҐ®Ўа §®ў ­­®Ґ §­ зҐ­ЁҐ.'#13#10
                               ;                 

  sInvalidParamsErrorMessage   = 'ЌҐўҐа­®Ґ зЁб«® Ї а ¬Ґва®ў. €бЇ®«м§г©вҐ ®ЇжЁо /?.';               
  
implementation

end.
