unit frmPAStoIDLMain_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ActnList, Buttons, ComCtrls, VirtualTrees,
  ImgList, ExtCtrls,
  JCLDebug,
  JCLFileUtils,
  JvExMask, JvToolEdit,
  PasToIDlTypes_u, DM_u, frameIntfItems_u;

type
  TAction = class(ActnList.TAction)
  public
    function Execute: Boolean; override;
  end;

  TfrmPAStoIDLMain = class(TForm, IMainCaptionUnitName, ICurIntfController)
    edtPasFileName: TJvFilenameEdit;
    gbMainOptions: TGroupBox;
    lblPasFileName: TLabel;
    lblIDLFileName: TLabel;
    edtIDLFileName: TJvFilenameEdit;
    btnParsePas: TBitBtn;
    actlst: TActionList;
    actParsePas: TAction;
    statMain: TStatusBar;
    ilIntfs: TImageList;
    gbIntfsList: TGroupBox;
    vstIntfs: TVirtualStringTree;
    gbIntfItems: TGroupBox;
    frameIntfItems: TframeIntfItems;
    splIntfItems: TSplitter;
    btnCreateIdl: TBitBtn;
    actCreateIdl: TAction;
    pnlCnvIntfEditer: TPanel;
    lstCnvIntfEditer: TListBox;
    chkCnvIntfEditer: TCheckBox;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure lstCnvIntfEditerDblClick(Sender: TObject);

    procedure vstIntfsGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure vstIntfsGetImageIndexEx(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
      var Ghosted: Boolean; var ImageIndex: Integer;
      var ImageList: TCustomImageList);
    procedure vstIntfsFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex);
    procedure vstIntfsEditing(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; var Allowed: Boolean);
    procedure vstIntfsCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; out EditLink: IVTEditLink);
    procedure vstIntfsDblClick(Sender: TObject);

    procedure actParsePasExecute(Sender: TObject);
    procedure actParsePasUpdate(Sender: TObject);
    procedure actCreateIdlExecute(Sender: TObject);
    procedure actCreateIdlUpdate(Sender: TObject);
    procedure lstCnvIntfEditerKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure chkCnvIntfEditerKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private

  protected
    // IMainCaptionUnitName
    procedure MainCaption(const ACnvUnitName : string);

    // ICurIntfController
    function CurIntf : TIntf;

  public
    procedure ApplicationException(Sender: TObject; E: Exception);

  end;

var
  frmPAStoIDLMain: TfrmPAStoIDLMain;

implementation

{$R *.dfm}

uses
  frmConvertationSettingsDLG_u;

type
  TEditLink = class(TInterfacedObject, IVTEditLink)
  private
    FEdit: TWinControl;
    FTree: TVirtualStringTree;
    FNode: PVirtualNode;
    FColumn: Integer;

  public
    destructor Destroy; override;

    function BeginEdit: Boolean; stdcall;
    function CancelEdit: Boolean; stdcall;
    function EndEdit: Boolean; stdcall;
    function GetBounds: TRect; stdcall;
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; stdcall;
    procedure ProcessMessage(var Message: TMessage); stdcall;
    procedure SetBounds(R: TRect); stdcall;
  end;

//==============================================================================
// TEditLink
// public
destructor TEditLink.Destroy;
begin

  inherited;
end;

function TEditLink.BeginEdit: Boolean;
begin
  Result := True;
  FEdit.Show;

//  if FEdit is TPanel then
//    TWinControl(FEdit.FindChildControl('dtpWtDate')).SetFocus
//  else
  FEdit.SetFocus;
end;

function TEditLink.CancelEdit: Boolean;
begin
  Result := True;
  FEdit.Hide;
end;

function TEditLink.EndEdit: Boolean;
//var
//  vLR : ISANLogRecord;
var
  S, vOld, vOldNew : string;
  i : integer;
begin
  Result := True;

  with FTree.Owner as TfrmPAStoIDLMain do
    case FColumn of
      2:
      begin

        with lstCnvIntfEditer do
          S := Items[ItemIndex];

        if not chkCnvIntfEditer.Checked then
          DM.Intfs[Self.FNode^.Index].NewParentName := S

        else
          begin
            vOld    := DM.Intfs[Self.FNode^.Index].ParentName;
            vOldNew := DM.Intfs[Self.FNode^.Index].NewParentName;

            for i := 0 to DM.Intfs.Count - 1 do
              if (CompareText(DM.Intfs[i].ParentName, vOld) = 0)
              and (CompareText(DM.Intfs[i].NewParentName, vOldNew) = 0)
              then
                DM.Intfs[i].NewParentName := S;
          end;

      end;
    end;

//  vLR := (FTree.Owner as TframeLogRecs).FLogRecords[FNode^.Index];
//
//  with (FTree.Owner as TframeLogRecs) do
//    case FColumn of
//      0: vLR.DateTime  := DateOf(dtpWtDate.DateTime) + TimeOf(dtpWtTime.DateTime);
//      1: vLR.Kind      := cmbxWtKind.ItemIndex;
//      2: vLR.SubType   := edtWtSubType.Text;
//      3: vLR.Msg       := mmWtMessage.Text;
//    end;

  FEdit.Hide;
  FTree.SetFocus;
end;

function TEditLink.GetBounds: TRect;
begin
  Result := FEdit.BoundsRect;
end;

function TEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex): Boolean;
var
  i : integer;
//  vLR : ISANLogRecord;
begin
  Result   := True;
  FTree    := Tree as TVirtualStringTree;
  FNode    := Node;
  FColumn  := Column;

  with (FTree.Owner as TfrmPAStoIDLMain) do
    case Self.FColumn of
      2:
      begin
        Self.FEdit := pnlCnvIntfEditer;

        chkCnvIntfEditer.Checked := False;

        lstCnvIntfEditer.Items.BeginUpdate;
        try
          lstCnvIntfEditer.Items.Clear;

          lstCnvIntfEditer.Items.Add('IUnknown');
          lstCnvIntfEditer.Items.Add('IDispatch');

          DM.Intfs.ExportNewParentName(lstCnvIntfEditer.Items);
//        for i := Low to High do

        finally
          lstCnvIntfEditer.Items.EndUpdate;
        end;

        lstCnvIntfEditer.ItemIndex := lstCnvIntfEditer.Items.IndexOf( DM.Intfs[Self.FNode^.Index].NewParentName )
      end;
    end;

//  vLR := (FTree.Owner as TframeLogRecs).FLogRecords[FNode^.Index];
//
//  with (FTree.Owner as TframeLogRecs) do
//    case Self.FColumn of
//      0:
//      begin
//        Self.FEdit := pnlWtUseDate;
//        dtpWtDate.DateTime := DateOf(vLR.DateTime);
//        dtpWtTime.DateTime := TimeOf(vLR.DateTime);
//      end;
//
//      1:
//      begin
//        Self.FEdit := cmbxWtKind;
//        cmbxWtKind.ItemIndex := vLR.Kind;
//      end;
//
//      2:
//      begin
//        Self.FEdit := edtWtSubType;
//        edtWtSubType.Text := vLR.SubType;
//      end;
//
//      3:
//      begin
//        Self.FEdit := mmWtMessage;
//        mmWtMessage.Text := vLR.Msg;
//      end;
//    end;

end;

procedure TEditLink.ProcessMessage(var Message: TMessage);
begin
  FEdit.WindowProc(Message);
end;

procedure TEditLink.SetBounds(R: TRect);
var
  Dummy: Integer;
begin
  // Since we don't want to activate grid extensions in the tree (this would influence how the selection is drawn)
  // we have to set the edit's width explicitly to the width of the column.
  FTree.Header.Columns.GetColumnBounds(FColumn, Dummy, R.Right);

  OffsetRect(R, FTree.Left, FTree.Top + FTree.Header.Height);

  if FColumn in [2] then
  R.Bottom := R.Top + FEdit.Height;

  FEdit.BoundsRect := R;


end;
//==============================================================================

//==============================================================================
// TAction
// public
function TAction.Execute: Boolean;
var
  vSaveNow : TDateTime;
  vSBMain : TStatusBar;
begin
  vSBMain := (Application.MainForm.FindComponent('statMain') as TStatusBar);
  vSBMain.Panels[0].Text := '';

  vSaveNow := Now;
  try
    Result := inherited Execute;
  finally
    vSBMain.Panels[0].Text := FormatDateTime('hh:nn:ss.zzz', Now - vSaveNow);
  end;
end;
//==============================================================================

//==============================================================================
// TfrmPAStoIDLMain
// protected
procedure TfrmPAStoIDLMain.MainCaption(const ACnvUnitName: string);
begin
  Caption := Format('Приобразование PAS-файла в IDL-файл — [%s]', [ACnvUnitName]) ;

  Application.Title := Caption;
end;

function TfrmPAStoIDLMain.CurIntf: TIntf;
begin
  if vstIntfs.FocusedNode = nil then
    Result := nil
  else
    Result := DM.Intfs[vstIntfs.FocusedNode^.Index];
end;

// public
procedure TfrmPAStoIDLMain.ApplicationException(Sender: TObject; E: Exception);
var
  Str: TStringList;
begin
  Str := TStringList.Create;
  try
    JclLastExceptStackListToStrings(Str, True, True, True, True);
    Str.Insert(0, E.Message);
    Str.Insert(1, '');
    Application.MessageBox(PChar(Str.Text), 'Ошибка', MB_OK or MB_ICONSTOP);
  finally
    FreeAndNil(Str);
  end;
end;

procedure TfrmPAStoIDLMain.FormCreate(Sender: TObject);
begin
  Application.OnException := ApplicationException;
  MainCaption('');
  Application.HintHidePause := 30000;

  edtPasFileName.FileName := DM.Props.PasFileName;
  edtIDLFileName.FileName := DM.Props.IdlFileName;
end;

procedure TfrmPAStoIDLMain.FormDestroy(Sender: TObject);
begin
//
end;

procedure TfrmPAStoIDLMain.lstCnvIntfEditerDblClick(Sender: TObject);
begin
  vstIntfs.EndEditNode;
end;

procedure TfrmPAStoIDLMain.lstCnvIntfEditerKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    vstIntfs.EndEditNode;
end;

procedure TfrmPAStoIDLMain.chkCnvIntfEditerKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    vstIntfs.EndEditNode;
end;

procedure TfrmPAStoIDLMain.vstIntfsGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: string);
var
  vIntf : TIntf;
begin
  vIntf := DM.Intfs[Node^.Index];

  case Column of
    0:  CellText := vIntf.Name;
    1:  CellText := vIntf.ParentName;
    2:  CellText := vIntf.NewParentName;
    3:  CellText := vIntf.GUID;
    4:  CellText := vIntf.Comments;
  end;

end;

procedure TfrmPAStoIDLMain.vstIntfsGetImageIndexEx(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
  var Ghosted: Boolean; var ImageIndex: Integer;
  var ImageList: TCustomImageList);
begin
  ImageList := ilIntfs;

  if Column > 0 then
    ImageIndex := -1
  else
    ImageIndex := 0;
end;

procedure TfrmPAStoIDLMain.vstIntfsFocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
var
  i : integer;
  vCIC : ICurIntfClient;
begin
  Sender.Selected[Node] := True;

  for i := 0 to ComponentCount - 1 do
    if (Components[i] as IUnknown).QueryInterface(ICurIntfClient, vCIC) = S_OK then
      vCIC.CurIntfUpdated;
end;

procedure TfrmPAStoIDLMain.vstIntfsEditing(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := Column = 2;

  if not Allowed then
    Sender.EditNode(Node, 2);
end;

procedure TfrmPAStoIDLMain.vstIntfsCreateEditor(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
begin
  EditLink := TEditLink.Create
end;

procedure TfrmPAStoIDLMain.vstIntfsDblClick(Sender: TObject);
begin
  PostMessage(vstIntfs.Handle, WM_KEYDOWN, VK_F2, 0);
end;

procedure TfrmPAStoIDLMain.actParsePasExecute(Sender: TObject);
var
  vIntf : TIntf;
begin
  DM.Props.PasFileName := edtPasFileName.FileName;

  vstIntfs.Clear;
  DM.Intfs.Clear;

  DM.Parsing;

  with vstIntfs do
  begin
    RootNodeCount := DM.Intfs.Count;

    FocusedNode := RootNode^.FirstChild;

    SetFocus;
  end;
end;

procedure TfrmPAStoIDLMain.actParsePasUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := FileExists(Trim(edtPasFileName.FileName));
end;

procedure TfrmPAStoIDLMain.actCreateIdlExecute(Sender: TObject);
var
  vCnvSettings : TConvertationSettings;
  i : integer;
begin
  vCnvSettings := TConvertationSettings.Create(DM.CnvUnitName);
  try

//    for i := 0 to DM.Intfs.Count - 1 do




    if not ConvertationSettingsDLG((Sender as TAction).Caption, vCnvSettings) then
      Exit;

    DM.Props.IdlFileName := edtIDLFileName.FileName;

    DM.CreateIdl;

  finally
    FreeAndNil(vCnvSettings);
  end;
end;

procedure TfrmPAStoIDLMain.actCreateIdlUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(vstIntfs.RootNode^.FirstChild)
                             and DirectoryExists( ExtractFilePath( Trim( edtIDLFileName.FileName )));
end;
//==============================================================================



initialization
  JclStartExceptionTracking;

finalization
  JclStopExceptionTracking;

END.
