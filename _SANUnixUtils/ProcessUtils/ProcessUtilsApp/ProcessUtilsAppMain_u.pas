unit ProcessUtilsAppMain_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Button2: TButton;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

function ProcessIsRun(const AProcessName : PAnsiChar) : boolean;  stdcall; external 'ProcessUtils.dll';
function KillRunedProcess(const AProcessName : PAnsiChar) : boolean; stdcall; external 'ProcessUtils.dll';

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  Label1.Caption := BoolToStr(ProcessIsRun('GRTaskLauncher.exe'), True);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Label2.Caption := BoolToStr(KillRunedProcess('GRTaskLauncher.exe'), True);
end;

end.
