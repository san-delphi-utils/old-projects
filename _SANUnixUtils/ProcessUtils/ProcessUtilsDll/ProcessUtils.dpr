library ProcessUtils;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Windows,
  TlHelp32;

{$R *.res}
//==============================================================================
function ProcessIsRun(const AProcessName : PAnsiChar) : boolean; stdcall;
var
  vSnapProc : THandle;
  vProcEntry : TProcessEntry32;
begin
  vSnapProc := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if vSnapProc <> INVALID_HANDLE_VALUE then
    try
      vProcEntry.dwSize := SizeOf(TProcessEntry32);

      if Process32First(vSnapProc, vProcEntry) then
        repeat

          if vProcEntry.szExeFile = String(AnsiString(AProcessName)) then  begin
            Result := True;
            Exit;
          end;//t..f

        until not Process32Next(vSnapProc, vProcEntry);

    finally
      CloseHandle(vSnapProc);
    end;//t..f

  Result := False
end;//ProcessIsRun
//==============================================================================
function KillRunedProcess(const AProcessName : PAnsiChar) : boolean; stdcall;
var
  vSnapProc, vProcHandle : THandle;
  vProcEntry : TProcessEntry32;
begin
  Result := False;

  vSnapProc := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if vSnapProc <> INVALID_HANDLE_VALUE then
    try
      vProcEntry.dwSize := SizeOf(TProcessEntry32);

      if Process32First(vSnapProc, vProcEntry) then
        repeat

          if vProcEntry.szExeFile = String(AnsiString(AProcessName)) then  begin

            vProcHandle := OpenProcess(PROCESS_TERMINATE, FALSE, vProcEntry.th32ProcessID);

            if vProcHandle = 0 then
              Exit;

            TerminateProcess(vProcHandle, DWORD(-1));

            Result := True;

            Exit;
          end;//t..f

        until not Process32Next(vSnapProc, vProcEntry);

    finally
      CloseHandle(vSnapProc);
    end;//t..f
end;//ProcessIsRun
//==============================================================================

exports
  ProcessIsRun,
  KillRunedProcess;

BEGIN
END.
