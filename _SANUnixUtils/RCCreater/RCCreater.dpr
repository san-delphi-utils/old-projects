program RCCreater;

{$APPTYPE CONSOLE}

uses
  Windows, SysUtils, StrUtils;

function FullPath(APath : string) : string;
var
  L : DWORD;
  P : PChar;
begin
  P := nil;
  L := GetFullPathName(PChar(APath), 0, nil, P);

  SetLength(Result, L);
  GetFullPathName(PChar(APath), L, PChar(Result), P);

  while Result[L] = #0 do
  begin
    Dec(L);
    SetLength(Result, L);
  end;

end;

var
  vPath, vResType, vTemplate, vRCfile, vFilter, vFn, vExt, S : string;
  vSR : TSearchRec;
  F : Text;
BEGIN
  try

    if ParamCount < 4 then
    begin
      Writeln('Param1:str - Path; Param2:str - ResType; Param3:str - ResNameTemplate; Param4:str - result RC file; [Param5:str - Filter (default *.*)]');
      Exit;
    end;

    vPath      := FullPath( ParamStr(1) );
    vResType   := ParamStr(2);
    vTemplate  := ParamStr(3);
    vRCfile    := FullPath( ParamStr(4) );

    if ParamCount >= 5 then
      vFilter := ParamStr(5)
    else
      vFilter := '*.*';

    if ExtractFileExt(vRCfile) = '' then
      vRCfile := vRCfile + '.RC';


    // ������ � ������
    AssignFile(F, vRCfile);
    Rewrite(F);
    try

      // ����� �� �����
      if FindFirst(vPath + '\' + vFilter, faAnyFile AND (NOT faDirectory), vSR) = 0 then
        try

          repeat

            // �������� ��� ����� � ���������� ��� ����� (".")
            vExt := ExtractFileExt(vSR.Name);
            vFn := vSR.Name;
            SetLength(vFn, Length(vFn) - Length(vExt));
            Delete(vExt, 1, 1);


            // ������� ������ �� �������
            S :=
            UpperCase
            (
              StringReplace
              (

                StringReplace
                (
                  vTemplate,
                  '<ext>',
                  vExt,
                  [rfReplaceAll, rfIgnoreCase]
                ),

                '<fn>',
                vFn,
                [rfReplaceAll, rfIgnoreCase]
              )
            );

           // ������ ������ RCDATA
           S := Format('%s %s "%s"', [S, vResType, vPath + '\' + vSR.Name]);


           // �������� � ����
           Writeln(F, S);

          until FindNext(vSR) > 0;

        finally
          FindClose(vSR);
        end;//t..f

    finally
      CloseFile(F);
    end;//t..f

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;//t..e
END.
