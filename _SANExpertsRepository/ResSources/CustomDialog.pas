UNIT $(ModuleIdent);

INTERFACE

$(Defs)

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

TYPE
  T$(DlgName)Rec = record
    // Dialog params fields
  end;

  T$(FormIdent) = class(TForm)
    pnlClient: TPanel;
    pnlButtons: TPanel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
  private

  protected
    procedure ReadToDLG(var AParams : T$(DlgName)Rec);
    procedure WriteFromDLG(var AParams : T$(DlgName)Rec);

  public

  end;

function $(DlgName)
(
  {$IFDEF USE_CAPTION}const ACaption : string;{$ENDIF}
  {$IFDEF USE_OWNER}const AOwner : TComponent;{$ENDIF}
  var AParams : T$(DlgName)Rec
) : boolean;

IMPLEMENTATION

{$R *.dfm}

function $(DlgName)
(
  {$IFDEF USE_CAPTION}const ACaption : string;{$ENDIF}
  {$IFDEF USE_OWNER}const AOwner : TComponent;{$ENDIF}
  var AParams : T$(DlgName)Rec
) : boolean;
var
  vForm : T$(FormIdent);
begin

  vForm := T$(FormIdent).Create({$IFDEF USE_OWNER}AOwner{$ELSE}nil{$ENDIF});
  with vForm do
    try
      {$IFDEF USE_CAPTION}
	      Caption := ACaption;
      {$ENDIF}

      ReadToDLG(AParams);

      Result := (ShowModal <> mrOk);

      if Result then
        WriteFromDLG(AParams);

    finally
      FreeAndNil(vForm);
    end;

end;

(*
procedure ExampleUse;
var
  vParams : T$(DlgName)Rec;
begin
  vParams... :=;

  if not $(DlgName)('<Caption>', Application.MainForm, vParams) then
    Exit;

  with vParams do
  begin
    // ...
  end;

end;
*)

//==============================================================================
// T$(FormIdent)
// protected
procedure T$(FormIdent).ReadToDLG(var AParams: T$(DlgName)Rec);
begin
  // Init dialog code
end;

procedure T$(FormIdent).WriteFromDLG(var AParams: T$(DlgName)Rec);
begin
  // Write dialog results code
end;
//==============================================================================

END.
