UNIT $(ModuleIdent);

INTERFACE

USES
  SysUtils, Classes, 
  SANDesignVisualisers;
  
TYPE
  // $(VisDesc)
  T$(VisName) = class(TSANCustomVisValueReplacer)
  protected
    function GetVisualizerDescription: String; override;
    function GetVisualizerIdentifier: String; override;
    function GetVisualizerName: String; override;
    function GetReplacementValue(const Expression, TypeName, EvalResult: string): string; override;
    
  public
    constructor Create(const ASupportedTypesFileName : TFileName);
  end;  

IMPLEMENTATION

//==============================================================================
// T$(VisName)
// protected
function T$(VisName).GetVisualizerDescription: String; 
begin
  Result := '$(VisDesc)';
end

function T$(VisName).GetVisualizerIdentifier: String; 
begin
  Result := '$(VisGUID)';
end

function T$(VisName).GetVisualizerName: String; 
begin
  Result := '$(VisName)';
end

function T$(VisName).GetReplacementValue(const Expression, TypeName, EvalResult: string): String; 
begin
  // insert your code here
end

// public
constructor T$(VisName).Create(const ASupportedTypesFileName : TFileName);
begin
  inherited Create(ASupportedTypesFileName);
  
  // Add static supported types here.
  // For example: SupportedTypes.InsertObject(0, 'Integer', TObject(1));
$(VisStaticSupTypes)
end;
//==============================================================================

END.
