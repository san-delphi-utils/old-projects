unit $(ModuleIdent);

interface

uses
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf, SANPkgAsDllCustomDM, SANPkgAsDllClient, SANPkgAsDllClientDM;

type
  T$(FormIdent) = class(TSANPkgAsDllEntryDataModule)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

const
  DLL_ENTRY_POINT_NAME = '$(EntryPointName)';

exports
  GetPkgAsDllEntryPoint name DLL_ENTRY_POINT_NAME;

initialization
  PkgAsDllEntryDataModuleClass := T$(FormIdent);
end.
