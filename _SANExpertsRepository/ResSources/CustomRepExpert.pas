{*******************************************************}
{                                                       }
{       $(ModuleIdent)                                  }
{       ModuleComment.                                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT $(ModuleIdent);
{$I NX.INC}
INTERFACE

USES
  ToolsAPI,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,

  SANMisc, SANINIProps, SANWinUtils,

  SANDesignReplacer, SANDesignCreaters, SANDesignWizards,

  SANExpertsRepCommon,
  SANExpertsRepCustomExpertDialogForm;

TYPE
  T$(FormIdent) = class;

  // Props for saving.
  T$(ExpName)Props = class(TCustomExpertProps)
  private
    function GetForm: T$(FormIdent);

  public
    property Form : T$(FormIdent)  read GetForm;

  published

  end;

  // Dialog form.
  T$(FormIdent) = class(TfrmCustomExpertDialog)
    procedure FormCreate(Sender: TObject);

  private
    function GetProps: T$(ExpName)Props;

  protected
    function GetINIFileName: TFileName; override;
    function ExpertPropsClass : TCustomExpertPropsClass; override;
    procedure DoWarnShapeValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string); override;

  public
    property Props : T$(ExpName)Props  read GetProps;

  end;
  
  // ExpertComment.
  T$(ExpFullName) = class(TSANCustomRepExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetGlyphResName : string; override;
    function DLGFormClass : TFormClass; override;
    procedure DoExecute(const ADLGForm : TForm; const AReplacer : TSANReplacer); override;
  end;
  

IMPLEMENTATION

{$R *.dfm}

const
  ICON_RES_NAME = 'SAN_EXP_REP_ICON_$(ExpNameUp)';

resourcestring
  sExpertCaption = 'Expert Caption';

//==============================================================================
// T$(ExpName)Props
// private
function T$(ExpName)Props.GetForm: T$(FormIdent);
begin
  Result := (inherited Form) as T$(FormIdent);
end;
//==============================================================================

//==============================================================================
// T$(FormIdent)
// private
function T$(FormIdent).GetProps: T$(ExpName)Props;
begin
  Result := (inherited Props) as T$(ExpName)Props;
end;

// protected
function T$(FormIdent).GetINIFileName: TFileName;
begin
  Result := '$(ExpName).ini';
end;

function T$(FormIdent).ExpertPropsClass: TCustomExpertPropsClass;
begin
  Result := T$(ExpName)Props;
end;

procedure T$(FormIdent).DoWarnShapeValueUpdated(const AFrame: TFrame;
  const AControl: TControl; const AValue: Variant; var AWarnHint: string);
begin
  inherited;

end;

// published
procedure T$(FormIdent).FormCreate(Sender: TObject);
begin
  Caption := LoadResString(@sExpertCaption);
  Icon.LoadFromResourceName(HInstance, ICON_RES_NAME);

  inherited;
end;
//==============================================================================

//==============================================================================
// T$(ExpFullName)
// protected
function T$(ExpFullName).GetIDString: string;
begin
  Result := 'SAN.RepExp.$(ExpName)';
end;

function T$(ExpFullName).GetName: string;
begin
  Result := '$(ExpName)';
end;

function T$(ExpFullName).GetComment: string;
begin
  Result := LoadResString(@sExpertCaption);
end;

function T$(ExpFullName).GetGlyphResName: string;
begin
  Result := ICON_RES_NAME;
end;

function T$(ExpFullName).DLGFormClass: TFormClass;
begin
  Result := T$(FormIdent);
end;

procedure T$(ExpFullName).DoExecute(const ADLGForm: TForm;
  const AReplacer: TSANReplacer);
var
  vForm : T$(FormIdent);
  vModule : IOTAModule;
begin
  vForm := T$(FormIdent)(ADLGForm);
(*
  vModule := TSANStaticModuleCreator.CreateModule
  (
    ctForm,
    vForm.frameNames.FullName,
    '',
    vForm.Props.FormNameFmt,
    vForm.Props.UnitNameFmt,
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_CUSTOMDIALOG_DFM'),
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_CUSTOMDIALOG_PAS'),
    AReplacer
  );

  GetActiveProject.AddFile(vModule.FileName, True);

  vModule.Show;
*)  
end;
//==============================================================================

END.
