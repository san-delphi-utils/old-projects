object $(FormIdent): T$(FormIdent)
  GUID = '$(GUID)'
  Caption = $(Caption)
  Description = $(Description)
  URL = $(URL)
  Author = $(Author)
  Version.V1Major = $(V1Major)
  Version.V2Minor = $(V2Minor)
  Version.V3Release = $(V3Release)
  Version.V4Build = $(V4Build)
  Height = 150
  Width = 215
end
