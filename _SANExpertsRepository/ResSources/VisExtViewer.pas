UNIT $(ModuleIdent);

INTERFACE

USES
  ToolsAPI,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs,
  SANDesignEvaluater, SANDesignDockableForm, SANDesignVisualisers;

TYPE
  T$(FormIdent) = class(TSANVisExternalViewerUpdaterFrame)
  private
    
  protected
    procedure DoAvailableStateChanged; override;

    procedure DoRefreshVisualizer(var AParam1, AParam2 : integer); override;

    procedure DoRefreshVisualizerStep(var AParam1, AParam2 : integer; var AContinue : boolean); override;
  
  public
    
  end;
  
  // $(VisDesc)
  T$(VisName) = class(TSANCustomVisExternalViewer)
  protected
    function GetVisualizerDescription: String; override;
    function GetVisualizerIdentifier: String; override;
    function GetVisualizerName: String; override;
    function GetMenuText: String; override;
    function GetViewerUpdaterFrameClass : TSANVisExternalViewerUpdaterFrameClass; override;
    function GetFormCaption(const AExpression, ATypeName, AEvalResult: String) : string; override;
    
  public
    constructor Create(const ASupportedTypesFileName : TFileName);
  end;    

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// T$(FormIdent)
// protected
procedure T$(FormIdent).DoAvailableStateChanged; 
begin
end;

procedure T$(FormIdent).DoRefreshVisualizer(var AParam1, AParam2 : integer); 
begin
end;

procedure T$(FormIdent).DoRefreshVisualizerStep(var AParam1, AParam2 : integer; var AContinue : boolean);
begin
end;
//==============================================================================

//==============================================================================
// T$(VisName)
// protected
function T$(VisName).GetVisualizerDescription: String; 
begin
  Result := '$(VisDesc)';
end

function T$(VisName).GetVisualizerIdentifier: String; 
begin
  Result := '$(VisGUID)';
end

function T$(VisName).GetVisualizerName: String; 
begin
  Result := '$(VisName)';
end

function T$(VisName).GetViewerUpdaterFrameClass : TSANVisExternalViewerUpdaterFrameClass;
begin
  Result := T$(FormIdent);
end;

function T$(VisName).GetFormCaption(const AExpression, ATypeName, AEvalResult: String) : string;
begin
  Result := Format('$(VisCaption)', [AExpression, ATypeName, AEvalResult]);
end

// public
constructor T$(VisName).Create(const ASupportedTypesFileName : TFileName);
begin
  inherited Create(ASupportedTypesFileName);
  
  // Add static supported types here.
  // For example: SupportedTypes.InsertObject(0, 'Integer', TObject(1));
$(VisStaticSupTypes)
end;
//==============================================================================


END.
