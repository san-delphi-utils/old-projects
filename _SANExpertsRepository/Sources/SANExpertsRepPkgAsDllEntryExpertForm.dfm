inherited frmPkgAsDllEntryExpert: TfrmPkgAsDllEntryExpert
  Caption = 'frmPkgAsDllEntryExpert'
  ExplicitWidth = 320
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlClient: TPanel
    inherited gbNames: TGroupBox
      inherited lblDMName: TLabel
        Width = 90
        Caption = #1048#1084#1103' '#1090#1086#1095#1082#1080' '#1074#1093#1086#1076#1072':'
        ExplicitWidth = 90
      end
    end
    inherited gbParams: TGroupBox
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1090#1086#1095#1082#1080' '#1074#1093#1086#1076#1072
      inherited lblGUID: TLabel
        Top = 62
        ExplicitTop = 62
      end
      inherited lblDescription: TLabel
        Top = 131
        ExplicitTop = 131
      end
      inherited lblCaption: TLabel
        Top = 86
        ExplicitTop = 86
      end
      object lblEntryPointName: TLabel [6]
        AlignWithMargins = True
        Left = 8
        Top = 15
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        Caption = #1048#1084#1103' '#1076#1083#1103' '#1101#1082#1089#1087#1086#1088#1090#1072' '#1092#1091#1085#1082#1094#1080#1080' '#1090#1086#1095#1082#1080' '#1074#1093#1086#1076#1072':'
        ExplicitWidth = 208
      end
      inherited mmDescription: TMemo
        Top = 147
        Height = 61
      end
      inherited edtCaption: TEdit
        Top = 102
      end
      inline frameEntryPointName: TframeEntryPointNameShaped
        AlignWithMargins = True
        Left = 8
        Top = 31
        Width = 509
        Height = 23
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 8
        Align = alTop
        TabOrder = 5
        ExplicitLeft = 8
        ExplicitTop = 31
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 494
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 486
          inherited sbtnGenerate: TSpeedButton
            Left = 464
            ExplicitLeft = 464
          end
          inherited edtName: TEdit
            Width = 464
            ExplicitLeft = 0
            ExplicitTop = 1
            ExplicitWidth = 464
          end
        end
      end
    end
  end
end
