inherited frameGUIDShaped: TframeGUIDShaped
  Height = 23
  ExplicitHeight = 23
  inherited shpWarning: TShape
    Height = 23
    ExplicitHeight = 27
  end
  inherited pnlClient: TPanel
    Height = 23
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 502
    ExplicitHeight = 23
    object sbtnGUID: TSpeedButton
      Left = 480
      Top = 0
      Width = 22
      Height = 23
      Hint = #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
      Align = alRight
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333FFFFF3333000033333707333333333FF3337733330000333700733333
        3333733337333333000033300733333333337333733333330000337073333333
        3337333F3333333300003300333333333337333F3FFFFFF30000330033300000
        3337333F733333F300003300733370003337333F373333F30000337007370000
        33373333F33333F3000033300000007033337333333373F30000333370007330
        33333733333773F3000033333333333333333377777337330000333333333333
        33333333333333330000}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = sbtnGUIDClick
      ExplicitLeft = 592
      ExplicitTop = 16
      ExplicitHeight = 22
    end
    object medtGUID: TMaskEdit
      AlignWithMargins = True
      Left = 0
      Top = 1
      Width = 480
      Height = 22
      Margins.Left = 0
      Margins.Top = 1
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alClient
      CharCase = ecUpperCase
      EditMask = '{AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA};1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 38
      ParentFont = False
      TabOrder = 0
      Text = '{00000000-0000-0000-0000-000000000000}'
      OnChange = medtGUIDChange
      OnKeyPress = medtGUIDKeyPress
    end
  end
end
