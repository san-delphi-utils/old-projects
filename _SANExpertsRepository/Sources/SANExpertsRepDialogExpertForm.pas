{*******************************************************}
{                                                       }
{       SANExpertsRepDialogExpertForm                   }
{       Эксперт создания диалогов.                      }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepDialogExpertForm;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Spin, StrUtils, TypInfo,

  SANMisc, SANINIProps, SANWinUtils,

  SANDesignReplacer, SANDesignCreaters, SANDesignWizards,

  SANExpertsRepCommon,
  SANExpertsRepCustomExpertDialogForm,
  SANExpertsRepCustomShapedFrame,
  SANExpertsRepNamesPrefPostShapedFrame,
  SANExpertsRepFormatNameShapedFrame;

TYPE
  TfrmDialogExpert = class;

  TDialogExpertProps = class(TCustomExpertProps)
  private
    FDlgNamePrefix,
    FDlgNamePostfix,
    FFormNameFmt,
    FUnitNameFmt : string;

    FFormBorderStyle : TFormBorderStyle;
    FFormPosition : TPosition;
    FButtonsAlign : TAlign;

    FUseCaption,
    FUseOwner : boolean;

    FPnlClientPadding,
    FPnlButtonsHeight,
    FPnlButtonsPadding,
    FButtonWidth,
    FButtonMargins : integer;

    function GetForm: TfrmDialogExpert;

    procedure SetDlgNamePrefix(const Value: string);
    procedure SetDlgNamePostfix(const Value: string);
    procedure SetFormNameFmt(const Value: string);
    procedure SetUnitNameFmt(const Value: string);

    procedure SetFormBorderStyle(const Value: TFormBorderStyle);
    procedure SetFormPosition(const Value: TPosition);
    procedure SetButtonsAlign(const Value: TAlign);

    procedure SetUseCaption(const Value: boolean);
    procedure SetUseOwner(const Value: boolean);

    procedure SetPnlClientPadding(const Value: integer);
    procedure SetPnlButtonsHeight(const Value: integer);
    procedure SetPnlButtonsPadding(const Value: integer);
    procedure SetButtonWidth(const Value: integer);
    procedure SetButtonMargins(const Value: integer);

  protected
    procedure DoDefaultStrPropValue(const APropInfo: PPropInfo); override;

  public
    property Form : TfrmDialogExpert  read GetForm;

  published
    property DlgNamePrefix   : string  read FDlgNamePrefix   write SetDlgNamePrefix;
    property DlgNamePostfix  : string  read FDlgNamePostfix  write SetDlgNamePostfix;
    property FormNameFmt     : string  read FFormNameFmt     write SetFormNameFmt;
    property UnitNameFmt     : string  read FUnitNameFmt     write SetUnitNameFmt;

    property FormBorderStyle : TFormBorderStyle  read FFormBorderStyle  write SetFormBorderStyle  default bsDialog;
    property FormPosition : TPosition  read FFormPosition  write SetFormPosition  default poOwnerFormCenter;

    property ButtonsAlign : TAlign  read FButtonsAlign  write SetButtonsAlign  default alRight;

    property UseCaption  : boolean  read FUseCaption  write SetUseCaption  default True;
    property UseOwner    : boolean  read FUseOwner    write SetUseOwner    default True;

    property PnlClientPadding   : integer  read FPnlClientPadding   write SetPnlClientPadding   default 4;
    property PnlButtonsHeight   : integer  read FPnlButtonsHeight   write SetPnlButtonsHeight   default 41;
    property PnlButtonsPadding  : integer  read FPnlButtonsPadding  write SetPnlButtonsPadding  default 4;
    property ButtonWidth        : integer  read FButtonWidth        write SetButtonWidth        default 75;
    property ButtonMargins      : integer  read FButtonMargins      write SetButtonMargins      default 4;

  end;

  TfrmDialogExpert = class(TfrmCustomExpertDialog)
    gbNames: TGroupBox;
    lblDLGName: TLabel;
    frameNames: TframeNamesPrefPostShaped;
    lblFormNameFmt: TLabel;
    frameFmtFormName: TframeFormatNameShaped;
    lblUnitNameFmt: TLabel;
    frameFmtUnitName: TframeFormatNameShaped;
    gbPAS: TGroupBox;
    chkUseCaption: TCheckBox;
    chkUseOwner: TCheckBox;
    gbDFM: TGroupBox;
    gbDFMForm: TGroupBox;
    lblPosition: TLabel;
    lblBorderStyle: TLabel;
    cmbxBorderStyle: TComboBox;
    cmbxPosition: TComboBox;
    gbDFMPnlClient: TGroupBox;
    lblPnlClientPadding: TLabel;
    sePnlClientPadding: TSpinEdit;
    gbDFMPnlButtons: TGroupBox;
    lblPnlButtonsHeight: TLabel;
    lblPnlButtonsPadding: TLabel;
    sePnlButtonsHeight: TSpinEdit;
    sePnlButtonsPadding: TSpinEdit;
    gbDFMButtons: TGroupBox;
    lblButtonWidth: TLabel;
    lblButtonMargins: TLabel;
    lblButtonsAlign: TLabel;
    seButtonWidth: TSpinEdit;
    seButtonMargins: TSpinEdit;
    cmbxButtonsAlign: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure chksCodeChange(Sender: TObject);
    procedure cmbxsVisualizeChange(Sender: TObject);
    procedure sesVisualizeChange(Sender: TObject);

  private
    function GetProps: TDialogExpertProps;

  protected
    function GetINIFileName: TFileName; override;
    function ExpertPropsClass : TCustomExpertPropsClass; override;
    procedure DoWarnShapeValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string); override;
    procedure UpdateFmtNames;

  public
    procedure FillReplacer(const AReplacer : TSANReplacer); override;

    property Props : TDialogExpertProps  read GetProps;
  end;

  /// <summary>
  /// Эксперт создания диалогов.
  /// </summary>
  TSANRepExpertCustomDialog = class(TSANCustomRepExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetGlyphResName : string; override;
    function DLGFormClass : TFormClass; override;
    procedure DoExecute(const ADLGForm : TForm; const AReplacer : TSANReplacer); override;
  end;

IMPLEMENTATION

{$R *.dfm}

const
  ICON_RES_NAME = 'SAN_EXP_REP_ICON_CUSTOMDIALOG';

resourcestring
  sExpertCaption = 'Эксперт создания диалогов';

//==============================================================================
// TDialogExpertProps
// private
function TDialogExpertProps.GetForm: TfrmDialogExpert;
begin
  Result := (inherited Form) as TfrmDialogExpert;
end;

procedure TDialogExpertProps.SetDlgNamePrefix(const Value: string);
begin
  if FDlgNamePrefix = Value then
    Exit;

  FDlgNamePrefix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.edtNamePrefix.Text := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TDialogExpertProps.SetDlgNamePostfix(const Value: string);
begin
  if FDlgNamePostfix = Value then
    Exit;

  FDlgNamePostfix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.edtNamePostfix.Text := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TDialogExpertProps.SetFormNameFmt(const Value: string);
begin
  if FFormNameFmt = Value then
    Exit;

  FFormNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtFormName.edtNameFmt.Text := Value;
end;

procedure TDialogExpertProps.SetUnitNameFmt(const Value: string);
begin
  if FUnitNameFmt = Value then
    Exit;

  FUnitNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtUnitName.edtNameFmt.Text := Value;
end;

procedure TDialogExpertProps.SetFormBorderStyle(const Value: TFormBorderStyle);
begin
  if FFormBorderStyle = Value then
    Exit;

  FFormBorderStyle := Value;

  if Assigned(Form) then
    with Form.cmbxBorderStyle do
      ItemIndex := Items.IndexOf( GetEnumName(TypeInfo(TBorderStyle), Ord(Value)));
end;

procedure TDialogExpertProps.SetFormPosition(const Value: TPosition);
begin
  if FFormPosition = Value then
    Exit;

  FFormPosition := Value;

  if Assigned(Form) then
    with Form.cmbxPosition do
      ItemIndex := Items.IndexOf( GetEnumName(TypeInfo(TPosition), Ord(Value)));
end;

procedure TDialogExpertProps.SetButtonsAlign(const Value: TAlign);
begin
  if FButtonsAlign = Value then
    Exit;

  FButtonsAlign := Value;

  if Assigned(Form) then
    with Form.cmbxButtonsAlign do
      ItemIndex := Items.IndexOf( GetEnumName(TypeInfo(TAlign), Ord(Value)));
end;

procedure TDialogExpertProps.SetUseCaption(const Value: boolean);
begin
  if FUseCaption = Value then
    Exit;

  FUseCaption := Value;

  if Assigned(Form) then
    Form.chkUseCaption.Checked := Value;
end;

procedure TDialogExpertProps.SetUseOwner(const Value: boolean);
begin
  if FUseOwner = Value then
    Exit;

  FUseOwner := Value;

  if Assigned(Form) then
    Form.chkUseOwner.Checked := Value;
end;

procedure TDialogExpertProps.SetPnlClientPadding(const Value: integer);
begin
  if FPnlClientPadding = Value then
    Exit;

  FPnlClientPadding := Value;

  if Assigned(Form) then
    Form.sePnlClientPadding.Value := Value;
end;

procedure TDialogExpertProps.SetPnlButtonsHeight(const Value: integer);
begin
  if FPnlButtonsHeight = Value then
    Exit;

  FPnlButtonsHeight := Value;

  if Assigned(Form) then
    Form.sePnlButtonsHeight.Value := Value;
end;

procedure TDialogExpertProps.SetPnlButtonsPadding(const Value: integer);
begin
  if FPnlButtonsPadding = Value then
    Exit;

  FPnlButtonsPadding := Value;

  if Assigned(Form) then
    Form.sePnlButtonsPadding.Value := Value;
end;

procedure TDialogExpertProps.SetButtonWidth(const Value: integer);
begin
  if FButtonWidth = Value then
    Exit;

  FButtonWidth := Value;

  if Assigned(Form) then
    Form.seButtonWidth.Value := Value;
end;

procedure TDialogExpertProps.SetButtonMargins(const Value: integer);
begin
  if FButtonMargins = Value then
    Exit;

  FButtonMargins := Value;

  if Assigned(Form) then
    Form.seButtonMargins.Value := Value;
end;

// protected
procedure TDialogExpertProps.DoDefaultStrPropValue(const APropInfo: PPropInfo);
begin

  if CompareText(String(APropInfo^.Name), 'DlgNamePostfix') = 0 then
    SetStrProp(Self, APropInfo, 'DLG')

  else
  if CompareText(String(APropInfo^.Name), 'FormNameFmt') = 0 then
    SetStrProp(Self, APropInfo, 'frm%s')

  else
  if CompareText(String(APropInfo^.Name), 'UnitNameFmt') = 0 then
    SetStrProp(Self, APropInfo, '%s_u')

  else
    inherited;

end;

//==============================================================================

//==============================================================================
// TfrmDialogExpert
// private
function TfrmDialogExpert.GetProps: TDialogExpertProps;
begin
  Result := (inherited Props) as TDialogExpertProps;
end;

// protected
function TfrmDialogExpert.GetINIFileName: TFileName;
begin
  Result := 'CustomDialog.ini';
end;

function TfrmDialogExpert.ExpertPropsClass: TCustomExpertPropsClass;
begin
  Result := TDialogExpertProps;
end;

procedure TfrmDialogExpert.DoWarnShapeValueUpdated(const AFrame: TFrame;
  const AControl: TControl; const AValue: Variant; var AWarnHint: string);
begin

  if AFrame = frameNames then
    begin
      AWarnHint := 'Некорректное имя диалога';

      if AControl = frameNames.edtNamePrefix  then
        Props.DlgNamePrefix := AValue

      else
      if AControl = frameNames.edtNamePostfix then
        Props.DlgNamePostfix := AValue

      else
      if AControl = frameNames.edtName then
        UpdateFmtNames;

    end

  else
  if AFrame = frameFmtFormName then
    begin
      AWarnHint := 'Некорректный формат имени формы';
      Props.FormNameFmt := AValue;
    end

  else
  if AFrame = frameFmtUnitName then
    begin
      AWarnHint := 'Некорректный формат имени модуля';
      Props.UnitNameFmt := AValue;
    end

end;

procedure TfrmDialogExpert.UpdateFmtNames;
begin
  frameFmtFormName.NameForFmt := frameNames.FullName;
  frameFmtUnitName.NameForFmt := frameNames.FullName;
end;

// public
procedure TfrmDialogExpert.FillReplacer(const AReplacer: TSANReplacer);
begin
  AReplacer.AddPair('DlgName', frameNames.FullName);

  AReplacer.AddPair
  (
    'Defs',
    IfThen(Props.UseCaption, '{$DEFINE USE_CAPTION}'#13#10) +
    IfThen(Props.UseOwner,   '{$DEFINE USE_OWNER}'#13#10)
  );

  AReplacer.AddPair('BorderStyle',  GetEnumName( TypeInfo(TFormBorderStyle),  Ord(Props.FormBorderStyle)));

  AReplacer.AddPair('Position',     GetEnumName( TypeInfo(TPosition),         Ord(Props.FormPosition)));

  AReplacer.AddPair('ButtonAlign',  GetEnumName( TypeInfo(TAlign),            Ord(Props.ButtonsAlign)));

  AReplacer.AddPair('pnlClientPadding',   IntToStr(Props.PnlClientPadding));

  AReplacer.AddPair('pnlButtonsHeight',   IntToStr(Props.PnlButtonsHeight));

  AReplacer.AddPair('pnlButtonsPadding',  IntToStr(Props.PnlButtonsPadding));

  AReplacer.AddPair('ButtonWidth',        IntToStr(Props.ButtonWidth));

  AReplacer.AddPair('ButtonMargins',      IntToStr(Props.ButtonMargins));
end;

// published
procedure TfrmDialogExpert.FormCreate(Sender: TObject);

  procedure InitBorderStyleComboBox;
  var
    vBS : TFormBorderStyle;
  begin
    with cmbxBorderStyle, Items do
    begin

      BeginUpdate;
      try

        for vBS := Low(TFormBorderStyle) to High(TFormBorderStyle) do
          AddObject( GetEnumName(TypeInfo(TFormBorderStyle), Ord(vBS)), TObject(vBS) );

      finally
        EndUpdate
      end;

    end;
  end;

  procedure InitPositionComboBox;
  var
    vPS : TPosition;
  begin
    with cmbxPosition, Items do
    begin

      BeginUpdate;
      try

        for vPS := Low(TPosition) to High(TPosition) do
          AddObject( GetEnumName(TypeInfo(TPosition), Ord(vPS)), TObject(vPS) );


      finally
        EndUpdate
      end;

    end;
  end;

begin
  Caption := LoadResString(@sExpertCaption);
  Icon.LoadFromResourceName(HInstance, ICON_RES_NAME);

  InitBorderStyleComboBox;

  InitPositionComboBox;

  PositiveSpins
  (
    [
      sePnlClientPadding,
      sePnlButtonsHeight,
      sePnlButtonsPadding,
      seButtonWidth,
      seButtonMargins
    ]
  );

  cmbxButtonsAlign.AddItem( GetEnumName(TypeInfo(TAlign), Ord(alLeft)),  TObject(alLeft));
  cmbxButtonsAlign.AddItem( GetEnumName(TypeInfo(TAlign), Ord(alRight)), TObject(alRight));

  inherited;
end;

procedure TfrmDialogExpert.chksCodeChange(Sender: TObject);
var
  vCK : TCheckBox;
  vPropName : string;
  vPropInfo : PPropInfo;
begin
  vCK := (Sender as TCheckBox);

  vPropName := vCK.Name;
  Delete(vPropName, 1, Length('chk'));

  vPropInfo := GetPropInfo(Props, vPropName);

  if Assigned(vPropInfo) then
    SetOrdProp(Props, vPropInfo, ORD(vCK.Checked));
end;

procedure TfrmDialogExpert.cmbxsVisualizeChange(Sender: TObject);
var
  vCB : TComboBox;
  vPropName : string;
  vPropInfo : PPropInfo;
begin
  vCB := (Sender as TComboBox);

  vPropName := vCB.Name;
  Delete(vPropName, 1, Length('cmbx'));

  vPropInfo := GetPropInfo(Props, vPropName);

  if Assigned(vPropInfo) then
    SetOrdProp(Props, vPropInfo, Integer(vCB.Items.Objects[vCB.ItemIndex]));
end;

procedure TfrmDialogExpert.sesVisualizeChange(Sender: TObject);
var
  vSE : TSpinEdit;
  vPropName : string;
  vPropInfo : PPropInfo;
begin
  vSE := (Sender as TSpinEdit);

  vPropName := vSE.Name;
  Delete(vPropName, 1, Length('se'));

  vPropInfo := GetPropInfo(Props, vPropName);

  if Assigned(vPropInfo) then
    SetOrdProp(Props, vPropInfo, vSE.Value);
end;
//==============================================================================

//==============================================================================
// TSANRepExpertCustomDialog
// protected
function TSANRepExpertCustomDialog.GetIDString: string;
begin
  Result := 'SAN.RepExp.CustomDialog';
end;

function TSANRepExpertCustomDialog.GetName: string;
begin
  Result := 'Custom Dialog Expert';
end;

function TSANRepExpertCustomDialog.GetComment: string;
begin
  Result := LoadResString(@sExpertCaption);
end;

function TSANRepExpertCustomDialog.GetGlyphResName: string;
begin
  Result := ICON_RES_NAME;
end;

function TSANRepExpertCustomDialog.DLGFormClass: TFormClass;
begin
  Result := TfrmDialogExpert;
end;

procedure TSANRepExpertCustomDialog.DoExecute(const ADLGForm: TForm;
  const AReplacer: TSANReplacer);
var
  vForm : TfrmDialogExpert;
  vModule : IOTAModule;
begin
  vForm := TfrmDialogExpert(ADLGForm);

  vModule := TSANStaticModuleCreator.CreateModule
  (
    ctForm,
    vForm.frameNames.FullName,
    '',
    vForm.Props.FormNameFmt,
    vForm.Props.UnitNameFmt,
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_CUSTOMDIALOG_DFM'),
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_CUSTOMDIALOG_PAS'),
    AReplacer
  );

  GetActiveProject.AddFile(vModule.FileName, True);

  vModule.Show;
end;
//==============================================================================


END.
