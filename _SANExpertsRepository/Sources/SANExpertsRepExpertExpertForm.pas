{*******************************************************}
{                                                       }
{       SANExpertsRepExpertExpertForm                   }
{       Эксперт создания эксперта репозитория.          }
{       загрузки "пакетов-как-Dll".                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepExpertExpertForm;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,TypInfo,

  SANMisc, SANINIProps, SANWinUtils,

  SANDesignReplacer, SANDesignCreaters, SANDesignWizards,

  SANExpertsRepCommon,
  SANExpertsRepCustomExpertDialogForm, SANExpertsRepFormatNameShapedFrame,
  SANExpertsRepCustomShapedFrame, SANExpertsRepNamesPrefPostShapedFrame;

TYPE
  TfrmExpertExpert = class;

  TExpertProps = class(TCustomExpertProps)
  private
    FUnitNameFmt: string;
    FNamePostfix: string;
    FFormNameFmt: string;
    FNamePrefix: string;

    function GetForm: TfrmExpertExpert;

    procedure SetNamePrefix(const Value: string);
    procedure SetNamePostfix(const Value: string);
    procedure SetFormNameFmt(const Value: string);
    procedure SetUnitNameFmt(const Value: string);

  public
    property Form : TfrmExpertExpert  read GetForm;

  published
    property NamePrefix   : string  read FNamePrefix   write SetNamePrefix;
    property NamePostfix  : string  read FNamePostfix  write SetNamePostfix;
    property FormNameFmt  : string  read FFormNameFmt  write SetFormNameFmt;
    property UnitNameFmt  : string  read FUnitNameFmt  write SetUnitNameFmt;

  end;

  TfrmExpertExpert = class(TfrmCustomExpertDialog)
    gbNames: TGroupBox;
    lblDLGName: TLabel;
    lblFormNameFmt: TLabel;
    lblUnitNameFmt: TLabel;
    frameNames: TframeNamesPrefPostShaped;
    frameFmtFormName: TframeFormatNameShaped;
    frameFmtUnitName: TframeFormatNameShaped;
    procedure FormCreate(Sender: TObject);
  private
    function GetProps: TExpertProps;

  protected
    function GetINIFileName: TFileName; override;
    function ExpertPropsClass : TCustomExpertPropsClass; override;
    procedure DoWarnShapeValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string); override;
    procedure UpdateFmtNames;

  public
    procedure FillReplacer(const AReplacer : TSANReplacer); override;

    property Props : TExpertProps  read GetProps;

  end;

  /// <summary>
  /// Эксперт создания эксперта.
  /// </summary>
  TSANRepExpertExpert = class(TSANCustomRepExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetGlyphResName : string; override;
    function DLGFormClass : TFormClass; override;
    procedure DoExecute(const ADLGForm : TForm; const AReplacer : TSANReplacer); override;
  end;

IMPLEMENTATION

{$R *.dfm}

const
  ICON_RES_NAME = 'SAN_EXP_REP_ICON_EXPERTEXPERT';

resourcestring
  sExpertCaption = 'Эксперт создания эксперта';

//==============================================================================
// TExpertProps
// private
function TExpertProps.GetForm: TfrmExpertExpert;
begin
  Result := (inherited Form) as TfrmExpertExpert;
end;

procedure TExpertProps.SetNamePostfix(const Value: string);
begin
  if FNamePostfix = Value then
    Exit;

  FNamePostfix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.edtNamePostfix.Text := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TExpertProps.SetNamePrefix(const Value: string);
begin
  if FNamePrefix = Value then
    Exit;

  FNamePrefix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.edtNamePrefix.Text := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TExpertProps.SetFormNameFmt(const Value: string);
begin
  if FFormNameFmt = Value then
    Exit;

  FFormNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtFormName.edtNameFmt.Text := Value;
end;

procedure TExpertProps.SetUnitNameFmt(const Value: string);
begin
  if FUnitNameFmt = Value then
    Exit;

  FUnitNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtUnitName.edtNameFmt.Text := Value;
end;

// protected
//==============================================================================

//==============================================================================
// TfrmExpertExpert
// private
function TfrmExpertExpert.GetProps: TExpertProps;
begin
  Result := (inherited Props) as TExpertProps;
end;

// protected
function TfrmExpertExpert.GetINIFileName: TFileName;
begin
  Result := 'Expert.ini';
end;

function TfrmExpertExpert.ExpertPropsClass: TCustomExpertPropsClass;
begin
  Result := TExpertProps;
end;

procedure TfrmExpertExpert.DoWarnShapeValueUpdated(const AFrame: TFrame;
  const AControl: TControl; const AValue: Variant; var AWarnHint: string);
begin

  if AFrame = frameNames then
    begin
      AWarnHint := 'Некорректное имя эксперта';

      if AControl = frameNames.edtNamePrefix  then
        Props.NamePrefix := AValue

      else
      if AControl = frameNames.edtNamePostfix then
        Props.NamePostfix := AValue

      else
      if AControl = frameNames.edtName then
        UpdateFmtNames;

    end

  else
  if AFrame = frameFmtFormName then
    begin
      AWarnHint := 'Некорректный формат имени формы';
      Props.FormNameFmt := AValue;
    end

  else
  if AFrame = frameFmtUnitName then
    begin
      AWarnHint := 'Некорректный формат имени модуля';
      Props.UnitNameFmt := AValue;
    end

end;

procedure TfrmExpertExpert.UpdateFmtNames;
begin
  frameFmtFormName.NameForFmt := frameNames.FullName;
  frameFmtUnitName.NameForFmt := frameNames.FullName;
end;

// public
procedure TfrmExpertExpert.FillReplacer(const AReplacer: TSANReplacer);
begin
  AReplacer.AddPair('ExpFullName',  frameNames.FullName);
  AReplacer.AddPair('ExpName',      frameNames.MidName);
  AReplacer.AddPair('ExpNameUp',    AnsiUpperCase(frameNames.MidName));
end;

// published
procedure TfrmExpertExpert.FormCreate(Sender: TObject);
begin
  Caption := LoadResString(@sExpertCaption);
  Icon.LoadFromResourceName(HInstance, ICON_RES_NAME);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANRepExpertExpert
// protected
function TSANRepExpertExpert.GetIDString: string;
begin
  Result := 'SAN.RepExp.ExpertExpert';
end;

function TSANRepExpertExpert.GetName: string;
begin
  Result := 'Expert for create expert';
end;

function TSANRepExpertExpert.GetComment: string;
begin
  Result := LoadResString(@sExpertCaption);
end;

function TSANRepExpertExpert.GetGlyphResName: string;
begin
  Result := ICON_RES_NAME;
end;

function TSANRepExpertExpert.DLGFormClass: TFormClass;
begin
  Result := TfrmExpertExpert;
end;

procedure TSANRepExpertExpert.DoExecute(const ADLGForm: TForm;
  const AReplacer: TSANReplacer);
var
  vForm : TfrmExpertExpert;
  vModule : IOTAModule;
begin
  vForm := TfrmExpertExpert(ADLGForm);

  vModule := TSANStaticModuleCreator.CreateModule
  (
    ctForm,
    vForm.frameNames.FullName,
    TfrmCustomExpertDialog.ClassName,
    vForm.Props.FormNameFmt,
    vForm.Props.UnitNameFmt,
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_CUSTOMREPEXPERT_DFM'),
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_CUSTOMREPEXPERT_PAS'),
    AReplacer
  );

  GetActiveProject.AddFile(vModule.FileName, True);

  vModule.Show;
end;
//==============================================================================


END.
