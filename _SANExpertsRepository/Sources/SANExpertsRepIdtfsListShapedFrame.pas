{*******************************************************}
{                                                       }
{       SANExpertsRepIdtfsListShapedFrame               }
{       Фрейм ввода списка корректных идентификаторов.  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepIdtfsListShapedFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,

  SANMisc,

  SANExpertsRepCustomShapedFrame;

TYPE
  /// <summary>
  /// Фрейм ввода списка корректных идентификаторов.
  /// </summary>
  TframeIdtfsListShaped = class(TframeShaped)
    mmIdtfs: TMemo;
    procedure mmIdtfsChange(Sender: TObject);
    procedure mmIdtfsKeyPress(Sender: TObject; var Key: Char);

  private
    function GetIdtfs: TStrings;

  protected
    function DoGetValidValues : boolean; override;

  public
    constructor Create(AOwner : TComponent); override;

    property Idtfs : TStrings  read GetIdtfs;
  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeIdtfsListShaped
// private
function TframeIdtfsListShaped.GetIdtfs: TStrings;
begin
  Result := mmIdtfs.Lines;
end;

// protected
function TframeIdtfsListShaped.DoGetValidValues: boolean;
var
  i : integer;
  vSL : TStrings;
begin
  vSL := mmIdtfs.Lines;

  for i := 0 to vSL.Count - 1 do
    if not CheckPasIdentifierName(vSL[i]) then
      Exit(False);

  Result := True;
end;

// public
constructor TframeIdtfsListShaped.Create(AOwner: TComponent);
begin
  inherited;

  ClearEditsText;
end;

// published
procedure TframeIdtfsListShaped.mmIdtfsChange(Sender: TObject);
var
  vWarnHint : string;
begin
  UpdateValue(Sender as TControl, (Sender as TMemo).Text, vWarnHint);

  UpdateWarnShape(DoGetValidValues, vWarnHint);
end;

procedure TframeIdtfsListShaped.mmIdtfsKeyPress(Sender: TObject; var Key: Char);
var
  vSet : TSysCharSet;
begin
  vSet := ['a'..'z', '_', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT), CHR(VK_RETURN)];

  if (Sender as TMemo).CaretPos.X > 0 then
    vSet := vSet + ['0'..'9'];

  if not CharInSet(LowerCase(Key)[1], vSet) then
  begin
    Key := #0;
    MessageBeep(MB_OK);
  end;

end;
//==============================================================================

END.
