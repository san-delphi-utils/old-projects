{*******************************************************}
{                                                       }
{       SANExpertsRepEntryPointNameShapedFrame          }
{       Фрейм ввода имени точки входа.                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepEntryPointNameShapedFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActiveX, ComObj, Buttons, StdCtrls,

  SANMisc,

  SANExpertsRepCustomShapedFrame;

TYPE
  TframeEntryPointNameShaped = class(TframeShaped)
    edtName: TEdit;
    sbtnGenerate: TSpeedButton;
    procedure sbtnGenerateClick(Sender: TObject);
    procedure edtNameChange(Sender: TObject);
    procedure edtNameKeyPress(Sender: TObject; var Key: Char);

  private
    function GetEntryPointName: string;
    procedure SetEntryPointName(const Value: string);

  protected
    function DoGetValidValues : boolean; override;

  public
    constructor Create(AOwner : TComponent); override;

    property EntryPointName : string  read GetEntryPointName  write SetEntryPointName;
  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeEntryPointNameShaped
// private
function TframeEntryPointNameShaped.GetEntryPointName: string;
begin
  Result := edtName.Text;
end;

procedure TframeEntryPointNameShaped.SetEntryPointName(const Value: string);
begin
  edtName.Text := Value;
end;

// protected
function TframeEntryPointNameShaped.DoGetValidValues: boolean;
begin
  Result := CheckPasIdentifierName(edtName.Text);
end;

// public
constructor TframeEntryPointNameShaped.Create(AOwner: TComponent);
begin
  inherited;

  sbtnGenerate.Click;
end;

// published
procedure TframeEntryPointNameShaped.sbtnGenerateClick(Sender: TObject);
var
  S : string;
  vGUID : TGUID;
begin
  OleCheck(CreateGUID(vGUID));

  S := GUIDToString(vGUID);

  S[1] := '_';
  SetLength(S, Length(S) - 1);

  S := StringReplace(S, '-', '', [rfReplaceAll, rfIgnoreCase]);

  edtName.Text := S;
end;

procedure TframeEntryPointNameShaped.edtNameChange(Sender: TObject);
var
  vWarnHint : string;
begin
  UpdateValue(Sender as TControl, (Sender as TEdit).Text, vWarnHint);

  UpdateWarnShape(DoGetValidValues, vWarnHint);
end;

procedure TframeEntryPointNameShaped.edtNameKeyPress(Sender: TObject;
  var Key: Char);
var
  vSet : TSysCharSet;
begin

  vSet := ['a'..'z', '_', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)];

  if (Sender as TEdit).Text <> '' then
    vSet := vSet + ['0'..'9'];

  if not CharInSet(LowerCase(Key)[1], vSet) then
  begin
    Key := #0;
    MessageBeep(MB_OK);
  end;
end;
//==============================================================================

END.
