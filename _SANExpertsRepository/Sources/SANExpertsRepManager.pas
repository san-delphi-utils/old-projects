{*******************************************************}
{                                                       }
{       SANExpertsRepManager                            }
{       Менедежер экспертов репозитория.                }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepManager;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  Windows, SysUtils, Classes, Variants, StrUtils, Controls, Forms, Dialogs,
  xmldom, XMLIntf, xmldoc,

  SANSync, SANWinUtils,
  SANPkgAsDllServer,
  SANExpertsRepIntfs, SANExpertsRepWizards, SANExpertsRepDialog;

CONST
  SAN_EXPERTS_REP_FOLDER      = 'SAN\ForBDS\SANExpertsRepositoryBPL';
  SAN_EXPERTS_REP_XML_FOLDER  = SAN_EXPERTS_REP_FOLDER + '\ExpertsXML';

TYPE
  ESANRepExpError = class(Exception);

  /// <summary>
  /// Менедежер экспертов репозитория.
  /// </summary>
  TSANRepExpEditorsManager = class(TSANPkgAsDllsServer, ISANRepExpEditorsManager)
  private
    FEditors : TInterfaceList;
    FWizardsIDStrGenerator : TSANGenerator;

    // Список не владеет объектами.
    FWizards : TSANExpertsRepWizards;

    /// <summary>
    /// Является владельцем FXMLDoc.
    /// Предотвращает Invalid Pointer Operation при TXMLDocument.Create(nil).
    /// </summary>
    FXMLDocHost : TComponent;
    FXMLDoc : TXMLDocument;

  protected
    // ISANRepExpEditorsManager
    function GetEditorsCount : integer; safecall;
    function GetEditors(const AIndex : integer) : ISANRepExpEditor; safecall;
    function EditorsFind(out AEditor : ISANRepExpEditor; const AName : WideString) : WordBool; safecall;
    function RegEditor(const AEditor : ISANRepExpEditor) : WordBool; safecall;
    function UnRegEditor(const AEditorName : WideString) : WordBool; safecall;
    function GetAppHandle : THandle; safecall;

    function DoEditorsFind(out AEditor : ISANRepExpEditor; const AName : WideString;
      const AIndexPtr : PInteger = nil) : WordBool;
    function DoEditorsExists(const AName : WideString; const AIndexPtr : PInteger = nil) : WordBool;

    procedure ValidateXML(var AExpertNode, AModulesNode, ADialogNode : IXMLNode);

  public
    constructor Create(const ADllEntryPointName : string); override;
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    procedure AddWizardFromXML(const AXMLFileName : TFileName);

    procedure LoadWizardsFromFolder(const APath : string);

    procedure ExecuteWizard(const AWizard : TSANExpertsRepCustomWizard);

  end;

//procedure SANRepExpEditorsManagerInit;
//procedure SANRepExpEditorsManagerFinal;
function SANRepExpEditorsManager : TSANRepExpEditorsManager;

IMPLEMENTATION

uses
  ComObj;

resourcestring
  sInvalidXMLErrorMessage         = 'SANExpertsRep: Некорректный XML-файл';
  sInvalidXMLNotNodeErrorMessage  = 'SANExpertsRep: Некорректный XML-файл. Нет узла "%s"';

type
  TWinControlHack = class(TWinControl);

//==============================================================================
// TSANRepExpEditorsManager
// protected
function TSANRepExpEditorsManager.GetEditorsCount: integer;
begin
  Result := FEditors.Count;
end;

function TSANRepExpEditorsManager.GetEditors(const AIndex: integer): ISANRepExpEditor;
begin
  Result := FEditors[AIndex] as ISANRepExpEditor;
end;

function TSANRepExpEditorsManager.EditorsFind(out AEditor: ISANRepExpEditor;
  const AName: WideString): WordBool;
begin
  Result := DoEditorsFind(AEditor, AName);
end;

function TSANRepExpEditorsManager.RegEditor(const AEditor: ISANRepExpEditor): WordBool;
begin
  Result := not DoEditorsExists(AEditor.Name);

  if Result then
    FEditors.Add(AEditor);
end;

function TSANRepExpEditorsManager.UnRegEditor(const AEditorName: WideString): WordBool;
var
  i : integer;
begin
  Result := DoEditorsExists(AEditorName, @i);

  if Result then
    FEditors.Delete(i);
end;

procedure TSANRepExpEditorsManager.ValidateXML(var AExpertNode, AModulesNode, ADialogNode: IXMLNode);
var
  vXMLNode : IXMLNode;
begin
  vXMLNode := FXMLDoc.DocumentElement;
  if not Assigned(vXMLNode) then
    raise ESANRepExpError.CreateResFmt(@sInvalidXMLNotNodeErrorMessage, ['XML']);

  AExpertNode := vXMLNode.ChildNodes.FindNode('EXPERT');

  AModulesNode := vXMLNode.ChildNodes.FindNode('MODULES');
  if not Assigned(AModulesNode) then
    raise ESANRepExpError.CreateResFmt(@sInvalidXMLNotNodeErrorMessage, ['MODULES']);

  ADialogNode := vXMLNode.ChildNodes.FindNode('DIALOG');
  if not Assigned(ADialogNode) then
    raise ESANRepExpError.CreateResFmt(@sInvalidXMLNotNodeErrorMessage, ['DIALOG']);
end;

function TSANRepExpEditorsManager.GetAppHandle: THandle;
begin
  Result := (BorlandIDEServices as IOTAServices).GetParentHandle;
end;

function TSANRepExpEditorsManager.DoEditorsFind(out AEditor: ISANRepExpEditor;
  const AName: WideString; const AIndexPtr: PInteger): WordBool;
var
  i : integer;
  vEdt : ISANRepExpEditor;
begin

  for i := 0 to FEditors.Count - 1 do
    if Supports(FEditors[i], ISANRepExpEditor, vEdt)
    and (CompareText(vEdt.Name, AName) = 0)
    then
    begin
      AEditor := vEdt;

      if Assigned(AIndexPtr) then
        AIndexPtr^ := i;

      Exit(True);
    end;

  Result := False;
end;

function TSANRepExpEditorsManager.DoEditorsExists(const AName: WideString;
  const AIndexPtr: PInteger): WordBool;
var
  vDummy : ISANRepExpEditor;
begin
  Result := DoEditorsFind(vDummy, AName, AIndexPtr);
end;

// public
constructor TSANRepExpEditorsManager.Create(const ADllEntryPointName: string);
begin
  inherited;

  FEditors := TInterfaceList.Create;

  FWizardsIDStrGenerator := TSANGenerator.Create;

  FWizards := TSANExpertsRepWizards.Create(False);

//  FXMLDoc := TXMLDocument(Application.FindComponent('xmldocSANRepExpEditorsManager'));
//  if not Assigned(FXMLDoc) then
//  begin
//    FXMLDoc := TXMLDocument.Create(Application);
//    FXMLDoc.Name := 'xmldocSANRepExpEditorsManager';
//  end;

  FXMLDocHost := TComponent.Create(nil);
  FXMLDocHost.Name := 'cmpSANRepExpEditorsManagerHost';

  FXMLDoc := TXMLDocument.Create(FXMLDocHost);
  FXMLDoc.Name := 'xmldocSANRepExpEditorsManager';
end;

destructor TSANRepExpEditorsManager.Destroy;
begin
  FWizards.UnRegWizards;

  FreeAndNil(FXMLDoc);
  FreeAndNil(FXMLDocHost);

  FreeAndNil(FWizards);

  FreeAndNil(FWizardsIDStrGenerator);

  FreeAndNil(FEditors);

  inherited;
end;

function TSANRepExpEditorsManager.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{6BDC6AE5-122E-48DE-8FFA-299058FBCD88}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

procedure TSANRepExpEditorsManager.AddWizardFromXML(const AXMLFileName : TFileName);

  procedure LoadXML;
  begin
    try
      FXMLDoc.LoadFromFile(AXMLFileName);
    except
      on EXMLDocError do
        ESANRepExpError.RaiseOuterException(ESANRepExpError.CreateRes(@sInvalidXMLErrorMessage));
    end;
  end;

var
  vExpertNode, vModulesNode, vDialogNode : IXMLNode;

  procedure TryReadAttribute(const AAttrName : string; var AVar : string);
  begin

    if not vExpertNode.HasAttribute(AAttrName) then
      Exit;

    AVar := VarToStr(vExpertNode.Attributes[AAttrName]);
  end;

  function IsProjectOrGroup : boolean;
  var
    i : integer;
  begin

    for i := 0 to vModulesNode.ChildNodes.Count - 1 do
      if MatchText
      (
        vModulesNode.ChildNodes[i].NodeName,
        [
          'PROJECTGROUP', 'APLICATION', 'LIBRARY', 'CONSOLE', 'PACKAGE', 'STATICLIBRARY', 'OPTIONSET'
        ]
      )
      then
        Exit(True);

    Result := False;
  end;

var
  vExpNum : Cardinal;
  vCurDir, vIDString, vName, vAuthor, vComment, vPage, vGlyphFileName : string;
  vClass : TSANExpertsRepCustomWizardClass;
  vWizard : TSANExpertsRepCustomWizard;
begin
  LoadXML;
  try
    ValidateXML(vExpertNode, vModulesNode, vDialogNode);

    vCurDir := GetCurrentDir;
    try
      SetCurrentDir(ExtractFileDir(AXMLFileName));

      vExpNum := FWizardsIDStrGenerator.Generate;

      vIDString       := Format('SAN.Experts.Rep.%d', [vExpNum]);
      vName           := Format('Expert №%d', [vExpNum]);
      vAuthor         := 'SAN';
      vComment        := '';
      vPage           := 'SAN';
      vGlyphFileName  := '';

      if Assigned(vExpertNode) then
      begin
        TryReadAttribute('Name',     vName);
        TryReadAttribute('Author',   vAuthor);
        TryReadAttribute('Comment',  vComment);
        TryReadAttribute('Page',     vPage);
        TryReadAttribute('Glyph',    vGlyphFileName);
      end;

      if IsProjectOrGroup then
        vClass := TSANExpertsRepProjectWizard
      else
        vClass := TSANExpertsRepFormWizard;

      vWizard := vClass.Create(FXMLDoc.XML.Text, vIDString, vName, vAuthor, vComment, vPage, vGlyphFileName);

      vWizard.RegWizard;

      FWizards.Add(vWizard);

    finally
      SetCurrentDir(vCurDir);
    end;

  finally
    FXMLDoc.Active := False;
  end;
end;

procedure TSANRepExpEditorsManager.LoadWizardsFromFolder(const APath: string);
var
  vSR : TSearchRec;
begin

  if FindFirst(APath + '*.srex', faAnyFile AND NOT faDirectory, vSR) = 0 then
    try

      repeat
        AddWizardFromXML(APath + vSR.Name);

      until FindNext(vSR) > 0;

    finally
      FindClose(vSR);
    end;
end;

procedure TSANRepExpEditorsManager.ExecuteWizard(const AWizard: TSANExpertsRepCustomWizard);
var
  vExpertNode, vModulesNode, vDialogNode : IXMLNode;
  vForm : TfrmSREDialog;
//------------------------------------------------------------------------------
  procedure BuildDialog(const ANodeItems : IXMLNodeList; const AParent : TWinControl);
  //----------------------------------------------------------------------------
    function AddEditor(const AItem : IXMLNode; var AEditorName : string) : boolean;
    var
      vBaseName : string;
      vEditor : ISANRepExpEditor;
      vEdtCtrl : ISANRepExpEditorControl;
    begin
      Result := False;

      if not AItem.HasAttribute('Editor') then
      begin
        AEditorName := 'Editor missing!';
        Exit;
      end;
      AEditorName := VarToStr(AItem.Attributes['Editor']);

      if AEditorName = '' then
      begin
        AEditorName := 'Empty editor name!';
        Exit;
      end;

      if not AItem.HasAttribute('BaseName') then
      begin
        AEditorName := AEditorName + ': BaseName missing!';
        Exit;
      end;
      vBaseName := VarToStr(AItem.Attributes['BaseName']);

      if not DoEditorsFind(vEditor, AEditorName) then
      begin
        AEditorName := AEditorName + ' not faund!';
        Exit;
      end;

      vEdtCtrl := vEditor.ControlCreate(AParent.Handle, vBaseName);
      try

        vEdtCtrl.IsStored := AItem.HasAttribute('Stored') and (AItem.Attributes['Stored'] <> 0);

        if AItem.HasAttribute('Warning') then
          vEdtCtrl.Warning := WideString(VarToStr(AItem.Attributes['Warning']));

      except
        vEdtCtrl.FreeControl;
        raise;
      end;
      vForm.AddEditorControl(vEdtCtrl);

      Result := True;
    end;
  //----------------------------------------------------------------------------
  var
    vItem : IXMLNode;
    i, vHeight : integer;
    vEditorName : string;
  begin

    for i := 0 to ANodeItems.Count - 1 do
    begin
      vItem := ANodeItems[i];

//      ShowMessage(vItem.NodeName);

      if CompareText(vItem.NodeName, 'LABEL') = 0 then
        vForm.AddLabel( VarToStr(vItem.Attributes['Caption']), AParent)

      else
      if CompareText(vItem.NodeName, 'SPACE') = 0 then
        begin

          if vItem.HasAttribute('Height') then
            vHeight := vItem.Attributes['Height']
          else
            vHeight := 8;

          vForm.AddSpace(vHeight, AParent);
        end

      else
      if CompareText(vItem.NodeName, 'GROUP') = 0 then
        BuildDialog
        (
          vItem.ChildNodes,
          vForm.AddGroup(VarToStr(vItem.Attributes['Caption']), AParent)
        )

      else
      if CompareText(vItem.NodeName, 'PARAM') = 0 then
        if not AddEditor(vItem, vEditorName) then
          vForm.AddNotEditor( vEditorName, AParent);
    end;

//    TWinControlHack(AParent).AutoSize := True;
//    vForm.AutoSizeWCtHeight(AParent);
  end;
//------------------------------------------------------------------------------
begin
  FXMLDoc.XML.Text := AWizard.XML;
  FXMLDoc.Active := True;
  try
    ValidateXML(vExpertNode, vModulesNode, vDialogNode);

    vForm := TfrmSREDialog.Create(nil);
    try
      vForm.Wizard := AWizard;

      if vDialogNode.HasAttribute('Width') then
        vForm.Width := vDialogNode.Attributes['Width'];

      BuildDialog(vDialogNode.ChildNodes, vForm.pnlClient);
      vForm.AutoSize := True;

      if vForm.ShowModal = mrOK then
      begin

      end;

    finally
      FreeAndNil(vForm);
    end;

  finally
    FXMLDoc.Active := False;
  end;
end;
//==============================================================================

var
  vSANRepExpEditorsManager : TSANRepExpEditorsManager;

procedure SANRepExpEditorsManagerInit;

  procedure LoadExtEditors;
  var
    vPath : string;
    vSR : TSearchRec;
  begin
    vPath := ExtractFilePath(ModuleFileName(HInstance));

//    ShowMessage(vPath);

    if FindFirst(vPath + '*.srpe', faAnyFile AND NOT faDirectory, vSR) = 0 then
      try

        repeat
//          ShowMessage(vPath + vSR.Name);

          vSANRepExpEditorsManager.Packages.Add(vPath + vSR.Name);

        until FindNext(vSR) > 0;

      finally
        FindClose(vSR);
      end;

  end;

begin
  vSANRepExpEditorsManager := TSANRepExpEditorsManager.Create(SRPE_ENTRY_POINT_NAME);

  LoadExtEditors;
end;

procedure SANRepExpEditorsManagerFinal;
begin
  vSANRepExpEditorsManager.Packages.Clear;

  FreeAndNil(vSANRepExpEditorsManager);
end;

function SANRepExpEditorsManager : TSANRepExpEditorsManager;
begin
  Result := vSANRepExpEditorsManager;
end;


INITIALIZATION
  SANRepExpEditorsManagerInit;

FINALIZATION
  SANRepExpEditorsManagerFinal;

END.
