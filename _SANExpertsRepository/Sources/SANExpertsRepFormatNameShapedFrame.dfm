inherited frameFormatNameShaped: TframeFormatNameShaped
  Height = 22
  ExplicitHeight = 22
  inherited shpWarning: TShape
    Height = 22
    ExplicitHeight = 26
  end
  inherited pnlClient: TPanel
    Height = 22
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 502
    ExplicitHeight = 22
    object bvlResults: TBevel
      Left = 383
      Top = 0
      Width = 9
      Height = 22
      Align = alLeft
      Shape = bsLeftLine
      ExplicitLeft = 463
      ExplicitTop = -64
      ExplicitHeight = 89
    end
    object lblNameResult: TLabel
      Left = 392
      Top = 0
      Width = 67
      Height = 13
      Align = alClient
      Caption = 'lblNameResult'
      ParentShowHint = False
      ShowHint = True
      Layout = tlCenter
    end
    object edtNameFmt: TEdit
      AlignWithMargins = True
      Left = 0
      Top = 0
      Width = 375
      Height = 22
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Align = alLeft
      TabOrder = 0
      Text = 'edtNameFmt'
      OnChange = edtNameFmtChange
      OnKeyPress = edtNameFmtKeyPress
      ExplicitHeight = 21
    end
  end
end
