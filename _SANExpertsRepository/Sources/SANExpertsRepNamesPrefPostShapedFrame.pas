{*******************************************************}
{                                                       }
{       SANExpertsRepNamesPrefPostShapedFrame           }
{       Фрейм ввода имени с префиксом и постфиксом.     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepNamesPrefPostShapedFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,

  SANMisc,

  SANExpertsRepCommon,
  SANExpertsRepCustomShapedFrame;

type
  /// <summary>
  /// Фрейм ввода имени с префиксом и постфиксом.
  /// </summary>
  TframeNamesPrefPostShaped = class(TframeShaped)
    edtNamePrefix: TEdit;
    lblNamePrefixHint: TLabel;
    edtName: TEdit;
    lblNameHint: TLabel;
    edtNamePostfix: TEdit;
    lblNamePostfixHint: TLabel;
    bvlResults: TBevel;
    lblNameResult: TLabel;
    pnlPrefix: TPanel;
    pnlName: TPanel;
    pnlPostfix: TPanel;
    procedure edtNameChange(Sender: TObject);
    procedure edtNameKeyPress(Sender: TObject; var Key: Char);

  private
    function GetPrefix: string;
    procedure SetPrefix(const Value: string);
    function GetMidName: string;
    procedure SetMidName(const Value: string);
    function GetPostfix: string;
    procedure SetPostfix(const Value: string);
    function GetFullName: string;

  protected
    function DoGetValidValues : boolean; override;

  public
    constructor Create(AOwner : TComponent); override;

    property Prefix    : string  read GetPrefix   write SetPrefix;
    property MidName   : string  read GetMidName  write SetMidName;
    property Postfix   : string  read GetPostfix  write SetPostfix;
    property FullName  : string  read GetFullName;
  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeNamesPrefPostShaped
// private
function TframeNamesPrefPostShaped.GetPrefix: string;
begin
  Result := edtNamePrefix.Text;
end;

procedure TframeNamesPrefPostShaped.SetPrefix(const Value: string);
begin
  edtNamePrefix.Text := Value;
end;

function TframeNamesPrefPostShaped.GetMidName: string;
begin
  Result := edtName.Text;
end;

procedure TframeNamesPrefPostShaped.SetMidName(const Value: string);
begin
  edtName.Text := Value;
end;

function TframeNamesPrefPostShaped.GetPostfix: string;
begin
  Result := edtNamePostfix.Text;
end;

procedure TframeNamesPrefPostShaped.SetPostfix(const Value: string);
begin
  edtNamePostfix.Text := Value;
end;

function TframeNamesPrefPostShaped.GetFullName: string;
begin
  Result := Prefix + MidName + Postfix;
end;

// protected
function TframeNamesPrefPostShaped.DoGetValidValues: boolean;
begin
  Result := CheckPasIdentifierName(FullName);
end;

// public
constructor TframeNamesPrefPostShaped.Create(AOwner: TComponent);
begin
  inherited;

  ClearEditsText;

  edtNamePrefix.Hint       := lblNamePrefixHint.Caption;
  edtName.Hint             := lblNameHint.Caption;
  edtNamePostfix.Hint      := lblNamePostfixHint.Caption;

  lblNamePrefixHint.Hint   := lblNamePrefixHint.Caption;
  lblNameHint.Hint         := lblNameHint.Caption;
  lblNamePostfixHint.Hint  := lblNamePostfixHint.Caption;
end;

// published
procedure TframeNamesPrefPostShaped.edtNameChange(Sender: TObject);
var
  vWarnHint : string;
begin
  lblNameResult.Caption := FullName;
  lblNameResult.Hint := lblNameResult.Caption;

  UpdateValue(Sender as TControl, (Sender as TEdit).Text, vWarnHint);

  UpdateWarnShape(DoGetValidValues, vWarnHint);
end;

procedure TframeNamesPrefPostShaped.edtNameKeyPress(Sender: TObject;
  var Key: Char);
var
  vSet : TSysCharSet;
begin

  vSet := ['a'..'z', '_', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)];

  if ((Sender as TEdit).Text <> '')

  or (Sender = edtName)
     and (edtNamePrefix.Text <> '')

  or (Sender = edtNamePostfix)
     and (edtNamePrefix.Text <> '') or (edtName.Text <> '')

  then
    vSet := vSet + ['0'..'9'];

  if not CharInSet(LowerCase(Key)[1], vSet) then
  begin
    Key := #0;
    MessageBeep(MB_OK);
  end;
end;
//==============================================================================

END.
