object frameCustomEditor: TframeCustomEditor
  Left = 0
  Top = 0
  Width = 320
  Height = 24
  TabOrder = 0
  object shpWarning: TShape
    AlignWithMargins = True
    Left = 305
    Top = 0
    Width = 7
    Height = 24
    Margins.Left = 8
    Margins.Top = 0
    Margins.Right = 8
    Margins.Bottom = 0
    Align = alRight
    Brush.Color = clLime
    ParentShowHint = False
    Shape = stCircle
    ShowHint = True
    ExplicitLeft = 313
    ExplicitHeight = 25
  end
  object pnlClient: TPanel
    Left = 0
    Top = 0
    Width = 297
    Height = 24
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnlClient'
    ShowCaption = False
    TabOrder = 0
  end
end
