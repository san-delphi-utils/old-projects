{*******************************************************}
{                                                       }
{       SANExpertsRepVisCaptionFormatShapedFrame        }
{       Фрейм ввода заголовка окна визуалайзера.        }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepVisCaptionFormatShapedFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,

  SANExpertsRepCustomShapedFrame;

type
  /// <summary>
  /// Фрейм ввода заголовка окна визуалайзера.
  /// </summary>
  TframeVisCaptionFormatShaped = class(TframeShaped)
    edtCaptionFmt: TEdit;
    procedure edtCaptionFmtChange(Sender: TObject);

  private
    function GetVisCaption: string;
    procedure SetVisCaption(const Value: string);

  protected
    function DoGetValidValues : boolean; override;

  public
    constructor Create(AOwner : TComponent); override;

    property VisCaption : string  read GetVisCaption  write SetVisCaption;
  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeVisCaptionFormatShaped
// private
function TframeVisCaptionFormatShaped.GetVisCaption: string;
begin
  Result := edtCaptionFmt.Text;
end;

procedure TframeVisCaptionFormatShaped.SetVisCaption(const Value: string);
begin
  edtCaptionFmt.Text := Value;
end;

// protected
function TframeVisCaptionFormatShaped.DoGetValidValues: boolean;
begin
  try
    Format(edtCaptionFmt.Text, ['S1', 'S2', 'S3']);
    Result := True;
  except
    Result := False;
  end;
end;

// public
constructor TframeVisCaptionFormatShaped.Create(AOwner: TComponent);
begin
  inherited;

  ClearEditsText;
end;

// published
procedure TframeVisCaptionFormatShaped.edtCaptionFmtChange(Sender: TObject);
var
  vWarnHint : string;
begin
  UpdateValue(Sender as TControl, (Sender as TEdit).Text, vWarnHint);

  UpdateWarnShape(DoGetValidValues, vWarnHint);
end;
//==============================================================================


END.
