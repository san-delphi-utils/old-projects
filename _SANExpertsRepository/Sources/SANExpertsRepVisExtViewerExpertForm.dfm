inherited frmVisExtViewerExpert: TfrmVisExtViewerExpert
  Caption = 'frmVisExtViewerExpert'
  ClientHeight = 488
  ExplicitWidth = 543
  ExplicitHeight = 516
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlButtons: TPanel
    Top = 447
    ExplicitTop = 447
  end
  inherited pnlClient: TPanel
    Height = 447
    Padding.Left = 6
    Padding.Right = 6
    ExplicitHeight = 447
    object gbNames: TGroupBox
      Left = 6
      Top = 0
      Width = 525
      Height = 164
      Align = alTop
      Caption = #1048#1084#1077#1085#1072
      Padding.Left = 6
      Padding.Right = 6
      Padding.Bottom = 6
      TabOrder = 0
      object lblDLGName: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 15
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        Caption = #1048#1084#1103' '#1074#1080#1079#1091#1072#1083#1072#1081#1079#1077#1088#1072':'
        ExplicitWidth = 96
      end
      object lblUnitNameFmt: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 71
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1084#1086#1076#1091#1083#1103' (%s - '#1080#1084#1103' '#1074#1080#1079#1091#1072#1083#1072#1081#1079#1077#1088#1072'):'
        ExplicitWidth = 243
      end
      object lblFrameNameFmt: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 117
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1092#1088#1077#1081#1084#1072' (%s - '#1080#1084#1103' '#1074#1080#1079#1091#1072#1083#1072#1081#1079#1077#1088#1072'):'
        ExplicitWidth = 244
      end
      inline frameNames: TframeNamesPrefPostShaped
        Left = 8
        Top = 31
        Width = 509
        Height = 32
        Align = alTop
        TabOrder = 0
        ExplicitLeft = 8
        ExplicitTop = 31
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 576
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
          inherited lblNameResult: TLabel
            Width = 87
            Height = 32
          end
          inherited pnlPrefix: TPanel
            inherited lblNamePrefixHint: TLabel
              Width = 130
            end
          end
          inherited pnlName: TPanel
            inherited lblNameHint: TLabel
              Width = 130
            end
          end
          inherited pnlPostfix: TPanel
            inherited lblNamePostfixHint: TLabel
              Width = 130
            end
          end
        end
      end
      inline frameFmtUnitName: TframeFormatNameShaped
        Left = 8
        Top = 87
        Width = 509
        Height = 22
        Align = alTop
        TabOrder = 1
        ExplicitLeft = 8
        ExplicitTop = 87
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 548
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
          inherited bvlResults: TBevel
            Left = 390
            ExplicitLeft = 390
          end
          inherited lblNameResult: TLabel
            Left = 399
            Width = 87
            Height = 22
            ExplicitLeft = 399
          end
          inherited edtNameFmt: TEdit
            Width = 382
            ExplicitWidth = 382
          end
        end
      end
      inline frameFmtFrameName: TframeFormatNameShaped
        Left = 8
        Top = 133
        Width = 509
        Height = 22
        Align = alTop
        TabOrder = 2
        ExplicitLeft = 8
        ExplicitTop = 133
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 548
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
          inherited bvlResults: TBevel
            Left = 390
            ExplicitLeft = 390
          end
          inherited lblNameResult: TLabel
            Left = 399
            Width = 87
            Height = 22
            ExplicitLeft = 399
          end
          inherited edtNameFmt: TEdit
            Width = 382
            ExplicitWidth = 382
          end
        end
      end
    end
    object gbParams: TGroupBox
      Left = 6
      Top = 164
      Width = 525
      Height = 283
      Align = alClient
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1074#1080#1079#1091#1072#1083#1072#1081#1079#1077#1088#1072
      Padding.Left = 6
      Padding.Right = 6
      Padding.Bottom = 6
      TabOrder = 1
      object lblGUID: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 15
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1081' '#1080#1076#1077#1085#1090#1080#1092#1080#1082#1072#1090#1086#1088':'
        ExplicitWidth = 151
      end
      object lblSupportedTypes: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 108
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1055#1086#1076#1076#1077#1088#1078#1080#1074#1072#1077#1084#1099#1077' '#1090#1080#1087#1099':'
        ExplicitWidth = 124
      end
      object lblDescription: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 227
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alBottom
        Caption = #1054#1087#1080#1089#1072#1085#1080#1077':'
        ExplicitWidth = 53
      end
      object lblFormCaption: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 62
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = 
          #1047#1072#1075#1086#1083#1086#1074#1086#1082' '#1092#1086#1088#1084#1099' '#1074#1080#1079#1091#1072#1083#1072#1081#1079#1077#1088#1072' (%0:s '#8212' Expression, %1:s '#8212' TypeName' +
          ', %2:s '#8212' EvalResult):'
        ExplicitWidth = 471
      end
      inline frameGUID: TframeGUIDShaped
        Left = 8
        Top = 31
        Width = 509
        Height = 23
        Align = alTop
        TabOrder = 0
        ExplicitLeft = 8
        ExplicitTop = 31
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 494
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
          inherited sbtnGUID: TSpeedButton
            Left = 464
            ExplicitLeft = 464
          end
          inherited medtGUID: TMaskEdit
            Width = 464
            ExplicitWidth = 464
          end
        end
      end
      inline frameSupTypes: TframeIdtfsListShaped
        Left = 8
        Top = 124
        Width = 509
        Height = 95
        Align = alClient
        TabOrder = 1
        ExplicitLeft = 8
        ExplicitTop = 124
        ExplicitWidth = 509
        ExplicitHeight = 95
        inherited shpWarning: TShape
          Left = 494
          Height = 95
          ExplicitLeft = 494
          ExplicitHeight = 123
        end
        inherited pnlClient: TPanel
          Width = 486
          Height = 95
          ExplicitWidth = 486
          ExplicitHeight = 95
          inherited mmIdtfs: TMemo
            Width = 486
            Height = 95
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 486
            ExplicitHeight = 95
          end
        end
      end
      object mmDescription: TMemo
        AlignWithMargins = True
        Left = 8
        Top = 243
        Width = 486
        Height = 32
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 23
        Margins.Bottom = 0
        Align = alBottom
        Lines.Strings = (
          'mmDescription')
        TabOrder = 2
      end
      inline frameVisCaption: TframeVisCaptionFormatShaped
        Left = 8
        Top = 78
        Width = 509
        Height = 22
        Align = alTop
        TabOrder = 3
        ExplicitLeft = 8
        ExplicitTop = 78
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 494
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
          ExplicitHeight = 22
          inherited edtCaptionFmt: TEdit
            Width = 486
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 486
          end
        end
      end
    end
  end
end
