{*******************************************************}
{                                                       }
{       SANExpertsRepVisExtViewerExpertForm             }
{       Эксперт создания визуалайзера                   }
{       расширенного просмотра.                         }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepVisExtViewerExpertForm;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, TypInfo,

  SANMisc, SANINIProps, SANWinUtils,

  SANDesignReplacer, SANDesignCreaters, SANDesignWizards,

  SANExpertsRepCommon,
  SANExpertsRepCustomExpertDialogForm, SANExpertsRepFormatNameShapedFrame,
  SANExpertsRepCustomShapedFrame, SANExpertsRepNamesPrefPostShapedFrame,
  SANExpertsRepGUIDShapedFrame, SANExpertsRepIdtfsListShapedFrame,
  SANExpertsRepVisCaptionFormatShapedFrame;

TYPE
  TfrmVisExtViewerExpert = class;

  TVisExtViewerProps = class(TCustomExpertProps)
  private
    FVisNamePostfix: string;
    FUnitNameFmt: string;
    FVisNamePrefix: string;
    FFrameNameFmt: string;
    FVisCaption: string;

    function GetForm: TfrmVisExtViewerExpert;

    procedure SetVisNamePrefix(const Value: string);
    procedure SetVisNamePostfix(const Value: string);
    procedure SetUnitNameFmt(const Value: string);
    procedure SetFrameNameFmt(const Value: string);
    procedure SetVisCaption(const Value: string);

  protected
    procedure DoDefaultStrPropValue(const APropInfo: PPropInfo); override;

  public
    property Form : TfrmVisExtViewerExpert  read GetForm;

  published
    property VisNamePrefix   : string  read FVisNamePrefix   write SetVisNamePrefix;
    property VisNamePostfix  : string  read FVisNamePostfix  write SetVisNamePostfix;
    property UnitNameFmt     : string  read FUnitNameFmt     write SetUnitNameFmt;
    property FrameNameFmt    : string  read FFrameNameFmt    write SetFrameNameFmt;
    property VisCaption      : string  read FVisCaption      write SetVisCaption;
  end;

  TfrmVisExtViewerExpert = class(TfrmCustomExpertDialog)
    gbNames: TGroupBox;
    lblDLGName: TLabel;
    lblUnitNameFmt: TLabel;
    frameNames: TframeNamesPrefPostShaped;
    frameFmtUnitName: TframeFormatNameShaped;
    lblFrameNameFmt: TLabel;
    frameFmtFrameName: TframeFormatNameShaped;
    gbParams: TGroupBox;
    lblGUID: TLabel;
    lblSupportedTypes: TLabel;
    lblDescription: TLabel;
    frameGUID: TframeGUIDShaped;
    frameSupTypes: TframeIdtfsListShaped;
    mmDescription: TMemo;
    lblFormCaption: TLabel;
    frameVisCaption: TframeVisCaptionFormatShaped;
    procedure FormCreate(Sender: TObject);
  private

  private
    function GetProps: TVisExtViewerProps;

  protected
    function GetINIFileName: TFileName; override;
    function ExpertPropsClass : TCustomExpertPropsClass; override;
    procedure DoWarnShapeValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string); override;
    procedure UpdateFmtNames;

  public
    procedure FillReplacer(const AReplacer : TSANReplacer); override;

    property Props : TVisExtViewerProps  read GetProps;

  end;

  /// <summary>
  /// Эксперт создания визуалайзера расширнного просмотра.
  /// </summary>
  TSANRepExpertVisExtViewer = class(TSANCustomRepExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetGlyphResName : string; override;
    function DLGFormClass : TFormClass; override;
    procedure DoExecute(const ADLGForm : TForm; const AReplacer : TSANReplacer); override;
  end;

IMPLEMENTATION

{$R *.dfm}

const
  ICON_RES_NAME = 'SAN_EXP_REP_ICON_VISEXTVIEWER';

resourcestring
  sExpertCaption = 'Эксперт создания визуалайзера расширнного просмотра';

//==============================================================================
// TVisExtViewerProps
// private
function TVisExtViewerProps.GetForm: TfrmVisExtViewerExpert;
begin
  Result := (inherited Form) as TfrmVisExtViewerExpert;
end;

procedure TVisExtViewerProps.SetVisNamePrefix(const Value: string);
begin
  if FVisNamePrefix = Value then
    Exit;

  FVisNamePrefix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.edtNamePrefix.Text := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TVisExtViewerProps.SetVisNamePostfix(const Value: string);
begin
  if FVisNamePostfix = Value then
    Exit;

  FVisNamePostfix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.edtNamePostfix.Text := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TVisExtViewerProps.SetUnitNameFmt(const Value: string);
begin
  if FUnitNameFmt = Value then
    Exit;

  FUnitNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtUnitName.edtNameFmt.Text := Value;
end;

procedure TVisExtViewerProps.SetFrameNameFmt(const Value: string);
begin
  if FFrameNameFmt = Value then
    Exit;

  FFrameNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtFrameName.edtNameFmt.Text := Value;
end;

procedure TVisExtViewerProps.SetVisCaption(const Value: string);
begin
  if FVisCaption = Value then
    Exit;

  FVisCaption := Value;

  if Assigned(Form) then
    Form.frameVisCaption.edtCaptionFmt.Text := Value;
end;

// protected
procedure TVisExtViewerProps.DoDefaultStrPropValue(const APropInfo: PPropInfo);
begin

  if CompareText(String(APropInfo^.Name), 'VisNamePostfix') = 0 then
    SetStrProp(Self, APropInfo, 'Visualiser')

  else
  if CompareText(String(APropInfo^.Name), 'UnitNameFmt') = 0 then
    SetStrProp(Self, APropInfo, '%s_u')

  else
  if CompareText(String(APropInfo^.Name), 'FrameNameFmt') = 0 then
    SetStrProp(Self, APropInfo, 'frame%s')

  else
  if CompareText(String(APropInfo^.Name), 'VisCaption') = 0 then
    SetStrProp(Self, APropInfo, 'Visualiser for %s')

  else
    inherited;

end;
//==============================================================================

//==============================================================================
// TfrmVisExtViewerExpert
// private
function TfrmVisExtViewerExpert.GetProps: TVisExtViewerProps;
begin
  Result := (inherited Props) as TVisExtViewerProps;
end;

// protected
function TfrmVisExtViewerExpert.GetINIFileName: TFileName;
begin
  Result := 'VisExtViewer.ini';
end;

function TfrmVisExtViewerExpert.ExpertPropsClass: TCustomExpertPropsClass;
begin
  Result := TVisExtViewerProps;
end;

procedure TfrmVisExtViewerExpert.DoWarnShapeValueUpdated(const AFrame: TFrame;
  const AControl: TControl; const AValue: Variant; var AWarnHint: string);
begin

  if AFrame = frameNames then
    begin
      AWarnHint := 'Некорректное имя визуалайзера';

      if AControl = frameNames.edtNamePrefix  then
        Props.VisNamePrefix := AValue

      else
      if AControl = frameNames.edtNamePostfix then
        Props.VisNamePostfix := AValue

      else
      if AControl = frameNames.edtName then
        UpdateFmtNames;

    end

  else
  if AFrame = frameFmtUnitName then
    begin
      AWarnHint := 'Некорректный формат имени модуля';
      Props.UnitNameFmt := AValue;
    end

  else
  if AFrame = frameFmtFrameName then
    begin
      AWarnHint := 'Некорректный формат имени фрейма';
      Props.FrameNameFmt := AValue;
    end

  else
  if AFrame = frameGUID then
    AWarnHint := 'Некорректный GUID'

  else
  if AFrame = frameVisCaption then
    begin
      AWarnHint := 'Некорректный заголовок';
      Props.VisCaption := AValue;
    end

  else
  if AFrame = frameSupTypes then
    AWarnHint := 'Некорректный список типов'

end;

procedure TfrmVisExtViewerExpert.UpdateFmtNames;
begin
  frameFmtUnitName.NameForFmt   := frameNames.FullName;
  frameFmtFrameName.NameForFmt  := frameNames.FullName;
end;

// public
procedure TfrmVisExtViewerExpert.FillReplacer(const AReplacer: TSANReplacer);
begin
  AReplacer.AddPair('VisName', frameNames.FullName);

  AReplacer.AddPair('VisDesc', mmDescription.Text);

  AReplacer.AddPair('VisGUID', frameGUID.GUID);

  AReplacer.AddPair('VisStaticSupTypes', VisualisersBuildStaticSupTypes(frameSupTypes.Idtfs));

  AReplacer.AddPair('VisCaption', Props.VisCaption);
end;

// published
procedure TfrmVisExtViewerExpert.FormCreate(Sender: TObject);
begin
  Caption := LoadResString(@sExpertCaption);
  Icon.LoadFromResourceName(HInstance, ICON_RES_NAME);

  mmDescription.Text := '';

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANRepExpertVisExtViewer
// protected
function TSANRepExpertVisExtViewer.GetIDString: string;
begin
  Result := 'SAN.RepExp.VisExtViewer';
end;

function TSANRepExpertVisExtViewer.GetName: string;
begin
  Result := 'Visualiser Extended Viewer';
end;

function TSANRepExpertVisExtViewer.GetComment: string;
begin
  Result := LoadResString(@sExpertCaption);
end;

function TSANRepExpertVisExtViewer.GetGlyphResName: string;
begin
  Result := ICON_RES_NAME;
end;

function TSANRepExpertVisExtViewer.DLGFormClass: TFormClass;
begin
  Result := TfrmVisExtViewerExpert;
end;

procedure TSANRepExpertVisExtViewer.DoExecute(const ADLGForm: TForm;
  const AReplacer: TSANReplacer);
var
  vForm : TfrmVisExtViewerExpert;
  vModule : IOTAModule;
begin
  vForm := TfrmVisExtViewerExpert(ADLGForm);

  vModule := TSANStaticModuleCreator.CreateModule
  (
    ctForm,
    vForm.frameNames.FullName,
    '',
    vForm.Props.FrameNameFmt,
    vForm.Props.UnitNameFmt,
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_VISEXTVIEWER_DFM'),
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_VISEXTVIEWER_PAS'),
    AReplacer
  );

  GetActiveProject.AddFile(vModule.FileName, True);

  vModule.Show;
end;
//==============================================================================


END.
