{*******************************************************}
{                                                       }
{       SANExpertsRepCustomShapedFrame                  }
{       Абстрактный фрейм c шейпом корректности ввода.  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepCustomShapedFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,

  SANExpertsRepCommon;

type
  /// <summary>
  /// Абстрактный фрейм c шейпом корректности ввода.
  /// </summary>
  TframeShaped = class(TFrame, IWarnShapeValidator, IWarnShapeValidatorHostProvider)
    shpWarning: TShape;
    pnlClient: TPanel;

  private
//    FPropsNames : TStrings;

  protected
    // IWarnShapeValidator
    function GetValidValues : boolean;

    // IWarnShapeValidatorHostProvider
    function GetWarnShapeValidatorHost : IWarnShapeValidatorHost;

    function DoGetValidValues : boolean; virtual; abstract;

    procedure UpdateWarnShape(const AValidateCondition: boolean = True; const AWarningHint: string = '');

    procedure UpdateValue(const AControl : TControl; const AValue : Variant; var AWarnHint : string);

    procedure ClearEditsText;

  public

  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeShaped
function TframeShaped.GetValidValues: boolean;
begin
  Result := DoGetValidValues;
end;

function TframeShaped.GetWarnShapeValidatorHost: IWarnShapeValidatorHost;
var
  vProvider : IWarnShapeValidatorHostProvider;
begin

  if Supports(Owner, IWarnShapeValidatorHostProvider, vProvider) then
    Result := vProvider.WarnShapeValidatorHost

  else
  if not Supports(Owner, IWarnShapeValidatorHost, Result) then
    Result := nil;

end;

procedure TframeShaped.UpdateWarnShape(const AValidateCondition: boolean;
  const AWarningHint: string);
begin
  SANExpertsRepCommon.UpdateWarnShape(shpWarning, AValidateCondition, AWarningHint);
end;

procedure TframeShaped.UpdateValue(const AControl: TControl; const AValue: Variant;
  var AWarnHint : string);
var
  vHost : IWarnShapeValidatorHost;
begin
  vHost := GetWarnShapeValidatorHost;

  if Assigned(vHost) then
    vHost.ValueUpdated(Self, AControl, AValue, AWarnHint);
end;

// public
procedure TframeShaped.ClearEditsText;
var
  i : integer;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TCustomEdit then
      TCustomEdit(Components[i]).Text := '';
end;
//==============================================================================

END.
