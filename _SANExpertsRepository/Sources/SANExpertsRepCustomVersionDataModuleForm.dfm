inherited frmCustomVersionDataModule: TfrmCustomVersionDataModule
  Caption = 'frmCustomVersionDataModule'
  ClientHeight = 558
  ExplicitWidth = 543
  ExplicitHeight = 586
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlButtons: TPanel
    Top = 517
    ExplicitTop = 517
  end
  inherited pnlClient: TPanel
    Height = 517
    Padding.Left = 6
    Padding.Right = 6
    ExplicitHeight = 517
    object gbNames: TGroupBox
      Left = 6
      Top = 0
      Width = 525
      Height = 164
      Align = alTop
      Caption = #1048#1084#1077#1085#1072
      Padding.Left = 6
      Padding.Right = 6
      Padding.Bottom = 6
      TabOrder = 0
      object lblDMName: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 15
        Width = 23
        Height = 13
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        Caption = #1048#1084#1103':'
      end
      object lblFormNameFmt: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 71
        Width = 212
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1084#1086#1076#1091#1083#1103' '#1076#1072#1085#1085#1099#1093' (%s - '#1080#1084#1103'):'
      end
      object lblUnitNameFmt: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 117
        Width = 170
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1084#1086#1076#1091#1083#1103' (%s - '#1080#1084#1103'):'
      end
      inline frameNames: TframeNamesPrefPostShaped
        Left = 8
        Top = 31
        Width = 509
        Height = 32
        Align = alTop
        TabOrder = 0
        ExplicitLeft = 8
        ExplicitTop = 31
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 576
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
        end
      end
      inline frameFmtFormName: TframeFormatNameShaped
        Left = 8
        Top = 87
        Width = 509
        Height = 22
        Align = alTop
        TabOrder = 1
        ExplicitLeft = 8
        ExplicitTop = 87
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 576
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
          inherited bvlResults: TBevel
            Left = 390
            ExplicitLeft = 390
          end
          inherited lblNameResult: TLabel
            Left = 399
            ExplicitLeft = 399
          end
          inherited edtNameFmt: TEdit
            Width = 382
            ExplicitWidth = 382
          end
        end
      end
      inline frameFmtUnitName: TframeFormatNameShaped
        Left = 8
        Top = 133
        Width = 509
        Height = 22
        Align = alTop
        TabOrder = 2
        ExplicitLeft = 8
        ExplicitTop = 133
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 548
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
          inherited bvlResults: TBevel
            Left = 390
            ExplicitLeft = 390
          end
          inherited lblNameResult: TLabel
            Left = 399
            ExplicitLeft = 399
          end
          inherited edtNameFmt: TEdit
            Width = 382
            ExplicitWidth = 382
          end
        end
      end
    end
    object gbParams: TGroupBox
      Left = 6
      Top = 164
      Width = 525
      Height = 353
      Align = alClient
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1074#1077#1088#1089#1080#1080
      Padding.Left = 6
      Padding.Right = 6
      Padding.Bottom = 6
      TabOrder = 1
      object lblGUID: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 15
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1081' '#1080#1076#1077#1085#1090#1080#1092#1080#1082#1072#1090#1086#1088':'
        ExplicitWidth = 151
      end
      object lblEntryPointName: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 31
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        Caption = #1048#1084#1103' '#1092#1091#1085#1082#1094#1080#1080' '#1090#1086#1095#1082#1080' '#1074#1093#1086#1076#1072':'
        ExplicitWidth = 137
      end
      object lblDescription: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 123
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1054#1087#1080#1089#1072#1085#1080#1077':'
        ExplicitWidth = 53
      end
      object lblCaption: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 55
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1042#1085#1077#1096#1085#1077#1077' '#1080#1084#1103':'
        ExplicitWidth = 69
      end
      object lblAuthor: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 216
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alBottom
        Caption = #1040#1074#1090#1086#1088':'
        ExplicitWidth = 35
      end
      object lblURL: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 261
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alBottom
        Caption = 'URL:'
        ExplicitWidth = 23
      end
      object lblVersion: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 306
        Width = 509
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alBottom
        Caption = #1042#1077#1088#1089#1080#1103':'
        ExplicitWidth = 39
      end
      inline frameGUID: TframeGUIDShaped
        Left = 8
        Top = 139
        Width = 509
        Height = 23
        Align = alTop
        TabOrder = 5
        ExplicitLeft = 8
        ExplicitTop = 78
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 494
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitWidth = 486
          inherited sbtnGUID: TSpeedButton
            Left = 464
            ExplicitLeft = 464
          end
          inherited medtGUID: TMaskEdit
            Width = 464
            ExplicitWidth = 464
          end
        end
      end
      object mmDescription: TMemo
        AlignWithMargins = True
        Left = 8
        Top = 162
        Width = 486
        Height = 46
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 23
        Margins.Bottom = 0
        Align = alClient
        Lines.Strings = (
          'mmDescription')
        TabOrder = 0
        ExplicitTop = 170
        ExplicitHeight = 38
      end
      inline frameEntryPointName: TframeEntryPointNameShaped
        Left = 8
        Top = 71
        Width = 509
        Height = 23
        Align = alTop
        TabOrder = 6
        ExplicitLeft = 8
        ExplicitTop = 31
        ExplicitWidth = 509
        inherited shpWarning: TShape
          Left = 494
          ExplicitLeft = 494
        end
        inherited pnlClient: TPanel
          Width = 486
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 486
          inherited sbtnGenerate: TSpeedButton
            Left = 464
            ExplicitLeft = 464
          end
          inherited edtName: TEdit
            Width = 464
            ExplicitLeft = 0
            ExplicitTop = 1
            ExplicitWidth = 464
          end
        end
      end
      object edtCaption: TEdit
        AlignWithMargins = True
        Left = 8
        Top = 94
        Width = 486
        Height = 21
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 23
        Margins.Bottom = 0
        Align = alTop
        TabOrder = 1
        Text = 'edtCaption'
        ExplicitTop = 125
      end
      object edtAuthor: TEdit
        AlignWithMargins = True
        Left = 8
        Top = 232
        Width = 486
        Height = 21
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 23
        Margins.Bottom = 0
        Align = alBottom
        TabOrder = 2
        Text = 'edtAuthor'
      end
      object edtURL: TEdit
        AlignWithMargins = True
        Left = 8
        Top = 277
        Width = 486
        Height = 21
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 23
        Margins.Bottom = 0
        Align = alBottom
        TabOrder = 4
        Text = 'edtURL'
      end
      object pnlVersion: TPanel
        Left = 8
        Top = 322
        Width = 509
        Height = 23
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'pnlVersion'
        Padding.Left = 8
        ShowCaption = False
        TabOrder = 3
        object lblMajor: TLabel
          Left = 9
          Top = 7
          Width = 31
          Height = 13
          Caption = 'Major:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblMinor: TLabel
          Left = 98
          Top = 7
          Width = 30
          Height = 13
          Caption = 'Minor:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblRelease: TLabel
          Left = 186
          Top = 7
          Width = 42
          Height = 13
          Caption = 'Release:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblBuild: TLabel
          Left = 286
          Top = 7
          Width = 26
          Height = 13
          Caption = 'Build:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object seMajor: TSpinEdit
          Left = 43
          Top = 3
          Width = 46
          Height = 23
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 3
          MaxValue = 255
          MinValue = 0
          ParentFont = False
          TabOrder = 0
          Value = 0
        end
        object seMinor: TSpinEdit
          Left = 131
          Top = 3
          Width = 46
          Height = 23
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 3
          MaxValue = 255
          MinValue = 0
          ParentFont = False
          TabOrder = 1
          Value = 0
        end
        object seRelease: TSpinEdit
          Left = 231
          Top = 3
          Width = 46
          Height = 23
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 3
          MaxValue = 255
          MinValue = 0
          ParentFont = False
          TabOrder = 2
          Value = 0
        end
        object seBuild: TSpinEdit
          Left = 314
          Top = 3
          Width = 46
          Height = 23
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 3
          MaxValue = 255
          MinValue = 0
          ParentFont = False
          TabOrder = 3
          Value = 0
        end
      end
    end
  end
end
