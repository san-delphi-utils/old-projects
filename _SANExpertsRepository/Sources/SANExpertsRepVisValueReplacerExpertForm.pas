{*******************************************************}
{                                                       }
{       SANExpertsRepVisValueReplacerExpertForm         }
{       Эксперт создания визуалайзера замены значения.  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepVisValueReplacerExpertForm;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, TypInfo,

  SANMisc, SANINIProps, SANWinUtils,

  SANDesignReplacer, SANDesignCreaters, SANDesignWizards,

  SANExpertsRepCommon,
  SANExpertsRepCustomExpertDialogForm, SANExpertsRepFormatNameShapedFrame,
  SANExpertsRepCustomShapedFrame, SANExpertsRepNamesPrefPostShapedFrame,
  SANExpertsRepGUIDShapedFrame, SANExpertsRepIdtfsListShapedFrame;

TYPE
  TfrmVisValueReplacerExpert = class;

  TVisValueReplacerProps = class(TCustomExpertProps)
  private
    FVisNamePostfix: string;
    FUnitNameFmt: string;
    FVisNamePrefix: string;

    function GetForm: TfrmVisValueReplacerExpert;

    procedure SetVisNamePrefix(const Value: string);
    procedure SetVisNamePostfix(const Value: string);
    procedure SetUnitNameFmt(const Value: string);

  protected
    procedure DoDefaultStrPropValue(const APropInfo: PPropInfo); override;

  public
    property Form : TfrmVisValueReplacerExpert  read GetForm;

  published
    property VisNamePrefix   : string  read FVisNamePrefix   write SetVisNamePrefix;
    property VisNamePostfix  : string  read FVisNamePostfix  write SetVisNamePostfix;
    property UnitNameFmt     : string  read FUnitNameFmt     write SetUnitNameFmt;

  end;

  TfrmVisValueReplacerExpert = class(TfrmCustomExpertDialog)
    gbNames: TGroupBox;
    lblDLGName: TLabel;
    lblUnitNameFmt: TLabel;
    frameNames: TframeNamesPrefPostShaped;
    frameFmtUnitName: TframeFormatNameShaped;
    gbParams: TGroupBox;
    lblGUID: TLabel;
    frameGUID: TframeGUIDShaped;
    lblSupportedTypes: TLabel;
    frameSupTypes: TframeIdtfsListShaped;
    lblDescription: TLabel;
    mmDescription: TMemo;
    procedure FormCreate(Sender: TObject);

  private
    function GetProps: TVisValueReplacerProps;

  protected
    function GetINIFileName: TFileName; override;
    function ExpertPropsClass : TCustomExpertPropsClass; override;
    procedure DoWarnShapeValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string); override;
    procedure UpdateFmtNames;

  public
    procedure FillReplacer(const AReplacer : TSANReplacer); override;

    property Props : TVisValueReplacerProps  read GetProps;

  end;

  /// <summary>
  /// Эксперт создания визуалайзера замены значения.
  /// </summary>
  TSANRepExpertVisValueReplacer = class(TSANCustomRepExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetGlyphResName : string; override;
    function DLGFormClass : TFormClass; override;
    procedure DoExecute(const ADLGForm : TForm; const AReplacer : TSANReplacer); override;
  end;


IMPLEMENTATION

{$R *.dfm}

const
  ICON_RES_NAME = 'SAN_EXP_REP_ICON_VISVALUEREPLACER';

resourcestring
  sExpertCaption = 'Эксперт создания визуалайзера замены значения';

//==============================================================================
// TVisValueReplacerProps
// private
function TVisValueReplacerProps.GetForm: TfrmVisValueReplacerExpert;
begin
  Result := (inherited Form) as TfrmVisValueReplacerExpert;
end;

procedure TVisValueReplacerProps.SetVisNamePrefix(const Value: string);
begin
  if FVisNamePrefix = Value then
    Exit;

  FVisNamePrefix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.edtNamePrefix.Text := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TVisValueReplacerProps.SetVisNamePostfix(const Value: string);
begin
  if FVisNamePostfix = Value then
    Exit;

  FVisNamePostfix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.edtNamePostfix.Text := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TVisValueReplacerProps.SetUnitNameFmt(const Value: string);
begin
  if FUnitNameFmt = Value then
    Exit;

  FUnitNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtUnitName.edtNameFmt.Text := Value;
end;

// protected
procedure TVisValueReplacerProps.DoDefaultStrPropValue(const APropInfo: PPropInfo);
begin

  if CompareText(String(APropInfo^.Name), 'VisNamePostfix') = 0 then
    SetStrProp(Self, APropInfo, 'Visualiser')

  else
  if CompareText(String(APropInfo^.Name), 'UnitNameFmt') = 0 then
    SetStrProp(Self, APropInfo, '%s_u')

  else
    inherited;

end;
//==============================================================================

//==============================================================================
// TfrmRepVisValueReplacerExpert
// private
function TfrmVisValueReplacerExpert.GetProps: TVisValueReplacerProps;
begin
  Result := (inherited Props) as TVisValueReplacerProps;
end;

// protected
function TfrmVisValueReplacerExpert.GetINIFileName: TFileName;
begin
  Result := 'VisValueReplacer.ini';
end;

function TfrmVisValueReplacerExpert.ExpertPropsClass: TCustomExpertPropsClass;
begin
  Result := TVisValueReplacerProps;
end;

procedure TfrmVisValueReplacerExpert.DoWarnShapeValueUpdated(
  const AFrame: TFrame; const AControl: TControl; const AValue: Variant;
  var AWarnHint: string);
begin

  if AFrame = frameNames then
    begin
      AWarnHint := 'Некорректное имя визуалайзера';

      if AControl = frameNames.edtNamePrefix  then
        Props.VisNamePrefix := AValue

      else
      if AControl = frameNames.edtNamePostfix then
        Props.VisNamePostfix := AValue

      else
      if AControl = frameNames.edtName then
        UpdateFmtNames;

    end

  else
  if AFrame = frameFmtUnitName then
    begin
      AWarnHint := 'Некорректный формат имени модуля';
      Props.UnitNameFmt := AValue;
    end

  else
  if AFrame = frameGUID then
    AWarnHint := 'Некорректный GUID'

  else
  if AFrame = frameSupTypes then
    AWarnHint := 'Некорректный список типов'

end;

procedure TfrmVisValueReplacerExpert.UpdateFmtNames;
begin
  frameFmtUnitName.NameForFmt := frameNames.FullName;
end;

// public
procedure TfrmVisValueReplacerExpert.FillReplacer(const AReplacer: TSANReplacer);
begin
  AReplacer.AddPair('VisName', frameNames.FullName);

  AReplacer.AddPair('VisDesc', mmDescription.Text);

  AReplacer.AddPair('VisGUID', frameGUID.GUID);

  AReplacer.AddPair('VisStaticSupTypes', VisualisersBuildStaticSupTypes(frameSupTypes.Idtfs));
end;

// published
procedure TfrmVisValueReplacerExpert.FormCreate(Sender: TObject);
begin
  Caption := LoadResString(@sExpertCaption);
  Icon.LoadFromResourceName(HInstance, ICON_RES_NAME);

  mmDescription.Text := '';

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANRepExpertVisValueReplacer
// protected
function TSANRepExpertVisValueReplacer.GetIDString: string;
begin
  Result := 'SAN.RepExp.VisValueReplacer';
end;

function TSANRepExpertVisValueReplacer.GetName: string;
begin
  Result := 'Visualiser Value Replacer';
end;

function TSANRepExpertVisValueReplacer.GetComment: string;
begin
  Result := LoadResString(@sExpertCaption);
end;

function TSANRepExpertVisValueReplacer.GetGlyphResName: string;
begin
  Result := ICON_RES_NAME;
end;

function TSANRepExpertVisValueReplacer.DLGFormClass: TFormClass;
begin
  Result := TfrmVisValueReplacerExpert;
end;

procedure TSANRepExpertVisValueReplacer.DoExecute(const ADLGForm: TForm;
  const AReplacer: TSANReplacer);
var
  vForm : TfrmVisValueReplacerExpert;
  vModule : IOTAModule;
begin
  vForm := TfrmVisValueReplacerExpert(ADLGForm);

  vModule := TSANStaticModuleCreator.CreateModule
  (
    ctUnit,
    vForm.frameNames.FullName,
    '',
    '',
    vForm.Props.UnitNameFmt,
    '',
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_VISVALUEREPLACER_PAS'),
    AReplacer
  );

  (BorlandIDEServices as IOTAModuleServices).GetActiveProject.AddFile(vModule.FileName, True);

  vModule.Show;
end;
//==============================================================================



END.
