object frmSREDialog: TfrmSREDialog
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'frmSREDialog'
  ClientHeight = 485
  ClientWidth = 609
  Color = clBtnFace
  Constraints.MinWidth = 615
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtons: TPanel
    Left = 0
    Top = 417
    Width = 609
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'pnlButtons'
    Padding.Left = 8
    Padding.Top = 8
    Padding.Right = 8
    Padding.Bottom = 8
    ShowCaption = False
    TabOrder = 0
    object btnOK: TBitBtn
      AlignWithMargins = True
      Left = 443
      Top = 8
      Width = 75
      Height = 25
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Action = actOK
      Align = alRight
      Caption = #1054#1050
      DoubleBuffered = True
      ModalResult = 1
      ParentDoubleBuffered = False
      TabOrder = 0
    end
    object btnCancel: TBitBtn
      Left = 526
      Top = 8
      Width = 75
      Height = 25
      Align = alRight
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      DoubleBuffered = True
      ModalResult = 2
      ParentDoubleBuffered = False
      TabOrder = 1
    end
    object btnDefaultValues: TBitBtn
      AlignWithMargins = True
      Left = 8
      Top = 8
      Width = 132
      Height = 25
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Action = actDefaultValues
      Align = alLeft
      Caption = #1047#1072#1087#1086#1084#1085#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 2
    end
    object btnLoadValues: TBitBtn
      AlignWithMargins = True
      Left = 156
      Top = 8
      Width = 132
      Height = 25
      Margins.Left = 8
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Action = actLoadValues
      Align = alLeft
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 3
    end
    object btnSaveValues: TBitBtn
      AlignWithMargins = True
      Left = 296
      Top = 8
      Width = 132
      Height = 25
      Margins.Left = 8
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Action = actSaveValues
      Align = alLeft
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 4
    end
  end
  object pnlClient: TPanel
    Left = 0
    Top = 0
    Width = 609
    Height = 417
    Align = alTop
    BevelOuter = bvNone
    Caption = 'pnlClient'
    Padding.Left = 8
    Padding.Top = 8
    Padding.Right = 8
    ShowCaption = False
    TabOrder = 1
  end
  object actlst: TActionList
    Left = 16
    Top = 8
    object actOK: TAction
      Caption = #1054#1050
      OnUpdate = actOKUpdate
    end
    object actDefaultValues: TAction
      Caption = #1047#1072#1087#1086#1084#1085#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
      OnExecute = actDefaultValuesExecute
      OnUpdate = actDefaultValuesUpdate
    end
    object actLoadValues: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
      OnExecute = actLoadValuesExecute
    end
    object actSaveValues: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
      OnExecute = actSaveValuesExecute
      OnUpdate = actSaveValuesUpdate
    end
  end
  object pmLoadValues: TPopupMenu
    Left = 80
    Top = 8
  end
end
