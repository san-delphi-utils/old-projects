{*******************************************************}
{                                                       }
{       SANExpertsRepGUIDShapedFrame                    }
{       Фрейм ввода GUID.                               }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepGUIDShapedFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, StdCtrls, Mask, ActiveX, ComObj,

  SANExpertsRepCustomShapedFrame;

TYPE
  /// <summary>
  /// Фрейм ввода GUID.
  /// </summary>
  TframeGUIDShaped = class(TframeShaped)
    medtGUID: TMaskEdit;
    sbtnGUID: TSpeedButton;
    procedure sbtnGUIDClick(Sender: TObject);
    procedure medtGUIDChange(Sender: TObject);
    procedure medtGUIDKeyPress(Sender: TObject; var Key: Char);

  private
    function GetGUID: string;
    procedure SetGUID(const Value: string);

  protected
    function DoGetValidValues : boolean; override;

  public
    constructor Create(AOwner : TComponent); override;

    property GUID : string read GetGUID  write SetGUID;
  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeGUIDShaped
// private
function TframeGUIDShaped.GetGUID: string;
begin
  Result := medtGUID.Text;
end;

procedure TframeGUIDShaped.SetGUID(const Value: string);
begin
  medtGUID.Text := Value;
end;

// protected
function TframeGUIDShaped.DoGetValidValues: boolean;
var
  vGUID : TGUID;
begin
  Result := Succeeded
  (
    CLSIDFromString
    (
      PWideChar
      (
        WideString
        (
          Trim
          (
            medtGUID.Text
          )
        )
      ),
      vGUID
    )
  );
end;

// public
constructor TframeGUIDShaped.Create(AOwner: TComponent);
begin
  inherited;

  sbtnGUID.Click;
end;

// published
procedure TframeGUIDShaped.sbtnGUIDClick(Sender: TObject);
var
  vGUID : TGUID;
begin
  OleCheck(CoCreateGuid(vGUID));

  medtGUID.Text := GUIDToString(vGUID);
end;

procedure TframeGUIDShaped.medtGUIDChange(Sender: TObject);
var
  vWarnHint : string;
begin
  UpdateValue(Sender as TControl, (Sender as TMaskEdit).Text, vWarnHint);

  UpdateWarnShape(DoGetValidValues, vWarnHint);
end;

procedure TframeGUIDShaped.medtGUIDKeyPress(Sender: TObject; var Key: Char);
begin
  if not CharInSet(LowerCase(Key)[1], ['0'..'9', 'a'..'f', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)]) then
  begin
    Key := #0;
    MessageBeep(MB_OK);
  end;
end;
//==============================================================================


END.
