{*******************************************************}
{                                                       }
{       SANExpertsRepCustomExpertDialogForm             }
{       Базовая форма для всех диалогов                 }
{       создания экспертов.                             }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepCustomExpertDialogForm;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,

  SANMisc, SANINIProps,

  SANDesignReplacer,

  SANExpertsRepCommon;

TYPE
  /// <summary>
  /// Объект-хранилище свойств, которые можно запомнить.
  /// </summary>
  TCustomExpertProps = class(TSANCustomINIProps)
  private
    FForm : TForm;

  protected
    property Form : TForm  read FForm;

  end;
  TCustomExpertPropsClass = class of TCustomExpertProps;

  /// <summary>
  /// Базовая форма для всех диалогов создания экспертов.
  /// </summary>
  TfrmCustomExpertDialog = class(TForm, IWarnShapeValidatorHost)
    pnlButtons: TPanel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    btnSaveProps: TBitBtn;
    pnlClient: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure btnSavePropsClick(Sender: TObject);

  private
    FProps : TCustomExpertProps;

  protected
    // IWarnShapeValidatorHost
    procedure WarnShapeValidated;
    procedure ValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string);

    function GetINIFileName: TFileName; virtual; abstract;
    function ExpertPropsClass : TCustomExpertPropsClass; virtual;
    procedure DoWarnShapeValidated(const ABtnOKEnabled : boolean); virtual;
    procedure DoWarnShapeValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string); virtual;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    procedure FillReplacer(const AReplacer : TSANReplacer); virtual;

    property INIFileName : TFileName  read GetINIFileName;
    property Props : TCustomExpertProps  read FProps;

  end;
  TfrmCustomExpertDialogClass = class of TfrmCustomExpertDialog;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TfrmCustomExpertDialog
// protected
procedure TfrmCustomExpertDialog.WarnShapeValidated;

  function CheckValidators : boolean;
  var
    i : integer;
    vValidator : IWarnShapeValidator;
  begin
    Result := False;

    for i := 0 to ComponentCount - 1 do
      if  Supports(Components[i], IWarnShapeValidator, vValidator)
      and not vValidator.ValidValues
      then
        Exit;

    Result := True;
  end;

var
  vEnabled : boolean;
begin

  if not CheckValidators then
    btnOK.Enabled := False

  else
    begin
      vEnabled := True;

      DoWarnShapeValidated(vEnabled);

      btnOK.Enabled := vEnabled;
    end
end;

procedure TfrmCustomExpertDialog.ValueUpdated(const AFrame: TFrame;
  const AControl: TControl; const AValue: Variant; var AWarnHint: string);
begin
  DoWarnShapeValueUpdated(AFrame, AControl, AValue, AWarnHint);
end;

function TfrmCustomExpertDialog.ExpertPropsClass: TCustomExpertPropsClass;
begin
  Result := TCustomExpertProps;
end;

procedure TfrmCustomExpertDialog.DoWarnShapeValidated(const ABtnOKEnabled : boolean);
begin
end;

procedure TfrmCustomExpertDialog.DoWarnShapeValueUpdated(const AFrame: TFrame;
  const AControl: TControl; const AValue: Variant; var AWarnHint: string);
begin
end;

procedure TfrmCustomExpertDialog.FillReplacer(const AReplacer: TSANReplacer);
begin
end;

// public
constructor TfrmCustomExpertDialog.Create(AOwner: TComponent);
begin
  FProps := ExpertPropsClass.Create;
  FProps.FForm := Self;

  inherited;
end;

destructor TfrmCustomExpertDialog.Destroy;
begin
  inherited;

  FreeAndNil(FProps);
end;

// published
procedure TfrmCustomExpertDialog.FormCreate(Sender: TObject);
begin
  FProps.LoadFromINI( AppDataRoot + INIFileName);

  WarnShapeValidated;
end;

procedure TfrmCustomExpertDialog.btnSavePropsClick(Sender: TObject);
begin
  FProps.SaveToINI( AppDataRoot + INIFileName);
end;
//==============================================================================

END.
