{*******************************************************}
{                                                       }
{       SANExpertsRepCustomEditorFrame                  }
{       Базовый фрейм редактора.                        }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepCustomEditorFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, WideStrings, TypInfo,

  SANExpertsRepIntfs;

TYPE
  EEREditorError = class(Exception);

  TframeCustomEditor = class;
  TframeCustomEditorClass = class of TframeCustomEditor;

  /// <summary>
  /// Параметр контрола редактора.
  /// </summary>
  TSANRepExpParam = class(TInterfacedObject, ISANRepExpParam)
  private
    FOriginalName : WideString;
    FFrame : TframeCustomEditor;

  protected
    // ISANRepExpParam
    function GetName : WideString; safecall;
    function GetValue : WideString; safecall;
    procedure SetValue(const AValue : WideString); safecall;

  public
    constructor Create(const AOriginalName : WideString;
      const AFrame : TframeCustomEditor);

    /// <summary>
    /// Уникальное имя параметра в рамках редактора.
    /// </summary>
    property Name : WideString  read GetName;

    /// <summary>
    /// Значение параметра.
    /// </summary>
    property Value : WideString  read GetValue  write SetValue;

  end;

  /// <summary>
  /// Базовый редактор.
  /// </summary>
  TSANRepExpEditor = class(TInterfacedObject, ISANRepExpEditor)
  private
    FParams : TWideStrings;
    FFrameClass : TframeCustomEditorClass;

  protected
    // ISANRepExpEditor
    function GetName : WideString; safecall;
    function GetParamsCount : integer; safecall;
    function GetParamsNames(const AIndex : integer) : WideString; safecall;
    function ControlCreate(const AParentHandle : THandle;
      const AParamBaseName : WideString) : ISANRepExpEditorControl; safecall;

  public
    constructor Create(const AFrameClass : TframeCustomEditorClass;
      const AParamsNames : array of WideString);
    destructor Destroy; override;

  end;

  /// <summary>
  /// Базовый фрейм редактора.
  /// </summary>
  TframeCustomEditor = class(TFrame, ISANRepExpEditorControl)
    pnlClient: TPanel;
    shpWarning: TShape;

  private
    FEditor : ISANRepExpEditor;
    FParamBaseName : WideString;
    FParams : TInterfaceList;
    FIsStored : WordBool;

    procedure CMRelease(var Message: TMessage); message CM_RELEASE;

  protected
    // ISANRepExpEditorControl
    function GetEditor : ISANRepExpEditor; safecall;
    function GetEditorHandle : THandle; safecall;
    function GetParamBaseName : WideString; safecall;
    function GetWarning : WideString; safecall;
    procedure SetWarning(const AWarning : WideString); safecall;
    function GetIsCorrect : WordBool; safecall;
    function GetIsStored : WordBool; safecall;
    procedure SetIsStored(const AIsStored : WordBool); safecall;
    function GetParamsCount : integer; safecall;
    function GetParams(const AIndex : integer) : ISANRepExpParam; safecall;
    function ParamsFind(out AParam : ISANRepExpParam; const AName : WideString) : WordBool; safecall;
    procedure FreeControl; safecall;

    procedure InitParams;
    procedure DoIsCorrect(var AIsCorrect : WordBool); virtual;
    procedure UpdateWarnShape;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    procedure Release;
    class function EditorName : WideString; virtual;
    class function CreateEditor : ISANRepExpEditor; virtual;
    class function RegEditor(const AEditorsManager : ISANRepExpEditorsManager) : boolean;
    class function UnRegEditor(const AEditorsManager : ISANRepExpEditorsManager) : boolean;

    /// <summary>
    /// Редактор к которому принадлежит контрол.
    /// </summary>
    property Editor : ISANRepExpEditor  read GetEditor;
    /// <summary>
    /// Базовое имя параметра.
    /// </summary>
    property ParamBaseName : WideString  read GetParamBaseName;
    /// <summary>
    /// Сообщение, которое показывается при некорректном вводе.
    /// </summary>
    property Warning : WideString  read GetWarning  write SetWarning;
    /// <summary>
    /// Параметры корректны?
    /// </summary>
    property IsCorrect : WordBool  read GetIsCorrect;
    /// <summary>
    /// Сохраняются ли параметры редактора.
    /// </summary>
    property IsStored : WordBool  read GetIsStored  write SetIsStored;
    /// <summary>
    /// Количество параметров.
    /// </summary>
    property ParamsCount : integer  read GetParamsCount;
    /// <summary>
    /// Параметр по индексу.
    /// </summary>
    property Params[const AIndex : integer] : ISANRepExpParam  read GetParams;
  end;

IMPLEMENTATION

{$R *.dfm}

resourcestring
  sNeedOverrideMethodErrorMessage   = 'Метод %s не замещен';
  sPropNotFaundInFrameErrorMessage  = 'Свойство %s не найдено в фрейме редактора %s';

//==============================================================================
// TSANRepExpParam
// protected
function TSANRepExpParam.GetName: WideString;
begin
  Result := FFrame.FParamBaseName + FOriginalName;
end;

function TSANRepExpParam.GetValue: WideString;
var
  vPropInfo : PPropInfo;
begin
  vPropInfo := GetPropInfo(FFrame, FOriginalName);

  if not Assigned(vPropInfo) then
    raise EEREditorError.CreateResFmt(@sPropNotFaundInFrameErrorMessage, [FOriginalName, FFrame.EditorName]);

  Result := GetStrProp(FFrame, vPropInfo)
end;

procedure TSANRepExpParam.SetValue(const AValue: WideString);
var
  vPropInfo : PPropInfo;
begin
  vPropInfo := GetPropInfo(FFrame, FOriginalName);

  if not Assigned(vPropInfo) then
    raise EEREditorError.CreateResFmt(@sPropNotFaundInFrameErrorMessage, [FOriginalName, FFrame.EditorName]);

  if Assigned(vPropInfo^.SetProc) then
    SetStrProp(FFrame, vPropInfo, AValue);
end;

// public
constructor TSANRepExpParam.Create(const AOriginalName : WideString;
  const AFrame : TframeCustomEditor);
begin
  inherited Create;

  FOriginalName  := AOriginalName;
  FFrame         := AFrame;
end;
//==============================================================================

//==============================================================================
// TSANRepExpEditor
function TSANRepExpEditor.GetName: WideString;
begin
  Result := FFrameClass.EditorName;
end;

function TSANRepExpEditor.GetParamsCount: integer;
begin
  Result := FParams.Count;
end;

function TSANRepExpEditor.GetParamsNames(const AIndex: integer): WideString;
begin
  Result := FParams[AIndex];
end;

function TSANRepExpEditor.ControlCreate(const AParentHandle: THandle;
  const AParamBaseName: WideString): ISANRepExpEditorControl;
var
  vFrame : TframeCustomEditor;
begin
  vFrame := FFrameClass.CreateParentedControl(AParentHandle) as TframeCustomEditor;

  try

    vFrame.FEditor := Self;
    vFrame.FParamBaseName := AParamBaseName;
    vFrame.InitParams;
    vFrame.Align := alTop;
    vFrame.BringToFront;
    vFrame.Show;

//    ShowMessageFmt('%s frame pos: %d, %d. Visible: %s', [vFrame.ClassName, vFrame.Left, vFrame.Top, BoolToStr(vFrame.Visible, True)]);

  except
    FreeAndNil(vFrame);
    raise;
  end;

  Result := vFrame;
end;

// public
constructor TSANRepExpEditor.Create(const AFrameClass: TframeCustomEditorClass;
  const AParamsNames: array of WideString);
var
  i : integer;
begin
  inherited Create;

  FParams := TWideStringList.Create;

  FFrameClass := AFrameClass;

  for i := Low(AParamsNames) to High(AParamsNames) do
    FParams.Add(AParamsNames[i]);
end;

destructor TSANRepExpEditor.Destroy;
begin
  FreeAndNil(FParams);
  inherited;
end;
//==============================================================================

//==============================================================================
// TframeCustomEditor
// private
procedure TframeCustomEditor.CMRelease(var Message: TMessage);
begin
  Free;
end;

// protected
function TframeCustomEditor.GetEditor: ISANRepExpEditor;
begin
  Result := FEditor;
end;

function TframeCustomEditor.GetEditorHandle: THandle;
begin
  Result := Handle;
end;

function TframeCustomEditor.GetParamBaseName: WideString;
begin
  Result := FParamBaseName;
end;

function TframeCustomEditor.GetWarning: WideString;
begin
  Result := WideString(shpWarning.Hint);
end;

procedure TframeCustomEditor.SetWarning(const AWarning: WideString);
begin
  shpWarning.Hint := AWarning;
end;

function TframeCustomEditor.GetIsCorrect: WordBool;
begin
  DoIsCorrect(Result);
end;

function TframeCustomEditor.GetIsStored: WordBool;
begin
  Result := FIsStored;
end;

procedure TframeCustomEditor.SetIsStored(const AIsStored: WordBool);
begin
  FIsStored := AIsStored;
end;

function TframeCustomEditor.GetParamsCount: integer;
begin
  Result := FParams.Count;
end;

function TframeCustomEditor.GetParams(const AIndex: integer): ISANRepExpParam;
begin
  Result := FParams[AIndex] as ISANRepExpParam;
end;

function TframeCustomEditor.ParamsFind(out AParam: ISANRepExpParam;
  const AName: WideString): WordBool;
var
  i : integer;
  vParam : ISANRepExpParam;
begin

  for i := 0 to FParams.Count - 1 do
  begin
    vParam := FParams[i] as ISANRepExpParam;

    if not CompareText(vParam.Name, AName) = 0 then
      Continue;

    AParam := vParam;
    Exit(True);
  end;

  Result := False;
end;

procedure TframeCustomEditor.FreeControl;
begin
  Release;
end;

procedure TframeCustomEditor.InitParams;
var
  i : integer;
begin
  for i := 0 to FEditor.ParamsCount - 1 do
    FParams.Add( TSANRepExpParam.Create(FEditor.ParamsNames[i], Self) );
end;

procedure TframeCustomEditor.DoIsCorrect(var AIsCorrect: WordBool);
begin
  AIsCorrect := True;
end;

procedure TframeCustomEditor.UpdateWarnShape;
begin
  with shpWarning do
    if IsCorrect then
      begin
        Brush.Color := clLime;
        ShowHint := False;
      end
    else
      begin
        Brush.Color := clRed;
        ShowHint := True;
      end;
end;

// public
constructor TframeCustomEditor.Create(AOwner: TComponent);
begin
  inherited;

  FParams := TInterfaceList.Create;

//  ShowMessageFmt('%s frame created!', [ClassName]);
end;

destructor TframeCustomEditor.Destroy;
begin
  FreeAndNil(FParams);

  inherited;
end;

procedure TframeCustomEditor.Release;
begin
  PostMessage(Handle, CM_RELEASE, 0, 0);
end;

class function TframeCustomEditor.EditorName: WideString;
var
  S : string;
  L : integer;
begin
  S := ClassName;

  L := Length('Tframe');

  if L >= Length(S) then
    raise EEREditorError.CreateResFmt(@sNeedOverrideMethodErrorMessage, ['EditorName']);

  Delete(S, 1, L);

  Result := S;
end;

class function TframeCustomEditor.CreateEditor: ISANRepExpEditor;
begin
  raise EEREditorError.CreateResFmt(@sNeedOverrideMethodErrorMessage, ['CreateEditor']);

  // Template:
  // Result := TSANRepExpEditor.Create(Self, ['p1', 'p2', 'p3']);
end;

class function TframeCustomEditor.RegEditor(
  const AEditorsManager: ISANRepExpEditorsManager): boolean;
begin
  Result := AEditorsManager.RegEditor(CreateEditor);
end;

class function TframeCustomEditor.UnRegEditor(
  const AEditorsManager: ISANRepExpEditorsManager): boolean;
begin
  Result := AEditorsManager.UnRegEditor(EditorName);
end;
//==============================================================================




END.
