{*******************************************************}
{                                                       }
{       SANExpertsRepCustomVersionDataModuleForm        }
{       Абстрактная форма создания модуля данных        }
{       с информацией о версии.                         }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepCustomVersionDataModuleForm;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Spin,

  SANMisc, SANINIProps,

  SANDesignUtils, SANDesignReplacer,

  SANExpertsRepCommon,
  SANExpertsRepCustomExpertDialogForm,
  SANExpertsRepCustomShapedFrame,
  SANExpertsRepGUIDShapedFrame,
  SANExpertsRepFormatNameShapedFrame,
  SANExpertsRepNamesPrefPostShapedFrame, SANExpertsRepEntryPointNameShapedFrame;

TYPE
  TfrmCustomVersionDataModule = class;

  TCustomVersionDataModuleProps = class(TCustomExpertProps)
  private
    FUnitNameFmt, FNamePostfix, FFormNameFmt, FNamePrefix: string;

    function GetForm: TfrmCustomVersionDataModule;

    procedure SetNamePrefix(const Value: string);
    procedure SetNamePostfix(const Value: string);
    procedure SetFormNameFmt(const Value: string);
    procedure SetUnitNameFmt(const Value: string);

  public
    property Form : TfrmCustomVersionDataModule  read GetForm;

  published
    property NamePrefix   : string  read FNamePrefix   write SetNamePrefix;
    property NamePostfix  : string  read FNamePostfix  write SetNamePostfix;
    property FormNameFmt  : string  read FFormNameFmt  write SetFormNameFmt;
    property UnitNameFmt  : string  read FUnitNameFmt  write SetUnitNameFmt;

  end;

  TfrmCustomVersionDataModule = class(TfrmCustomExpertDialog)
    gbNames: TGroupBox;
    lblDMName: TLabel;
    lblFormNameFmt: TLabel;
    lblUnitNameFmt: TLabel;
    frameNames: TframeNamesPrefPostShaped;
    frameFmtFormName: TframeFormatNameShaped;
    frameFmtUnitName: TframeFormatNameShaped;
    gbParams: TGroupBox;
    lblGUID: TLabel;
    lblDescription: TLabel;
    lblCaption: TLabel;
    frameGUID: TframeGUIDShaped;
    mmDescription: TMemo;
    edtCaption: TEdit;
    lblAuthor: TLabel;
    edtAuthor: TEdit;
    lblURL: TLabel;
    edtURL: TEdit;
    lblVersion: TLabel;
    pnlVersion: TPanel;
    lblMajor: TLabel;
    seMajor: TSpinEdit;
    lblMinor: TLabel;
    seMinor: TSpinEdit;
    lblRelease: TLabel;
    seRelease: TSpinEdit;
    lblBuild: TLabel;
    seBuild: TSpinEdit;
    procedure FormCreate(Sender: TObject);

  private
    function GetProps: TCustomVersionDataModuleProps;

  protected
    procedure DoWarnShapeValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string); override;
    procedure UpdateFmtNames;

  public
    procedure FillReplacer(const AReplacer : TSANReplacer); override;

    property Props : TCustomVersionDataModuleProps  read GetProps;

  end;
  
IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TCustomVersionDataModuleProps
// private
function TCustomVersionDataModuleProps.GetForm: TfrmCustomVersionDataModule;
begin
  Result := (inherited Form) as TfrmCustomVersionDataModule;
end;

procedure TCustomVersionDataModuleProps.SetNamePrefix(const Value: string);
begin
  if FNamePrefix = Value then
    Exit;

  FNamePrefix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.Prefix := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TCustomVersionDataModuleProps.SetNamePostfix(const Value: string);
begin
  if FNamePostfix = Value then
    Exit;

  FNamePostfix := Value;

  if Assigned(Form) then
  begin
    Form.frameNames.Postfix := Value;
    Form.UpdateFmtNames;
  end;
end;

procedure TCustomVersionDataModuleProps.SetFormNameFmt(const Value: string);
begin
  if FFormNameFmt = Value then
    Exit;

  FFormNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtFormName.FormatName := Value;
end;

procedure TCustomVersionDataModuleProps.SetUnitNameFmt(const Value: string);
begin
  if FUnitNameFmt = Value then
    Exit;

  FUnitNameFmt := Value;

  if Assigned(Form) then
    Form.frameFmtUnitName.FormatName := Value;
end;
//==============================================================================

//==============================================================================
// TfrmCustomVersionDataModule
// private
function TfrmCustomVersionDataModule.GetProps: TCustomVersionDataModuleProps;
begin
  Result := (inherited Props) as TCustomVersionDataModuleProps;
end;

// protected
procedure TfrmCustomVersionDataModule.DoWarnShapeValueUpdated(const AFrame: TFrame;
  const AControl: TControl; const AValue: Variant; var AWarnHint: string);
begin

  if AFrame = frameNames then
    begin
      AWarnHint := 'Некорректное имя';

      if AControl = frameNames.edtNamePrefix  then
        Props.NamePrefix := AValue

      else
      if AControl = frameNames.edtNamePostfix then
        Props.NamePostfix := AValue

      else
      if AControl = frameNames.edtName then
        UpdateFmtNames;

    end

  else
  if AFrame = frameFmtFormName then
    begin
      AWarnHint := 'Некорректный формат имени модуля данных';
      Props.FormNameFmt := AValue;
    end

  else
  if AFrame = frameFmtUnitName then
    begin
      AWarnHint := 'Некорректный формат имени модуля';
      Props.UnitNameFmt := AValue;
    end

  else
  if AFrame = frameGUID then
    AWarnHint := 'Некорректный GUID'

end;

procedure TfrmCustomVersionDataModule.UpdateFmtNames;
begin
  frameFmtFormName.NameForFmt := frameNames.FullName;
  frameFmtUnitName.NameForFmt := frameNames.FullName;
end;

// published
procedure TfrmCustomVersionDataModule.FillReplacer(const AReplacer: TSANReplacer);
begin
  AReplacer.AddPair('GUID',            frameGUID.GUID);
  AReplacer.AddPair('Caption',         StringToDFMString(Trim(edtCaption.Text)));
  AReplacer.AddPair('Description',     StringToDFMString(Trim(mmDescription.Text)));
  AReplacer.AddPair('URL',             StringToDFMString(Trim(edtURL.Text)));
  AReplacer.AddPair('Author',          StringToDFMString(Trim(edtAuthor.Text)));
  AReplacer.AddPair('V1Major',         IntToStr(seMajor.Value));
  AReplacer.AddPair('V2Minor',         IntToStr(seMinor.Value));
  AReplacer.AddPair('V3Release',       IntToStr(seRelease.Value));
  AReplacer.AddPair('V4Build',         IntToStr(seBuild.Value));
end;

procedure TfrmCustomVersionDataModule.FormCreate(Sender: TObject);
begin
  edtCaption.Text     := '';
  edtAuthor.Text      := '';
  edtURL.Text         := '';
  mmDescription.Text  := '';

  seMajor.Value := 1;

  inherited;
end;
//==============================================================================


END.
