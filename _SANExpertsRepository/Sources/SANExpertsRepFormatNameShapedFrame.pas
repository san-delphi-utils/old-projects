{*******************************************************}
{                                                       }
{       SANExpertsRepFormatNameShapedFrame              }
{       Фрейм ввода форматированного имени.             }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepFormatNameShapedFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,

  SANMisc,

  SANExpertsRepCustomShapedFrame;

type
  /// <summary>
  /// Фрейм ввода форматированного имени.
  /// </summary>
  TframeFormatNameShaped = class(TframeShaped)
    edtNameFmt: TEdit;
    bvlResults: TBevel;
    lblNameResult: TLabel;
    procedure edtNameFmtChange(Sender: TObject);
    procedure edtNameFmtKeyPress(Sender: TObject; var Key: Char);

  private
    FNameForFmt: string;

    procedure SetNameForFmt(const Value: string);
    function GetFormatName: string;
    procedure SetFormatName(const Value: string);

  protected
    function DoGetValidValues : boolean; override;

    procedure UpdateNameFmt;

  public
    constructor Create(AOwner : TComponent); override;

    property NameForFmt : string  read FNameForFmt    write SetNameForFmt;
    property FormatName : string  read GetFormatName  write SetFormatName;
  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeFormatNameShaped
// private
procedure TframeFormatNameShaped.SetNameForFmt(const Value: string);
begin
  if FNameForFmt = Value then
    Exit;

  FNameForFmt := Value;

  UpdateNameFmt;
end;

function TframeFormatNameShaped.GetFormatName: string;
begin
  Result := edtNameFmt.Text;
end;

procedure TframeFormatNameShaped.SetFormatName(const Value: string);
begin
  edtNameFmt.Text := Value;
end;

// protected
function TframeFormatNameShaped.DoGetValidValues: boolean;
begin
  Result := CheckPasIdentifierName(lblNameResult.Caption);
end;

procedure TframeFormatNameShaped.UpdateNameFmt;
var
  vWarnHint : string;
begin
  lblNameResult.Caption := Format(edtNameFmt.Text, [NameForFmt]);
  lblNameResult.Hint := lblNameResult.Caption;

  UpdateValue(edtNameFmt, edtNameFmt.Text, vWarnHint);

  UpdateWarnShape(DoGetValidValues, vWarnHint);
end;

// public
constructor TframeFormatNameShaped.Create(AOwner: TComponent);
begin
  inherited;

  ClearEditsText;
end;

// published
procedure TframeFormatNameShaped.edtNameFmtChange(Sender: TObject);
begin
  UpdateNameFmt;
end;

procedure TframeFormatNameShaped.edtNameFmtKeyPress(Sender: TObject;
  var Key: Char);
var
  vSet : TSysCharSet;
begin

  vSet := ['a'..'z', '_', '%', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)];
  if (Sender as TEdit).Text <> '' then
    vSet := vSet + ['0'..'9'];

  if not CharInSet(LowerCase(Key)[1], vSet) then
  begin
    Key := #0;
    MessageBeep(MB_OK);
  end;

end;
//==============================================================================

END.
