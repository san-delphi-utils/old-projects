inherited frmExpertExpert: TfrmExpertExpert
  Caption = 'frmExpertExpert'
  ClientHeight = 204
  ExplicitWidth = 543
  ExplicitHeight = 232
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlButtons: TPanel
    Top = 163
    ExplicitTop = 163
  end
  inherited pnlClient: TPanel
    Top = 164
    Height = 0
    ExplicitTop = 164
    ExplicitHeight = 0
  end
  object gbNames: TGroupBox
    Left = 0
    Top = 0
    Width = 537
    Height = 164
    Align = alTop
    Caption = #1048#1084#1077#1085#1072
    Padding.Left = 6
    Padding.Right = 6
    Padding.Bottom = 6
    TabOrder = 2
    object lblDLGName: TLabel
      AlignWithMargins = True
      Left = 8
      Top = 15
      Width = 72
      Height = 13
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Align = alTop
      Caption = #1048#1084#1103' '#1101#1082#1089#1087#1077#1088#1090#1072':'
    end
    object lblFormNameFmt: TLabel
      AlignWithMargins = True
      Left = 8
      Top = 71
      Width = 216
      Height = 13
      Margins.Left = 0
      Margins.Top = 8
      Margins.Right = 0
      Align = alTop
      Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1092#1086#1088#1084#1099' (%s - '#1080#1084#1103' '#1101#1082#1089#1087#1077#1088#1090#1072'):'
    end
    object lblUnitNameFmt: TLabel
      AlignWithMargins = True
      Left = 8
      Top = 117
      Width = 219
      Height = 13
      Margins.Left = 0
      Margins.Top = 8
      Margins.Right = 0
      Align = alTop
      Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1084#1086#1076#1091#1083#1103' (%s - '#1080#1084#1103' '#1101#1082#1089#1087#1077#1088#1090#1072'):'
    end
    inline frameNames: TframeNamesPrefPostShaped
      Left = 8
      Top = 31
      Width = 521
      Height = 32
      Align = alTop
      TabOrder = 0
      ExplicitLeft = 8
      ExplicitTop = 31
      ExplicitWidth = 521
      inherited shpWarning: TShape
        Left = 506
        ExplicitLeft = 576
      end
      inherited pnlClient: TPanel
        Width = 498
        ExplicitWidth = 498
      end
    end
    inline frameFmtFormName: TframeFormatNameShaped
      Left = 8
      Top = 87
      Width = 521
      Height = 22
      Align = alTop
      TabOrder = 1
      ExplicitLeft = 8
      ExplicitTop = 87
      ExplicitWidth = 521
      inherited shpWarning: TShape
        Left = 506
        ExplicitLeft = 576
      end
      inherited pnlClient: TPanel
        Width = 498
        ExplicitWidth = 498
        inherited bvlResults: TBevel
          Left = 390
          ExplicitLeft = 390
        end
        inherited lblNameResult: TLabel
          Left = 399
          ExplicitLeft = 399
        end
        inherited edtNameFmt: TEdit
          Width = 382
          ExplicitWidth = 382
        end
      end
    end
    inline frameFmtUnitName: TframeFormatNameShaped
      Left = 8
      Top = 133
      Width = 521
      Height = 22
      Align = alTop
      TabOrder = 2
      ExplicitLeft = 8
      ExplicitTop = 133
      ExplicitWidth = 521
      inherited shpWarning: TShape
        Left = 506
        ExplicitLeft = 548
      end
      inherited pnlClient: TPanel
        Width = 498
        ExplicitWidth = 498
        inherited bvlResults: TBevel
          Left = 390
          ExplicitLeft = 390
        end
        inherited lblNameResult: TLabel
          Left = 399
          ExplicitLeft = 399
        end
        inherited edtNameFmt: TEdit
          Width = 382
          ExplicitWidth = 382
        end
      end
    end
  end
end
