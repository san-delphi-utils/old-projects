{*******************************************************}
{                                                       }
{       SANExpertsRepPkgAsDllAndPluginsReg              }
{       Регистрация модулей данных и редакторов свойств }
{       для "Пакетов-как-dll" и плагинов на их основе.  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepPkgAsDllAndPluginsReg;
{$I NX.INC}
INTERFACE

USES
  ToolsAPI, DesignIntf, DesignEditors,

  SysUtils, TypInfo,

  SANPkgAsDllCustomDM, SANPluginsServerDM, SANPkgAsDllClient, SANPkgAsDllClientDM, SANPluginsClientDM,

  SANDesignPropertyEditers;

TYPE
  TSANVersionPropertyEditor = class(TClassProperty)
  public
    function GetValue: string; override;
  end;

procedure Register;

IMPLEMENTATION

procedure Register;
begin
  RegisterCustomModule(TSANPluginsServerDataModule, TCustomModule);
  RegisterCustomModule(TSANPkgAsDllEntryDataModule, TCustomModule);
  RegisterCustomModule(TSANPluginDataModule, TCustomModule);

  RegisterPropertyEditor(TypeInfo(TSANVersionPropObj),  TSANVersionInitDoneDataModule,  'Version',  TSANVersionPropertyEditor);
  RegisterPropertyEditor(TypeInfo(string),              TSANVersionInitDoneDataModule,  'GUID',     TSANGUIDPropertyEditor);
end;

//==============================================================================
// TSANVersionPropertyEditor
// public
function TSANVersionPropertyEditor.GetValue: string;
var
  vVPObj : TSANVersionPropObj;
begin
  vVPObj := (GetObjectProp(GetComponent(0), GetPropInfo) as TSANVersionPropObj);

  Result := Format('%d.%d.%d.%d', [vVPObj.V1Major, vVPObj.V2Minor, vVPObj.V3Release, vVPObj.V4Build]);
end;
//==============================================================================

END.
