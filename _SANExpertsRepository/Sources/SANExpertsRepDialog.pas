{*******************************************************}
{                                                       }
{       SANExpertsRepDialog                             }
{       Форма диалога всех экспертов репозитория.       }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepDialog;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ActnList, Menus, INIFiles, Types,

  SANExpertsRepIntfs, SANExpertsRepWizards;

CONST
  DEFAULT_SAVE = '_default';

TYPE
  /// <summary>
  /// Метод обратного вызова перечисления параметров редакторов.
  /// </summary>
  TEnumEditorsParamsObjProc = procedure(const AEditorControl : ISANRepExpEditorControl;
    const AEditorParam : ISANRepExpParam; const AEnumParam : Pointer) of object;

  /// <summary>
  /// Метод чтения или запими параметра в/из INI.
  /// </summary>
  TINIParamObjProc = procedure(const AINIFile : TIniFile; const ASection : string; const AParam : ISANRepExpParam) of object;

  /// <summary>
  /// Форма диалога всех экспертов репозитория.
  /// </summary>
  TfrmSREDialog = class(TForm)
    pnlButtons: TPanel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    btnDefaultValues: TBitBtn;
    pnlClient: TPanel;
    btnLoadValues: TBitBtn;
    btnSaveValues: TBitBtn;
    actlst: TActionList;
    actOK: TAction;
    actDefaultValues: TAction;
    actLoadValues: TAction;
    actSaveValues: TAction;
    pmLoadValues: TPopupMenu;

    procedure FormShow(Sender: TObject);

    procedure actOKUpdate(Sender: TObject);
    procedure actDefaultValuesExecute(Sender: TObject);
    procedure actDefaultValuesUpdate(Sender: TObject);
    procedure actLoadValuesExecute(Sender: TObject);
    procedure actSaveValuesExecute(Sender: TObject);
    procedure actSaveValuesUpdate(Sender: TObject);

  private
    FEditorControls : TInterfaceList;
    FWizard: TSANExpertsRepCustomWizard;

    function GetEditorControlsCount: integer;
    function GetEditorControls(const AIndex: integer): ISANRepExpEditorControl;
    procedure SetWizard(const Value: TSANExpertsRepCustomWizard);

  protected
    procedure DoAddControl(const AClass : TControlClass; const AParent : TWinControl; var AControl);

    procedure UpdateLoadValues;
    procedure INIParamsValues(const ASection : string; const AINIParamProc : TINIParamObjProc);
    procedure DoSaveValues(const AINIFile : TIniFile; const ASection : string; const AParam : ISANRepExpParam);
    procedure DoLoadValues(const AINIFile : TIniFile; const ASection : string; const AParam : ISANRepExpParam);
    procedure SaveValues(const ASection : string);
    procedure LoadValues(const ASection : string);
    procedure LoadFromMenu(Sender : TObject);

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    function AddLabel(const ACaption : string; const AParent : TWinControl) : TLabel;
    function AddSpace(const AHeight: integer; const AParent : TWinControl) : TBevel;
    function AddGroup(const ACaption : string; const AParent : TWinControl) : TGroupBox;
    function AddNotEditor(const AEditorName : string; const AParent : TWinControl) : TLabel;
    procedure AddEditorControl(const AEditorControl : ISANRepExpEditorControl);

    procedure AutoSizeWCtHeight(const AWinControl : TWinControl);


    property EditorControlsCount : integer  read GetEditorControlsCount;
    property EditorControls[const AIndex : integer] : ISANRepExpEditorControl  read GetEditorControls;

    property Wizard : TSANExpertsRepCustomWizard  read FWizard write SetWizard;
  end;

IMPLEMENTATION

uses
  SANExpertsRepManager;

{$R *.dfm}

//==============================================================================
// TfrmCustomExpertDialog
// private
function TfrmSREDialog.GetEditorControlsCount: integer;
begin
  Result := FEditorControls.Count;
end;

function TfrmSREDialog.GetEditorControls(const AIndex: integer): ISANRepExpEditorControl;
begin
  Result := FEditorControls[AIndex] as ISANRepExpEditorControl;
end;

procedure TfrmSREDialog.SetWizard(const Value: TSANExpertsRepCustomWizard);
begin
  FWizard := Value;

  Caption := FWizard.Comment;
  Icon.Assign(FWizard.Glyph);
  UpdateLoadValues;
end;

// protected
procedure TfrmSREDialog.DoAddControl(const AClass: TControlClass;
  const AParent: TWinControl; var AControl);
var
  vControl, vLastControl : TControl;
  i : integer;
begin
  vLastControl := nil;
  for i := 0 to AParent.ControlCount - 1 do
    if not Assigned(vLastControl)
    or (vLastControl.Top < AParent.Controls[i].Top)
    then
      vLastControl := AParent.Controls[i];

  vControl := AClass.Create(Self);

  if Assigned(vLastControl) then
    vControl.Top := vLastControl.Top + vLastControl.Height
  else
    vControl.Top := 0;

  vControl.Parent := AParent;
  vControl.Align := alTop;

  vControl.Show;

  TControl(AControl) := vControl;
end;

procedure TfrmSREDialog.UpdateLoadValues;

  procedure ClearMenu;
  var
    i : integer;
  begin
    for i := pmLoadValues.Items.Count - 1 downto 0 do
      pmLoadValues.Items.Delete(i);
  end;

  function AddMenuItem(const AName, ACaption : string): TMenuItem;
  begin
    Result := TMenuItem.Create(pmLoadValues);

    Result.Name     := AName;
    Result.Caption  := ACaption;
    Result.OnClick  := LoadFromMenu;

    pmLoadValues.Items.Add(Result);
  end;

  procedure AddDefaultLoad(const AStrings : TStrings);
  var
    vIndex : integer;
  begin
    vIndex := AStrings.IndexOf(DEFAULT_SAVE);

    if vIndex < 0 then
      Exit;

    AStrings.Delete(vIndex);

    AddMenuItem('miDefault', 'По умолчанию');

    if AStrings.Count > 0 then
      AddMenuItem('miDefaultSep', '-');
  end;

  procedure AddValuesForLoad(const AStrings : TStrings);
  var
    i : integer;
  begin
    for i := 0 to AStrings.Count - 1 do
      AddMenuItem( Format('miValues%d', [i]), AStrings[i]);
  end;

var
  vINI : TINIFile;
  vStrings : TStrings;
begin
  vINI := TINIFile.Create(SAN_EXPERTS_REP_FOLDER + '\' + Wizard.Name);
  try

    vStrings := TStringList.Create;
    try

      vINI.ReadSections(vStrings);

      actLoadValues.Enabled := vStrings.Count > 0;

      ClearMenu;

      if vStrings.Count > 0 then
      begin
        AddDefaultLoad(vStrings);
        AddValuesForLoad(vStrings);
      end;

    finally
      FreeAndNil(vStrings);
    end;

  finally
    FreeAndNil(vINI);
  end;
end;

procedure TfrmSREDialog.INIParamsValues(const ASection: string;
  const AINIParamProc: TINIParamObjProc);
var
  vINI : TINIFile;
  i, j : integer;
  vEdtCtrl : ISANRepExpEditorControl;
  vParam : ISANRepExpParam;
begin
  vINI := TINIFile.Create(SAN_EXPERTS_REP_FOLDER + '\' + Wizard.Name);
  try

    for i := 0 to EditorControlsCount - 1 do
    begin
      vEdtCtrl := EditorControls[i];

      if vEdtCtrl.IsStored then
        for j := 0 to vEdtCtrl.ParamsCount - 1 do
        begin
          vParam := vEdtCtrl.Params[j];

          AINIParamProc(vINI, ASection, vParam);
        end;
    end;

  finally
    FreeAndNil(vINI);
  end;
end;

procedure TfrmSREDialog.DoSaveValues(const AINIFile : TIniFile;
  const ASection : string; const AParam : ISANRepExpParam);
begin
  AINIFile.WriteString(ASection, AParam.Name, AParam.Value);
end;

procedure TfrmSREDialog.DoLoadValues(const AINIFile : TIniFile;
  const ASection : string; const AParam : ISANRepExpParam);
begin
  AParam.Value := AINIFile.ReadString(ASection, AParam.Name, AParam.Value);
end;

procedure TfrmSREDialog.SaveValues(const ASection: string);
begin
  INIParamsValues(ASection, DoSaveValues);
end;

procedure TfrmSREDialog.LoadValues(const ASection: string);
begin
  INIParamsValues(ASection, DoLoadValues);
end;

procedure TfrmSREDialog.LoadFromMenu(Sender: TObject);
begin
  LoadValues((Sender as TMenuItem).Caption);
end;

// public
constructor TfrmSREDialog.Create(AOwner: TComponent);
begin
  FEditorControls := TInterfaceList.Create;

  inherited;
end;

destructor TfrmSREDialog.Destroy;
var
  i : integer;
begin
  for i := 0 to EditorControlsCount - 1 do
    EditorControls[i].FreeControl;

  FEditorControls.Clear;

  inherited;

  FreeAndNil(FEditorControls);
end;

function TfrmSREDialog.AddLabel(const ACaption: string;
  const AParent: TWinControl): TLabel;
begin
  DoAddControl(TLabel, AParent, Result);

  Result.Caption := ACaption;
  Result.Layout := tlCenter;

  Result.Transparent  := False;
  Result.Color        := clSkyBlue;
end;

function TfrmSREDialog.AddSpace(const AHeight: integer; const AParent: TWinControl): TBevel;
begin
  DoAddControl(TBevel, AParent, Result);

  Result.Shape := bsSpacer;
  Result.Height := AHeight;
end;

function TfrmSREDialog.AddGroup(const ACaption: string;
  const AParent: TWinControl): TGroupBox;
begin
  DoAddControl(TGroupBox, AParent, Result);

  Result.Caption := ACaption;

  with Result.Padding do
  begin
    Left    := 6;
    Right   := 6;
    Bottom  := 6;
  end;

end;

function TfrmSREDialog.AddNotEditor(const AEditorName: string;
  const AParent: TWinControl): TLabel;
begin
  Result := AddLabel(AEditorName, AParent);

  Result.Alignment    := taCenter;
  Result.Transparent  := False;
  Result.Color        := clRed;
  Result.Font.Color   := clWhite;
end;

procedure TfrmSREDialog.AddEditorControl(const AEditorControl: ISANRepExpEditorControl);
begin
  FEditorControls.Add(AEditorControl);
end;

procedure TfrmSREDialog.AutoSizeWCtHeight(const AWinControl: TWinControl);
var
  vCHeight : integer;
  i : integer;
begin
  vCHeight := 0;

  for i := 0 to AWinControl.ControlCount - 1 do
    if AWinControl.Controls[i].Align in [alTop, alBottom] then
      Inc(vCHeight, AWinControl.Controls[i].Height);

  AWinControl.ClientHeight := vCHeight;
end;

// published
procedure TfrmSREDialog.FormShow(Sender: TObject);
begin
  LoadValues(DEFAULT_SAVE);
end;

procedure TfrmSREDialog.actOKUpdate(Sender: TObject);
var
  i : integer;
begin

  for i := 0 to EditorControlsCount - 1 do
    if not EditorControls[i].IsCorrect then
    begin
      (Sender as TAction).Enabled := False;
      Exit;
    end;

  (Sender as TAction).Enabled := True;
end;

procedure TfrmSREDialog.actDefaultValuesExecute(Sender: TObject);
begin
  SaveValues(DEFAULT_SAVE);
end;

procedure TfrmSREDialog.actDefaultValuesUpdate(Sender: TObject);
var
  i : integer;
begin

  for i := 0 to EditorControlsCount - 1 do
    if EditorControls[i].IsStored then
    begin
      (Sender as TAction).Enabled := True;
      Exit;
    end;

  (Sender as TAction).Enabled := False;
end;

procedure TfrmSREDialog.actLoadValuesExecute(Sender: TObject);
var
  vControl : TControl;
begin
  vControl := (Sender as TAction).ActionComponent as TControl;

  with vControl.ClientToScreen(Point(0, 0)) do
    pmLoadValues.Popup(X, Y);
end;

procedure TfrmSREDialog.actSaveValuesExecute(Sender: TObject);
var
  S : string;
begin

  if not InputQuery('Сохранение значений', 'Имя набора параметров', S) then
    Exit;

  SaveValues(S);
end;

procedure TfrmSREDialog.actSaveValuesUpdate(Sender: TObject);
var
  i : integer;
begin

  for i := 0 to EditorControlsCount - 1 do
    if EditorControls[i].IsStored then
    begin
      (Sender as TAction).Enabled := True;
      Exit;
    end;

  (Sender as TAction).Enabled := False;
end;
//==============================================================================



END.
