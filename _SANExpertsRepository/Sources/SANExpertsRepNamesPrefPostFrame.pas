{*******************************************************}
{                                                       }
{       SANExpertsRepNamesPrefPostFrame                 }
{       Фрейм ввода имени с префиксом и постфиксом.     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepNamesPrefPostFrame;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,

  SANMisc,

  SANExpertsRepIntfs, SANExpertsRepCustomEditorFrame;

TYPE
  /// <summary>
  /// Фрейм ввода имени с префиксом и постфиксом.
  /// </summary>
  TframeNamesPrefPost = class(TframeCustomEditor)
    bvlResults: TBevel;
    lblNameResult: TLabel;
    pnlName: TPanel;
    lblNameHint: TLabel;
    edtName: TEdit;
    pnlPostfix: TPanel;
    lblNamePostfixHint: TLabel;
    edtNamePostfix: TEdit;
    pnlPrefix: TPanel;
    lblNamePrefixHint: TLabel;
    edtNamePrefix: TEdit;
    procedure edtNameChange(Sender: TObject);
    procedure edtNameKeyPress(Sender: TObject; var Key: Char);

  private
    function GetPrefix: string;
    procedure SetPrefix(const Value: string);
    function GetMidName: string;
    procedure SetMidName(const Value: string);
    function GetPostfix: string;
    procedure SetPostfix(const Value: string);
    function GetFullName: string;

  protected
    procedure DoIsCorrect(var AIsCorrect : WordBool); override;

  public
    constructor Create(AOwner : TComponent); override;

    class function CreateEditor : ISANRepExpEditor; override;

  published
    property Prefix    : string  read GetPrefix   write SetPrefix;
    property MidName   : string  read GetMidName  write SetMidName;
    property Postfix   : string  read GetPostfix  write SetPostfix;
    property FullName  : string  read GetFullName;

  end;

IMPLEMENTATION

{$R *.dfm}

//==============================================================================
// TframeNamesPrefPost
// private
function TframeNamesPrefPost.GetPrefix: string;
begin
  Result := edtNamePrefix.Text;
end;

procedure TframeNamesPrefPost.SetPrefix(const Value: string);
begin
  edtNamePrefix.Text := Value;
end;

function TframeNamesPrefPost.GetMidName: string;
begin
  Result := edtName.Text;
end;

procedure TframeNamesPrefPost.SetMidName(const Value: string);
begin
  edtName.Text := Value;
end;

function TframeNamesPrefPost.GetPostfix: string;
begin
  Result := edtNamePostfix.Text;
end;

procedure TframeNamesPrefPost.SetPostfix(const Value: string);
begin
  edtNamePostfix.Text := Value;
end;

function TframeNamesPrefPost.GetFullName: string;
begin
  Result := Prefix + MidName + Postfix;
end;

// protected
procedure TframeNamesPrefPost.DoIsCorrect(var AIsCorrect: WordBool);
begin
  AIsCorrect := CheckPasIdentifierName(FullName);
end;

// public
constructor TframeNamesPrefPost.Create(AOwner: TComponent);
begin
  inherited;

  edtName.Text         := '';
  edtNamePrefix.Text   := '';
  edtNamePostfix.Text  := '';
end;

class function TframeNamesPrefPost.CreateEditor: ISANRepExpEditor;
begin
  Result := TSANRepExpEditor.Create(Self, ['Prefix', 'MidName', 'Postfix', 'FullName']);
end;

// published
procedure TframeNamesPrefPost.edtNameChange(Sender: TObject);
begin
  lblNameResult.Caption := FullName;
  lblNameResult.Hint := lblNameResult.Caption;

  UpdateWarnShape;
end;

procedure TframeNamesPrefPost.edtNameKeyPress(Sender: TObject; var Key: Char);
var
  vSet : TSysCharSet;
begin

  vSet := ['a'..'z', '_', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)];

  if ((Sender as TEdit).Text <> '')

  or (Sender = edtName)
     and (edtNamePrefix.Text <> '')

  or (Sender = edtNamePostfix)
     and (edtNamePrefix.Text <> '') or (edtName.Text <> '')

  then
    vSet := vSet + ['0'..'9'];

  if not CharInSet(LowerCase(Key)[1], vSet) then
  begin
    Key := #0;
    MessageBeep(MB_OK);
  end;

end;
//==============================================================================

END.
