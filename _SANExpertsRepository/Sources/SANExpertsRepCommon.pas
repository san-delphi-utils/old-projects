{*******************************************************}
{                                                       }
{       SANExpertsRepCommon                             }
{       Общие параметры экспертов репозитория SAN.      }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepCommon;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, Controls, Variants, Graphics, SHFolder, Types,
  Forms, ExtCtrls, Spin,

  SANWinUtils,

  SANDesignReplacer, SANDesignWizards;

TYPE
  ESANExpertsRep = class(Exception);

  /// <summary>
  /// Базовый эксперт репозитория в данном пакете.
  /// </summary>
  TSANCustomRepExpert = class(TSANCustomRepositoryWizard)
  protected
    function GetAuthor: string; override;
    function GetPage: string; override;
    function Get_HInstace : LongWord; override; safecall;
    procedure Execute; override;
    function DLGFormClass : TFormClass; virtual; abstract;
    procedure DoExecute(const ADLGForm : TForm; const AReplacer : TSANReplacer); virtual; abstract;
  end;

  /// <summary>
  /// Интерфейс проверки ввода.
  /// </summary>
  IWarnShapeValidator = interface(IInterface)
    ['{F5427F3B-A367-4EC2-8CAF-5E9EAD80AA88}']

    function GetValidValues : boolean;
    /// <summary>
    /// Корректны ли значения.
    /// </summary>
    property ValidValues : boolean  read GetValidValues;
  end;

  /// <summary>
  /// Интерфейс реакции на проверку ввода.
  /// </summary>
  IWarnShapeValidatorHost = interface(IInterface)
    ['{7DC5AC77-2161-4895-930E-D2F8DDC6D317}']

    /// <summary>
    /// Произведена проверка.
    /// </summary>
    procedure WarnShapeValidated;

    /// <summary>
    /// Значение изменено.
    /// </summary>
    procedure ValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string);
  end;

  /// <summary>
  /// Интерфейс провайдера реакции на проверку ввода.
  /// </summary>
  IWarnShapeValidatorHostProvider = interface(IInterface)
    ['{0A771DA7-E73F-402D-96DF-1CF54BCBF1A4}']

    function GetWarnShapeValidatorHost : IWarnShapeValidatorHost;
    property WarnShapeValidatorHost : IWarnShapeValidatorHost  read GetWarnShapeValidatorHost;
  end;


/// <summary>
/// Корневая папка всех экспертов репозитория в AppData.
/// </summary>
function AppDataRoot(const AForWrite : boolean = False) : string;

/// <summary>
/// Задать для набора spinedit-ов только положительный диапазон значений.
/// </summary>
procedure PositiveSpins(const ASpins : array of TSpinEdit);

/// <summary>
/// Проверка ввода. С индикатором в виде шейпа.
/// </summary>
procedure UpdateWarnShape(const AShape: TShape;
  const AValidateCondition: boolean = True; const AWarningHint: string = '');

/// <summary>
/// Обновить положение шейпа, относительно связанного контрола.
/// </summary>
procedure UpdateShapeTopPos(const AShape : TShape; const ABindedControl : TControl);

/// <summary>
/// Построить строку статических типов, поддерживаемых визуалайзером.
/// </summary>
function VisualisersBuildStaticSupTypes(const AStrings : TStrings): string;

procedure Register;

IMPLEMENTATION

uses
  SANDesign_ToolsAPI,

  SANExpertsRepCustomExpertDialogForm,

  SANExpertsRepExpertExpertForm,

  SANExpertsRepDialogExpertForm,
  SANExpertsRepVisValueReplacerExpertForm,
  SANExpertsRepVisExtViewerExpertForm,
  SANExpertsRepPkgAsDllEntryExpertForm;

{$R ..\_autogenres\SANExpertsRepIcons.res}
{$R ..\_autogenres\SANExpertsRepBitmaps.res}
{$R ..\_autogenres\SANExpertsRepTemplates.res}

//==============================================================================
// TSANCustomRepExpert
// protected
function TSANCustomRepExpert.GetAuthor: string;
begin
  Result := 'SAN';
end;

function TSANCustomRepExpert.GetPage: string;
begin
  Result := 'SAN';
end;

function TSANCustomRepExpert.Get_HInstace: LongWord;
begin
  Result := HInstance;
end;

procedure TSANCustomRepExpert.Execute;
var
  vFormClass : TfrmCustomExpertDialogClass;
  vForm : TfrmCustomExpertDialog;
  vParams : TSANReplacer;
begin
  vFormClass := TfrmCustomExpertDialogClass(DLGFormClass);

  vForm := vFormClass.Create(nil);
  try

    if vForm.ShowModal <> mrOk then
      Exit;

    vParams := TSANReplacer.Create;
    try
      vForm.FillReplacer(vParams);

      DoExecute(vForm, vParams);

    finally
      FreeAndNil(vParams);
    end;

  finally
    FreeAndNil(vForm);
  end;
end;
//==============================================================================

function AppDataRoot(const AForWrite : boolean) : string;
begin
  Result := SpecialFolderPath(CSIDL_COMMON_APPDATA) + '\SAN\ForBDS\SANExpertsRepositoryBPL\';

  if AForWrite then
    ForceDirectories(Result);
end;

procedure PositiveSpins(const ASpins : array of TSpinEdit);
var
  i : integer;
begin

  for i := Low(ASpins) to High(ASpins) do
    with ASpins[i] do
    begin
      MaxValue   := MaxInt;
      MaxLength  := Length(IntToStr(MaxInt));
    end;

end;

procedure UpdateWarnShape(const AShape: TShape;
  const AValidateCondition: boolean; const AWarningHint: string);
var
  vWarnShapeValidatorHost : IWarnShapeValidatorHost;
  vWarnShapeValidatorHostProvider : IWarnShapeValidatorHostProvider;
begin

  with AShape do
    if AValidateCondition then
      begin
        Brush.Color := clLime;
        Hint := '';
      end
    else
      begin
        Brush.Color := clRed;
        Hint := AWarningHint;
      end;


  if Supports(AShape.Owner, IWarnShapeValidatorHostProvider, vWarnShapeValidatorHostProvider) then
    vWarnShapeValidatorHost := vWarnShapeValidatorHostProvider.WarnShapeValidatorHost

  else
  if not Supports(AShape.Owner, IWarnShapeValidatorHost, vWarnShapeValidatorHost) then
    vWarnShapeValidatorHost := nil;

  if Assigned(vWarnShapeValidatorHost) then
    vWarnShapeValidatorHost.WarnShapeValidated;
end;

procedure UpdateShapeTopPos(const AShape : TShape; const ABindedControl : TControl);
var
  vPT : TPoint;
begin

  with ABindedControl do
    vPT := ClientToScreen(CenterPoint(BoundsRect));

  with AShape do
    Top := ScreenToClient(vPT).Y - RectHeight(BoundsRect) div 2;
end;

function VisualisersBuildStaticSupTypes(const AStrings : TStrings): string;
const
  SUP_TYPE_LINE = '  SupportedTypes.InsertObject(%d, ''%s'', TObject(1));'#13#10;
var
  i : integer;
begin
  Result := '';

  for i := 0 to AStrings.Count - 1 do
    Result := Result + Format(SUP_TYPE_LINE, [i, AStrings[i]]);

  Result := TrimRight(Result);
end;

procedure Register;
begin
  RegisterPackageWizard(TSANRepExpertExpert.Create);

  RegisterPackageWizard(TSANRepExpertCustomDialog.Create);
  RegisterPackageWizard(TSANRepExpertVisValueReplacer.Create);
  RegisterPackageWizard(TSANRepExpertVisExtViewer.Create);
  RegisterPackageWizard(TSANRepExpertPkgAsDllEntry.Create);
end;


END.
