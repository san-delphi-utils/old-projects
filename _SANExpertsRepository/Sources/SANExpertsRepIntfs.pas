{*******************************************************}
{                                                       }
{       SANExpertsRepIntfs                              }
{       Интерфейсы экспертов репозитория.               }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepIntfs;
{$I NX.INC}
INTERFACE

CONST
  SRPE_ENTRY_POINT_NAME = '_BEAB0E27B65347BC8BD89162511C3E11';

TYPE
  /// <summary>
  /// Параметр формы редактирования.
  /// </summary>
  ISANRepExpParam = interface(IInterface)
    ['{642F8253-6203-4E9A-8239-DFFD43E5B6A7}']

    function GetName : WideString; safecall;
    /// <summary>
    /// Уникальное имя параметра в рамках редактора.
    /// </summary>
    property Name : WideString  read GetName;

    function GetValue : WideString; safecall;
    procedure SetValue(const AValue : WideString); safecall;
    /// <summary>
    /// Значение параметра.
    /// </summary>
    property Value : WideString  read GetValue  write SetValue;
  end;

  ISANRepExpEditorControl = interface;

  /// <summary>
  /// Редактор одного или нескольких параметров.
  /// </summary>
  ISANRepExpEditor = interface(IInterface)
    ['{60111553-C72C-4C18-9B30-ACE514584664}']

    function GetName : WideString; safecall;
    /// <summary>
    /// Уникальное имя редактора.
    /// </summary>
    property Name : WideString  read GetName;

    function GetParamsCount : integer; safecall;
    /// <summary>
    /// Количество параметров.
    /// </summary>
    property ParamsCount : integer  read GetParamsCount;

    function GetParamsNames(const AIndex : integer) : WideString; safecall;
    /// <summary>
    /// Имя параметра по индексу.
    /// </summary>
    property ParamsNames[const AIndex : integer] : WideString  read GetParamsNames;

    /// <summary>
    /// Создать контрол редактора.
    /// </summary>
    /// <param name="AParentHandle">
    /// Описатель родительского окна.
    /// </param>
    /// <param name="AParamBaseName">
    /// Базовое имя параметра.
    /// </param>
    /// <returns>
    /// ISANRepExpEditorControl. Созданый контрол.
    /// </returns>
    function ControlCreate(const AParentHandle : THandle; const AParamBaseName : WideString) : ISANRepExpEditorControl; safecall;
  end;

  /// <summary>
  /// Контрол редактора параметров.
  /// </summary>
  ISANRepExpEditorControl = interface(IInterface)
    ['{6AC66744-A202-4C81-B0AA-313FEF2E2A57}']


    function GetEditor : ISANRepExpEditor; safecall;
    /// <summary>
    /// Редактор к которому принадлежит контрол.
    /// </summary>
    property Editor : ISANRepExpEditor  read GetEditor;

    function GetEditorHandle : THandle; safecall;
    /// <summary>
    /// Описатель окна контрола.
    /// </summary>
    property EditorHandle : THandle  read GetEditorHandle;

    function GetParamBaseName : WideString; safecall;
    /// <summary>
    /// Базовое имя параметра.
    /// </summary>
    property ParamBaseName : WideString  read GetParamBaseName;

    function GetWarning : WideString; safecall;
    procedure SetWarning(const AWarning : WideString); safecall;
    /// <summary>
    /// Сообщение, которое показывается при некорректном вводе.
    /// </summary>
    property Warning : WideString  read GetWarning  write SetWarning;

    function GetIsCorrect : WordBool; safecall;
    /// <summary>
    /// Параметры корректны?
    /// </summary>
    property IsCorrect : WordBool  read GetIsCorrect;

    function GetIsStored : WordBool; safecall;
    procedure SetIsStored(const AIsStored : WordBool); safecall;
    /// <summary>
    /// Сохраняются ли параметры редактора.
    /// </summary>
    property IsStored : WordBool  read GetIsStored  write SetIsStored;

    function GetParamsCount : integer; safecall;
    /// <summary>
    /// Количество параметров.
    /// </summary>
    property ParamsCount : integer  read GetParamsCount;

    function GetParams(const AIndex : integer) : ISANRepExpParam; safecall;
    /// <summary>
    /// Параметр по индексу.
    /// </summary>
    property Params[const AIndex : integer] : ISANRepExpParam  read GetParams;

    /// <summary>
    /// Поиск параметра по имени.
    /// </summary>
    /// <param name="AParam">
    /// Найденый параметр в случае успеха.
    /// </param>
    /// <param name="AName">
    /// Имя параметра, по которому осуществляется поиск.
    /// </param>
    /// <returns>
    /// WordBool. True, в случае успеха.
    /// </returns>
    function ParamsFind(out AParam : ISANRepExpParam; const AName : WideString) : WordBool; safecall;

    /// <summary>
    /// Освободить контрол редактора.
    /// </summary>
    procedure FreeControl; safecall;
  end;

  /// <summary>
  /// Интерфейс управления редакторами.
  /// </summary>
  ISANRepExpEditorsManager = interface(IInterface)
    ['{021778DB-82CF-41BC-A941-67318077A3D7}']

    function GetEditorsCount : integer; safecall;
    /// <summary>
    /// Количество параметров.
    /// </summary>
    property EditorsCount : integer  read GetEditorsCount;

    function GetEditors(const AIndex : integer) : ISANRepExpEditor; safecall;
    /// <summary>
    /// Параметр по индексу.
    /// </summary>
    property Editors[const AIndex : integer] : ISANRepExpEditor  read GetEditors;

    /// <summary>
    /// Поиск редактора по имени.
    /// </summary>
    /// <param name="AEditor">
    /// Найденый редактор в случае успеха.
    /// </param>
    /// <param name="AName">
    /// Имя редактора, по которому осуществляется поиск.
    /// </param>
    /// <returns>
    /// WordBool. True, в случае успеха.
    /// </returns>
    function EditorsFind(out AEditor : ISANRepExpEditor; const AName : WideString) : WordBool; safecall;

    /// <summary>
    /// Регистрация редактора.
    /// </summary>
    /// <param name="AEditor">
    /// Редактор.
    /// </param>
    /// <returns>
    /// WordBool. True, успешно зарегистрирован.
    /// </returns>
    function RegEditor(const AEditor : ISANRepExpEditor) : WordBool; safecall;

    /// <summary>
    /// Разрегистрация редактора.
    /// </summary>
    /// <param name="AEditorName">
    /// Имя редактора.
    /// </param>
    /// <returns>
    /// WordBool. True, успешно разрегистрирован.
    /// </returns>
    function UnRegEditor(const AEditorName : WideString) : WordBool; safecall;

    function GetAppHandle : THandle; safecall;
    /// <summary>
    /// Описатель окна приложения.
    /// </summary>
    property AppHandle : THandle  read GetAppHandle;
  end;

IMPLEMENTATION

END.
