{*******************************************************}
{                                                       }
{       SANExpertsRepPkgAsDllEntryExpertForm            }
{       Эксперт создания точки входа "пакета-как-Dll".  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepPkgAsDllEntryExpertForm;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, StdCtrls, Buttons, ExtCtrls, TypInfo,

  SANWinUtils,

  SANDesignReplacer, SANDesignCreaters, SANDesignWizards,

  SANExpertsRepCommon,
  SANExpertsRepCustomExpertDialogForm,
  SANExpertsRepCustomVersionDataModuleForm,
  SANExpertsRepCustomShapedFrame,
  SANExpertsRepEntryPointNameShapedFrame,
  SANExpertsRepGUIDShapedFrame,
  SANExpertsRepFormatNameShapedFrame,
  SANExpertsRepNamesPrefPostShapedFrame;

TYPE
  TfrmPkgAsDllEntryExpert = class;

  TPkgAsDllEntryProps = class(TCustomVersionDataModuleProps)
  private
    FEntryPointName: string;

    function GetForm: TfrmPkgAsDllEntryExpert;
    procedure SetEntryPointName(const Value: string);

  protected
    procedure DoDefaultStrPropValue(const APropInfo: PPropInfo); override;

  public
    property Form : TfrmPkgAsDllEntryExpert  read GetForm;

  published
    property EntryPointName  : string  read FEntryPointName  write SetEntryPointName;

  end;

  TfrmPkgAsDllEntryExpert = class(TfrmCustomVersionDataModule)
    frameEntryPointName: TframeEntryPointNameShaped;
    lblEntryPointName: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    function GetProps: TPkgAsDllEntryProps;

  protected
    function GetINIFileName: TFileName; override;
    function ExpertPropsClass : TCustomExpertPropsClass; override;
    procedure DoWarnShapeValueUpdated(const AFrame : TFrame; const AControl : TControl;
      const AValue : Variant; var AWarnHint : string); override;

  public
    procedure FillReplacer(const AReplacer : TSANReplacer); override;

    property Props : TPkgAsDllEntryProps  read GetProps;
  end;

  /// <summary>
  /// Эксперт создания точки входа "пакета-как-Dll".
  /// </summary>
  TSANRepExpertPkgAsDllEntry = class(TSANCustomRepExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetGlyphResName : string; override;
    function DLGFormClass : TFormClass; override;
    procedure DoExecute(const ADLGForm : TForm; const AReplacer : TSANReplacer); override;
  end;

IMPLEMENTATION

{$R *.dfm}

const
  ICON_RES_NAME = 'SAN_EXP_REP_ICON_PKGASDLLENTRY';

resourcestring
  sExpertCaption = 'Эксперт создания точки входа "Пакета-как-Dll"';


//==============================================================================
// TPkgAsDllEntryProps
// private
function TPkgAsDllEntryProps.GetForm: TfrmPkgAsDllEntryExpert;
begin
  Result := (inherited Form) as TfrmPkgAsDllEntryExpert;
end;

procedure TPkgAsDllEntryProps.SetEntryPointName(const Value: string);
begin
  if FEntryPointName = Value then
    Exit;

  FEntryPointName := Value;

  if Assigned(Form) then
    Form.frameEntryPointName.EntryPointName := Value;
end;

// protected
procedure TPkgAsDllEntryProps.DoDefaultStrPropValue(const APropInfo: PPropInfo);
begin

  if CompareText(String(APropInfo^.Name), 'EntryPointName') = 0 then
    SetStrProp(Self, APropInfo, Form.frameEntryPointName.EntryPointName)
  else
    inherited;

end;
//==============================================================================

//==============================================================================
// TfrmPkgAsDllEntryExpert
// private
function TfrmPkgAsDllEntryExpert.GetProps: TPkgAsDllEntryProps;
begin
  Result := (inherited Props) as TPkgAsDllEntryProps;
end;

// protected
function TfrmPkgAsDllEntryExpert.GetINIFileName: TFileName;
begin
  Result := 'PkgAsDllEntry.ini';
end;

function TfrmPkgAsDllEntryExpert.ExpertPropsClass: TCustomExpertPropsClass;
begin
  Result := TPkgAsDllEntryProps;
end;

procedure TfrmPkgAsDllEntryExpert.DoWarnShapeValueUpdated(const AFrame: TFrame;
  const AControl: TControl; const AValue: Variant; var AWarnHint: string);
begin

  if AFrame = frameEntryPointName then
    begin
      AWarnHint := 'Некорректное имя функции точки входа';
      Props.EntryPointName := AValue;
    end
  else
    inherited;

end;

// public
procedure TfrmPkgAsDllEntryExpert.FillReplacer(const AReplacer: TSANReplacer);
begin
  AReplacer.AddPair('EntryPointName', Props.EntryPointName);
  inherited;
end;

// published
procedure TfrmPkgAsDllEntryExpert.FormCreate(Sender: TObject);
begin
  Caption := LoadResString(@sExpertCaption);
  Icon.LoadFromResourceName(HInstance, ICON_RES_NAME);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANRepExpertPkgAsDllEntry
// protected
function TSANRepExpertPkgAsDllEntry.GetIDString: string;
begin
  Result := 'SAN.RepExp.PkgAsDllEntry';
end;

function TSANRepExpertPkgAsDllEntry.GetName: string;
begin
  Result := 'PkgAsDll Entry';
end;

function TSANRepExpertPkgAsDllEntry.GetComment: string;
begin
  Result := LoadResString(@sExpertCaption);
end;

function TSANRepExpertPkgAsDllEntry.GetGlyphResName: string;
begin
  Result := ICON_RES_NAME;
end;

function TSANRepExpertPkgAsDllEntry.DLGFormClass: TFormClass;
begin
  Result := TfrmPkgAsDllEntryExpert;
end;

procedure TSANRepExpertPkgAsDllEntry.DoExecute(const ADLGForm: TForm;
  const AReplacer: TSANReplacer);
var
  vForm : TfrmPkgAsDllEntryExpert;
  vModule : IOTAModule;
begin
  vForm := TfrmPkgAsDllEntryExpert(ADLGForm);

  vModule := TSANStaticModuleCreator.CreateModule
  (
    ctForm,
    vForm.frameNames.FullName,
    '',
    vForm.Props.FormNameFmt,
    vForm.Props.UnitNameFmt,
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_PKGASDLLENTRY_DFM'),
    ReadTextResourceW(HInstance, 'SAN_EXP_REP_TEMPLATE_PKGASDLLENTRY_PAS'),
    AReplacer
  );

  GetActiveProject.AddFile(vModule.FileName, True);

  vModule.Show;
end;
//==============================================================================

END.
