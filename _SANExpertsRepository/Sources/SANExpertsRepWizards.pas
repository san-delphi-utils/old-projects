{*******************************************************}
{                                                       }
{       SANExpertsRepWizards                            }
{       Менедежер экспертов репозитория.                }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepWizards;
{$I NX.INC}
INTERFACE

USES
  SANDesign_ToolsAPI,
  Windows, SysUtils, Classes, Generics.Collections, Graphics,
  SANDesignWizards;

TYPE
  /// <summary>
  /// Абстрактный класс эксперта репозитория SANExpertsRep.
  /// </summary>
  TSANExpertsRepCustomWizard = class(TSANStaticRepositoryWizard)
  private
    FGlyph : TIcon;
    FWizardIndex : integer;
    FXML : string;

  protected
    procedure Execute; override;

  public
    constructor Create(const AXML, AIDString, AName, AAuthor, AComment, APage : string;
      const AGlyphFileName : TFileName);
    destructor Destroy; override;

    procedure RegWizard;
    procedure UnRegWizard;

    property IDString  : string  read GetIDString;
    property Name      : string  read GetName;
    property Author    : string  read GetAuthor;
    property Comment   : string  read GetComment;
    property Page      : string  read GetPage;

    property Glyph : TIcon  read FGlyph;
    property XML : string  read FXML;
  end;
  TSANExpertsRepCustomWizardClass = class of TSANExpertsRepCustomWizard;

  /// <summary>
  /// Класс эксперта репозитория создающий модуль внутри проекта.
  /// </summary>
  TSANExpertsRepFormWizard     = class(TSANExpertsRepCustomWizard, IOTAFormWizard);
  /// <summary>
  /// Класс эксперта репозитория создающий проект.
  /// </summary>
  TSANExpertsRepProjectWizard  = class(TSANExpertsRepCustomWizard, IOTAProjectWizard);

  /// <summary>
  /// Список экспертов.
  /// </summary>
  TSANExpertsRepWizards = class(TObjectList<TSANExpertsRepCustomWizard>)
  public
    procedure UnRegWizards;
  end;

IMPLEMENTATION

uses
  SANExpertsRepManager;

//==============================================================================
// TSANExpertsRepCustomWizard
// protected
procedure TSANExpertsRepCustomWizard.Execute;
begin
  SANRepExpEditorsManager.ExecuteWizard(Self);
end;


procedure TSANExpertsRepCustomWizard.RegWizard;
begin
  FWizardIndex := (BorlandIDEServices as IOTAWizardServices).AddWizard(Self);
end;

procedure TSANExpertsRepCustomWizard.UnRegWizard;
var
  vWizardIndex : integer;
begin
  vWizardIndex := FWizardIndex;
  FWizardIndex := -1;
  (BorlandIDEServices as IOTAWizardServices).RemoveWizard(vWizardIndex);
end;

constructor TSANExpertsRepCustomWizard.Create(const AXML, AIDString, AName, AAuthor, AComment,
  APage: string; const AGlyphFileName: TFileName);
begin
  FWizardIndex := -1;

  FGlyph := TIcon.Create;
  if FileExists(AGlyphFileName) then
    FGlyph.LoadFromFile(AGlyphFileName);

  inherited Create(AIDString, AName, AAuthor, AComment, APage, FGlyph.Handle);

  FXML := AXML;
end;

destructor TSANExpertsRepCustomWizard.Destroy;
begin
  FreeAndNil(FGlyph);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANExpertsRepWizards
procedure TSANExpertsRepWizards.UnRegWizards;
var
  vWizard : TSANExpertsRepCustomWizard;
begin

  for vWizard in Self do
    vWizard.UnRegWizard;

  Clear;
end;
//==============================================================================

END.

