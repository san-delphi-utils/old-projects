inherited frmDialogExpert: TfrmDialogExpert
  Caption = 'frmDialogExpert'
  ClientHeight = 406
  ClientWidth = 619
  ExplicitWidth = 625
  ExplicitHeight = 434
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlButtons: TPanel
    Top = 365
    Width = 619
    ExplicitTop = 365
    ExplicitWidth = 619
    inherited btnOK: TBitBtn
      Left = 453
      ExplicitLeft = 453
    end
    inherited btnCancel: TBitBtn
      Left = 536
      ExplicitLeft = 536
    end
  end
  inherited pnlClient: TPanel
    Width = 619
    Height = 365
    Padding.Left = 6
    Padding.Right = 6
    ExplicitWidth = 619
    ExplicitHeight = 365
    object gbNames: TGroupBox
      Left = 6
      Top = 0
      Width = 607
      Height = 164
      Align = alTop
      Caption = #1048#1084#1077#1085#1072
      Padding.Left = 6
      Padding.Right = 6
      Padding.Bottom = 6
      TabOrder = 0
      object lblDLGName: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 15
        Width = 68
        Height = 13
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        Caption = #1048#1084#1103' '#1076#1080#1072#1083#1086#1075#1072':'
      end
      object lblFormNameFmt: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 71
        Width = 212
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1092#1086#1088#1084#1099' (%s - '#1080#1084#1103' '#1076#1080#1072#1083#1086#1075#1072'):'
      end
      object lblUnitNameFmt: TLabel
        AlignWithMargins = True
        Left = 8
        Top = 117
        Width = 215
        Height = 13
        Margins.Left = 0
        Margins.Top = 8
        Margins.Right = 0
        Align = alTop
        Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' '#1084#1086#1076#1091#1083#1103' (%s - '#1080#1084#1103' '#1076#1080#1072#1083#1086#1075#1072'):'
      end
      inline frameNames: TframeNamesPrefPostShaped
        Left = 8
        Top = 31
        Width = 591
        Height = 32
        Align = alTop
        TabOrder = 0
        ExplicitLeft = 8
        ExplicitTop = 31
        ExplicitWidth = 591
        inherited shpWarning: TShape
          Left = 576
          ExplicitLeft = 576
        end
        inherited pnlClient: TPanel
          Width = 568
          ExplicitWidth = 568
        end
      end
      inline frameFmtFormName: TframeFormatNameShaped
        Left = 8
        Top = 87
        Width = 591
        Height = 22
        Align = alTop
        TabOrder = 1
        ExplicitLeft = 8
        ExplicitTop = 87
        ExplicitWidth = 591
        inherited shpWarning: TShape
          Left = 576
          ExplicitLeft = 576
        end
        inherited pnlClient: TPanel
          Width = 568
          ExplicitWidth = 568
          inherited bvlResults: TBevel
            Left = 390
            ExplicitLeft = 390
          end
          inherited lblNameResult: TLabel
            Left = 399
            ExplicitLeft = 399
          end
          inherited edtNameFmt: TEdit
            Width = 382
            ExplicitWidth = 382
          end
        end
      end
      inline frameFmtUnitName: TframeFormatNameShaped
        Left = 8
        Top = 133
        Width = 591
        Height = 22
        Align = alTop
        TabOrder = 2
        ExplicitLeft = 8
        ExplicitTop = 133
        ExplicitWidth = 591
        inherited shpWarning: TShape
          Left = 576
          ExplicitLeft = 548
        end
        inherited pnlClient: TPanel
          Width = 568
          ExplicitWidth = 568
          inherited bvlResults: TBevel
            Left = 390
            ExplicitLeft = 390
          end
          inherited lblNameResult: TLabel
            Left = 399
            ExplicitLeft = 399
          end
          inherited edtNameFmt: TEdit
            Width = 382
            ExplicitWidth = 382
          end
        end
      end
    end
    object gbPAS: TGroupBox
      Left = 6
      Top = 164
      Width = 607
      Height = 43
      Align = alTop
      Caption = #1050#1086#1076
      Padding.Left = 6
      Padding.Right = 6
      Padding.Bottom = 6
      TabOrder = 1
      object chkUseCaption: TCheckBox
        AlignWithMargins = True
        Left = 8
        Top = 15
        Width = 182
        Height = 20
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 8
        Margins.Bottom = 0
        Align = alLeft
        Caption = #1055#1077#1088#1077#1076#1072#1074#1072#1090#1100' '#1079#1072#1075#1086#1083#1086#1074#1086#1082' (Caption)'
        TabOrder = 0
        OnClick = chksCodeChange
      end
      object chkUseOwner: TCheckBox
        Left = 198
        Top = 15
        Width = 401
        Height = 20
        Align = alClient
        Caption = #1055#1077#1088#1077#1076#1072#1074#1072#1090#1100' '#1074#1083#1072#1076#1077#1083#1100#1094#1072' '#1092#1086#1088#1084#1099' (Owner)'
        TabOrder = 1
        OnClick = chksCodeChange
      end
    end
    object gbDFM: TGroupBox
      Left = 6
      Top = 207
      Width = 607
      Height = 158
      Align = alClient
      Caption = #1042#1080#1079#1091#1072#1083#1080#1079#1072#1094#1080#1103
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object gbDFMForm: TGroupBox
        Left = 8
        Top = 12
        Width = 433
        Height = 44
        Caption = #1060#1086#1088#1084#1072' '#1076#1080#1072#1083#1086#1075#1072
        TabOrder = 0
        object lblPosition: TLabel
          Left = 232
          Top = 17
          Width = 41
          Height = 13
          Caption = 'Position:'
        end
        object lblBorderStyle: TLabel
          Left = 8
          Top = 17
          Width = 60
          Height = 13
          Caption = 'BorderStyle:'
        end
        object cmbxBorderStyle: TComboBox
          Left = 73
          Top = 14
          Width = 145
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          OnChange = cmbxsVisualizeChange
        end
        object cmbxPosition: TComboBox
          Left = 279
          Top = 14
          Width = 145
          Height = 21
          Style = csDropDownList
          TabOrder = 1
          OnChange = cmbxsVisualizeChange
        end
      end
      object gbDFMPnlClient: TGroupBox
        Left = 447
        Top = 12
        Width = 150
        Height = 44
        Caption = #1055#1072#1085#1077#1083#1100' '#1082#1083#1080#1077#1085#1090#1089#1082#1086#1081' '#1095#1072#1089#1090#1080
        TabOrder = 1
        DesignSize = (
          150
          44)
        object lblPnlClientPadding: TLabel
          Left = 8
          Top = 17
          Width = 42
          Height = 13
          Caption = 'Padding:'
        end
        object sePnlClientPadding: TSpinEdit
          Left = 56
          Top = 14
          Width = 86
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
          OnChange = sesVisualizeChange
        end
      end
      object gbDFMPnlButtons: TGroupBox
        Left = 8
        Top = 59
        Width = 263
        Height = 44
        Caption = #1055#1072#1085#1077#1083#1100' '#1082#1085#1086#1087#1086#1082
        TabOrder = 2
        object lblPnlButtonsHeight: TLabel
          Left = 8
          Top = 17
          Width = 35
          Height = 13
          Caption = 'Height:'
        end
        object lblPnlButtonsPadding: TLabel
          Left = 135
          Top = 17
          Width = 42
          Height = 13
          Caption = 'Padding:'
        end
        object sePnlButtonsHeight: TSpinEdit
          Left = 49
          Top = 14
          Width = 73
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
          OnChange = sesVisualizeChange
        end
        object sePnlButtonsPadding: TSpinEdit
          Left = 183
          Top = 14
          Width = 73
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 0
          OnChange = sesVisualizeChange
        end
      end
      object gbDFMButtons: TGroupBox
        Left = 8
        Top = 106
        Width = 453
        Height = 44
        Caption = #1050#1085#1086#1087#1082#1080
        TabOrder = 3
        object lblButtonWidth: TLabel
          Left = 8
          Top = 17
          Width = 32
          Height = 13
          Caption = 'Width:'
        end
        object lblButtonMargins: TLabel
          Left = 135
          Top = 17
          Width = 41
          Height = 13
          Caption = 'Margins:'
        end
        object lblButtonsAlign: TLabel
          Left = 267
          Top = 17
          Width = 27
          Height = 13
          Caption = 'Align:'
        end
        object seButtonWidth: TSpinEdit
          Left = 49
          Top = 14
          Width = 73
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
          OnChange = sesVisualizeChange
        end
        object seButtonMargins: TSpinEdit
          Left = 183
          Top = 14
          Width = 73
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 0
          OnChange = sesVisualizeChange
        end
        object cmbxButtonsAlign: TComboBox
          Left = 299
          Top = 14
          Width = 145
          Height = 21
          Style = csDropDownList
          TabOrder = 2
          OnChange = cmbxsVisualizeChange
        end
      end
    end
  end
end
