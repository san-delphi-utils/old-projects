{*******************************************************}
{                                                       }
{       SANExpertsRepEntry                              }
{       Точка входа пакета.                             }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepEntry;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, Variants, Forms,
  SANPkgAsDllClient,
  SANExpertsRepIntfs;

//TYPE


IMPLEMENTATION

uses
  SANExpertsRepNamesPrefPostForm;


//==============================================================================
type
  /// <summary>
  /// Класс точки входа
  /// </summary>
  TSANEREntry = class(TSANPkgAsDllEntry)
  protected
    procedure DoInit(const AParams: IInterface); override;
    procedure DoDone(const AParams: IInterface); override;

  end;

// protected
procedure TSANEREntry.DoInit(const AParams: IInterface);
var
  vEdtsMan : ISANRepExpEditorsManager;
begin

  if Supports(AParams, ISANRepExpEditorsManager, vEdtsMan) then
  begin
    Application.Handle := vEdtsMan.AppHandle;

    TfrmNamesPrefPost.RegEditor(vEdtsMan);

  end;

end;

procedure TSANEREntry.DoDone(const AParams: IInterface);
var
  vEdtsMan : ISANRepExpEditorsManager;
begin

  if Supports(AParams, ISANRepExpEditorsManager, vEdtsMan) then
  begin
    TfrmNamesPrefPost.UnRegEditor(vEdtsMan);

  end;

end;
//==============================================================================

exports
  GetPkgAsDllEntryPoint name SRPE_ENTRY_POINT_NAME;

INITIALIZATION
  PkgAsDllEntryClass := TSANEREntry;
END.
