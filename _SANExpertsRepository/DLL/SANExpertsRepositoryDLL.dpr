library SANExpertsRepositoryDLL;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  ShareMem,
  SysUtils,
  Classes,
  Forms,
  Dialogs,
  SANDesign_ToolsAPI;

{$R *.res}

type
  TWis = class(TNotifierObject, IOTAWizard, IOTARepositoryWizard,
    IOTARepositoryWizard60, IOTARepositoryWizard80, IOTAFormWizard)
  protected
    function GetIDString: string;
    function GetName: string;
    function GetState: TWizardState;
    procedure Execute;

    function GetAuthor: string;
    function GetComment: string;
    function GetPage: string;
    function GetGlyph: Cardinal;

    // IOTARepositoryWizard60
    function GetDesigner: string;

    // IOTARepositoryWizard80
    function GetGalleryCategory: IOTAGalleryCategory;
    function GetPersonality: string;
  end;

{ TWis }

procedure TWis.Execute;
begin
  ShowMessage('Execute');
end;

function TWis.GetAuthor: string;
begin
  Result := 'SAN';
end;

function TWis.GetComment: string;
begin
  Result := 'Test';
end;

function TWis.GetDesigner: string;
begin
  Result := dVCL;
end;

function TWis.GetIDString: string;
begin
  Result := 'SAN.Test.Dll';
end;

function TWis.GetName: string;
begin
  Result := 'Test';
end;

function TWis.GetPage: string;
begin
  Result := 'SAN';
end;

function TWis.GetGalleryCategory: IOTAGalleryCategory;
begin
  Result := (BorlandIDEServices as IOTAGalleryCategoryManager).FindCategory('Borland.Delphi.New.Expert');
end;

function TWis.GetPersonality: string;
begin
  Result := sDelphiPersonality;
end;

function TWis.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

function TWis.GetGlyph: Cardinal;
begin
  Result := 0;
end;


const  InvalidIndex : integer = -1;
var
  FExpertIndex: integer;

//  vDlg : TSANRepExpertCustomDialog;

procedure DoneWizard;
var
  vWizardServices: IOTAWizardServices;
begin
  try

    if FExpertIndex <> InvalidIndex then
      begin
        vWizardServices := BorlandIDEServices as IOTAWizardServices;
        vWizardServices.RemoveWizard(FExpertIndex);
        FExpertIndex := InvalidIndex;
        vWizardServices := nil;
      end;

  finally
    SANDesign_ToolsAPI.FinalSANToolsAPI;
  end;

  ShowMessage('Done!');
end;

function InitWizard(const ABorlandIDEServices: IBorlandIDEServices;
    ARegisterProc: TWizardRegisterProc;
    var ATerminate: TWizardTerminateProc): Boolean stdcall;
var
  vWizardServices: IOTAWizardServices;
begin
  SANDesign_ToolsAPI.InitSANToolsAPI(ABorlandIDEServices, ARegisterProc);
  ATerminate := DoneWizard;

  Application.Handle := (BorlandIDEServices as IOTAServices).GetParentHandle;

  if Supports(BorlandIDEServices, IOTAWizardServices, vWizardServices) then
    FExpertIndex := vWizardServices.AddWizard
    (
      TWis.Create
//    TSANRepExpertCustomDialog.Create
    )
  else
    FExpertIndex := InvalidIndex;

//  if FExpertIndex <> InvalidIndex then
//    begin
//      vWizardServices := BorlandIDEServices as IOTAWizardServices;
//      vWizardServices.RemoveWizard(FExpertIndex);
//      FExpertIndex := InvalidIndex;
//    end;


//  ShowMessageFmt('Index: %d', [FExpertIndex]);

//  ARegisterProc(TSANRepExpertCustomDialog.Create);

  Result := True;
end;

exports
  InitWizard name WizardEntryPoint;



begin
end.
