{*******************************************************}
{                                                       }
{       SANExpertsRepositoryLogo                        }
{       Отображение логотипа пакета при загрузке IDE.   }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExpertsRepositoryLogo;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, SHFolder,
  ToolsAPI,
  SANWinUtils,
  SANExpertsRepManager;

procedure Register;

IMPLEMENTATION

{$R ..\_autogenres\SANExpertsRepBitmaps.RES}

procedure Register;
var
  vPath : string;
begin

  if Assigned(SplashScreenServices) then
    SplashScreenServices.AddPluginBitmap
    (
      'SAN Repository Experts',
      LoadBitmap(hInstance, 'SAN_EXP_REP_BMP_LOGO'),
      False,
      '',
      {$I SANExpertsRepositoryBPL.textversion}
    );

  vPath := SpecialFolderPath(CSIDL_COMMON_APPDATA) + SAN_EXPERTS_REP_XML_FOLDER + '\';
  ForceDirectories(vPath);
  SANRepExpEditorsManager.LoadWizardsFromFolder(vPath);
end;

END.
