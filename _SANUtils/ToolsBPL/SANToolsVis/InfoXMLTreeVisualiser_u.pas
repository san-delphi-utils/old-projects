{*******************************************************}
{                                                       }
{       InfoXMLTreeVisualiser_u                         }
{       Визуалайзер для интерфейса ISANInfoXMLTree      }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT InfoXMLTreeVisualiser_u;
// Visualizer
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  ToolsAPI,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ImgList, StdCtrls, StrUtils, xmldom, XMLIntf, msxmldom,
  XMLDoc, SHFolder;
//==============================================================================
TYPE
  TAvailableState = (asAvailable, asProcRunning, asOutOfScope);
//------------------------------------------------------------------------------
  TInfoXMLTreeViewerFrame = class(TFrame, IOTADebuggerVisualizerExternalViewerUpdater, IOTAThreadNotifier)
    stNotAvailable: TStaticText;
    tvVis: TTreeView;
    xml: TXMLDocument;

    procedure FrameResize(Sender: TObject);

  private
    FOwningForm: TCustomForm;
    FAvailableState : TAvailableState;
    FClosedProc: TOTAVisualizerClosedProcedure;

    FNotifierIndex: Integer;
    FCompleted: Boolean;
    FDeferredResult: string;
    FDeferredError: Boolean;

  private
    procedure Set_AvailableState(const Value: TAvailableState);

    function Evaluate(Expression: string): string;

  protected
    procedure SetParent(AParent: TWinControl); override;

    procedure SetForm(AForm: TCustomForm);
	
    // Fill in these methods
    procedure DoRefreshVisualizer(const Expression, TypeName, EvalResult: String);
    procedure DoAvailableStateChange;

  public
    // IOTADebuggerVisualizerExternalViewerUpdater
    procedure CloseVisualizer;
    procedure MarkUnavailable(Reason: TOTAVisualizerUnavailableReason);
    procedure RefreshVisualizer(const Expression, TypeName, EvalResult: String);
    procedure SetClosedCallback(ClosedProc: TOTAVisualizerClosedProcedure);

    // IOTAThreadNotifier
    procedure AfterSave;
    procedure BeforeSave;
    procedure Destroyed;
    procedure Modified;
    procedure ThreadNotify(Reason: TOTANotifyReason);
    procedure EvaluteComplete(const ExprStr, ResultStr: string; CanModify: Boolean;
      ResultAddress, ResultSize: LongWord; ReturnCode: Integer);
    procedure ModifyComplete(const ExprStr, ResultStr: string; ReturnCode: Integer);

    constructor Create(AOwner : TComponent); override;

	
  public
    property AvailableState : TAvailableState  read FAvailableState  write Set_AvailableState;

  end;//TInfoXMLTreeViewerFrame
//==============================================================================
procedure Register;
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
uses
  DesignIntf, Actnlist, Menus, IniFiles,
  SANFilesAndFS;
//==============================================================================
{$R *.dfm}
//==============================================================================

resourcestring
  sFormCaption           = 'InfoXMLTree visualizer for %s';
  sProcessNotAccessible  = 'process not accessible';
  sOutOfScope            = 'out of scope';

type
  IFrameFormHelper = interface
    ['{3DA7E3AC-5432-49C6-A79A-C63156B209A8}']
    function GetForm: TCustomForm;
    function GetFrame: TCustomFrame;
    procedure SetForm(Form: TCustomForm);
    procedure SetFrame(Form: TCustomFrame);
  end;//IFrameFormHelper
//------------------------------------------------------------------------------
  TInfoXMLTreeViewerForm = class(TInterfacedObject, INTACustomDockableForm, IFrameFormHelper)
  private
    FMyFrame: TInfoXMLTreeViewerFrame;
    FMyForm: TCustomForm;
    FExpression, FTypeName, FEvalResult: string;

  public
    constructor Create(const AExpression, ATypeName, AEvalResult: string);

    { INTACustomDockableForm }
    function GetCaption: string;
    function GetFrameClass: TCustomFrameClass;
    procedure FrameCreated(AFrame: TCustomFrame);
    function GetIdentifier: string;
    function GetMenuActionList: TCustomActionList;
    function GetMenuImageList: TCustomImageList;
    procedure CustomizePopupMenu(PopupMenu: TPopupMenu);
    function GetToolbarActionList: TCustomActionList;
    function GetToolbarImageList: TCustomImageList;
    procedure CustomizeToolBar(ToolBar: TToolBar);
    procedure LoadWindowState(Desktop: TCustomIniFile; const Section: string);
    procedure SaveWindowState(Desktop: TCustomIniFile; const Section: string; IsProject: Boolean);
    function GetEditState: TEditState;
    function EditAction(Action: TEditAction): Boolean;

    { IFrameFormHelper }
    function GetForm: TCustomForm;
    function GetFrame: TCustomFrame;
    procedure SetForm(Form: TCustomForm);
    procedure SetFrame(Frame: TCustomFrame);

  end;//TInfoXMLTreeViewerForm
//==============================================================================
  TInfoXMLTreeVisualiser = class(TInterfacedObject, IOTADebuggerVisualizer, IOTADebuggerVisualizerExternalViewer)
  private
    FSupportedTypes : TStrings;

  protected
    //IOTADebuggerVisualizer
    procedure GetSupportedType(Index: Integer; var TypeName: String; var AllDescendents: Boolean);
    function GetSupportedTypeCount: Integer;
    function GetVisualizerDescription: String;
    function GetVisualizerIdentifier: String;
    function GetVisualizerName: String;

    // IOTADebuggerVisualizerExternalViewer
    function GetMenuText: String;
    function Show(const Expression, TypeName, EvalResult: String; SuggestedLeft, SuggestedTop: Integer): IOTADebuggerVisualizerExternalViewerUpdater;

  public
    constructor Create;
    destructor Destroy; override;

  end;//TInfoXMLTreeVisualiser
//==============================================================================

var
  vInfoXMLTreeVisualiser: IOTADebuggerVisualizer;

//==============================================================================
// TInfoXMLTreeVisualiser
//==============================================================================
//IOTADebuggerVisualizer
//==============================================================================
procedure TInfoXMLTreeVisualiser.GetSupportedType(Index: Integer;
  var TypeName: String; var AllDescendents: Boolean);
begin
  TypeName := FSupportedTypes[Index];
  AllDescendents := False;
end;//TInfoXMLTreeVisualiser.GetSupportedType
//==============================================================================
function TInfoXMLTreeVisualiser.GetSupportedTypeCount: Integer;
begin
  Result := FSupportedTypes.Count;
end;//TInfoXMLTreeVisualiser.GetSupportedTypeCount
//==============================================================================
function TInfoXMLTreeVisualiser.GetVisualizerDescription: String;
begin
  Result := 'Viewer for InfoXMLTree values';
end;//TInfoXMLTreeVisualiser.GetVisualizerDescription
//==============================================================================
function TInfoXMLTreeVisualiser.GetVisualizerIdentifier: String;
begin
  Result := 'SANTools.Vis.InfoXMLTree'
end;//TInfoXMLTreeVisualiser.GetVisualizerIdentifier
//==============================================================================
function TInfoXMLTreeVisualiser.GetVisualizerName: String;
begin
  Result := 'InfoXMLTreeVisualiser';
end;//TInfoXMLTreeVisualiser.GetVisualizerName
//==============================================================================
//==============================================================================
// IOTADebuggerVisualizerExternalViewer
//==============================================================================
function TInfoXMLTreeVisualiser.GetMenuText: String;
begin
  Result := GetVisualizerName;
end;//TInfoXMLTreeVisualiser.GetMenuText
//==============================================================================
function TInfoXMLTreeVisualiser.Show(const Expression, TypeName,
  EvalResult: String; SuggestedLeft,
  SuggestedTop: Integer): IOTADebuggerVisualizerExternalViewerUpdater;
var
  AForm: TCustomForm;
  AFrame: TInfoXMLTreeViewerFrame;
  VisDockForm: INTACustomDockableForm;
begin
  VisDockForm := TInfoXMLTreeViewerForm.Create(Expression, TypeName, EvalResult) as INTACustomDockableForm;
  AForm := (BorlandIDEServices as INTAServices).CreateDockableForm(VisDockForm);
  AForm.Left := SuggestedLeft;
  AForm.Top := SuggestedTop;
  (VisDockForm as IFrameFormHelper).SetForm(AForm);
  AFrame := (VisDockForm as IFrameFormHelper).GetFrame as TInfoXMLTreeViewerFrame;
  
  AFrame.RefreshVisualizer(Expression, TypeName, EvalResult);
  
  Result := AFrame as IOTADebuggerVisualizerExternalViewerUpdater;
end;//TInfoXMLTreeVisualiser.Show
//==============================================================================
constructor TInfoXMLTreeVisualiser.Create;
var
  vFileName : string;
begin
  inherited Create;

  FSupportedTypes := TStringList.Create;

  try
    vFileName := SpecialFolderPath(CSIDL_COMMON_APPDATA) + '\SAN\Tools\Vis\' + GetVisualizerName + '.txt';

    if FileExists(vFileName) then
      FSupportedTypes.LoadFromFile(vFileName);

  except
    on E : Exception do
      MessageBox(0, PChar(E.Message), PChar(GetVisualizerName + ': ' + E.ClassName), MB_ICONSTOP);
  end;//t..e
end;
//==============================================================================
destructor TInfoXMLTreeVisualiser.Destroy;
begin
  FreeAndNil(FSupportedTypes);
  inherited;
end;
//==============================================================================


//==============================================================================
// TInfoXMLTreeViewerFrame
//==============================================================================
// private
//==============================================================================
procedure TInfoXMLTreeViewerFrame.Set_AvailableState(const Value: TAvailableState);
begin
  FAvailableState := Value;

  with stNotAvailable do  begin
    Visible := (FAvailableState <> asAvailable);

    case FAvailableState of
      asProcRunning:  Text := sProcessNotAccessible;
      asOutOfScope:   Text := sOutOfScope;
    end;//case

	DoAvailableStateChange;

    Invalidate;
  end;//with
end;//TInfoXMLTreeViewerFrame.Set_AvailableState
//==============================================================================
function TInfoXMLTreeViewerFrame.Evaluate(Expression: string): string;
var
  CurProcess: IOTAProcess;
  CurThread: IOTAThread;
  ResultStr: array[0..4095] of Char;
  CanModify: Boolean;
  ResultAddr, ResultSize, ResultVal: LongWord;
  EvalRes: TOTAEvaluateResult;
  DebugSvcs: IOTADebuggerServices;
begin
  begin
    Result := '';
    if Supports(BorlandIDEServices, IOTADebuggerServices, DebugSvcs) then
      CurProcess := DebugSvcs.CurrentProcess;
    if CurProcess <> nil then
    begin
      CurThread := CurProcess.CurrentThread;
      if CurThread <> nil then
      begin
        EvalRes := CurThread.Evaluate(Expression, @ResultStr, Length(ResultStr),
          CanModify, eseAll, '', ResultAddr, ResultSize, ResultVal, '', 0);
        case EvalRes of
          erOK: Result := ResultStr;
          erDeferred:
            begin
              FCompleted := False;
              FDeferredResult := '';
              FDeferredError := False;
              FNotifierIndex := CurThread.AddNotifier(Self);
              while not FCompleted do
                DebugSvcs.ProcessDebugEvents;
              CurThread.RemoveNotifier(FNotifierIndex);
              FNotifierIndex := -1;
              if not FDeferredError then
              begin
                if FDeferredResult <> '' then
                  Result := FDeferredResult
                else
                  Result := ResultStr;
              end;
            end;
          erBusy:
            begin
              DebugSvcs.ProcessDebugEvents;
              Result := Evaluate(Expression);
            end;
        end;
      end;
    end;
  end;
end;//TInfoXMLTreeViewerFrame.Evaluate
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TInfoXMLTreeViewerFrame.SetParent(AParent: TWinControl);
begin

  if AParent = nil then
    if Assigned(FClosedProc) then
      FClosedProc;


  inherited;
end;//TInfoXMLTreeViewerFrame.SetParent
//==============================================================================
procedure TInfoXMLTreeViewerFrame.SetForm(AForm: TCustomForm);
begin
  FOwningForm := AForm;
end;//TInfoXMLTreeViewerFrame.SetForm
//==============================================================================
procedure TInfoXMLTreeViewerFrame.DoRefreshVisualizer(const Expression, TypeName, EvalResult: String);

  function AttrNode(const AAttrNode : IXMLNode) : string;
  begin
    with AAttrNode do
      Result := NodeName + ' = ' + Text;
  end;

  procedure XMLToTree(const AXMLNodes : IXMLNodeList; const AParant : TTreeNode);
  var
    i, j : integer;
    vXMLNode : IXMLNode;
    S : string;
    vNode : TTreeNode;
  begin
    vNode := nil;

    for i := 0 to AXMLNodes.Count - 1 do
    begin
      vXMLNode := AXMLNodes[i];

      if vXMLNode.NodeType in [ntText] then
        Continue;

      if vXMLNode.IsTextElement then
        S := Format('%s=%s', [vXMLNode.NodeName, vXMLNode.Text])
      else
        S := Format('%s', [vXMLNode.NodeName]);

      if vXMLNode.AttributeNodes.Count > 0 then
      begin
        S := S + ' (' + AttrNode(vXMLNode.AttributeNodes[0]);

        for j := 1 to vXMLNode.AttributeNodes.Count - 1 do
          S := S + '; ' + AttrNode(vXMLNode.AttributeNodes[j]);

        S := S + ')';
      end;

      if Assigned(AParant) then
        vNode := tvVis.Items.AddChild(AParant, S)
      else
        vNode := tvVis.Items.Add(vNode, S);

      XMLToTree(vXMLNode.ChildNodes, vNode);
    end;

  end;

var
  vTempFN : TFileName;
begin
  vTempFN := GetEnvironmentVariable('temp') + '\InfoXMLTreeViewer.xml';
  try
    Evaluate( Format('XMLTreeSave(%s, ''%s'')', [Expression, vTempFN]) );

//    ShowMessage(vTempFN);

    xml.LoadFromFile(vTempFN);
    xml.Active := True;

    tvVis.Items.BeginUpdate;
    try
      tvVis.Items.Clear;

      XMLToTree(xml.ChildNodes, nil);

    finally
      tvVis.Items.EndUpdate;
    end;

    tvVis.FullExpand;

  finally
    DeleteFile(vTempFN)
  end;
end;//TInfoXMLTreeViewerFrame.DoRefreshVisualizer
//==============================================================================
procedure TInfoXMLTreeViewerFrame.DoAvailableStateChange;
begin
  tvVis.Items.Clear;
end;//TInfoXMLTreeViewerFrame.DoAvailableStateChange
//==============================================================================
//==============================================================================
// public
//==============================================================================
// IOTADebuggerVisualizerExternalViewerUpdater
//==============================================================================
procedure TInfoXMLTreeViewerFrame.CloseVisualizer;
begin
  if FOwningForm <> nil then
    FOwningForm.Close;
end;//TInfoXMLTreeViewerFrame.CloseVisualizer
//==============================================================================
procedure TInfoXMLTreeViewerFrame.MarkUnavailable(Reason: TOTAVisualizerUnavailableReason);
begin
  if Reason = ovurProcessRunning then
  begin
    AvailableState := asProcRunning;
  end else if Reason = ovurOutOfScope then
    AvailableState := asOutOfScope;
end;//TInfoXMLTreeViewerFrame.MarkUnavailable
//==============================================================================
procedure TInfoXMLTreeViewerFrame.RefreshVisualizer(const Expression, TypeName, EvalResult: String);
begin
  AvailableState := asAvailable;
 
  DoRefreshVisualizer(Expression, TypeName, EvalResult); 
end;//TInfoXMLTreeViewerFrame.RefreshVisualizer
//==============================================================================
procedure TInfoXMLTreeViewerFrame.SetClosedCallback(ClosedProc: TOTAVisualizerClosedProcedure);
begin
  FClosedProc := ClosedProc;
end;//TInfoXMLTreeViewerFrame.SetClosedCallback
//==============================================================================
// IOTAThreadNotifier
//==============================================================================
procedure TInfoXMLTreeViewerFrame.AfterSave;
begin
end;//TInfoXMLTreeViewerFrame.AfterSave
//==============================================================================
procedure TInfoXMLTreeViewerFrame.BeforeSave;
begin
end;//TInfoXMLTreeViewerFrame.BeforeSave
//==============================================================================
procedure TInfoXMLTreeViewerFrame.Destroyed;
begin
end;//TInfoXMLTreeViewerFrame.Destroyed
//==============================================================================
procedure TInfoXMLTreeViewerFrame.Modified;
begin
end;//TInfoXMLTreeViewerFrame.Modified
//==============================================================================
procedure TInfoXMLTreeViewerFrame.ThreadNotify(Reason: TOTANotifyReason);
begin
end;//TInfoXMLTreeViewerFrame.ThreadNotify
//==============================================================================
procedure TInfoXMLTreeViewerFrame.EvaluteComplete(const ExprStr,
  ResultStr: string; CanModify: Boolean; ResultAddress, ResultSize: LongWord;
  ReturnCode: Integer);
begin
  FCompleted := True;
  FDeferredResult := ResultStr;
  FDeferredError := ReturnCode <> 0;
end;//TInfoXMLTreeViewerFrame.EvaluteComplete
//==============================================================================
procedure TInfoXMLTreeViewerFrame.ModifyComplete(const ExprStr,
  ResultStr: string; ReturnCode: Integer);
begin
end;//TInfoXMLTreeViewerFrame.ModifyComplete
//==============================================================================
//==============================================================================
constructor TInfoXMLTreeViewerFrame.Create(AOwner: TComponent);
begin
  inherited;

  AvailableState := asAvailable;
end;//TInfoXMLTreeViewerFrame.Create
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TInfoXMLTreeViewerFrame.FrameResize(Sender: TObject);
begin
  stNotAvailable.Left  := (ClientWidth  - stNotAvailable.Width ) div 2;
  stNotAvailable.Top   := (ClientHeight - stNotAvailable.Height) div 2;
end;//TInfoXMLTreeViewerFrame.FrameResize
//==============================================================================

//==============================================================================
// TInfoXMLTreeViewerForm
//==============================================================================
// public
//==============================================================================
constructor TInfoXMLTreeViewerForm.Create(const AExpression, ATypeName, AEvalResult: string);
begin
  inherited Create;
  FExpression := AExpression;
  FTypeName   := ATypeName;
  FEvalResult := AEvalResult;
end;//TInfoXMLTreeViewerForm.Create
//==============================================================================
// INTACustomDockableForm
//==============================================================================
procedure TInfoXMLTreeViewerForm.CustomizePopupMenu(PopupMenu: TPopupMenu);
begin
  // no toolbar
end;//TInfoXMLTreeViewerForm.CustomizePopupMenu
//==============================================================================
procedure TInfoXMLTreeViewerForm.CustomizeToolBar(ToolBar: TToolBar);
begin
  // no toolbar
end;//TInfoXMLTreeViewerForm.CustomizeToolBar
//==============================================================================
function TInfoXMLTreeViewerForm.EditAction(Action: TEditAction): Boolean;
begin
  Result := False;
end;//TInfoXMLTreeViewerForm.EditAction
//==============================================================================
procedure TInfoXMLTreeViewerForm.FrameCreated(AFrame: TCustomFrame);
begin
  FMyFrame := TInfoXMLTreeViewerFrame(AFrame);
end;//TInfoXMLTreeViewerForm.FrameCreated
//==============================================================================
function TInfoXMLTreeViewerForm.GetCaption: string;
begin
  Result := Format(sFormCaption, [FExpression]);
end;//TInfoXMLTreeViewerForm.GetCaption
//==============================================================================
function TInfoXMLTreeViewerForm.GetEditState: TEditState;
begin
  Result := [];
end;//TInfoXMLTreeViewerForm.GetEditState
//==============================================================================
function TInfoXMLTreeViewerForm.GetFrameClass: TCustomFrameClass;
begin
  Result := TInfoXMLTreeViewerFrame;
end;//TInfoXMLTreeViewerForm.GetFrameClass
//==============================================================================
function TInfoXMLTreeViewerForm.GetIdentifier: string;
begin
  Result := 'InfoXMLTreeVisualizer';
end;//TInfoXMLTreeViewerForm.GetIdentifier
//==============================================================================
function TInfoXMLTreeViewerForm.GetMenuActionList: TCustomActionList;
begin
  Result := nil;
end;//TInfoXMLTreeViewerForm.GetMenuActionList
//==============================================================================
function TInfoXMLTreeViewerForm.GetMenuImageList: TCustomImageList;
begin
  Result := nil;
end;//TInfoXMLTreeViewerForm.GetMenuImageList
//==============================================================================
function TInfoXMLTreeViewerForm.GetToolbarActionList: TCustomActionList;
begin
  Result := nil;
end;//TInfoXMLTreeViewerForm.GetToolbarActionList
//==============================================================================
function TInfoXMLTreeViewerForm.GetToolbarImageList: TCustomImageList;
begin
  Result := nil;
end;//TInfoXMLTreeViewerForm.GetToolbarImageList
//==============================================================================
procedure TInfoXMLTreeViewerForm.LoadWindowState(Desktop: TCustomIniFile;
  const Section: string);
begin
  //no desktop saving
end;//TInfoXMLTreeViewerForm.LoadWindowState
//==============================================================================
procedure TInfoXMLTreeViewerForm.SaveWindowState(Desktop: TCustomIniFile;
  const Section: string; IsProject: Boolean);
begin
  //no desktop saving
end;//TInfoXMLTreeViewerForm.SaveWindowState
//==============================================================================
//==============================================================================
function TInfoXMLTreeViewerForm.GetForm: TCustomForm;
begin
  Result := FMyForm;
end;//TInfoXMLTreeViewerForm.GetForm
//==============================================================================
function TInfoXMLTreeViewerForm.GetFrame: TCustomFrame;
begin
  Result := FMyFrame;
end;//TInfoXMLTreeViewerForm.GetFrame
//==============================================================================
procedure TInfoXMLTreeViewerForm.SetForm(Form: TCustomForm);
begin
  FMyForm := Form;
  if Assigned(FMyFrame) then
    FMyFrame.SetForm(FMyForm);
end;//TInfoXMLTreeViewerForm.SetForm
//==============================================================================
procedure TInfoXMLTreeViewerForm.SetFrame(Frame: TCustomFrame);
begin
  FMyFrame := TInfoXMLTreeViewerFrame(Frame);
end;//TInfoXMLTreeViewerForm.SetFrame
//==============================================================================

//==============================================================================
procedure Register;
begin

  vInfoXMLTreeVisualiser := TInfoXMLTreeVisualiser.Create;
  (BorlandIDEServices as IOTADebuggerServices).RegisterDebugVisualizer(vInfoXMLTreeVisualiser);

end;//Register
//==============================================================================
procedure RemoveVisualizers;
var
  DebuggerServices: IOTADebuggerServices;
begin
  if Supports(BorlandIDEServices, IOTADebuggerServices, DebuggerServices) then  begin
    DebuggerServices.UnregisterDebugVisualizer(vInfoXMLTreeVisualiser);
    vInfoXMLTreeVisualiser := nil;
  end;//if
end;//RemoveVisualizers
//==============================================================================


INITIALIZATION//////////////////////////////////////////////////////////////////
FINALIZATION////////////////////////////////////////////////////////////////////
  RemoveVisualizers;

END.
