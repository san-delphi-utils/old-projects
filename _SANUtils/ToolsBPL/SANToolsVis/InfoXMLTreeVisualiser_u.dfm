object InfoXMLTreeViewerFrame: TInfoXMLTreeViewerFrame
  Left = 0
  Top = 0
  Width = 544
  Height = 391
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  OnResize = FrameResize
  object tvVis: TTreeView
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 538
    Height = 385
    Align = alClient
    Indent = 19
    TabOrder = 1
  end
  object stNotAvailable: TStaticText
    Left = 224
    Top = 184
    Width = 73
    Height = 17
    Caption = 'stNotAvailable'
    Color = clWindow
    ParentColor = False
    TabOrder = 0
    Transparent = False
  end
  object xml: TXMLDocument
    Left = 40
    Top = 24
    DOMVendorDesc = 'MSXML'
  end
end
