object frmSANGUIDDialog: TfrmSANGUIDDialog
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'frmSANGUIDDialog'
  ClientHeight = 94
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Padding.Left = 8
  Padding.Top = 8
  Padding.Right = 8
  Padding.Bottom = 8
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 16
  object pnlButtons: TPanel
    Left = 8
    Top = 53
    Width = 408
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'pnlButtons'
    Padding.Top = 8
    ShowCaption = False
    TabOrder = 1
    object btnCancel: TBitBtn
      Left = 333
      Top = 8
      Width = 75
      Height = 25
      Align = alRight
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      DoubleBuffered = True
      ModalResult = 2
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 0
    end
    object btnOK: TBitBtn
      AlignWithMargins = True
      Left = 250
      Top = 8
      Width = 75
      Height = 25
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Align = alRight
      Caption = #1054#1050
      Default = True
      DoubleBuffered = True
      ModalResult = 1
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 1
    end
  end
  object pnlCustomData: TPanel
    AlignWithMargins = True
    Left = 11
    Top = 11
    Width = 402
    Height = 39
    Align = alClient
    Caption = 'pnlCustomData'
    TabOrder = 0
    DesignSize = (
      402
      39)
    object lblGUID: TLabel
      Left = 8
      Top = 11
      Width = 33
      Height = 16
      Caption = 'GUID:'
    end
    object sbtnGUID: TSpeedButton
      Left = 353
      Top = 8
      Width = 25
      Height = 24
      Hint = #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
      Anchors = [akTop, akRight]
      Glyph.Data = {
        B6040000424DB604000000000000360000002800000018000000100000000100
        1800000000008004000000000000000000000000000000000000FF00FFFF00FF
        FF00FFB8947AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFFFFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFE5BB9C8B542CC4B5AAFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF7F7F7FC3C3C3FFFFFFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFF3DFD0D4864BAC673391623F825C3F7A583E85
        6C59B5A69BFF00FFFF00FFFF00FFFF00FF7F7F7FC3C3C3C3C3C3C3C3C3FFFFFF
        FFFFFFFFFFFFFFFFFFFF00FFFF00FFFF00FFFF00FFD7996AD58040DA8441D782
        40D88341D78240BF7339895229836752FF00FFFF00FFFF00FF7F7F7FC3C3C3C3
        C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3FFFFFFFF00FFFF00FFEDB990E59050
        E48B48E38945E48B48E89E66EEBA92F0C4A2EFBA93E695589E6C48E3DBD57F7F
        7FC3C3C3C3C3C3C3C3C3C3C3C3C3C3C37F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFF
        FFFFF5DDCAF1C6A4EDB489E89E66DF8743E0B492FF00FFFF00FFFF00FFFF00FF
        EAAA7ABF84597F7F7F7F7F7FC3C3C3C3C3C3C3C3C37F7F7FFF00FFFF00FFFF00
        FFFF00FF7F7F7FFFFFFFFF00FFFF00FFF6E0D0F2C9AAEAA875BA7F53FF00FFFF
        00FFFF00FFFF00FFFF00FFECB387FF00FFFF00FF7F7F7F7F7F7FC3C3C37F7F7F
        FF00FFFF00FFFF00FFFF00FFFF00FF7F7F7FFF00FFFF00FFFF00FFFF00FFF5DD
        CBECB68CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF7F7F7F7F7F7FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFD0AC91D3C8C0FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7F7F7FFFFFFFFF00FFFF00FFFF
        00FFDAA782BEA28FFF00FFFF00FFFF00FFFF00FFFF00FFDD88477F5739A49284
        CFC4BCFF00FF7F7F7FFFFFFFFF00FFFF00FFFF00FFFF00FFFF00FF7F7F7FC3C3
        C3FFFFFFFFFFFFFF00FFF5DCC9E59253A07D63C8BEB7FF00FFFF00FFFF00FFE6
        975BC4753B985B2D7A5436A28B797F7F7FC3C3C3FFFFFFFFFFFFFF00FFFF00FF
        FF00FF7F7F7FC3C3C3C3C3C3C3C3C3FFFFFFFF00FFF4D9C5E89E659F60307653
        39795B44AB7852CD7E44E28844DD8542CA793C9A6239FF00FF7F7F7FC3C3C3C3
        C3C3FFFFFFFFFFFFFFFFFFC3C3C3C3C3C3C3C3C3C3C3C3FFFFFFFF00FFFF00FF
        F6E0D0F0BF9AEAA570E59150E48D4AE48E4BE48E4CE48C49E48E4DE0B99BFF00
        FFFF00FF7F7F7F7F7F7FC3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3FF
        FFFFFF00FFFF00FFFF00FFFF00FFF5D8C3F1C9AAEFC19DF0C09DEBAD7DE59353
        DEA478FF00FFFF00FFFF00FFFF00FFFF00FF7F7F7F7F7F7F7F7F7F7F7F7FC3C3
        C3C3C3C3FFFFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFF0C7A8E79C62F3E1D5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF7F7F7FC3C3C3FFFFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFF4D7C1F1CEB2FF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF7F7F7F7F7F7FFF00FFFF00FF}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = sbtnGUIDClick
      ExplicitLeft = 488
    end
    object shpGUID: TShape
      Left = 385
      Top = 17
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
      ExplicitLeft = 520
    end
    object medtGUID: TMaskEdit
      Left = 47
      Top = 8
      Width = 305
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      EditMask = '{AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA};1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 38
      ParentFont = False
      TabOrder = 0
      Text = '{00000000-0000-0000-0000-000000000000}'
      Visible = False
      OnChange = edtGUIDChange
      OnKeyPress = edtGUIDKeyPress
    end
    object edtGUID: TJvMaskEdit
      Left = 47
      Top = 8
      Width = 332
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      EditMask = '{AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA};1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 38
      ParentFont = False
      ShowButton = True
      TabOrder = 1
      Text = '{00000000-0000-0000-0000-000000000000}'
      OnChange = edtGUIDChange
      OnKeyPress = edtGUIDKeyPress
    end
  end
end
