{*******************************************************}
{                                                       }
{       SANGUIDDialog                                   }
{       Диалог редактирования GUID.                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANGUIDDialog;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, StdCtrls, ComObj, ActiveX, Mask, JvExMask,
  JvToolEdit, JvMaskEdit;

TYPE
  TfrmSANGUIDDialog = class(TForm)
    lblGUID: TLabel;
    sbtnGUID: TSpeedButton;
    shpGUID: TShape;
    medtGUID: TMaskEdit;
    pnlButtons: TPanel;
    btnCancel: TBitBtn;
    btnOK: TBitBtn;
    pnlCustomData: TPanel;
    edtGUID: TJvMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);

    procedure sbtnGUIDClick(Sender: TObject);
    procedure edtGUIDChange(Sender: TObject);
    procedure edtGUIDKeyPress(Sender: TObject; var Key: Char);

  private
    FFlagGUID : boolean;

    function GetGUID: string;
    procedure SetGUID(const Value: string);

  protected
    procedure SetWarnShape(const AShape : TShape; var AFlag : boolean;
      const AValidateCondition : boolean; const AWarningHint : string);

  public
    property GUID : string  read GetGUID  write SetGUID;

  end;

function SANGUIDDLG(const ACaption : TCaption; var AGUIDStr : WideString) : boolean;

IMPLEMENTATION

resourcestring
  sIncorrectGUIDWarningMessage = 'Введен не корректный GUID';

{$R *.dfm}

type
  TJvMaskEditHack = class(TJvMaskEdit);

function SANGUIDDLG(const ACaption : TCaption; var AGUIDStr : WideString) : boolean;
begin
  with TfrmSANGUIDDialog.Create(nil) do
  try
    Caption := ACaption;

    GUID := AGUIDStr;

    Result := (ShowModal = mrOk);

    if Result then
      AGUIDStr := GUID;

  finally
    Free
  end;
end;

//==============================================================================
// TfrmSANGUIDDialog
// private
function TfrmSANGUIDDialog.GetGUID: string;
begin
  Result := edtGUID.Text;
end;

procedure TfrmSANGUIDDialog.SetGUID(const Value: string);
begin
  edtGUID.Text := Value;
end;

// protected
procedure TfrmSANGUIDDialog.SetWarnShape(const AShape: TShape; var AFlag: boolean;
  const AValidateCondition: boolean; const AWarningHint: string);
begin
  AFlag := AValidateCondition;

  with AShape do
    if AValidateCondition then
      begin
        Brush.Color := clLime;
        Hint := '';
      end
    else
      begin
        Brush.Color := clRed;
        Hint := AWarningHint;
      end;

  btnOK.Enabled := FFlagGUID;
end;

// published
procedure TfrmSANGUIDDialog.FormCreate(Sender: TObject);
begin
  FFlagGUID := True;

  with TJvMaskEditHack(edtGUID) do
  begin
    Button.Glyph.Assign(sbtnGUID.Glyph);
    Button.NumGlyphs := sbtnGUID.NumGlyphs;
    OnButtonClick := sbtnGUIDClick;
  end;

end;

procedure TfrmSANGUIDDialog.FormResize(Sender: TObject);
begin
  with TJvMaskEditHack(edtGUID) do
    Button.Width := Button.Height;
end;

procedure TfrmSANGUIDDialog.sbtnGUIDClick(Sender: TObject);
var
  vGUID : TGUID;
begin
  OleCheck(CoCreateGuid(vGUID));

  GUID := GUIDToString(vGUID);
end;

procedure TfrmSANGUIDDialog.edtGUIDChange(Sender: TObject);
var
  vGUID : TGUID;
  vHR : HRESULT;
begin
  vHR := CLSIDFromString
  (
    PWideChar
    (
      WideString
      (
        Trim
        (
          GUID
        )
      )
    ),
    vGUID
  );

  SetWarnShape(shpGUID, FFlagGUID, vHR = S_OK, LoadResString(@sIncorrectGUIDWarningMessage));
end;

procedure TfrmSANGUIDDialog.edtGUIDKeyPress(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', 'A'..'F', 'a'..'f', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)]) then
  begin
    Key := #0;
    MessageBeep(MB_OK);
  end;
end;
//==============================================================================

END.
