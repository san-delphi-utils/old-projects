{*******************************************************}
{                                                       }
{       SANUtilsExtEditors                              }
{       Внешние редакторы.                              }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANUtilsExtEditors;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, Variants, Forms,
//  Dialogs,
  SANPkgAsDllClient,
  SANExtEditorsIntf;

TYPE
  TSANGUIDExtEditor = class(TInterfacedObject, ISANExtEditor)
  protected
    // ISANExtEditor
    function Get_Name : WideString; safecall;
    function Execute(const ACaption : WideString; var AValue : OleVariant) : WordBool; safecall;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

  end;

IMPLEMENTATION

uses
  ComObj,
  SANGUIDDialog;

const
  GUID_EXT_EDITOR : WideString = 'GUIDEditor';

//==============================================================================
// TSANGUIDExtEditor
// protected
function TSANGUIDExtEditor.Get_Name: WideString;
begin
  Result := GUID_EXT_EDITOR;
end;

function TSANGUIDExtEditor.Execute(const ACaption: WideString;
  var AValue: OleVariant): WordBool;
var
  S : WideString;
begin
  S := VarToStr(AValue);

  Result := SANGUIDDLG(ACaption, S);

  if Result then
    AValue := WideString(S);
end;

// public
function TSANGUIDExtEditor.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{283480F4-E916-4478-A720-9DD955A90DA6}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
type
  /// <summary>
  /// Класс точки входа
  /// </summary>
  TSANEEDEntry = class(TSANPkgAsDllEntry)
  protected
    procedure DoInit(const AParams: IInterface); override;
    procedure DoDone(const AParams: IInterface); override;

  end;

// protected
procedure TSANEEDEntry.DoInit(const AParams: IInterface);
var
  vEdtsMan : ISANExtEditorsManager;
begin

  if Supports(AParams, ISANExtEditorsManager, vEdtsMan) then
  begin
    Application.Handle := vEdtsMan.AppHandle;

    vEdtsMan.RegEditor(TSANGUIDExtEditor.Create);
  end;

end;

procedure TSANEEDEntry.DoDone(const AParams: IInterface);
var
  vEdtsMan : ISANExtEditorsManager;
begin

  if Supports(AParams, ISANExtEditorsManager, vEdtsMan) then
  begin
    vEdtsMan.UnRegEditor(GUID_EXT_EDITOR);

  end;

end;
//==============================================================================

exports
  GetPkgAsDllEntryPoint name EXT_EDITORS_ENTRY_POINT_NAME;

INITIALIZATION
  PkgAsDllEntryClass := TSANEEDEntry;
END.
