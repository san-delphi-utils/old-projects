{*******************************************************}
{                                                       }
{       SANRTTIUtils                                    }
{       Работа с RTTI                                   }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANRTTIUtils;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes, TypInfo,
  SANMisc;

TYPE
  /// <summary>
  /// Динамический массив объектов
  /// </summary>
  TObjects = array of TObject;

  /// <summary>Тип: Процедура перебора published-свойств класса</summary>
  /// <param name="AClass">Класс, для которого производится перебор.</param>
  /// <param name="APropInfo">Информация о свойстве.</param>
  /// <param name="AIndex">Индекс перебора.</param>
  /// <param name="AParam">Пользовательский параметр.</param>
  TSANRTTIEnumClassPropsProc = procedure (const AClass : TClass;
    const APropInfo : PPropInfo; const AIndex : integer; const AParam : Pointer);

  /// <summary>Тип: Процедура перебора published-свойств объекта</summary>
  /// <param name="AObject">Объект, для которого производится перебор.</param>
  /// <param name="APropInfo">Информация о свойстве.</param>
  /// <param name="AIndex">Индекс перебора.</param>
  /// <param name="AParam">Пользовательский параметр.</param>
  TSANRTTIEnumObjectPropsProc = procedure (const AObject : TObject;
    const APropInfo : PPropInfo; const AIndex : integer; const AParam : Pointer);

  /// <summary>Тип: Процедура перебора published-методов класса</summary>
  /// <param name="AClass">Класс, для которого производится перебор.</param>
  /// <param name="AMethodName">Имя очередного метода.</param>
  /// <param name="AMethodAddress">Указатель на очередной метод.</param>
  /// <param name="AIndex">Индекс перебора.</param>
  /// <param name="AParam">Пользовательский параметр.</param>
  TSANRTTIEnumClassMethodsProc = procedure(const AClass : TClass;
    const AMethodName : string; const AMethodAddress : Pointer;
    const AIndex : integer; const AParam : Pointer);

//------------------------------------------------------------------------------

/// <summary>Перебор published-свойств класса AClass процедурой AEnumProc c параметром AParam</summary>
procedure SANRTTIEnumProps(const AClass : TClass; const AEnumProc : TSANRTTIEnumClassPropsProc;
  const AParam : Pointer); overload;
/// <summary>Перебор published-свойств объекта AObject процедурой AEnumProc c параметром AParam</summary>
procedure SANRTTIEnumProps(const AObject : TObject; const AEnumProc : TSANRTTIEnumObjectPropsProc;
  const AParam : Pointer); overload;
/// <summary>Перебор published-методов класса AClass процедурой AEnumProc c параметром AParam</summary>
procedure SANRTTIEnumMethods(const AClass : TClass; const AEnumProc : TSANRTTIEnumClassMethodsProc;
  const AParam : Pointer);

//------------------------------------------------------------------------------

/// <summary>
/// Задать всем объектам открытого массива AObjectArr значение published-свойства с именем APropName
/// </summary>
procedure SANRTTISetProps(const APropName : string; const AValue : integer;
  const AObjectArr : array of TObject); overload;

/// <summary>
/// Задать всем объектам открытого массива AObjectArr значение published-свойства с именем APropName
/// </summary>
procedure SANRTTISetProps(const APropName : string; const AValue : boolean;
  const AObjectArr : array of TObject); overload;


IMPLEMENTATION


procedure SANRTTIEnumProps(const AClass : TClass; const AEnumProc : TSANRTTIEnumClassPropsProc;
  const AParam : Pointer); overload;
// Обработать все published-свойства процедурой AEnumProc c параметром AParam
var
  i, vCount  : integer;
  vTypeData  : PTypeData;
  vList      : PPropList;
begin
  vTypeData := GetTypeData(AClass.ClassInfo);
  vCount := vTypeData.PropCount;

  if vCount <= 0 then
    Exit;

  GetMem(vList, SizeOf(PPropInfo)*vCount);
  try
    GetPropInfos(AClass.ClassInfo, vList);

    for i := 0 to vCount - 1 do
      AEnumProc(AClass, vList[i], i, AParam);

  finally
    FreeMem(vList, SizeOf(PPropInfo)*vCount);
  end;//t..f

end;

procedure SANRTTIEnumProps(const AObject : TObject; const AEnumProc : TSANRTTIEnumObjectPropsProc;
  const AParam : Pointer); overload;

var
  i, vCount  : integer;
  vTypeData  : PTypeData;
  vList      : PPropList;
begin
  vTypeData := GetTypeData(AObject.ClassInfo);
  vCount := vTypeData.PropCount;

  if vCount <= 0 then
    Exit;

  GetMem(vList, SizeOf(PPropInfo)*vCount);
  try
    GetPropInfos(AObject.ClassInfo, vList);

    for i := 0 to vCount - 1 do
      AEnumProc(AObject, vList[i], i, AParam);

  finally
    FreeMem(vList, SizeOf(PPropInfo)*vCount);
  end;//t..f

end;

procedure SANRTTIEnumMethods(const AClass : TClass; const AEnumProc : TSANRTTIEnumClassMethodsProc;
  const AParam : Pointer);
type
  // Тип для просмотра published методов объекта
  TMethodtableEntry = packed record
    len: Word;
    adr: Pointer;
    name: ShortString;
  end;//TMethodtableEntry
  {Note: name occupies only the size required, so it is not a true shortstring! The actual
  entry size is variable, so the method table is not an array of TMethodTableEntry!}
var
  pp: ^Pointer;
  pMethodTable: Pointer;
  pMethodEntry: ^TMethodTableEntry;
  i, numEntries: Word;
begin
  pp := Pointer(Integer(AClass) + vmtMethodtable);
  pMethodTable := pp^;

  if pMethodtable <> nil then  begin
    {first word of the method table contains the number of entries}
    numEntries := PWord(pMethodTable)^;

    {make pointer to first method entry, it starts at the second word of the table}
    pMethodEntry := Pointer(Integer(pMethodTable) + 2);
    for i := 1 to numEntries do  begin

      with pMethodEntry^ do
        AEnumProc(AClass, String(name), adr, i - 1, AParam);

      {make pointer to next method entry}
      pMethodEntry := Pointer(Integer(pMethodEntry) + pMethodEntry^.len);
    end;//for
  end;//if
end;

procedure SetOrdPropEnumProc(const AObject : TObject; var AParam : Pointer);
var
  vPropInfo : PPropInfo;
begin
  vPropInfo := GetPropInfo(AObject, PStringItem(AParam)^.FString);

  if Assigned(vPropInfo) then
    SetOrdProp(AObject, vPropInfo, Integer( PStringItem(AParam)^.FObject ) );
end;

procedure SANRTTISetProps(const APropName : string; const AValue : integer;
  const AObjectArr : array of TObject);
var
  vNameValue : TStringItem;
begin

  with vNameValue do
  begin
    FString := APropName;
    FObject := TObject(AValue);
  end;

  EnumObjects(SetOrdPropEnumProc, @vNameValue, AObjectArr);
end;

procedure SANRTTISetProps(const APropName : string; const AValue : boolean;
  const AObjectArr : array of TObject);
begin
  SANRTTISetProps(APropName, Ord(AValue), AObjectArr);
end;

END.
