{*******************************************************}
{                                                       }
{       SANVCLMisc                                      }
{       Набор разнообразных функций работы с VCL        }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANVCLMisc;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, Types, TypInfo, StrUtils,
  Controls, Forms,
  SANMisc, SANRTTIUtils;

/// <summary>
/// Перевод приложения на передний план.
/// </summary>
procedure AppGoToForeground;

/// <summary>
/// Обойти каскадом все контролы, принадлежащие ATopControl, и задать значение Enabled.
/// </summary>
/// <param name="AEnabled">
/// Новое значение свойства Enabled.
/// </param>
/// <param name="ATopControl">
/// Контрол, с которого начинается каскдный обход.
/// </param>
/// <param name="ADepth">
/// Глубина рекурсивного обхода. <0 - не ограничена; =0 - только ATopControl; >0 - кол-во уровней иерархии.
/// </param>
/// <param name="AIgnoreUpdatedActions">
/// При True, контролы с назначеными Action-ами у которых задан OnUpdate не будут обработаны.
/// </param>
procedure EnabledCascade(const AEnabled : boolean; const ATopControl : TControl;
  const ADepth : integer = -1;
  const AIgnoreUpdatedActions : boolean = True);

/// <summary>
/// Для массива компонентов задать св-во Visible.
/// </summary>
/// <param name="AVisible">
/// Новое значение свойства Visible.
/// </param>
/// <param name="AComponentsArr">
/// Массив обрабатоваемых компонентов.
/// </param>
procedure VisibleComponentsArray(const AVisible : boolean; const ACompanentsArr : array of TComponent);

/// <summary>
/// Для массива компонентов задать св-во Enabled.
/// </summary>
/// <param name="AEnabled">
/// Новое значение свойства Enabled.
/// </param>
/// <param name="AComponentsArr">
/// Массив обрабатоваемых компонентов.
/// </param>
procedure EnabledComponentsArray(const AEnabled : boolean; const ACompanentsArr : array of TComponent);

IMPLEMENTATION

procedure AppGoToForeground;
// Спасибо Антону Григорьеву за эту функцию
var
  Info: TAnimationInfo;
  Animation: Boolean;
begin
  // Проверяем, включена ли анимация для окон
  Info.cbSize := SizeOf(TAnimationInfo);
  Animation := SystemParametersInfo(SPI_GETANIMATION, SizeOf(Info), @Info, 0) and
    (Info.iMinAnimate <> 0);
  // Если включена, отключаем, чтобы не было ненужного мерцания
  if Animation then
  begin
    Info.iMinAnimate := 0;
    SystemParametersInfo(SPI_SETANIMATION, SizeOf(Info), @Info, 0);
  end;
  // Если приложение не минимизировано, минимизируем
  if not IsIconic(Application.Handle) then
    Application.Minimize;
  // Восстанавливаем приложение. При этом оно автоматически выводится
  // на передний план
  Application.Restore;
  // Если анимация окон была включена, снова включаем её
  if Animation then
  begin
    Info.iMinAnimate := 1;
    SystemParametersInfo(SPI_SETANIMATION, SizeOf(Info), @Info, 0);
  end;
end;

procedure EnabledCascade(const AEnabled : boolean; const ATopControl : TControl;
  const ADepth : integer = -1;
  const AIgnoreUpdatedActions : boolean = True);

  procedure RecursionWork(const AControl : TControl; const ADepthLevel : integer);
  var
    i : integer;
  begin

    if AIgnoreUpdatedActions
    or not (Assigned(AControl.Action) and Assigned(AControl.Action.OnUpdate))
    then
      AControl.Enabled := AEnabled;

    if (AControl is TWinControl) and NOA(ADepth >= 0, ADepth < ADepthLevel) then
      with AControl as TWinControl do
        for i := 0 to ControlCount - 1 do
          RecursionWork(Controls[i], ADepthLevel + 1);
  end;

begin
  RecursionWork(ATopControl, 0);
end;

function ComponentsToObjectsArray(const ACompanentsArr : array of TComponent) : TObjects;
var
  L, i : integer;
begin
  L := Length(ACompanentsArr);
  SetLength(Result, L);

  for i := 0 to L - 1 do
    Result[i] := ACompanentsArr[i];
end;

procedure VisibleComponentsArray(const AVisible : boolean; const ACompanentsArr : array of TComponent);
begin
  SANRTTISetProps('Visible', AVisible, ComponentsToObjectsArray(ACompanentsArr));
end;

procedure EnabledComponentsArray(const AEnabled : boolean; const ACompanentsArr : array of TComponent);
begin
  SANRTTISetProps('Enabled', AEnabled, ComponentsToObjectsArray(ACompanentsArr));
end;


END.
