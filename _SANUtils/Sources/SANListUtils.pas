{*******************************************************}
{                                                       }
{       SANListUtils                                    }
{       Работа со списками                              }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANListUtils;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes;

type
  /// <summary>
  ///   Параметры поиска по списку
  /// </summary>
  TSANListParams = record
    /// <summary>
    ///   Параметр поиска.
    /// </summary>
    FindParam : Pointer;

    /// <summary>
    ///   Направление поиска. (Ум.: FromBeginning)
    /// </summary>
    Direction : TList.TDirection;

    /// <summary>
    ///   Первый проверяемый индекс. (Ум.: -1)
    /// </summary>
    FirstIndex : integer;

    /// <summary>
    ///   Указатель на Integer-переменную, получающую индекс найденного элемента в случаи успеха. (Ум.: nil)
    /// </summary>
    ResIndexPtr : PInteger;

    /// <summary>
    ///   Элемент, исключаемый при поиске. (Ум.: nil)
    /// </summary>
    ExcludeItem : Pointer;
  end;

  /// <summary>Тип: Функция поиска, используется для поиска в списках.</summary>
  /// <param name="AItem">Проверяемый элемент списка.</param>
  /// <param name="AValue">Значение с которым сравнивается параметр.</param>
  /// <returns>Boolean. True, если AItem "подходит" для поиска по AValue.</returns>
  TSANListFindFunc = function (const AItem, AValue : Pointer) : boolean;

  /// <summary>Тип: Метод поиска, используется для поиска в списках.</summary>
  /// <param name="AItem">Проверяемый элемент списка.</param>
  /// <param name="AValue">Значение с которым сравнивается параметр.</param>
  /// <returns>Boolean. True, если AItem "подходит" для поиска по AValue.</returns>
  TSANListFindObjFunc = function (const AItem, AValue : Pointer) : boolean of object;

  /// <summary>Тип: Процедура очистки указателя.</summary>
  /// <param name="APtr">Очищаемый указатель.</param>
  TSANClearProc = procedure (const APtr : Pointer);
  /// <summary>Тип: Метод очистки указателя.</summary>
  /// <param name="APtr">Очищаемый указатель.</param>
  TSANClearObjProc = procedure (const APtr : Pointer) of object;

  /// <summary>
  /// Список владеющий указателями, отвечающий за их разрушение
  /// </summary>
  TSANPtrOwnerList = class(TList)
  private
    FClearProc : TSANClearProc;

  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;

  public
    constructor Create(const AClearProc : TSANClearProc = nil);
  end;

//------------------------------------------------------------------------------
// Набор функций задания параметров
//------------------------------------------------------------------------------
/// <summary>
///   Задание параметров поиска
/// </summary>
function SANListParams(const AFindParam : Pointer) : TSANListParams; overload;

/// <summary>
///   Задание параметров поиска
/// </summary>
function SANListParams(const AFindParam : Pointer; const AFirstIndex : integer) : TSANListParams; overload;

/// <summary>
///   Задание параметров поиска
/// </summary>
function SANListParams(const AFindParam : Pointer; const AFirstIndex : integer;
  const AResIndexPtr : PInteger) : TSANListParams; overload;

/// <summary>
///   Задание параметров поиска
/// </summary>
function SANListParams(const AFindParam : Pointer; const AFirstIndex : integer;
  const AResIndexPtr : PInteger; const ADirection : TList.TDirection) : TSANListParams; overload;

/// <summary>
///   Задание параметров поиска
/// </summary>
function SANListParams(const AFindParam : Pointer; const AFirstIndex : integer;
  const AResIndexPtr : PInteger; const ADirection : TList.TDirection;
  const AExcludeItem : Pointer) : TSANListParams; overload;
//------------------------------------------------------------------------------

/// <summary>Функция поиска элемента в списке TList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска элемента в списке TList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска элемента в списке TThreadList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска элемента в списке TThreadList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска элемента в списке IInterfaceList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска элемента в списке IInterfaceList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска элемента в списке TList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска элемента в списке TList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска элемента в списке TThreadList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска элемента в списке TThreadList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска элемента в списке IInterfaceList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска элемента в списке IInterfaceList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListFind(var AResultItem; const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке TList.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке TList.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке TThreadList.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке TThreadList.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке IInterfaceList.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке IInterfaceList.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке TList.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке TList.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке TThreadList.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке TThreadList.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке IInterfaceList.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция проверки существования элемента в списке IInterfaceList.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListExists(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке TList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке TList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке TThreadList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке TThreadList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке IInterfaceList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке IInterfaceList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке TList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке TList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке TThreadList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке TThreadList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке IInterfaceList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и извлечения элемента в списке IInterfaceList.</summary>
/// <param name="AResultItem">Если поиск успешен, содержит ссылку на найденый элемент.</param>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если поиск успешен.</returns>
function SANListExtract(var AResultItem; const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>
/// Процедура очистки. Рассматривает APtr, как объект и вызывает для него Free.
/// </summary>
/// <param name="APtr">Объект</param>
procedure SANListCP_FreeObject(const APtr : Pointer);

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList. Элемент дополнительно обрабатывается процедурой AClearProc.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearProc">Процедура очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList. Элемент дополнительно обрабатывается методом AClearMethod.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AClearMethod">Метод очистки.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке IInterfaceList.</summary>
/// <param name="AInterfaceList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент считается объектом, для него вызывается Free.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemoveObj(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент считается объектом, для него вызывается Free.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemoveObj(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент считается объектом, для него вызывается Free.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemoveObj(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент считается объектом, для него вызывается Free.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindFunc">Функция поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemoveObj(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент считается объектом, для него вызывается Free.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemoveObj(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TList. Элемент считается объектом, для него вызывается Free.</summary>
/// <param name="AList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemoveObj(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент считается объектом, для него вызывается Free.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParams">Параметры поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemoveObj(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean; overload;

/// <summary>Функция поиска и удаления элемента в списке TThreadList. Элемент считается объектом, для него вызывается Free.</summary>
/// <param name="AThreadList">Список, по которому идет поиск.</param>
/// <param name="AFindObjFunc">Метод поиска.</param>
/// <param name="AFindParam">Параметр поиска.</param>
/// <returns>Boolean. True, если элемент существует.</returns>
function SANListRemoveObj(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean; overload;

/// <summary>
/// Для списка TList вызвать процедуру AClearProc для каждого элемента,
/// затем очистить список
/// </summary>
/// <param name="AList">Список TList.</param>
/// <param name="AClearProc">Процедура очистки.</param>
procedure SANListClear(const AList : TList; const AClearProc : TSANClearProc); overload;

/// <summary>
/// Для списка TList вызвать процедуру AClearMethod для каждого элемента,
/// затем очистить список
/// </summary>
/// <param name="AList">Список TList.</param>
/// <param name="AClearMethod">Метод очистки.</param>
procedure SANListClear(const AList : TList; const AClearMethod : TSANClearObjProc); overload;

/// <summary>
/// Для списка TThreadList вызвать процедуру AClearProc для каждого элемента,
/// затем очистить список
/// </summary>
/// <param name="AThList">Список TThreadList.</param>
/// <param name="AClearProc">Процедура очистки.</param>
procedure SANListClear(const AThList : TThreadList; const AClearProc : TSANClearProc); overload;

/// <summary>
/// Для списка TThreadList вызвать процедуру AClearMethod для каждого элемента,
/// затем очистить список
/// </summary>
/// <param name="AThList">Список TThreadList.</param>
/// <param name="AClearMethod">Метод очистки.</param>
procedure SANListClear(const AThList : TThreadList; const AClearMethod : TSANClearObjProc); overload;

/// <summary>
/// Для списка IInterfaceList вызвать процедуру AClearProc для каждого элемента,
/// затем очистить список
/// </summary>
/// <param name="AInterfaceList">Список IInterfaceList.</param>
/// <param name="AClearProc">Процедура очистки.</param>
procedure SANListClear(const AInterfaceList : IInterfaceList; const AClearProc : TSANClearProc); overload;

/// <summary>
/// Для списка IInterfaceList вызвать процедуру AClearMethod для каждого элемента,
/// затем очистить список
/// </summary>
/// <param name="AInterfaceList">Список IInterfaceList.</param>
/// <param name="AClearMethod">Метод очистки.</param>
procedure SANListClear(const AInterfaceList : IInterfaceList; const AClearMethod : TSANClearObjProc); overload;

/// <summary>Извлеч из списка TThreadList элемент AItem.</summary>
/// <param name="AThList">Список TThreadList.</param>
/// <param name="AList">Извлекаемый элемент.</param>
/// <returns>Boolean. True, элемент извлечен успешено.</returns>
function SANThreadListExtract(const AThList : TThreadList; const AItem : Pointer) : boolean;

/// <summary>Для списка TList, содержащего объекты вызвать Free для каждого объекта.</summary>
/// <param name="AList">Список TList.</param>
procedure SANListClearObjects(const AList : TList); overload;

/// <summary>Для списка TThreadList, содержащего объекты вызвать Free для каждого объекта.</summary>
/// <param name="AThList">Список TThreadList.</param>
procedure SANListClearObjects(const AThList : TThreadList); overload;

//==============================================================================
IMPLEMENTATION
//==============================================================================

//==============================================================================
function SANListParams(const AFindParam : Pointer) : TSANListParams; overload;
begin
  Result := SANListParams(AFindParam, -1, nil, FromBeginning, nil)
end;

function SANListParams(const AFindParam : Pointer; const AFirstIndex : integer) : TSANListParams; overload;
begin
  Result := SANListParams(AFindParam, AFirstIndex, nil, FromBeginning, nil)
end;

function SANListParams(const AFindParam : Pointer; const AFirstIndex : integer;
  const AResIndexPtr : PInteger) : TSANListParams; overload;
begin
  Result := SANListParams(AFindParam, AFirstIndex, AResIndexPtr, FromBeginning, nil)
end;

function SANListParams(const AFindParam : Pointer; const AFirstIndex : integer;
  const AResIndexPtr : PInteger; const ADirection : TList.TDirection) : TSANListParams; overload;
begin
  Result := SANListParams(AFindParam, AFirstIndex, AResIndexPtr, ADirection)
end;

function SANListParams(const AFindParam : Pointer; const AFirstIndex : integer;
  const AResIndexPtr : PInteger; const ADirection : TList.TDirection;
  const AExcludeItem : Pointer) : TSANListParams; overload;
begin

  with Result do
  begin
    FindParam    := AFindParam;
    Direction    := ADirection;
    FirstIndex   := AFirstIndex;
    ResIndexPtr  := AResIndexPtr;
    ExcludeItem  := AExcludeItem;
  end;

end;
//==============================================================================

//==============================================================================
function Method(const ACode : Pointer; const AData : Pointer) : TMethod; overload;
begin
  Result.Code := ACode;
  Result.Data := AData;
end;

function Method(const AFindFunc : TSANListFindFunc) : TMethod; overload;
begin
  Result := Method(@AFindFunc, nil);
end;

function Method(const AFindObjFunc : TSANListFindObjFunc) : TMethod; overload;
begin
  Result := TMethod(AFindObjFunc);
end;

function Method(const AClearProc : TSANClearProc) : TMethod; overload;
begin
  Result := Method(@AClearProc, nil);
end;

function Method(const AClearObjProc : TSANClearObjProc) : TMethod; overload;
begin
  Result := TMethod(AClearObjProc);
end;
//==============================================================================

//==============================================================================
type
  TGetItemFunc = function(const AList : Pointer; const AIndex : integer) : Pointer;

function GITList(const AList : Pointer; const AIndex : integer) : Pointer; {$IFDEF SAN_LIST_UTILS_INLINE}inline;{$ENDIF}
begin
  Result := TList(AList)[AIndex]
end;

function GIIIntfList(const AList : Pointer; const AIndex : integer) : Pointer; {$IFDEF SAN_LIST_UTILS_INLINE}inline;{$ENDIF}
begin
  Result := Pointer( IInterfaceList(AList)[AIndex] );
end;
//==============================================================================

//==============================================================================
type
  TExtractItemProc = procedure(const AList, AItem : Pointer; const AIndex : integer);

procedure EIList(const AList, AItem : Pointer; const AIndex : integer);
begin
  TList(AList).Extract(AItem)
end;

procedure EIIntfList(const AList, AItem : Pointer; const AIndex : integer);
begin
  IInterfaceList(AList).Delete(AIndex)
end;
//==============================================================================

//==============================================================================
type
  TDeleteItemProc = procedure(const AList : Pointer; const AIndex : integer);

procedure DIList(const AList : Pointer; const AIndex : integer);
begin
  TList(AList).Delete(AIndex)
end;

procedure DIIntfList(const AList : Pointer; const AIndex : integer);
begin
  IInterfaceList(AList).Delete(AIndex)
end;
//==============================================================================

//==============================================================================
type
  TInitListProc = procedure(const AOuterList : Pointer; var AInnerList : Pointer; var ACount : integer);

procedure ILList(const AOuterList : Pointer; var AInnerList : Pointer; var ACount : integer);
begin
  AInnerList := AOuterList;
  ACount := TList(AInnerList).Count;
end;

procedure ILThList(const AOuterList : Pointer; var AInnerList : Pointer; var ACount : integer);
begin
  AInnerList := TThreadList(AOuterList).LockList;
  ACount := TList(AInnerList).Count;
end;

procedure ILIntfList(const AOuterList : Pointer; var AInnerList : Pointer; var ACount : integer);
begin
  AInnerList := AOuterList;
  ACount := IInterfaceList(AInnerList).Count;
end;
//==============================================================================

//==============================================================================
type
  TFinalListProc = procedure(const AOuterList, AInnerList : Pointer);

procedure FLThList(const AOuterList, AInnerList : Pointer);
begin
  TThreadList(AOuterList).UnlockList;
end;
//==============================================================================

//==============================================================================
type
  TInnerFindParamsRec = record
    GetItemFunc  : TGetItemFunc;
    ExtractProc  : TExtractItemProc;
    DeleteProc   : TDeleteItemProc;
    InitProc     : TInitListProc;
    FinalProc    : TFinalListProc;
  end;

// Внутренние константы, назначаютя при инициализации модуля, и затем не меняются.
// По одной переменной на каждый тип списка.
var
  IFPR_L, IFPR_TH, IFPR_IL : TInnerFindParamsRec;
//==============================================================================

//==============================================================================
type
  TStaticOrMethodFindFunc = function (const AFindMethod : TMethod;
    const AItem, AFindParam : Pointer) : boolean;

function StaticFindFunc(const AFindMethod : TMethod;
    const AItem, AFindParam : Pointer) : boolean; {$IFDEF SAN_LIST_UTILS_INLINE}inline;{$ENDIF}
begin
  Result := TSANListFindFunc(AFindMethod.Code)(AItem, AFindParam);
end;

function MethodFindFunc(const AFindMethod : TMethod;
    const AItem, AFindParam : Pointer) : boolean; {$IFDEF SAN_LIST_UTILS_INLINE}inline;{$ENDIF}
begin
  Result := TSANListFindObjFunc(AFindMethod)(AItem, AFindParam)
end;
//==============================================================================

//==============================================================================
type
  TStaticOrMethodClearProc = procedure(const AClearMethod : TMethod; const AItem : Pointer);

procedure StaticClearProc(const AClearMethod : TMethod; const AItem : Pointer); {$IFDEF SAN_LIST_UTILS_INLINE}inline;{$ENDIF}
begin
  TSANClearProc(AClearMethod.Code)(AItem);
end;

procedure MethodClearProc(const AClearMethod : TMethod; const AItem : Pointer); {$IFDEF SAN_LIST_UTILS_INLINE}inline;{$ENDIF}
begin
  TSANClearObjProc(AClearMethod)(AItem)
end;
//==============================================================================

const
  DO_FIND_FLAG_EXTRACT = 1;
  DO_FIND_FLAG_REMOVE = 2;
//==============================================================================
function SANListDoFindEx(var AResultItem; const AList : Pointer;
  const AInnerFindParamsRec : TInnerFindParamsRec;
  const AFindMethod : TMethod; const AFindParams : TSANListParams;
  const AFlags : integer; const AExtMethod : TMethod) : boolean; overload;
var
  i, vCount, vFirstIndex, vResIndex : integer;
  vList, vItem : Pointer;
  vFindFunc : TStaticOrMethodFindFunc;
begin
  Result := False;
  vResIndex := 0;

  if Assigned(AFindMethod.Data) then
    vFindFunc := MethodFindFunc
  else
    vFindFunc := StaticFindFunc;

  AInnerFindParamsRec.InitProc(AList, vList, vCount);
  try

    case AFindParams.Direction of

      FromBeginning:
        begin

          if AFindParams.FirstIndex < 0 then
            vFirstIndex := 0
          else
            vFirstIndex := AFindParams.FirstIndex;

          for i := vFirstIndex to vCount - 1 do
          // DoFind(i)
          begin
            vItem := AInnerFindParamsRec.GetItemFunc(vList, i);

            if (vItem = AFindParams.ExcludeItem)
            or not vFindFunc(AFindMethod, vItem, AFindParams.FindParam)
            then
              Continue;

            Result := True;

            Pointer(AResultItem) := vItem;

            vResIndex := i;

            if Assigned(AFindParams.ResIndexPtr) then
              AFindParams.ResIndexPtr^ := i
          end;

        end;//FromBeginning

      FromEnd:
        begin

          if AFindParams.FirstIndex < 0 then
            vFirstIndex := vCount - 1
          else
            vFirstIndex := AFindParams.FirstIndex;

          for i := vFirstIndex downto 0 do
          // DoFind(i)
          begin
            vItem := AInnerFindParamsRec.GetItemFunc(vList, i);

            if (vItem = AFindParams.ExcludeItem)
            or not vFindFunc(AFindMethod, vItem, AFindParams.FindParam)
            then
              Continue;

            Result := True;

            Pointer(AResultItem) := vItem;

            vResIndex := i;

            if Assigned(AFindParams.ResIndexPtr) then
              AFindParams.ResIndexPtr^ := i
          end;

        end;//FromEnd

    else
      raise EListError.CreateFmt('Invlalid TList.TDirection (%d).', [Ord(AFindParams.Direction)]);
    end;

    if Result then
    begin

     if (AFlags and DO_FIND_FLAG_EXTRACT <> 0) then
       AInnerFindParamsRec.ExtractProc(vList, Pointer(AResultItem), vResIndex)

     else
     if (AFlags and DO_FIND_FLAG_REMOVE <> 0) then
       begin

         if Assigned(AExtMethod.Code) then
           if Assigned(AExtMethod.Data) then
             TSANClearObjProc(AExtMethod)(Pointer(AResultItem))
           else
             TSANClearProc(AExtMethod.Code)(Pointer(AResultItem));

         AInnerFindParamsRec.DeleteProc(vList, vResIndex);
       end;

    end;//if Result

  finally
    if Assigned(AInnerFindParamsRec.FinalProc) then
      AInnerFindParamsRec.FinalProc(AList, vList);
  end;

end;

function SANListDoFindEx(var AResultItem; const AList : Pointer;
  const AInnerFindParamsRec : TInnerFindParamsRec;
  const AFindMethod : TMethod; const AFindParams : TSANListParams;
  const AFlags : integer) : boolean; overload;
begin
  Result := SANListDoFindEx(AResultItem, AList, AInnerFindParamsRec, AFindMethod, AFindParams, AFlags, Method(nil, nil));
end;

function SANListDoFindEx(var AResultItem; const AList : Pointer;
  const AInnerFindParamsRec : TInnerFindParamsRec;
  const AFindFunc : TSANListFindFunc; const AFindParams : TSANListParams;
  const AFlags : integer) : boolean; overload;
begin
  Result := SANListDoFindEx(AResultItem, AList, AInnerFindParamsRec, Method(AFindFunc), AFindParams, AFlags, Method(nil, nil));
end;

function SANListFind(var AResultItem; const AList : TList;
  const AFindFunc : TSANListFindFunc; const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AList, IFPR_L, Method(AFindFunc), AFindParams, 0);
end;

function SANListFind(var AResultItem; const AList : TList;
  const AFindFunc : TSANListFindFunc; const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AList, IFPR_L, Method(AFindFunc), SANListParams(AFindParam), 0);
end;

function SANListFind(var AResultItem; const AThreadList : TThreadList;
  const AFindFunc : TSANListFindFunc; const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AThreadList, IFPR_TH, Method(AFindFunc), AFindParams, 0);
end;

function SANListFind(var AResultItem; const AThreadList : TThreadList;
  const AFindFunc : TSANListFindFunc; const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AThreadList, IFPR_TH, Method(AFindFunc), SANListParams(AFindParam), 0);
end;

function SANListFind(var AResultItem; const AInterfaceList : IInterfaceList;
  const AFindFunc : TSANListFindFunc; const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, Pointer(AInterfaceList), IFPR_IL, Method(AFindFunc), AFindParams, 0);
end;

function SANListFind(var AResultItem; const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, Pointer(AInterfaceList), IFPR_IL, Method(AFindFunc), SANListParams(AFindParam), 0);
end;

function SANListFind(var AResultItem; const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AList, IFPR_L, Method(AFindObjFunc), AFindParams, 0);
end;

function SANListFind(var AResultItem; const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AList, IFPR_L, Method(AFindObjFunc), SANListParams(AFindParam), 0);
end;

function SANListFind(var AResultItem; const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AThreadList, IFPR_TH, Method(AFindObjFunc), AFindParams, 0);
end;

function SANListFind(var AResultItem; const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AThreadList, IFPR_TH, Method(AFindObjFunc), SANListParams(AFindParam), 0);
end;

function SANListFind(var AResultItem; const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), AFindParams, 0);
end;

function SANListFind(var AResultItem; const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), SANListParams(AFindParam), 0);
end;

function SANListExists(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AList, AFindFunc, AFindParams);
end;

function SANListExists(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean; overload;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AList, AFindFunc, SANListParams(AFindParam));
end;

function SANListExists(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AThreadList, AFindFunc, AFindParams);
end;

function SANListExists(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AThreadList, AFindFunc, SANListParams(AFindParam));
end;

function SANListExists(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AInterfaceList, AFindFunc, AFindParams);
end;

function SANListExists(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AInterfaceList, AFindFunc, SANListParams(AFindParam));
end;

function SANListExists(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AList, AFindObjFunc, AFindParams);
end;

function SANListExists(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AList, AFindObjFunc, SANListParams(AFindParam));
end;

function SANListExists(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AThreadList, AFindObjFunc, AFindParams);
end;

function SANListExists(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AThreadList, AFindObjFunc, SANListParams(AFindParam));
end;

function SANListExists(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AInterfaceList, AFindObjFunc, AFindParams);
end;

function SANListExists(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
var
  vDummy : Pointer;
begin
  Result := SANListFind(vDummy, AInterfaceList, AFindObjFunc, SANListParams(AFindParam));
end;

function SANListExtract(var AResultItem; const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AList, IFPR_L, AFindFunc, AFindParams, DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AList, IFPR_L, AFindFunc, SANListParams(AFindParam), DO_FIND_FLAG_EXTRACT);
end;

function SANListExtract(var AResultItem; const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AThreadList, IFPR_TH, AFindFunc, AFindParams, DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AThreadList, IFPR_TH, AFindFunc, SANListParams(AFindParam), DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, Pointer(AInterfaceList), IFPR_IL, AFindFunc, AFindParams, DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, Pointer(AInterfaceList), IFPR_IL, AFindFunc, SANListParams(AFindParam), DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AList, IFPR_L, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AList, IFPR_L, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_EXTRACT);
end;

function SANListExtract(var AResultItem; const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AThreadList, IFPR_TH, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, AThreadList, IFPR_TH, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_EXTRACT)
end;

function SANListExtract(var AResultItem; const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListDoFindEx(AResultItem, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_EXTRACT)
end;

procedure SANListCP_FreeObject(const APtr : Pointer);
begin
  TObject(APtr).Free;
end;

function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindFunc), AFindParams, DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearProc : TSANClearProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearProc));
end;

function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AClearMethod : TSANClearObjProc; const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE, Method(AClearMethod));
end;

function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AList, IFPR_L, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, AThreadList, IFPR_TH, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), AFindParams, DO_FIND_FLAG_REMOVE);
end;

function SANListRemove(const AInterfaceList : IInterfaceList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
var
  vDumm : Pointer;
begin
  Result := SANListDoFindEx(vDumm, Pointer(AInterfaceList), IFPR_IL, Method(AFindObjFunc), SANListParams(AFindParam), DO_FIND_FLAG_REMOVE);
end;

function SANListRemoveObj(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListRemove(AList, AFindFunc, SANListCP_FreeObject, AFindParams);
end;

function SANListRemoveObj(const AList : TList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListRemove(AList, AFindFunc, SANListCP_FreeObject, SANListParams(AFindParam));
end;

function SANListRemoveObj(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListRemove(AThreadList, AFindFunc, SANListCP_FreeObject, AFindParams);
end;

function SANListRemoveObj(const AThreadList : TThreadList; const AFindFunc : TSANListFindFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListRemove(AThreadList, AFindFunc, SANListCP_FreeObject, SANListParams(AFindParam));
end;

function SANListRemoveObj(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListRemove(AList, AFindObjFunc, SANListCP_FreeObject, AFindParams);
end;

function SANListRemoveObj(const AList : TList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListRemove(AList, AFindObjFunc, SANListCP_FreeObject, SANListParams(AFindParam));
end;

function SANListRemoveObj(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParams : TSANListParams) : boolean;
begin
  Result := SANListRemove(AThreadList, AFindObjFunc, SANListCP_FreeObject, AFindParams);
end;

function SANListRemoveObj(const AThreadList : TThreadList; const AFindObjFunc : TSANListFindObjFunc;
  const AFindParam : Pointer) : boolean;
begin
  Result := SANListRemove(AThreadList, AFindObjFunc, SANListCP_FreeObject, SANListParams(AFindParam));
end;

procedure SANListDoClear(const AList : Pointer; const AInnerFindParamsRec : TInnerFindParamsRec;
  const AClearMethod : TMethod); overload;
var
  vList, vItem : Pointer;
  vCount : integer;
  vClearProc : TStaticOrMethodClearProc;
  i : integer;
begin

  AInnerFindParamsRec.InitProc(AList, vList, vCount);
  try

    if vCount = 0 then
      Exit;

    if Assigned(AClearMethod.Data) then
      vClearProc := MethodClearProc
    else
      vClearProc := StaticClearProc;

    for i := vCount - 1 downto 0 do
    begin
      vItem := AInnerFindParamsRec.GetItemFunc(vList, i);

      vClearProc(AClearMethod, vItem);

      AInnerFindParamsRec.DeleteProc(vList, i);
    end;

  finally
    if Assigned(AInnerFindParamsRec.FinalProc) then
      AInnerFindParamsRec.FinalProc(AList, vList)
  end;

end;

procedure SANListDoClear(const AList : Pointer; const AInnerFindParamsRec : TInnerFindParamsRec;
  const AClearProc : TSANClearProc); overload;
begin
  SANListDoClear(AList, AInnerFindParamsRec, Method(AClearProc));
end;

procedure SANListClear(const AList : TList; const AClearProc : TSANClearProc);
begin
  SANListDoClear(AList, IFPR_L, AClearProc);
end;

procedure SANListClear(const AList : TList; const AClearMethod : TSANClearObjProc);
begin
  SANListDoClear(AList, IFPR_L, Method(AClearMethod));
end;

procedure SANListClear(const AThList : TThreadList; const AClearProc : TSANClearProc);
begin
  SANListDoClear(AThList, IFPR_TH, AClearProc);
end;

procedure SANListClear(const AThList : TThreadList; const AClearMethod : TSANClearObjProc);
begin
  SANListDoClear(AThList, IFPR_TH, Method(AClearMethod));
end;

procedure SANListClear(const AInterfaceList : IInterfaceList; const AClearProc : TSANClearProc); overload;
begin
  SANListDoClear(Pointer(AInterfaceList), IFPR_IL, AClearProc);
end;

procedure SANListClear(const AInterfaceList : IInterfaceList; const AClearMethod : TSANClearObjProc); overload;
begin
  SANListDoClear(Pointer(AInterfaceList), IFPR_IL, Method(AClearMethod));
end;

function SANThreadListExtract(const AThList : TThreadList; const AItem : Pointer) : boolean;
begin
  with AThList.LockList do
  try
    Result := Assigned(Extract(AItem));
  finally
    AThList.UnlockList;
  end;
end;

procedure SANListClearObjects(const AList : TList); overload;
begin
  SANListClear(AList, SANListCP_FreeObject);
end;

procedure SANListClearObjects(const AThList : TThreadList); overload;
begin
  SANListClear(AThList, SANListCP_FreeObject);
end;

//==============================================================================
// TSANPtrOwnerList
// protected
procedure TSANPtrOwnerList.Notify(Ptr: Pointer; Action: TListNotification);
begin

  if Action = lnDeleted then
    if Assigned(FClearProc) then
      FClearProc(Ptr)
    else
      Dispose(Ptr);

  inherited;
end;

// public
constructor TSANPtrOwnerList.Create(const AClearProc: TSANClearProc);
begin
  inherited Create;

  FClearProc := AClearProc;
end;
//==============================================================================

INITIALIZATION

  with IFPR_L do
  begin
    GetItemFunc  := GITList;
    ExtractProc  := EIList;
    DeleteProc   := DIList;
    InitProc     := ILList;
    FinalProc    := nil;
  end;

  with IFPR_TH do
  begin
    GetItemFunc  := GITList;
    ExtractProc  := EIList;
    DeleteProc   := DIList;
    InitProc     := ILThList;
    FinalProc    := FLThList;
  end;

  with IFPR_IL do
  begin
    GetItemFunc  := GIIIntfList;
    ExtractProc  := EIIntfList;
    DeleteProc   := DIIntfList;
    InitProc     := ILIntfList;
    FinalProc    := nil;
  end;

END.
