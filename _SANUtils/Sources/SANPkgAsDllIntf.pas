{*******************************************************}
{                                                       }
{       SANPkgAsDllIntf                                 }
{       Общие интерфейсы для взаимодействия             }
{       с "пакетом-как-DLL"                             }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPkgAsDllIntf;
{$I NX.INC}
INTERFACE

TYPE
  /// <summary>
  ///   Интерфейс точки входа "Пакета-как-Dll".
  /// </summary>
  ISANPkgAsDllEntry = interface(IInterface)
  ['{A2DFC442-F06A-467D-ABF9-2A563A53C246}']
    procedure Init(const AInitParams : IInterface); safecall;
    procedure Done(const ADoneParams : IInterface); safecall;
  end;

  /// <summary>
  ///   Информация о версии.
  /// </summary>
  ISANVersionInfo = interface(IInterface)
  ['{B2582030-0CF0-4AA1-AC4B-261DF6FA9E1F}']
    function GetGUID: TGUID; safecall;
    function GetCaption: WideString; safecall;
    function GetDescription: WideString; safecall;
    function GetURL: WideString; safecall;
    function GetAuthor: WideString; safecall;
    function GetVersion: Longword; safecall;

    property GUID: TGUID read GetGUID;
    property Caption: WideString read GetCaption;
    property Description: WideString read GetDescription;
    property URL: WideString read GetURL;
    property Author: WideString read GetAuthor;
    property Version: Longword read GetVersion;
  end;

  ISANPkgAsDllsServer = interface;

  /// <summary>
  ///   Серверный интерфейс обертки над подключенным "Пакетом-как-Dll".
  /// </summary>
  ISANPkgAsDll = interface(IInterface)
  ['{9E59F902-9F05-4B6E-96DE-875960FBD70D}']
    function GetFileName : WideString; safecall;
    function GetHandle : HMODULE; safecall;
    function GetEntry : ISANPkgAsDllEntry; safecall;
    function GetServer : ISANPkgAsDllsServer; safecall;

    /// <summary>
    ///   Имя файла Dll.
    /// </summary>
    property FileName : WideString  read GetFileName;

    /// <summary>
    ///   Описатель Dll.
    /// </summary>
    property Handle : HMODULE  read GetHandle;

    /// <summary>
    ///   Интерфейс точки входа Dll.
    /// </summary>
    property Entry : ISANPkgAsDllEntry  read GetEntry;

    /// <summary>
    ///   Ссылка на владеющий сервер.
    /// </summary>
    property Server : ISANPkgAsDllsServer  read GetServer;
  end;

  /// <summary>
  ///   Серверный интерфейс списка оберток над подключенными "Пакетом-как-Dll".
  /// </summary>
  ISANPkgAsDlls = interface(IInterface)
  ['{953C5A24-06D2-4774-99C8-EE65DFF12D43}']
    function GetCount : integer; safecall;
    function GetItems(const AIndex : integer) : ISANPkgAsDll; safecall;

    /// <summary>
    ///   Очистка.
    /// </summary>
    procedure Clear; safecall;

    /// <summary>
    ///   Попытка поиска по имени файла.
    /// </summary>
    function TryFindByFile(const AFileName : WideString; out APkgAsDll : ISANPkgAsDll) : WordBool; safecall;
    /// <summary>
    ///   Поиск по имени файла. Исключение при неудаче.
    /// </summary>
    function FindByFile(const AFileName : WideString) : ISANPkgAsDll; safecall;
    /// <summary>
    ///   Проверка существования по имени файла.
    /// </summary>
    function ExistsByFile(const AFileName : WideString) : WordBool; safecall;
    /// <summary>
    ///   Удаление по имени файла.
    /// </summary>
    function RemoveByFile(const AFileName : WideString) : WordBool; safecall;

    /// <summary>
    ///   Попытка поиска по GUID версии.
    /// </summary>
    function TryFindByGUID(const AVersionGUID : TGUID; out APkgAsDll : ISANPkgAsDll) : WordBool; safecall;
    /// <summary>
    ///   Поиск по GUID версии. Исключение при неудаче.
    /// </summary>
    function FindByGUID(const AVersionGUID : TGUID) : ISANPkgAsDll; safecall;
    /// <summary>
    ///   Проверка существования по GUID версии.
    /// </summary>
    function ExistsByGUID(const AVersionGUID : TGUID) : WordBool; safecall;
    /// <summary>
    ///   Удаление по GUID версии.
    /// </summary>
    function RemoveByGUID(const AVersionGUID : TGUID) : WordBool; safecall;

    /// <summary>
    ///   Количество элементов в списке.
    /// </summary>
    property Count : integer  read GetCount;

    /// <summary>
    ///   Элементы по индексу.
    /// </summary>
    property Items[const AIndex : integer] : ISANPkgAsDll  read GetItems; default;
  end;

  /// <summary>
  /// Действие при повторении GUID
  /// </summary>
  TSANPkgAsDllDuplicateGUIDAction = (dgaError, dgaReplace, dgaIgnore);

  /// <summary>
  ///   Серверный интерфейс обработчика событий.
  /// </summary>
  ISANPkgAsDllsServerEvents = interface(IInterface)
  ['{FF69BA4F-F961-47E6-BBA7-0E1172635450}']

    /// <summary>
    ///   Выбор действия при дублировании GUID версий пакетов.
    /// </summary>
    procedure PackageGUIDsDuplicated(const ASender : ISANPkgAsDllsServer;
      const AOldPackage, ANewPackage : ISANPkgAsDll; var AAction : TSANPkgAsDllDuplicateGUIDAction); safecall;

  end;

  /// <summary>
  ///   Сервер подключения "Пакетов-как-Dll".
  /// </summary>
  ISANPkgAsDllsServer = interface(IInterface)
  ['{F41810CC-2376-42AD-B80C-43106783F17B}']
    function GetDllEntryPointName : WideString; safecall;
    function GetPackages : ISANPkgAsDlls; safecall;
    function GetEvents : ISANPkgAsDllsServerEvents; safecall;
    procedure SetEvents(const AEvents : ISANPkgAsDllsServerEvents); safecall;

    /// <summary>
    ///   Загрузка нового пакета.
    /// </summary>
    function LoadPackage(const AFileName : WideString) : ISANPkgAsDll; safecall;

    /// <summary>
    ///   Имя точки входа.
    /// </summary>
    property DllEntryPointName : WideString  read GetDllEntryPointName;

    /// <summary>
    ///   Список загруженных пакетов.
    /// </summary>
    property Packages : ISANPkgAsDlls  read GetPackages;

    /// <summary>
    ///   События сервера.
    /// </summary>
    property Events : ISANPkgAsDllsServerEvents  read GetEvents  write SetEvents;

  end;

type
  TSANPkgAsDllExportEntryPointProc    = function(const AInitParams: IInterface): ISANPkgAsDllEntry; safecall;                   // early init
  TSANPkgAsDllExportEntryPointAltProc = function(const AInitParams: IInterface; out Intf: ISANPkgAsDllEntry): HRESULT; stdcall; // early init

IMPLEMENTATION

END.
