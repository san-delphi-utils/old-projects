{*******************************************************}
{                                                       }
{       SANPluginsServerDM                              }
{       Серверная часть плагинов в                      }
{       "пакетах-как-DLL"                               }
{       Оформлена в виде модуля данных,                 }
{       для визуального проектирования                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPluginsServerDM;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf, SANPkgAsDllCustomDM, SANPkgAsDllServer,
  SANPluginsIntf, SANPluginsServer;

TYPE
  /// <summary>
  /// Модуль данных для визуального проектирования сервера плагинов.
  /// </summary>
  TSANPluginsServerDataModule = class(TSANVersionInitDoneDataModule)
  private
    FPlgServer : ISANPlgServer;
    FDllEntryPointName: string;

  public
    constructor Create(AOwner : TComponent); override;

    property PlgServer : ISANPlgServer  read FPlgServer;

  published
    property GUID;
    property Caption;
    property Description;
    property URL;
    property Author;
    property Version;

    property DllEntryPointName : string  read FDllEntryPointName  write FDllEntryPointName;
  end;


IMPLEMENTATION

//==============================================================================
// TSANPluginsServerDataModule
constructor TSANPluginsServerDataModule.Create(AOwner: TComponent);
begin
  inherited;

  FPlgServer := TSANPlgServer.CreateOwned(DllEntryPointName, Self);
end;
//==============================================================================

END.
