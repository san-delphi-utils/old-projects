{*******************************************************}
{                                                       }
{       SANMisc                                         }
{       Набор функций связанных с русским языком        }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANRus;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes, DateUtils, StrUtils;

TYPE
  /// <summary>
  /// Падеж руссого языка
  /// </summary>
  TRusCase = (
    // Именительный     Кто? Что?
    rcNominative,
    // Родительный      Кого? Чего?
    rcGenitive,
    // Дательный        Кому? Чему?
    rcDative,
    // Винительный      Кого? Что?
    rcAccusative,
    // Творительный     Кем? Чем?
    rcInstrumental,
    // Предложный       О ком? О чём?; В ком? В чём?
    rcPrepositional
  );


/// <summary>
/// Месяц AMonth на русском языке в падеже ARusCase.
/// AFirstUpper - должна ли первая буква быть заглавной.
/// </summary>
function RusMonth(const AMonth : byte; const ARusCase : TRusCase; const AFirstUpper : boolean = True) : string;

/// <summary>
/// Выбрать одну из трех форм существительного, следующего за числом ANumber.
/// Пример: 1 день, 2 дня, 10 дней
/// </summary>
function NumSubst(const ANumber : integer; const ASbst1, ASbst2, ASbst3 : string) : string;

/// <summary>
/// Отыскать все фрагменты <ABeg>..<AEnd> в строке S,
/// применить NumSubst к каждому фрагменту.
/// Фрагмент имеет вид: <ABeg> <Index> <ASep> <ASbst1> <ASep> <ASbst2> <ASep> <ASbst3> <AEnd>.
/// Пример: "N д{ень|ня|ней}" -> 1 день, 2 дня, 10 дней
/// </summary>
function NumSubstDecode(const S : string; const ANumbersArr : array of integer;
  const ABeg : string = '{'; const ASep : string = '|'; const AEnd : string = '}') : string;

IMPLEMENTATION

uses
  SANMisc;

function RusMonth(const AMonth : byte; const ARusCase : TRusCase; const AFirstUpper : boolean) : string;
//------------------------------------------------------------------------------
const
  RUS_MONTHS : array [MonthJanuary..MonthDecember, Low(TRusCase)..High(TRusCase)] of string = (
    (
      'Январь',
      'Января',
      'Январю',
      'Январь',
      'Январем',
      'Январе'
    ),

    (
      'Февраль',
      'Февраля',
      'Февралю',
      'Февраль',
      'Февралем',
      'Феврале'
    ),

    (
      'Март',
      'Марта',
      'Марту',
      'Март',
      'Мартом',
      'Марте'
    ),

    (
      'Апрель',
      'Апреля',
      'Апрелю',
      'Апрель',
      'Апрелем',
      'Апреле'
    ),

    (
      'Май',
      'Мая',
      'Маю',
      'Май',
      'Маем',
      'Мае'
    ),

    (
      'Июнь',
      'Июня',
      'Июню',
      'Июнь',
      'Июнем',
      'Июне'
    ),

    (
      'Июль',
      'Июля',
      'Июлю',
      'Июль',
      'Июлем',
      'Июле'
    ),

    (
      'Август',
      'Августа',
      'Августу',
      'Август',
      'Августом',
      'Августе'
    ),

    (
      'Сентябрь',
      'Сентября',
      'Сентябрю',
      'Сентябрь',
      'Сентябрем',
      'Сентябре'
    ),

    (
      'Октябрь',
      'Октября',
      'Октябрю',
      'Октябрь',
      'Октябрем',
      'Октябре'
    ),

    (
      'Ноябрь',
      'Ноября',
      'Ноябрю',
      'Ноябрь',
      'Ноябрем',
      'Ноябре'
    ),

    (
      'Декабрь',
      'Декабря',
      'Декабрю',
      'Декабрь',
      'Декабрем',
      'Декабре'
    )
  );//RUS_MONTHS
//------------------------------------------------------------------------------
begin

  if (AMonth < MonthJanuary) or (AMonth > MonthDecember) then
    raise Exception.CreateFmt('RusMonth'#13#10'Некорректное значение месяца %d! Допустимы %d..%d.', [AMonth, MonthJanuary, MonthDecember]);

  if Ord(ARusCase) > Ord(High(TRusCase)) then
    raise Exception.CreateFmt('RusMonth'#13#10'Некорректное значение падежа TRusCase(%d)!', [Ord(ARusCase)]);


  Result := RUS_MONTHS[AMonth, ARusCase];


  if AFirstUpper then
    Exit;


  Result[1] := AnsiLowerCase(Result[1])[1];
end;

function NumSubst(const ANumber : integer; const ASbst1, ASbst2, ASbst3 : string) : string;
begin
  if (not (ABS(ANumber) mod 10 in [1..4]))
  or (ABS(ANumber) mod 100 in [11..14]) then
    Result := ASbst3 //'дней'
  else
    case ABS(ANumber) mod 10 of
      1:
        Result := ASbst1; //'день'

      2..4:
        Result := ASbst2; //'дня'
    end;//case
end;

function NumSubstDecode(const S : string; const ANumbersArr : array of integer;
  const ABeg : string = '{'; const ASep : string = '|'; const AEnd : string = '}') : string;
var
  vFrag, vIdxS, vSbst1, vSbst2, vSbst3 : string;
  vFragBeg, vFragEnd, vFragOffset, i, vNumIndex : integer;
begin
  Result := '';

  vFragOffset := 1;
  repeat

    vFragBeg := PosEx(ABeg, S, vFragOffset);
    if vFragBeg < 1 then
      Break;

    vFragEnd := PosEx(AEnd, S, vFragBeg + Length(ABeg));
    if vFragEnd < 1 then
      Break;

    Result := Result + Copy(S, vFragOffset, vFragBeg - vFragOffset);

    vFrag := Copy(S, vFragBeg, vFragEnd - vFragBeg + 1);

    i := 1 + Length(ABeg);
    if  ReadSubStr(vFrag, i, vIdxS, ASep)
    and TryStrToInt(vIdxS, vNumIndex)
    and (vNumIndex >= Low(ANumbersArr))
    and (vNumIndex <= High(ANumbersArr))
    and ReadSubStr(vFrag, i, vSbst1, ASep)
    and ReadSubStr(vFrag, i, vSbst2, ASep)
    and ReadSubStr(vFrag, i, vSbst3, AEnd)
    then

{
    if  ReadSubStr(vFrag, i, vIdxS, ASep) then
    if TryStrToInt(vIdxS, vNumIndex)      then
    if (vNumIndex >= Low(ANumbersArr))    then
    if (vNumIndex <= High(ANumbersArr))   then
    if ReadSubStr(vFrag, i, vSbst1, ASep) then
    if ReadSubStr(vFrag, i, vSbst2, ASep) then
    if ReadSubStr(vFrag, i, vSbst3, ASep) then
}
      Result := Result + NumSubst(ANumbersArr[vNumIndex], vSbst1, vSbst2, vSbst3)

    else
      Result := Result + vFrag;

    vFragOffset := vFragEnd + Length(AEnd);
  until vFragOffset >= Length(S);

  Result := Result + Copy(S, vFragOffset, MaxInt);
end;

END.
