{*******************************************************}
{                                                       }
{       SANINIProps                                     }
{       Объект хранящий свойства в INI                  }
{       используя RTTI                                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANINIProps;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes, INIFiles, TypInfo,
  SANRTTIUtils;

TYPE
  /// <summary>
  /// Объект свойств. Все его published-свойства сохраняются и читаются в/из INI.
  /// </summary>
  TSANCustomINIProps = class(TInterfacedPersistent)
  private

  protected
    function GetSection: string; virtual;

    procedure DoDefaultOrdPropValue(const APropInfo: PPropInfo); virtual;
    procedure DoDefaultStrPropValue(const APropInfo: PPropInfo); virtual;
    procedure DefaultPropValue(const APropInfo : PPropInfo); virtual;

  public
    procedure LoadFromINI(const AFileName : TFileName);
    procedure SaveToINI(const AFileName : TFileName);

    /// <summary>
    /// Секция INI-файла, в которой хранятся свойства.
    /// </summary>
    property Section : string  read GetSection;

  published

  end;


IMPLEMENTATION

//==============================================================================
// TSANCustomINIProps
// enumprocs
procedure SLVPropsLoadFromINI(const AObject : TObject; const APropInfo : PPropInfo;
  const AIndex : integer; const AParam : Pointer);
var
  vSection, vName, S : string;
begin
  vSection := (AObject as TSANCustomINIProps).Section;
  vName := String(APropInfo^.Name);


  if not TIniFile(AParam).ValueExists(vSection, vName) then
  begin
    (AObject as TSANCustomINIProps).DefaultPropValue(APropInfo);
    Exit;
  end;


  case APropInfo^.PropType^^.Kind of

    tkInteger, tkChar, tkEnumeration:
      begin
        S := TIniFile(AParam).ReadString(vSection, vName, '');

        SetOrdProp(AObject, APropInfo, GetEnumValue(APropInfo^.PropType^, S) );
      end;

    tkString, tkLString, tkWString, tkUString:
      begin
        S := TIniFile(AParam).ReadString(vSection, vName, '');
        SetStrProp(AObject, APropInfo, S);
      end;

  end;

end;

procedure SLVPropsSaveToINI(const AObject : TObject; const APropInfo : PPropInfo;
  const AIndex : integer; const AParam : Pointer);
var
  vSection : string;
begin
  vSection := (AObject as TSANCustomINIProps).Section;

  case APropInfo^.PropType^^.Kind of

    tkInteger, tkChar, tkEnumeration:
      TIniFile(AParam).WriteString(vSection, String(APropInfo^.Name), GetEnumName(APropInfo^.PropType^,  GetOrdProp(AObject, APropInfo)));


    tkString, tkLString, tkWString, tkUString:
      TIniFile(AParam).WriteString(vSection, String(APropInfo^.Name), GetStrProp(AObject, APropInfo));

  end;

end;

// protected
function TSANCustomINIProps.GetSection: string;
begin
  Result := 'Props';
end;

procedure TSANCustomINIProps.DoDefaultOrdPropValue(const APropInfo: PPropInfo);
begin
  SetOrdProp(Self, APropInfo, APropInfo^.Default)
end;

procedure TSANCustomINIProps.DoDefaultStrPropValue(const APropInfo: PPropInfo);
begin
end;

procedure TSANCustomINIProps.DefaultPropValue(const APropInfo: PPropInfo);
begin

  case APropInfo^.PropType^^.Kind of
    tkInteger, tkChar, tkEnumeration:
      DoDefaultOrdPropValue(APropInfo);

    tkString, tkLString, tkWString, tkUString:
      DoDefaultStrPropValue(APropInfo);

  end;

end;

// public
procedure TSANCustomINIProps.LoadFromINI(const AFileName: TFileName);
var
  vINI : TIniFile;
begin
  vINI := TIniFile.Create(AFileName);
  try

    SANRTTIEnumProps(Self, SLVPropsLoadFromINI, vINI);

  finally
    FreeAndNil(vINI);
  end;
end;

procedure TSANCustomINIProps.SaveToINI(const AFileName: TFileName);
var
  vINI : TIniFile;
begin
  vINI := TIniFile.Create(AFileName);
  try

    SANRTTIEnumProps(Self, SLVPropsSaveToINI, vINI);

  finally
    FreeAndNil(vINI);
  end;
end;
//==============================================================================


END.
