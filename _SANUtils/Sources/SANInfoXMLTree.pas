{*******************************************************}
{                                                       }
{       SANInfoXMLTree                                  }
{       Интерфейс сохранения структуры дерева           }
{       в формате XML                                   }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANInfoXMLTree;
{$I NX.INC}
INTERFACE

TYPE
  ISANInfoXMLTree = interface(IInterface)
    ['{42307EF5-C61F-4E81-A644-9DD09D6F9764}']

    function ToXML(const AIndent : integer = 0) : string;
  end;

procedure XMLTreeSave(const AXMLTree : ISANInfoXMLTree; const AFileName : string;
  const ARewrite : boolean = True);

IMPLEMENTATION

procedure XMLTreeSave(const AXMLTree : ISANInfoXMLTree; const AFileName : string;
  const ARewrite : boolean);
var
  F : System.Text;
begin
  AssignFile(F, AFileName);

  if ARewrite then
    Rewrite(F)
  else
    Append(F);

  try
    Write(F, AXMLTree.ToXML);
  finally
    CloseFile(F);
  end;
end;

END.
