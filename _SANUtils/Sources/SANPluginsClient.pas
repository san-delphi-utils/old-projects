{*******************************************************}
{                                                       }
{       SANPluginsClient                                }
{       Клиентская часть плагинов в                     }
{       "пакетах-как-DLL"                               }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPluginsClient;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, Generics.Collections,
  SANPkgAsDllIntf, SANPkgAsDllClient,
  SANPluginsIntf;

TYPE
  ESANPlgClientError = class(ESANPkgAsDllClientError);

  TSANPlgPluginsList = class;

  /// <summary>
  /// Простой абстрактный класс плагина
  /// </summary>
  TSANPlgPlugin = class(TInterfacedObject, ISANVersionInfo, ISANPlgPlugin)
  private
    FOwnerList : TSANPlgPluginsList;

    // Защита от повторного вызова с сервера
    FIsInit, FIsDone : boolean;

  protected
    // ISANVersionInfo
    function GetGUID: TGUID; virtual; safecall;
    function GetCaption: WideString; virtual; safecall;
    function GetDescription: WideString; virtual; safecall;
    function GetURL: WideString; virtual; safecall;
    function GetAuthor: WideString; virtual; safecall;
    function GetVersion: Longword; virtual; safecall;

    procedure DoInit(const AParams: IInterface); virtual;
    procedure DoDone(const AParams: IInterface); virtual;

    // ISANPlgPlugin
    procedure Init(const AInitParams : IInterface); safecall;
    procedure Done(const ADoneParams : IInterface); safecall;

  public
    destructor Destroy; override;

    property GUID: TGUID read GetGUID;
    property Caption: WideString read GetCaption;
    property Description: WideString read GetDescription;
    property URL: WideString read GetURL;
    property Author: WideString read GetAuthor;
    property Version: Longword read GetVersion;

  end;

  /// <summary>
  /// Список объектов плагинов
  /// </summary>
  TSANPlgPluginsList = class(TObjectList<TSANPlgPlugin>)
  private
    FRegPluginIntf : ISANPlgRegistratorPlugin;

  public
    constructor Create(const ARegPluginIntf : ISANPlgRegistratorPlugin);
    destructor Destroy; override;
    procedure RegistrAll;
    procedure UnRegistrAll;
    function Add(const APlugin : TSANPlgPlugin) : integer;

    property RegPluginIntf : ISANPlgRegistratorPlugin
      read FRegPluginIntf  write FRegPluginIntf;
  end;

IMPLEMENTATION

//==============================================================================
// TSANPlgPlugin
// protected
function TSANPlgPlugin.GetGUID: TGUID;
begin
  Result := StringToGUID('{00000000-0000-0000-0000-000000000000}');
end;

function TSANPlgPlugin.GetCaption: WideString;
begin
  Result := 'CustomPlugin'
end;

function TSANPlgPlugin.GetDescription: WideString;
begin
  Result := 'CustomPluginDescription'
end;

function TSANPlgPlugin.GetURL: WideString;
begin
  Result := ''
end;

function TSANPlgPlugin.GetAuthor: WideString;
begin
  Result := ''
end;

function TSANPlgPlugin.GetVersion: Longword;
begin
  Result := 0
end;

procedure TSANPlgPlugin.DoInit(const AParams: IInterface);
begin
end;

procedure TSANPlgPlugin.DoDone;
begin
end;

procedure TSANPlgPlugin.Init(const AInitParams: IInterface);
begin
  if FIsInit then
    Exit;

  FIsInit := True;

  DoInit(AInitParams);
end;

procedure TSANPlgPlugin.Done(const ADoneParams : IInterface);
begin
  if FIsDone then
    Exit;

  FIsDone := True;

  DoDone(ADoneParams);
end;

// public
destructor TSANPlgPlugin.Destroy;
begin
  if Assigned(FOwnerList) then
    FOwnerList.Extract(Self);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANPlgPluginsList
// public
constructor TSANPlgPluginsList.Create(const ARegPluginIntf: ISANPlgRegistratorPlugin);
begin
  inherited Create(True);

  FRegPluginIntf := ARegPluginIntf;
end;

destructor TSANPlgPluginsList.Destroy;
begin

  if Assigned(FRegPluginIntf) then
    UnRegistrAll;

  Clear;

  FRegPluginIntf := nil;

  inherited;
end;

procedure TSANPlgPluginsList.RegistrAll;
var
  i : integer;
  vPlugin : ISANPlgPlugin;
begin

  i := 0;
  while i <= Count - 1 do
  begin
    vPlugin := Items[i];

    if FRegPluginIntf.RegisterPlugin(vPlugin) then
      Inc(i)

    // Удаление плагина, за счет обнуления счетчика ссылок
    else
      vPlugin := nil;

  end;

end;

procedure TSANPlgPluginsList.UnRegistrAll;
var
  i : integer;
begin

  i := 0;
  while i <= Count - 1 do
  begin

    // При разрегистрации должен вызватья Done, и сниметься ссылка на интерфейс,
    // что приводит к удалению из списка.
    // Сетуация когда в этом месте получено False представляется плохо,
    // а главное не понятно, что делать в этом случае
    Assert(FRegPluginIntf.UnRegisterPlugin( Items[i].GUID ));

  end;

  Clear;
end;

function TSANPlgPluginsList.Add(const APlugin: TSANPlgPlugin): integer;
begin
  APlugin.FOwnerList := Self;
  Result := inherited Add(APlugin);
end;
//==============================================================================


INITIALIZATION

FINALIZATION

END.
