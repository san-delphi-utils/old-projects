{*******************************************************}
{                                                       }
{       SANCancelSecondRun                              }
{       Запрет повторного запуска.                      }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANCancelSecondRun;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, StrUtils;

TYPE
  ESANSecRunError = class(Exception);

/// <summary>
/// Ограничить повторный запуск через файл-маппинг.
/// AAppHandle - хендл приложения, которое защищается от повторного запуска.
/// AFullExeName - возможность запуска копий из разных папок.
/// AActivateIfExists - активация уже запущенного приложения.
/// AMappingHandle - хендл маппинга, который должен быть очищен SANSecRunMappingFree в try..finally.
/// AIsSecondRun - запуск является повторным.
/// </summary>
procedure SANSecRunMappingCreate(const AAppHandle : THandle; const AFullExeName,
  AActivateIfExists: boolean; var AMappingHandle : THandle; var AIsSecondRun : boolean);

/// <summary>
/// Завершить ограничение повторного запуска через файл-маппинг.
/// AMappingHandle - хендл маппинга полученый в SANSecRunMappingCreate.
/// </summary>
procedure SANSecRunMappingFree(const AMappingHandle : THandle);

IMPLEMENTATION

procedure SANSecRunMappingCreate(const AAppHandle : THandle; const AFullExeName,
  AActivateIfExists: boolean; var AMappingHandle : THandle; var AIsSecondRun : boolean);
const
  MemFileSize = 127;
var
  vMemFileName     : string;
  vlpBaseAddress   : ^THandle;
begin

  if AFullExeName then
    vMemFileName := AnsiReplaceText(ParamStr(0), '\', '/')
  else
    vMemFileName := ExtractFileName(ParamStr(0));

  AMappingHandle := CreateFileMapping
  (
    HWND($FFFFFFFF),
    nil,
    PAGE_READWRITE,
    0,
    MemFileSize,
    PChar(vMemFileName)
  );

  if AMappingHandle = 0 then
    RaiseLastOSError;

  try

    AIsSecondRun := GetLastError = ERROR_ALREADY_EXISTS;


    if AIsSecondRun and not AActivateIfExists then
      Exit;


    if AIsSecondRun then
      vlpBaseAddress := MapViewOfFile(AMappingHandle, FILE_MAP_READ,  0, 0, 0)
    else
      vlpBaseAddress := MapViewOfFile(AMappingHandle, FILE_MAP_WRITE, 0, 0, 0);


    if vlpBaseAddress = nil then
      RaiseLastOSError;

    try

      if not AIsSecondRun then
        vlpBaseAddress^ := AAppHandle

      else
        if vlpBaseAddress^ <> AAppHandle then
        begin
          ShowWindow(vlpBaseAddress^, SW_RESTORE);
          SetForegroundWindow(vlpBaseAddress^);
        end;

    finally
      UnMapViewOfFile(vlpBaseAddress);
    end;

  except
    SANSecRunMappingFree(AMappingHandle);
    raise;
  end;
end;


procedure SANSecRunMappingFree(const AMappingHandle : THandle);
begin
  CloseHandle(AMappingHandle);
end;


END.
