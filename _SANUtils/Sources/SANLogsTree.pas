{*******************************************************}
{                                                       }
{       SANLogsTree                                     }
{       Вывод текста ввиде дерева.                      }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANLogsTree;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes, Contnrs, StrUtils, Math,
  SANLogsIntf, SANLogsBase;

TYPE
  ESANLogTreeError = class(ESANLogError);
//==============================================================================
  TSLTree = class(TInterfacedPersistent, ISANCustomLog)
  private
    FRedirectLog : ISANCustomLog;

    FLevels : TList;

    FVLineChar, FHLineChar, FCrossChar : Char;
    FVIndent, FHIndent, FSubIndent, FLeftIndent : byte;

    FFirstLine, FVIndentIfEmpty : boolean;

  private
    function Get_LevelsCount: integer;
    function Get_LevelsNext(const ALevel: integer): boolean;

  protected
    // ISANCustomLog
    function Write(const AKind : LongWord; const ASubType : WideString; const AMessage : WideString = ''; ADateTime : TDateTime = 0) : integer; safecall;
    function BeginWrite(const AKind : LongWord; const ASubType : WideString; ADateTime : TDateTime = 0) : integer; safecall;
    procedure PartialWrite(const APartialMessage : WideString); safecall;
    procedure EndWrite; safecall;

    procedure ValidateRedirectLog;

    function IndentStr(const AForceVrtLine : boolean = False) : string;

  public
    constructor Create(const ARedirectLog : ISANCustomLog);
    destructor Destroy; override;

    /// <summary>
    /// Начало вывода в дерево.
    /// </summary>
    procedure TreeBeginWrite(const AKind : LongWord; const ASubType : string; ADateTime : TDateTime = 0);
    /// <summary>
    /// Вывод строки в дерево.
    /// </summary>
    procedure WriteStr(const AStr : string);
    /// <summary>
    /// Завершение вывода в дерево.
    /// </summary>
    procedure TreeEndWrite;

    /// <summary>
    /// Начать новый лист.
    /// Св-во LevelsNext[] показывает какие ветки продолжаются (при True)
    /// Если ALevel в пределах [0, LevelsCount - 1] и LevelsNext[ALevel] = True,
    /// то добавляется подузел уровня ALevel, иначе подузел уровня LevelsCount
    /// AExistParentNext - новое значение для LevelsNext[ALevel]
    /// </summary>
    procedure NewNode(const AExistParentNext : boolean; const ALevel :integer = -1);
    /// <summary>
    /// Завершить текущий лист, отрисовать линии старших листьев (если они есть)
    /// </summary>
    procedure EndNode;

    /// <summary>
    /// Простой лист с текстом
    /// </summary>
    procedure SimpleNode(const ANodeText : string;  const AExistParentNext : boolean;
      const ALevel : integer = -1);

    /// <summary>
    /// Ссылка на лог, в который перенаправляется вывод
    /// </summary>
    property RedirectLog : ISANCustomLog  read FRedirectLog  write FRedirectLog;

    /// <summary>
    /// Кол-во уровней открытых в данный момент
    /// </summary>
    property LevelsCount : integer  read Get_LevelsCount;
    /// <summary>
    /// True - для уровней на которых ожидается следующий,
    /// False - для уровней на нет следующего узла
    /// </summary>
    property LevelsNext [const ALevel : integer] : boolean  read Get_LevelsNext;

    /// <summary>
    /// Символ, вертикальной линии. По умолчанию "|"
    /// </summary>
    property VLineChar : Char  read FVLineChar  write FVLineChar;
    /// <summary>
    /// Символ, горизонтальной линии. По умолчанию "-"
    /// </summary>
    property HLineChar : Char  read FHLineChar  write FHLineChar;
    /// <summary>
    /// Символ, пересечения линиий. По умолчанию "+"
    /// </summary>
    property CrossChar : Char  read FCrossChar  write FCrossChar;

    /// <summary>
    /// Вертикальный отступ.
    /// </summary>
    property VIndent     : byte  read FVIndent     write FVIndent;
    /// <summary>
    /// Горизогтальный отступ.
    /// </summary>
    property HIndent     : byte  read FHIndent     write FHIndent;
    /// <summary>
    /// Отступ для подэлемента.
    /// </summary>
    property SubIndent   : byte  read FSubIndent   write FSubIndent;
    /// <summary>
    /// Отступ слева.
    /// </summary>
    property LeftIndent  : byte  read FLeftIndent  write FLeftIndent;

    /// <summary>
    /// Применять ли вертикальный отступ для пустого дерева.
    /// </summary>
    property VIndentIfEmpty : boolean  read FVIndentIfEmpty  write FVIndentIfEmpty;
  end;
//==============================================================================

IMPLEMENTATION

resourcestring
  sNotLogLinkErrorMessage  = 'Для вывода логов в виде дерева не задана ссылка на лог';

//==============================================================================
// TSLTree
// private
function TSLTree.Get_LevelsCount: integer;
begin
  Result := FLevels.Count;
end;

function TSLTree.Get_LevelsNext(const ALevel: integer): boolean;
begin
  Result := Boolean(FLevels[ALevel]);
end;

// protected
function TSLTree.Write(const AKind: LongWord; const ASubType,
  AMessage: WideString; ADateTime: TDateTime): integer;
begin
  WriteStr(AMessage);
  Result := 0;
end;

function TSLTree.BeginWrite(const AKind : LongWord; const ASubType : WideString;
  ADateTime : TDateTime) : integer;
begin
  Result := 0;
end;

procedure TSLTree.PartialWrite(const APartialMessage : WideString);
begin
  WriteStr(APartialMessage);
end;

procedure TSLTree.EndWrite;
begin
end;

procedure TSLTree.ValidateRedirectLog;
begin
  if not Assigned(FRedirectLog) then
    raise ESANLogTreeError.CreateRes(@sNotLogLinkErrorMessage);
end;

function TSLTree.IndentStr(const AForceVrtLine : boolean): string;
var
  i : integer;
begin
  Result := IfThen(LevelsCount > 0, DupeString(' ', FLeftIndent));

  for i := 0 to LevelsCount - 1 do
    if i < LevelsCount - 1 then
      Result := Result +
                IfThen(LevelsNext[i], VLineChar, ' ') +
                DupeString(' ', FHIndent + FSubIndent)
    else
      Result := Result +
                IfThen(FFirstLine, CrossChar, IfThen(LevelsNext[i] or AForceVrtLine, VLineChar, ' ') ) +
                DupeString( IfThen(FFirstLine, HLineChar, ' '), FHIndent);

end;

// public
constructor TSLTree.Create(const ARedirectLog : ISANCustomLog);
begin
  inherited Create;

  FRedirectLog := ARedirectLog;

  FLevels := TList.Create;

  FVLineChar := '|';
  FHLineChar := '-';
  FCrossChar := '+';

  FHIndent     := 2;
  FVIndent     := 1;
  FSubIndent   := 2;
  FLeftIndent  := 2;
end;

destructor TSLTree.Destroy;
begin
  FreeAndNil(FLevels);

  inherited;
end;

procedure TSLTree.TreeBeginWrite(const AKind: LongWord;
  const ASubType: string; ADateTime: TDateTime);
begin
  ValidateRedirectLog;

  FRedirectLog.BeginWrite(AKind, ASubType, ADateTime);
end;

procedure TSLTree.WriteStr(const AStr: string);
begin
  ValidateRedirectLog;

  if Copy(AStr, Length(AStr) - 1, 2) = #13#10 then
    FRedirectLog.PartialWrite(IndentStr + AStr)
  else
    FRedirectLog.PartialWrite(IndentStr + AStr + #13#10);

  FFirstLine := False;
end;

procedure TSLTree.TreeEndWrite;
begin
  ValidateRedirectLog;

  FRedirectLog.EndWrite;
end;

procedure TSLTree.NewNode(const AExistParentNext : boolean; const ALevel: integer);
var
  vRowIndex : byte;
  vEmptyTree : boolean;
begin
  ValidateRedirectLog;

  vEmptyTree := (LevelsCount = 0);


  if (ALevel < 0) or (ALevel > LevelsCount - 1) or (not LevelsNext[ALevel]) then
    FLevels.Add( Pointer(AExistParentNext) )

  else  begin
    FLevels[ALevel] := Pointer(AExistParentNext);

    with FLevels do
      while Count - 1 > ALevel do
        Delete(Count - 1);
  end;//else if


  if (not vEmptyTree) or (FVIndentIfEmpty) then
    for vRowIndex := 1 to VIndent do
      FRedirectLog.PartialWrite(IndentStr(True) + #13#10);

  FFirstLine := True;
end;

procedure TSLTree.EndNode;
begin
  with FLevels do
    while (Count > 0) and (not LevelsNext[Count - 1]) do
      Delete(Count - 1);
end;

procedure TSLTree.SimpleNode(const ANodeText: string; const AExistParentNext: boolean;
  const ALevel: integer);
begin
  NewNode(AExistParentNext, ALevel);
  try
    WriteStr(ANodeText);
  finally
    EndNode;
  end;//t..f
end;

END.
