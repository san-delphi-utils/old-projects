{*******************************************************}
{                                                       }
{       SANPluginsServer                                }
{       Серверная часть плагинов в                      }
{       "пакетах-как-DLL"                               }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPluginsServer;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf, SANPkgAsDllServer,
  SANPluginsIntf;

TYPE
  ESANPlgServerError = class(ESANPkgAsDllServerError);

  /// <summary>
  ///   Список плагинов.
  /// </summary>
  TSANPlgPluginsList = class(TInterfaceList, IInterfaceList, ISANPlgPlugins)
  private
    FServer : Pointer;// weak link

  protected
    function GetServer : ISANPlgServer; safecall;
    function GetCount: Integer; safecall;
    function GetItems(const AIndex: Integer): ISANPlgPlugin; safecall;
    procedure Clear; safecall;
    function TryFindByGUID(const AGUID : TGUID; out APlugin : ISANPlgPlugin) : WordBool; safecall;
    function FindByGUID(const AGUID : TGUID) : ISANPlgPlugin; safecall;
    function ExistsByGUID(const AGUID : TGUID) : WordBool; safecall;
    function RemoveByGUID(const AGUID : TGUID) : WordBool; safecall;

  public
    constructor Create(const AServer : ISANPlgServer);

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property Server : ISANPlgServer  read GetServer;
    property Items[const AIndex: Integer]: ISANPlgPlugin read GetItems; default;
  end;

  /// <summary>
  ///   Серверный объект, управляющий набором плагинов
  /// </summary>
  TSANPlgServer = class(TSANPkgAsDllsServer, ISANPkgAsDllsServer,
    ISANPlgRegistratorPlugin, ISANPlgServer)
  private
    FPlugins : ISANPlgPlugins;

  protected
    procedure DoPluginsGUIDsDuplicated(const AOldPlugin, ANewPlugin : ISANPlgPlugin;
      var AAction : TSANPkgAsDllDuplicateGUIDAction);
    procedure ValidatePluginGUID(const ANewPlugin : ISANPlgPlugin; out AContinue : boolean);

    // ISANPlgRegistratorPlugin
    function RegisterPlugin(const APlugin: ISANPlgPlugin) : boolean; safecall;
    function UnRegisterPlugin(const APluginGUID: TGUID) : boolean; safecall;

    // ISANPlgServer
    function GetPlugins : ISANPlgPlugins; safecall;
    function GetEvents : ISANPlgServerEvents; safecall;
    procedure SetEvents(const AEvents : ISANPlgServerEvents); safecall;

  public
    constructor Create(const ADllEntryPointName : string); override;
    procedure BeforeDestruction; override;
    destructor Destroy; override;

    property Plugins  : ISANPlgPlugins       read GetPlugins;
    property Events   : ISANPlgServerEvents  read GetEvents  write SetEvents;
  end;

IMPLEMENTATION

uses
  ComObj,
  SANPluginsMessages;

//==============================================================================
// TSANPlgPluginsList
// protected
function TSANPlgPluginsList.GetServer: ISANPlgServer;
begin
  Result := ISANPlgServer(FServer);
end;

function TSANPlgPluginsList.GetCount: Integer;
begin
  Result := inherited Count;
end;

function TSANPlgPluginsList.GetItems(const AIndex: Integer): ISANPlgPlugin;
begin
  Result := (inherited Items[AIndex]) as ISANPlgPlugin;
end;

procedure TSANPlgPluginsList.Clear;
var
  i : integer;
begin

  for i := 0 to Count - 1 do
    Items[i].Done(Server);

  inherited Clear;
end;

function TSANPlgPluginsList.TryFindByGUID(const AGUID: TGUID;
  out APlugin: ISANPlgPlugin): WordBool;
var
  i : integer;
  vItem : ISANPlgPlugin;
begin
  Result   := False;
  APlugin  := nil;

  for i := 0 to Count - 1 do
  begin
    vItem := Items[i];

    if vItem.GUID <> AGUID then
      Continue;

    APlugin  := vItem;
    Exit(True);
  end;

end;

function TSANPlgPluginsList.FindByGUID(const AGUID: TGUID): ISANPlgPlugin;
begin
  if not TryFindByGUID(AGUID, Result) then
    raise ESANPlgServerError.CreateResFmt
    (
      @sServer_PluginNotFoundByGUIDFmtError,
      [GUIDToString(AGUID)]
    );
end;

function TSANPlgPluginsList.ExistsByGUID(const AGUID: TGUID): WordBool;
var
  vDummy : ISANPlgPlugin;
begin
  Result := TryFindByGUID(AGUID, vDummy);
end;

function TSANPlgPluginsList.RemoveByGUID(const AGUID: TGUID): WordBool;
var
  i : integer;
begin
  Result := False;

  for i := 0 to Count - 1 do
    if Items[i].GUID = AGUID then
    begin
      Delete(i);
      Exit;
    end;
end;

// public
constructor TSANPlgPluginsList.Create(const AServer: ISANPlgServer);
begin
  inherited Create;

  FServer := Pointer(AServer);
end;

function TSANPlgPluginsList.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANPlgPlugins,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANPlgServer
// protected
procedure TSANPlgServer.DoPluginsGUIDsDuplicated(const AOldPlugin,
  ANewPlugin: ISANPlgPlugin; var AAction: TSANPkgAsDllDuplicateGUIDAction);
begin
  if Assigned(Events) then
    Events.PluginsGUIDsDuplicated(Self, AOldPlugin, ANewPlugin, AAction);
end;

procedure TSANPlgServer.ValidatePluginGUID(const ANewPlugin: ISANPlgPlugin;
  out AContinue : boolean);
var
  vOldPlugin : ISANPlgPlugin;
  vAction : TSANPkgAsDllDuplicateGUIDAction;
begin
  AContinue := True;

  if not FPlugins.TryFindByGUID(ANewPlugin.GUID, vOldPlugin) then
    Exit;

  vAction := dgaError;

  DoPluginsGUIDsDuplicated(vOldPlugin, ANewPlugin, vAction);

  case vAction of

    dgaError:
      raise ESANPlgServerError.CreateResFmt
      (
        @sServer_DuplicatePluginVersionGUIDFmtError,
        [ANewPlugin.Caption, GUIDToString(ANewPlugin.GUID), vOldPlugin.Caption]
      );

    dgaReplace:
      Assert(FPlugins.RemoveByGUID(ANewPlugin.GUID));

    dgaIgnore:
      AContinue := False;
  end;

end;

function TSANPlgServer.RegisterPlugin(const APlugin: ISANPlgPlugin) : boolean;
var
  vContinue : boolean;
begin
  Result := False;

  ValidatePluginGUID(APlugin, vContinue);

  if not vContinue then
    Exit;

  APlugin.Init(Self);

  (FPlugins as IInterfaceList).Add(APlugin);

  Result := True;
end;

function TSANPlgServer.UnRegisterPlugin(const APluginGUID: TGUID) : boolean;
var
  vPlugin : ISANPlgPlugin;
begin
  Result := FPlugins.TryFindByGUID(APluginGUID, vPlugin) and FPlugins.RemoveByGUID(APluginGUID);

  if Result then
    vPlugin.Done(Self);
end;

function TSANPlgServer.GetPlugins: ISANPlgPlugins;
begin
  Result := FPlugins;
end;

function TSANPlgServer.GetEvents: ISANPlgServerEvents;
begin
  if not Supports(inherited Events, ISANPlgServerEvents, Result) then
    Result := nil;
end;

procedure TSANPlgServer.SetEvents(const AEvents: ISANPlgServerEvents);
begin
  inherited SetEvents(AEvents);
end;

// public
constructor TSANPlgServer.Create(const ADllEntryPointName : string);
begin
  inherited;

  FPlugins := TSANPlgPluginsList.Create(Self);
end;

procedure TSANPlgServer.BeforeDestruction;
begin
  FPlugins.Clear;

  inherited;
end;

destructor TSANPlgServer.Destroy;
begin
  inherited;

  FPlugins := nil;
end;
//==============================================================================



END.
