{*******************************************************}
{                                                       }
{       SANDynamicClasses                               }
{       Переопределяемые классы                         }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANDynamicClasses;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes,
  SANInfoXMLTree;

TYPE
  ESANDynamicCls = class(Exception);

  TSANDynamicClsInfo = class;

  TSANDynClsEnumDynClassesProc = procedure (const AClass : TClass; var AParam : Pointer);
  TSANDynClsEnumDynClassesObjProc = procedure (const AClass : TClass; var AParam : Pointer) of object;
  TSANDynClsListEnumInfosProc = procedure (const ADynClassInfo : TSANDynamicClsInfo; var AParam : Pointer);
  TSANDynClsListEnumInfosObjProc = procedure (const ADynClassInfo : TSANDynamicClsInfo; var AParam : Pointer) of object;

  /// <summary>
  /// Информация о динамически определяемом классе
  /// </summary>
  TSANDynamicClsInfo = class(TInterfacedPersistent, ISANInfoXMLTree)
  private
    FBaseClass: TClass;
    FBaseClassLevel : integer;
    FInheritanceChecking : boolean;
    FClassesList : TThreadList;


    function GetTopClass: TClass;
    procedure SetTopClass(const Value: TClass);
    function GetDynClassesCount: integer;
    function GetDynClasses(const Index: integer): TClass;

  protected
    // ISANInfoXMLTree
    function ToXML(const AIndent : integer = 0) : string;

  public
    constructor Create(const ABaseClass : TClass; const AInheritanceChecking : boolean = True);
    destructor Destroy; override;

    function FindDynClassByName(var ADynClasses; const AName : string) : boolean;
    function RemoveDynClass(const ADynClasses : TClass) : boolean;

    procedure EnumDynClasses(const AEnumProc : TSANDynClsEnumDynClassesProc; AParam : Pointer = nil); overload;
    procedure EnumDynClasses(const AEnumProc : TSANDynClsEnumDynClassesObjProc; AParam : Pointer = nil); overload;

    property BaseClass : TClass  read FBaseClass;
    property TopClass  : TClass  read GetTopClass  write SetTopClass;

    property BaseClassLevel : integer  read FBaseClassLevel;
    property InheritanceChecking : boolean  read FInheritanceChecking;

    property DynClassesCount : integer  read GetDynClassesCount;
    property DynClasses[const Index : integer] : TClass  read GetDynClasses;
  end;

  /// <summary>
  /// Набор объектов информации о динамически определяемом классе
  /// </summary>
  TSANDynamicClsInfoList = class(TInterfacedPersistent, ISANInfoXMLTree)
  private
    FDynClssList : TThreadList;

    function GetDynClsInfos(const Index: integer): TSANDynamicClsInfo;
    function GetDynClsInfosCount: integer;
    function GetDynClsInfoByBest(const ABestClass: TClass): TSANDynamicClsInfo;

  protected
    procedure DoClear;

    // ISANInfoXMLTree
    function ToXML(const AIndent : integer = 0) : string;

  public
    constructor Create;
    destructor Destroy; override;

    procedure Clear;
    function FindByBestClass(var ADynamicClsInfo; const ABestClass : TClass) : boolean;
    function ExistsByBestClass(const ABestClass : TClass) : boolean;
    function FindByClass(var ADynamicClsInfo; AClass : TClass) : boolean;
    function ExistsByClass(const AClass : TClass) : boolean;
    function ExtractByClass(var ADynamicClsInfo; const ABestClass : TClass) : boolean; overload;
    function ExtractByClass(const ABestClass : TClass) : TSANDynamicClsInfo; overload;
    function RemoveByClass(const ABestClass : TClass) : boolean;
    function TryAdd(var ADynamicClsInfo; const ABestClass : TClass) : boolean; overload;
    function TryAdd(const ABestClass : TClass) : TSANDynamicClsInfo; overload;
    function Add(const ABestClass : TClass) : TSANDynamicClsInfo;

    procedure EnumDynClsInfos(const AEnumProc : TSANDynClsListEnumInfosProc; AParam : Pointer = nil); overload;
    procedure EnumDynClsInfos(const AEnumProc : TSANDynClsListEnumInfosObjProc; AParam : Pointer = nil); overload;

    property DynClsInfosCount : integer  read GetDynClsInfosCount;
    property DynClsInfos[const Index : integer] : TSANDynamicClsInfo  read GetDynClsInfos;
    property DynClsInfoByBest[const ABestClass : TClass] : TSANDynamicClsInfo  read GetDynClsInfoByBest; default;
  end;

IMPLEMENTATION

USES
  Math, StrUtils,
  SANMisc,
  SANListUtils;

resourcestring
  sBaseClassAlreadyAddedErrorMessage  = 'Базовый класс «%s» уже добавлен в список';
  sBaseClassNotAssignedErrorMessage   = 'Базовый класс не задан';
  sInfoNotFoundErrorMessage           = 'Нет информации для базового класса «%s»';
  sInheritanceCheckFailErrorMessage   = 'Класс «%s» не является потомком базового класса «%s»';

//==============================================================================
/// <summary>
/// Поиск класса по имени.
/// </summary>
/// <param name="AItem">
/// Проверяемый элемент списка, класс.
/// </param>
/// <param name="AValue">
/// Значение с которым сравнивается параметр, указатель на строку.
/// </param>
/// <returns>
/// Boolean. True, если AItem "подходит" для поиска по AValue.
/// </returns>
function SANListFF_ClassByName(const AItem, AValue : Pointer) : boolean;
begin
  Result := CompareText(TClass(AItem).ClassName, PString(AValue)^) = 0;
end;
//==============================================================================

//==============================================================================
// TSANDynamicClsInfo
// private
function TSANDynamicClsInfo.GetTopClass: TClass;
begin
  with FClassesList.LockList do
  try

    if Count = 0 then
      Result := Self.FBaseClass
    else
      Result := TClass(First);

  finally
    FClassesList.UnlockList;
  end;
end;

procedure TSANDynamicClsInfo.SetTopClass(const Value: TClass);
begin

  if not Assigned(Value) then
    Exit;


  if FInheritanceChecking and not Value.InheritsFrom(BaseClass) then
    raise ESANDynamicCls.CreateResFmt(@sInheritanceCheckFailErrorMessage, [Value.ClassName, BaseClass.ClassName]);


  with FClassesList.LockList do  try

    Insert(0, Value);

  finally
    FClassesList.UnlockList;
  end;

end;

function TSANDynamicClsInfo.GetDynClassesCount: integer;
begin
  with FClassesList.LockList do
  try
    Result := Count;
  finally
    FClassesList.UnlockList;
  end;
end;

function TSANDynamicClsInfo.GetDynClasses(const Index: integer): TClass;
begin
  with FClassesList.LockList do
  try
    Result := TClass(Items[Index]);
  finally
    FClassesList.UnlockList;
  end;
end;

// protected
function TSANDynamicClsInfo.ToXML(const AIndent : integer): string;
var
  i : integer;
begin
  Result := '';

  StrAddFmt(Result, '<%s BaseClass="%s" InheritanceChecking="%s">', [ClassName, BaseClass.ClassName, BoolToStr(FInheritanceChecking, True)], AIndent);
  try

    with FClassesList.LockList do
    try

      for i := 0 to Count - 1 do
        StrAddFmt(Result, '<DynClsInfo>%s</DynClsInfo>', [ TClass(Items[i]).ClassName], AIndent + 1);

    finally
      FClassesList.UnlockList;
    end;

  finally
    StrAddFmt(Result, '</%s>', [ClassName], AIndent);
  end;
end;

// public
constructor TSANDynamicClsInfo.Create(const ABaseClass: TClass;
  const AInheritanceChecking : boolean);
begin
  inherited Create;

  if not Assigned(ABaseClass) then
    raise ESANDynamicCls.CreateRes(@sBaseClassNotAssignedErrorMessage);

  FBaseClass := ABaseClass;
  FBaseClassLevel := GetClassLevel(FBaseClass);

  FInheritanceChecking := AInheritanceChecking;

  FClassesList := TThreadList.Create;
end;

destructor TSANDynamicClsInfo.Destroy;
begin
  FreeAndNil(FClassesList);

  inherited;
end;

function TSANDynamicClsInfo.FindDynClassByName(var ADynClasses;
  const AName: string): boolean;
begin
  Result := SANListFind(ADynClasses, FClassesList, SANListFF_ClassByName, @AName);
end;

function TSANDynamicClsInfo.RemoveDynClass(const ADynClasses : TClass) : boolean;
begin
  with FClassesList.LockList do
  try
    Result := Remove(ADynClasses) > -1;
  finally
    FClassesList.UnlockList;
  end;
end;

procedure TSANDynamicClsInfo.EnumDynClasses(const AEnumProc: TSANDynClsEnumDynClassesProc;
  AParam: Pointer);
var
  i : integer;
begin
  with FClassesList.LockList do
  try
    for i := 0 to Count - 1 do
      AEnumProc(TClass(Items[i]), AParam);
  finally
    FClassesList.UnlockList;
  end;
end;

procedure TSANDynamicClsInfo.EnumDynClasses(const AEnumProc: TSANDynClsEnumDynClassesObjProc;
  AParam: Pointer);
var
  i : integer;
begin
  with FClassesList.LockList do
  try
    for i := 0 to Count - 1 do
      AEnumProc(TClass(Items[i]), AParam);
  finally
    FClassesList.UnlockList;
  end;
end;
//==============================================================================

//==============================================================================
// TSANDynamicClsInfoList
// private
function TSANDynamicClsInfoList.GetDynClsInfosCount: integer;
begin
  with FDynClssList.LockList do
  try
    Result := Count
  finally
    FDynClssList.UnlockList;
  end;
end;

function TSANDynamicClsInfoList.GetDynClsInfos(const Index: integer): TSANDynamicClsInfo;
begin
  with FDynClssList.LockList do
  try
    Result := TSANDynamicClsInfo( Items[Index] );
  finally
    FDynClssList.UnlockList;
  end;
end;

function TSANDynamicClsInfoList.GetDynClsInfoByBest(const ABestClass: TClass): TSANDynamicClsInfo;
begin
  if not FindByBestClass(Result, ABestClass) then
    if Assigned(ABestClass) then
      raise ESANDynamicCls.CreateResFmt(@sInfoNotFoundErrorMessage, [ABestClass.ClassName])

    else
      raise ESANDynamicCls.CreateRes(@sBaseClassNotAssignedErrorMessage)
end;

// protected
procedure TSANDynamicClsInfoList.DoClear;
var
  i : integer;
begin
  with FDynClssList.LockList do
  try

    for i := 0 to Count - 1 do
      TObject(Items[i]).Free;

  finally
    FDynClssList.UnlockList;
  end;
end;

function TSANDynamicClsInfoList.ToXML(const AIndent : integer): string;
var
  i : integer;
begin
  Result := '';

  with FDynClssList.LockList do
  try

    StrAddFmt(Result, '<%s Count="%d">', [Self.ClassName, Count], AIndent);
    try

      for i := 0 to Count - 1 do
        StrAdd(Result, TSANDynamicClsInfo(Items[i]).ToXML(AIndent + 1));

    finally
      StrAddFmt(Result, '</%s>', [Self.ClassName], AIndent);
    end;

  finally
    FDynClssList.UnlockList;
  end;

end;

// public
constructor TSANDynamicClsInfoList.Create;
begin
  inherited Create;

  FDynClssList := TThreadList.Create;
end;

destructor TSANDynamicClsInfoList.Destroy;
begin
  DoClear;
  FreeAndNil(FDynClssList);
  inherited;
end;

procedure TSANDynamicClsInfoList.Clear;
begin
  DoClear;
  FDynClssList.Clear;
end;

function SANListFF_FindByBestClass(const AItem, AValue : Pointer) : boolean;
begin
  Result := TSANDynamicClsInfo(AItem).BaseClass = TClass(AValue);
end;

function TSANDynamicClsInfoList.FindByBestClass(var ADynamicClsInfo;
  const ABestClass: TClass): boolean;
begin
  if Assigned(ABestClass) then
    Result := SANListFind(ADynamicClsInfo, FDynClssList, SANListFF_FindByBestClass, ABestClass)
  else
    Result := False;
end;

function TSANDynamicClsInfoList.ExistsByBestClass(const ABestClass: TClass): boolean;
begin
  if Assigned(ABestClass) then
    Result := SANListExists(FDynClssList, SANListFF_FindByBestClass, ABestClass)
  else
    Result := False;
end;

function TSANDynamicClsInfoList.FindByClass(var ADynamicClsInfo; AClass: TClass): boolean;
var
  vList : TList;
  vIdx : integer;
begin

  if not Assigned(AClass) or (AClass = TObject) then
    Exit(False);

  vIdx := 0;

  vList := FDynClssList.LockList;
  try

    repeat

      // Т.к. список отсортирован по убыванию уровня иерархии класса,
      // при поиске базового класса можно просматривать список однократно (порциями),
      // запоминая индекс (vIdx) последнего просмотренного элемента.
      if SANListFind
        (
         ADynamicClsInfo,
         vList,
         SANListFF_FindByBestClass,
         SANListParams(AClass, vIdx, @vIdx)
        )
      then
        Exit(True);

      AClass := AClass.ClassParent;
      Inc(vIdx);
    until AClass = TObject;

  finally
    FDynClssList.UnlockList;
  end;//t..f

  Result := False;
end;

function TSANDynamicClsInfoList.ExistsByClass(const AClass: TClass): boolean;
var
  vDummy : TObject;
begin
  Result := FindByClass(vDummy, AClass);
end;

function TSANDynamicClsInfoList.ExtractByClass(var ADynamicClsInfo;
  const ABestClass : TClass): boolean;
var
  vList : TList;
  vInfo : TObject;
begin
  vList := FDynClssList.LockList;
  try

    Result := SANListFind(vInfo, vList, SANListFF_FindByBestClass, SANListParams(ABestClass));

    if Result then
      TObject(ADynamicClsInfo) := vList.Extract(vInfo);

  finally
    FDynClssList.UnlockList;
  end;//t..f
end;

function TSANDynamicClsInfoList.ExtractByClass(
  const ABestClass : TClass): TSANDynamicClsInfo;
begin
  if not ExtractByClass(Result, ABestClass) then
    Result := nil;
end;

function TSANDynamicClsInfoList.RemoveByClass(const ABestClass : TClass): boolean;
var
  vInfo : TObject;
begin
  Result := ExtractByClass(vInfo, ABestClass);

  if Result then
    FreeAndNil(vInfo);
end;

/// <summary>
/// Сортировка по убыванию уровня иерархии класса
/// </summary>
function CompareClassesLevel(Item1, Item2 : Pointer) : integer;
var
  P1, P2 : TSANDynamicClsInfo;
begin
  P1 := TSANDynamicClsInfo(Item1);
  P2 := TSANDynamicClsInfo(Item2);

  Result := -CompareValue( P1.FBaseClassLevel, P2.FBaseClassLevel);
end;

function TSANDynamicClsInfoList.TryAdd(var ADynamicClsInfo; const ABestClass: TClass): boolean;
var
  vList : TList;
begin

  Result := Assigned(ABestClass) and not ExistsByBestClass(ABestClass);
  if not Result then
    Exit;

  TObject(ADynamicClsInfo) := TSANDynamicClsInfo.Create(ABestClass);

  vList := FDynClssList.LockList;
  try
    vList.Add(TObject(ADynamicClsInfo));

    FDynClssList.Add( TObject(ADynamicClsInfo) );

    vList.Sort(CompareClassesLevel);
  finally
    FDynClssList.UnlockList;
  end;

end;

function TSANDynamicClsInfoList.TryAdd(const ABestClass: TClass): TSANDynamicClsInfo;
begin
  if not TryAdd(Result, ABestClass) then
    Result := nil;
end;

function TSANDynamicClsInfoList.Add(const ABestClass: TClass): TSANDynamicClsInfo;
begin
  if not TryAdd(Result, ABestClass) then
    if Assigned(ABestClass) then
      raise ESANDynamicCls.CreateResFmt(@sBaseClassAlreadyAddedErrorMessage, [ABestClass.ClassName])

    else
      raise ESANDynamicCls.CreateRes(@sBaseClassNotAssignedErrorMessage)
end;

procedure TSANDynamicClsInfoList.EnumDynClsInfos(
  const AEnumProc: TSANDynClsListEnumInfosProc; AParam: Pointer);
var
  i : integer;
begin
  with FDynClssList.LockList do
  try
    for i := 0 to Count - 1 do
      AEnumProc(TSANDynamicClsInfo(Items[i]), AParam);
  finally
    FDynClssList.UnlockList;
  end;
end;

procedure TSANDynamicClsInfoList.EnumDynClsInfos(
  const AEnumProc: TSANDynClsListEnumInfosObjProc; AParam: Pointer);
var
  i : integer;
begin
  with FDynClssList.LockList do
  try
    for i := 0 to Count - 1 do
      AEnumProc(TSANDynamicClsInfo(Items[i]), AParam);
  finally
    FDynClssList.UnlockList;
  end;
end;
//==============================================================================

END.

