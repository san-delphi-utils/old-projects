UNIT SANRepositoryExpertsUtils;//////////////////////////////////////////////////
// Набор классов для построения экспертов репозитория
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  ToolsAPI,
  Windows, SysUtils, Classes, Dialogs;
//==============================================================================
CONST
  STATIC_CREATOR_EXISTING     = $0001;
  STATIC_CREATOR_UNNAMED      = $0002;
  STATIC_CREATOR_MAIN_FORM    = $0004;
  STATIC_CREATOR_SHOW_FORM    = $0008;
  STATIC_CREATOR_SHOW_SOURCE  = $0010;

  STATIC_CREATOR_DEFAULT_FLAGS = STATIC_CREATOR_SHOW_FORM or STATIC_CREATOR_SHOW_SOURCE or STATIC_CREATOR_UNNAMED;
//==============================================================================
TYPE
  TSAN_CreatorType = (ctUnit, ctForm, ctText);
//==============================================================================
  // Бозовый объект файла модуля. DFM или PAS.
  TSAN_Static_File = class( TInterfacedObject, IOTAFile )
  private
    FSource : string;
    FAge : TDateTime;

  protected
    // IOTAFile
    function GetSource: string;
    function GetAge: TDateTime;

  public
    constructor Create(const ASource : string; const AAge : TDateTime = -1);
  end;//TSAN_Static_File
//==============================================================================
  // Бозовый объект создания навого модуля.
  TSAN_Default_Creator = class( TNotifierObject, IOTACreator, IOTAModuleCreator )
  protected
    { IOTACreator }
    function GetCreatorType: string;  dynamic;
    { Return False if this is a new module }
    function GetExisting: Boolean;  dynamic;
    { Return the File system IDString that this module uses for reading/writing }
    function GetFileSystem: string;  dynamic;
    { Return the Owning module, if one exists (for a project module, this would
      be a project; for a project this is a project group) }
    function GetOwner: IOTAModule;  dynamic;
    { Return true, if this item is to be marked as un-named.  This will force the
      save as dialog to appear the first time the user saves. }
    function GetUnnamed: Boolean;  dynamic;

    { IOTAModuleCreator }
    { Create and return the Implementation source for this module. (C++ .cpp }
    function GetAncestorName: string;  dynamic;
    function GetImplFileName: string;  dynamic;
    function GetIntfFileName: string;  dynamic;
    function GetFormName: string;  dynamic;
    function GetMainForm: Boolean;  dynamic;
    function GetShowForm: Boolean;  dynamic;
    function GetShowSource: Boolean;  dynamic;
    function NewFormFile( const FormIdent, AncestorIdent: string ): IOTAFile;  dynamic;
    function NewImplSource( const ModuleIdent, FormIdent, AncestorIdent: string ): IOTAFile;  dynamic;
    function NewIntfSource( const ModuleIdent, FormIdent, AncestorIdent: string ): IOTAFile;  dynamic;
    procedure FormCreated( const FormEditor: IOTAFormEditor );  dynamic;

  end;//TSAN_Default_Creator
//==============================================================================
  // Объект создания навого модуля, исолзующий файлы TSAN_Static_File
  TSAN_Static_Creator = class(TSAN_Default_Creator)
  private
    FReplaceStringsTemplate : TStrings;

    FCreatorType : TSAN_CreatorType;
    FNewFormFile_Source,  FNewImplSource_Source, FFormName, FFileName, FAncestorName  : string;
    FNewFormFile_Age,     FNewImplSource_Age     : TDateTime;

  protected
    function GetCreatorType: string;  override;

    function DoNewFileOrSource(ASource : string; const AFormIdent, AAncestorIdent: string; const AAge : TDateTime; const AModuleIdent : string = ''): IOTAFile;

    function GetAncestorName: string; override;
    function GetImplFileName: string; override;
    function GetFormName: string; override;

    function NewFormFile(const FormIdent, AncestorIdent: string ): IOTAFile; override;
    function NewImplSource(const ModuleIdent, FormIdent, AncestorIdent: string ): IOTAFile; override;


  public
    constructor Create(const ACreatorType : TSAN_CreatorType; const ABaseName, AAncestorName, AFormNameFormat, AUnitNameFormat,
      ANewFormFile_Source, ANewImplSource_Source : string;
      const AReplaceStringsTemplate : TStrings;
      const ANewFormFile_Age : TDateTime = -1; const ANewImplSource_Age : TDateTime = -1);

    destructor Destroy; override;

    class function CreateModule(const ACreatorType : TSAN_CreatorType; const ABaseName, AAncestorName, AFormNameFormat, AUnitNameFormat,
      ANewFormFile_Source, ANewImplSource_Source : string;
      const AReplaceStringsTemplate : TStrings;
      const ANewFormFile_Age : TDateTime = -1; const ANewImplSource_Age : TDateTime = -1) : IOTAModule;

  end;//TSAN_Static_Creator
//==============================================================================
  // Базовый объект эксперта репозитория.
  TSAN_Repository_Expert = class(TNotifierObject, IOTAWizard, IOTARepositoryWizard, IOTAFormWizard,
    IOTARepositoryWizard60,
    IOTARepositoryWizard80
    )
  protected
    //From IOTAWizard
    function GetIDString: string; virtual; abstract;
    function GetName: string; virtual; abstract;
    function GetState: TWizardState; dynamic;
    procedure Execute; virtual; abstract;

    //From IOTARepositoryWizard, IOTAFormWizard
    function GetAuthor: string; dynamic;
    function GetComment: string; virtual; abstract;
    function GetPage: string; virtual; abstract;
    function GetGlyph: Cardinal; dynamic;

    // IOTARepositoryWizard60
    function GetDesigner: string; dynamic;

    // IOTARepositoryWizard80
    function GetGalleryCategory: IOTAGalleryCategory; dynamic;
    function GetPersonality: string; dynamic;

    // Имя ресурса, из которого загружается иконка
    function GetGlyphResName : string; virtual;

  end;//TSAN_Repository_Expert
//==============================================================================
  TSAN_RepositoryResource_Expert = class(TSAN_Repository_Expert)
  private
    FInstance : Cardinal;

  public
    constructor Create(const AInstance : Cardinal); virtual;

    function TryReadTextFromRes(const AResName : string; var AText : string) : boolean;
    function ReadTextFromRes(const AResName : string; const ADefault : string = '') : string;

  end;//TSAN_RepositoryResource_Expert
//==============================================================================

//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================

//==============================================================================
// TSAN_Static_File
//==============================================================================
// protected
//==============================================================================
// IOTAFile
//==============================================================================
function TSAN_Static_File.GetSource: string;
begin
  Result := FSource;
end;//TSAN_Static_File.GetSource
//==============================================================================

function TSAN_Static_File.GetAge: TDateTime;
begin
  Result := FAge;
end;//TSAN_Static_File.GetAge
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSAN_Static_File.Create(const ASource : string; const AAge : TDateTime);
begin
  inherited Create;

  FSource  := ASource;
  FAge     := AAge;
end;//TSAN_Static_File.Create
//==============================================================================

//==============================================================================
// TSAN_Default_Creator
//==============================================================================
// protected
//==============================================================================
// IOTACreator
//==============================================================================
function TSAN_Default_Creator.GetCreatorType: string;
begin
  Result := sForm;
end;//TSAN_Default_Creator.GetCreatorType
//==============================================================================
function TSAN_Default_Creator.GetExisting: Boolean;
begin
  Result := False;
end;//TSAN_Default_Creator.GetExisting
//==============================================================================
function TSAN_Default_Creator.GetFileSystem: string;
begin
  Result := '';
end;//TSAN_Default_Creator.GetFileSystem
//==============================================================================
function TSAN_Default_Creator.GetOwner: IOTAModule;
var
  ModuleServices: IOTAModuleServices;
  Module: IOTAModule;
  NewModule: IOTAModule;
begin
  // You may prefer to return the project group's ActiveProject instead
  Result := nil;
  ModuleServices := ( BorlandIDEServices as IOTAModuleServices );
  Module := ModuleServices.CurrentModule;

  if Module <> nil then  begin
    if Module.QueryInterface( IOTAProject, NewModule ) = S_OK then
      Result := NewModule

    else if Module.OwnerModuleCount > 0 then  begin
      NewModule := Module.OwnerModules[0];
      if NewModule <> nil then
        if NewModule.QueryInterface( IOTAProject, Result ) <> S_OK then
          Result := nil;
    end;//else if
  end;//if

end;//TSAN_Default_Creator.GetOwner
//==============================================================================
function TSAN_Default_Creator.GetUnnamed: Boolean;
begin
  Result := True;
end;//TSAN_Default_Creator.GetUnnamed
//==============================================================================
// IOTAModuleCreator
//==============================================================================
function TSAN_Default_Creator.GetAncestorName: string;
begin
  Result := '';
end;//TSAN_Default_Creator.GetAncestorName
//==============================================================================
function TSAN_Default_Creator.GetFormName: string;
begin
  Result := '';
end;//TSAN_Default_Creator.GetFormName
//==============================================================================
function TSAN_Default_Creator.GetImplFileName: string;
begin
  Result := '';
end;//TSAN_Default_Creator.GetImplFileName
//==============================================================================
function TSAN_Default_Creator.GetIntfFileName: string;
begin
  Result := '';
end;//TSAN_Default_Creator.GetIntfFileName
//==============================================================================
function TSAN_Default_Creator.GetMainForm: Boolean;
begin
  Result := False;
end;//TSAN_Default_Creator.GetMainForm
//==============================================================================
function TSAN_Default_Creator.GetShowForm: Boolean;
begin
  Result := True;
end;//TSAN_Default_Creator.GetShowForm
//==============================================================================
function TSAN_Default_Creator.GetShowSource: Boolean;
begin
  Result := True;
end;//TSAN_Default_Creator.GetShowSource
//==============================================================================
function TSAN_Default_Creator.NewFormFile(const FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  Result := nil;
end;//TSAN_Default_Creator.NewFormFile
//==============================================================================
function TSAN_Default_Creator.NewImplSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  Result := nil;
end;//TSAN_Default_Creator.NewImplSource
//==============================================================================
function TSAN_Default_Creator.NewIntfSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
  Result := nil;
end;//TSAN_Default_Creator.NewIntfSource
//==============================================================================
procedure TSAN_Default_Creator.FormCreated(const FormEditor: IOTAFormEditor);
begin
end;//TSAN_Default_Creator.FormCreated
//==============================================================================


//==============================================================================
// TSAN_Static_Creator
//==============================================================================
// protected
//==============================================================================
function TSAN_Static_Creator.GetCreatorType: string;
begin
  case FCreatorType of
    ctUnit: Result := sUnit;
    ctForm: Result := sForm;
    else    Result := sText;
  end;//case
end;//TSAN_Static_Creator.GetCreatorType
//==============================================================================
function TSAN_Static_Creator.DoNewFileOrSource( ASource : string;
  const AFormIdent, AAncestorIdent: string; const AAge : TDateTime;
  const AModuleIdent : string): IOTAFile;
const
  RSL_PRFX   : string = '$(';
  RSL_PSTFX  : string = ')';
//------------------------------------------------------------------------------
var
  vRSL : TStrings;
//------------------------------------------------------------------------------
  procedure AddFirstRplcValue(const AName, AValue : string);
  begin
    vRSL.Insert(0, AName + '=' + AValue);
  end;//AddFirstRplcValue
//------------------------------------------------------------------------------
var
  i : integer;
begin

  if ASource = '' then
    Exit;

  vRSL := TStringList.Create;
  try
    vRSL.Assign(FReplaceStringsTemplate);

    AddFirstRplcValue('AncestorIdent',  AAncestorIdent);
    AddFirstRplcValue('FormIdent',      AFormIdent);
    AddFirstRplcValue('ModuleIdent',  AModuleIdent);

    for i := 0 to vRSL.Count - 1 do
      ASource := StringReplace(
        ASource,
        RSL_PRFX + vRSL.Names[i] + RSL_PSTFX,
        vRSL.ValueFromIndex[i],
        [rfReplaceAll, rfIgnoreCase]
      );

    Result := TSAN_Static_File.Create(ASource, AAge);
  finally
    FreeAndNil(vRSL);
  end;//t..f
end;//TSAN_Static_Creator.DoNewFileOrSource
//==============================================================================
function TSAN_Static_Creator.GetAncestorName: string;
begin
  Result := FAncestorName;
end;//TSAN_Static_Creator.GetAncestorName
//==============================================================================
function TSAN_Static_Creator.GetImplFileName: string;
begin
  Result := FFileName;
end;//TSAN_Static_Creator.GetImplFileName
//==============================================================================
function TSAN_Static_Creator.GetFormName: string;
begin
  Result := FFormName;
end;//TSAN_Static_Creator.GetFormName
//==============================================================================
function TSAN_Static_Creator.NewFormFile(const FormIdent, AncestorIdent: string): IOTAFile;
begin
//  ShowMessageFmt('%s %s %s', ['NewFormFile', FormIdent, AncestorIdent]);
  Result := DoNewFileOrSource(FNewFormFile_Source, FormIdent, AncestorIdent, FNewFormFile_Age);
end;//TSAN_Static_Creator.NewFormFile
//==============================================================================
function TSAN_Static_Creator.NewImplSource(const ModuleIdent, FormIdent,
  AncestorIdent: string): IOTAFile;
begin
//  ShowMessageFmt('%s %s %s %s', ['NewImplSource', ModuleIdent, FormIdent, AncestorIdent]);
  Result := DoNewFileOrSource(FNewImplSource_Source, FormIdent, AncestorIdent, FNewImplSource_Age, ModuleIdent);
end;//TSAN_Static_Creator.NewImplSource
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSAN_Static_Creator.Create(const ACreatorType : TSAN_CreatorType; const ABaseName, AAncestorName,
  AFormNameFormat, AUnitNameFormat, ANewFormFile_Source, ANewImplSource_Source : string;
  const AReplaceStringsTemplate : TStrings;
  const ANewFormFile_Age, ANewImplSource_Age : TDateTime);
var
  vUnitIdent, vClassName, vFileName : string;
begin
  inherited Create;

  FReplaceStringsTemplate := TStringList.Create;
  if Assigned(AReplaceStringsTemplate) then
    FReplaceStringsTemplate.Assign(AReplaceStringsTemplate);

  FCreatorType := ACreatorType;

  (BorlandIDEServices as IOTAModuleServices).GetNewModuleAndClassName(ABaseName, vUnitIdent, vClassName, vFileName);

  FFormName := Format(AFormNameFormat, [ABaseName]);
  FFileName := ExtractFilePath(vFileName) + Format(AUnitNameFormat, [ABaseName]) + '.pas';

  FAncestorName := AAncestorName;

  FNewFormFile_Source    := ANewFormFile_Source;
  FNewImplSource_Source  := ANewImplSource_Source;
  FNewFormFile_Age       := ANewFormFile_Age;
  FNewImplSource_Age     := ANewImplSource_Age;
end;//TSAN_Static_Creator.Create
//==============================================================================
destructor TSAN_Static_Creator.Destroy;
begin
  FreeAndNil(FReplaceStringsTemplate);

  inherited;
end;//TSAN_Static_Creator.Destroy
//==============================================================================
class function TSAN_Static_Creator.CreateModule(const ACreatorType : TSAN_CreatorType;
  const ABaseName, AAncestorName, AFormNameFormat, AUnitNameFormat,
  ANewFormFile_Source, ANewImplSource_Source : string;
  const AReplaceStringsTemplate : TStrings;
  const ANewFormFile_Age, ANewImplSource_Age : TDateTime): IOTAModule;
begin

  Result := ( BorlandIDEServices as IOTAModuleServices ).CreateModule(
    Self.Create(
      ACreatorType,
      ABaseName,
      AAncestorName,
      AFormNameFormat,
      AUnitNameFormat,
      ANewFormFile_Source,
      ANewImplSource_Source,
      AReplaceStringsTemplate,
      ANewFormFile_Age,
      ANewImplSource_Age
    )
  );

end;//TSAN_Static_Creator.CreateModule
//==============================================================================


//==============================================================================
// TSAN_Repository_Expert
//==============================================================================
// protected
//==============================================================================
// IOTAWizard
//==============================================================================
function TSAN_Repository_Expert.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;//TSAN_Repository_Expert.GetState
//==============================================================================
// IOTAFormWizard
//==============================================================================
function TSAN_Repository_Expert.GetAuthor: string;
begin
  Result := 'SAN';
end;//TSAN_Repository_Expert.GetAuthor
//==============================================================================
function TSAN_Repository_Expert.GetGlyph: Cardinal;
begin
  Result := LoadIcon(HInstance, PChar( GetGlyphResName ));
end;//TSAN_Repository_Expert.GetGlyph
//==============================================================================
// IOTARepositoryWizard60
//==============================================================================
function TSAN_Repository_Expert.GetDesigner: string;
begin
  Result := dVCL;
end;//TSAN_Repository_Expert.GetDesigner
//==============================================================================
// IOTARepositoryWizard80
//==============================================================================
function TSAN_Repository_Expert.GetGalleryCategory: IOTAGalleryCategory;
begin
  Result := (BorlandIDEServices as IOTAGalleryCategoryManager).FindCategory('Borland.Delphi.New.Expert');
end;//TSAN_Repository_Expert.GetGalleryCategory
//==============================================================================
function TSAN_Repository_Expert.GetPersonality: string;
begin
  Result := sDelphiPersonality;
end;//TSAN_Repository_Expert.GetPersonality
//==============================================================================
//==============================================================================
function TSAN_Repository_Expert.GetGlyphResName: string;
begin
  Result := '';
end;//TSAN_Repository_Expert.GetGlyphResName
//==============================================================================




//==============================================================================
// TSAN_RepositoryResource_Expert
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSAN_RepositoryResource_Expert.Create(const AInstance : Cardinal);
begin
  inherited Create;

  FInstance := AInstance;
end;//TSAN_RepositoryResource_Expert.Create
//==============================================================================
function TSAN_RepositoryResource_Expert.TryReadTextFromRes(const AResName: string;
  var AText: string): boolean;
// Прочитать текст из ресурса с именем AResName
var
  vText : AnsiString;
  vResInstance : THandle;
  HRes : HRSRC;
begin
  vResInstance := FindResourceHinstance(FInstance);
  HRes := FindResource(vResInstance, PWideChar(AResName), RT_RCDATA);

  Result := (HRes <> 0);
  if not Result then
    Exit;

  vText := PAnsiChar(
    LockResource(
      LoadResource(vResInstance, HRes)
    )
  );

  SetLength(vText, SizeOfResource(vResInstance, HRes));

  AText := String(vText);
end;//TSAN_RepositoryResource_Expert.TryReadTextFromRes
//==============================================================================
function TSAN_RepositoryResource_Expert.ReadTextFromRes(const AResName,
  ADefault: string): string;
begin
  if not TryReadTextFromRes(AResName, Result) then
    Result := ADefault;
end;//TSAN_RepositoryResource_Expert.ReadTextFromRes
//==============================================================================

END.
