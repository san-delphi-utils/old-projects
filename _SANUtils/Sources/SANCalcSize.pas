{*******************************************************}
{                                                       }
{       SANCalcSize                                     }
{       Вычисление размеров данных с учетом размера     }
{       указателей.                                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANCalcSize;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes, Contnrs;

TYPE
  /// <summary>
  /// Все объекты поддерживающие интерфейс IDDOMCalcSize возвращают функцией CalcSize
  /// размер занимаемый динамическими данными.
  /// </summary>
  ISANCalcSize = interface(IInterface)
    ['{A347DAFD-B1E3-46D8-8114-7020BC61CE6E}']

    function CalcSize : Int64;
  end;

  /// <summary>
  /// Тип: Функция высчитывающая размер переданного указателя.
  /// </summary>
  TSANGetSizeFunc = function (const APtr : Pointer) : Int64;

/// <summary>
/// Рассчет объема памяти строки string.
/// </summary>
function SANCalcSizeString(const AStr : string) : Int64;

/// <summary>
/// Рассчет объема памяти строки WideString.
/// </summary>
function SANCalcSizeWideString(const AWideString : WideString) : Int64;

/// <summary>
/// Рассчет объема памяти строки AnsiString.
/// </summary>
function SANCalcSizeAnsiString(const AAnsiString : AnsiString) : Int64;

/// <summary>
/// Рассчет объема памяти строки UnicodeString.
/// </summary>
function SANCalcSizeUnicodeString(const AUnicodeString : UnicodeString) : Int64;

/// <summary>
/// Рассчет полного объема памяти InstanceSize + CalcSize для объекта AObj.
/// </summary>
function SANCalcSizeObj(const AObj : TObject) : Int64;

/// <summary>
/// Рассчет полного объема памяти InstanceSize + размер списка указателей для списка AList.
/// </summary>
function SANCalcSizeLinksList(const AList : TList) : Int64;

/// <summary>
/// Рассчет полного объема памяти InstanceSize + CalcSize для простого списка объектов AList.
/// </summary>
function SANCalcSizeOwnerList(const AList : TObjectList) : Int64; overload;

/// <summary>
/// Рассчет полного объема памяти InstanceSize + CalcSize для простого списка указателей AList,
/// отвечающего за унечтожение элементов с фиксированным размером AItemSize.
/// </summary>
function SANCalcSizeOwnerList(const AList : TList; const AItemSize : Int64) : Int64; overload;

/// <summary>
/// Рассчет полного объема памяти InstanceSize + CalcSize для простого списка указателей AList,
/// отвечающего за унечтожение элементов с переменным размером, высчитываемым функцией AGetSizeFunc.
/// </summary>
function SANCalcSizeOwnerList(const AList : TList; const AGetSizeFunc : TSANGetSizeFunc) : Int64; overload;

/// <summary>
/// Функция высчитывающая размер переданного указателя.
/// Указатель - объект.
/// </summary>
function SANGetSizeFunc_ObjInPtr(const APtr : Pointer) : Int64;

IMPLEMENTATION

function SANCalcSizeString(const AStr : string) : Int64;
begin
  Result := StringElementSize(AStr)*Length(AStr);
end;

function SANCalcSizeWideString(const AWideString : WideString) : Int64;
begin
  Result := StringElementSize(AWideString)*Length(AWideString);
end;

function SANCalcSizeAnsiString(const AAnsiString : AnsiString) : Int64;
begin

  Result := StringElementSize(AAnsiString)*Length(AAnsiString);
end;

function SANCalcSizeUnicodeString(const AUnicodeString : UnicodeString) : Int64;
begin
  Result := StringElementSize(AUnicodeString)*Length(AUnicodeString);
end;

function SANCalcSizeObj(const AObj : TObject) : Int64;
var
  vCSI : ISANCalcSize;
begin
  Result := AObj.InstanceSize;

  if AObj.GetInterface(ISANCalcSize, vCSI) then
    Inc(Result, vCSI.CalcSize);
end;

function SANCalcSizeLinksList(const AList : TList) : Int64;
begin
  Result := AList.InstanceSize + AList.Capacity * 4;
end;

function SANCalcSizeOwnerList(const AList : TObjectList) : Int64;
var
  i : integer;
begin
  Result := SANCalcSizeLinksList(AList);

  for i := 0 to AList.Count - 1 do
    Inc(Result, SANCalcSizeObj(AList[i]));
end;

function SANCalcSizeOwnerList(const AList : TList; const AItemSize : Int64) : Int64;
begin
  Result := SANCalcSizeLinksList(AList) + AList.Count * AItemSize;
end;

function SANCalcSizeOwnerList(const AList : TList; const AGetSizeFunc : TSANGetSizeFunc) : Int64; overload;
var
  i : integer;
begin
  Result := SANCalcSizeLinksList(AList);

  for i := 0 to AList.Count - 1 do
    Inc(Result, AGetSizeFunc(AList[i]));
end;

function SANGetSizeFunc_ObjInPtr(const APtr : Pointer) : Int64;
begin
  Result := SANCalcSizeObj( TObject(APtr) );
end;



END.
