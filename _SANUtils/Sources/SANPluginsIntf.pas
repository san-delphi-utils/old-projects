{*******************************************************}
{                                                       }
{       SANPluginsIntf                                  }
{       Интерфейсы и общие типы плагинов в              }
{       "пакетах-как-DLL"                               }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPluginsIntf;
{$I NX.INC}
INTERFACE

USES
  SANPkgAsDllIntf;

TYPE
  ISANPlgPlugin = interface(ISANVersionInfo)
  ['{A2750A94-82BC-4A3F-8E8C-F23688C3D2B5}']
    procedure Init(const AInitParams : IInterface); safecall;
    procedure Done(const ADoneParams : IInterface); safecall;
  end;

  ISANPlgRegistratorPlugin = interface(IInterface)
  ['{0792E55A-EBBD-418E-8572-95460045B577}']
    function RegisterPlugin(const APlugin: ISANPlgPlugin) : boolean; safecall;
    function UnRegisterPlugin(const APluginGUID: TGUID) : boolean; safecall;
  end;

  ISANPlgServer = interface;

  ISANPlgPlugins = interface(IInterface)
  ['{2753D384-E675-4F24-A0EE-B83FD1818374}']
    function GetServer : ISANPlgServer; safecall;
    function GetCount: Integer; safecall;
    function GetItems(const AIndex: Integer): ISANPlgPlugin; safecall;

    /// <summary>
    ///   Очистка.
    /// </summary>
    procedure Clear; safecall;

    function TryFindByGUID(const AGUID : TGUID; out APlugin : ISANPlgPlugin) : WordBool; safecall;
    function FindByGUID(const AGUID : TGUID) : ISANPlgPlugin; safecall;
    function ExistsByGUID(const AGUID : TGUID) : WordBool; safecall;
    function RemoveByGUID(const AGUID : TGUID) : WordBool; safecall;

    property Server : ISANPlgServer  read GetServer;
    property Count: Integer read GetCount;
    property Items[const AIndex: Integer]: ISANPlgPlugin read GetItems; default;
  end;

  /// <summary>
  ///   Серверный интерфейс обработчика событий.
  /// </summary>
  ISANPlgServerEvents = interface(ISANPkgAsDllsServerEvents)
  ['{98CA0BCA-850B-4B3C-8739-08563EA5035A}']

    /// <summary>
    ///   Выбор действия при дублировании GUID версий плагинов.
    /// </summary>
    procedure PluginsGUIDsDuplicated(const ASender : ISANPlgServer;
      const AOldPlugin, ANewPlugin : ISANPlgPlugin; var AAction : TSANPkgAsDllDuplicateGUIDAction); safecall;

  end;

  /// <summary>
  ///   Сервер плагинов.
  /// </summary>
  ISANPlgServer = interface(ISANPkgAsDllsServer)
  ['{E055411C-4F0C-44EA-B204-B5C5A44A0812}']
    function GetPlugins : ISANPlgPlugins; safecall;
    function GetEvents : ISANPlgServerEvents; safecall;
    procedure SetEvents(const AEvents : ISANPlgServerEvents); safecall;

    /// <summary>
    ///   Список плагинов.
    /// </summary>
    property Plugins : ISANPlgPlugins  read GetPlugins;

    /// <summary>
    ///   События сервера.
    /// </summary>
    property Events : ISANPlgServerEvents  read GetEvents  write SetEvents;

  end;


IMPLEMENTATION

END.
