{*******************************************************}
{                                                       }
{       SANLogs                                         }
{       Логирование. Клиентский модуль.                 }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANLogs;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes,
  SANPkgAsDllServer,
  SANLogsIntf, SANLogsBase;

// From SANLogsIntf
TYPE
  ISANLogNamed                  = SANLogsIntf.ISANLogNamed;
  ISANLogProvideHInstace        = SANLogsIntf.ISANLogProvideHInstace;
  ISANLogRecord                 = SANLogsIntf.ISANLogRecord;
  ISANLogRecordsEnumirator      = SANLogsIntf.ISANLogRecordsEnumirator;
  ISANLogRecordsFilter          = SANLogsIntf.ISANLogRecordsFilter;
  ISANLogRecordsNotifier        = SANLogsIntf.ISANLogRecordsNotifier;
  ISANLogRecords                = SANLogsIntf.ISANLogRecords;
  ISANCustomLog                 = SANLogsIntf.ISANCustomLog;
  ISANLogPathPart               = SANLogsIntf.ISANLogPathPart;
  ISANLogPathPartsEnumirator    = SANLogsIntf.ISANLogPathPartsEnumirator;
  ISANLogPathNotifier           = SANLogsIntf.ISANLogPathNotifier;
  ISANLogPath                   = SANLogsIntf.ISANLogPath;
  ISANLogNotifier               = SANLogsIntf.ISANLogNotifier;
  ISANLog                       = SANLogsIntf.ISANLog;
  ISANLogInfo                   = SANLogsIntf.ISANLogInfo;
  ISANLogInfosEnumirator        = SANLogsIntf.ISANLogInfosEnumirator;
  ISANLogsEnumirator            = SANLogsIntf.ISANLogsEnumirator;
  ISANLogsStorageNotifier       = SANLogsIntf.ISANLogsStorageNotifier;
  ISANLogsSet                   = SANLogsIntf.ISANLogsSet;
  ISANLogsStorage               = SANLogsIntf.ISANLogsStorage;
  ISANLogsStoragesEnumirator    = SANLogsIntf.ISANLogsStoragesEnumirator;
  ISANLogsStoragesListNotifier  = SANLogsIntf.ISANLogsStoragesListNotifier;
  ISANLogsStoragesList          = SANLogsIntf.ISANLogsStoragesList;
  ISANLogsManager               = SANLogsIntf.ISANLogsManager;

// From SANLogsBase
CONST
  LMK_INFO     = SANLogsBase.LMK_INFO;
  LMK_WARNING  = SANLogsBase.LMK_WARNING;
  LMK_ERROR    = SANLogsBase.LMK_ERROR;
  LMK_FATAL    = SANLogsBase.LMK_FATAL;

TYPE
  ESANLogError = SANLogsBase.ESANLogError;

  /// <summary>
  ///  Менеджер логов и хранилищ
  /// </summary>
  TSANLogsManager = class(TSANCustomLogsManager)
  private
    FPkgAsDllsServer : TSANPkgAsDllsServer;

    function GetLogByName(const ALogName: string): ISANLog;

  protected
    function Get_HInstace : LongWord; override; safecall;

  public
    constructor Create(const AName : string);
    destructor Destroy; override;


    /// <summary>
    /// Получить интерфейс перечислителя логов. Пока существует перечислитель,
    /// внутренний список логов заблокирован.
    /// </summary>
    /// <returns>
    /// ISANLogsEnumirator. Перечислитель списка логов.
    /// </returns>
    function LogsEnumiration : ISANLogsEnumirator;

    /// <summary>
    /// Очистка списка логов.
    /// </summary>
    procedure LogsClear;

    /// <summary>
    /// Поиск лога по имени.
    /// </summary>
    /// <param name="ALog">
    /// Найденый лог, если поиск успешен.
    /// </param>
    /// <param name="ALogName">
    /// Имя лога.
    /// </param>
    /// <returns>
    /// Boolean. True, поиск успешен.
    /// </returns>
    function LogsFindByName(out ALog : ISANLog; const ALogName : string) : boolean;

    /// <summary>
    /// Извлечь лог из набора.
    /// </summary>
    /// <param name="ALogName">
    /// Имя лога.
    /// </param>
    /// <returns>
    /// ISANLog. Извлеченная запись или nil в случае неудачи.
    /// </returns>
    function LogsExtractByName(const ALogName : string) : ISANLog;

    /// <summary>
    /// Удалить лог из набора.
    /// </summary>
    /// <param name="ALogName">
    /// Имя лога.
    /// </param>
    /// <returns>
    /// Boolean. True, удаление успешно.
    /// </returns>
    function LogsRemoveByName(const ALogName : string) : boolean;

    /// <summary>
    /// Запросить имена используемых логов.
    /// </summary>
    /// <param name="AUsedLogsNames">
    /// Заполняемый список строк
    /// </param>
    procedure UsedLogsNamesQuery(const AUsedLogsNames : TStrings);

    /// <summary>
    /// Подключиться к логу. Создать, если не создан.
    /// </summary>
    /// <param name="ALogName">
    /// Имя лога.
    /// </param>
    /// <param name="APathSetup">
    /// Интерфейс настройки пути к логу.
    /// </param>
    /// <param name="AStorageName">
    /// Имя хранилища. Если не задано, берется первое хранилище.
    /// </param>
    /// <returns>
    /// ISANLog. Интерфейс лога.
    /// </returns>
    function AttachLog(const ALogName : string; const APathSetup : ISANLogPathSetup;
      const AStorageName : string = '') : ISANLog;

    /// <summary>
    /// Общее число логов у менеджера.
    /// </summary>
    property LogsCount : integer  read Get_LogsCount;

    /// <summary>
    /// Получение лога по индексу.
    /// </summary>
    property Logs[const Index : integer] : ISANLog  read Get_Logs;

    /// <summary>
    /// Получение лога по имени.
    /// </summary>
    property LogByName[const ALogName : string] : ISANLog  read GetLogByName; default;

    /// <summary>
    /// Список подключенных хранилищь логов.
    /// </summary>
    property Storages : ISANLogsStoragesList  read Get_Storages;

    /// <summary>
    /// Управление пакетами.
    /// </summary>
    property PkgAsDllsServer : TSANPkgAsDllsServer  read FPkgAsDllsServer;

  end;

/// <summary>
/// Инициализация синглтона менеджера логов
/// </summary>
/// <param name="AAutoFinalize">
/// Включает автофинализацию при выгрузке модуля. При FALSE необходимо вручную
/// вызывать LogsManagerFinalize, а если не вызвать - поднимается исключение.
/// </param>
procedure LogsManagerInitialize(const AAutoFinalize : boolean = True);

/// <summary>
/// Финализация синглтона менеджера логов
/// </summary>
procedure LogsManagerFinalize;

/// <summary>
/// Доступ к синглтону менеджера логов
/// </summary>
/// <param name="ARaiseNotAssignedError">
/// Поднять исключение, если не инициализирован.
/// </param>
/// <returns>
/// TSANLogsManager. Менеджер логов.
/// </returns>
function LogsManager(const ARaiseNotAssignedError : boolean = True) : TSANLogsManager;

IMPLEMENTATION

uses
  SANListUtils;

resourcestring
  sLogNotFoundErrorMessage                = 'Лог с именем «%s» не найден.';
  sLogsManagerNotInitializedErrorMessage  = 'Менеджер логов не инициализирован.';
  sLogsManagerNotFinalizeErrorMessage     = 'Менеджер логов не финализирован.';
  sEmptyLogsStoragesErrorMessage          = 'Нет ни одного хранилища логов.';
  sStorageNotFoundErrorMessage            = 'Не найдено хранилище с именем «%s».';
(*
  sFilterProcNotAssignedErrorMessage      = 'Для фильтрации списка записей лога не задана фильтрующая процедура.';
  sSecondCallBeginWriteErrorMessage       = 'Запись в лог с именем «%s» уже начата.';
  sNotCallBeginWriteErrorMessage          = 'Запись в лог с именем «%s» еще не началась. Нет предшествующего вызова BeginWrite.';
  sSecondCreationTryErrorMessage          = 'Попытка повторного создания лога с именем «%s».';
  sEmptyParhErrorMessage                  = 'Задан пустой идентификационный путь.';
  sSecondStorageRegTryErrorMessage        = 'Попытка повторной регистрации хранилища логов с именем «%s».';
  sGetStorageFromDLLFailErrorMessage      = 'Не удается получить хранилище из DLL.';
  sStorageNotAssignedErrorMessage         = 'Для лога не задано хранилище.';
*)

//==============================================================================
// TSANLogsManager
// private
function TSANLogsManager.GetLogByName(const ALogName: string): ISANLog;
begin
  if not(inherited FindByName(Result, ALogName)) then
    raise ESANLogError.CreateResFmt(@sLogNotFoundErrorMessage, [ALogName]);
end;

// protected
function TSANLogsManager.Get_HInstace: LongWord;
begin
  Result := HInstance;
end;

// public
constructor TSANLogsManager.Create(const AName: string);
begin
  inherited Create(AName);

  FPkgAsDllsServer := TSANPkgAsDllsServer.CreateOwned(LOGS_DLL_ENTRY_POINT_NAME, Self);
end;

destructor TSANLogsManager.Destroy;
begin
  LogsClear;

  FreeAndNil(FPkgAsDllsServer);

  inherited;
end;

function TSANLogsManager.LogsEnumiration: ISANLogsEnumirator;
begin
  Result := inherited Enumiration;
end;

procedure TSANLogsManager.LogsClear;
begin
  inherited Clear;
end;

function TSANLogsManager.LogsFindByName(out ALog: ISANLog; const ALogName: string): boolean;
begin
  Result := inherited FindByName(ALog, ALogName);
end;

function TSANLogsManager.LogsExtractByName(const ALogName: string): ISANLog;
begin
  Result := inherited ExtractByName(ALogName);
end;

function TSANLogsManager.LogsRemoveByName(const ALogName: string): boolean;
begin
  Result := inherited RemoveByName(ALogName);
end;

procedure TSANLogsManager.UsedLogsNamesQuery(const AUsedLogsNames: TStrings);
var
  vEnum : ISANLogsEnumirator;
  i : integer;
begin
  vEnum := LogsEnumiration;

  AUsedLogsNames.BeginUpdate;
  try

    for i := 0 to vEnum.Count - 1 do
      AUsedLogsNames.Add(vEnum[i].Name);

  finally
    AUsedLogsNames.EndUpdate;
  end;
end;

function TSANLogsManager.AttachLog(const ALogName: string;
  const APathSetup : ISANLogPathSetup; const AStorageName: string): ISANLog;

  procedure SelectStorage(out AStorage : ISANLogsStorage);
  begin
    if AStorageName = '' then
      begin

        if Storages.Count = 0 then
          raise ESANLogError.CreateRes(@sEmptyLogsStoragesErrorMessage);

        AStorage := Storages[0];
      end
    else
      if not Storages.FindByName(AStorage, AStorageName) then
        raise ESANLogError.CreateResFmt(@sStorageNotFoundErrorMessage, [AStorageName]);
  end;

var
  vStorage : ISANLogsStorage;
begin

  if FindByName(Result, ALogName) then
    Exit;

  SelectStorage(vStorage);

  Result := SANLogsStorageAttachLog(vStorage, ALogName, APathSetup);
end;
//==============================================================================

var
  vLogsManager : TSANLogsManager = nil;
  vAutoFinalize : boolean = False;

procedure LogsManagerInitialize(const AAutoFinalize : boolean);
begin
  if not Assigned(vLogsManager) then
    vLogsManager := TSANLogsManager.Create('DefaultLogsManager');

  vAutoFinalize := AAutoFinalize;
end;

procedure LogsManagerFinalize;
begin
  FreeAndNil(vLogsManager);
end;

function LogsManager(const ARaiseNotAssignedError : boolean) : TSANLogsManager;
begin

  if ARaiseNotAssignedError and not Assigned(vLogsManager) then
    raise ESANLogError.CreateRes(@sLogsManagerNotInitializedErrorMessage);

  Result := vLogsManager;
end;

INITIALIZATION

FINALIZATION
  if Assigned(vLogsManager) then
    if vAutoFinalize then
      LogsManagerFinalize
    else
      raise ESANLogError.CreateRes(@sLogsManagerNotFinalizeErrorMessage);

END.

