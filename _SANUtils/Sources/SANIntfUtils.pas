{*******************************************************}
{                                                       }
{       SANIntfUtils                                    }
{       Работа с интерфейсами и их базовыми классами    }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANIntfUtils;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, ActiveX, ComObj;

TYPE
  EIntfUtils = class(Exception);

  TWideStringArray = array [0..MaxInt div SizeOf(WideString) - 1] of WideString;
  PWideStringArray = ^TWideStringArray;

  TVarDataArray = array[0..MaxInt div SizeOf(TVarData) - 1] of TVarData;
  PVarDataArray = ^TVarDataArray;

  TPSAPackFunc = function (out APSA : PSafeArray; const AData : Pointer) : HResult;
  TPSAUnPackFunc = function (const APSA : PSafeArray; const AData : Pointer) : HResult;

  /// <summary>
  /// Интерфейс с доступным для чтения счетчиком ссылок.
  /// </summary>
  ISANRefCount = interface(IInterface)
    ['{524A296F-7198-4277-8DC0-1C4EF93727AE}']
    function GetRefCount : integer; stdcall;
    /// <summary>
    /// Значение счетчика ссылок.
    /// </summary>
    property RefCount : integer  read GetRefCount;
  end;

  /// <summary>
  /// Интерфейс именованого объект.
  /// </summary>
  ISANNamed = interface(IInterface)
    ['{ED656DEF-128B-46D6-A4BC-74AB3728616F}']

    function Get_Name : WideString; safecall;
    /// <summary>
    /// Имя объекта.
    /// </summary>
    property Name : WideString  read Get_Name;
  end;

  /// <summary>
  /// Интерфейс предоставления hInstace расположения объекта.
  /// </summary>
  ISANProvideHInstace = interface(IInterface)
    ['{5133C0F1-18F1-4B0C-8866-42392EBA7557}']
    function Get_HInstace : LongWord; safecall;
    /// <summary>
    /// hInstace расположения объекта.
    /// </summary>
    property HInstace : LongWord  read Get_HInstace;
  end;


  /// <summary>
  /// TInterfacedObject с виртуальными методами IInterface.
  /// </summary>
  TSANVirtIntfObject = class(TInterfacedObject, ISANRefCount)
  protected
    // IInterface
    function QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function _AddRef: Integer; virtual; stdcall;
    function _Release: Integer; virtual; stdcall;

    // ISANRefCountInterface
    function GetRefCount : integer; stdcall;
  end;

  TSANChildAutoIntfObject = class;

  /// <summary>
  /// Прототип процидуры изменения счетчика ссылок родительского объекта.
  /// </summary>
  TSANChildIntfObjParentRefProc = procedure (const AParent : TObject);

  /// <summary>
  /// Дочерний объект интерфейса. Имеет ссылку на !ОБЪЕКТ! владельца,
  /// при увеличении счетчика ссылок повышает счетчик ссылок владельца,
  /// при уменьшении счетчика ссылок понижает счетчик ссылок владельца.
  /// </summary>
  TSANChildIntfObject = class(TSANVirtIntfObject)
  private
    FParent : TObject;
    FParentAddRef, FParentRelease : TSANChildIntfObjParentRefProc;

  protected
    /// <summary>
    /// Повысить счетчик ссылок свой и родительский.
    /// </summary>
    function AddRefWithParent : integer;
    /// <summary>
    /// Понизить счетчик ссылок свой и родительский.
    /// </summary>
    function ReleaseWithParent : integer;
    /// <summary>
    /// Повысить счетчик ссылок объекта, не изменяя родительский.
    /// </summary>
    function AddRefNotParent : integer;
    /// <summary>
    /// Понизить счетчик ссылок объекта, не изменяя родительский.
    /// </summary>
    function ReleaseNotParent : integer;

    function _AddRef: Integer; override; stdcall;
    function _Release: Integer; override; stdcall;

    /// <summary>
    /// Родительский объект, для которого изменяется значение счетчика ссылок.
    /// </summary>
    property Parent : TObject read FParent;

  public
    /// <summary>
    /// Конструктор для родительского объекта одного из классов
    /// TInterfacedObject, TInterfacedPersistent или TSANVirtIntfObject.
    /// Если передан объект другого класса - поднимается исключение.
    /// </summary>
    /// <param name="AParent">
    /// Родительский объект, для которого будет изменяться значение счетчика ссылок.
    /// </param>
    constructor Create(const AParent : TObject);
    /// <summary>
    /// Специальный конструктор для нестандартных классов родительского объекта.
    /// Примечание: для классов родителя TInterfacedObject, TInterfacedPersistent, TComponent, TSANVirtIntfObject
    /// уже есть конструкторы CreateIntfObjChild, CreateIntfPersChild, CreateComponentChild, CreateVirtIntfObjChild,
    /// соответственно.
    /// </summary>
    /// <param name="AParent">
    /// Родительский объект, для которого будет изменяться значение счетчика ссылок.
    /// </param>
    /// <param name="AParentAddRef">
    /// Процедура повышения значения счетчика ссылок родителя, соответствующая прототипу TSANChildIntfObjParentRefProc.
    /// </param>
    /// <param name="AParentRelease">
    /// Процедура понижения значения счетчика ссылок родителя, соответствующая прототипу TSANChildIntfObjParentRefProc.
    /// </param>
    constructor CreateSpecial(const AParent : TObject;
      const AParentAddRef, AParentRelease : TSANChildIntfObjParentRefProc);
  end;

  /// <summary>
  /// Дочерний объект интерфейса автоматизации. Имеет ссылку на !ОБЪЕКТ! владельца,
  /// при увеличении счетчика ссылок повышает счетчик ссылок владельца,
  /// при уменьшении счетчика ссылок понижает счетчик ссылок владельца.
  /// </summary>
  TSANChildAutoIntfObject = class(TAutoIntfObject, ISANRefCount)
  private
    FParent : TObject;
    FParentAddRef, FParentRelease : TSANChildIntfObjParentRefProc;

  protected
    /// <summary>
    /// Повысить счетчик ссылок свой и родительский.
    /// </summary>
    function AddRefWithParent : integer;
    /// <summary>
    /// Понизить счетчик ссылок свой и родительский.
    /// </summary>
    function ReleaseWithParent : integer;
    /// <summary>
    /// Повысить счетчик ссылок объекта, не изменяя родительский.
    /// </summary>
    function AddRefNotParent : integer;
    /// <summary>
    /// Понизить счетчик ссылок объекта, не изменяя родительский.
    /// </summary>
    function ReleaseNotParent : integer;

    // IInterface
    function QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function _AddRef: Integer; virtual; stdcall;
    function _Release: Integer; virtual; stdcall;

    // ISANRefCountInterface
    function GetRefCount : integer; stdcall;

    /// <summary>
    /// Родительский объект, для которого изменяется значение счетчика ссылок.
    /// </summary>
    property Parent : TObject read FParent;

  public
    /// <summary>
    /// Конструктор для родительского объекта одного из классов
    /// TInterfacedObject, TInterfacedPersistent или TSANVirtIntfObject.
    /// Если передан объект другого класса - поднимается исключение.
    /// </summary>
    /// <param name="AParent">
    /// Родительский объект, для которого будет изменяться значение счетчика ссылок.
    /// </param>
    constructor Create(const TypeLib: ITypeLib; const DispIntf: TGUID; const AParent : TObject);
    /// <summary>
    /// Специальный конструктор для нестандартных классов родительского объекта.
    /// Примечание: для классов родителя TInterfacedObject, TInterfacedPersistent, TComponent, TSANVirtIntfObject
    /// уже есть конструкторы CreateIntfObjChild, CreateIntfPersChild, CreateComponentChild, CreateVirtIntfObjChild,
    /// соответственно.
    /// </summary>
    /// <param name="AParent">
    /// Родительский объект, для которого будет изменяться значение счетчика ссылок.
    /// </param>
    /// <param name="AParentAddRef">
    /// Процедура повышения значения счетчика ссылок родителя, соответствующая прототипу TSANChildIntfObjParentRefProc.
    /// </param>
    /// <param name="AParentRelease">
    /// Процедура понижения значения счетчика ссылок родителя, соответствующая прототипу TSANChildIntfObjParentRefProc.
    /// </param>
    constructor CreateSpecial(const TypeLib: ITypeLib; const DispIntf: TGUID;
      const AParent : TObject;
      const AParentAddRef, AParentRelease : TSANChildIntfObjParentRefProc);
  end;

  /// <summary>
  /// Абстрактный перечислитель для TThreadList. При создании объекта список
  /// блкируется, при унечтожении разблокируется.
  /// </summary>
  TSANCustomThreadListEnumirator = class(TSANChildIntfObject)
  private
    FThListRef : TThreadList;
    FLockListRef : TList;

  protected
    /// <summary>
    /// Ссылка на внутренний список заблокированного TThreadList-а.
    /// </summary>
    property LockListRef : TList  read FLockListRef;

  public
    /// <summary>
    /// Простая версия конструктора.
    /// </summary>
    /// <param name="AThListRef">
    /// Блокируемый многопоточный список, для элементов которого производится перечисление.
    /// </param>
    /// <param name="AParent">
    /// Родительский объект, для которого будет изменяться значение счетчика ссылок.
    /// </param>
    constructor Create(const AThListRef : TThreadList; const AParent : TObject);
    /// <summary>
    /// Специальная версия конструктора.
    /// </summary>
    /// <param name="AThListRef">
    /// Блокируемый многопоточный список, для элементов которого производится перечисление.
    /// </param>
    /// <param name="AParent">
    /// Родительский объект, для которого будет изменяться значение счетчика ссылок.
    /// </param>
    /// <param name="AParentAddRef">
    /// Процедура повышения значения счетчика ссылок родителя, соответствующая прототипу TSANChildIntfObjParentRefProc.
    /// </param>
    /// <param name="AParentRelease">
    /// Процедура понижения значения счетчика ссылок родителя, соответствующая прототипу TSANChildIntfObjParentRefProc.
    /// </param>
    /// <seealso>
    /// TSANChildIntfObject.CreateSpecial.
    /// </seealso>
    constructor CreateSpecial(const AThListRef : TThreadList; const AParent : TObject;
      const AParentAddRef, AParentRelease : TSANChildIntfObjParentRefProc);
    destructor Destroy; override;
  end;

  /// <summary>
  /// Интерфейс абстрактного набора перехватчиков событий
  /// </summary>
  ISANCustomNotifiers = interface(IInterface)
    ['{EBA434C6-9696-441D-A0C2-9D826200C9EF}']

    function GetCount : integer; safecall;
    /// <summary>
    /// Количество элементов.
    /// </summary>
    property Count : integer  read GetCount;

    function GetNotifiers(const Index : integer) : IInterface; safecall;
    /// <summary>
    /// Элемент по индексу.
    /// </summary>
    property Notifiers[const Index : integer] : IInterface  read GetNotifiers; default;
  end;

  /// <summary>
  /// Интерфейс перечислителя перехватчиков.
  /// </summary>
  ISANNotifiersEnumirator = interface(ISANCustomNotifiers)
    ['{D0EDCBF5-2C1C-415B-971D-3FB454F52955}']
  end;


  /// <summary>
  /// Интерфейс набора перехватчиков событий.
  /// </summary>
  ISANNotifiers = interface(ISANCustomNotifiers)
    ['{5DFAC53A-7C2D-4344-8B73-D96248093F95}']

    /// <summary>
    /// Очистка списка.
    /// </summary>
    procedure Clear; safecall;
    /// <summary>
    /// Поиск по идентификатеру.
    /// </summary>
    /// <param name="ANotifier">
    /// Найденый интерфейс, при успешном поиске.
    /// </param>
    /// <param name="ANotifierID">
    /// Идентификатор полученый функцией AddNotifier.
    /// </param>
    /// <returns>
    /// WordBool. True, поиск успешен.
    /// </returns>
    function FindNotifier(out ANotifier : IInterface; const ANotifierID : integer) : WordBool; safecall;
    /// <summary>
    /// Удаление по идентификатеру.
    /// </summary>
    /// <param name="ANotifierID">
    /// Идентификатор полученый функцией AddNotifier.
    /// </param>
    /// <returns>
    /// WordBool. True, удаление успешно.
    /// </returns>
    function RemoveNotifier(const ANotifierID : integer) : WordBool; safecall;
    /// <summary>
    /// Добавление интерфейса оповещения о событиях.
    /// </summary>
    /// <param name="ANotifier">
    /// Добавляемый интерфейс.
    /// </param>
    /// <returns>
    /// Integer. Идентификатор интерфейса в списке для функций FindNotifier и RemoveNotifier.
    /// </returns>
    function AddNotifier(const ANotifier : IInterface) : integer; safecall;
    /// <summary>
    /// Создать перечисление.
    /// </summary>
    /// <returns>
    /// ISANNotifiersEnumirator. Интерфейс перечисления.
    /// </returns>
    function Enumiration : ISANNotifiersEnumirator; safecall;
  end;

  /// <summary>
  /// Перечислитель интерфейсов перехватчиков событий
  /// </summary>
  TSANCustomNotifiersEnumirator = class(TSANCustomThreadListEnumirator, ISANNotifiersEnumirator)
  protected
    // ISANNotifiersEnumirator
    function GetCount : integer; safecall;
    function GetNotifiers(const Index : integer) : IInterface; safecall;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

  end;

  /// <summary>
  /// Набор интерфейсов перехватчиков событий
  /// </summary>
  TSANCustomNotifiers = class(TSANChildIntfObject, ISANNotifiers)
  protected
    type
      TNotifierItem = record
        Notifier : IInterface;
        ID : integer;
      end;
      PNotifierItem = ^TNotifierItem;

  private
    FIDCounter : integer;
    FNotifiers : TThreadList;

  protected
    procedure DoClearItem(const APtr : Pointer);
    function DoFindItem(const AItem, AValue : Pointer) : boolean;

    // ISANNotifiers
    function GetCount : integer; safecall;
    function GetNotifiers(const Index : integer) : IInterface; safecall;
    procedure Clear; safecall;
    function FindNotifier(out ANotifier : IInterface; const ANotifierID : integer) : WordBool; safecall;
    function RemoveNotifier(const ANotifierID : integer) : WordBool; safecall;
    function AddNotifier(const ANotifier : IInterface) : integer; safecall;
    function Enumiration : ISANNotifiersEnumirator; safecall;

    /// <summary>
    /// Многопоточный список записей с информацией об интерфейсах и их идентификаторах
    /// </summary>
    property Notifiers : TThreadList  read FNotifiers;

  public
    /// <summary>
    /// Простая версия конструктора.
    /// </summary>
    /// <param name="AThListRef">
    /// Блокируемый многопоточный список, для элементов которого производится перечисление.
    /// </param>
    /// <param name="AParent">
    /// Родительский объект, для которого будет изменяться значение счетчика ссылок.
    /// </param>
    constructor Create(const AParent : TObject);
    /// <summary>
    /// Специальная версия конструктора.
    /// </summary>
    /// <param name="AThListRef">
    /// Блокируемый многопоточный список, для элементов которого производится перечисление.
    /// </param>
    /// <param name="AParent">
    /// Родительский объект, для которого будет изменяться значение счетчика ссылок.
    /// </param>
    /// <param name="AParentAddRef">
    /// Процедура повышения значения счетчика ссылок родителя, соответствующая прототипу TSANChildIntfObjParentRefProc.
    /// </param>
    /// <param name="AParentRelease">
    /// Процедура понижения значения счетчика ссылок родителя, соответствующая прототипу TSANChildIntfObjParentRefProc.
    /// </param>
    /// <seealso>
    /// TSANChildIntfObject.CreateSpecial.
    /// </seealso>
    constructor CreateSpecial(const AParent : TObject;
      const AParentAddRef, AParentRelease : TSANChildIntfObjParentRefProc);
    destructor Destroy; override;
  end;

// Разработчики модуля ActiveX почему-то забыли импортировать функцию
// SafeArrayGetVartype, поэтому делаем это сами
function SafeArrayGetVartype(psa: PSafeArray; out vt: TVarType): HResult; stdcall;
  external 'oleaut32.dll';

/// <summary>
/// Повысить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TInterfacedObject.
/// </param>
procedure InterfacedObjectAddRef(const AObj : TObject);
/// <summary>
/// Понизить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TInterfacedObject.
/// </param>
procedure InterfacedObjectRelease(const AObj : TObject);

/// <summary>
/// Повысить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TInterfacedPersistent.
/// </param>
procedure InterfacedPersistentAddRef(const AObj : TObject);
/// <summary>
/// Понизить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TInterfacedPersistent.
/// </param>
procedure InterfacedPersistentRelease(const AObj : TObject);

/// <summary>
/// Повысить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TComponent.
/// </param>
procedure ComponentAddRef(const AObj : TObject);
/// <summary>
/// Понизить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TComponent.
/// </param>
procedure ComponentRelease(const AObj : TObject);

/// <summary>
/// Повысить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TComObject.
/// </param>
procedure ComObjectAddRef(const AObj : TObject);
/// <summary>
/// Понизить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TComObject.
/// </param>
procedure ComObjectRelease(const AObj : TObject);

/// <summary>
/// Повысить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TSANVirtIntfObject.
/// </param>
procedure SANVirtIntfObjectAddRef(const AObj : TObject);
/// <summary>
/// Понизить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TSANVirtIntfObject.
/// </param>
procedure SANVirtIntfObjectRelease(const AObj : TObject);

/// <summary>
/// Повысить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TSANChildAutoIntfObject.
/// </param>
procedure SANChildAutoIntfObjectAddRef(const AObj : TObject);
/// <summary>
/// Понизить значение счетчика ссылок на объект.
/// </summary>
/// <param name="AObj">
/// Объект наследник TSANChildAutoIntfObject.
/// </param>
procedure SANChildAutoIntfObjectRelease(const AObj : TObject);


/// <summary>
/// Для объекта получить пару известных процедур управления счетчиком ссылок.
/// Для nil - возвращаетя nil, nil. Для неизвестного класса поднимается исключение EIntfUtils.
/// </summary>
/// <param name="AObj">
/// Объект, для которого подбираются процедуры.
/// </param>
/// <param name="AAddRefProc">
/// Процедура повышения счетчика ссылок.
/// </param>
/// <param name="AReleaseProc">
/// Процедура понижения счетчика ссылок.
/// </param>
procedure GetAddRefAndReleaseProcs(const AObj : TObject; out AAddRefProc, AReleaseProc : TSANChildIntfObjParentRefProc);

/// <summary>
/// Освободить ссылку объекта интерфейса.
/// </summary>
/// <param name="APtr">
/// Указатель TInterfacedObject.
/// </param>
procedure ReleaseIntfObj(const APtr : Pointer);
/// <summary>
/// Освободить ссылку дочернего объекта интерфейса,
/// не затрагивая родительский счетчик ссылок.
/// </summary>
/// <param name="APtr">
/// Указатель TInterfacedObject.
/// </param>
procedure ReleaseIntfObjChildOnly(const APtr : Pointer);
/// <summary>
/// Освободить ссылку интерфейса.
/// </summary>
/// <param name="APtr">
/// Указатель IInterface.
/// </param>
procedure ReleaseIntf(const APtr : Pointer);
/// <summary>
/// Освободить ссылку объекта интерфейса. Задать Nil-указатель в переменную.
/// </summary>
/// <param name="AIntfObject">
/// Указатель TInterfacedObject.
/// </param>
procedure ReleaseAndNil(var AIntfObject);

/// <summary>
/// Запаковать в вариантную переменную данные в виде безопастного массива.
/// </summary>
/// <param name="AOleVar">
/// Результат запаковки безопастного массива в вариантную переменную.
/// </param>
/// <param name="APackFunc">
/// Функция запаковки.
/// </param>
/// <param name="AData">
/// Указатель источника данных для запаковки.
/// </param>
/// <returns>
/// HRESULT. Код ошибки.
/// </returns>
function PSAPackDataToOleVariant(out AOleVar : OleVariant;
  const APackFunc : TPSAPackFunc; const AData : Pointer) : HResult;
/// <summary>
/// Получить из вариантной переменной данные в виде безопастного массива.
/// </summary>
/// <param name="AOleVar">
/// Безопасный массив запакованый в вариантную переменную.
/// При удачном извлечении переменная очищается.
/// </param>
/// <param name="AUnPackFunc">
/// Функция извлечения.
/// </param>
/// <param name="AData">
/// Указатель для сохранения извеченных данных.
/// </param>
/// <returns>
/// HRESULT. Код ошибки.
/// </returns>
function PSAUnPackDataFromOleVariant(var AOleVar : OleVariant;
  const AUnPackFunc : TPSAUnPackFunc; const AData : Pointer) : HResult;
/// <summary>
/// Перенос безопасного массива из одной вариантной переменной в другую.
/// </summary>
/// <param name="AOldOleVar">
/// Старое хранилище безопасного массива.
/// </param>
/// <param name="ANewOleVar">
/// Новое хранилище безопасного массива.
/// </param>
procedure PSAMoveToOleVariant(var AOldOleVar : OleVariant; out ANewOleVar : OleVariant);
/// <summary>
/// Задать строковое значение вариантной записи.
/// </summary>
/// <param name="AVarData">
/// Изменяемая запись.
/// </param>
/// <param name="AStr">
/// Строковое значение.
/// </param>
/// <returns>
/// HRESULT. Код ошибки.
/// </returns>
function VarDataOleStr(var AVarData : TVarData; const AStr : string) : HResult;
/// <summary>
/// Функция запаковки списка строк в безопасный массив.
/// </summary>
/// <param name="ASafeArr">
/// Безопасный массив для запаковки.
/// </param>
/// <param name="AData">
/// Указатель объекта, наследника TStrings.
/// </param>
/// <returns>
/// HRESULT. Код ошибки.
/// </returns>
function PSAStringsToSafeArray(out ASafeArr : PSafeArray; const AData : Pointer) : HResult;
/// <summary>
/// Функция распаковки списка строк из безопасного массива.
/// </summary>
/// <param name="ASafeArr">
/// Безопасный массив для распаковки.
/// </param>
/// <param name="AData">
/// Указатель объекта, наследника TStrings.
/// </param>
/// <returns>
/// HRESULT. Код ошибки.
/// </returns>
function PSAStringsFromSafeArray(const ASafeArr : PSafeArray; const AData : Pointer) : HResult;

/// <summary>
/// Функция поиска по имени для списка объектов, поддерживающих интерфейс ISANNamed.
/// </summary>
/// <param name="AItem">
/// Проверяемый элемент списка, объект поддерживающих интерфейс ISANNamed.
/// </param>
/// <param name="AValue">
/// Значение с которым сравнивается параметр, указатель на строку.
/// </param>
/// <returns>
/// Boolean. True, если AItem "подходит" для поиска по AValue.
/// </returns>
function SANListFF_SANNamedFindByName(const AItem, AValue : Pointer) : boolean;

IMPLEMENTATION

uses
  SANListUtils;

resourcestring
  sUnknownIntfObjectClassErrorMessage      = 'Класс "%s" не имеет известных процедур управления счетчиком ссылок';
  sReleaseIntfObjInvalidClassErrorMessage  = 'Класс "%s" не поддерживается функциями ReleaseIntfObj, ReleaseAndNil';

type
  TInterfacedObjectHack      = class(TInterfacedObject);
  TInterfacedPersistentHack  = class(TInterfacedPersistent);
  TComponentHack             = class(TComponent);

//==============================================================================
// Процедуры TSANChildIntfObjParentRefProc
procedure InterfacedObjectAddRef(const AObj : TObject);
begin
  TInterfacedObjectHack(AObj)._AddRef;
end;

procedure InterfacedObjectRelease(const AObj : TObject);
begin
  TInterfacedObjectHack(AObj)._Release;
end;

procedure InterfacedPersistentAddRef(const AObj : TObject);
begin
  TInterfacedPersistentHack(AObj)._AddRef;
end;

procedure InterfacedPersistentRelease(const AObj : TObject);
begin
  TInterfacedPersistentHack(AObj)._Release;
end;

procedure ComponentAddRef(const AObj : TObject);
begin
  TComponentHack(AObj)._AddRef;
end;

procedure ComponentRelease(const AObj : TObject);
begin
  TComponentHack(AObj)._Release;
end;

procedure ComObjectAddRef(const AObj : TObject);
begin
  TComObject(AObj).ObjAddRef;
end;

procedure ComObjectRelease(const AObj : TObject);
begin
  TComObject(AObj).ObjRelease;
end;

procedure SANVirtIntfObjectAddRef(const AObj : TObject);
begin
  TSANVirtIntfObject(AObj)._AddRef;
end;

procedure SANVirtIntfObjectRelease(const AObj : TObject);
begin
  TSANVirtIntfObject(AObj)._Release;
end;

procedure SANChildAutoIntfObjectAddRef(const AObj : TObject);
begin
  TSANChildAutoIntfObject(AObj)._AddRef;
end;

procedure SANChildAutoIntfObjectRelease(const AObj : TObject);
begin
  TSANChildAutoIntfObject(AObj)._Release;
end;
//==============================================================================
procedure GetAddRefAndReleaseProcs(const AObj : TObject; out AAddRefProc, AReleaseProc : TSANChildIntfObjParentRefProc);
begin

  if not Assigned(AObj) then
    begin
      AAddRefProc  := nil;
      AReleaseProc := nil;
    end

  else
  if AObj is TSANChildAutoIntfObject then
    begin
      AAddRefProc   := SANChildAutoIntfObjectAddRef;
      AReleaseProc  := SANChildAutoIntfObjectRelease;
    end

  else
  if AObj is TSANVirtIntfObject then
    begin
      AAddRefProc   := SANVirtIntfObjectAddRef;
      AReleaseProc  := SANVirtIntfObjectRelease;
    end

  else
  if AObj is TComponent then
    begin
      AAddRefProc   := ComponentAddRef;
      AReleaseProc  := ComponentRelease;
    end

  else
  if AObj is TComObject then
    begin
      AAddRefProc   := ComObjectAddRef;
      AReleaseProc  := ComObjectRelease;
    end

  else
  if AObj is TInterfacedPersistent then
    begin
      AAddRefProc   := InterfacedPersistentAddRef;
      AReleaseProc  := InterfacedPersistentRelease;
    end

  else
  if AObj is TInterfacedObject then
    begin
      AAddRefProc   := InterfacedObjectAddRef;
      AReleaseProc  := InterfacedObjectRelease;
    end

  else
    raise EIntfUtils.CreateResFmt(@sUnknownIntfObjectClassErrorMessage, [AObj.ClassName]);

end;
//==============================================================================
procedure ReleaseIntfObj(const APtr : Pointer);
var
  vObj : TObject;
begin
  vObj := TObject(APtr);

  if vObj is TSANChildAutoIntfObject then
    TSANChildAutoIntfObject(vObj)._Release

  else
  if vObj is TSANVirtIntfObject then
    TSANVirtIntfObject(vObj)._Release

  else
  if vObj is TComponent then
    TComponentHack(vObj)._Release

  else
  if vObj is TComObject then
    TComObject(vObj).ObjRelease

  else
  if vObj is TInterfacedPersistent then
    TInterfacedPersistentHack(vObj)._Release

  else
  if vObj is TInterfacedObject then
    TInterfacedObjectHack(vObj)._Release

  else
    raise EIntfUtils.CreateResFmt(@sReleaseIntfObjInvalidClassErrorMessage, [vObj.ClassName]);

end;

procedure ReleaseIntfObjChildOnly(const APtr : Pointer);
var
  vObj : TSANChildIntfObject;
begin
  vObj := TSANChildIntfObject(APtr);

  vObj.ReleaseNotParent;
end;

procedure ReleaseIntf(const APtr : Pointer);
begin
  IInterface(APtr)._Release;
end;

procedure ReleaseAndNil(var AIntfObject);
var
  vPtr : Pointer;
begin
  vPtr := Pointer(AIntfObject);

  if not Assigned(vPtr) then
    Exit;

  Pointer(AIntfObject) := nil;

  ReleaseIntfObj(vPtr);
end;
//==============================================================================

//==============================================================================
function PSAPackDataToOleVariant(out AOleVar : OleVariant;
  const APackFunc : TPSAPackFunc; const AData : Pointer) : HResult;
var
  vPSA : PSafeArray;
begin
  Result := S_OK;
  try

    if not (Assigned(@APackFunc) and Assigned(AData)) then
      Exit(E_FAIL);

    Result := APackFunc(vPSA, AData);

    if not Failed(Result) then
    begin
      PSafeArray( TVarData(AOleVar).VArray ) := vPSA;
      TVarData(AOleVar).VType := varArray;
    end;

  finally
    if Failed(Result) then
    begin
      TVarData(AOleVar).VType  := varError;
      TVarData(AOleVar).VError := Result;
    end;
  end;
end;

function PSAUnPackDataFromOleVariant(var AOleVar : OleVariant;
  const AUnPackFunc : TPSAUnPackFunc; const AData : Pointer) : HResult;
var
  vPSA : PSafeArray;
begin
  Result := S_OK;
  try

    if not (Assigned(@AUnPackFunc) and Assigned(AData)) then
      Exit(E_FAIL);

    if TVarData(AOleVar).VType <> varArray then
    begin
      VarClear(AOleVar);
      Exit(E_InvalidArg);
    end;

    vPSA := PSafeArray(TVarData(AOleVar).VArray);
    try

      Result := AUnPackFunc(vPSA, AData);

    finally
      SafeArrayDestroy(vPSA);
      TVarData(AOleVar).VArray := nil;
      TVarData(AOleVar).VType := varEmpty
    end;

  finally
    if Failed(Result) then
    begin
      TVarData(AOleVar).VType  := varError;
      TVarData(AOleVar).VError := Result;
    end;
  end;

end;

procedure PSAMoveToOleVariant(var AOldOleVar : OleVariant; out ANewOleVar : OleVariant);
begin
  TVarData(ANewOleVar).VArray := TVarData(AOldOleVar).VArray;
  TVarData(ANewOleVar).VType := varArray;

  TVarData(AOldOleVar).VArray := nil;
  TVarData(AOldOleVar).VType := varEmpty;
end;

function VarDataOleStr(var AVarData : TVarData; const AStr : string) : HResult;
var
  L : integer;
begin
  L := Length(AStr);

  AVarData.VOleStr := SysAllocStringLen(nil, L);
  if AVarData.VOleStr = nil then
    Exit(E_OutOfMemory);

  AVarData.VType := varOleStr;
  if L > 0 then
    StringToWideChar(AStr, AVarData.VOleStr, L + 1);

  Result := S_OK;
end;

function PSAStringsToSafeArray(out ASafeArr : PSafeArray; const AData : Pointer) : HResult;
var
  vStrings : TStrings;
  P: PWideStringArray;
  I: Integer;
begin
  vStrings := TStrings(AData);

  ASafeArr := SafeArrayCreateVector(varOleStr, 0, vStrings.Count);
  if ASafeArr = nil then
    Exit(E_Fail);

  Result := SafeArrayAccessData(ASafeArr, PPointer(@P)^);
  // Если произошла ошибка, очищаем массив
  if Failed(Result) then
  begin
    SafeArrayDestroy(ASafeArr);
    ASafeArr := nil;
    Exit;
  end;

  try

    // Заполняем массив
    for I := 0 to vStrings.Count - 1 do
      P^[I] := vStrings[I];

  finally

    // "Отпираем" массив
    Result := SafeArrayUnaccessData(ASafeArr);
    // Если произошла ошибка, очищаем массив
    if Failed(Result) then
    begin
      SafeArrayDestroy(ASafeArr);
      ASafeArr := nil;
    end;

  end;

end;

function PSAStringsFromSafeArray(const ASafeArr : PSafeArray; const AData : Pointer) : HResult;
var
  vStrings : TStrings;
  I, vUBound: Integer;
  P : PWideStringArray;
begin
  vStrings := TStrings(AData);

  vStrings.BeginUpdate;
  try

    if ASafeArr = nil then
      Exit(E_InvalidArg);

    try

      // Получаем верхнюю границу массива
      Result := SafeArrayGetUBound(ASafeArr, 1, vUBound);
      if Failed(Result) then
        Exit;

      // Заполняем AStrings строками, полученными от сервера
      Result := SafeArrayAccessData(ASafeArr, PPointer(@P)^);
      if Failed(Result) then
        Exit;

      try

        for I := 0 to vUBound do
          vStrings.Add(P^[I]);

      finally
        if Failed(Result) then
          SafeArrayUnaccessData(ASafeArr)
        else
          Result := SafeArrayUnaccessData(ASafeArr);
      end;

    finally
      SafeArrayDestroy(ASafeArr);
    end;

  finally
    vStrings.EndUpdate
  end;
end;
//==============================================================================

//==============================================================================
function SANListFF_SANNamedFindByName(const AItem, AValue : Pointer) : boolean;
var
  vNamed : ISANNamed;
begin
  Result := (IInterface(AItem).QueryInterface(ISANNamed, vNamed) = S_OK)
        and (CompareText(vNamed.Name, PString(AValue)^) = 0)
end;
//==============================================================================

//==============================================================================
// TSANVirtIntfObject
// protected
function TSANVirtIntfObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  Result := inherited QueryInterface(IID, Obj);
end;

function TSANVirtIntfObject._AddRef: Integer;
begin
  Result := inherited _AddRef;
end;

function TSANVirtIntfObject._Release: Integer;
begin
  Result := inherited _Release;
end;

function TSANVirtIntfObject.GetRefCount: integer;
begin
  Result := RefCount;
end;
//==============================================================================

//==============================================================================
// TSANChildIntfObject
// protected
function TSANChildIntfObject.AddRefWithParent: integer;
begin
  if Assigned(FParent) and Assigned(FParentAddRef) then
    FParentAddRef(FParent);

  Result := inherited _AddRef;
end;

function TSANChildIntfObject.ReleaseWithParent: integer;
var
  vParent : TObject;
  vParentRelease : TSANChildIntfObjParentRefProc;
begin

  if Assigned(FParent) and Assigned(FParentRelease) then
    begin
      vParent := FParent;
      vParentRelease := FParentRelease;

      Result := inherited _Release;

      vParentRelease(vParent);
    end

  else
    Result := inherited _Release;

end;

function TSANChildIntfObject.AddRefNotParent: integer;
begin
  Result := inherited _AddRef;
end;

function TSANChildIntfObject.ReleaseNotParent: integer;
begin
  Result := inherited _Release;
end;

function TSANChildIntfObject._AddRef: Integer;
begin
  Result := AddRefWithParent;
end;

function TSANChildIntfObject._Release: Integer;
begin
  Result := ReleaseWithParent;
end;

// public
constructor TSANChildIntfObject.Create(const AParent: TObject);
begin
  inherited Create;

  GetAddRefAndReleaseProcs(AParent, FParentAddRef, FParentRelease);

  FParent := AParent;
end;

constructor TSANChildIntfObject.CreateSpecial(const AParent: TObject;
  const AParentAddRef, AParentRelease: TSANChildIntfObjParentRefProc);
begin
  inherited Create;

  FParent         := AParent;
  FParentAddRef   := AParentAddRef;
  FParentRelease  := AParentRelease
end;
//==============================================================================

//==============================================================================
// TSANChildAutoIntfObject
// protected
function TSANChildAutoIntfObject.AddRefWithParent: integer;
begin
  if Assigned(FParent) and Assigned(FParentAddRef) then
    FParentAddRef(FParent);

  Result := inherited _AddRef;
end;

function TSANChildAutoIntfObject.ReleaseWithParent: integer;
var
  vParent : TObject;
  vParentRelease : TSANChildIntfObjParentRefProc;
begin

  if Assigned(FParent) and Assigned(FParentRelease) then
    begin
      vParent := FParent;
      vParentRelease := FParentRelease;

      Result := inherited _Release;

      vParentRelease(vParent);
    end

  else
    Result := inherited _Release;

end;

function TSANChildAutoIntfObject.AddRefNotParent: integer;
begin
  Result := inherited _AddRef;
end;

function TSANChildAutoIntfObject.ReleaseNotParent: integer;
begin
  Result := inherited _Release;
end;

function TSANChildAutoIntfObject.QueryInterface(const IID: TGUID;  out Obj): HResult;
begin
  Result := inherited QueryInterface(IID, Obj);
end;

function TSANChildAutoIntfObject._AddRef: Integer;
begin
  Result := AddRefWithParent;
end;

function TSANChildAutoIntfObject._Release: Integer;
begin
  Result := ReleaseWithParent;
end;

function TSANChildAutoIntfObject.GetRefCount: integer;
begin
  Result := RefCount;
end;

// public
constructor TSANChildAutoIntfObject.Create(const TypeLib: ITypeLib;
  const DispIntf: TGUID; const AParent: TObject);
begin
  inherited Create(TypeLib, DispIntf);

  GetAddRefAndReleaseProcs(AParent, FParentAddRef, FParentRelease);

  FParent := AParent;
end;

constructor TSANChildAutoIntfObject.CreateSpecial(const TypeLib: ITypeLib;
  const DispIntf: TGUID; const AParent: TObject;
  const AParentAddRef, AParentRelease: TSANChildIntfObjParentRefProc);
begin
  inherited Create(TypeLib, DispIntf);

  FParent         := AParent;
  FParentAddRef   := AParentAddRef;
  FParentRelease  := AParentRelease
end;
//==============================================================================

//==============================================================================
// TSANCustomThreadListEnumirator
// public
constructor TSANCustomThreadListEnumirator.Create(const AThListRef: TThreadList;
  const AParent: TObject);
begin
  inherited Create(AParent);

  FThListRef := AThListRef;
  FLockListRef := FThListRef.LockList;
end;

constructor TSANCustomThreadListEnumirator.CreateSpecial(const AThListRef: TThreadList;
  const AParent: TObject; const AParentAddRef, AParentRelease: TSANChildIntfObjParentRefProc);
begin
  inherited CreateSpecial(AParent, AParentAddRef, AParentRelease);

  FThListRef := AThListRef;
  FLockListRef := FThListRef.LockList;
end;

destructor TSANCustomThreadListEnumirator.Destroy;
begin
  FThListRef.UnlockList;
  inherited;
end;
//==============================================================================

//==============================================================================
// TSANCustomNotifiersEnumirator
// protected
function TSANCustomNotifiersEnumirator.GetCount: integer;
begin
  Result := LockListRef.Count;
end;

function TSANCustomNotifiersEnumirator.GetNotifiers(const Index: integer): IInterface;
begin
  Result := TSANCustomNotifiers.PNotifierItem(LockListRef[Index])^.Notifier;
end;

// public
function TSANCustomNotifiersEnumirator.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{C60E7899-05FF-4F88-BB84-1681935C8A6B}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANCustomNotifiers
// protected
procedure TSANCustomNotifiers.DoClearItem(const APtr: Pointer);
begin
  PNotifierItem(APtr)^.Notifier := nil;
  Dispose(APtr);
end;

function TSANCustomNotifiers.DoFindItem(const AItem, AValue: Pointer): boolean;
begin
  Result := PNotifierItem(AItem)^.ID = Integer(AValue);
end;

function TSANCustomNotifiers.GetCount: integer;
begin
  with FNotifiers.LockList do
  try
    Result := Count;
  finally
    FNotifiers.UnlockList;
  end;
end;

function TSANCustomNotifiers.GetNotifiers(const Index: integer): IInterface;
begin
  with FNotifiers.LockList do
  try
    Result := PNotifierItem(Items[Index])^.Notifier;
  finally
    FNotifiers.UnlockList;
  end;
end;

procedure TSANCustomNotifiers.Clear;
begin
  if Assigned(FNotifiers) then
    SANListClear(FNotifiers, DoClearItem);
end;

function TSANCustomNotifiers.FindNotifier(out ANotifier: IInterface;
  const ANotifierID: integer): WordBool;
var
  P : PNotifierItem;
begin
  Result := SANListFind(P, FNotifiers, DoFindItem, Pointer(ANotifierID));

  if Result then
    ANotifier := P^.Notifier;
end;

function TSANCustomNotifiers.RemoveNotifier(const ANotifierID: integer): WordBool;
begin
  Result := SANListRemove(FNotifiers, DoFindItem, DoClearItem, Pointer(ANotifierID));
end;

function TSANCustomNotifiers.AddNotifier(const ANotifier: IInterface): integer;
var
  P : PNotifierItem;
begin
  with FNotifiers.LockList do
  try
    Inc(FIDCounter);
    Result := FIDCounter;

    New(P);

    P^.Notifier  := ANotifier;
    P^.ID        := FIDCounter;

    Add(P);
  finally
    FNotifiers.UnlockList;
  end;
end;

function TSANCustomNotifiers.Enumiration: ISANNotifiersEnumirator;
begin
  Result := TSANCustomNotifiersEnumirator.Create(FNotifiers, Self);
end;

// public
constructor TSANCustomNotifiers.Create(const AParent: TObject);
begin
  inherited Create(AParent);

  FNotifiers := TThreadList.Create;
end;

constructor TSANCustomNotifiers.CreateSpecial(const AParent: TObject;
  const AParentAddRef, AParentRelease: TSANChildIntfObjParentRefProc);
begin
  inherited CreateSpecial(AParent, AParentAddRef, AParentRelease);

  FNotifiers := TThreadList.Create;
end;

destructor TSANCustomNotifiers.Destroy;
begin
  Clear;

  FreeAndNil(FNotifiers);

  inherited;
end;
//==============================================================================



END.
