{*******************************************************}
{                                                       }
{       SANProtocolCodecBase                            }
{       Протокол в виде сток                            }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANProtocolCodecBase;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes,
  SANMisc, SANSync,
  SANProtocol;

TYPE
  ESANProtocolCodecBaseError = class(ESANProtocolError);

  // Протокол содержащий имя команды и неанализируемый буфер данных
  // +------------------------------+-------------------------------+-------------+--------------+
  // | Длина имени команды (1 байт) | Длина буфера данных (4 байта) | Имя команды | Буфер данных |
  // +------------------------------+-------------------------------+-------------+--------------+
  TSANProtocolCodecCommandName = class(TSANCustomProtocolCodec)
  protected
    function Get_Name : WideString; override; safecall;
    procedure NewDataPackage(var ANewDataPackage; const ATemplateDataPackage: Pointer = nil); override;
    procedure FreeDataPackage(const ADataPackage: Pointer); override;
    function ReadCommandName(const ADataPackage: Pointer) : string; override;
    procedure CreateDecodingTempData(var ATempData); override;
    procedure FreeDecodingTempData(const ATempData: Pointer); override;
    procedure EncodeDataPackage(const ADataPackage: Pointer; var ABuffer : AnsiString); override;

    function StdDecodeMove(var ADecodingParams : TSANProtocolDecodingRec) : boolean; override;
    function StdDecodeChange(var ADecodingParams : TSANProtocolDecodingRec) : boolean; override;
  end;

  TSANProtocolCodecStrings = class(TSANCustomProtocolCodec, ISANProtocolCodecDynamic)
  private
    FLocker : TSANLocker;
    FLineBreak : string;

  protected
    function Get_Name : WideString; override; safecall;
    procedure NewDataPackage(var ANewDataPackage; const ATemplateDataPackage: Pointer = nil); override;
    procedure FreeDataPackage(const ADataPackage: Pointer); override;
    function ReadCommandName(const ADataPackage: Pointer) : string; override;
    procedure CreateDecodingTempData(var ATempData); override;
    procedure FreeDecodingTempData(const ATempData: Pointer); override;
    procedure EncodeDataPackage(const ADataPackage: Pointer; var ABuffer : AnsiString); override;

    function GetLocker : TSANLocker;

    function StdDecodeMove(var ADecodingParams : TSANProtocolDecodingRec) : boolean; override;
    function StdDecodeChange(var ADecodingParams : TSANProtocolDecodingRec) : boolean; override;

  public
    constructor Create(const ALineBreak : string = #13#10);
    destructor Destroy; override;

    /// <summary>
    /// Смена разделителя строк
    /// </summary>
    /// <returns>
    /// Boolean. True - разделитель строк изменен
    /// </returns>
    function ChangeLineBreak(const ALineBreak : string) : boolean;

  end;

IMPLEMENTATION

type
  TCommandNameStage = (cnsNone, cnsNameL, cnsDataL, cnsName, cnsData);
  PCommandNameTempData = ^TCommandNameTempData;
  TCommandNameTempData = record
    CommandNameLength : Byte;
    DataLength : integer;
    CommandName : AnsiString;
  end;

//==============================================================================
// TSANProtocolCodecCommandName
// protected
function TSANProtocolCodecCommandName.Get_Name: WideString;
begin
  Result := 'CommandName';
end;

procedure TSANProtocolCodecCommandName.NewDataPackage(var ANewDataPackage;
  const ATemplateDataPackage: Pointer);
var
  vStrPtr : PString;
begin
  New(vStrPtr);

  try

    if Assigned(ATemplateDataPackage) then
      vStrPtr^ := PString(ATemplateDataPackage)^
    else
      SetLength(vStrPtr^, 0);

    Pointer(ANewDataPackage) := vStrPtr;

  except
    FreeDataPackage(vStrPtr);
    raise;
  end;
end;

procedure TSANProtocolCodecCommandName.FreeDataPackage(const ADataPackage: Pointer);
begin

  if not Assigned(ADataPackage) then
    Exit;

  SetLength(PString(ADataPackage)^, 0);

  Dispose(ADataPackage);
end;

function TSANProtocolCodecCommandName.ReadCommandName(const ADataPackage: Pointer): string;
begin
  Result := PString(ADataPackage)^
end;

procedure TSANProtocolCodecCommandName.CreateDecodingTempData(var ATempData);
var
  vTempData : PCommandNameTempData;
begin
  New(vTempData);

  FillChar(vTempData^, SizeOf(TCommandNameTempData), 0);

  Pointer(ATempData) := vTempData;
end;

procedure TSANProtocolCodecCommandName.FreeDecodingTempData(const ATempData: Pointer);
begin
  if not Assigned(ATempData) then
    Exit;

  SetLength(PCommandNameTempData(ATempData)^.CommandName, 0);

  Dispose(ATempData);
end;

procedure TSANProtocolCodecCommandName.EncodeDataPackage(const ADataPackage: Pointer;
  var ABuffer: AnsiString);
var
  vCmdName, vBuffer : AnsiString;
  vCNLength : Byte;
  vBufferLength : integer;
begin
  vCmdName       := AnsiString(PString(ADataPackage)^);
  vBuffer        := ABuffer;
  vCNLength      := Length(vCmdName);
  vBufferLength  := Length(ABuffer);

  BuildBuffer
  (
    ABuffer,
    [ SizeOf(Byte),  SizeOf(Integer),     vCNLength,  vBufferLength],
    [   @vCNLength,   @vBufferLength,  @vCmdName[1],    @vBuffer[1]]
  );
end;

function TSANProtocolCodecCommandName.StdDecodeMove(
  var ADecodingParams : TSANProtocolDecodingRec) : boolean;
var
  vPTD : PCommandNameTempData;
begin
  Result := False;

  vPTD := PCommandNameTempData(ADecodingParams.TempData);

  case TCommandNameStage(ADecodingParams.Stage.StageNo) of

    cnsNameL:
      ADecodingParams.MovePtr(@vPTD^.CommandNameLength);

    cnsDataL:
      ADecodingParams.MovePtr(@vPTD^.DataLength);

    cnsName:
      ADecodingParams.MoveAnsiStr(vPTD^.CommandName);

  end;//case

end;

function TSANProtocolCodecCommandName.StdDecodeChange(
  var ADecodingParams: TSANProtocolDecodingRec): boolean;
var
  vPTD : PCommandNameTempData;
begin
  Result := False;

  vPTD := PCommandNameTempData(ADecodingParams.TempData);

  case TCommandNameStage(ADecodingParams.Stage.StageNo) of

    cnsNone:
      ADecodingParams.Stage.Left := SizeOf(Byte);

    cnsNameL:
      ADecodingParams.Stage.Left := SizeOf(Integer);

    cnsDataL:
      begin
        SetLength(vPTD^.CommandName, vPTD^.CommandNameLength);
        ADecodingParams.Stage.Left := vPTD^.CommandNameLength;
      end;

    cnsName:
      begin
        PString(ADecodingParams.DataPackage)^ := String(vPTD^.CommandName);
        Exit(True);
      end;

  end;//case

end;
//==============================================================================

type
  TStringsStage = (ssNone, ssTextL, ssText);
  PStringsTempData = ^TStringsTempData;
  TStringsTempData = record
    TextBytesLength : integer;
    TextBytes : TBytes;
  end;

//==============================================================================
// TSANProtocolCodecStrings
// protected
function TSANProtocolCodecStrings.Get_Name: WideString;
begin
  Result := 'Strings';
end;

procedure TSANProtocolCodecStrings.NewDataPackage(var ANewDataPackage;
  const ATemplateDataPackage: Pointer);
var
  vSL : TStringList;
begin
  vSL := TStringList.Create;
  try

    FLocker.CriticalSection.Enter;
    try
      vSL.LineBreak := FLineBreak;
    finally
      FLocker.CriticalSection.Leave;
    end;

    if Assigned(ATemplateDataPackage) then
      vSL.Assign(TPersistent(ATemplateDataPackage));

  except
    FreeDataPackage(vSL);
    raise;
  end;

  TObject(ANewDataPackage) := vSL;
end;

procedure TSANProtocolCodecStrings.FreeDataPackage(const ADataPackage: Pointer);
begin
  TObject(ADataPackage).Free;
end;

procedure TSANProtocolCodecStrings.CreateDecodingTempData(var ATempData);
var
  vPTD : PStringsTempData;
begin
  New(vPTD);

  Pointer(ATempData) := vPTD;
end;

procedure TSANProtocolCodecStrings.FreeDecodingTempData(const ATempData: Pointer);
begin

  if not Assigned(ATempData) then
    Exit;

  SetLength(PStringsTempData(ATempData)^.TextBytes, 0);

  Dispose(ATempData);
end;

function TSANProtocolCodecStrings.ReadCommandName(
  const ADataPackage: Pointer): string;
begin

  with TStrings(ADataPackage) do
    if Count = 0 then
      Result := ''
    else
      Result := Strings[0];

end;

procedure TSANProtocolCodecStrings.EncodeDataPackage(const ADataPackage: Pointer;
  var ABuffer: AnsiString);
var
  vBytes : TBytes;
  L : integer;
begin
  vBytes := WideBytesOf( TStrings(ADataPackage).Text );
  L := Length(vBytes);

  BuildBuffer
  (
    ABuffer,
    [ SizeOf(Integer),           L],
    [              @L,  @vBytes[0]]
  );

end;

function TSANProtocolCodecStrings.GetLocker: TSANLocker;
begin
  Result := FLocker;
end;

function TSANProtocolCodecStrings.StdDecodeMove(
  var ADecodingParams: TSANProtocolDecodingRec): boolean;
var
  vPTD : PStringsTempData;
begin
  Result := False;

  vPTD := PStringsTempData(ADecodingParams.TempData);

  case TStringsStage(ADecodingParams.Stage.StageNo) of

    ssTextL:
      ADecodingParams.MovePtr(@vPTD^.TextBytesLength);

    ssText:
      ADecodingParams.MoveBytes(vPTD^.TextBytes);
  end;//case
end;

function TSANProtocolCodecStrings.StdDecodeChange(
  var ADecodingParams: TSANProtocolDecodingRec): boolean;
var
  vPTD : PStringsTempData;
begin
  Result := False;

  vPTD := PStringsTempData(ADecodingParams.TempData);

  case TStringsStage(ADecodingParams.Stage.StageNo) of

    ssNone:
      ADecodingParams.Stage.Left := SizeOf(Integer);

    ssTextL:
      if vPTD^.TextBytesLength = 0 then
        Exit(True)
      else
        begin
          ADecodingParams.Stage.Left := vPTD^.TextBytesLength;
          SetLength(vPTD^.TextBytes, vPTD^.TextBytesLength);
        end;

    ssText:
      begin
        TStrings(ADecodingParams.DataPackage).Text := WideStringOf(vPTD^.TextBytes);
        Exit(True);
      end;

  end;//case

end;

// public
constructor TSANProtocolCodecStrings.Create(const ALineBreak: string);
begin
  inherited Create;

  FLocker := TSANLocker.Create;

  FLineBreak := ALineBreak;
end;

destructor TSANProtocolCodecStrings.Destroy;
begin
  FreeAndNil(FLocker);

  inherited;
end;

function TSANProtocolCodecStrings.ChangeLineBreak(const ALineBreak: string): boolean;
begin
  FLocker.CriticalSection.Enter;
  try

    Result := not FLocker.IsLocked(False);

    if Result then
      FLineBreak := ALineBreak;

  finally
    FLocker.CriticalSection.Leave;
  end;
end;
//==============================================================================

END.
