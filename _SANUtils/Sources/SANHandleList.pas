{*******************************************************}
{                                                       }
{       SANHandleList                                   }
{       Список, содержащий дескрипторы THandle          }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANHandleList;
{$I NX.INC}
INTERFACE

USES
  Classes;

TYPE
  TSANHandleListNotyfyEvent = procedure(Sender : TList; const AItem : THandle;
    const AAction: TListNotification) of object;
  /// <summary>
  /// Список, содержащий дескрипторы THandle
  /// </summary>
  TSANHandleList = class(TList)
  private
    FOnNotify : TSANHandleListNotyfyEvent;

  protected
    function Get(Index: Integer): THandle; inline;
    procedure Put(Index: Integer; Item: THandle); inline;
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;

  public
    function Add(Item: THandle): Integer; inline;
    function Extract(Item: THandle): THandle; inline;
    function ExtractItem(Item: THandle; Direction: TList.TDirection): THandle; inline;
    function First: THandle; inline;
    function IndexOf(Item: THandle): Integer; inline;
    function IndexOfItem(Item: THandle; Direction: TList.TDirection): Integer; inline;
    procedure Insert(Index: Integer; Item: THandle); inline;
    function Last: THandle; inline;
    function Remove(Item: THandle): Integer; inline;
    function RemoveItem(Item: THandle; Direction: TList.TDirection): Integer; inline;

    property Items[Index: Integer]: THandle read Get write Put; default;

    property OnNotify : TSANHandleListNotyfyEvent  read FOnNotify  write FOnNotify;
  end;

IMPLEMENTATION

//==============================================================================
// TSANHandleList
// protected
function TSANHandleList.Get(Index: Integer): THandle;
begin
  Result := THandle(inherited Get(Index));
end;

procedure TSANHandleList.Put(Index: Integer; Item: THandle);
begin
  inherited Put(Index, Pointer(Item));
end;

procedure TSANHandleList.Notify(Ptr: Pointer; Action: TListNotification);
begin
  if Assigned(FOnNotify) then
    FOnNotify(Self, THandle(Ptr), Action);
end;

// public
function TSANHandleList.Add(Item: THandle): Integer;
begin
  Result := inherited Add( Pointer(Item) );
end;

function TSANHandleList.Extract(Item: THandle): THandle;
begin
  Result := THandle(inherited Extract(Pointer(Item)) );
end;

function TSANHandleList.ExtractItem(Item: THandle;
  Direction: TList.TDirection): THandle;
begin
  Result := THandle(inherited ExtractItem(Pointer(Item), Direction) );
end;

function TSANHandleList.First: THandle;
begin
  Result := THandle(inherited First);
end;

function TSANHandleList.IndexOf(Item: THandle): Integer;
begin
  Result := inherited IndexOf( Pointer(Item) );
end;

function TSANHandleList.IndexOfItem(Item: THandle;
  Direction: TList.TDirection): Integer;
begin
  Result := inherited IndexOfItem( Pointer(Item), Direction);
end;

procedure TSANHandleList.Insert(Index: Integer; Item: THandle);
begin
  inherited Insert(Index, Pointer(Item))
end;

function TSANHandleList.Last: THandle;
begin
  Result := THandle(inherited Last);
end;

function TSANHandleList.Remove(Item: THandle): Integer;
begin
  Result := inherited Remove(Pointer(Item))
end;

function TSANHandleList.RemoveItem(Item: THandle;
  Direction: TList.TDirection): Integer;
begin
  Result := inherited RemoveItem(Pointer(Item), Direction)
end;
//==============================================================================


END.
