{*******************************************************}
{                                                       }
{       SANSync                                         }
{       Синхронизация данных между потоками             }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANSync;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Classes, Contnrs, SyncObjs;

TYPE
  TSANSyncQueueList = class;
  TSANSyncQueuesSet = class;

  TSANSyncQueueEvent = procedure(const Sender : TObject; const APtr : Pointer) of object;

  /// <summary>
  /// Очередь синхронизации потоков. Передает по очереди указатели
  /// из дочернего потока в главный.
  /// </summary>
  TSANSyncQueue = class(TInterfacedPersistent)
  private
    FQueue : TThreadList;
    FOwnerList : TSANSyncQueueList;
    FOnProcessItem : TSANSyncQueueEvent;

    function GetIndex: integer;

  public
    constructor Create(const AOwnerList : TSANSyncQueueList; const AOnProcessItem : TSANSyncQueueEvent);
    procedure BeforeDestruction; override;
    destructor Destroy; override;

    procedure Add(const APtr : Pointer);
    procedure Process;

    property Index : integer  read GetIndex;
    property OnProcessItem : TSANSyncQueueEvent  read FOnProcessItem  write FOnProcessItem;
  end;

  /// <summary>
  /// Список очередей синхронизации потоков.
  /// </summary>
  TSANSyncQueueList = class(TObjectList)
  private
    FQueuesSet : TSANSyncQueuesSet;

    function GetItem(const Index: Integer): TSANSyncQueue;

  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;

  public
    function Extract(Item: TSANSyncQueue): TSANSyncQueue;
    function ExtractItem(Item: TSANSyncQueue; Direction: TList.TDirection): TSANSyncQueue;
    function First: TSANSyncQueue;
    function Last: TSANSyncQueue;

    function Add(const AOnProcessItem : TSANSyncQueueEvent): TSANSyncQueue;

  public
    property Items[const Index: Integer]: TSANSyncQueue read GetItem; default;
  end;

  /// <summary>
  /// Набор очередей синхронизации потоков. Обрабатывает сообщения
  /// в главном потоке, вызывая обработку дочерней очереди.
  /// </summary>
  TSANSyncQueuesSet = class(TInterfacedPersistent)
  private
    FSyncQueueList : TSANSyncQueueList;

    FWndHandle : HWND;

  protected
    procedure ReadWndMessages(var AMessage: TMessage);

  public
    constructor Create;
    destructor Destroy; override;

    property SyncQueues : TSANSyncQueueList  read FSyncQueueList;
  end;

  /// <summary>
  /// Многопоточный замок. Блокировка, разблокировка, подсчет блокировок.
  /// </summary>
  TSANLocker = class(TObject)
  private
    FLocksCount : integer;
    FCriticalSection : TCriticalSection;
    FOnLock, FOnUnLock : TNotifyEvent;

    function GetLocksCount: integer;

  public
    constructor Create(const AOnLock : TNotifyEvent = nil; const AOnUnLock : TNotifyEvent = nil);
    destructor Destroy; override;

    /// <summary>
    /// Заблокировать
    /// </summary>
    procedure Lock;

    /// <summary>
    /// Разблокировать
    /// </summary>
    procedure UnLock;

    /// <summary>
    /// Заблокирован ли замок
    /// </summary>
    /// <param name="AUseCriticalSection">
    /// True - использовать встроенную критическую секцию
    /// </param>
    function IsLocked(const AUseCriticalSection : boolean = True): boolean;

    /// <summary>
    /// Количество блокировок
    /// </summary>
    property LocksCount : integer  read GetLocksCount;

    /// <summary>
    /// Критическая секция замка
    /// </summary>
    property CriticalSection : TCriticalSection  read FCriticalSection;

  end;

  /// <summary>
  /// Генератор целочисленных значений.
  /// </summary>
  TSANGenerator = class(TObject)
  private
    FCurValue : Cardinal;
    FCriticalSection : TCriticalSection;

  public
    constructor Create(const AStartValue : Cardinal = 0);
    destructor Destroy; override;

    function Generate(const AUseCriticalSection : boolean = True) : Cardinal;
    procedure ResetCurValue(const ANewValue : Cardinal; const AUseCriticalSection : boolean = True);

    /// <summary>
    /// Текущее значение генератора.
    /// </summary>
    property CurValue : Cardinal  read FCurValue;

    /// <summary>
    /// Критическая секция генератора.
    /// </summary>
    property CriticalSection : TCriticalSection  read FCriticalSection;
  end;

IMPLEMENTATION

//==============================================================================
// TSANSyncQueue
// private
function TSANSyncQueue.GetIndex: integer;
begin
  if Assigned(FOwnerList) then
    Result := FOwnerList.IndexOf(Self)
  else
    Result := -1;
end;

// public
constructor TSANSyncQueue.Create(const AOwnerList : TSANSyncQueueList;
  const AOnProcessItem: TSANSyncQueueEvent);
begin
  inherited Create;

  FOwnerList := AOwnerList;

  FQueue := TThreadList.Create;

  FOnProcessItem := AOnProcessItem;
end;

procedure TSANSyncQueue.BeforeDestruction;
begin
  Process;

  inherited;
end;

destructor TSANSyncQueue.Destroy;
begin
  if Assigned(FOwnerList) then
    FOwnerList.Extract(Self);

  FreeAndNil(FQueue);

  inherited;
end;

procedure TSANSyncQueue.Add(const APtr: Pointer);
begin

  with FQueue.LockList do
  try
    Add(APtr);
  finally
    FQueue.UnlockList;
  end;

  if Assigned(FOwnerList) and Assigned(FOwnerList.FQueuesSet) then
    PostMessage(FOwnerList.FQueuesSet.FWndHandle, WM_USER + 1 + Index, 0, 0);
end;

procedure TSANSyncQueue.Process;
var
  vPtr : Pointer;
begin

  with FQueue.LockList do
  try

    while Count > 0 do
    begin

      vPtr := Extract(First);

      if Assigned(FOnProcessItem) then
        FOnProcessItem(Self, vPtr);
    end;

  finally
    FQueue.UnlockList;
  end;

end;
//==============================================================================

//==============================================================================
// TSANSyncQueueList
// private
function TSANSyncQueueList.GetItem(const Index: Integer): TSANSyncQueue;
begin
  Result := TSANSyncQueue(inherited Items[Index]);
end;

// protected
procedure TSANSyncQueueList.Notify(Ptr: Pointer; Action: TListNotification);
begin

  if OwnsObjects then
    with TSANSyncQueue(Ptr) do
      case Action of

        lnAdded:
          if FOwnerList = nil then
            FOwnerList := Self;

        else
          if FOwnerList = Self then
            FOwnerList := nil;

      end;//case; with; if

  inherited;
end;

// public
function TSANSyncQueueList.Extract(Item: TSANSyncQueue): TSANSyncQueue;
begin
  Result := TSANSyncQueue(inherited Extract(Item));
end;

function TSANSyncQueueList.ExtractItem(Item: TSANSyncQueue; Direction: TList.TDirection): TSANSyncQueue;
begin
  Result := TSANSyncQueue(inherited ExtractItem(Item, Direction));
end;

function TSANSyncQueueList.First: TSANSyncQueue;
begin
  Result := TSANSyncQueue(inherited First);
end;

function TSANSyncQueueList.Last: TSANSyncQueue;
begin
  Result := TSANSyncQueue(inherited Last);
end;

function TSANSyncQueueList.Add(const AOnProcessItem : TSANSyncQueueEvent): TSANSyncQueue;
begin
  Result := TSANSyncQueue.Create(Self, AOnProcessItem);

  inherited Add(Result);
end;
//==============================================================================

//==============================================================================
// TSANSyncQueuesSet
// protected
procedure TSANSyncQueuesSet.ReadWndMessages(var AMessage: TMessage);
var
  vIndex : integer;
begin
  vIndex := AMessage.Msg - (WM_USER + 1);

  if (vIndex >= 0) and (vIndex < FSyncQueueList.Count) then
    FSyncQueueList[vIndex].Process;
end;

// public
constructor TSANSyncQueuesSet.Create;
begin
  inherited Create;

  FSyncQueueList := TSANSyncQueueList.Create;
  FSyncQueueList.FQueuesSet := Self;

  FWndHandle := AllocateHWnd(ReadWndMessages);
end;

destructor TSANSyncQueuesSet.Destroy;
begin
  DeallocateHWnd(FWndHandle);

  FreeAndNil(FSyncQueueList);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANLocker
// private
function TSANLocker.GetLocksCount: integer;
begin
  FCriticalSection.Enter;
  try
    Result := FLocksCount;
  finally
    FCriticalSection.Leave;
  end;
end;

// public
constructor TSANLocker.Create(const AOnLock, AOnUnLock : TNotifyEvent);
begin
  inherited Create;

  FCriticalSection := TCriticalSection.Create;

  FOnLock    := AOnLock;
  FOnUnLock  := AOnUnLock;
end;

destructor TSANLocker.Destroy;
begin
  FreeAndNil(FCriticalSection);

  inherited;
end;

procedure TSANLocker.Lock;
begin
  FCriticalSection.Enter;
  try
    Inc(FLocksCount);

    if Assigned(FOnLock) then
      FOnLock(Self);

  finally
    FCriticalSection.Leave;
  end;
end;

procedure TSANLocker.UnLock;
begin
  FCriticalSection.Enter;
  try

    if FLocksCount > 0 then
      Dec(FLocksCount)
    else
      FLocksCount := 0;

    if Assigned(FOnUnLock) then
      FOnUnLock(Self);

  finally
    FCriticalSection.Leave;
  end;

end;

function TSANLocker.IsLocked(const AUseCriticalSection : boolean): boolean;
begin
  if AUseCriticalSection then
    Result := LocksCount <> 0
  else
    Result := FLocksCount <> 0
end;

//==============================================================================

//==============================================================================
// TSANGenerator
// public
constructor TSANGenerator.Create(const AStartValue: Cardinal);
begin
  inherited Create;

  FCurValue := AStartValue;

  FCriticalSection := TCriticalSection.Create;
end;

destructor TSANGenerator.Destroy;
begin
  FreeAndNil(FCriticalSection);

  inherited;
end;

function TSANGenerator.Generate(const AUseCriticalSection: boolean): Cardinal;
begin

  if AUseCriticalSection then
    FCriticalSection.Enter;

  try

    Inc(FCurValue);

    Result := FCurValue;

  finally

    if AUseCriticalSection then
      FCriticalSection.Leave;

  end;

end;

procedure TSANGenerator.ResetCurValue(const ANewValue: Cardinal;
  const AUseCriticalSection: boolean);
begin

  if AUseCriticalSection then
    FCriticalSection.Enter;

  try

    FCurValue := ANewValue;

  finally

    if AUseCriticalSection then
      FCriticalSection.Leave;

  end;

end;
//==============================================================================


END.
