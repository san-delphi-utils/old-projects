{*******************************************************}
{                                                       }
{       SANThreadsManager                               }
{       Менеджер потоков.                               }
{       Отвечает за управление набором потоков.         }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANThreadsManager;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes;

TYPE
  TSANThreadsManagerEnumProc    = procedure (const AThread : TThread; var AParam : Pointer);
  TSANThreadsManagerEnumObjProc = procedure (const AThread : TThread; var AParam : Pointer) of object;

  /// <summary>
  /// Менеджер потоков.
  /// </summary>
  TSANThreadsManager = class(TInterfacedPersistent)
  private
    FThreadsList : TThreadList;

    function GetThreads(const Index: integer): TThread;
    function GetThreadsCount: integer;

  protected
    procedure DoTerminateAll(const AList : TList);
    procedure DoWaitForAll(const AList : TList);
    procedure DoFreeAll(const AList : TList);

  public
    constructor Create;
    destructor Destroy; override;

    function Add(const AThread : TThread) : TThread;
    function Extract(const AThread : TThread) : TThread;
    function Remove(const AThread : TThread) : boolean;

    procedure TerminateAll;
    procedure WaitForAll;
    procedure Clear;

    procedure EnumThreads(const AEnumProc : TSANThreadsManagerEnumProc;    AParam : Pointer = nil); overload;
    procedure EnumThreads(const AEnumProc : TSANThreadsManagerEnumObjProc; AParam : Pointer = nil); overload;

    property ThreadsCount : integer  read GetThreadsCount;
    property Threads[const Index : integer] : TThread  read GetThreads; default;

  end;

/// <summary>
/// Поток AThread должен ожидать ATimeOut миллисекунд с проверкой завершения
/// каждые ATimeStep миллисекунд
/// </summary>
procedure ThreadWaitTimeOut(const AThread : TThread; const ATimeOut, ATimeStep : Cardinal);

IMPLEMENTATION

type
  TThreadHack = class(TThread);

procedure ThreadWaitTimeOut(const AThread : TThread; const ATimeOut, ATimeStep : Cardinal);
var
  vStep : integer;
begin

  for vStep := 0 to (ATimeOut div ATimeStep) - 1 do  begin

    if TThreadHack(AThread).Terminated then
      Break;

    AThread.Sleep(ATimeStep);
  end;

end;


//==============================================================================
// TSANThreadsManager
// private
function TSANThreadsManager.GetThreadsCount: integer;
begin
  with FThreadsList.LockList do
  try
    Result := Count;
  finally
    FThreadsList.UnlockList;
  end;
end;

function TSANThreadsManager.GetThreads(const Index: integer): TThread;
begin
  with FThreadsList.LockList do
  try
    Result := TThread(Items[Index]);
  finally
    FThreadsList.UnlockList;
  end;
end;

// protected
procedure TSANThreadsManager.DoTerminateAll(const AList: TList);
var
  i : integer;
begin
  for i := 0 to AList.Count - 1 do
    TThread(AList[i]).Terminate;
end;

procedure TSANThreadsManager.DoWaitForAll(const AList: TList);
var
  i : integer;
begin
  for i := 0 to AList.Count - 1 do
    TThread(AList[i]).WaitFor;
end;

procedure TSANThreadsManager.DoFreeAll(const AList: TList);
var
  i : integer;
begin
  for i := 0 to AList.Count - 1 do
    TThread(AList[i]).Free;
end;

// public
constructor TSANThreadsManager.Create;
begin
  inherited Create;

  FThreadsList := TThreadList.Create;
end;

destructor TSANThreadsManager.Destroy;
begin
  Clear;

  inherited;
end;

function TSANThreadsManager.Add(const AThread: TThread): TThread;
begin
  with FThreadsList.LockList do
  try
    Result := AThread;

    Add(Result);
  finally
    FThreadsList.UnlockList;
  end;
end;

function TSANThreadsManager.Extract(const AThread: TThread): TThread;
begin
  with FThreadsList.LockList do
  try
    Result := TThread(Extract(AThread));
  finally
    FThreadsList.UnlockList;
  end;
end;

function TSANThreadsManager.Remove(const AThread: TThread): boolean;
var
  vThread: TThread;
begin
  vThread := Extract(AThread);

  Result := Assigned(vThread);
  if not Result then
    Exit;

  vThread.Terminate;
  vThread.WaitFor;
  FreeAndNil(vThread);
end;

procedure TSANThreadsManager.TerminateAll;
var
  vList : TList;
begin
  vList := FThreadsList.LockList;
  try
    DoTerminateAll(vList);
  finally
    FThreadsList.UnlockList;
  end;
end;

procedure TSANThreadsManager.WaitForAll;
var
  vList : TList;
begin
  vList := FThreadsList.LockList;
  try
    DoWaitForAll(vList);
  finally
    FThreadsList.UnlockList;
  end;
end;

procedure TSANThreadsManager.Clear;
var
  vList : TList;
begin
  vList := FThreadsList.LockList;
  try
    DoTerminateAll(vList);
    DoWaitForAll(vList);
    DoFreeAll(vList);
    vList.Clear
  finally
    FThreadsList.UnlockList;
  end;
end;

procedure TSANThreadsManager.EnumThreads(const AEnumProc: TSANThreadsManagerEnumProc;
  AParam: Pointer);
var
  i : integer;
begin
  with FThreadsList.LockList do
  try

  for i := 0 to Count - 1 do
    AEnumProc(TThread(Items[i]), AParam);

  finally
    FThreadsList.UnlockList;
  end;
end;

procedure TSANThreadsManager.EnumThreads(const AEnumProc: TSANThreadsManagerEnumObjProc;
  AParam: Pointer);
var
  i : integer;
begin
  with FThreadsList.LockList do
  try

  for i := 0 to Count - 1 do
    AEnumProc(TThread(Items[i]), AParam);

  finally
    FThreadsList.UnlockList;
  end;
end;
//==============================================================================

END.
