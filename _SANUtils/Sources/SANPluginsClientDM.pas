{*******************************************************}
{                                                       }
{       SANPluginsClientDM                              }
{       Клиентская часть плагинов в                     }
{       "пакетах-как-DLL"                               }
{       Оформлена в виде модуля данных,                 }
{       для визуального проектирования                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPluginsClientDM;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf, SANPkgAsDllCustomDM, SANPkgAsDllClient, SANPkgAsDllClientDM,
  SANPluginsIntf, SANPluginsClient;

TYPE
  /// <summary>
  /// Модуль данных для визуального проектирования плагина.
  /// </summary>
  TSANPluginDataModule = class(TSANVersionInitDoneDataModule)
  published
    property GUID;
    property Caption;
    property Description;
    property URL;
    property Author;
    property Version;

    property OnInit;
    property OnDone;
  end;
  TSANPluginDataModuleClass = class of TSANPluginDataModule;

  /// <summary>
  /// Класс плагина с визуальным проектированием
  /// </summary>
  TSANPluginForDM = class(TSANPlgPlugin)
  private
    FDM : TSANPluginDataModule;

  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;

    procedure DoInit(const AParams: IInterface); override;
    procedure DoDone(const AParams: IInterface); override;

    // ISANVersionInfo
    function GetGUID: TGUID; override; safecall;
    function GetCaption: WideString; override; safecall;
    function GetDescription: WideString; override; safecall;
    function GetURL: WideString; override; safecall;
    function GetAuthor: WideString; override; safecall;
    function GetVersion: Longword; override; safecall;

  public
    constructor Create(const APluginDataModuleClass : TSANPluginDataModuleClass);
    destructor Destroy; override;

  end;

  /// <summary>
  /// Переопределение точки входа, под визуальное проектирование плагинов.
  /// </summary>
  TSANPkgAsDllEntryWithPluginsForDM = class(TSANPkgAsDllEntryForDM)
  private
    class var FCreationCounter : integer;
    class var FPlugins : TSANPlgPluginsList;

  protected
    procedure DoInit(const AParams: IInterface); override;
    procedure DoDone(const AParams: IInterface); override;

  public
    class constructor Create;
    class destructor Destroy;

    constructor Create; override;
    destructor Destroy; override;

  end;

/// <summary>
/// Регистрация плагина TSANPluginForDM по классу модуля данных внутри пакета
/// </summary>
procedure SANPluginRegister(const APluginDataModuleClass : TSANPluginDataModuleClass); overload;
/// <summary>
/// Регистрация плагина внутри пакета
/// </summary>
procedure SANPluginRegister(const APlugin : TSANPlgPlugin); overload;

IMPLEMENTATION

uses
  SANPluginsMessages;

procedure SANPluginRegister(const APluginDataModuleClass : TSANPluginDataModuleClass);
var
  vPlugin : TSANPlgPlugin;
begin
  vPlugin := TSANPluginForDM.Create(APluginDataModuleClass);
  TSANPkgAsDllEntryWithPluginsForDM.FPlugins.Add(vPlugin);
end;

procedure SANPluginRegister(const APlugin : TSANPlgPlugin); overload;
begin
  TSANPkgAsDllEntryWithPluginsForDM.FPlugins.Add(APlugin);
end;

//==============================================================================
// TSANPluginForDM
// protected
function TSANPluginForDM.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  Result := inherited QueryInterface(IID, Obj);

  if Result = E_NOINTERFACE then
    Result := FDM.QueryInterface(IID, Obj);
end;

procedure TSANPluginForDM.DoInit(const AParams: IInterface);
begin
  FDM.DoInit(AParams);
end;

procedure TSANPluginForDM.DoDone(const AParams: IInterface);
begin
  FDM.DoDone(AParams)
end;

function TSANPluginForDM.GetGUID: TGUID;
begin
  Result := FDM.GetGUID;
end;

function TSANPluginForDM.GetCaption: WideString;
begin
  Result := FDM.GetCaption;
end;

function TSANPluginForDM.GetDescription: WideString;
begin
  Result := FDM.GetDescription;
end;

function TSANPluginForDM.GetURL: WideString;
begin
  Result := FDM.GetURL;
end;

function TSANPluginForDM.GetAuthor: WideString;
begin
  Result := FDM.GetAuthor;
end;

function TSANPluginForDM.GetVersion: Longword;
begin
  Result := FDM.GetVersion;
end;

// public
constructor TSANPluginForDM.Create(const APluginDataModuleClass : TSANPluginDataModuleClass);
begin
  inherited Create;

  FDM := APluginDataModuleClass.Create(nil);
end;

destructor TSANPluginForDM.Destroy;
begin
  FreeAndNil(FDM);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANPkgAsDllEntryWithPluginsForDM
// protected
procedure TSANPkgAsDllEntryWithPluginsForDM.DoInit(const AParams: IInterface);
begin
  FPlugins.RegPluginIntf := AParams as ISANPlgRegistratorPlugin;
  FPlugins.RegistrAll;

  inherited;
end;

procedure TSANPkgAsDllEntryWithPluginsForDM.DoDone(const AParams: IInterface);
begin
  inherited;

  FPlugins.UnRegistrAll;
end;

// public
class constructor TSANPkgAsDllEntryWithPluginsForDM.Create;
begin
  inherited;

  FPlugins := TSANPlgPluginsList.Create(nil);
end;

class destructor TSANPkgAsDllEntryWithPluginsForDM.Destroy;
begin
  FreeAndNil(FPlugins);

  inherited;
end;

constructor TSANPkgAsDllEntryWithPluginsForDM.Create;
begin
  inherited;

  if FCreationCounter > 1 then
    raise ESANPlgClientError.CreateRes(@sClientDM_MultiEntryNotSupported);

  Inc(FCreationCounter);
end;

destructor TSANPkgAsDllEntryWithPluginsForDM.Destroy;
begin
  FPlugins.UnRegistrAll;

  inherited;
end;
//==============================================================================

INITIALIZATION
  PkgAsDllEntryClass := TSANPkgAsDllEntryWithPluginsForDM;

FINALIZATION

END.
