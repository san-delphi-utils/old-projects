{*******************************************************}
{                                                       }
{       SANPkgAsDllClient                               }
{       Клиентская часть (внутри пакета)                }
{       взаимодействия с "пакетом-как-DLL"              }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPkgAsDllClient;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf;

TYPE
  ESANPkgAsDllClientError = class(Exception);

  /// <summary>
  /// Базовый класс точки входа в "пакет-как-DLL"
  /// </summary>
  TSANPkgAsDllEntry = class(TInterfacedObject, ISANPkgAsDllEntry)
  private
    class var FInitPackageParams: IInterface;

    // Защита от повторного вызова с сервера
    FIsInit, FIsDone : boolean;

  protected
    function _Release: Integer; stdcall;

    procedure DoInit(const AParams: IInterface); virtual;
    procedure DoDone(const AParams: IInterface); virtual;

    // ISANPlgPackageEntry
    procedure Init(const AParams: IInterface); safecall;
    procedure Done(const AParams: IInterface); safecall;

  public
    constructor Create; virtual;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

  end;
  TSANPkgAsDllEntryClass = class of TSANPkgAsDllEntry;

var
  // You may override class at initialization section of your unit
  PkgAsDllEntryClass : TSANPkgAsDllEntryClass = TSANPkgAsDllEntry;

// Accessible in initialization sections only
function InitPackageParams: IInterface;

// For export in childs packages
function GetPkgAsDllEntryPoint(const AParams: IInterface; out Intf: ISANPkgAsDllEntry): HRESULT; stdcall;

IMPLEMENTATION

uses
  ComObj;

function GetProcAddress(hModule: HMODULE; lpProcName: PAnsiChar): Pointer; stdcall; external kernel32 name 'GetProcAddress';

procedure _Done;
type
  TPackageUnload = procedure;
var
  PackageUnload: TPackageUnload;
begin
  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('04 PLUGIN: _Done enter');
  {$ENDIF PLUGINSYSTEMDEBUG}
  @PackageUnload := GetProcAddress(HInstance, 'Finalize'); //Do not localize
  PackageUnload;
  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('   PLUGIN: _Done leave');
  {$ENDIF PLUGINSYSTEMDEBUG}
end;

procedure _Init;
type
  TPackageLoad = procedure;
var
  PackageLoad: TPackageLoad;
begin
  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('04 PLUGIN: _Init enter');
  {$ENDIF PLUGINSYSTEMDEBUG}
  @PackageLoad := GetProcAddress(HInstance, 'Initialize'); //Do not localize
  PackageLoad;
  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('   PLUGIN: _Init leave');
  {$ENDIF PLUGINSYSTEMDEBUG}
end;

// Unhandled exception support

var
  WasError: Boolean;

// Blocks Halt in SysUtils.ExceptHandler
procedure DoneExceptions;
begin
  if ExceptObject <> nil then
  begin
    WasError := True;
    {$IFDEF PLUGINSYSTEMDEBUG}
    OutputDebugString('   PLUGIN: unhandled exception');
    {$ENDIF PLUGINSYSTEMDEBUG}

    // Shows error message - from SysUtils.ExceptHandler
    ShowException(ExceptObject, ExceptAddr);
    // <- here: removed Halt

    // Release exception object while we can (memory manager will be shutdowned soon)
    TObject(AcquireExceptionObject).Free;
    ReleaseExceptionObject;
  end;
end;

function InitPackageParams: IInterface;
begin
  Result := TSANPkgAsDllEntry.FInitPackageParams;
end;

function GetPkgAsDllEntryPoint(const AParams: IInterface; out Intf: ISANPkgAsDllEntry): HRESULT; stdcall;
begin
  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('   PLUGIN: GetPkgAsDllEntryPoint enter');
  {$ENDIF PLUGINSYSTEMDEBUG}
  WasError := False;
  Intf := nil;
  if not ModuleIsPackage then
  begin
    {$IFDEF PLUGINSYSTEMDEBUG}
    OutputDebugString('   PLUGIN: GetPkgAsDllEntryPoint leave');
    {$ENDIF PLUGINSYSTEMDEBUG}
    Result := HResultFromWin32(ERROR_INVALID_MODULETYPE);
    Exit;
  end;
  TSANPkgAsDllEntry.FInitPackageParams := AParams;
  try
    // Initialize package
    _Init;
  except // 'Initialize' calls finalization sections in case of exception => DoneException was called and showed error message.
    WasError := True;
    // Clear reference to non-existing exception object (it was deleted in DoneExceptions) - prevent destructor double-call
    AcquireExceptionObject;
    ReleaseExceptionObject;
  end;
  if WasError then
  begin
    {$IFDEF PLUGINSYSTEMDEBUG}
    OutputDebugString('   PLUGIN: _Init failed');
    {$ENDIF PLUGINSYSTEMDEBUG}
    WasError := False;
    {$IFDEF PLUGINSYSTEMDEBUG}
    OutputDebugString('   PLUGIN: GetPkgAsDllEntryPoint leave');
    {$ENDIF PLUGINSYSTEMDEBUG}
    Result := HResultFromWin32(ERROR_DLL_INIT_FAILED);
    Exit;
  end;

  // Package was fully initialized.
  try
    TSANPkgAsDllEntry.FInitPackageParams := nil;
    Intf := PkgAsDllEntryClass.Create;
    Result := 0;
  except
    {$IFDEF PLUGINSYSTEMDEBUG}
    OutputDebugString('   PLUGIN: exception in GetPkgAsDllEntryPoint');
    {$ENDIF PLUGINSYSTEMDEBUG}
    ShowException(ExceptObject, ExceptAddr);
    Result := HResultFromWin32(ERROR_DLL_INIT_FAILED);
  end;

  if Failed(Result) then
  begin
    if Intf = nil then
      _Done
    else
      Intf := nil;                     // _Done called here from _Release
    Result := HResultFromWin32(ERROR_DLL_INIT_FAILED);
  end;
  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('   PLUGIN: GetPkgAsDllEntryPoint leave');
  {$ENDIF PLUGINSYSTEMDEBUG}
end;

//==============================================================================
// TSANPkgAsDllEntry
// protected
function TSANPkgAsDllEntry._Release: Integer;

  // From System.pas
  function InterlockedDecrement(var Addend: Integer): Integer;
  asm
        MOV   EDX,-1
        XCHG  EAX,EDX
   LOCK XADD  [EDX],EAX
        DEC   EAX
  end;

begin
  Result := InterlockedDecrement(FRefCount);
  if Result = 0 then
  begin
    try
      try
        Destroy;              // Release TSANPkgAsDllEntry instance
      finally
        _Done;                // Shutdown the package (all interfaces should be released by now)
      end;
    except // _Done calls finalization sections => DoneException was called and showed error message.
      // Clear reference to non-existing exception object (it was deleted in DoneExceptions) - prevent destructor double-call
      AcquireExceptionObject;
      ReleaseExceptionObject;
    end;
    // FreeLibrary will be called immediately after exit
  end;
end;

procedure TSANPkgAsDllEntry.DoInit(const AParams: IInterface);
begin
end;

procedure TSANPkgAsDllEntry.DoDone;
begin
end;

procedure TSANPkgAsDllEntry.Init(const AParams: IInterface);
begin

  if FIsInit then
    Exit;

  FIsInit := True;

  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('10 PLUGIN: TSANPkgAsDllEntry.Init');
  {$ENDIF PLUGINSYSTEMDEBUG}

  DoInit(AParams);
end;

procedure TSANPkgAsDllEntry.Done(const AParams: IInterface);
begin

  if FIsDone then
    Exit;

  FIsDone := True;

  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('10 PLUGIN: TSANPkgAsDllEntry.Done');
  {$ENDIF PLUGINSYSTEMDEBUG}

  DoDone(AParams);
end;

// public
constructor TSANPkgAsDllEntry.Create;
begin
  inherited Create;
end;

function TSANPkgAsDllEntry.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANPkgAsDllEntry,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

INITIALIZATION
  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('05 PLUGIN: ImplInit.pas initialization');
  {$ENDIF PLUGINSYSTEMDEBUG}

FINALIZATION
  {$IFDEF PLUGINSYSTEMDEBUG}
  OutputDebugString('05 PLUGIN: ImplInit.pas finalization');
  {$ENDIF PLUGINSYSTEMDEBUG}

  DoneExceptions;
END.

