{*******************************************************}
{                                                       }
{       SANLogsBase                                     }
{       Логирование. Общие типы данных и методы.        }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANLogsBase;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, Variants, ActiveX, ComObj,
  SANMisc,
  SANIntfUtils,
  SANLogsIntf;

CONST
  LOGS_DLL_ENTRY_POINT_NAME = '_B439EE67124F4F2592B16521605BF16A';

  LMK_INFO     = 0;
  LMK_WARNING  = 1;
  LMK_ERROR    = 2;
  LMK_FATAL    = 3;

TYPE
  ESANLogError = class(Exception);

  /// <summary>
  /// Одна запись лога
  /// </summary>
  TSANLogRecord = class(TSANChildIntfObject, ISANLogRecord)
  private
    FID : integer;
    FDateTime : TDateTime;
    FKind : LongWord;
    FSubType, FMsg : string;

  protected
    function Get_ID : integer; safecall;
    procedure Set_ID(const AID : integer); safecall;
    function Get_DateTime : TDateTime; safecall;
    procedure Set_DateTime(const ADateTime : TDateTime); safecall;
    function Get_Kind : Longword; safecall;
    procedure Set_Kind(const AKind : Longword); safecall;
    function Get_SubType : Widestring; safecall;
    procedure Set_SubType(const ASubType : WideString); safecall;
    function Get_Msg : Widestring; safecall;
    procedure Set_Msg(const AMsg : WideString); safecall;

    procedure Clear; safecall;

    procedure From(const AID : integer; const ADateTime : TDateTime;
      const AKind : LongWord; const ASubType, AMsg : WideString); safecall;

    procedure AssignRec(const ASource : ISANLogRecord); safecall;

    function IsEqualsRecords(const ALogRecord : ISANLogRecord) : WordBool; safecall;

  public
    constructor CreateFrom(const AParent : TObject; const AID : integer; const ADateTime : TDateTime;
      const AKind : LongWord; const ASubType, AMsg : WideString);

    constructor CreateAssigned(const AParent : TObject; const ASource : ISANLogRecord);

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

  end;

  /// <summary>
  /// Перечислитель записей лога
  /// </summary>
  TSANLogRecordsEnumirator = class(TSANCustomThreadListEnumirator, ISANLogRecordsEnumirator)
  protected
    function Get_Count : integer; safecall;
    function Get_Records(const Index : integer) : ISANLogRecord; safecall;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  /// Набор записей в логе.
  /// </summary>
  TSANLogRecords = class(TSANChildIntfObject, ISANLogRecords)
  private
    FRecList : TThreadList;
    FNotifier : ISANLogRecordsNotifier;

  protected
    procedure DoClearItem(const APtr : Pointer);
    function DoAdd(const AList : TList; const ARecord : TSANLogRecord) : TSANLogRecord; overload;
    function DoAdd(const AList : TList; const AID : integer;
      const ADateTime : TDateTime; const AKind : LongWord;
      const ASubType, AMsg : WideString) : TSANLogRecord; overload;

    function Get_Count : integer; safecall;
    function Get_Records(const Index : integer) : ISANLogRecord; safecall;
    function Enumiration : ISANLogRecordsEnumirator; safecall;
    procedure Clear; safecall;
    function FindByID(out ALogRecord : ISANLogRecord; const AID : integer) : WordBool; safecall;
    function ExtractByID(const AID : integer) : ISANLogRecord; safecall;
    function RemoveByID(const AID : integer) : WordBool; safecall;
    procedure Delete(const AIndex : integer); safecall;
    function AddAsCopy(const ALogRecord : ISANLogRecord) : ISANLogRecord; safecall;
    function Add(const AID : integer; const ADateTime : TDateTime;
      const AKind : LongWord; const ASubType, AMsg : WideString) : ISANLogRecord; safecall;
    function Filter(AFilter : ISANLogRecordsFilter) : integer; safecall;
    procedure ExportRecords(out ARecords : OleVariant); safecall;
    procedure ImportRecords(var ARecords : OleVariant; const AReWrite : WordBool); safecall;
    function Get_Notifier : ISANLogRecordsNotifier; safecall;
    procedure Set_Notifier(const ANotifier : ISANLogRecordsNotifier); safecall;

  public
    constructor Create(const AParent : TObject);
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  TSANLogPath = class;

  /// <summary>
  /// Часть идентификационного пути лога в рамках хранилища.
  /// </summary>
  TSANLogPathPart = class(TSANChildIntfObject, ISANLogNamed, ISANLogPathPart)
  private
    FName : string;
    FValue : OleVariant;

    function Get_Parent: TSANLogPath;

  protected
    procedure DoValueChanging(const AOldValue, ANewValue: OleVariant); dynamic;
    procedure DoValueChanged(const AOldValue, ANewValue: OleVariant); dynamic;

    function Get_ParentPath : ISANLogPath; safecall;
    function Get_Name : WideString; safecall;
    function Get_Value : OleVariant; safecall;
    procedure Set_Value(const AValue : OleVariant); safecall;
    function IsEqualsPathParts(const APathPart : ISANLogPathPart) : WordBool; safecall;

    property Parent : TSANLogPath  read Get_Parent;

  public
    constructor Create(const AParentPath : TSANLogPath; const AName : string); virtual;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

  end;
  TSANLogPathPartClass = class of TSANLogPathPart;


  /// <summary>
  /// Перечислитель частей идентификационного пути лога в рамках хранилища.
  /// </summary>
  TSANLogPathPartsEnumirator = class(TSANCustomThreadListEnumirator, ISANLogPathPartsEnumirator)
  protected
    function Get_Count : integer; safecall;
    function Get_PathParts(const Index : integer) : ISANLogPathPart; safecall;
    function FindByName(out APathPart : ISANLogPathPart; const AName : WideString) : WordBool; safecall;
    function RemoveByName(const AName : WideString) : WordBool; safecall;
    procedure Delete(const AIndex : integer); safecall;
    function Add(const AName : WideString) : ISANLogPathPart; safecall;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  /// Набор перехватчиков событий пути лога.
  /// </summary>
  TSANLogPathNotifiers = class(TSANCustomNotifiers)
  private
    function Get_Parent: TSANLogPath;

  protected
    procedure NotifyValueChanging(const APathPart : ISANLogPathPart;
      const AOldValue, ANewValue : OleVariant);
    procedure NotifyValueChanged(const APathPart : ISANLogPathPart;
      const AOldValue, ANewValue : OleVariant);
    procedure NotifyDestroying;

    property Parent : TSANLogPath  read Get_Parent;
  end;

  TSANCustomLogsStorage = class;

  /// <summary>
  /// Идентификационный путь лога в рамках хранилища.
  /// </summary>
  TSANLogPath = class(TSANChildIntfObject, ISANLogPath)
  private
    FPathPartsList : TThreadList;
    FNotifiersList : TSANLogPathNotifiers;

    function Get_ObjStorage: TSANCustomLogsStorage;

  protected
    class function PathPartClass : TSANLogPathPartClass; virtual;
    procedure CreatePathPart(out APathPart; const AName : string);
    procedure DoDelete(const AList : TList; const AIndex : integer);
    function DoAdd(const AList : TList; const AName : string) : TSANLogPathPart;

    // ISANCustomLogPathParts
    function Get_Count : integer; safecall;
    function Get_PathParts(const Index : integer) : ISANLogPathPart; safecall;
    function FindByName(out APathPart : ISANLogPathPart; const AName : WideString) : WordBool; safecall;
    function RemoveByName(const AName : WideString) : WordBool; safecall;
    procedure Delete(const AIndex : integer); safecall;
    function Add(const AName : WideString) : ISANLogPathPart; safecall;

    // ISANLogPath
    function Get_Storage : ISANLogsStorage; safecall;
    function Enumiration : ISANLogPathPartsEnumirator; safecall;
    function IsEqualsPath(const APath : ISANLogPath) : WordBool; safecall;
    function AddNotifier(const APathNotifier : ISANLogPathNotifier) : integer; safecall;
    function RemoveNotifier(const APathNotifierID : integer) : WordBool; safecall;

    property Storage : TSANCustomLogsStorage  read Get_ObjStorage;
    property PathPartsList : TThreadList  read FPathPartsList;
    property NotifiersList : TSANLogPathNotifiers  read FNotifiersList;

  public
    constructor Create(const AStorage : TSANCustomLogsStorage; const ANames : array of string);
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  TSANCustomLog = class;

  /// <summary>
  /// Набор перехватчиков событий лога.
  /// </summary>
  TSANCustomLogNotifiers = class(TSANCustomNotifiers)
  private
    function Get_Parent: TSANCustomLog;

  protected
    procedure NotifyRecordWrited(const ALogRecord : ISANLogRecord);
    procedure NotifyRecordsWrited(const ALogRecords : ISANLogRecords; const AReWrite: WordBool);
    procedure NotifyFiltered(const AFilteredCount : integer);
    procedure NotifyDestroying;

    property Parent : TSANCustomLog  read Get_Parent;
  end;

  /// <summary>
  /// Абстрактный лог
  /// </summary>
  TSANCustomLog = class(TSANChildIntfObject, ISANLogNamed, ISANLogProvideHInstace, ISANCustomLog, ISANLog)
  private
    FName : string;
    FPath : ISANLogPath;
    FPathNotifier : ISANLogPathNotifier;
    FPathNotifierID : integer;
    FLogNotifiers : TSANCustomLogNotifiers;
    FLastRec : TSANLogRecord;
    FWriteInProgress : boolean;


    function Get_ObjStorage: TSANCustomLogsStorage;

  protected
    procedure VlidateSecondCallBeginWrite;
    procedure VlidateNotCallBeginWrite;

    procedure DoBeforeChangePath(const AOldPath, ANewPath : ISANLogPath); virtual;
    procedure DoAfterChangePath(const AOldPath, ANewPath : ISANLogPath); virtual;
    procedure DoPathPartChanging(const APathPart : ISANLogPathPart;
      const AOldValue, ANewValue : OleVariant); virtual;
    procedure DoPathPartChanged(const APathPart : ISANLogPathPart;
      const AOldValue, ANewValue : OleVariant); virtual;

    procedure DoWrite(const ADateTime : TDateTime; const AKind : LongWord; const ASubType : WideString; const AMessage : WideString; var ANewRecID : integer); virtual; abstract;
    procedure DoBeginWrite(const ADateTime : TDateTime; const AKind : LongWord; const ASubType : WideString; var ANewRecID : integer); virtual; abstract;
    procedure DoPartialWrite(const APartialMessage : WideString); virtual; abstract;
    procedure DoEndWrite; virtual; abstract;
    procedure DoWriteRecords(const ALogRecords : ISANLogRecords; const AReWrite : WordBool); virtual; abstract;
    procedure DoRead(const ALogRecords : ISANLogRecords; const AFirstID, ALastID : integer); virtual; abstract;
    procedure DoFilter(const AFilter : ISANLogRecordsFilter; var AFilteredCount : integer); virtual; abstract;

    // ISANLogProvideHInstace
    function Get_HInstace : LongWord; virtual; safecall; abstract;

    // ISANCustomLog
    function Write(const AKind : LongWord; const ASubType : WideString; const AMessage : WideString = ''; ADateTime : TDateTime = 0) : integer; safecall;
    function BeginWrite(const AKind : LongWord; const ASubType : WideString; ADateTime : TDateTime = 0) : integer; safecall;
    procedure PartialWrite(const APartialMessage : WideString); safecall;
    procedure EndWrite; safecall;

    // ISANLog
    function Get_Name : WideString; safecall;
    function Get_Path : ISANLogPath;  safecall;
    procedure Set_Path(const APath : ISANLogPath); safecall;
    function Get_Storage : ISANLogsStorage;  safecall;
    procedure WriteRecords(const ALogRecords : ISANLogRecords; const AReWrite : WordBool); safecall;
    procedure Read(const ALogRecords : ISANLogRecords;
      const AFirstID : integer = -1; const ALastID : integer = -1); safecall;
    function Filter(AFilter : ISANLogRecordsFilter) : integer; safecall;
    function IsEqualsLogs(const ALog : ISANLog) : WordBool; safecall;
    function AddNotifier(const ALogNotifier : ISANLogNotifier) : integer; safecall;
    function RemoveNotifier(const ALogNotifierID : integer) : WordBool; safecall;

    property Storage : TSANCustomLogsStorage  read Get_ObjStorage;
    property LastRec : TSANLogRecord  read FLastRec;
    property WriteInProgress : boolean  read FWriteInProgress;

  public
    constructor Create(const AStorage : TSANCustomLogsStorage;
      const AName : string; const APath : ISANLogPath); virtual;
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;
  TSANCustomLogClass = class of TSANCustomLog;

  /// <summary>
  /// Информация о логе
  /// </summary>
  TSANLogInfo = class(TSANChildIntfObject, ISANLogNamed, ISANLogInfo)
  private
    FName : WideString;
    FPath : ISANLogPath;
    FLog : ISANLog;
    function Get_ObjStorage: TSANCustomLogsStorage;

  protected
    // ISANLogInfo
    function Get_Name : WideString; safecall;
    function Get_Path : ISANLogPath; safecall;
    function Get_Storage : ISANLogsStorage; safecall;
    function Get_Log : ISANLog; safecall;
    function OpenLog : ISANLog; safecall;

    property Storage : TSANCustomLogsStorage  read Get_ObjStorage;

  public
    constructor Create(const AStorage : TSANCustomLogsStorage;
      const AName : string; const APath : ISANLogPath; const ALog : ISANLog = nil);
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  /// Перечислитель информации о логах
  /// </summary>
  TSANLogInfosEnumirator = class(TSANChildIntfObject, ISANLogInfosEnumirator)
  private
    FLogInfos : TList;

  protected
    // ISANLogInfosEnumirator
    function Get_Count : integer; safecall;
    function Get_LogInfos(const Index : integer) : ISANLogInfo; safecall;

  public
    constructor Create(const AParent : TObject; const ALogInfos : TList);
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  /// Перечислитель логов.
  /// </summary>
  TSANCustomLogsEnumirator = class(TSANCustomThreadListEnumirator, ISANLogsEnumirator)
  protected
    // ISANLogsEnumirator
    function Get_Count : integer; safecall;
    function Get_Logs(const Index : integer) : ISANLog; safecall;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  /// Набор перехватчиков событий хранилища логов.
  /// </summary>
  TSANCustomLogsStorageNotifiers = class(TSANCustomNotifiers)
  private
    function Get_Parent: TSANCustomLogsStorage;

  protected
    procedure NotifyLogAdded(const ALog : ISANLog);
    procedure NotifyLogRemoving(const ALog : ISANLog);
    procedure NotifyDestroying;

    property Parent : TSANCustomLogsStorage  read Get_Parent;
  end;

  /// <summary>
  /// Абстрактное хранилище логов.
  /// </summary>
  TSANCustomLogsStorage = class(TSANChildIntfObject, ISANLogNamed, ISANLogProvideHInstace, ISANLogsSet, ISANLogsStorage)
  private
    FName : string;
    FLogsList : TThreadList;
    FNotifiersList : TSANCustomLogsStorageNotifiers;

  protected
    procedure ValidatePath(const APath : ISANLogPath);

    procedure DoCreateLog(out ALog; const AName: WideString;
      const APath: ISANLogPath);
    function DoAddLog(const AList : TList; const AName: WideString;
      const APath: ISANLogPath) : TSANCustomLog;
    procedure DoClearLog(const APtr : Pointer);

    function LogClass : TSANCustomLogClass; virtual; abstract;
    procedure DoCreateNewPath(out APath : ISANLogPath); virtual; abstract;
    procedure DoCheckPathIsCorrect(const APath : ISANLogPath; out AIsCorrect : WordBool; out AErrors : WideString); virtual; abstract;
    procedure DoPathPartAvailableValuesList(const APathPart : ISANLogPathPart;
      out ACanBuildList : WordBool; out AValues: OleVariant); virtual;

    procedure DoBuildLogsInfoEnumiration(const APath : ISANLogPath; out ALogInfosEnumirator : ISANLogInfosEnumirator); virtual; abstract;

    // ISANLogProvideHInstace
    function Get_HInstace : LongWord; virtual; safecall; abstract;

    // ISANLogsSet_
    function Get_LogsCount : integer; safecall;
    function Get_Logs(const Index : integer) : ISANLog; safecall;
    function Enumiration : ISANLogsEnumirator; safecall;
    procedure Clear; safecall;
    function FindByName(out ALog : ISANLog; const AName : WideString) : WordBool; safecall;
    function ExtractByName(const AName : WideString) : ISANLog; safecall;
    function RemoveByName(const AName : WideString) : WordBool; safecall;

    // ISANLogsStorage
    function Get_Name : WideString; safecall;
    function CreateNewPath : ISANLogPath; safecall;
    function CheckPathIsCorrect(const APath : ISANLogPath; out AErrors : WideString) : WordBool; safecall;
    function PathPartAvailableValuesList(const APathPart : ISANLogPathPart;
      out AValues : OleVariant) : WordBool; safecall;
    function AddLog(const AName : WideString; const APath : ISANLogPath) : ISANLog; safecall;
    function EnumirateLogsInfo(const APath : ISANLogPath) : ISANLogInfosEnumirator; safecall;
    function AddNotifier(const AStorageNotifier : ISANLogsStorageNotifier) : integer; safecall;
    function RemoveNotifier(const AStorageNotifierID : integer) : WordBool; safecall;
    function IsEqualsStorages(const AStorage : ISANLogsStorage) : WordBool; safecall;

  public
    constructor Create(const AParent : TObject; const AName : string);
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  /// Перечислитель интерфейсов хранилищ.
  /// </summary>
  TSANLogsStoragesIntfEnumirator = class(TSANCustomThreadListEnumirator, ISANLogsStoragesEnumirator)
  protected
    // ISANLogsStoragesEnumirator
    function Get_Count : integer; safecall;
    function Get_Storages(const Index : integer) : ISANLogsStorage; safecall;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  TSANLogsStoragesIntfList = class;

  /// <summary>
  /// Набор перехватчиков событий списка хранилищ логов.
  /// </summary>
  TSANLogsStoragesIntfListNotifiers = class(TSANCustomNotifiers)
  private
    function Get_Parent: TSANLogsStoragesIntfList;

  protected
    procedure NotifyStorageAdded(const AStorage : ISANLogsStorage);
    procedure NotifyStorageRemoving(const AStorage : ISANLogsStorage);
    procedure NotifyDestroying;

    property Parent : TSANLogsStoragesIntfList  read Get_Parent;
  end;

  /// <summary>
  /// Список интерфейсов хранилищ.
  /// </summary>
  TSANLogsStoragesIntfList = class(TSANChildIntfObject, ISANLogsStoragesList)
  public
    type
      TStorageAndIDRec = record
        Storage : ISANLogsStorage;
        NotifierID : integer;
      end;
      PStorageAndIDRec = ^TStorageAndIDRec;

  private
    FStoragesNotifier : ISANLogsStorageNotifier;
    FStoragesList : TThreadList;
    FNotifiersList : TSANLogsStoragesIntfListNotifiers;

  protected
    procedure DoClearItem(const APtr : Pointer);
    function DoFindItem(const AItem, AValue : Pointer) : boolean;
    function DoAdd(const AList : TList; const AStorage : ISANLogsStorage) : integer;

    // ISANLogsStoragesList
    function Get_Count : integer; safecall;
    function Get_Storages(const Index : integer) : ISANLogsStorage; safecall;
    function Enumiration : ISANLogsStoragesEnumirator; safecall;
    procedure Clear; safecall;
    function FindByName(out AStorage : ISANLogsStorage; const AName : WideString) : WordBool; safecall;
    function ExtractByName(const AName : WideString) : ISANLogsStorage; safecall;
    function RemoveByName(const AName : WideString) : WordBool; safecall;
    function Add(const AStorage : ISANLogsStorage) : integer; safecall;
    function AddNotifier(const AStoragesListNotifier : ISANLogsStoragesListNotifier) : integer; safecall;
    function RemoveNotifier(const AStoragesListNotifierID : integer) : WordBool; safecall;

  public
    constructor Create(const AParent : TObject);
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property StoragesNotifier : ISANLogsStorageNotifier  read FStoragesNotifier  write FStoragesNotifier;
  end;

  /// <summary>
  /// Перечислитель интерфейсов логов.
  /// </summary>
  TSANLogsIntfEnumirator = class(TSANCustomThreadListEnumirator, ISANLogsEnumirator)
  protected
    // ISANLogsStoragesEnumirator
    function Get_Count : integer; safecall;
    function Get_Logs(const Index : integer) : ISANLog; safecall;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

  /// <summary>
  /// Абстрактный менеджер хранилищ и логов.
  /// </summary>
  TSANCustomLogsManager = class(TInterfacedPersistent, ISANLogNamed, ISANLogProvideHInstace, ISANLogsSet,
    ISANLogsManager, ISANLogsStorageNotifier, ISANLogsStoragesListNotifier)
  private
    FName : string;
    FLogsIntfsList : TThreadList;
    FStoragesIntfList : TSANLogsStoragesIntfList;
    FStoragesListNotifierID : integer;

  protected
    // ISANLogProvideHInstace
    function Get_HInstace : LongWord; virtual; safecall; abstract;

    // ISANLogsSet
    function Get_LogsCount : integer; safecall;
    function Get_Logs(const Index : integer) : ISANLog; safecall;
    function Enumiration : ISANLogsEnumirator; safecall;
    procedure Clear; safecall;
    function FindByName(out ALog : ISANLog; const AName : WideString) : WordBool; safecall;
    function ExtractByName(const AName : WideString) : ISANLog; safecall;
    function RemoveByName(const AName : WideString) : WordBool; safecall;

    // ISANLogsManager
    function Get_Name : WideString; safecall;
    function Get_Storages : ISANLogsStoragesList; safecall;

    // ISANLogsStorageNotifier
    procedure ISANLogsStorageNotifier.Destroying = StorageNotifierDestroying;
    procedure LogAdded(const AStorage : ISANLogsStorage; const ALog : ISANCustomLog); safecall;
    procedure LogRemoving(const AStorage : ISANLogsStorage; const ALog : ISANCustomLog); safecall;
    procedure StorageNotifierDestroying(const AStorage : ISANLogsStorage); safecall;

    // ISANLogsStoragesListNotifier
    procedure ISANLogsStoragesListNotifier.Destroying = StoragesListNotifierDestroying;
    procedure StorageAdded(const AStorages : ISANLogsStoragesList; const AStorage : ISANLogsStorage); safecall;
    procedure StorageRemoving(const AStorages : ISANLogsStoragesList; const AStorage : ISANLogsStorage); safecall;
    procedure StoragesListNotifierDestroying(const AStorages : ISANLogsStoragesList); safecall;

  public
    constructor Create(const AName : string);
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;
  end;

/// <summary>
/// Получить текст типа сообщения.
/// </summary>
/// <param name="AKind">
/// Тип сообщения.
/// </param>
/// <returns>
/// String. Текст сообщения.
/// </returns>
function SANLogDecodeKind(const AKind : LongWord) : string;

/// <summary>
/// Получить тип сообщения по тексту.
/// </summary>
/// <param name="AKindText">
/// Текст сообщения.
/// </param>
/// <returns>
/// LongWord. Тип сообщения. $FFFFFFFF - если тип не опознан.
/// </returns>
function SANLogEncodeKind(const AKindText : string) : LongWord;

/// <summary>
/// Функция поиска по имени для ISANLogNamed
/// </summary>
/// <param name="AItem">
/// Указатель TInterfacedObject.
/// </param>
/// <param name="AParam">
/// Указатель PWideString.
/// </param>
function SANListFF_LogsNamedFindByName(const AItem, AParam: Pointer): boolean;

/// <summary>
/// Функция поиска по имени для ISANLogNamed
/// </summary>
/// <param name="AItem">
/// Указатель IInterface.
/// </param>
/// <param name="AParam">
/// Указатель PWideString.
/// </param>
function SANListFF_LogsNamedIntfFindByName(const AItem, AParam: Pointer): boolean;


IMPLEMENTATION

uses
  SANListUtils;

resourcestring
//  sFilterProcNotAssignedErrorMessage      = 'Для фильтрации списка записей лога не задана фильтрующая процедура';
  sSecondCallBeginWriteErrorMessage       = 'Запись в лог с именем "%s" уже начата';
  sNotCallBeginWriteErrorMessage          = 'Запись в лог с именем "%s" еще не началась. Нет предшествующего вызова BeginWrite';
  sInvalidPathErrorMessage                = 'Попытка назначить логу с именем "%s" некорректный путь. %s';
  sIncorrectPathPartErrorMessage          = 'Для получения списка значений части пути в хранилище "%s", передана некорректная часть пути.';
  sOtherStoragePathErrorMessage           = 'Для получения списка значений части пути в хранилище "%s", передана часть пути из другого хранилища "%s".';
//  sLogNotFoundErrorMessage                = 'Лог с именем «%s» не найден';
//  sSecondCreationTryErrorMessage          = 'Попытка повторного создания лога с именем «%s»';
//  sEmptyLogsStoragesErrorMessage          = 'Нет ни одного хранилища логов';
//  sStorageNotFoundErrorMessage            = 'Не найдено хранилище с именем «%s»';
//  sEmptyParhErrorMessage                  = 'Задан пустой идентификационный путь';
//  sSecondStorageRegTryErrorMessage        = 'Попытка повторной регистрации хранилища логов с именем «%s»';
//  sGetStorageFromDLLFailErrorMessage      = 'Не удается получить хранилище из DLL';
//  sStorageNotAssignedErrorMessage         = 'Для лога не задано хранилище';
//  sLogsManagerNotInitializedErrorMessage  = 'Менеджер логов не инициализирован';
//  sLogsManagerNotFinalizeErrorMessage     = 'Менеджер логов не финализирован';

//==============================================================================
function SANLogDecodeKind(const AKind : LongWord) : string;
begin

  case AKind of
    0: Result := 'Information';
    1: Result := 'Warning';
    2: Result := 'Error';
    3: Result := 'Fatal';

    else
      Result := '<Unknown>';
  end;

end;
//==============================================================================
function SANLogEncodeKind(const AKindText : string) : LongWord;
begin

  Result := 0;
  repeat

    if CompareText(AKindText, SANLogDecodeKind(Result)) = 0 then
      Exit;

    Inc(Result);
  until Result >= 4;

  Result := $FFFFFFFF;
end;
//==============================================================================

//==============================================================================
type
  TInterfacedObjectHack = class(TInterfacedObject);
  PWideString = ^WideString;

function SANListFF_LogsNamedFindByName(const AItem, AParam: Pointer): boolean;
var
  vNamed : ISANLogNamed;
begin
  Result := (TInterfacedObjectHack(AItem).QueryInterface(ISANLogNamed, vNamed) = S_OK)
        and (CompareText(vNamed.Name, PWideString(AParam)^) = 0);
end;

function SANListFF_LogsNamedIntfFindByName(const AItem, AParam: Pointer): boolean;
var
  vNamed : ISANLogNamed;
begin
  Result := (IInterface(AItem).QueryInterface(ISANLogNamed, vNamed) = S_OK)
        and (CompareText(vNamed.Name, PWideString(AParam)^) = 0);
end;

function SANListFF_LogRecByID(const AItem, AValue : Pointer) : boolean;
begin
  Result := TSANLogRecord(AItem).FID = Integer(AValue);
end;
//==============================================================================

//==============================================================================
{
  TPSAPackFunc = function (out APSA : PSafeArray; const AData : Pointer) : HResult;
  TPSAUnPackFunc = function (const APSA : PSafeArray; const AData : Pointer) : HResult;

}
/// <summary>
/// Функция запаковки записей лога в безопасный массив.
/// </summary>
/// <param name="ASafeArr">
/// Безопасный массив для запаковки.
/// </param>
/// <param name="AData">
/// Указатель объекта, наследника TSANLogRecords.
/// </param>
/// <returns>
/// HRESULT. Код ошибки.
/// </returns>
function PSALogRecordsToSafeArray(out ASafeArr : PSafeArray; const AData : Pointer) : HResult;
var
  vRecords : TSANLogRecords;
  vList : TList;
  vDataP, vRecP : PVarDataArray;
  vLogRec : TSANLogRecord;
  i : integer;
begin
  vRecords := TSANLogRecords(AData);

  vList := vRecords.FRecList.LockList;
  try

    ASafeArr := SafeArrayCreateVector(varVariant, 0, vList.Count);

    if ASafeArr = nil then
      Exit(E_Fail);

    Result := SafeArrayAccessData(ASafeArr, PPointer(@vDataP)^);
    if Failed(Result) then
    begin
      SafeArrayDestroy(ASafeArr);
      ASafeArr := nil;
      Exit;
    end;

    try

      for i := 0 to vList.Count - 1 do
      begin
        PSafeArray(vDataP^[i].VArray) := SafeArrayCreateVector(varVariant, 0, 5);

        if vDataP^[i].VArray = nil then
          Exit(E_Fail);

        vDataP^[i].VType := varArray;

        Result := SafeArrayAccessData(PSafeArray(vDataP^[i].VArray), PPointer(@vRecP)^);
        if Failed(Result) then
        begin
          SafeArrayDestroy(PSafeArray(vDataP^[i].VArray));
          vDataP[i].VArray := nil;
          Exit;
        end;

        try
          vLogRec := TSANLogRecord(vList[i]);

          vRecP^[0].VInteger  := vLogRec.Get_ID;
          vRecP^[0].VType     := varInteger;

          vRecP^[1].VDouble   := vLogRec.Get_DateTime;
          vRecP^[1].VType     := varDouble;

          vRecP^[2].VInteger  := vLogRec.Get_Kind;
          vRecP^[2].VType     := varInteger;

          Result := VarDataOleStr(vRecP^[3], vLogRec.Get_SubType);
          if Failed(Result) then
            Exit;

          Result := VarDataOleStr(vRecP^[4], vLogRec.Get_Msg);
          if Failed(Result) then
            Exit;

        finally
          Result := SafeArrayUnaccessData(PSafeArray(vDataP^[i].VArray));
          if Failed(Result) then
          begin
            SafeArrayDestroy(PSafeArray(vDataP^[i].VArray));
            vDataP^[i].VArray := nil;
          end;
        end;
      end;

    finally
      Result := SafeArrayUnaccessData(ASafeArr);
      if Failed(Result) then
      begin
        SafeArrayDestroy(ASafeArr);
        ASafeArr := nil;
      end;
    end;

  finally
    vRecords.FRecList.UnlockList;
  end;
end;

/// <summary>
/// Функция распаковки записей лога из безопасного массива.
/// </summary>
/// <param name="ASafeArr">
/// Безопасный массив для распаковки.
/// </param>
/// <param name="AData">
/// Указатель объекта, наследника TSANLogRecords.
/// </param>
/// <returns>
/// HRESULT. Код ошибки.
/// </returns>
function PSALogRecordsFromSafeArray(const ASafeArr : PSafeArray; const AData : Pointer) : HResult;
var
  vRecords : TSANLogRecords;
  vList : TList;
  vDataP, vRecP : PVarDataArray;
  vDataRecPSA : PSafeArray;
  i, vBound, vUBound : integer;
  vVT : TVarType;
begin

  if ASafeArr = nil then
    Exit(E_InvalidArg);

  // Проверяем, что массив AData одномерный
  if SafeArrayGetDim(ASafeArr) <> 1 then
    Exit(E_InvalidArg);

  // Проверяем, что нижний индекс массива равен нулю
  Result := SafeArrayGetLBound(ASafeArr, 1, vBound);
  if Failed(Result) then
    Exit;
  if vBound <> 0 then
    Exit(E_InvalidArg);

  // Проверяем, что элементы массива имеют тип Variant
  Result := SafeArrayGetVartype(ASafeArr, vVT);
  if Failed(Result) then
    Exit;
  if vVT <> varVariant then
    Exit(E_InvalidArg);


  Result := SafeArrayAccessData(ASafeArr, PPointer(@vDataP)^);
  if Failed(Result) then
    Exit;

  try
    Result := SafeArrayGetUBound(ASafeArr, 1, vUBound);
    if Failed(Result) then
      Exit;

    vRecords := TSANLogRecords(AData);
    vList := vRecords.FRecList.LockList;
    try

      for i := 0 to vUBound do
      begin
        vDataRecPSA := PSafeArray(vDataP^[i].VArray);

        // Проверяем, что массив AData одномерный
        if SafeArrayGetDim(vDataRecPSA) <> 1 then
          Exit(E_InvalidArg);

        // Проверяем, что нижний индекс массива равен нулю
        Result := SafeArrayGetLBound(vDataRecPSA, 1, vBound);
        if Failed(Result) then
          Exit;
        if vBound <> 0 then
          Exit(E_InvalidArg);

        // Проверяем, что верхний индекс массива равен четырем
        Result := SafeArrayGetUBound(vDataRecPSA, 1, vBound);
        if Failed(Result) then
          Exit;
        if vBound <> 4 then
          Exit(E_InvalidArg);

        // Проверяем, что элементы массива имеют тип Variant
        Result := SafeArrayGetVartype(vDataRecPSA, vVT);
        if Failed(Result) then
          Exit;
        if vVT <> varVariant then
          Exit(E_InvalidArg);


        Result := SafeArrayAccessData(vDataRecPSA, PPointer(@vRecP)^);
        if Failed(Result) then
          Exit;

        try

          // Проверка типов элементов
          if (vRecP^[0].VType <> varInteger)
          or (vRecP^[1].VType <> varDouble)
          or (vRecP^[2].VType <> varInteger)
          or (vRecP^[3].VType <> varOleStr)
          or (vRecP^[4].VType <> varOleStr)
          then
            Exit(E_InvalidArg);


          vRecords.DoAdd
          (
            vList,
            vRecP^[0].VInteger,
            vRecP^[1].VDouble,
            vRecP^[2].VInteger,
            vRecP^[3].VOleStr,
            vRecP^[4].VOleStr
          );

        finally
          if Failed(Result) then
            SafeArrayUnaccessData(vDataRecPSA)
          else
            Result := SafeArrayUnaccessData(vDataRecPSA);
        end;

      end;

    finally
      vRecords.FRecList.UnlockList;
    end;

  finally
    if Failed(Result) then
      SafeArrayUnaccessData(ASafeArr)
    else
      Result := SafeArrayUnaccessData(ASafeArr)
  end;

end;
//==============================================================================

type
  TSANLogsPathNotifier = class(TInterfacedObject, ISANLogPathNotifier)
  private
    FLog : TSANCustomLog;

  protected
    // ISANLogPathNotifier
    procedure ValueChanging(const APathPart : ISANLogPathPart;
      const AOldValue, ANewValue : OleVariant); safecall;
    procedure ValueChanged(const APathPart : ISANLogPathPart;
      const AOldValue, ANewValue : OleVariant); safecall;
    procedure Destroying(const APath : ISANLogPath); safecall;

  public
    constructor Create(const ALog : TSANCustomLog);
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

  end;

// protected
procedure TSANLogsPathNotifier.ValueChanging(const APathPart: ISANLogPathPart;
  const AOldValue, ANewValue: OleVariant);
begin
  FLog.DoPathPartChanging(APathPart, AOldValue, ANewValue);
end;

procedure TSANLogsPathNotifier.ValueChanged(const APathPart: ISANLogPathPart;
  const AOldValue, ANewValue: OleVariant);
begin
  FLog.DoPathPartChanged(APathPart, AOldValue, ANewValue);
end;

procedure TSANLogsPathNotifier.Destroying(const APath: ISANLogPath);
begin
end;

// public
constructor TSANLogsPathNotifier.Create(const ALog: TSANCustomLog);
begin
  inherited Create;

  FLog := ALog;
end;

function TSANLogsPathNotifier.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{B327D058-7E92-40BD-B962-7488F4DCBCE1}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogRecord
// protected
function TSANLogRecord.Get_ID: integer;
begin
  Result := FID;
end;

procedure TSANLogRecord.Set_ID(const AID: integer);
begin
  FID := AID;
end;

function TSANLogRecord.Get_DateTime: TDateTime;
begin
  Result := FDateTime;
end;

procedure TSANLogRecord.Set_DateTime(const ADateTime: TDateTime);
begin
  FDateTime := ADateTime;
end;

function TSANLogRecord.Get_Kind: Longword;
begin
  Result := FKind;
end;

procedure TSANLogRecord.Set_Kind(const AKind: Longword);
begin
  FKind := AKind;
end;

function TSANLogRecord.Get_SubType: Widestring;
begin
  Result := FSubType
end;

procedure TSANLogRecord.Set_SubType(const ASubType: WideString);
begin
  FSubType := ASubType;
end;

function TSANLogRecord.Get_Msg: Widestring;
begin
  Result := FMsg
end;

procedure TSANLogRecord.Set_Msg(const AMsg: WideString);
begin
  FMsg := AMsg;
end;

procedure TSANLogRecord.Clear;
begin
  FID        := 0;
  FDateTime  := 0;
  FKind      := 0;
  FSubType   := '';
  FMsg       := '';
end;

procedure TSANLogRecord.From(const AID: integer; const ADateTime: TDateTime;
  const AKind: LongWord; const ASubType, AMsg: WideString);
begin
  FID        := AID;
  FDateTime  := ADateTime;
  FKind      := AKind;
  FSubType   := ASubType;
  FMsg       := AMsg;
end;

procedure TSANLogRecord.AssignRec(const ASource: ISANLogRecord);
begin
  FID        := ASource.ID;
  FDateTime  := ASource.DateTime;
  FKind      := ASource.Kind;
  FSubType   := ASource.SubType;
  FMsg       := ASource.Msg;
end;

function TSANLogRecord.IsEqualsRecords(const ALogRecord: ISANLogRecord): WordBool;
begin
  Result := (FID        = ALogRecord.ID)
        and (FDateTime  = ALogRecord.DateTime)
        and (FKind      = ALogRecord.Kind)
        and (FSubType   = ALogRecord.SubType)
        and (FMsg       = ALogRecord.Msg);
end;

//public
constructor TSANLogRecord.CreateFrom(const AParent : TObject; const AID: integer; const ADateTime: TDateTime;
  const AKind: LongWord; const ASubType, AMsg: WideString);
begin
  inherited Create(AParent);

  From(AID, ADateTime, AKind, ASubType, AMsg);
end;

constructor TSANLogRecord.CreateAssigned(const AParent : TObject; const ASource: ISANLogRecord);
begin
  inherited Create(AParent);

  AssignRec(ASource);
end;

function TSANLogRecord.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{34EE04E2-D769-4576-B6CE-8F7F78276BFF}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogRecordsEnumirator
// protected
function TSANLogRecordsEnumirator.Get_Count: integer;
begin
  Result := LockListRef.Count;
end;

function TSANLogRecordsEnumirator.Get_Records(const Index: integer): ISANLogRecord;
begin
  Result := TSANLogRecord(LockListRef[Index]);
end;

// public
function TSANLogRecordsEnumirator.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{9630A23C-3D17-4B43-BF07-B1E6C502E9F6}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogRecords
// protected
procedure TSANLogRecords.DoClearItem(const APtr: Pointer);
begin

  if Assigned(FNotifier) then
    FNotifier.Removing(Self, TSANLogRecord(APtr));

  TSANLogRecord(APtr).ReleaseNotParent;
end;

function TSANLogRecords.DoAdd(const AList : TList; const ARecord : TSANLogRecord) : TSANLogRecord;
begin
  try
    ARecord.AddRefNotParent;
  except
    ARecord.Free;
    raise;
  end;

  AList.Add(ARecord);

  if Assigned(FNotifier) then
    FNotifier.Added(Self, ARecord);

  Result := ARecord;
end;

function TSANLogRecords.DoAdd(const AList: TList; const AID: integer;
  const ADateTime: TDateTime; const AKind: LongWord; const ASubType,
  AMsg: WideString): TSANLogRecord;
begin
  Result := DoAdd(AList, TSANLogRecord.CreateFrom(Self, AID, ADateTime, AKind, ASubType, AMsg));
end;

function TSANLogRecords.Get_Count: integer;
begin
  with FRecList.LockList do
  try
    Result := Count;
  finally
    FRecList.UnlockList;
  end;
end;

function TSANLogRecords.Get_Records(const Index: integer): ISANLogRecord;
begin
  with FRecList.LockList do
  try
    Result := TSANLogRecord(Items[Index]);
  finally
    FRecList.UnlockList;
  end;
end;

function TSANLogRecords.Enumiration: ISANLogRecordsEnumirator;
begin
  Result := TSANLogRecordsEnumirator.Create(FRecList, Self);
end;

procedure TSANLogRecords.Clear;
begin
  if Assigned(FRecList) then
    SANListClear(FRecList, DoClearItem);
end;

function TSANLogRecords.FindByID(out ALogRecord: ISANLogRecord;
  const AID: integer): WordBool;
var
  vLR : TSANLogRecord;
begin
  Result := SANListFind(vLR, FRecList, SANListFF_LogRecByID, Pointer(AID));

  if Result then
    ALogRecord := vLR;
end;

function TSANLogRecords.ExtractByID(const AID: integer): ISANLogRecord;
var
  vLR : TSANLogRecord;
begin
  if SANListExtract(vLR, FRecList, SANListFF_LogRecByID, Pointer(AID)) then
    begin
      if Assigned(FNotifier) then
        FNotifier.Extracted(Self, vLR);

      Result := vLR
    end
  else
    Result := nil;
end;

function TSANLogRecords.RemoveByID(const AID: integer): WordBool;
begin
  Result := SANListRemove(FRecList, SANListFF_LogRecByID, DoClearItem, Pointer(AID));
end;

procedure TSANLogRecords.Delete(const AIndex: integer);
var
  vRec : TSANLogRecord;
begin
  with FRecList.LockList do
  try
    vRec := TSANLogRecord(Items[AIndex]);
    Delete(AIndex);
    DoClearItem(vRec);
  finally
    FRecList.UnlockList;
  end;
end;

function TSANLogRecords.Add(const AID: integer; const ADateTime: TDateTime;
  const AKind: LongWord; const ASubType, AMsg: WideString): ISANLogRecord;
var
  vList : TList;
begin
  vList := FRecList.LockList;
  try

    Result := DoAdd(vList, AID, ADateTime, AKind, ASubType, AMsg);

  finally
    FRecList.UnlockList;
  end;
end;

function TSANLogRecords.AddAsCopy(const ALogRecord: ISANLogRecord): ISANLogRecord;
var
  vList : TList;
begin
  vList := FRecList.LockList;
  try

    Result := DoAdd(vList, TSANLogRecord.CreateAssigned(Self, ALogRecord) );

  finally
    FRecList.UnlockList;
  end;
end;

function TSANLogRecords.Filter(AFilter: ISANLogRecordsFilter): integer;
var
  vList : TList;
  i : integer;
  vLR : TSANLogRecord;
begin
  Result := 0;
  vList := FRecList.LockList;
  try

    i := 0;
    while i <= vList.Count - 1 do
    begin
      vLR := TSANLogRecord(vList[i]);

      if  NOA(AFilter.FirstID > -1, vLR.FID >= AFilter.FirstID)
      and NOA(AFilter.LastID  > -1, vLR.FID <= AFilter.LastID)
      and AFilter.Filtered(vLR)
      then
      begin
        vLR.ReleaseNotParent;
        vList.Delete(i);
        Inc(Result);
        Continue;
      end;

      Inc(i);
    end;

  finally
    FRecList.UnlockList;
  end;

  if Assigned(FNotifier) then
    FNotifier.Filtered(Self, Result);
end;

procedure TSANLogRecords.ExportRecords(out ARecords: OleVariant);
begin
  PSAPackDataToOleVariant(ARecords, PSALogRecordsToSafeArray, Self);
end;

procedure TSANLogRecords.ImportRecords(var ARecords: OleVariant;
  const AReWrite: WordBool);
begin
  if AReWrite then
    Clear;

  PSAUnPackDataFromOleVariant(ARecords, PSALogRecordsFromSafeArray, Self);
end;

function TSANLogRecords.Get_Notifier: ISANLogRecordsNotifier;
begin
  Result := FNotifier;
end;

procedure TSANLogRecords.Set_Notifier(const ANotifier: ISANLogRecordsNotifier);
begin
  FNotifier := ANotifier;
end;

// public
constructor TSANLogRecords.Create(const AParent : TObject);
begin
  inherited Create(AParent);

  FRecList := TThreadList.Create;
end;

destructor TSANLogRecords.Destroy;
begin

  if Assigned(FNotifier) then
  begin
    FNotifier.Destroying(Self);
    FNotifier := nil;
  end;

  Clear;

  FreeAndNil(FRecList);

  inherited;
end;

function TSANLogRecords.SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{9A471060-6ED1-44B0-820D-2DE95F0BAC02}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogPathPart
// private
function TSANLogPathPart.Get_Parent: TSANLogPath;
begin
  Result := (inherited Parent) as TSANLogPath;
end;

// protected
procedure TSANLogPathPart.DoValueChanging(const AOldValue, ANewValue: OleVariant);
begin
  Parent.FNotifiersList.NotifyValueChanging(Self, AOldValue, ANewValue);
end;

procedure TSANLogPathPart.DoValueChanged(const AOldValue, ANewValue: OleVariant);
begin
  Parent.FNotifiersList.NotifyValueChanged(Self, AOldValue, ANewValue);
end;

function TSANLogPathPart.Get_ParentPath: ISANLogPath;
begin
  Result := Parent;
end;

function TSANLogPathPart.Get_Name: WideString;
begin
  Result := FName;
end;

function TSANLogPathPart.Get_Value: OleVariant;
begin
  Result := FValue;
end;

procedure TSANLogPathPart.Set_Value(const AValue: OleVariant);
var
  vOldValue : OleVariant;
begin

  if VarCompareValue(FValue, AValue) = vrEqual then
    Exit;

  vOldValue := FValue;

  DoValueChanging(vOldValue, AValue);

  FValue := AValue;

  DoValueChanged(vOldValue, AValue);
end;

function TSANLogPathPart.IsEqualsPathParts(const APathPart: ISANLogPathPart): WordBool;
begin
  Result := (AnsiCompareText(FName, APathPart.Name) = 0)
        and (VarCompareValue(FValue, APathPart.Value) = vrEqual);
end;

// public
constructor TSANLogPathPart.Create(const AParentPath : TSANLogPath; const AName: string);
begin
  inherited Create(AParentPath);

  FName := AName;
end;

function TSANLogPathPart.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{0EFAE823-3C11-40E6-8C7E-BEF05749233E}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogPathPartsEnumirator
// protected
function TSANLogPathPartsEnumirator.Get_Count: integer;
begin
  Result := LockListRef.Count;
end;

function TSANLogPathPartsEnumirator.Get_PathParts(const Index: integer): ISANLogPathPart;
begin
  Result := TSANLogPathPart(LockListRef[Index]);
end;

function TSANLogPathPartsEnumirator.FindByName(out APathPart: ISANLogPathPart;
  const AName: WideString): WordBool;
var
  vLPP : TSANLogPathPart;
begin
  Result := SANListFind(vLPP, LockListRef, SANListFF_LogsNamedFindByName, @AName);

  if Result then
    APathPart := vLPP;
end;

function TSANLogPathPartsEnumirator.RemoveByName(const AName: WideString): WordBool;
begin
  Result := SANListRemove(LockListRef, SANListFF_LogsNamedFindByName, ReleaseIntfObjChildOnly, @AName);
end;

procedure TSANLogPathPartsEnumirator.Delete(const AIndex: integer);
var
  vLPP : TSANLogPathPart;
begin
  vLPP := TSANLogPathPart(LockListRef[AIndex]);

  Delete(AIndex);

  vLPP.ReleaseNotParent;
end;

function TSANLogPathPartsEnumirator.Add(const AName: WideString): ISANLogPathPart;
var
  vLPP : TSANLogPathPart;
begin
  (Self.Parent as TSANLogPath).CreatePathPart(vLPP, AName);

  try
    vLPP.AddRefNotParent;
  except
    FreeAndNil(vLPP);
    raise;
  end;

  LockListRef.Add(vLPP);

  Result := vLPP;
end;

// public
function TSANLogPathPartsEnumirator.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{C4CE0501-D7AF-4AC4-922D-7BF06347D5E8}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogPathNotifiers
// private
function TSANLogPathNotifiers.Get_Parent: TSANLogPath;
begin
  Result := (inherited Parent) as TSANLogPath;
end;

// protected
procedure TSANLogPathNotifiers.NotifyValueChanging(const APathPart: ISANLogPathPart;
   const AOldValue, ANewValue: OleVariant);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogPathNotifier).ValueChanging(APathPart, AOldValue, ANewValue);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANLogPathNotifiers.NotifyValueChanged(const APathPart: ISANLogPathPart;
  const AOldValue, ANewValue: OleVariant);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogPathNotifier).ValueChanged(APathPart, AOldValue, ANewValue);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANLogPathNotifiers.NotifyDestroying;
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogPathNotifier).Destroying(Self.Parent);

  finally
    Notifiers.UnlockList
  end;
end;
//==============================================================================

//==============================================================================
// TSANLogPath
// private
function TSANLogPath.Get_ObjStorage: TSANCustomLogsStorage;
begin
  Result := (Parent as TSANCustomLogsStorage);
end;

// protected
class function TSANLogPath.PathPartClass: TSANLogPathPartClass;
begin
  Result := TSANLogPathPart;
end;

procedure TSANLogPath.CreatePathPart(out APathPart; const AName: string);
begin
  TSANLogPathPart(APathPart) := PathPartClass.Create(Self, AName);
end;

procedure TSANLogPath.DoDelete(const AList: TList; const AIndex: integer);
var
  vLPP : TSANLogPathPart;
begin
  vLPP := TSANLogPathPart(AList[AIndex]);

  AList.Delete(AIndex);

  vLPP.ReleaseNotParent;
end;

function TSANLogPath.DoAdd(const AList: TList; const AName: string): TSANLogPathPart;
begin
  CreatePathPart(Result, AName);

  try
    Result.AddRefNotParent;
  except
    FreeAndNil(Result);
    raise;
  end;

  AList.Add(Result);
end;

function TSANLogPath.Get_Count: integer;
begin
  with FPathPartsList.LockList do
  try
    Result := Count;
  finally
    FPathPartsList.UnlockList;
  end;
end;

function TSANLogPath.Get_PathParts(const Index: integer): ISANLogPathPart;
begin
  with FPathPartsList.LockList do
  try
    Result := TSANLogPathPart(Items[Index]);
  finally
    FPathPartsList.UnlockList;
  end;
end;

function TSANLogPath.FindByName(out APathPart: ISANLogPathPart;
  const AName: WideString): WordBool;
var
  vLPP : TSANLogPathPart;
begin
  Result := SANListFind(vLPP, FPathPartsList, SANListFF_LogsNamedFindByName, @AName);

  if Result then
    APathPart := vLPP;
end;

function TSANLogPath.RemoveByName(const AName: WideString): WordBool;
begin
  Result := SANListRemove(FPathPartsList, SANListFF_LogsNamedFindByName, ReleaseIntfObjChildOnly, @AName);
end;

procedure TSANLogPath.Delete(const AIndex: integer);
var
  vList : TList;
begin
  vList := FPathPartsList.LockList;
  try
    DoDelete(vList, AIndex);
  finally
    FPathPartsList.UnlockList;
  end;
end;

function TSANLogPath.Add(const AName: WideString): ISANLogPathPart;
var
  vList : TList;
begin
  vList := FPathPartsList.LockList;
  try
    Result := DoAdd(vList, AName);
  finally
    FPathPartsList.UnlockList;
  end;
end;

function TSANLogPath.Get_Storage: ISANLogsStorage;
begin
  Result := Storage;
end;

function TSANLogPath.Enumiration: ISANLogPathPartsEnumirator;
begin
  Result := TSANLogPathPartsEnumirator.Create(FPathPartsList, Self);
end;

function TSANLogPath.IsEqualsPath(const APath: ISANLogPath): WordBool;
var
  vPathEnum : ISANLogPathPartsEnumirator;
  i : integer;
begin

  if not (Assigned(APath) and Storage.IsEqualsStorages(APath.Storage)) then
    Exit(False);

  vPathEnum := APath.Enumiration;

  with FPathPartsList.LockList do
  try

    if vPathEnum.Count <> Count then
      Exit(False);

    for i := 0 to Count - 1 do
      if not TSANLogPathPart(Items[i]).IsEqualsPathParts(vPathEnum.PathParts[i]) then
        Exit(False);

  finally
    FPathPartsList.UnlockList;
  end;

  Result := True;
end;

function TSANLogPath.AddNotifier(const APathNotifier: ISANLogPathNotifier): integer;
begin
  Result := FNotifiersList.AddNotifier(APathNotifier)
end;

function TSANLogPath.RemoveNotifier(const APathNotifierID: integer): WordBool;
begin
  Result := FNotifiersList.RemoveNotifier(APathNotifierID);
end;

// public
constructor TSANLogPath.Create(const AStorage : TSANCustomLogsStorage; const ANames : array of string);
var
  vList : TList;
  i : integer;
begin
  inherited Create(AStorage);

  FPathPartsList := TThreadList.Create;

  vList := FPathPartsList.LockList;
  try

    for i := Low(ANames) to High(ANames) do
      DoAdd(vList, ANames[i]);

  finally
    FPathPartsList.UnlockList;
  end;

  FNotifiersList := TSANLogPathNotifiers.Create(Self);
  try
    FNotifiersList.AddRefNotParent;
  except
    FreeAndNil(FNotifiersList);
    raise;
  end;
end;

destructor TSANLogPath.Destroy;
begin

  if Assigned(FNotifiersList) then
  begin
    FNotifiersList.NotifyDestroying;
    FNotifiersList.ReleaseNotParent;
    FNotifiersList := nil;
  end;

  if Assigned(FPathPartsList) then
    SANListClear(FPathPartsList, ReleaseIntfObjChildOnly);

  FreeAndNil(FPathPartsList);
  inherited;
end;

function TSANLogPath.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{D5410F15-B2DF-4586-8427-5AA7329804C4}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANCustomLogNotifiers
// private
function TSANCustomLogNotifiers.Get_Parent: TSANCustomLog;
begin
  Result := (inherited Parent) as TSANCustomLog;
end;

// protected
procedure TSANCustomLogNotifiers.NotifyRecordWrited(const ALogRecord: ISANLogRecord);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogNotifier).RecordWrited(Self.Parent, ALogRecord);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANCustomLogNotifiers.NotifyRecordsWrited(const ALogRecords: ISANLogRecords; const AReWrite: WordBool);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogNotifier).RecordsWrited(Self.Parent, ALogRecords, AReWrite);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANCustomLogNotifiers.NotifyFiltered(const AFilteredCount: integer);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogNotifier).Filtered(Self.Parent, AFilteredCount);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANCustomLogNotifiers.NotifyDestroying;
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogNotifier).Destroying(Self.Parent);

  finally
    Notifiers.UnlockList
  end;
end;
//==============================================================================

//==============================================================================
// TSANCustomLog
// private
function TSANCustomLog.Get_ObjStorage: TSANCustomLogsStorage;
begin
  Result := (Parent as TSANCustomLogsStorage);
end;

// protected
procedure TSANCustomLog.VlidateSecondCallBeginWrite;
begin
  if FWriteInProgress then
    raise ESANLogError.CreateResFmt(@sSecondCallBeginWriteErrorMessage, [FName]);
end;

procedure TSANCustomLog.VlidateNotCallBeginWrite;
begin
  if not FWriteInProgress then
    raise ESANLogError.CreateResFmt(@sNotCallBeginWriteErrorMessage, [FName]);
end;

procedure TSANCustomLog.DoAfterChangePath(const AOldPath, ANewPath: ISANLogPath);
begin
end;

procedure TSANCustomLog.DoBeforeChangePath(const AOldPath, ANewPath: ISANLogPath);
begin
end;

procedure TSANCustomLog.DoPathPartChanging(const APathPart: ISANLogPathPart;
  const AOldValue, ANewValue: OleVariant);
begin
end;

procedure TSANCustomLog.DoPathPartChanged(const APathPart: ISANLogPathPart;
  const AOldValue, ANewValue: OleVariant);
begin
end;

function TSANCustomLog.Write(const AKind: LongWord; const ASubType,
  AMessage: WideString; ADateTime: TDateTime): integer;
begin
  VlidateSecondCallBeginWrite;

  if ADateTime = 0 then
    ADateTime := Now;

  DoWrite(ADateTime, AKind, ASubType, AMessage, Result);

  FLastRec.From(Result, ADateTime, AKind, ASubType, AMessage);

  FLogNotifiers.NotifyRecordWrited(FLastRec);

  FLastRec.Clear;
end;

function TSANCustomLog.BeginWrite(const AKind: LongWord;
  const ASubType: WideString; ADateTime: TDateTime): integer;
begin
  VlidateSecondCallBeginWrite;

  if ADateTime = 0 then
    ADateTime := Now;

  DoBeginWrite(ADateTime, AKind, ASubType, Result);

  FLastRec.From(Result, ADateTime, AKind, ASubType, '');

  FWriteInProgress := True;
end;

procedure TSANCustomLog.PartialWrite(const APartialMessage: WideString);
begin
  VlidateNotCallBeginWrite;

  DoPartialWrite(APartialMessage);

  FLastRec.FMsg := FLastRec.FMsg + APartialMessage;
end;

procedure TSANCustomLog.EndWrite;
begin
  VlidateNotCallBeginWrite;

  DoEndWrite;

  FLogNotifiers.NotifyRecordWrited(FLastRec);

  FWriteInProgress := False;
end;

function TSANCustomLog.Get_Name: WideString;
begin
  Result := FName;
end;

function TSANCustomLog.Get_Path: ISANLogPath;
begin
  Result := FPath;
end;

procedure TSANCustomLog.Set_Path(const APath: ISANLogPath);
var
  vOldPath : ISANLogPath;
begin
  Storage.ValidatePath(APath);

  if SANLogsPathsIsEquals(FPath, APath) then
    Exit;

  vOldPath := FPath;

  if Assigned(vOldPath) then
    vOldPath.RemoveNotifier(FPathNotifierID);

  DoBeforeChangePath(vOldPath, APath);

  FPath := APath;

  DoAfterChangePath(vOldPath, APath);

  if Assigned(APath) then
    FPathNotifierID := APath.AddNotifier(FPathNotifier);
end;

function TSANCustomLog.Get_Storage: ISANLogsStorage;
begin
  Result := Storage;
end;

procedure TSANCustomLog.WriteRecords(const ALogRecords: ISANLogRecords;
  const AReWrite: WordBool);
begin
  VlidateSecondCallBeginWrite;

  DoWriteRecords(ALogRecords, AReWrite);

  FLogNotifiers.NotifyRecordsWrited(ALogRecords, AReWrite);
end;

procedure TSANCustomLog.Read(const ALogRecords: ISANLogRecords; const AFirstID,
  ALastID: integer);
begin
  DoRead(ALogRecords, AFirstID, ALastID);
end;

function TSANCustomLog.Filter(AFilter : ISANLogRecordsFilter) : integer;
begin
  DoFilter(AFilter, Result);

  FLogNotifiers.NotifyFiltered(Result);
end;

function TSANCustomLog.IsEqualsLogs(const ALog: ISANLog): WordBool;
begin
  Result := Storage.IsEqualsStorages(ALog.Storage)
        and SANLogsPathsIsEquals(FPath, ALog.Path);
end;

function TSANCustomLog.AddNotifier(const ALogNotifier: ISANLogNotifier): integer;
begin
  Result := FLogNotifiers.AddNotifier(ALogNotifier);
end;

function TSANCustomLog.RemoveNotifier(const ALogNotifierID: integer): WordBool;
begin
  Result := FLogNotifiers.RemoveNotifier(ALogNotifierID);
end;

// public
constructor TSANCustomLog.Create(const AStorage : TSANCustomLogsStorage;
  const AName : string; const APath : ISANLogPath);
begin
  inherited Create(AStorage);

  FName := AName;

  FPathNotifier := TSANLogsPathNotifier.Create(Self);

  FLogNotifiers := TSANCustomLogNotifiers.Create(Self);
  try
    FLogNotifiers.AddRefNotParent;
  except
    FreeAndNil(FLogNotifiers);
    raise;
  end;

  FLastRec := TSANLogRecord.Create(Self);
  try
    FLastRec.AddRefNotParent;
  except
    FreeAndNil(FLastRec);
    raise;
  end;

  Set_Path(APath);
end;

destructor TSANCustomLog.Destroy;
begin

  if FWriteInProgress then
    EndWrite;

  if Assigned(FLogNotifiers) then
  begin
    FLogNotifiers.NotifyDestroying;
    FLogNotifiers.ReleaseNotParent;
    FLogNotifiers := nil;
  end;

  if Assigned(FPathNotifier) and Assigned(FPath) then
    FPath.RemoveNotifier(FPathNotifierID);

  FPathNotifier := nil;
  FPath := nil;

  FLastRec.ReleaseNotParent;
  FLastRec := nil;

  inherited;
end;

function TSANCustomLog.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{909D3AC2-2FC5-44B3-8B93-044A0ECEBC10}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogInfo
// private
function TSANLogInfo.Get_ObjStorage: TSANCustomLogsStorage;
begin
  Result := Parent as TSANCustomLogsStorage;
end;

// protected
function TSANLogInfo.Get_Name: WideString;
begin
  Result := FName;
end;

function TSANLogInfo.Get_Path: ISANLogPath;
begin
  Result := FPath;
end;

function TSANLogInfo.Get_Storage: ISANLogsStorage;
begin
  Result := Storage;
end;

function TSANLogInfo.Get_Log: ISANLog;
begin
  Result := FLog;
end;

function TSANLogInfo.OpenLog: ISANLog;
begin

  if not Assigned(FLog) then
    FLog := Storage.AddLog(FName, FPath);

  Result := FLog;
end;

// public
constructor TSANLogInfo.Create(const AStorage: TSANCustomLogsStorage;
  const AName: string; const APath: ISANLogPath; const ALog: ISANLog);
begin
  inherited Create(AStorage);

  FName     := AName;
  FPath     := APath;
  FLog      := ALog;
end;

destructor TSANLogInfo.Destroy;
begin
  FPath  := nil;
  FLog   := nil;

  inherited;
end;

function TSANLogInfo.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{781DC9DF-6450-48CD-A83B-EA737977EA16}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogInfosEnumirator
// protected
function TSANLogInfosEnumirator.Get_Count: integer;
begin
  Result := FLogInfos.Count;
end;

function TSANLogInfosEnumirator.Get_LogInfos(const Index: integer): ISANLogInfo;
begin
  Result := TSANLogInfo(FLogInfos[Index]);
end;

// public
constructor TSANLogInfosEnumirator.Create(const AParent : TObject; const ALogInfos: TList);
begin
  inherited Create(AParent);
  FLogInfos := ALogInfos;
end;

destructor TSANLogInfosEnumirator.Destroy;
begin

  if Assigned(FLogInfos) then
  begin
    SANListClear(FLogInfos, ReleaseIntfObjChildOnly);
    FreeAndNil(FLogInfos);
  end;

  inherited;
end;

function TSANLogInfosEnumirator.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{0D744305-1099-432E-98FD-4CEA2622F829}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANCustomLogsEnumirator
// private
function TSANCustomLogsEnumirator.Get_Count: integer;
begin
  Result := LockListRef.Count;
end;

function TSANCustomLogsEnumirator.Get_Logs(const Index: integer): ISANLog;
begin
  Result := TSANCustomLog(LockListRef[Index]);
end;

// public
function TSANCustomLogsEnumirator.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{681B2161-B3EB-458B-B787-638316A6BC82}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANCustomLogsStorageNotifiers
// private
function TSANCustomLogsStorageNotifiers.Get_Parent: TSANCustomLogsStorage;
begin
  Result := inherited Parent as TSANCustomLogsStorage;
end;

// protected
procedure TSANCustomLogsStorageNotifiers.NotifyLogAdded(const ALog: ISANLog);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogsStorageNotifier).LogAdded(Self.Parent, ALog);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANCustomLogsStorageNotifiers.NotifyLogRemoving(const ALog: ISANLog);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogsStorageNotifier).LogRemoving(Self.Parent, ALog);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANCustomLogsStorageNotifiers.NotifyDestroying;
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogsStorageNotifier).Destroying(Self.Parent);

  finally
    Notifiers.UnlockList
  end;
end;
//==============================================================================

//==============================================================================
// TSANCustomLogsStorage
// protected
procedure TSANCustomLogsStorage.ValidatePath(const APath: ISANLogPath);
var
  WS : WideString;
begin
  if not CheckPathIsCorrect(APath, WS) then
    raise ESANLogError.CreateResFmt(@sInvalidPathErrorMessage, [FName, WS]);
end;

procedure TSANCustomLogsStorage.DoCreateLog(out ALog; const AName: WideString;
  const APath: ISANLogPath);
begin
  TObject(ALog) := LogClass.Create(Self, AName, APath);
end;

function TSANCustomLogsStorage.DoAddLog(const AList: TList;
  const AName: WideString; const APath: ISANLogPath): TSANCustomLog;
begin
  DoCreateLog(Result, AName, APath);

  try
    Result.AddRefNotParent;
  except
    FreeAndNil(Result);
    raise;
  end;

  FLogsList.Add(Result);

  FNotifiersList.NotifyLogAdded(Result);
end;

procedure TSANCustomLogsStorage.DoClearLog(const APtr: Pointer);
var
  vLog : TSANCustomLog;
begin
  vLog := TSANCustomLog(APtr);

  try
    FNotifiersList.NotifyLogRemoving(vLog);
  finally
    vLog.ReleaseNotParent;
  end;
end;

procedure TSANCustomLogsStorage.DoPathPartAvailableValuesList(
  const APathPart : ISANLogPathPart; out ACanBuildList : WordBool;
  out AValues: OleVariant);
begin
  ACanBuildList := False;
  AValues := Null;
end;

function TSANCustomLogsStorage.Get_LogsCount: integer;
begin
  with FLogsList.LockList do
  try
    Result := Count;
  finally
    FLogsList.UnlockList;
  end;
end;

function TSANCustomLogsStorage.Get_Logs(const Index: integer): ISANLog;
begin
  with FLogsList.LockList do
  try
    Result := TSANCustomLog(Items[Index]);
  finally
    FLogsList.UnlockList;
  end;
end;

function TSANCustomLogsStorage.Enumiration: ISANLogsEnumirator;
begin
  Result := TSANCustomLogsEnumirator.Create(FLogsList, Self);
end;

procedure TSANCustomLogsStorage.Clear;
begin
  if Assigned(FLogsList) then
    SANListClear(FLogsList, DoClearLog);
end;

function TSANCustomLogsStorage.FindByName(out ALog: ISANLog;
  const AName: WideString): WordBool;
var
  vLog : TSANCustomLog;
begin
  Result := SANListFind(vLog, FLogsList, SANListFF_LogsNamedFindByName, @AName);
  if Result then
    ALog := vLog;
end;

function TSANCustomLogsStorage.ExtractByName(const AName: WideString): ISANLog;
begin
  if not SANListExtract(Result, FLogsList, SANListFF_LogsNamedFindByName, @AName) then
    Result := nil;
end;

function TSANCustomLogsStorage.RemoveByName(const AName: WideString): WordBool;
begin
  Result := SANListRemove(FLogsList, SANListFF_LogsNamedFindByName, DoClearLog, @AName);
end;

function TSANCustomLogsStorage.Get_Name: WideString;
begin
  Result := FName;
end;

function TSANCustomLogsStorage.CreateNewPath: ISANLogPath;
begin
  DoCreateNewPath(Result);
end;

function TSANCustomLogsStorage.CheckPathIsCorrect(const APath : ISANLogPath; out AErrors : WideString) : WordBool;
begin

  if not Assigned(APath) then
    begin
      Result := False;
      AErrors := 'Передан nil-указатель в качестве пути.'
    end
  else
    DoCheckPathIsCorrect(APath, Result, AErrors);

end;

function TSANCustomLogsStorage.PathPartAvailableValuesList(
  const APathPart : ISANLogPathPart; out AValues: OleVariant): WordBool;
var
  vPath : ISANLogPath;
  vStorage : ISANLogsStorage;
begin
  vPath := APathPart.ParentPath;

  if not Assigned(vPath) then
    raise ESANLogError.CreateResFmt(@sIncorrectPathPartErrorMessage, [Get_Name]);

  vStorage := vPath.Storage;

  if not Assigned(vStorage) then
    raise ESANLogError.CreateResFmt(@sIncorrectPathPartErrorMessage, [Get_Name]);

  if not IsEqualsStorages(vStorage) then
    raise ESANLogError.CreateResFmt(@sOtherStoragePathErrorMessage, [Get_Name, vStorage.Name]);

  DoPathPartAvailableValuesList(APathPart, Result, AValues);
end;

function TSANCustomLogsStorage.AddLog(const AName: WideString;
  const APath: ISANLogPath): ISANLog;
var
  vList : TList;
begin
  vList := FLogsList.LockList;
  try
    Result := DoAddLog(vList, AName, APath);
  finally
    FLogsList.UnlockList;
  end;
end;

function TSANCustomLogsStorage.EnumirateLogsInfo(const APath: ISANLogPath): ISANLogInfosEnumirator;
begin
  DoBuildLogsInfoEnumiration(APath, Result);
end;

function TSANCustomLogsStorage.AddNotifier(const AStorageNotifier: ISANLogsStorageNotifier): integer;
begin
  Result := FNotifiersList.AddNotifier(AStorageNotifier);
end;

function TSANCustomLogsStorage.RemoveNotifier(const AStorageNotifierID: integer): WordBool;
begin
  Result := FNotifiersList.RemoveNotifier(AStorageNotifierID);
end;

function TSANCustomLogsStorage.IsEqualsStorages(const AStorage: ISANLogsStorage): WordBool;
begin
  Result := AnsiCompareText(FName, AStorage.Name) = 0;
end;

// public
constructor TSANCustomLogsStorage.Create(const AParent : TObject; const AName: string);
begin
  inherited Create(AParent);

  FName := AName;

  FLogsList := TThreadList.Create;

  FNotifiersList := TSANCustomLogsStorageNotifiers.Create(Self);
  try
    FNotifiersList.AddRefNotParent;
  except
    FreeAndNil(FNotifiersList);
    raise;
  end;
end;

destructor TSANCustomLogsStorage.Destroy;
begin
  Clear;

  if Assigned(FNotifiersList) then
  begin
    FNotifiersList.NotifyDestroying;
    FNotifiersList.ReleaseNotParent;
    FNotifiersList := nil;
  end;

  FreeAndNil(FLogsList);

  inherited;
end;

function TSANCustomLogsStorage.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{E0991BC0-21D4-4CCD-9236-747A563B0A98}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogsStoragesIntfEnumirator
// protected
function TSANLogsStoragesIntfEnumirator.Get_Count: integer;
begin
  Result := LockListRef.Count;
end;

function TSANLogsStoragesIntfEnumirator.Get_Storages(const Index: integer): ISANLogsStorage;
begin
  Result := TSANLogsStoragesIntfList.PStorageAndIDRec(LockListRef[Index]).Storage;
end;

// public
function TSANLogsStoragesIntfEnumirator.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{CB3B6E99-54F5-457E-AA09-9E7DE3D139C0}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogsStoragesIntfListNotifiers
// private
function TSANLogsStoragesIntfListNotifiers.Get_Parent: TSANLogsStoragesIntfList;
begin
  Result := (inherited Parent) as TSANLogsStoragesIntfList;
end;

// protected
procedure TSANLogsStoragesIntfListNotifiers.NotifyStorageAdded(
  const AStorage: ISANLogsStorage);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogsStoragesListNotifier).StorageAdded(Self.Parent, AStorage);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANLogsStoragesIntfListNotifiers.NotifyStorageRemoving(
  const AStorage: ISANLogsStorage);
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogsStoragesListNotifier).StorageRemoving(Self.Parent, AStorage);

  finally
    Notifiers.UnlockList
  end;
end;

procedure TSANLogsStoragesIntfListNotifiers.NotifyDestroying;
var
  i : integer;
begin
  with Notifiers.LockList do
  try

    for i := 0 to Count - 1 do
      (PNotifierItem(Items[i]).Notifier as ISANLogsStoragesListNotifier).Destroying(Self.Parent);

  finally
    Notifiers.UnlockList
  end;
end;
//==============================================================================

//==============================================================================
// TSANLogsStoragesIntfList
// protected
procedure TSANLogsStoragesIntfList.DoClearItem(const APtr: Pointer);
var
  P : PStorageAndIDRec;
begin
  P := PStorageAndIDRec(APtr);

  try

    FNotifiersList.NotifyStorageRemoving(P^.Storage);

  finally
    P^.Storage.RemoveNotifier(P^.NotifierID);
    P^.Storage := nil;
    Dispose(APtr);
  end;

end;

function TSANLogsStoragesIntfList.DoFindItem(const AItem, AValue: Pointer): boolean;
begin
  Result := AnsiCompareText(PStorageAndIDRec(AItem)^.Storage.Name, PWideString(AValue)^) = 0;
end;

function TSANLogsStoragesIntfList.DoAdd(const AList: TList;
  const AStorage: ISANLogsStorage): integer;
var
  P : PStorageAndIDRec;
begin

  New(P);
  try
    P^.Storage := AStorage;
    P^.NotifierID := AStorage.AddNotifier(FStoragesNotifier);

  except
    P^.Storage := nil;
    Dispose(P);
    raise;
  end;

  Result := AList.Add(P);

  FNotifiersList.NotifyStorageAdded(AStorage);
end;


function TSANLogsStoragesIntfList.Get_Count: integer;
begin
  with FStoragesList.LockList do
  try
    Result := Count;
  finally
    FStoragesList.UnlockList;
  end;
end;

function TSANLogsStoragesIntfList.Get_Storages(const Index: integer): ISANLogsStorage;
begin
  with FStoragesList.LockList do
  try
    Result := PStorageAndIDRec(Items[Index])^.Storage;
  finally
    FStoragesList.UnlockList;
  end;
end;

function TSANLogsStoragesIntfList.Enumiration: ISANLogsStoragesEnumirator;
begin
  Result := TSANLogsStoragesIntfEnumirator.Create(FStoragesList, Self);
end;

procedure TSANLogsStoragesIntfList.Clear;
begin
  if Assigned(FStoragesList) then
    SANListClear(FStoragesList, DoClearItem);
end;

function TSANLogsStoragesIntfList.FindByName(out AStorage: ISANLogsStorage;
  const AName: WideString): WordBool;
var
  vRec : PStorageAndIDRec;
begin
  Result := SANListFind(vRec, FStoragesList, DoFindItem, @AName);

  if Result then
    AStorage := vRec^.Storage;
end;

function TSANLogsStoragesIntfList.ExtractByName(const AName: WideString): ISANLogsStorage;
var
  vRec : PStorageAndIDRec;
begin
  if SANListExtract(vRec, FStoragesList, DoFindItem, @AName) then
    begin
      Result := vRec^.Storage;

      DoClearItem(vRec);
    end
  else
    Result := nil;
end;

function TSANLogsStoragesIntfList.RemoveByName(const AName: WideString): WordBool;
begin
  Result := SANListRemove(FStoragesList, DoFindItem, DoClearItem, @AName)
end;

function TSANLogsStoragesIntfList.Add(const AStorage: ISANLogsStorage): integer;
var
  vList : TList;
begin
  vList := FStoragesList.LockList;
  try
    Result := DoAdd(vList, AStorage);
  finally
    FStoragesList.UnlockList
  end;
end;

function TSANLogsStoragesIntfList.AddNotifier(
  const AStoragesListNotifier: ISANLogsStoragesListNotifier): integer;
begin
  Result := FNotifiersList.AddNotifier(AStoragesListNotifier);
end;

function TSANLogsStoragesIntfList.RemoveNotifier(
  const AStoragesListNotifierID: integer): WordBool;
begin
  Result := FNotifiersList.RemoveNotifier(AStoragesListNotifierID);
end;

// public
constructor TSANLogsStoragesIntfList.Create(const AParent : TObject);
begin
  inherited Create(AParent);

  FNotifiersList := TSANLogsStoragesIntfListNotifiers.Create(Self);
  try
    FNotifiersList.AddRefNotParent;
  except
    FreeAndNil(FNotifiersList);
    raise;
  end;

  FStoragesList := TThreadList.Create;
end;

destructor TSANLogsStoragesIntfList.Destroy;
begin
  Clear;

  FStoragesNotifier := nil;

  if Assigned(FNotifiersList) then
  begin
    FNotifiersList.NotifyDestroying;
    FNotifiersList.ReleaseNotParent;
    FNotifiersList := nil;
  end;

  FreeAndNil(FStoragesList);

  inherited;
end;

function TSANLogsStoragesIntfList.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{58CB3073-28B0-4599-84C3-17AE2590552B}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANLogsIntfEnumirator
// protected
function TSANLogsIntfEnumirator.Get_Count: integer;
begin
  Result := LockListRef.Count;
end;

function TSANLogsIntfEnumirator.Get_Logs(const Index : integer) : ISANLog;
begin
  Result := ISANLog(LockListRef[Index]);
end;

function TSANLogsIntfEnumirator.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{D2D635AB-5C4A-4ADD-8DFE-2A10BDDA27E5}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANCustomLogsManager
// protected
function TSANCustomLogsManager.Get_LogsCount: integer;
begin
  with FLogsIntfsList.LockList do
  try
    Result := Count;
  finally
    FLogsIntfsList.UnlockList;
  end;
end;

function TSANCustomLogsManager.Get_Logs(const Index: integer): ISANLog;
begin
  with FLogsIntfsList.LockList do
  try
    Result := ISANLog(Items[Index]);
  finally
    FLogsIntfsList.UnlockList;
  end;
end;

function TSANCustomLogsManager.Enumiration: ISANLogsEnumirator;
begin
  Result := TSANLogsIntfEnumirator.Create(FLogsIntfsList, Self);
end;

procedure TSANCustomLogsManager.Clear;
begin
  if Assigned(FLogsIntfsList) then
    SANListClear(FLogsIntfsList, ReleaseIntf);
end;

function TSANCustomLogsManager.FindByName(out ALog: ISANLog; const AName: WideString): WordBool;
begin
  Result := SANListFind(ALog, FLogsIntfsList, SANListFF_LogsNamedIntfFindByName, @AName);
end;

function TSANCustomLogsManager.ExtractByName(const AName: WideString): ISANLog;
begin
  if not SANListExtract(Result, FLogsIntfsList, SANListFF_LogsNamedIntfFindByName, @AName) then
    Result := nil;
end;

function TSANCustomLogsManager.RemoveByName(const AName: WideString): WordBool;
begin
  Result := SANListRemove(FLogsIntfsList, SANListFF_LogsNamedIntfFindByName, ReleaseIntf, @AName)
end;

function TSANCustomLogsManager.Get_Name: WideString;
begin
  Result := FName;
end;

function TSANCustomLogsManager.Get_Storages: ISANLogsStoragesList;
begin
  Result := FStoragesIntfList;
end;

procedure TSANCustomLogsManager.LogAdded(const AStorage: ISANLogsStorage;
  const ALog: ISANCustomLog);
var
  vIndex : integer;
begin
  with FLogsIntfsList.LockList do
  try
    vIndex := Add(nil);
    IInterface(List[vIndex]) := ALog;
  finally
    FLogsIntfsList.UnlockList;
  end;
end;

procedure TSANCustomLogsManager.LogRemoving(const AStorage: ISANLogsStorage;
  const ALog: ISANCustomLog);
var
  vList : TList;
begin
  vList := FLogsIntfsList.LockList;
  try

    if vList.Remove( Pointer(ALog) ) > -1 then
      ALog._Release;

  finally
    FLogsIntfsList.UnlockList;
  end;
end;

procedure TSANCustomLogsManager.StorageNotifierDestroying(const AStorage: ISANLogsStorage);
begin
//  FStoragesIntfList.RemoveByName(AStorage.Name);
end;

procedure TSANCustomLogsManager.StorageAdded(
  const AStorages: ISANLogsStoragesList; const AStorage: ISANLogsStorage);
begin
end;

procedure TSANCustomLogsManager.StorageRemoving(
  const AStorages: ISANLogsStoragesList; const AStorage: ISANLogsStorage);
begin
  AStorage.Clear;
end;

procedure TSANCustomLogsManager.StoragesListNotifierDestroying(
  const AStorages: ISANLogsStoragesList);
begin
end;

// public
constructor TSANCustomLogsManager.Create(const AName: string);
begin
  inherited Create;

  FName := AName;

  FLogsIntfsList := TThreadList.Create;

  FStoragesIntfList := TSANLogsStoragesIntfList.Create(Self);
  try
    FStoragesIntfList.AddRefNotParent;
  except
    FreeAndNil(FStoragesIntfList);
    raise;
  end;
  FStoragesIntfList.StoragesNotifier := Self;
  FStoragesListNotifierID := FStoragesIntfList.AddNotifier(Self);
end;

destructor TSANCustomLogsManager.Destroy;
begin
  Clear;

  if Assigned(FStoragesIntfList) then
  begin
    FStoragesIntfList.Clear;
    FStoragesIntfList.StoragesNotifier := nil;
    FStoragesIntfList.RemoveNotifier(FStoragesListNotifierID);
    FStoragesIntfList.ReleaseNotParent;
    FStoragesIntfList := nil;
  end;

  FreeAndNil(FLogsIntfsList);

  inherited;
end;

function TSANCustomLogsManager.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{CC03EACF-0432-48CC-BE39-FD06BBE11A3C}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================










END.
