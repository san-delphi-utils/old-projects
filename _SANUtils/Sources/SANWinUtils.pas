{*******************************************************}
{                                                       }
{       SANWinUtils                                     }
{       Функции работы с файлами и системой             }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANWinUtils;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, Types, ShellAPI, SHFolder, StrUtils, ComObj,
  SANMisc
  ;

TYPE
  ESANWinUtils = class(Exception);

/// <summary>
/// Получение иконки, ассоциированной с файлом или папкой.
/// </summary>
/// <param name="AFileOrDir">
/// Имя файла или папки.
/// </param>
/// <param name="ALarge">
/// При True берется большой размер иконки.
/// </param>
/// <returns>
/// HIcon. Дескриптор иконки.
/// </returns>
function AssociatedIcon(const AFileOrDir : string; const ALarge : boolean): HIcon;

/// <summary>
/// Путь к специальным папкам Windows из SHFolder. В случаи неудачи — исключение.
/// </summary>
/// <param name="AFolder">
/// Константа папки.
/// </param>
/// <param name="AHasEndSlash">
/// Слеш в конце пути. По умолчанию — есть.
/// </param>
/// <returns>
/// String. Путь к папке.
/// </returns>
function SpecialFolderPath(const AFolder : integer; const AHasEndSlash : boolean = True) : string;

/// <summary>
/// Заполняет список AStrings наденными в системе DOS-устройствами с именами,
/// начинающимеся с ANameStart.
/// </summary>
procedure DosDeviceStrings(const AStrings: TStrings; const ANameStart : string);

/// <summary>
/// Заполняет список AStrings наденными в системе COM-портами.
/// </summary>
procedure ComPortsStrings(const AStrings: TStrings);

/// <summary>
/// Попытка прочитать текстовый ресурс (AnsiString) по имени.
/// </summary>
/// <param name="AText">
/// Текст ресурса в виде ANSI-строки в случае успеха.
/// </param>
/// <param name="AInstance">
/// Дескриптор модуля в котором идет поиск рескрса.
/// </param>
/// <param name="AResName">
/// Имя ресурса.
/// </param>
/// <returns>
/// Boolean. Ресурс найден и прочитан.
/// </returns>
function TryReadTextResourceA(out AText: AnsiString; const AInstance : HMODULE; const AResName: string): boolean;
/// <summary>
/// Попытка прочитать текстовый ресурс (UnicodeString) по имени.
/// </summary>
/// <param name="AText">
/// Текст ресурса в виде UNICODE-строки в случае успеха.
/// </param>
/// <param name="AInstance">
/// Дескриптор модуля в котором идет поиск рескрса.
/// </param>
/// <param name="AResName">
/// Имя ресурса.
/// </param>
/// <returns>
/// Boolean. Ресурс найден и прочитан.
/// </returns>
function TryReadTextResourceW(out AText: string; const AInstance : HMODULE; const AResName: string): boolean;

/// <summary>
/// Прочитать текстовый ресурс (AnsiString) по имени, подняв системное исключение
/// в случае неудачи.
/// </summary>
/// <param name="AResName">
/// Имя ресурса.
/// </param>
/// <param name="AInstance">
/// Дескриптор модуля в котором идет поиск рескрса.
/// </param>
/// <returns>
/// AnsiString. Текст ресурса в виде ANSI-строки.
/// </returns>
function ReadTextResourceA(const AInstance : HMODULE; const AResName: string): AnsiString;
/// <summary>
/// Прочитать текстовый ресурс (UnicodeString) по имени, подняв системное исключение
/// в случае неудачи.
/// </summary>
/// <param name="AInstance">
/// Дескриптор модуля в котором идет поиск рескрса.
/// </param>
/// <param name="AResName">
/// Имя ресурса.
/// </param>
/// <returns>
/// String. Текст ресурса в виде UNICODE-строки.
/// </returns>
function ReadTextResourceW(const AInstance : HMODULE; const AResName: string): string;

/// <summary>
/// Имя файла по описателю. Обертка над GetModuleFileName. При неудачном вызове
/// вызывает RaiseLastOSError.
/// </summary>
/// <param name="AModuleHandle">
/// Описатель модуля.
/// </param>
/// <returns>
/// TFileName. Имя файла.
/// </returns>
function ModuleFileName(const AModuleHandle : HMODULE) : TFileName;

IMPLEMENTATION

function AssociatedIcon(const AFileOrDir : string; const ALarge : boolean): HIcon;
var
  vSHFILEINFO : TSHFileInfo;
  vFlags : integer;
begin
  vFlags := SHGFI_ICON or (SHGFI_LARGEICON * ORD(ALarge)) or (SHGFI_SMALLICON * ORD(not ALarge));
  if FileExists(AFileOrDir) then
    vFlags := vFlags or SHGFI_USEFILEATTRIBUTES;

  if SHGetFileInfo(PChar(AFileOrDir), FILE_ATTRIBUTE_NORMAL, vSHFILEINFO, SizeOf(TSHFileInfo), vFlags) = 0 then
    raise ESANWinUtils.CreateFmt('Не удалось получить иконку для %s', [AFileOrDir]);

  Result := vSHFILEINFO.hIcon;
end;

function SpecialFolderPath(const AFolder : integer; const AHasEndSlash : boolean) : string;
const
  SHGFP_TYPE_CURRENT = 0;
var
  vPath: array [0..MAX_PATH] of char;
begin
  OleCheck(SHGetFolderPath(0, AFolder, 0, SHGFP_TYPE_CURRENT, @vPath[0]));

  Result := vPath + IfThen(AHasEndSlash, '\');
end;

function DosDevice0Str : string;
var
  vRes, vErr, vBufSize: integer;
begin
  vBufSize := 1024 * 5;
  vRes := 0;

  while vRes = 0 do
  begin
    SetLength(Result, vBufSize);
    SetLastError(ERROR_SUCCESS);
    vRes := QueryDosDevice(nil, @Result[1], vBufSize);
    vErr := GetLastError();

    // Вариант для двухтонки
    if (vRes <> 0) and (vErr = ERROR_INSUFFICIENT_BUFFER) then
      begin
        vBufSize := vRes;
        vRes := 0;
      end;

    if (vRes = 0) and (vErr = ERROR_INSUFFICIENT_BUFFER) then
      begin
        vBufSize := vBufSize + 1024;
      end;

    if (vErr <> ERROR_SUCCESS) and (vErr <> ERROR_INSUFFICIENT_BUFFER) then
      raise ESANWinUtils.Create(SysErrorMessage(vErr));
  end;

  SetLength(Result, vRes);
end;

procedure DosDeviceStrings(const AStrings: TStrings; const ANameStart : string);
var
  vDosDevice0Str, vName: string;
  i : integer;
begin
  vDosDevice0Str := DosDevice0Str;

  AStrings.BeginUpdate;
  try

    AStrings.Clear;

    i := 1;

    while ReadSubStr(vDosDevice0Str, i, vName, #0) do
      if StartsStr(ANameStart, vName) then
        AStrings.Add(vName);

  finally
    AStrings.EndUpdate;
  end;

end;

procedure ComPortsStrings(const AStrings: TStrings);
begin
  DosDeviceStrings(AStrings, 'COM');
end;

function TryReadTextResourceA(out AText: AnsiString; const AInstance : HMODULE; const AResName: string): boolean;
var
  vResInstance : THandle;
  HRes : HRSRC;
begin
  vResInstance := FindResourceHinstance(AInstance);
  HRes := FindResource(vResInstance, PWideChar(AResName), RT_RCDATA);

  Result := (HRes <> 0);
  if not Result then
    Exit;

  AText := PAnsiChar(
    LockResource(
      LoadResource(vResInstance, HRes)
    )
  );

  SetLength(AText, SizeOfResource(vResInstance, HRes));
end;

function TryReadTextResourceW(out AText: string; const AInstance : HMODULE; const AResName: string): boolean;
var
  vAnsiText : AnsiString;
begin
  Result := TryReadTextResourceA(vAnsiText, AInstance, AResName);

  if Result then
    AText := string(vAnsiText);
end;

function ReadTextResourceA(const AInstance : HMODULE; const AResName: string): AnsiString;
begin
  if not TryReadTextResourceA(Result, AInstance, AResName) then
    RaiseLastOSError;
end;

function ReadTextResourceW(const AInstance : HMODULE; const AResName: string): string;
begin
  if not TryReadTextResourceW(Result, AInstance, AResName) then
    RaiseLastOSError;
end;

function ModuleFileName(const AModuleHandle : HMODULE) : TFileName;
var
  vFN: array[0..MAX_PATH - 1] of Char;
  l : Cardinal;
begin
  l := GetModuleFileName(AModuleHandle, vFN, MAX_PATH);
  if l = 0 then
    RaiseLastOSError;

  SetLength(Result, l);

  Move(vFN[0], Result[1], l * SizeOf(Char));
end;

END.
