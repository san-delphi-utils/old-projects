{*******************************************************}
{                                                       }
{       SANLogsIntf                                     }
{       Логирование. Общие интерфейсы.                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANLogsIntf;
{$I NX.INC}
INTERFACE

TYPE
//  /// <summary>
//  /// Базовый интерфейс, от которого наследуются все остальные интерфейсы.
//  /// </summary>
//  ISANLogsBase = interface(IInterface)
//    ['{E025799E-70D4-4801-90E0-4B69E0B25F0E}']
//  {$IFDEF DEBUG}
//    function GetRefCount : integer; stdcall;
//    property RefCount : integer  read GetRefCount;
//  {$ENDIF}
//  end;

  /// <summary>
  /// Интерфейс именованного объекта.
  /// </summary>
  ISANLogNamed = interface(IInterface)
    ['{5BF71C87-63AC-4770-ADF6-C2A91EE38F67}']
    function Get_Name : WideString; safecall;
    /// <summary>
    /// Имя объекта.
    /// </summary>
    property Name : WideString  read Get_Name;
  end;

  /// <summary>
  /// Интерфейс предоставления hInstace расположения объекта.
  /// </summary>
  ISANLogProvideHInstace = interface(IInterface)
    ['{8AE00640-31CB-424C-B4B0-FF07681B4CD0}']
    function Get_HInstace : LongWord; safecall;
    /// <summary>
    /// hInstace расположения объекта.
    /// </summary>
    property HInstace : LongWord  read Get_HInstace;
  end;

  /// <summary>
  /// Интерфейс одной записи лога.
  /// </summary>
  ISANLogRecord = interface(IInterface)
    ['{0B4AB9AF-3D4F-49E3-9695-F533C4B49C9F}']

    function Get_ID : integer; safecall;
    procedure Set_ID(const AID : integer); safecall;
    /// <summary>
    /// Идентификатор записи, зависит от типа хранения в логе.
    /// Значение не может быть отрицательным.
    /// </summary>
    property ID : integer  read Get_ID  write Set_ID;

    function Get_DateTime : TDateTime; safecall;
    procedure Set_DateTime(const ADateTime : TDateTime); safecall;
    /// <summary>
    /// Дата и время записи.
    /// </summary>
    property DateTime : TDateTime  read Get_DateTime  write Set_DateTime;

    function Get_Kind : Longword; safecall;
    procedure Set_Kind(const AKind : Longword); safecall;
    /// <summary>
    /// Тип сообщения.
    /// </summary>
    property Kind : Longword  read Get_Kind  write Set_Kind;

    function Get_SubType : Widestring; safecall;
    procedure Set_SubType(const ASubType : WideString); safecall;
    /// <summary>
    /// Текстовый подтип сообщения.
    /// </summary>
    property SubType : WideString  read Get_SubType  write Set_SubType;

    function Get_Msg : Widestring; safecall;
    procedure Set_Msg(const AMsg : WideString); safecall;
    /// <summary>
    /// Текст сообщения.
    /// </summary>
    property Msg : WideString  read Get_Msg  write Set_Msg;

    /// <summary>
    /// Метод очистки.
    /// </summary>
    procedure Clear; safecall;

    /// <summary>
    /// Метод задающий набор свойств.
    /// </summary>
    /// <param name="AID">
    /// Идентификатор записи.
    /// </param>
    /// <param name="ADateTime">
    /// Дата и время записи.
    /// </param>
    /// <param name="AKind">
    /// Тип сообщения.
    /// </param>
    /// <param name="ASubType">
    /// Текстовый подтип сообщения.
    /// </param>
    /// <param name="AMsg">
    /// Текст сообщения.
    /// </param>
    procedure From(const AID : integer; const ADateTime : TDateTime;
      const AKind : LongWord; const ASubType, AMsg : WideString); safecall;

    /// <summary>
    /// Скопировать данные из другой записи
    /// </summary>
    /// <param name="ASource">
    /// Запись лога, источник данных для копирования
    /// </param>
    procedure AssignRec(const ASource : ISANLogRecord); safecall;

    /// <summary>
    /// Сравнение двух записей на идентичность.
    /// </summary>
    /// <param name="ALogRecord">
    /// Запись, с которой идет сравнение.
    /// </param>
    /// <returns>
    /// WordBool. True, записи идентичны.
    /// </returns>
    function IsEqualsRecords(const ALogRecord : ISANLogRecord) : WordBool; safecall;

  end;

  /// <summary>
  /// Интерфайс перечисления записей лога.
  /// </summary>
  ISANLogRecordsEnumirator = interface(IInterface)
    ['{02E59CB1-03C3-46F1-AC82-466E3C2E330E}']

    function Get_Count : integer; safecall;
    /// <summary>
    /// Количество записей.
    /// </summary>
    property Count : integer  read Get_Count;

    function Get_Records(const Index : integer) : ISANLogRecord; safecall;
    /// <summary>
    /// Запись по индексу.
    /// </summary>
    property Records[const Index : integer] : ISANLogRecord  read Get_Records; default;
  end;

  /// <summary>
  /// Интерфейс фильтра набора записей.
  /// </summary>
  ISANLogRecordsFilter = interface(IInterface)
    ['{3DF69A03-3FC5-4390-A448-536E310547A3}']

    function Get_FirstID : integer; safecall;
    procedure Set_FirstID(const AFirstID : integer); safecall;
    /// <summary>
    /// Значение ID, с которого начинается анализ.
    /// При отрицательных значениях анализ идет сначала.
    /// </summary>
    property FirstID : integer  read Get_FirstID  write Set_FirstID;

    function Get_LastID : integer; safecall;
    procedure Set_LastID(const ALastID : integer); safecall;
    /// <summary>
    /// Значение ID, до которого идет анализ.
    /// При отрицательных значениях анализ идет до конца.
    /// </summary>
    property LastID : integer  read Get_LastID  write Set_LastID;

    /// <summary>
    /// Проверка одной записи
    /// </summary>
    /// <returns>
    /// WordBool. True, запись должна быть отфильтрована
    /// </returns>
    function Filtered(const ALogRecord : ISANLogRecord) : WordBool; safecall;
  end;

  ISANLogRecords = interface;

  /// <summary>
  /// Интерфейс перехватчика событий набора записей в логе.
  /// </summary>
  ISANLogRecordsNotifier = interface(IInterface)
    ['{FAF0BA96-24CB-4F1D-B6D4-89992396275A}']

    /// <summary>
    /// Набор записей отфильтрован.
    /// </summary>
    /// <param name="ARecords">
    /// Набор записей, в котором произошло событие.
    /// </param>
    /// <param name="AFilteredCount">
    /// Количество отфильтрованых записей.
    /// </param>
    procedure Filtered(const ARecords : ISANLogRecords; const AFilteredCount : integer); safecall;

    /// <summary>
    /// Из набора извлечена запись.
    /// </summary>
    /// <param name="ARecords">
    /// Набор записей, в котором произошло событие.
    /// </param>
    /// <param name="ARecord">
    /// Извлеченная запись.
    /// </param>
    procedure Extracted(const ARecords : ISANLogRecords; const ARecord : ISANLogRecord); safecall;

    /// <summary>
    /// Из набора удаляется запись.
    /// </summary>
    /// <param name="ARecords">
    /// Набор записей, в котором произошло событие.
    /// </param>
    /// <param name="ARecord">
    /// Удаляемая запись.
    /// </param>
    procedure Removing(const ARecords : ISANLogRecords; const ARecord : ISANLogRecord); safecall;

    /// <summary>
    /// В набор добавлена запись.
    /// </summary>
    /// <param name="ARecords">
    /// Набор записей, в котором произошло событие.
    /// </param>
    /// <param name="ARecord">
    /// Добавленная запись.
    /// </param>
    procedure Added(const ARecords : ISANLogRecords; const ARecord : ISANLogRecord); safecall;

    /// <summary>
    /// Вызывается при унечтожении набора записей.
    /// </summary>
    /// <param name="ARecords">
    /// Набор записей, в котором произошло событие.
    /// </param>
    procedure Destroying(const ARecords : ISANLogRecords); safecall;
  end;

  /// <summary>
  /// Интерфейс набора записей в логе.
  /// </summary>
  ISANLogRecords = interface(IInterface)
    ['{ECAE7D89-2FC5-4898-B2EC-69FD4A0DBC19}']

    function Get_Count : integer; safecall;
    /// <summary>
    /// Количество записей.
    /// </summary>
    property Count : integer  read Get_Count;

    function Get_Records(const Index : integer) : ISANLogRecord; safecall;
    /// <summary>
    /// Запись по индексу.
    /// </summary>
    property Records[const Index : integer] : ISANLogRecord  read Get_Records; default;

    /// <summary>
    /// Перечисление записей лога.
    /// </summary>
    /// <returns>
    /// ISANLogRecordsEnumirator. Интерфейс перечаслителя записей лога.
    /// </returns>
    function Enumiration : ISANLogRecordsEnumirator; safecall;

    /// <summary>
    /// Очистить.
    /// </summary>
    procedure Clear; safecall;

    /// <summary>
    /// Поиск записи по ID.
    /// </summary>
    /// <param name="ALogRecord">
    /// Найденая запись, если поиск успешен.
    /// </param>
    /// <param name="AID">
    /// ID записи для поиска.
    /// </param>
    /// <returns>
    /// WordBool. True, поиск успешен.
    /// </returns>
    function FindByID(out ALogRecord : ISANLogRecord; const AID : integer) : WordBool; safecall;

    /// <summary>
    /// Извлечь запись из набора.
    /// </summary>
    /// <param name="AID">
    /// ID извлекаемой записи.
    /// </param>
    /// <returns>
    /// ISANLogRecord. Извлеченная запись или nil в случае неудачи.
    /// </returns>
    function ExtractByID(const AID : integer) : ISANLogRecord; safecall;

    /// <summary>
    /// Удалить запись из набора по ID.
    /// </summary>
    /// <param name="AID">
    /// ID удаляемой записи.
    /// </param>
    /// <returns>
    /// WordBool. True, удаление успешно.
    /// </returns>
    function RemoveByID(const AID : integer) : WordBool; safecall;

    /// <summary>
    /// Удалить запись из набора по индексу.
    /// </summary>
    /// <param name="AIndex">
    /// Индекс удаляемой записи.
    /// </param>
    procedure Delete(const AIndex : integer); safecall;

    /// <summary>
    /// Добавить запись в набор, как копию другой записи.
    /// </summary>
    /// <param name="ALogRecjrd">
    /// Запись лога, по которой происходит добавление.
    /// </param>
    /// <returns>
    /// ISANLogRecord. Созданая запись.
    /// </returns>
    function AddAsCopy(const ALogRecord : ISANLogRecord) : ISANLogRecord; safecall;

    /// <summary>
    /// Добавить запись в набор, как копию другой записи.
    /// </summary>
    /// <param name="AID">
    /// Идентификатор записи.
    /// </param>
    /// <param name="ADateTime">
    /// Дата и время записи.
    /// </param>
    /// <param name="AKind">
    /// Тип сообщения.
    /// </param>
    /// <param name="ASubType">
    /// Текстовый подтип сообщения.
    /// </param>
    /// <param name="AMsg">
    /// Текст сообщения.
    /// </param>
    /// <returns>
    /// ISANLogRecord. Созданая запись.
    /// </returns>
    function Add(const AID : integer; const ADateTime : TDateTime;
      const AKind : LongWord; const ASubType, AMsg : WideString) : ISANLogRecord; safecall;

    /// <summary>
    /// Фильтрация
    /// </summary>
    /// <param name="AFilter">
    /// Фильтр, производящий фильтрацию. Передача интерфейса по значению
    /// может быть использована для освобождения памяти, если интерфейс ни где
    /// более не используется.
    /// </param>
    /// <returns>
    /// Integer. Количество отфильтрованых записей.
    /// </returns>
    function Filter(AFilter : ISANLogRecordsFilter) : integer; safecall;

    /// <summary>
    /// Экспорт записей в безопасный массив.
    /// </summary>
    /// <param name="ARecords">
    /// Безопасный массив в вариантной переменной.
    /// </param>
    procedure ExportRecords(out ARecords : OleVariant); safecall;

    /// <summary>
    /// Импорт записей из безопасного массива.
    /// </summary>
    /// <param name="ARecords">
    /// Безопасный массив в вариантной переменной.
    /// После импорта переменная очищается.
    /// </param>
    /// <param name="AReWrite">
    /// True - удалить старые записи.
    /// </param>
    procedure ImportRecords(var ARecords : OleVariant; const AReWrite : WordBool); safecall;

    function Get_Notifier : ISANLogRecordsNotifier; safecall;
    procedure Set_Notifier(const ANotifier : ISANLogRecordsNotifier); safecall;
    /// <summary>
    /// Перехватчик событий набора записей
    /// </summary>
    property Notifier : ISANLogRecordsNotifier  read Get_Notifier  write Set_Notifier;
  end;

  /// <summary>
  /// Интерфейс вырожденного лога (только запись).
  /// </summary>
  ISANCustomLog  = interface(IInterface)
    ['{D4620B71-4E20-4C13-8391-A50E1265AC64}']

    /// <summary>
    /// Одна законченная запись.
    /// </summary>
    /// <param name="AKind">
    /// Целочисленный тип записи.
    /// </param>
    /// <param name="ASubType">
    /// Строка подтипа записи.
    /// </param>
    /// <param name="AMessage">
    /// Строка сообщения записи.
    /// </param>
    /// <param name="ADateTime">
    /// Дата и время записи. Если ADateTime = 0, берется Now.
    /// </param>
    /// <returns>
    /// Integer. ID новой записи, если это возможно, иниче - 0.
    /// </returns>
    function Write(const AKind : LongWord; const ASubType : WideString; const AMessage : WideString = ''; ADateTime : TDateTime = 0) : integer; safecall;

    /// <summary>
    /// Запись в лог в режиме, когда сообщение записывается по частям.
    /// Начало записи.
    /// </summary>
    /// <param name="AKind">
    /// Целочисленный тип записи.
    /// </param>
    /// <param name="ASubType">
    /// Строка подтипа записи.
    /// </param>
    /// <param name="ADateTime">
    /// Дата и время записи. Если ADateTime = 0, берется Now.
    /// </param>
    /// <returns>
    /// Integer. ID новой записи, если это возможно, иниче - 0.
    /// </returns>
    function BeginWrite(const AKind : LongWord; const ASubType : WideString; ADateTime : TDateTime = 0) : integer; safecall;

    /// <summary>
    /// Запись в лог в режиме, когда сообщение записывается по частям.
    /// Записать часть сообщения.
    /// </summary>
    /// <param name="APartialMessage">
    /// Часть сообщения записи.
    /// </param>
    procedure PartialWrite(const APartialMessage : WideString); safecall;

    /// <summary>
    /// Запись в лог в режиме, когда сообщение записывается по частям.
    /// Завершить запись сообщения.
    /// </summary>
    procedure EndWrite; safecall;

  end;

  ISANLogPath = interface;

  /// <summary>
  /// Интерфейс части идентификационного пути лога в рамках хранилища.
  /// </summary>
  ISANLogPathPart = interface(IInterface)
    ['{E42495BE-6A38-44F8-AE49-4D48DA2184EB}']

    function Get_ParentPath : ISANLogPath; safecall;
    /// <summary>
    /// Путь, к которому относится часть.
    /// </summary>
    property ParentPath : ISANLogPath  read Get_ParentPath;

    function Get_Name : WideString; safecall;
    /// <summary>
    /// Уникальное имя части идентификационного пути.
    /// </summary>
    property Name : WideString  read Get_Name;

    function Get_Value : OleVariant; safecall;
    procedure Set_Value(const AValue : OleVariant); safecall;
    /// <summary>
    /// Значение.
    /// </summary>
    property Value : OleVariant  read Get_Value  write Set_Value;

    /// <summary>
    /// Сравнение двух частей пути на идентичность.
    /// </summary>
    /// <param name="APathPart">
    /// Часть пути, с которой идет сравнение.
    /// </param>
    /// <returns>
    /// WordBool. True, части путей идентичны.
    /// </returns>
    function IsEqualsPathParts(const APathPart : ISANLogPathPart) : WordBool; safecall;

  end;

  /// <summary>
  /// Интерфейс перехватчика событий идентификационного пути в хранилище.
  /// </summary>
  ISANLogPathNotifier = interface(IInterface)
    ['{750BBA80-D9E2-4598-8229-187FC03BB4B6}']

    /// <summary>
    /// Вызывается перед сменой значения для части пути.
    /// </summary>
    /// <param name="APathPart">
    /// Часть пути, в которой произошло событие.
    /// </param>
    /// <param name="AOldValue">
    /// Старое значение части пути.
    /// </param>
    /// <param name="ANewValue">
    /// Старое новое значение части пути.
    /// </param>
    procedure ValueChanging(const APathPart : ISANLogPathPart;
      const AOldValue, ANewValue : OleVariant); safecall;

    /// <summary>
    /// Вызывается после смены значения для части пути.
    /// </summary>
    /// <param name="APathPart">
    /// Часть пути, в которой произошло событие.
    /// </param>
    /// <param name="AOldValue">
    /// Старое значение части пути.
    /// </param>
    /// <param name="ANewValue">
    /// Старое новое значение части пути.
    /// </param>
    procedure ValueChanged(const APathPart : ISANLogPathPart;
      const AOldValue, ANewValue : OleVariant); safecall;

    /// <summary>
    /// Вызывается при унечтожении пути.
    /// </summary>
    /// <param name="APath">
    /// Путь, в котором произошло событие.
    /// </param>
    procedure Destroying(const APath : ISANLogPath); safecall;
  end;

  /// <summary>
  /// Абстрактный интерфейс списка частей идентификационного пути лога в рамках хранилища.
  /// </summary>
  ISANCustomLogPathParts = interface(IInterface)
    ['{D13ABFA2-5401-4D23-9A82-C4712D0E2406}']

    function Get_Count : integer; safecall;
    /// <summary>
    /// Количество частей.
    /// </summary>
    property Count : integer  read Get_Count;

    function Get_PathParts(const Index : integer) : ISANLogPathPart; safecall;
    /// <summary>
    /// Часть пути по индексу.
    /// </summary>
    property PathParts[const Index : integer] : ISANLogPathPart  read Get_PathParts; default;

    /// <summary>
    /// Поиск части пути по имени.
    /// </summary>
    /// <param name="APathPart">
    /// Найденая часть пути, если поиск успешен.
    /// </param>
    /// <param name="AName">
    /// Имя части пути.
    /// </param>
    /// <returns>
    /// WordBool. True, поиск успешен.
    /// </returns>
    function FindByName(out APathPart : ISANLogPathPart; const AName : WideString) : WordBool; safecall;

    /// <summary>
    /// Удаление части пути по имени.
    /// </summary>
    /// <param name="AName">
    /// Имя части пути.
    /// </param>
    /// <returns>
    /// WordBool. True, удаление успешено.
    /// </returns>
    function RemoveByName(const AName : WideString) : WordBool; safecall;

    /// <summary>
    /// Удаление части пути по индексу.
    /// </summary>
    /// <param name="AIndex">
    /// Индекс в списке.
    /// </param>
    procedure Delete(const AIndex : integer); safecall;

    /// <summary>
    /// Добавление части пути.
    /// </summary>
    /// <param name="AName">
    /// Имя части пути.
    /// </param>
    /// <returns>
    /// ISANLogPathPart. Добавленная часть пути.
    /// </returns>
    function Add(const AName : WideString) : ISANLogPathPart; safecall;

  end;

  /// <summary>
  /// Интерфейс перечисления частей идентификационного пути лога в рамках хранилища.
  /// </summary>
  ISANLogPathPartsEnumirator = interface(ISANCustomLogPathParts)
    ['{6F9ED80F-2B5E-4261-B8DC-2F62C8302C12}']
  end;

  ISANLogsStorage = interface;

  /// <summary>
  /// Интерфейс идентификационного пути лога в рамках хранилища
  /// </summary>
  ISANLogPath = interface(ISANCustomLogPathParts)
    ['{B4F7A1D2-DD4B-4C17-AF7F-C5F4B95542D7}']

    function Get_Storage : ISANLogsStorage; safecall;
    /// <summary>
    /// Хранилище, к которому принадлежит путь
    /// </summary>
    property Storage : ISANLogsStorage  read Get_Storage;

    /// <summary>
    /// Перечисление частей пути.
    /// </summary>
    /// <returns>
    /// ISANLogPathPartsEnumirator. Интерфейс перечаслителя частей пути.
    /// </returns>
    function Enumiration : ISANLogPathPartsEnumirator; safecall;

    /// <summary>
    /// Сравнение двух путей на идентичность.
    /// </summary>
    /// <param name="APath">
    /// Путь, с которым идет сравнение.
    /// </param>
    /// <returns>
    /// WordBool. True, пути идентичны.
    /// </returns>
    function IsEqualsPath(const APath : ISANLogPath) : WordBool; safecall;

    /// <summary>
    /// Добавить перехватчик событий
    /// </summary>
    /// <param name="APathNotifier">
    /// Перехватчик
    /// </param>
    /// <returns>
    /// Integer. Числовой идентификатор добавленного перехватчика в данном пути.
    /// </returns>
    function AddNotifier(const APathNotifier : ISANLogPathNotifier) : integer; safecall;

    /// <summary>
    /// Снять перехватчик событий
    /// </summary>
    /// <param name="APathNotifierID">
    /// Числовой идентификатор перехватчика полученый функцией AddNotifier.
    /// </param>
    /// <returns>
    /// WordBool. True, успешно.
    /// </returns>
    function RemoveNotifier(const APathNotifierID : integer) : WordBool; safecall;
  end;

  /// <summary>
  /// Интерфейс перехватчика событий лога.
  /// </summary>
  ISANLogNotifier = interface(IInterface)
    ['{35D8FCDE-9305-4BB6-80AF-3D91ABE7E6F2}']

    /// <summary>
    /// В лог записана запись.
    /// </summary>
    /// <param name="ALog">
    /// Лог, в котором произошло событие.
    /// </param>
    /// <param name="ALogRecord">
    /// Записанная запись.
    /// </param>
    procedure RecordWrited(const ALog : ISANCustomLog; const ALogRecord : ISANLogRecord); safecall;

    /// <summary>
    /// В лог записан набор записей.
    /// </summary>
    /// <param name="ALog">
    /// Лог, в котором произошло событие.
    /// </param>
    /// <param name="ALogRecords">
    /// Записанный набор записей.
    /// </param>
    /// <param name="AReWrite">
    /// Флаг перезаписи.
    /// </param>
    procedure RecordsWrited(const ALog : ISANCustomLog;
      const ALogRecords : ISANLogRecords; const AReWrite: WordBool); safecall;

    /// <summary>
    /// Лог отфильтрован.
    /// </summary>
    /// <param name="ALog">
    /// Лог, в котором произошло событие.
    /// </param>
    /// <param name="AFilteredCount">
    /// Количество отфильтрованых записей.
    /// </param>
    procedure Filtered(const ALog : ISANCustomLog; const AFilteredCount : integer); safecall;

    /// <summary>
    /// Вызывается при унечтожении лога.
    /// </summary>
    /// <param name="ALog">
    /// Лог, в котором произошло событие.
    /// </param>
    procedure Destroying(const ALog : ISANCustomLog); safecall;
  end;

  /// <summary>
  /// Интерфейс пастройки идентификационного пути лога в рамках хранилища
  /// </summary>
  ISANLogPathSetup = interface(IInterface)
    ['{B57E4284-F7EF-4A79-BA41-FC63F7305FF8}']

    /// <summary>
    /// Настройка пути
    /// </summary>
    /// <param name="AStorage">
    /// Хранилище, для которого настраевается путь
    /// </param>
    /// <param name="ALogPath">
    /// Настраеваемый путь
    /// </param>
    procedure SetupLogPath(const AStorage : ISANLogsStorage; const ALogPath : ISANLogPath);
  end;

  /// <summary>
  /// Интерфейс полноценного лога.
  /// </summary>
  ISANLog = interface(ISANCustomLog)
    ['{9D746EF8-A1C2-452B-A84C-BE9C24A69517}']

    function Get_Name : WideString; safecall;
    /// <summary>
    /// Уникальное имя лога, однозначно определяющее его
    /// в списке логов.
    /// </summary>
    property Name : WideString  read Get_Name;


    function Get_Path : ISANLogPath; safecall;
    procedure Set_Path(const APath : ISANLogPath); safecall;
    /// <summary>
    /// Уникальный идентификатор лога в рамках хранилища.
    /// </summary>
    property Path : ISANLogPath  read Get_Path  write Set_Path;

    function Get_Storage : ISANLogsStorage; safecall;
    /// <summary>
    /// Хранилище, к которому относится лог.
    /// </summary>
    property Storage : ISANLogsStorage  read Get_Storage;

    /// <summary>
    /// Вписать в лог набор записей.
    /// </summary>
    /// <param name="ALogRecords">
    /// Набор записей, которые надо вписать в лог.
    /// </param>
    /// <param name="AReWrite">
    /// True - очистить старые данные, False - добавить новые данные в конец.
    /// </param>
    procedure WriteRecords(const ALogRecords : ISANLogRecords; const AReWrite : WordBool); safecall;

    /// <summary>
    /// Прочитать записи из лога.
    /// Список создается/унечтожается пользователем лога.
    /// AFirstID, ALastID - ограничения выборки, отрицательные значения - без ограничения.
    /// </summary>
    /// <param name="ALogRecords">
    /// Набор записей, в который производится чтение.
    /// </param>
    /// <param name="AFirstID">
    /// Значение ID, с которого начинается анализ.
    /// При отрицательных значениях анализ идет сначала.
    /// </param>
    /// <param name="ALastID">
    /// Значение ID, до которого идет анализ.
    /// При отрицательных значениях анализ идет до конца.
    /// </param>
    procedure Read(const ALogRecords : ISANLogRecords;
      const AFirstID : integer = -1; const ALastID : integer = -1); safecall;

    /// <summary>
    /// Отфильтровать записи в логе. Отфильтрованные записи удаляются.
    /// </summary>
    /// <param name="AFilter">
    /// Фильтр, производящий фильтрацию. Передача интерфейса по значению
    /// может быть использована для освобождения памяти, если интерфейс ни где
    /// более не используется.
    /// </param>
    /// <returns>
    /// Integer. Количество отфильтрованых записей.
    /// </returns>
    function Filter(AFilter : ISANLogRecordsFilter) : integer; safecall;

    /// <summary>
    /// Сравнение двух логов по месту ведения записи. Логи могут иметь разные имена,
    /// но писать, например в один файл или таблицу БД, такие логи идентичны.
    /// </summary>
    /// <param name="ALog">
    /// Лог, с которым идет сравнение.
    /// </param>
    /// <returns>
    /// WordBool. True, логи идентичны.
    /// </returns>
    function IsEqualsLogs(const ALog : ISANLog) : WordBool; safecall;

    /// <summary>
    /// Добавить перехватчик событий
    /// </summary>
    /// <param name="ALogNotifier">
    /// Перехватчик
    /// </param>
    /// <returns>
    /// Integer. Числовой идентификатор добавленного перехватчика в данном логе.
    /// </returns>
    function AddNotifier(const ALogNotifier : ISANLogNotifier) : integer; safecall;

    /// <summary>
    /// Снять перехватчик событий
    /// </summary>
    /// <param name="ALogNotifierID">
    /// Числовой идентификатор перехватчика полученый функцией AddNotifier.
    /// </param>
    /// <returns>
    /// WordBool. True, успешно.
    /// </returns>
    function RemoveNotifier(const ALogNotifierID : integer) : WordBool; safecall;
  end;

  /// <summary>
  /// Интерфейс информации о логе.
  /// </summary>
  ISANLogInfo = interface(IInterface)
    ['{FD1E38C2-4A54-4BDE-AF2B-E62E72F533A0}']

    function Get_Name : WideString; safecall;
    /// <summary>
    /// Уникальное имя лога, однозначно определяющее его
    /// в списке логов.
    /// </summary>
    property Name : WideString  read Get_Name;


    function Get_Path : ISANLogPath; safecall;
    /// <summary>
    /// Уникальный идентификатор лога в рамках хранилища.
    /// </summary>
    property Path : ISANLogPath  read Get_Path;

    function Get_Storage : ISANLogsStorage; safecall;
    /// <summary>
    /// Хранилище, к которому относится информация о логе.
    /// </summary>
    property Storage : ISANLogsStorage  read Get_Storage;

    function Get_Log : ISANLog; safecall;
    /// <summary>
    /// Ссылка на лог, если он создан
    /// </summary>
    property Log : ISANLog  read Get_Log;

    /// <summary>
    /// Открыть лог. Если лог не создан - создать.
    /// </summary>
    /// <returns>
    /// ISANLog. Ссылка на открытый лог.
    /// </returns>
    function OpenLog : ISANLog; safecall;
  end;

  /// <summary>
  /// Интерфейс перечисления информации о логах.
  /// </summary>
  ISANLogInfosEnumirator = interface(IInterface)
    ['{8A4035BE-C516-4CF6-B705-777D7A2553DD}']

    function Get_Count : integer; safecall;
    /// <summary>
    /// Количество интерфейсов информации о логах.
    /// </summary>
    property Count : integer  read Get_Count;

    function Get_LogInfos(const Index : integer) : ISANLogInfo; safecall;
    /// <summary>
    /// Получение интерфейса информации о логе.
    /// </summary>
    property LogInfos[const Index : integer] : ISANLogInfo  read Get_LogInfos; default;
  end;

  /// <summary>
  /// Интерфайс перечисления логов.
  /// </summary>
  ISANLogsEnumirator = interface(IInterface)
    ['{FB92F732-3B6A-4B1C-B637-3B666200ECF8}']

    function Get_Count : integer; safecall;
    /// <summary>
    /// Количество логов в данном хранилище.
    /// </summary>
    property Count : integer  read Get_Count;

    function Get_Logs(const Index : integer) : ISANLog; safecall;
    /// <summary>
    /// Получение интерфейса лога.
    /// </summary>
    property Logs[const Index : integer] : ISANLog  read Get_Logs; default;
  end;

  /// <summary>
  /// Интерфейс перехватчика событий хранилища.
  /// </summary>
  ISANLogsStorageNotifier = interface(IInterface)
    ['{7EA4D39C-973A-4A2F-8CE2-9098F82D50F6}']

    /// <summary>
    /// В хранилище добавлен лог.
    /// </summary>
    /// <param name="AStorage">
    /// Хранилище, в котором произошло событие.
    /// </param>
    /// <param name="ALog">
    /// Добавленый лог.
    /// </param>
    procedure LogAdded(const AStorage : ISANLogsStorage; const ALog : ISANCustomLog); safecall;

    /// <summary>
    /// Из хранилища удаляется лог.
    /// </summary>
    /// <param name="AStorage">
    /// Хранилище, в котором произошло событие.
    /// </param>
    /// <param name="ALog">
    /// Удаляемый лог.
    /// </param>
    procedure LogRemoving(const AStorage : ISANLogsStorage; const ALog : ISANCustomLog); safecall;

    /// <summary>
    /// Вызывается при унечтожении хранилища.
    /// </summary>
    /// <param name="AStorage">
    /// Хранилище, в котором произошло событие.
    /// </param>
    procedure Destroying(const AStorage : ISANLogsStorage); safecall;
  end;

  /// <summary>
  /// Интерфейс набора логов.
  /// </summary>
  ISANLogsSet = interface(IInterface)
    ['{EC8D5701-3751-470C-BDBD-F03DD89395A3}']

    function Get_LogsCount : integer; safecall;
    /// <summary>
    /// Количество логов в данном хранилище.
    /// </summary>
    property LogsCount : integer  read Get_LogsCount;

    function Get_Logs(const Index : integer) : ISANLog; safecall;
    /// <summary>
    /// Получение интерфейса лога из хранилища.
    /// </summary>
    property Logs[const Index : integer] : ISANLog  read Get_Logs;

    /// <summary>
    /// Перечисление логов.
    /// </summary>
    /// <returns>
    /// ISANLogsEnumirator. Интерфейс перечаслителя логов.
    /// </returns>
    function Enumiration : ISANLogsEnumirator; safecall;

    /// <summary>
    /// Очистка набора логов.
    /// </summary>
    procedure Clear; safecall;

    /// <summary>
    /// Поиск лога по имени.
    /// </summary>
    /// <param name="ALog">
    /// Найденый лог, если поиск успешен.
    /// </param>
    /// <param name="AName">
    /// Имя лога.
    /// </param>
    /// <returns>
    /// WordBool. True, поиск успешен.
    /// </returns>
    function FindByName(out ALog : ISANLog; const AName : WideString) : WordBool; safecall;

    /// <summary>
    /// Извлечь лог из набора.
    /// </summary>
    /// <param name="AName">
    /// Имя лога.
    /// </param>
    /// <returns>
    /// ISANLog. Извлеченная запись или nil в случае неудачи.
    /// </returns>
    function ExtractByName(const AName : WideString) : ISANLog; safecall;

    /// <summary>
    /// Удалить лог из набора.
    /// </summary>
    /// <param name="AName">
    /// Имя лога.
    /// </param>
    /// <returns>
    /// WordBool. True, удаление успешно.
    /// </returns>
    function RemoveByName(const AName : WideString) : WordBool; safecall;
  end;

  /// <summary>
  /// Интерфейс хранилища логов.
  /// </summary>
  ISANLogsStorage = interface(ISANLogsSet)
    ['{507DA03C-2319-4990-9DEB-1AC0928401FF}']

    function Get_Name : WideString; safecall;
    /// <summary>
    /// Уникальное имя хранилища, однозначно определяющее его
    /// в списке хранилищ.
    /// </summary>
    property Name : WideString  read Get_Name;

    /// <summary>
    /// Получить указатель на новый путь, для заполнения.
    /// </summary>
    /// <returns>
    /// ISANLogPath. Интерфейс пути в рамках этого хранилища.
    /// </returns>
    function CreateNewPath : ISANLogPath; safecall;

    /// <summary>
    /// Проверить корректность пути для этого хранилища.
    /// </summary>
    /// <param name="APath">
    /// Интерфейс пути в хранилище.
    /// </param>
    /// <param name="AErrors">
    /// Сообщение об ошибках, если путь не корректен.
    /// </param>
    /// <returns>
    /// WordBool. True, путь соответствует хранилищу.
    /// </returns>
    function CheckPathIsCorrect(const APath : ISANLogPath; out AErrors : WideString) : WordBool; safecall;

    /// <summary>
    /// Получить для имени части пути список допустимых значений.
    /// </summary>
    /// <param name="APathPart">
    /// Часть пути данного хранилища.
    /// </param>
    /// <param name="AValues">
    /// Список значений.
    /// </param>
    /// <returns>
    /// WordBool. True, для данной части пути есть список предопределенных значений.
    /// </returns>
    function PathPartAvailableValuesList(const APathPart : ISANLogPathPart;
      out AValues : OleVariant) : WordBool; safecall;


    /// <summary>
    /// Добавить новый лог
    /// </summary>
    /// <param name="AName">
    /// Уникальное имя лога
    /// </param>
    /// <param name="APath">
    /// Интерфейс пути в рамках этого хранилища.
    /// </param>
    /// <returns>
    /// ISANLog. Интерфейс лога.
    /// </returns>
    function AddLog(const AName : WideString; const APath : ISANLogPath) : ISANLog; safecall;

    /// <summary>
    /// Перечисление информачии о логах по указанному неполному пути.
    /// </summary>
    /// <param name="APath">
    /// Интерфейс пути в рамках этого хранилища.
    /// </param>
    /// <returns>
    /// ISANLogInfosEnumirator. Интерфейс перечисления.
    /// </returns>
    function EnumirateLogsInfo(const APath : ISANLogPath) : ISANLogInfosEnumirator; safecall;

    /// <summary>
    /// Добавить перехватчик событий
    /// </summary>
    /// <param name="AStorageNotifier">
    /// Перехватчик
    /// </param>
    /// <returns>
    /// Integer. Числовой идентификатор добавленного перехватчика в данном хранилище.
    /// </returns>
    function AddNotifier(const AStorageNotifier : ISANLogsStorageNotifier) : integer; safecall;

    /// <summary>
    /// Снять перехватчик событий
    /// </summary>
    /// <param name="AStorageNotifierID">
    /// Числовой идентификатор перехватчика полученый функцией AddNotifier.
    /// </param>
    /// <returns>
    /// WordBool. True, успешно.
    /// </returns>
    function RemoveNotifier(const AStorageNotifierID : integer) : WordBool; safecall;

    /// <summary>
    /// Сравнение двух хранилищ логов на идентичность.
    /// </summary>
    /// <param name="AStorage">
    /// Хранилища логов, с которым идет сравнение.
    /// </param>
    /// <returns>
    /// WordBool. True, хранилища идентичны.
    /// </returns>
    function IsEqualsStorages(const AStorage : ISANLogsStorage) : WordBool; safecall;
  end;

  /// <summary>
  /// Интерфейс перечисления хранилищ.
  /// </summary>
  ISANLogsStoragesEnumirator = interface(IInterface)
    ['{A2D47379-F36D-44B2-85CB-FED4CE158ABD}']

    function Get_Count : integer; safecall;
    /// <summary>
    /// Количество хранилищ.
    /// </summary>
    property Count : integer  read Get_Count;

    function Get_Storages(const Index : integer) : ISANLogsStorage; safecall;
    /// <summary>
    /// Получение интерфейса хранилища.
    /// </summary>
    property Storages[const Index : integer] : ISANLogsStorage  read Get_Storages; default;
  end;

  ISANLogsStoragesList = interface;

  /// <summary>
  /// Интерфейс перехватчика событий списка хранилищ.
  /// </summary>
  ISANLogsStoragesListNotifier = interface(IInterface)
    ['{EFDC34B3-8FD5-497D-86A6-DD34E705395B}']

    /// <summary>
    /// Вызывается при добавлении хранилища в список.
    /// </summary>
    /// <param name="AStorages">
    /// Список хранилищ, в котором произошло событие.
    /// </param>
    /// <param name="AStorage">
    /// Добавленое хранилище.
    /// </param>
    procedure StorageAdded(const AStorages : ISANLogsStoragesList; const AStorage : ISANLogsStorage); safecall;

    /// <summary>
    /// Вызывается перед удалением хранилища из списка.
    /// </summary>
    /// <param name="AStorages">
    /// Список хранилищ, в котором произошло событие.
    /// </param>
    /// <param name="AStorage">
    /// Удаляемое хранилище.
    /// </param>
    procedure StorageRemoving(const AStorages : ISANLogsStoragesList; const AStorage : ISANLogsStorage); safecall;

    /// <summary>
    /// Вызывается при унечтожении списка хранилищ.
    /// </summary>
    /// <param name="AStorages">
    /// Список хранилищ, в котором произошло событие.
    /// </param>
    procedure Destroying(const AStorages : ISANLogsStoragesList); safecall;
  end;

  /// <summary>
  /// Интерфейс списка хранилищ.
  /// </summary>
  ISANLogsStoragesList = interface(IInterface)
    ['{1719DABD-79FC-4B5F-8E9F-D52FB8A6FEB0}']

    function Get_Count : integer; safecall;
    /// <summary>
    /// Количество хранилищ.
    /// </summary>
    property Count : integer  read Get_Count;

    function Get_Storages(const Index : integer) : ISANLogsStorage; safecall;
    /// <summary>
    /// Получение интерфейса хранилища.
    /// </summary>
    property Storages[const Index : integer] : ISANLogsStorage  read Get_Storages; default;

    /// <summary>
    /// Перечисление хранилищ.
    /// </summary>
    /// <returns>
    /// ISANLogsStoragesEnumirator. Интерфейс перечаслителя хранилищ.
    /// </returns>
    function Enumiration : ISANLogsStoragesEnumirator; safecall;

    /// <summary>
    /// Очистка списка хранилищ.
    /// </summary>
    procedure Clear; safecall;

    /// <summary>
    /// Поиск хранилища по имени.
    /// </summary>
    /// <param name="AStorage">
    /// Найденое хранилище, если поиск успешен.
    /// </param>
    /// <param name="AName">
    /// Имя хранилища.
    /// </param>
    /// <returns>
    /// WordBool. True, поиск успешен.
    /// </returns>
    function FindByName(out AStorage : ISANLogsStorage; const AName : WideString) : WordBool; safecall;

    /// <summary>
    /// Извлечь хранилище из списка.
    /// </summary>
    /// <param name="AName">
    /// Имя хранилища.
    /// </param>
    /// <returns>
    /// ISANLog. Извлеченная запись или nil в случае неудачи.
    /// </returns>
    function ExtractByName(const AName : WideString) : ISANLogsStorage; safecall;

    /// <summary>
    /// Удалить хранилище из списка.
    /// </summary>
    /// <param name="AName">
    /// Имя хранилища.
    /// </param>
    /// <returns>
    /// WordBool. True, удаление успешно.
    /// </returns>
    function RemoveByName(const AName : WideString) : WordBool; safecall;

    /// <summary>
    /// Добавить хранилище в список.
    /// </summary>
    /// <param name="AStorage">
    /// Хранилище.
    /// </param>
    /// <returns>
    /// Integer. Индекс в списке.
    /// </returns>
    function Add(const AStorage : ISANLogsStorage) : integer; safecall;

    /// <summary>
    /// Добавить перехватчик событий
    /// </summary>
    /// <param name="AStoragesList">
    /// Перехватчик
    /// </param>
    /// <returns>
    /// Integer. Числовой идентификатор добавленного перехватчика в данном хранилище.
    /// </returns>
    function AddNotifier(const AStoragesListNotifier : ISANLogsStoragesListNotifier) : integer; safecall;

    /// <summary>
    /// Снять перехватчик событий
    /// </summary>
    /// <param name="AStoragesListNotifierID">
    /// Числовой идентификатор перехватчика полученый функцией AddNotifier.
    /// </param>
    /// <returns>
    /// WordBool. True, успешно.
    /// </returns>
    function RemoveNotifier(const AStoragesListNotifierID : integer) : WordBool; safecall;

  end;

  /// <summary>
  /// Интерфейс менеджера хранилищ и логов.
  /// </summary>
  ISANLogsManager = interface(ISANLogsSet)
    ['{D288192D-706B-42D1-BF8E-4B3753EF0642}']

    function Get_Name : WideString; safecall;
    /// <summary>
    /// Уникальное имя менеджера. На всякий случай, на будущее.
    /// </summary>
    property Name : WideString  read Get_Name;

    function Get_Storages : ISANLogsStoragesList; safecall;
    /// <summary>
    /// Список хранилищ
    /// </summary>
    property Storages : ISANLogsStoragesList  read Get_Storages;

  end;

/// <summary>
/// Проверка существования записи по ID.
/// </summary>
/// <param name="ALogRecords">
/// Набор записей.
/// </param>
/// <param name="AID">
/// ID записи для поиска.
/// </param>
/// <returns>
/// WordBool. True, запись существует.
/// </returns>
function SANLogRecordsExists(const ALogRecords : ISANLogRecords; const AID : integer) : WordBool;

/// <summary>
/// Извлечь запись из набора.
/// </summary>
/// <param name="ALogRecords">
/// Набор записей.
/// </param>
/// <param name="ALogRecord">
/// Извлекаемая запись.
/// </param>
/// <returns>
/// ISANLogRecord. Извлеченная запись или nil в случае неудачи.
/// </returns>
function SANLogRecordsExtract(const ALogRecords : ISANLogRecords; const ALogRecord : ISANLogRecord) : ISANLogRecord;

/// <summary>
/// Удалить элемент из набора.
/// </summary>
/// <param name="ALogRecords">
/// Набор записей.
/// </param>
/// <param name="ALogRecord">
/// Удаляемая запись.
/// </param>
/// <returns>
/// WordBool. True, удаление успешно.
/// </returns>
function SANLogRecordsRemove(const ALogRecords : ISANLogRecords; const ALogRecord : ISANLogRecord) : WordBool;

/// <summary>
/// Сравнить два пути к логу.
/// </summary>
/// <param name="APath1">
/// Первый путь для сравнения.
/// </param>
/// <param name="APath2">
/// Второй путь для сравнения.
/// </param>
/// <returns>
/// WordBool. True, пути совпадают.
/// </returns>
function SANLogsPathsIsEquals(const APath1, APath2 : ISANLogPath) : WordBool;

/// <summary>
/// Проверка существования лога по имени.
/// </summary>
/// <param name="ALogsSet">
/// Набор логов.
/// </param>
/// <param name="AName">
/// Имя лога.
/// </param>
/// <returns>
/// WordBool. True, поиск успешен.
/// </returns>
function SANLogsSet_ExistsByName(const ALogsSet : ISANLogsSet; const AName : WideString) : WordBool;

/// <summary>
/// Извлечь лог из набора.
/// </summary>
/// <param name="ALogsSet">
/// Набор логов.
/// </param>
/// <param name="ALog">
/// Извлекаемый лог.
/// </param>
/// <returns>
/// ISANLog. Извлеченная запись или nil в случае неудачи.
/// </returns>
function SANLogsSet_Extract(const ALogsSet : ISANLogsSet; const ALog : ISANLog) : ISANLog;

/// <summary>
/// Удалить лог из набора.
/// </summary>
/// <param name="ALogsSet">
/// Набор логов.
/// </param>
/// <param name="ALog">
/// Удаляемый лог.
/// </param>
/// <returns>
/// WordBool. True, удаление успешно.
/// </returns>
function SANLogsSet_Remove(const ALogsSet : ISANLogsSet; const ALog : ISANLog) : WordBool;

/// <summary>
/// Получить существующий лог с именем ALogName или создать новый используя интерфейс настройки.
/// </summary>
/// <param name="AStorage">
/// Хранилище логов.
/// </param>
/// <param name="ALogName">
/// Имя лога.
/// </param>
/// <param name="APathSetup">
/// Интерфейс настройки лога.
/// </param>
/// <returns>
/// ISANLog. Интерфейс лога.
/// </returns>
function SANLogsStorageAttachLog(const AStorage : ISANLogsStorage; const ALogName : WideString;
  const APathSetup : ISANLogPathSetup) : ISANLog;

/// <summary>
/// Проверка существования хранилища по имени.
/// </summary>
/// <param name="AStoragesList">
/// Список хранилищ.
/// </param>
/// <param name="AName">
/// Имя хранилища.
/// </param>
/// <returns>
/// WordBool. True, поиск успешен.
/// </returns>
function SANLogsStoragesListFindByName(const AStoragesList : ISANLogsStoragesList;
  const AName : WideString) : WordBool;

/// <summary>
/// Извлечь хранилище из списка.
/// </summary>
/// <param name="AStoragesList">
/// Список хранилищ.
/// </param>
/// <param name="AStorage">
/// Извлекаемое хранилище.
/// </param>
/// <returns>
/// ISANLog. Извлеченная запись или nil в случае неудачи.
/// </returns>
function SANLogsStoragesListExtractByName(const AStoragesList : ISANLogsStoragesList;
  const AStorage : ISANLogsStorage) : ISANLogsStorage;

/// <summary>
/// Удалить хранилище из списка.
/// </summary>
/// <param name="AStoragesList">
/// Список хранилищ.
/// </param>
/// <param name="AStorage">
/// Удаляемое хранилище.
/// </param>
/// <returns>
/// WordBool. True, удаление успешно.
/// </returns>
function SANLogsStoragesListRemoveByName(const AStoragesList : ISANLogsStoragesList;
  const AStorage : ISANLogsStorage) : WordBool;

IMPLEMENTATION

function SANLogRecordsExists(const ALogRecords : ISANLogRecords; const AID : integer) : WordBool;
var
  vLR : ISANLogRecord;
begin
  Result := ALogRecords.FindByID(vLR, AID);
end;

function SANLogRecordsExtract(const ALogRecords : ISANLogRecords; const ALogRecord : ISANLogRecord) : ISANLogRecord;
begin
  Result := ALogRecords.ExtractByID(ALogRecord.ID);
end;

function SANLogRecordsRemove(const ALogRecords : ISANLogRecords; const ALogRecord : ISANLogRecord) : WordBool;
begin
  Result := ALogRecords.RemoveByID(ALogRecord.ID);
end;

function SANLogsPathsIsEquals(const APath1, APath2 : ISANLogPath) : WordBool;
begin
  Result := not (Assigned(APath1) or Assigned(APath2))
         or Assigned(APath1) and Assigned(APath2) and APath1.IsEqualsPath(APath2);
end;

function SANLogsSet_ExistsByName(const ALogsSet : ISANLogsSet; const AName : WideString) : WordBool;
var
  vLog : ISANLog;
begin
  Result := ALogsSet.FindByName(vLog, AName);
end;

function SANLogsSet_Extract(const ALogsSet : ISANLogsSet; const ALog : ISANLog) : ISANLog;
begin
  Result := ALogsSet.ExtractByName(ALog.Name);
end;

function SANLogsSet_Remove(const ALogsSet : ISANLogsSet; const ALog : ISANLog) : WordBool;
begin
  Result := ALogsSet.RemoveByName(ALog.Name);
end;

function SANLogsStorageAttachLog(const AStorage : ISANLogsStorage; const ALogName : WideString;
  const APathSetup : ISANLogPathSetup) : ISANLog;
var
  vPath : ISANLogPath;
begin

  if AStorage.FindByName(Result, ALogName) then
    Exit;

  vPath := AStorage.CreateNewPath;

  APathSetup.SetupLogPath(AStorage, vPath);

  Result := AStorage.AddLog(ALogName, vPath);
end;

function SANLogsStoragesListFindByName(const AStoragesList : ISANLogsStoragesList; const AName : WideString) : WordBool;
var
  vLS : ISANLogsStorage;
begin
  Result := AStoragesList.FindByName(vLS, AName);
end;

function SANLogsStoragesListExtractByName(const AStoragesList : ISANLogsStoragesList;
  const AStorage : ISANLogsStorage) : ISANLogsStorage;
begin
  Result := AStoragesList.ExtractByName(AStorage.Name);
end;

function SANLogsStoragesListRemoveByName(const AStoragesList : ISANLogsStoragesList;
  const AStorage : ISANLogsStorage) : WordBool;
begin
  Result := AStoragesList.RemoveByName(AStorage.Name);
end;



END.
