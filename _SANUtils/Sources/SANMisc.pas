{*******************************************************}
{                                                       }
{       SANMisc                                         }
{       Набор разнообразных функций                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANMisc;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes, Types, StrUtils;

TYPE
  ESANMiscError = class(Exception);

  TEnumObjectsProc = procedure (const AObject : TObject; var AParam : Pointer);
  TEnumObjectsObjProc = procedure (const AObject : TObject; var AParam : Pointer) of object;

/// <summary>
/// Приобразование указателя в читабельную строку.
/// Первая %s в формате заменяется на 16-ричное значение.
/// </summary>
function PtrToStr(const P : Pointer; const AFormatStr : string = '$%s'; const ANilFormatStr : string = 'nil') : string;

/// <summary>
/// Приобразование точки в читабельную строку.
/// Первые 2 %d в формате заменяется на X и Y.
/// </summary>
function PointToStr(const APoint : TPoint; const AFormatStr : string = '(%d;%d)'):string;

/// <summary>
/// Приобразование прямоугольной области в читабельную строку.
/// Первые 4 %d в формате заменяется на Left, Top, Right, Bottom.
/// </summary>
function RectToStr(const ARect : TRect; const AFormatStr : string = '(%d,%d,%d,%d)'):string;

/// <summary>
/// Not Or And. Условие ACondition не выполняется,
/// или выполняются вместе и ACondition, и AExtCondition.
/// </summary>
function NOA(const ACondition, AExtCondition : boolean) : boolean;

/// <summary>
/// Попытка получить из строки AStr подстроку, начинающуюся с AIndex и
/// заканчивающуюся одним из элемнтов ASeps или концом строки.
/// </summary>
function ReadSubStr(const AStr : string; var AIndex : integer; var ASubstr : string;
  const ASeps : TSysCharSet) : boolean; overload;

/// <summary>
/// Попытка получить из строки AStr подстроку, начинающуюся с AIndex и
/// заканчивающуюся подстрокой ASep или концом строки.
/// </summary>
function ReadSubStr(const AStr : string; var AIndex : integer; var ASubstr : string;
  const ASep : string) : boolean; overload;

/// <summary>
/// Склеить массив строк AStrArr, добавляя к каждой строке ASep и в конце AEnd.
/// </summary>
function StrArrToStr(const AStrArr : array of string; const ASep : string = '';
  const AEnd : string = ''): string;

/// <summary>
/// Построить буфер AnsiString по массивам указателей на источники и длин источников.
/// </summary>
procedure BuildBuffer(var ABuffer : AnsiString;
  const ALengthsArr : array of integer; const ASourcesPtrsArr : array of Pointer); overload;

/// <summary>
/// Построить буфер TBytes по массивам указателей на источники и длин источников.
/// </summary>
procedure BuildBuffer(var ABuffer : TBytes;
  const ALengthsArr : array of integer; const ASourcesPtrsArr : array of Pointer); overload;

/// <summary>
/// Добавить к стороковой переменной одну строку с отступом и переводом каретки.
/// </summary>
procedure StrAdd(var S : string; const ALine : string; const AIndent : integer = 0);

/// <summary>
/// Добавить к стороковой переменной одну форматированную строку с отступом и переводом каретки.
/// </summary>
procedure StrAddFmt(var S : string; const AFmtLine : string; const AArgs : array of const;
  const AIndent : integer = 0);

/// <summary>
/// К каждой подстроке до #13#10 в строке S добавить слева AIndentStr x AIndent.
/// </summary>
function StrShift(const S : string; const AIndent : integer; const AIndentStr : string = ' ') : string;

/// <summary>
/// Перевод 16-ричного числа в 10-е.
/// </summary>
function HexToInt(HexStr : string) : Int64;

/// <summary>
/// Уровень иерархии класса
/// </summary>
function GetClassLevel(AClass : TClass) : integer;

/// <summary>
/// Обработка массива объектов процедурой с параметром
/// </summary>
procedure EnumObjects(const AEnumProc : TEnumObjectsProc; AParam : Pointer;
  const AObjectArr : array of TObject); overload;

/// <summary>
/// Обработка массива объектов процедурой с параметром
/// </summary>
procedure EnumObjects(const AEnumProc : TEnumObjectsObjProc; AParam : Pointer;
  const AObjectArr : array of TObject); overload;

/// <summary>
/// Проверить, является ли строка корректным именем идентификатора паскаля.
/// </summary>
function CheckPasIdentifierName(S : string) : boolean;

IMPLEMENTATION

resourcestring
  sArrayLenthAndSorcesErrorMessage    = 'Не совпадает количество элементов в массиве длин (%d) и массиве источников(%d)';

function PtrToStr(const P : Pointer; const AFormatStr, ANilFormatStr : string) : string;
var
  S, F : string;
begin
  S := IntToHex( Integer(P), SizeOf(Pointer)*2);

  if Assigned(P) then
    F := AFormatStr
  else
    F := ANilFormatStr;

  Result := Format(F, [S]);
end;

function PointToStr(const APoint : TPoint; const AFormatStr : string):string;
begin
  with APoint do
    Result := Format(AFormatStr, [X, Y]);
end;

function RectToStr(const ARect : TRect; const AFormatStr : string):string;
begin
  with ARect do
    Result := Format(AFormatStr, [Left, Top, Right, Bottom]);
end;

function NOA(const ACondition, AExtCondition : boolean) : boolean;
begin
  Result := not ACondition or ACondition and AExtCondition;
end;

function ReadSubStr(const AStr : string; var AIndex : integer; var ASubstr : string;
  const ASeps : TSysCharSet) : boolean;
var
  i : integer;
begin

  Result := (AIndex <= Length(AStr));

  if not Result then
    Exit;


  i := AIndex + 1;
  while (i <= Length(AStr)) and (not CharInSet(AStr[i], ASeps)) do
    Inc(i);

  ASubstr := Copy(AStr, AIndex, i - AIndex);
  AIndex := i + 1;
end;

function ReadSubStr(const AStr : string; var AIndex : integer; var ASubstr : string;
  const ASep : string) : boolean;
var
  i : integer;
begin

  Result := (AIndex <= Length(AStr));

  if not Result then
    Exit;

  i := PosEx(ASep, AStr, AIndex);
  if i = 0 then
    i := Length(AStr) + 1;

  ASubstr := Copy(AStr, AIndex, i - AIndex);
  AIndex := i + Length(ASep);
end;

function StrArrToStr(const AStrArr : array of string; const ASep, AEnd : string) : string;
var
  i, l, n, vSubL, vSepL, vEndL : integer;
begin
  vSepL := Length(ASep);
  vEndL := Length(AEnd);
  l := vEndL + Length(AStrArr) * vSepL;

  for i := 0 to High(AStrArr) do
    Inc(l, Length(AStrArr[i]));

  SetLength(Result, l);

  n := 1;

  for i := 0 to High(AStrArr) do
  begin
    vSubL := Length(AStrArr[i]);

    Move(AStrArr[i][1], Result[n], vSubL * SizeOf(Char));
    Inc(n, vSubL);

    Move(ASep[1], Result[n], vSepL * SizeOf(Char));
    Inc(n, vSepL);
  end;

  if vEndL > 0 then
    Move(AEnd[1], Result[n], vEndL * SizeOf(Char));
end;

procedure BuildBuffer(var ABuffer : AnsiString;
  const ALengthsArr : array of integer; const ASourcesPtrsArr : array of Pointer);

  procedure BufferLength;
  var
    i, vLength : integer;
  begin
    vLength := 0;
    for i := Low(ALengthsArr) to High(ALengthsArr) do
      Inc(vLength, ALengthsArr[i]);

    SetLength(ABuffer, vLength);
  end;

  procedure BufferMoves;
  var
    i, vOffset : integer;
  begin
    vOffset := 0;
    for i := Low(ALengthsArr) to High(ALengthsArr) do
    begin
      Move(ASourcesPtrsArr[i]^, ABuffer[1 + vOffset], ALengthsArr[i]);
      Inc(vOffset, ALengthsArr[i]);
    end;
  end;

begin

  if Length(ALengthsArr) <> Length(ASourcesPtrsArr) then
    raise ESANMiscError.CreateResFmt(@sArrayLenthAndSorcesErrorMessage, [Length(ALengthsArr), Length(ASourcesPtrsArr)]);

  BufferLength;

  BufferMoves;
end;

procedure BuildBuffer(var ABuffer : TBytes;
  const ALengthsArr : array of integer; const ASourcesPtrsArr : array of Pointer); overload;

  procedure BufferLength;
  var
    i, vLength : integer;
  begin
    vLength := 0;
    for i := Low(ALengthsArr) to High(ALengthsArr) do
      Inc(vLength, ALengthsArr[i]);

    SetLength(ABuffer, vLength);
  end;

  procedure BufferMoves;
  var
    i, vOffset : integer;
  begin
    vOffset := 0;
    for i := Low(ALengthsArr) to High(ALengthsArr) do
    begin
      Move(ASourcesPtrsArr[i]^, ABuffer[vOffset], ALengthsArr[i]);
      Inc(vOffset, ALengthsArr[i]);
    end;
  end;

begin

  if Length(ALengthsArr) <> Length(ASourcesPtrsArr) then
    raise ESANMiscError.CreateResFmt(@sArrayLenthAndSorcesErrorMessage, [Length(ALengthsArr), Length(ASourcesPtrsArr)]);

  BufferLength;

  BufferMoves;
end;

procedure StrAdd(var S : string; const ALine : string; const AIndent : integer = 0);
begin
  S := S + DupeString('  ', AIndent) + ALine + #13#10;
end;

procedure StrAddFmt(var S : string; const AFmtLine : string; const AArgs : array of const;
  const AIndent : integer = 0);
begin
  StrAdd(S, Format(AFmtLine, AArgs), AIndent);
end;

function StrShift(const S : string; const AIndent : integer; const AIndentStr : string = ' ') : string;
var
  vPos, vOffset, L, L2 : integer;
  vAddStr : string;
begin
  vOffset := 1;
  vAddStr := DupeString(AIndentStr, AIndent);
  L := Length(S);
  L2 := Length(#13#10);
  Result := '';

  repeat
    Result := Result + vAddStr;

    vPos := PosEx(#13#10, S, vOffset);
    if vPos = 0 then
      Break;

    Result := Result + Copy(S, vOffset, vPos - vOffset + L2);

    vOffset := vPos + L2;
  until vOffset > L;

  Result := Result + Copy(S, vOffset, MaxInt);
end;


function HexToInt(HexStr : string) : Int64;
var
  i : integer;
begin
  HexStr := UpperCase(HexStr);

  if HexStr[Length(HexStr)] = 'H' then
    Delete(HexStr, Length(HexStr), 1);

  Result := 0;

  for i := 1 to Length(HexStr) do
  begin

    Result := Result shl 4;
    if CharInSet(HexStr[i], ['0'..'9']) then
      Result := Result + (Byte(HexStr[i]) - 48)

    else
      if CharInSet(HexStr[i], ['A'..'F']) then
        Result := Result + (Byte(HexStr[i]) - 55)

      else
        Exit(0);

  end;

end;

function GetClassLevel(AClass : TClass) : integer;
begin

  if not Assigned(AClass) then
    Exit(0);

  Result := 1;

  while AClass <> TObject do  begin
    Inc(Result);
    AClass := AClass.ClassParent;
  end;

end;

procedure EnumObjects(const AEnumProc : TEnumObjectsProc; AParam : Pointer;
  const AObjectArr : array of TObject);
var
  i : integer;
begin
  for i := Low(AObjectArr) to High(AObjectArr) do
    AEnumProc(AObjectArr[i], AParam);
end;

procedure EnumObjects(const AEnumProc : TEnumObjectsObjProc; AParam : Pointer;
  const AObjectArr : array of TObject);
var
  i : integer;
begin
  for i := Low(AObjectArr) to High(AObjectArr) do
    AEnumProc(AObjectArr[i], AParam);
end;

function CheckPasIdentifierName(S : string) : boolean;
var
  i : integer;
begin
  Result := False;

  if S = '' then
    Exit;

  S := LowerCase(S);

  if not CharInSet(S[1], ['a'..'z', '_']) then
    Exit;

  for i := 2 to Length(S) do
    if not CharInSet(S[i], ['a'..'z', '0'..'9', '_']) then
      Exit;

  Result := True;
end;

END.
