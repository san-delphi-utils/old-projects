{*******************************************************}
{                                                       }
{       SANPkgAsDllClientDM                             }
{       Клиентская часть (внутри пакета)                }
{       взаимодействия с "пакетом-как-DLL".             }
{       Оформлена в виде модуля данных,                 }
{       для визуального проектирования                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPkgAsDllClientDM;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf, SANPkgAsDllCustomDM, SANPkgAsDllClient;

TYPE

  /// <summary>
  /// Модуль данных для визуального проектирования точки входа "пакета-как-DLL".
  /// </summary>
  TSANPkgAsDllEntryDataModule = class(TSANVersionInitDoneDataModule)
  published
    property GUID;
    property Caption;
    property Description;
    property URL;
    property Author;
    property Version;

    property OnInit;
    property OnDone;
  end;
  TSANPkgAsDllEntryDataModuleClass = class of TSANPkgAsDllEntryDataModule;

  /// <summary>
  /// Переопределение точки входа, под визуальное проектирование.
  /// </summary>
  TSANPkgAsDllEntryForDM = class(TSANPkgAsDllEntry)
  private
    FDM : TSANPkgAsDllEntryDataModule;

  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;

    procedure DoInit(const AParams: IInterface); override;
    procedure DoDone(const AParams: IInterface); override;

    property DM : TSANPkgAsDllEntryDataModule  read FDM;

  public
    constructor Create; override;
    destructor Destroy; override;

  end;

var
  PkgAsDllEntryDataModuleClass : TSANPkgAsDllEntryDataModuleClass = TSANPkgAsDllEntryDataModule;

IMPLEMENTATION

//==============================================================================
// TSANPkgAsDllEntryForDM
// protected
function TSANPkgAsDllEntryForDM.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  Result := inherited QueryInterface(IID, Obj);

  if Result = E_NOINTERFACE then
    Result := FDM.QueryInterface(IID, Obj);
end;

procedure TSANPkgAsDllEntryForDM.DoInit(const AParams: IInterface);
begin
  FDM.DoInit(AParams);
end;

procedure TSANPkgAsDllEntryForDM.DoDone(const AParams: IInterface);
begin
  FDM.DoDone(AParams);
end;

// public
constructor TSANPkgAsDllEntryForDM.Create;
begin
  inherited;

  FDM := PkgAsDllEntryDataModuleClass.Create(nil);
end;

destructor TSANPkgAsDllEntryForDM.Destroy;
begin
  FreeAndNil(FDM);

  inherited;
end;
//==============================================================================

INITIALIZATION
  PkgAsDllEntryClass := TSANPkgAsDllEntryForDM;
END.
