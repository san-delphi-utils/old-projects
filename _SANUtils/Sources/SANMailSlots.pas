{*******************************************************}
{                                                       }
{       SANMailSlots                                    }
{       Работа с почтовыми ящиками.                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANMailSlots;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Classes, StrUtils, SyncObjs,
  SANProtocol;

TYPE
  ESANMailSlotError = class(Exception);
//==============================================================================
  /// <summary>
  /// Абстрактный объект работы с почтовым ящиком
  /// </summary>
  TSANCustomMailSlot = class(TInterfacedPersistent)
  private
    FMailSlotName, FEventName : string;
    FMailSlot, FEvent : THandle;

  protected
    procedure DoCreateMailSlot(var AMailSlot : THandle); virtual; abstract;
    procedure DoCreateEvent(var AEvent : THandle); virtual; abstract;

  public
    constructor Create(const AMailSlotName, AEventName : string;
      const AMailSlot : THandle = INVALID_HANDLE_VALUE;
      const AEvent : THandle = INVALID_HANDLE_VALUE);

    destructor Destroy; override;

    property MailSlotName  : string  read FMailSlotName;
    property EventName     : string  read FEventName;

    property MailSlot  : THandle  read FMailSlot;
    property Event     : THandle  read FEvent;
  end;

  /// <summary>
  /// Клиентский объект работы с почтовым ящиком
  /// </summary>
  TSANClientMailSlot = class(TSANCustomMailSlot, ISANTransporterSender)
  protected
    procedure DoCreateMailSlot(var AMailSlot : THandle); override;
    procedure DoCreateEvent(var AEvent : THandle); override;


  public
    procedure SendBuffer(const ABuffer : AnsiString);

  end;

  /// <summary>
  /// Серверный объект работы с почтовым ящиком
  /// </summary>
  TSANServerMailSlot = class(TSANCustomMailSlot, ISANTransporterReceiver)
  private
    FProtocolRouterCS : TCriticalSection;
    FProtocolRouter : TSANProtocolQueuedRouter;
    FServerThread : TThread;

  protected
    function GetProtocolRouter : TSANProtocolBaseRouter;
    procedure SetProtocolRouter(const AProtocolRouter : TSANProtocolBaseRouter);

    procedure DoCreateMailSlot(var AMailSlot : THandle); override;
    procedure DoCreateEvent(var AEvent : THandle); override;

  public
    constructor Create(const AMailSlotName, AEventName : string;
      const AMailSlot : THandle = INVALID_HANDLE_VALUE;
      const AEvent : THandle = INVALID_HANDLE_VALUE);

    destructor Destroy; override;

    class function TryCreate(var AMailSlot; const AMailSlotName, AEventName : string) : boolean;

    /// <summary>
    /// Роутер команд
    /// </summary>
    property ProtocolRouter : TSANProtocolBaseRouter  read GetProtocolRouter  write SetProtocolRouter;
  end;
//==============================================================================

IMPLEMENTATION

resourcestring
  sMailSlotServerModeErrorMessage         = 'Создание почтового ящика в режиме сервера:';
  sMailSlotClientModeErrorMessage         = 'Создание почтового ящика в режиме клиента:';
  sMailSlotAccessErrorMessage             = 'Получение доступа к ящику «%s»: %s';
  sEventAccessErrorMessage                = 'Создание системного события «%s»: %s';
  sServerMailSlotServerReadingThreadName  = 'MailSlotServerReadingThread (%s; %s)';

//==============================================================================
// TSANCustomMailSlot
// public
constructor TSANCustomMailSlot.Create(const AMailSlotName, AEventName : string;
  const AMailSlot, AEvent: THandle);
begin
  inherited Create;

  FMailSlotName  := AMailSlotName;
  FEventName     := AEventName;
  FMailSlot      := AMailSlot;
  FEvent         := AEvent;

  if FMailSlot = INVALID_HANDLE_VALUE then
    DoCreateMailSlot(FMailSlot);

  if FEvent = INVALID_HANDLE_VALUE then
    DoCreateEvent(FEvent);

end;

destructor TSANCustomMailSlot.Destroy;
begin
  CloseHandle(FEvent);
  CloseHandle(FMailSlot);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANClientMailSlot
// protected
procedure TSANClientMailSlot.DoCreateMailSlot(var AMailSlot: THandle);
begin
  AMailSlot := CreateFile
  (
    PChar(MailslotName),
    GENERIC_WRITE,
    FILE_SHARE_READ,
    nil,
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL,
    0
  );

  if AMailSlot = INVALID_HANDLE_VALUE then
    raise ESANMailSlotError.CreateResFmt(@sMailSlotAccessErrorMessage, [MailslotName, SysErrorMessage(GetLastError)]);
end;

procedure TSANClientMailSlot.DoCreateEvent(var AEvent: THandle);
begin
  AEvent := OpenEvent(EVENT_MODIFY_STATE, False, PChar(EventName));

  if AEvent = INVALID_HANDLE_VALUE then
    raise ESANMailSlotError.CreateResFmt(@sEventAccessErrorMessage, [EventName, SysErrorMessage(GetLastError)]);
end;

// public
procedure TSANClientMailSlot.SendBuffer(const ABuffer: AnsiString);
var
  vBytesWritten : Cardinal;
begin
  WriteFile(FMailSlot, ABuffer[1], Length(ABuffer), vBytesWritten, nil);

  SetEvent(FEvent);
end;
//==============================================================================

//==============================================================================
type
  TSANServerMailSlotServerReadingThread = class(TThread)
  private
    FServerMailSlot : TSANServerMailSlot;

  protected
    /// <summary>
    /// Ожидание события почтового ящика
    /// </summary>
    /// <returns>
    /// Boolean. True - Ожидание успешно
    /// </returns>
    function WaitMailSlotEvent : boolean;

    /// <summary>
    /// Прочитать порцию данных в буфер ABuffer
    /// </summary>
    /// <returns>
    /// Boolean. True - Успешно
    /// </returns>
    function ReadBuffer(var ABuffer : AnsiString) : boolean;

    /// <summary>
    /// Ожидание события почтового ящика и чтение порции данных в буфер ABuffer
    /// </summary>
    /// <returns>
    /// Boolean. True - Ожидание успешно
    /// </returns>
    function ReadBufferByEvent(var ABuffer : AnsiString) : boolean;

    procedure Execute; override;

  public
    constructor Create(const AServerMailSlot : TSANServerMailSlot);

  end;

// protected
function TSANServerMailSlotServerReadingThread.WaitMailSlotEvent: boolean;
begin
  case WaitForSingleObject(FServerMailSlot.FEvent, 500) of
    WAIT_OBJECT_0:
      Result := True;

    WAIT_FAILED:
      raise ESANMailSlotError.Create(SysErrorMessage(GetLastError));

    else
      Result := False;
  end;
end;

function TSANServerMailSlotServerReadingThread.ReadBuffer(var ABuffer: AnsiString): boolean;
var
  vMessageSize, l: DWORD;
begin
  // Получаем размер следующего сообщения в почтовом ящике
  GetMailslotInfo(FServerMailSlot.FMailSlot, nil, vMessageSize, nil, nil);

  // Если сообщения нет
  Result := (vMessageSize <> MAILSLOT_NO_MESSAGE) and (vMessageSize > 0);
  if not Result then
    Exit;

  l := Length(ABuffer);

  SetLength(ABuffer, l + vMessageSize);

  ReadFile(FServerMailSlot.FMailSlot, ABuffer[l + 1], vMessageSize, vMessageSize, nil);
end;

function TSANServerMailSlotServerReadingThread.ReadBufferByEvent(
  var ABuffer: AnsiString): boolean;
begin
  Sleep(0);

  Result := WaitMailSlotEvent and ReadBuffer(ABuffer);
end;

procedure TSANServerMailSlotServerReadingThread.Execute;
var
  vDecodingRec : TSANProtocolDecodingRec;
  vRouter : TSANProtocolQueuedRouter;
begin
  NameThreadForDebugging
  (
    AnsiString
    (
      Format
      (
        LoadResString(@sServerMailSlotServerReadingThreadName),
        [FServerMailSlot.MailSlotName, FServerMailSlot.EventName]
      )
    )
  );


  FillChar(vDecodingRec, SizeOf(TSANProtocolDecodingRec), 0);


  repeat

    if not ReadBufferByEvent(vDecodingRec.Buffer) then
      Continue;


    vRouter := FServerMailSlot.FProtocolRouter;
    if not Assigned(vRouter) then
      Continue;


    FServerMailSlot.FProtocolRouterCS.Enter;
    try

      vRouter.InThreadDecoding(Self, ReadBufferByEvent, vDecodingRec, FServerMailSlot, nil);

    finally
      FServerMailSlot.FProtocolRouterCS.Leave
    end;

  until Terminated;

end;

// public
constructor TSANServerMailSlotServerReadingThread.Create(const AServerMailSlot: TSANServerMailSlot);
begin
  FServerMailSlot := AServerMailSlot;

  inherited Create(False);
end;
//==============================================================================

//==============================================================================
// TSANServerMailSlot
// protected
function TSANServerMailSlot.GetProtocolRouter: TSANProtocolBaseRouter;
begin
  Result := FProtocolRouter;
end;

procedure TSANServerMailSlot.SetProtocolRouter(const AProtocolRouter: TSANProtocolBaseRouter);
begin
  FProtocolRouterCS.Enter;
  try
    FProtocolRouter := AProtocolRouter as TSANProtocolQueuedRouter;
  finally
    FProtocolRouterCS.Leave
  end;
end;

procedure TSANServerMailSlot.DoCreateMailSlot(var AMailSlot: THandle);
begin
  AMailSlot := CreateMailSlot(PChar(MailslotName), 0, MAILSLOT_WAIT_FOREVER, nil);

  if (AMailSlot = INVALID_HANDLE_VALUE) then
    raise ESANMailSlotError.CreateResFmt(@sMailSlotAccessErrorMessage, [MailslotName, SysErrorMessage(GetLastError)]);
end;

procedure TSANServerMailSlot.DoCreateEvent(var AEvent: THandle);
begin
  AEvent := CreateEvent(nil, False, False, PChar(EventName));

  if AEvent = INVALID_HANDLE_VALUE then
    raise ESANMailSlotError.CreateResFmt(@sEventAccessErrorMessage, [EventName, SysErrorMessage(GetLastError)]);
end;

// public
constructor TSANServerMailSlot.Create(const AMailSlotName, AEventName : string;
  const AMailSlot, AEvent: THandle);
begin
  inherited;

  FProtocolRouterCS := TCriticalSection.Create;

  FServerThread := TSANServerMailSlotServerReadingThread.Create(Self);
end;

destructor TSANServerMailSlot.Destroy;
begin
  FServerThread.Terminate;
  FServerThread.WaitFor;

  FreeAndNil(FServerThread);

  FreeAndNil(FProtocolRouterCS);

  inherited;
end;

class function TSANServerMailSlot.TryCreate(var AMailSlot; const AMailSlotName,
  AEventName: string): boolean;
var
  vMailSlot, vEvent : THandle;
  vLastError : Cardinal;
begin

  vMailSlot := CreateMailSlot(PChar(AMailslotName), 0, MAILSLOT_WAIT_FOREVER, nil);
  if (vMailSlot = INVALID_HANDLE_VALUE) then
  begin
    vLastError := GetLastError;

    if vLastError = ERROR_ALREADY_EXISTS then
      Exit(False)

    else
      raise ESANMailSlotError.CreateResFmt(@sMailSlotAccessErrorMessage, [AMailslotName, SysErrorMessage(vLastError)]);
  end;

  vEvent := CreateEvent(nil, False, False, PChar(AEventName));
  if vEvent = INVALID_HANDLE_VALUE then
  begin
    CloseHandle(vMailSlot);

    vLastError := GetLastError;

    if vLastError = ERROR_ALREADY_EXISTS then
      Exit(False)

    else
      raise ESANMailSlotError.CreateResFmt(@sEventAccessErrorMessage, [AEventName, SysErrorMessage(vLastError)]);
  end;

  Result := True;

  TObject(AMailSlot) := Self.Create(AMailSlotName, AEventName, vMailSlot, vEvent);
end;
//==============================================================================


END.
