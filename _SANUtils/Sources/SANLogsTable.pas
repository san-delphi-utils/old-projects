{*******************************************************}
{                                                       }
{       SANLogsTable                                    }
{       Вывод текста ввиде таблицы.                     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANLogsTable;
{$I NX.INC}
INTERFACE

USES
  SysUtils, Classes, Contnrs, StrUtils, Math,
  SANLogsIntf, SANLogsBase;

TYPE
  ESANLogTableError = class(ESANLogError);

  TSLColumn = class(TPersistent)
  private
    FWidth      : word;
    FAlignment  : TAlignment;
    FStrings    : TStrings;

  protected
    procedure AssignTo(Dest : TPersistent); override;

  public
    constructor Create;
    destructor Destroy; override;

    /// <summary>
    /// Ширина колонки
    /// </summary>
    property Width      : word        read FWidth      write FWidth;
    /// <summary>
    /// Выравнивание текста колонки
    /// </summary>
    property Alignment  : TAlignment  read FAlignment  write FAlignment;
    /// <summary>
    /// Буфер позволяющий сначала назначить текст каждой колонки,
    /// а затем вывести их все текста,  Выравнивание текста колонки
    /// </summary>
    property Strings    : TStrings    read FStrings;

  end;
//==============================================================================
  TSLColumns = class(TObjectList)
  private
    function Get_MaxRowCount: integer;

  protected
    function GetItem(Index: Integer): TSLColumn;

  public
    function Add: TSLColumn;
    function Extract(Item: TSLColumn): TSLColumn;
    function First: TSLColumn;
    function Last: TSLColumn;

    property Items[Index: Integer]: TSLColumn read GetItem; default;

    /// <summary>
    /// Максимальное кол-во строк в тексте среди всех колонок таблицы
    /// </summary>
    property MaxRowCount : integer  read Get_MaxRowCount;

  end;
//==============================================================================
  TSLTable = class(TInterfacedPersistent, ISANCustomLog)
  private
    FRedirectLog : ISANCustomLog;

    FColumns : TSLColumns;

    FVLineChar, FHLineChar, FCrossChar : Char;

    function Get_TotalWidth: integer;

  protected
    // ISANCustomLog
    function Write(const AKind : LongWord; const ASubType : WideString; const AMessage : WideString = ''; ADateTime : TDateTime = 0) : integer; safecall;
    function BeginWrite(const AKind : LongWord; const ASubType : WideString; ADateTime : TDateTime = 0) : integer; safecall;
    procedure PartialWrite(const APartialMessage : WideString); safecall;
    procedure EndWrite; safecall;

    procedure ValidateRedirectLog;
    procedure RedirectLogPartialWrite(const APartialMessage : string);

    /// <summary>
    /// Форматировать текст AFullCellText в соответствии с параметрами столбца с индексом AColIndex,
    /// игнорировать выравнивание колонок
    /// </summary>
    function CellText(const AFullCellText : string; const AColIndex : integer; const AAlignment : TAlignment) : string; overload;
    /// <summary>
    /// Форматировать текст AFullCellText в соответствии с параметрами столбца с индексом AColIndex
    /// </summary>
    function CellText(const AFullCellText : string; const AColIndex : integer) : string; overload;

  public
    constructor Create(const ARedirectLog : ISANCustomLog);
    destructor Destroy; override;

    /// <summary>
    /// Начало вывода в таблицу.
    /// </summary>
    procedure TableBeginWrite(const AKind : LongWord; const ASubType : string; ADateTime : TDateTime = 0);
    /// <summary>
    /// Завершение вывода в таблицу.
    /// </summary>
    procedure TableEndWrite;

    /// <summary>
    /// Задать кол-во и ширину колонок таблици
    /// </summary>
    procedure InitColsByWidth(const AColWidths : array of word);
    /// <summary>
    /// Вывести горизонтальную линию таблицы
    /// AShowColsSep - показывать разделители колонок
    /// </summary>
    procedure Write_HrzLine(const AShowColsSep : boolean = True);
    /// <summary>
    /// Вывод многострочного заголовка таблицы (без деления на колонки)
    /// из массива строк
    /// </summary>
    procedure Write_Header(const AAlignment : TAlignment; const AStrArr : array of string); overload;
    /// <summary>
    /// Вывод многострочного заголовка таблицы (без деления на колонки)
    /// из списка строк
    /// </summary>
    procedure Write_Header(const AAlignment : TAlignment; const AStrings : TStrings); overload;
    /// <summary>
    /// Вывод многострочного заголовка таблицы (без деления на колонки)
    /// из строки разделением по столбцам строки строки ASeparatedStr
    /// подстрокой ASeparator
    /// </summary>
    procedure Write_Header(const AAlignment : TAlignment; const ASeparatedStr : string;
      const ASeparator : string = #13#10); overload;
    /// <summary>
    /// Вывод номеров колонок
    /// </summary>
    procedure Write_ColsNumbers(const AAlignment : TAlignment = taLeftJustify);
    /// <summary>
    /// Вывод строки таблицы из списка строк
    /// </summary>
    procedure Write_Strings(const AStrings : TStrings);
    /// <summary>
    /// Очистить текст колонок
    /// </summary>
    procedure Clear_ColsText;
    /// <summary>
    /// Вывод строки таблицы из списков строк колонок
    /// </summary>
    procedure Write_ColsText;
    /// <summary>
    /// Вывод строки таблицы из массива строк
    /// </summary>
    procedure Write_StrArr(const AStrArr : array of string);
    /// <summary>
    /// Вывод строки таблицы, c разделением по столбцам строки ASeparatedStr
    /// подстрокой ASeparator
    /// </summary>
    procedure Write_SepStr(const ASeparatedStr : string; const ASeparator : string = '|');

    /// <summary>
    /// Ссылка на лог, в который перенаправляется вывод
    /// </summary>
    property RedirectLog : ISANCustomLog  read FRedirectLog  write FRedirectLog;

    /// <summary>
    /// Список колонок таблицы
    /// </summary>
    property Columns : TSLColumns  read FColumns;

    /// <summary>
    /// Символ, вертикальной линии. По умолчанию "|"
    /// </summary>
    property VLineChar : Char  read FVLineChar  write FVLineChar;
    /// <summary>
    /// Символ, горизонтальной линии. По умолчанию "-"
    /// </summary>
    property HLineChar : Char  read FHLineChar  write FHLineChar;
    /// <summary>
    /// Символ, пересечения линиий. По умолчанию "+"
    /// </summary>
    property CrossChar : Char  read FCrossChar  write FCrossChar;

    /// <summary>
    /// Полная ширина одной строки таблицы с текущими настройками.
    /// </summary>
    property TotalWidth : integer  read Get_TotalWidth;

  end;
//==============================================================================

IMPLEMENTATION

resourcestring
  sNotLogLinkErrorMessage  = 'Для табличного вывода логов не задана ссылка на лог';

//==============================================================================
// TSLColumn
// protected
procedure TSLColumn.AssignTo(Dest: TPersistent);
begin
  if not (Dest is TSLColumn) then
    inherited

  else  with Dest as TSLColumn do  begin
    FWidth      := Self.FWidth;
    FAlignment  := Self.FAlignment;

    FStrings.Assign(Self.FStrings);
  end;//with; else if
end;

// public
constructor TSLColumn.Create;
begin
  inherited Create;

  FStrings := TStringList.Create;
end;

destructor TSLColumn.Destroy;
begin
  FreeAndNil(FStrings);

  inherited;
end;
//==============================================================================

//==============================================================================
// TSLColumns
// private
function TSLColumns.Get_MaxRowCount: integer;
var
  i : integer;
begin
  Result := 0;

  for i := 0 to Count - 1 do
    Result := MAX(Result, Items[i].Strings.Count);
end;

// protected
function TSLColumns.GetItem(Index: Integer): TSLColumn;
begin
  Result := TSLColumn(inherited Items[Index])
end;

// public
function TSLColumns.Add: TSLColumn;
begin
  Result := TSLColumn.Create;

  inherited Add(Result);
end;

function TSLColumns.Extract(Item: TSLColumn): TSLColumn;
begin
  Result := TSLColumn(inherited Extract(Item));
end;

function TSLColumns.First: TSLColumn;
begin
  Result := TSLColumn(inherited First);
end;

function TSLColumns.Last: TSLColumn;
begin
  Result := TSLColumn(inherited Last);
end;
//==============================================================================

//==============================================================================
// TSLTable
// private
function TSLTable.Get_TotalWidth: integer;
var
  i : integer;
begin

  Result := 0;


  if Columns.Count = 0 then
    Exit;


  for i := 0 to Columns.Count - 1 do
    Inc(Result, Columns[i].Width);

  Inc(Result, Columns.Count*3 + 1);
end;

// protected
function TSLTable.Write(const AKind: LongWord; const ASubType,
  AMessage: WideString; ADateTime: TDateTime): integer;
begin
  Write_HrzLine;
  Write_SepStr(AMessage);
  Write_HrzLine;
  Result := 0;
end;

function TSLTable.BeginWrite(const AKind : LongWord; const ASubType : WideString; ADateTime : TDateTime) : integer;
begin
  Write_HrzLine;
  Result := 0;
end;

procedure TSLTable.PartialWrite(const APartialMessage : WideString);
begin
  Write_SepStr(APartialMessage);
end;

procedure TSLTable.EndWrite;
begin
  Write_HrzLine;
end;

procedure TSLTable.ValidateRedirectLog;
begin
  if not Assigned(FRedirectLog) then
    raise ESANLogTableError.CreateRes(@sNotLogLinkErrorMessage);
end;

procedure TSLTable.RedirectLogPartialWrite(const APartialMessage: string);
begin
  ValidateRedirectLog;

  FRedirectLog.PartialWrite(APartialMessage + #13#10);
end;

function TSLTable.CellText(const AFullCellText: string; const AColIndex: integer;
  const AAlignment: TAlignment): string;
var
  vColumn : TSLColumn;
  vDLgth : integer;
begin
  vColumn := Columns[AColIndex];
  vDLgth := vColumn.Width - Length(AFullCellText);

  if vDLgth <= 0 then
    Result := Copy(AFullCellText, 1, vColumn.Width)

  else  case AAlignment of
    taLeftJustify:
      Result := AFullCellText + DupeString(' ', vDLgth);

    taRightJustify:
      Result := DupeString(' ', vDLgth) + AFullCellText;

    taCenter:
      Result := DupeString(' ', vDLgth div 2) + AFullCellText + DupeString(' ', vDLgth div 2) + IfThen(Odd(vDLgth), ' ');
  end;//case; else if


  Result := ' ' + Result + ' ' + VLineChar;
end;

function TSLTable.CellText(const AFullCellText: string; const AColIndex: integer): string;
begin
  Result := CellText(AFullCellText, AColIndex, Columns[AColIndex].Alignment);
end;

// public
constructor TSLTable.Create(const ARedirectLog : ISANCustomLog);
begin
  inherited Create;

  FRedirectLog := ARedirectLog;

  FColumns := TSLColumns.Create(True);

  FVLineChar := '|';
  FHLineChar := '-';
  FCrossChar := '+';
end;

destructor TSLTable.Destroy;
begin
  FreeAndNil(FColumns);

  inherited;
end;

procedure TSLTable.TableBeginWrite(const AKind: LongWord;
  const ASubType: string; ADateTime: TDateTime);
begin
  ValidateRedirectLog;

  FRedirectLog.BeginWrite(AKind, ASubType, ADateTime);
end;

procedure TSLTable.TableEndWrite;
begin
  ValidateRedirectLog;

  FRedirectLog.EndWrite;
end;

procedure TSLTable.InitColsByWidth(const AColWidths: array of Word);
var
  i : integer;
begin
  Columns.Clear;

  for i := Low(AColWidths) to High(AColWidths) do
    Columns.Add.FWidth := AColWidths[i];
end;

procedure TSLTable.Write_HrzLine(const AShowColsSep : boolean);
var
  S : string;

  procedure AddColumnChars(const AColIndex : integer; const AEndChar : Char);
  begin
    S := S
       + DupeString(HLineChar, Columns[AColIndex].Width + 2)
       + AEndChar;
  end;

var
  i : integer;
  vColsSep : Char;
begin
  S := CrossChar;

  if AShowColsSep then
    vColsSep := CrossChar
  else
    vColsSep := HLineChar;

  for I := 0 to Columns.Count - 2 do
    AddColumnChars(i, vColsSep);

  AddColumnChars(Columns.Count - 1, CrossChar);

  RedirectLogPartialWrite(S);
end;

procedure TSLTable.Write_Header(const AAlignment : TAlignment; const AStrArr: array of string);
var
  i, vWidth, dWidth : integer;
  S : string;
begin

  if Length(AStrArr) = 0 then
    Exit;


  vWidth := TotalWidth - 4;

  for i := Low(AStrArr) to High(AStrArr) do  begin
    dWidth := vWidth - Length(AStrArr[i]);

    case AAlignment of
      taLeftJustify:
        S := AStrArr[i] + DupeString(' ', dWidth);

      taRightJustify:
        S := DupeString(' ', dWidth) + AStrArr[i];

      taCenter:
        S := DupeString(' ', dWidth div 2) + AStrArr[i] + DupeString(' ', dWidth div 2 + ORD(Odd(dWidth)));

    end;//case

    RedirectLogPartialWrite(VLineChar + ' ' + S + ' ' + VLineChar);
  end;//for

end;

procedure TSLTable.Write_Header(const AAlignment: TAlignment; const AStrings: TStrings);
begin
  Write_Header(AAlignment, AStrings.ToStringArray);
end;

procedure TSLTable.Write_Header(const AAlignment: TAlignment;
  const ASeparatedStr, ASeparator : string);
var
  vStrings : TStringList;
begin
  vStrings := TStringList.Create;
  try
    vStrings.LineBreak  := ASeparator;
    vStrings.Text       := ASeparatedStr;

    Write_Header(AAlignment, vStrings);
  finally
    FreeAndNil(vStrings);
  end;//t..f
end;

procedure TSLTable.Write_ColsNumbers(const AAlignment: TAlignment);
var
  i : integer;
  vRow : string;
begin
  vRow := VLineChar;

  for i := 0 to Columns.Count - 1 do
    vRow := vRow + CellText(IntToStr(i + 1), i, AAlignment);

  RedirectLogPartialWrite(vRow);
end;

procedure TSLTable.Write_Strings(const AStrings: TStrings);
var
  i : integer;
  vRow : string;
begin
  vRow := VLineChar;

  for i := 0 to MIN(AStrings.Count - 1, Columns.Count - 1) do
    vRow := vRow + CellText(AStrings[i], i);

  for i := AStrings.Count to Columns.Count - 1 do
    vRow := vRow + CellText('', i);

  RedirectLogPartialWrite(vRow);
end;

procedure TSLTable.Clear_ColsText;
var
  i : integer;
begin
  for i := 0 to Columns.Count - 1 do
    Columns[i].Strings.Clear;
end;

procedure TSLTable.Write_ColsText;
var
  i, vRowIndex, vRowCount : integer;
  vRow : string;
begin
  vRowCount := Columns.MaxRowCount;

  for vRowIndex := 0 to vRowCount - 1 do  begin
    vRow := VLineChar;

    for i := 0 to Columns.Count - 1 do
      with Columns[i].Strings do
        if vRowIndex < Count then
          vRow := vRow + CellText(Strings[vRowIndex], i)
        else
          vRow := vRow + CellText('', i);

    RedirectLogPartialWrite(vRow);
  end;//for vRowIndex

  Clear_ColsText;
end;

procedure TSLTable.Write_StrArr(const AStrArr: array of string);
var
  i : integer;
begin
  for i := 0 to MIN(High(AStrArr), Columns.Count - 1) do
    Columns[i].Strings.Text := AStrArr[i];

  for i := High(AStrArr) + 1 to Columns.Count - 1 do
    Columns[i].Strings.Text := '';

  Write_ColsText;
end;

procedure TSLTable.Write_SepStr(const ASeparatedStr, ASeparator : string);
var
  vStrings : TStringList;
begin
  vStrings := TStringList.Create;
  try
    vStrings.LineBreak  := ASeparator;
    vStrings.Text       := ASeparatedStr;

    Write_Strings(vStrings);
  finally
    FreeAndNil(vStrings);
  end;//t..f
end;
//==============================================================================


END.
