{*******************************************************}
{                                                       }
{       SANProtocol                                     }
{       Абстрактный протокол передачи данных            }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANProtocol;
{$I NX.INC}
INTERFACE

USES
  Windows, Messages, SysUtils, Classes, Contnrs, SyncObjs,
  SANSync, SANIntfUtils;

CONST
  /// <summary>
  /// Префикс всех методов-обработчиков команды
  /// </summary>
  SAN_PROTOCOL_COMMAND_PREFIX = 'CMD_';

  /// <summary>
  /// Сообщение обработки очереди записей данных
  /// </summary>
  CM_SAN_PROTOCOL_PROCESS_QUEUE = WM_USER + 1;

TYPE
  ESANProtocolError = class(Exception);
  //----------------------------------------------------------------------------
  ISANProtocolCodec = interface;
  TSANProtocolBaseRouter = class;
  //----------------------------------------------------------------------------
  /// <summary>
  /// Указатель на структуру полученных данныч
  /// </summary>
  PSANProtocolDataRec = ^TSANProtocolDataRec;
  /// <summary>
  /// Структура полученных данныч
  /// </summary>
  TSANProtocolDataRec = record
    /// <summary>
    /// Передатчик данных
    /// </summary>
    Transporter : Pointer;

    /// <summary>
    /// Соединение передачи данных
    /// </summary>
    Connection : Pointer;

    /// <summary>
    /// Пакет данных
    /// </summary>
    DataPackage : Pointer;
  end;
  //----------------------------------------------------------------------------
  /// <summary>
  /// Параметры используемые для декодирования пакета
  /// для чтения одного однородного фрагмента данных
  /// </summary>
  TSANProtocolDecodingStageRec = record
    /// <summary>
    /// Номер этапа
    /// </summary>
    StageNo : integer;
    /// <summary>
    /// Количество байт, которое осталось прочитать на данном этапе
    /// </summary>
    Left : integer;
    /// <summary>
    /// Смещение на данном этапе
    /// </summary>
    Offset : integer;
    /// <summary>
    /// Шаг на данном этапе (высчитывается по буферу и Left). Изменяет Left и Offset
    /// </summary>
    Step : integer;
  end;
  //----------------------------------------------------------------------------
  /// <summary>
  /// Параметры используемые для декодирования пакета
  /// </summary>
  TSANProtocolDecodingRec = record

    /// <summary>
    /// Обрабатываемый буфер
    /// </summary>
    Buffer : AnsiString;

    /// <summary>
    /// Ссылка на создаваемый пакет
    /// </summary>
    DataPackage : Pointer;

    /// <summary>
    /// Параметры используемые для декодирования пакета
    /// для поэтапного чтения
    /// </summary>
    Stage : TSANProtocolDecodingStageRec;

    /// <summary>
    /// Ссылка на временные данные декодирования
    /// </summary>
    TempData : Pointer;

    /// <summary>
    /// Смещение от начала буфера. Часть буфера до смещения не анализируется
    /// </summary>
    BufOffset : integer;

    /// <summary>
    /// Длина буфера при анализе. Часть буфера после значения длины не анализируется
    /// </summary>
    BufLength : integer;

    /// <summary>
    /// Новое декодирование по протоколу AProtocolCodec
    /// </summary>
    procedure BeginDecoding(const AProtocolCodec : ISANProtocolCodec);

    /// <summary>
    /// Буфер изменен, необходимо перезаполнить BufOffset и BufLength
    /// </summary>
    procedure ResetBuffer;

    /// <summary>
    /// Копирование данных по адресу Address из буфера Buffer, используя Stage
    /// </summary>
    procedure MovePtr(const AAddress : Pointer);

    /// <summary>
    /// Копирование данных в ANSI-строку AAnsiStr
    /// </summary>
    procedure MoveAnsiStr(var AAnsiStr : AnsiString);

    /// <summary>
    /// Копирование данных в открытый массив байт
    /// </summary>
    procedure MoveBytes(var ABytes : TBytes);

    /// <summary>
    /// Завершить текущее декодирование по протоколу AProtocolCodec
    /// </summary>
    procedure EndDecoding(const AProtocolCodec : ISANProtocolCodec);
  end;
  //----------------------------------------------------------------------------
  TSANProtocolCommand = procedure(const ADataRec : PSANProtocolDataRec;
    var AHandled : boolean) of object;
  TSANProtocolGetCommanderEvent = procedure (const ASender : TObject;
    const ADataRec : PSANProtocolDataRec; var ACommander : TObject) of object;
  TSANProtocolCommandEvent = procedure (const ASender : TObject;
    const ADataRec : PSANProtocolDataRec) of object;
  TSANProtocolCommandErrorEvent = procedure (const ASender, ACommander : TObject;
    const AException : Exception; const ACommandName : string;
    const ADataRec : PSANProtocolDataRec) of object;
  TSANProtocolReadBufferFunc = function (var ABuffer : AnsiString) : boolean of object;
  TSANProtocolCodecStdDecodeFunc = function (
    var ADecodingParams : TSANProtocolDecodingRec) : boolean of object;
  //----------------------------------------------------------------------------
  /// <summary>
  /// Кодер-декодер протокола. Все что нужно, чтобы характеризовать протокол.
  /// </summary>
  ISANProtocolCodec = interface(IInterface)
    ['{AC31F8AF-38AC-4E01-8528-EE703BFCAC4D}']

    function Get_Name : WideString; safecall;
    /// <summary>
    /// Имя протокола
    /// </summary>
    property Name : WideString  read Get_Name;

    /// <summary>
    /// Создать новый пакет данных
    /// </summary>
    /// <param name="ANewDataPackage">
    /// Созданый пакет
    /// </param>
    /// <param name="ATemplateDataPackage">
    /// Если задан, новый пакет создается как копия ATemplateDataPackage
    /// </param>
    procedure NewDataPackage(var ANewDataPackage; const ATemplateDataPackage: Pointer = nil);

    /// <summary>
    /// Освобождение памяти, занимаемой пакетом. Если ADataPackage = nil,
    /// ничего не делает
    /// </summary>
    /// <param name="ADataPackage">
    /// Пакет данных
    /// </param>
    procedure FreeDataPackage(const ADataPackage: Pointer);

    /// <summary>
    /// Получение имени команды из пакета данных. Пакет не меняется.
    /// </summary>
    /// <param name="ADataPackage">
    /// Пакет данных данного протокола
    /// </param>
    function ReadCommandName(const ADataPackage: Pointer) : string;

    /// <summary>
    /// Выделить память на временную информацию декодирования
    /// </summary>
    /// <param name="ATempData">
    /// Временная информация для декодирования
    /// </param>
    procedure CreateDecodingTempData(var ATempData);

    /// <summary>
    /// Освобождение памяти, занимаемой информацией о временной информацией декодирования.
    ///  Если ATempData = nil, ничего не делает
    /// </summary>
    /// <param name="ATempData">
    /// Временная информация для декодирования
    /// </param>
    procedure FreeDecodingTempData(const ATempData: Pointer);

    /// <summary>
    /// Декодировать пакет ADataPackage из буфера ABuffer.
    /// </summary>
    /// <param name="ADecodingParams">
    /// Параметры декодирования, запись TSANProtocolDecodingRec
    /// </param>
    /// <returns>
    /// Boolean. True - пакет успешно декодирован
    /// </returns>
    function DecodeDataPackage(var ADecodingParams : TSANProtocolDecodingRec) : boolean;

    /// <summary>
    /// Передать пакет данных по всем кодекам стека, для получения буфера.
    /// </summary>
    /// <param name="ADataPackage">
    /// Пакет данных данного протокола
    /// </param>
    /// <param name="ABuffer">
    /// Буфер в виде ANSI-строки, на основе части которого создается пакет
    /// </param>
    procedure EncodeDataPackage(const ADataPackage: Pointer; var ABuffer : AnsiString);

  end;

  /// <summary>
  /// Динамический кодер-декодер протокола. Может изменяться во времени,
  /// а следовательно должен поддерживать блокировку своего изменения.
  /// </summary>
  ISANProtocolCodecDynamic = interface(ISANProtocolCodec)
    ['{947EBC19-F8B5-43F9-A543-4A43A48DC3FE}']

    function GetLocker : TSANLocker;

    /// <summary>
    /// Многопоточный замок
    /// </summary>
    property Locker : TSANLocker  read GetLocker;
  end;

  /// <summary>
  /// Интерфейс отправки данных
  /// </summary>
  ISANTransporterSender = interface(IInterface)
    ['{9E54C7AF-B031-433B-ADDC-6B87E0E6903E}']

    /// <summary>
    /// Отправить буфер данных
    /// </summary>
    procedure SendBuffer(const ABuffer : AnsiString);
  end;

  /// <summary>
  /// Интерфейс получения данных
  /// </summary>
  ISANTransporterReceiver = interface(IInterface)
    ['{E71E22A3-936E-4E94-86A9-EC42EE757455}']

    function GetProtocolRouter : TSANProtocolBaseRouter;
    procedure SetProtocolRouter(const AProtocolRouter : TSANProtocolBaseRouter);

    /// <summary>
    /// Роутер команд
    /// </summary>
    property ProtocolRouter : TSANProtocolBaseRouter  read GetProtocolRouter  write SetProtocolRouter;
  end;

  /// <summary>
  /// Абсрактный класс кодека протокола со стандартным декодингом
  /// </summary>
  TSANCustomProtocolCodec = class(TInterfacedObject, ISANNamed, ISANProtocolCodec)
  protected
    function Get_Name : WideString; virtual; safecall; abstract;
    procedure NewDataPackage(var ANewDataPackage; const ATemplateDataPackage: Pointer = nil); virtual; abstract;
    procedure FreeDataPackage(const ADataPackage: Pointer); virtual; abstract;
    function ReadCommandName(const ADataPackage: Pointer) : string; virtual; abstract;
    procedure CreateDecodingTempData(var ATempData); virtual; abstract;
    procedure FreeDecodingTempData(const ATempData: Pointer); virtual; abstract;
    function DecodeDataPackage(var ADecodingParams : TSANProtocolDecodingRec) : boolean; virtual;
    procedure EncodeDataPackage(const ADataPackage: Pointer; var ABuffer : AnsiString); virtual; abstract;

    function StdDecodeMove(var ADecodingParams : TSANProtocolDecodingRec) : boolean; virtual; abstract;
    function StdDecodeChange(var ADecodingParams : TSANProtocolDecodingRec) : boolean; virtual; abstract;
  end;

  /// <summary>
  /// Список интерфейсов кодеков протоколов ISANProtocolCodec
  /// </summary>
  TSANProtocolCodecsList = class(TInterfacedPersistent)
  private
    FLocker : TSANLocker;
    FProtocolCodecs : IInterfaceList;

  private
    function GetCount: integer;
    function GetItems(const Index: integer): ISANProtocolCodec;
    function GetByName(const AProtocolCodecName: string): ISANProtocolCodec;
    function GetFirst: ISANProtocolCodec;
    function GetLast: ISANProtocolCodec;

  protected
    procedure DoLock(Sender : TObject); virtual;
    procedure DoUnLock(Sender : TObject); virtual;

  public
    constructor Create;
    destructor Destroy; override;

    /// <summary>
    /// Добавить кодек протокола в список
    /// </summary>
    /// <returns>
    /// Boolean. True - успешно добавлено
    /// </returns>
    function Add(const AProtocolCodec : ISANProtocolCodec) : boolean;
    /// <summary>
    /// Найти кодек протокола по имени
    /// </summary>
    function Find(out AProtocolCodec : ISANProtocolCodec; const AProtocolCodecName : string) : boolean; overload;
    /// <summary>
    /// Найти кодек протокола по имени
    /// </summary>
    function Find(const AProtocolCodecName : string) : ISANProtocolCodec; overload;
    /// <summary>
    /// Удалить из списка кодек протокола с именем AProtocolCodecName
    /// </summary>
    /// <returns>
    /// Boolean. True - успешно удалено
    /// </returns>
    function Remove(const AProtocolCodecName : string) : boolean;
    /// <summary>
    /// Очистить список
    /// </summary>
    /// <returns>
    /// Boolean. True - успешно
    /// </returns>
    function Clear : boolean;

  public
    property Count : integer  read GetCount;
    property Items[const Index : integer] : ISANProtocolCodec  read GetItems;
    property ByName[const AProtocolCodecName : string] : ISANProtocolCodec  read GetByName; default;
    property First : ISANProtocolCodec  read GetFirst;
    property Last : ISANProtocolCodec  read GetLast;
    property Locker : TSANLocker  read FLocker;
  end;

  /// <summary>
  /// Стек протоколов. Буфер преобразуется в пакет данных проходя через стек
  /// в прямом направлении, а пакет преобразуется в буфер проходя в обратном направлении.
  /// </summary>
  TSANProtocolStack = class(TSANProtocolCodecsList, ISANNamed, ISANProtocolCodec, ISANProtocolCodecDynamic)
  protected
    procedure DoLock(Sender : TObject); override;
    procedure DoUnLock(Sender : TObject); override;

    // ISANProtocolCodec
    function Get_Name : WideString; safecall;
    procedure NewDataPackage(var ANewDataPackage; const ATemplateDataPackage: Pointer = nil);
    procedure FreeDataPackage(const ADataPackage: Pointer);
    function ReadCommandName(const ADataPackage: Pointer) : string;
    procedure CreateDecodingTempData(var ATempData);
    procedure FreeDecodingTempData(const ATempData: Pointer);
    function DecodeDataPackage(var ADecodingParams : TSANProtocolDecodingRec) : boolean;
    procedure EncodeDataPackage(const ADataPackage: Pointer; var ABuffer : AnsiString);

    // ISANProtocolCodecDynamic
    function GetLocker : TSANLocker;

  public
    property ProtocolCodecName : WideString  read Get_Name;

  end;

  /// <summary>
  /// Простой объект распределения команд
  /// </summary>
  TSANProtocolBaseRouter = class(TInterfacedPersistent)
  private
    FCommanders : TObjectList;

    FProtocolCodec: ISANProtocolCodec;
    FProtocolCodecLocker : TSANLocker;

    FOnGetCommander : TSANProtocolGetCommanderEvent;
    FOnUnHandledCommand : TSANProtocolCommandEvent;
    FOnError : TSANProtocolCommandErrorEvent;

  protected
    /// <summary>
    /// Проверка прототипа метода. (Пока не реализована)
    /// </summary>
    procedure ValidateCommandPrototype(const ACommander : TObject; const AMethodName : string);

  public
    constructor Create;
    destructor Destroy; override;

    /// <summary>
    /// Смена кодека протокола
    /// </summary>
    /// <returns>
    /// Boolean. True - кодек протокола изменен
    /// </returns>
    function ChangeProtocolCodec(const AProtocolCodec: ISANProtocolCodec) : boolean;

    /// <summary>
    /// Выполнение команды, как published-метода объекта ACommander
    /// </summary>
    /// <returns>
    /// Boolean. True - команда обработана
    /// </returns>
    function ProcessCommand(const ACommander : TObject;
      const ADataRec : TSANProtocolDataRec) : boolean;

    /// <summary>
    /// Проход по списку объектов Commanders и попытка выполнить команду ProcessCommand
    /// </summary>
    /// <returns>
    /// Boolean. True - команда обработана
    /// </returns>
    function Routing(const ADataRec : TSANProtocolDataRec) : boolean;

    /// <summary>
    /// Список объектов, реагирующих на команды
    /// </summary>
    property Commanders : TObjectList  read FCommanders;

    /// <summary>
    /// Кодек протокола
    /// </summary>
    property ProtocolCodec : ISANProtocolCodec  read FProtocolCodec;

    /// <summary>
    /// Событие получения объекта обработки команды
    /// </summary>
    property OnGetCommander : TSANProtocolGetCommanderEvent  read FOnGetCommander  write FOnGetCommander;

    /// <summary>
    /// Событие при неперехваченой команде
    /// </summary>
    property OnUnHandledCommand : TSANProtocolCommandEvent  read FOnUnHandledCommand  write FOnUnHandledCommand;

    /// <summary>
    /// Событие при ошибке выполнения команды
    /// </summary>
    property OnError : TSANProtocolCommandErrorEvent  read FOnError  write FOnError;
  end;

  /// <summary>
  /// Объект распределения команд и накопления пакетов данных для обработки
  /// </summary>
  TSANProtocolQueuedRouter = class(TSANProtocolBaseRouter)
  private
    FSyncQueuesSet : TSANSyncQueuesSet;
    FDataRecsQueue : TSANSyncQueue;

  protected
    procedure DataRecProcessed(const Sender : TObject; const APtr : Pointer);

    // Набор очередей синхронизации данных между потоками
    property SyncQueuesSet : TSANSyncQueuesSet  read FSyncQueuesSet;

  public
    constructor Create;
    destructor Destroy; override;

    /// <summary>
    /// Добавление пакета в очередь
    /// </summary>
    /// <param name="ATransporter">
    /// Передатчик данных
    /// </param>
    /// <param name="AConnection">
    /// Соединение передачи данных
    /// </param>
    /// <param name="ADataPackage">
    /// Пакет данных протокола ProtocolCodec
    /// </param>
    procedure PackagesQueueAdd(const ATransporter, AConnection, ADataPackage: Pointer);


    /// <summary>
    /// Процедура декодирования в потоке
    /// </summary>
    /// <param name="AThread">
    /// Вызывающий поток
    /// </param>
    /// <param name="AReadingBuffer">
    /// Метод потока, читающий данные в буфер
    /// </param>
    /// <param name="ADecodingParams">
    /// Параметры декодирования
    /// </param>
    /// <param name="ADataPackage">
    /// Пакет данных протокола ProtocolCodec
    /// </param>
    /// <param name="ATransporter">
    /// Передатчик данных
    /// </param>
    /// <param name="AConnection">
    /// Соединение передачи данных
    /// </param>
    procedure InThreadDecoding(const AThread : TThread;
      const AReadingBuffer : TSANProtocolReadBufferFunc;
      var ADecodingParams : TSANProtocolDecodingRec;
      const ATransporter, AConnection : Pointer);

  end;

/// <summary>
/// Проверка, назначен ли протокол AProtocolCodec
/// </summary>
procedure SANProtocolCodecValidate(const AProtocolCodec : ISANProtocolCodec);

/// <summary>
/// Очистка пакета и переменной в которой пакет находится
/// </summary>
procedure SANProtocolCodecFreeAndNilDataPackage(const AProtocolCodec : ISANProtocolCodec;
  var ADataPackage);

/// <summary>
/// Заблокировать изменение протокола AProtocolCodec
/// </summary>
procedure SANProtocolCodecLock(const AProtocolCodec : ISANProtocolCodec);
/// <summary>
/// Разблокировать изменение протокола AProtocolCodec
/// </summary>
procedure SANProtocolCodecUnLock(const AProtocolCodec : ISANProtocolCodec);
/// <summary>
/// Заблокирован ли протокол AProtocolCodec
/// </summary>
/// <returns>
/// Boolean. True - кодек заблокирован.
/// </returns>
function SANProtocolCodecLocked(const AProtocolCodec : ISANProtocolCodec) : boolean;

/// <summary>
/// Стандартное поэтапное декодирование. Сводится к вызывам в цикле методов
/// копирования данных определенной длинны и смены этапов декодирования Stage.
/// На этапе Stage = 0 вызывается метод AChangeStageFunc и начинается этап Stage = 1.
/// </summary>
/// <param name="AMoveDataFunc">
/// Функция копирования данных. Результат игнорируется
/// </param>
/// <param name="AChangeStageFunc">
/// Функция смены этапа. True - декодирование полностью завершено
/// </param>
/// <returns>
/// Boolean. True - декодирование полностью завершено
/// </returns>
function SANProtocolCodecStdDecoding(var ADecodingParams : TSANProtocolDecodingRec;
  const AMoveDataFunc, AChangeStageFunc : TSANProtocolCodecStdDecodeFunc) : boolean;

/// <summary>
/// Отправка данных ADataPackage по протоколу AProtocolCodec
/// через транспортировщик отправки ATransporterSender
/// </summary>
procedure SANProtocolSend(const AProtocolCodec : ISANProtocolCodec;
  const ATransporterSender : ISANTransporterSender; const ADataPackage: Pointer);


IMPLEMENTATION

uses
  SANListUtils;

resourcestring
  sProtocolCodecNotAssignedErrorMessage    = 'Кодек протокола (ISANProtocolCodec) не назначен';
  sProtocolStackNotCompatibleErrorMessage  = 'Пакет протокола «%s» не совместим с пакетом протокола «%s»';
  sInvalidCommandPrototypeErrorMessage     = 'Метод «%s» не соответствует прототипу обработчика команды';
  sProtocolCodecNotFoundErrorMessage       = 'Не найден кодек протокола «%s»';
  sCommandNameIsErrorMessage               = 'При выполнении команды «%s»';

type
  TThreadHack = class(TThread);

procedure SANProtocolCodecValidate(const AProtocolCodec : ISANProtocolCodec);
begin
  if not Assigned(AProtocolCodec) then
    raise ESANProtocolError.CreateRes(@sProtocolCodecNotAssignedErrorMessage);
end;

procedure SANProtocolCodecFreeAndNilDataPackage(const AProtocolCodec : ISANProtocolCodec;
  var ADataPackage);
begin
  AProtocolCodec.FreeDataPackage( Pointer(ADataPackage) );
  Pointer(ADataPackage) := nil;
end;

procedure SANProtocolCodecLock(const AProtocolCodec : ISANProtocolCodec);
var
  vPCDyn : ISANProtocolCodecDynamic;
begin

  if AProtocolCodec.QueryInterface(ISANProtocolCodecDynamic, vPCDyn) <> S_OK then
    Exit;

  vPCDyn.Locker.Lock;
end;

procedure SANProtocolCodecUnLock(const AProtocolCodec : ISANProtocolCodec);
var
  vPCDyn : ISANProtocolCodecDynamic;
begin

  if AProtocolCodec.QueryInterface(ISANProtocolCodecDynamic, vPCDyn) <> S_OK then
    Exit;

  vPCDyn.Locker.UnLock;
end;

function SANProtocolCodecLocked(const AProtocolCodec : ISANProtocolCodec) : boolean;
var
  vPCDyn : ISANProtocolCodecDynamic;
begin
  Result := (AProtocolCodec.QueryInterface(ISANProtocolCodecDynamic, vPCDyn) = S_OK)
        and vPCDyn.Locker.IsLocked;
end;

function SANProtocolCodecStdDecoding(var ADecodingParams : TSANProtocolDecodingRec;
  const AMoveDataFunc, AChangeStageFunc : TSANProtocolCodecStdDecodeFunc) : boolean;
begin
  Result := False;

  if ADecodingParams.Stage.StageNo = 0 then
  begin
    AChangeStageFunc(ADecodingParams);
    Inc(ADecodingParams.Stage.StageNo);
  end;


  while ADecodingParams.BufOffset < ADecodingParams.BufLength do
  try
    ADecodingParams.Stage.Step := ADecodingParams.Stage.Left;

    if ADecodingParams.BufOffset + ADecodingParams.Stage.Step > ADecodingParams.BufLength then
      ADecodingParams.Stage.Step := ADecodingParams.BufLength - ADecodingParams.BufOffset;

    // Копирование
    AMoveDataFunc(ADecodingParams);

    Dec(ADecodingParams.Stage.Left, ADecodingParams.Stage.Step);

    if ADecodingParams.Stage.Left = 0 then
      begin
        ADecodingParams.Stage.Offset := 0;

        //Переход к следующей итерации
        if AChangeStageFunc(ADecodingParams) then
          Exit(True);

        Inc(ADecodingParams.Stage.StageNo);
      end
    else
      Inc(ADecodingParams.Stage.Offset, ADecodingParams.Stage.Step);

  finally
    Inc(ADecodingParams.BufOffset, ADecodingParams.Stage.Step);
  end;

end;

procedure SANProtocolSend(const AProtocolCodec : ISANProtocolCodec;
  const ATransporterSender : ISANTransporterSender; const ADataPackage: Pointer);
var
  vBuffer : AnsiString;
begin
  AProtocolCodec.EncodeDataPackage(ADataPackage, vBuffer);

  ATransporterSender.SendBuffer(vBuffer);
end;

//==============================================================================
// TSANProtocolDecodingRec
procedure TSANProtocolDecodingRec.BeginDecoding(const AProtocolCodec: ISANProtocolCodec);
begin
  SANProtocolCodecLock(AProtocolCodec);

  with Self do
  begin
    AProtocolCodec.NewDataPackage(DataPackage);
    AProtocolCodec.CreateDecodingTempData(TempData);

    System.FillChar(Stage, System.SizeOf(TSANProtocolDecodingStageRec), 0);
  end;

end;

procedure TSANProtocolDecodingRec.ResetBuffer;
begin

  with Self do
  begin
    BufLength := System.Length(Buffer);
    BufOffset := 0;
  end;

end;

procedure TSANProtocolDecodingRec.MovePtr(const AAddress: Pointer);
begin
  with Self do
    System.Move(Buffer[BufOffset + 1], (PChar(AAddress) + Stage.Offset)^, Stage.Step);
end;

procedure TSANProtocolDecodingRec.MoveAnsiStr(var AAnsiStr: AnsiString);
begin
  with Self do
    System.Move(Buffer[BufOffset + 1], AAnsiStr[1 + Stage.Offset], Stage.Step);
end;

procedure TSANProtocolDecodingRec.MoveBytes(var ABytes: TBytes);
begin
  with Self do
    System.Move(Buffer[BufOffset + 1], ABytes[Stage.Offset], Stage.Step);
end;

procedure TSANProtocolDecodingRec.EndDecoding(const AProtocolCodec: ISANProtocolCodec);
begin
  AProtocolCodec.FreeDecodingTempData(Self.TempData);

  with Self do
  begin
    DataPackage := nil;
    TempData := nil;
    System.SetLength(Buffer, BufLength);
    System.Delete(Buffer, 1, BufOffset);
  end;

  SANProtocolCodecUnLock(AProtocolCodec);
end;
//==============================================================================

//==============================================================================
// TSANCustomProtocolCodec
// protected
function TSANCustomProtocolCodec.DecodeDataPackage(
  var ADecodingParams: TSANProtocolDecodingRec): boolean;
begin
  Result := SANProtocolCodecStdDecoding(ADecodingParams, StdDecodeMove, StdDecodeChange);
end;
//==============================================================================

//==============================================================================
// TSANProtocolCodecsList
// ptivate
function TSANProtocolCodecsList.GetCount: integer;
begin
  Result := FProtocolCodecs.Count;
end;

function TSANProtocolCodecsList.GetItems(const Index: integer): ISANProtocolCodec;
begin
  Result := (FProtocolCodecs[Index] as ISANProtocolCodec);
end;

function TSANProtocolCodecsList.GetByName(const AProtocolCodecName: string): ISANProtocolCodec;
begin
  Result := Find(AProtocolCodecName);

  if not Assigned(Result) then
    raise ESANProtocolError.CreateResFmt(@sProtocolCodecNotFoundErrorMessage, [AProtocolCodecName]);
end;

function TSANProtocolCodecsList.GetFirst: ISANProtocolCodec;
begin
  Result := Items[0];
end;

function TSANProtocolCodecsList.GetLast: ISANProtocolCodec;
begin
  Result := Items[Count - 1];
end;

// protected
procedure TSANProtocolCodecsList.DoLock(Sender: TObject);
begin
end;

procedure TSANProtocolCodecsList.DoUnLock(Sender: TObject);
begin
end;

// public
constructor TSANProtocolCodecsList.Create;
begin
  inherited Create;

  FLocker := TSANLocker.Create(DoLock, DoUnLock);

  FProtocolCodecs := TInterfaceList.Create;
end;

destructor TSANProtocolCodecsList.Destroy;
begin
  FProtocolCodecs := nil;
  FreeAndNil(FLocker);
  inherited;
end;

function TSANProtocolCodecsList.Add(const AProtocolCodec: ISANProtocolCodec) : boolean;
begin
  FLocker.CriticalSection.Enter;
  try
    Result := not FLocker.IsLocked(False);

    if Result then
      FProtocolCodecs.Add(AProtocolCodec);

  finally
    FLocker.CriticalSection.Leave
  end;
end;

function TSANProtocolCodecsList.Find(out AProtocolCodec: ISANProtocolCodec;
  const AProtocolCodecName: string): boolean;
begin
  Result := SANListFind(AProtocolCodec, FProtocolCodecs, SANListFF_SANNamedFindByName, @AProtocolCodecName);
end;

function TSANProtocolCodecsList.Find(const AProtocolCodecName: string): ISANProtocolCodec;
begin
  if not Find(Result, AProtocolCodecName) then
    Result := nil;
end;

function TSANProtocolCodecsList.Remove(const AProtocolCodecName: string): boolean;
begin
  FLocker.CriticalSection.Enter;
  try

    Result := (not FLocker.IsLocked(False))
          and SANListRemove(FProtocolCodecs, SANListFF_SANNamedFindByName, @AProtocolCodecName);

  finally
    FLocker.CriticalSection.Leave
  end;
end;

function TSANProtocolCodecsList.Clear: boolean;
begin
  FLocker.CriticalSection.Enter;
  try
    Result := not FLocker.IsLocked(False);

    if Result then
      FProtocolCodecs.Clear;

  finally
    FLocker.CriticalSection.Leave
  end;
end;
//==============================================================================

//==============================================================================
// TSANProtocolStack
// protected
procedure TSANProtocolStack.DoLock(Sender: TObject);
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    SANProtocolCodecLock(Items[i]);
end;

procedure TSANProtocolStack.DoUnLock(Sender: TObject);
var
  i : integer;
begin
  for i := Count - 1 downto 0 do
    SANProtocolCodecUnLock(Items[i]);
end;

function TSANProtocolStack.Get_Name: WideString;
const
  PROTOCOL_STACK = 'ProtocolStack:';
var
  i : integer;
begin
  if Count = 0 then
    Result := PROTOCOL_STACK + '<Empty>'

  else
    begin
      Result := PROTOCOL_STACK + First.Name;

      for i := 1 to Count - 1 do
        Result := Result + '->' + Items[i].Name;
    end;
end;

procedure TSANProtocolStack.NewDataPackage(var ANewDataPackage;
  const ATemplateDataPackage: Pointer);
var
  vPtrArr : PPointerArray;
  i : integer;
begin

  GetMem(vPtrArr, SizeOf(Pointer)*Count);
  try
    FillChar(vPtrArr^[0], Count, 0);

    for i := 0 to Count - 1 do
      if Assigned(ATemplateDataPackage) then
        Items[i].NewDataPackage(vPtrArr^[i], PPointerArray(ATemplateDataPackage)^[i])
      else
        Items[i].NewDataPackage(vPtrArr^[i]);

    Pointer(ANewDataPackage) := vPtrArr;
  except
    FreeDataPackage(vPtrArr);
    Pointer(ANewDataPackage) := nil;
    raise;
  end;

end;

procedure TSANProtocolStack.FreeDataPackage(const ADataPackage: Pointer);
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    SANProtocolCodecFreeAndNilDataPackage(Items[i], PPointerArray(ADataPackage)^[i]);

  FreeMem(ADataPackage, SizeOf(Pointer)*Count);
end;

function TSANProtocolStack.ReadCommandName(const ADataPackage: Pointer): string;
var
  i : integer;
begin

  for i := 0 to Count - 1 do
  begin
    Result := Items[i].ReadCommandName( PPointerArray(ADataPackage)^[i] );

    if Result <> '' then
      Exit;
  end;

end;

procedure TSANProtocolStack.CreateDecodingTempData(var ATempData);
// Для внешнего вызова нужны только временные данные первого протокола
begin
  First.CreateDecodingTempData(ATempData);
end;

procedure TSANProtocolStack.FreeDecodingTempData(const ATempData: Pointer);
// Для внешнего вызова нужны только временные данные первого протокола
begin
  First.FreeDecodingTempData(ATempData);
end;

function TSANProtocolStack.DecodeDataPackage(var ADecodingParams : TSANProtocolDecodingRec) : boolean;
{
  procedure ProcessNextProtocols;
  var
    i : integer;
    vCodec : ISANProtocolCodec;
    vDP : TSANProtocolDecodingRec;
  begin

    for i := 1 to Count - 1 do
    begin
      vCodec := Items[i];

      vDP := ADecodingParams;
      vDP.DataPackage := PPointerArray(ADecodingParams.DataPackage)^[i];
      FillChar(vDP.Stage, SizeOf(TSANProtocolDecodingStageRec), 0);
      vCodec.CreateDecodingTempData(vDP.TempData);
      try

        if not vCodec.DecodeDataPackage(vDP) then
          raise ESANProtocolError.CreateResFmt
          (
            @sProtocolStackNotCompatibleErrorMessage,
            [vCodec.Name, Items[i - 1].Name]
          );

      finally
        vCodec.FreeDecodingTempData(vDP.TempData);
      end;
    end;

  end;
}
var
  i : integer;
  vDP : TSANProtocolDecodingRec;
  vCodec : ISANProtocolCodec;
begin
  Result := False;
//  Result := First.DecodeDataPackage(ADecodingParams);
//
//  if Result then
//    ProcessNextProtocols;

  vDP := ADecodingParams;
  try

    for i := 0 to Count - 1 do
    begin

      vCodec := Items[i];
      try

        vDP.DataPackage := PPointerArray(ADecodingParams.DataPackage)^[i];
        FillChar(vDP.Stage, SizeOf(TSANProtocolDecodingStageRec), 0);
        vCodec.CreateDecodingTempData(vDP.TempData);
        try

          Result := vCodec.DecodeDataPackage(vDP);

          if not Result then
            if i = 0 then
              Exit

            else
              raise ESANProtocolError.CreateResFmt
              (
                @sProtocolStackNotCompatibleErrorMessage,
                [vCodec.Name, Items[i - 1].Name]
              );

        finally
          vCodec.FreeDecodingTempData(vDP.TempData);
        end;

      finally
        vCodec := nil;
      end;

    end;

  finally
    ADecodingParams.BufOffset := vDP.BufOffset;
    ADecodingParams.BufLength := vDP.BufLength;
  end;

end;

procedure TSANProtocolStack.EncodeDataPackage(const ADataPackage: Pointer;
  var ABuffer: AnsiString);
var
  i : integer;
begin
  SetLength(ABuffer, 0);

  for i := Count - 1 downto 0 do
    Items[i].EncodeDataPackage(PPointerArray(ADataPackage)^[i], ABuffer);
end;

function TSANProtocolStack.GetLocker: TSANLocker;
begin
  Result := Locker;
end;
//==============================================================================

//==============================================================================
// TSANProtocolBaseRouter
// private

// protected
procedure TSANProtocolBaseRouter.ValidateCommandPrototype(
  const ACommander: TObject; const AMethodName: string);
begin
  if False then
    ESANProtocolError.CreateResFmt(@sInvalidCommandPrototypeErrorMessage, [AMethodName]);
end;

// public
constructor TSANProtocolBaseRouter.Create;
begin
  inherited Create;

  FProtocolCodecLocker := TSANLocker.Create;

  // По умолчанию роутер не владеет объектами команд
  FCommanders := TObjectList.Create(False);
end;

destructor TSANProtocolBaseRouter.Destroy;
begin
  FreeAndNil(FProtocolCodecLocker);

  FreeAndNil(FCommanders);

  inherited;
end;

function TSANProtocolBaseRouter.ChangeProtocolCodec(
  const AProtocolCodec: ISANProtocolCodec): boolean;
begin
  FProtocolCodecLocker.CriticalSection.Enter;
  try
    Result := not FProtocolCodecLocker.IsLocked(False);

    if Result then
      FProtocolCodec := AProtocolCodec;

  finally
    FProtocolCodecLocker.CriticalSection.Leave;
  end;
end;

function TSANProtocolBaseRouter.ProcessCommand(const ACommander: TObject;
  const ADataRec: TSANProtocolDataRec): boolean;
var
  vCommandName  : string;
  vMethod       : TMethod;
begin

  if ACommander is TSANProtocolBaseRouter then
    Exit( (ACommander as TSANProtocolBaseRouter).Routing(ADataRec) );


  SANProtocolCodecValidate(ProtocolCodec);

  try
    vCommandName := SAN_PROTOCOL_COMMAND_PREFIX + ProtocolCodec.ReadCommandName(ADataRec.DataPackage);

    vMethod.Code := ACommander.MethodAddress(vCommandName);
    if not Assigned(vMethod.Code) then
      Exit(False);

    ValidateCommandPrototype(ACommander, vCommandName);

    vMethod.Data := ACommander;

    TSANProtocolCommand(vMethod)(@ADataRec, Result);

  except
    on E : Exception do
      begin
        E.Message := LoadResString(@sCommandNameIsErrorMessage) + ' ' + E.Message;

        if Assigned(FOnError) then
          FOnError(Self, ACommander, E, vCommandName, @ADataRec)
        else
          raise;

      end;
  end;//t..e

end;

function TSANProtocolBaseRouter.Routing(const ADataRec: TSANProtocolDataRec): boolean;

  function GetCommander : boolean;
  var
    vCommander : TObject;
  begin
    FOnGetCommander(Self, @ADataRec, vCommander);

    Result := Assigned(vCommander)
          and ProcessCommand(vCommander, ADataRec);
  end;

  function ForCommanders : boolean;
  var
    i : integer;
  begin
    for i := 0 to FCommanders.Count - 1 do
      if ProcessCommand(FCommanders[i], ADataRec) then
        Exit(True);

    Result := False;
  end;

begin
  SANProtocolCodecValidate(ProtocolCodec);

  try

    if Assigned(FOnGetCommander) and GetCommander
    or ForCommanders
    then
      Exit(True);

    if Assigned(FOnUnHandledCommand) then
      FOnUnHandledCommand(Self, @ADataRec);

  finally
    ProtocolCodec.FreeDataPackage(ADataRec.DataPackage);
  end;

  Result := False;
end;
//==============================================================================

//==============================================================================
// TSANProtocolQueuedRouter
// private

// protected
procedure TSANProtocolQueuedRouter.DataRecProcessed(const Sender: TObject;
  const APtr: Pointer);
begin
  try
    Routing( PSANProtocolDataRec(APtr)^ );
  finally
    Dispose(APtr);
  end;
end;

// public
constructor TSANProtocolQueuedRouter.Create;
begin
  inherited Create;

  FSyncQueuesSet := TSANSyncQueuesSet.Create;
  FDataRecsQueue := FSyncQueuesSet.SyncQueues.Add(DataRecProcessed);
end;

destructor TSANProtocolQueuedRouter.Destroy;
begin
  FreeAndNil(FSyncQueuesSet);

  inherited;
end;

procedure TSANProtocolQueuedRouter.PackagesQueueAdd(const ATransporter, AConnection,
  ADataPackage: Pointer);
var
  vRec : PSANProtocolDataRec;
begin
  New(vRec);
  try
    vRec^.Transporter  := ATransporter;
    vRec^.Connection   := AConnection;
    vRec^.DataPackage  := ADataPackage;

    FDataRecsQueue.Add(vRec);
  except
    Dispose(vRec);
    raise;
  end;
end;

procedure TSANProtocolQueuedRouter.InThreadDecoding(const AThread: TThread;
  const AReadingBuffer: TSANProtocolReadBufferFunc;
  var ADecodingParams: TSANProtocolDecodingRec; const ATransporter,
  AConnection: Pointer);
begin
  SANProtocolCodecValidate(ProtocolCodec);

  FProtocolCodecLocker.Lock;
  try

    ADecodingParams.BeginDecoding(ProtocolCodec);
    try

      try

        ADecodingParams.ResetBuffer;
        if not ProtocolCodec.DecodeDataPackage(ADecodingParams) then
          repeat

            if TThreadHack(AThread).Terminated then
              Exit;

            if not AReadingBuffer(ADecodingParams.Buffer) then
              Continue;

            ADecodingParams.ResetBuffer;

            if ProtocolCodec.DecodeDataPackage(ADecodingParams) then
              Break;

          until FALSE;


        PackagesQueueAdd(ATransporter, AConnection, ADecodingParams.DataPackage);

      except
        SANProtocolCodecFreeAndNilDataPackage(ProtocolCodec, ADecodingParams.DataPackage);
        raise;
      end;

    finally
      ADecodingParams.EndDecoding(ProtocolCodec);
    end;

  finally
    FProtocolCodecLocker.UnLock;
  end;

end;
//==============================================================================









END.
