{*******************************************************}
{                                                       }
{       SANPkgAsDllServer                               }
{       Серверная часть взаимодействия                  }
{       с "пакетом-как-DLL"                             }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPkgAsDllServer;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf;

TYPE
  ESANPkgAsDllServerError = class(Exception);

  TSANPkgAsDllsList = class;
  TSANPkgAsDllsServer = class;

  /// <summary>
  /// Серверная обертка для общения с "пакетом-как-DLL"
  /// </summary>
  TSANPkgAsDll = class(TInterfacedObject, IInterface, ISANPkgAsDll)
  private
    FServer : Pointer;// weak link
    FFileName : WideString;
    FHandle : HMODULE;
    FEntryIntf : ISANPkgAsDllEntry;

  protected
    // IInterface
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;

    // ISANPkgAsDll
    function GetFileName  : WideString;           safecall;
    function GetHandle    : HMODULE;              safecall;
    function GetEntry     : ISANPkgAsDllEntry;    safecall;
    function GetServer    : ISANPkgAsDllsServer;  safecall;

  public
    constructor Create(const APackageFileName : WideString; const AServer : ISANPkgAsDllsServer);
    procedure BeforeDestruction; override;
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property FileName  : WideString           read GetFileName;
    property Handle    : HMODULE              read GetHandle;
    property Entry     : ISANPkgAsDllEntry    read GetEntry;
    property Server    : ISANPkgAsDllsServer  read GetServer;
  end;

  /// <summary>
  /// Список серверных оберток для общения с "пакетом-как-DLL"
  /// </summary>
  TSANPkgAsDllsList = class(TInterfaceList, IInterfaceList, ISANPkgAsDlls)
  protected
    function GetCount : integer; safecall;
    function GetItems(const AIndex : integer) : ISANPkgAsDll; safecall;
    procedure Clear; safecall;
    function TryFindByFile(const AFileName : WideString; out APkgAsDll : ISANPkgAsDll) : WordBool; safecall;
    function FindByFile(const AFileName : WideString) : ISANPkgAsDll; safecall;
    function ExistsByFile(const AFileName : WideString) : WordBool; safecall;
    function RemoveByFile(const AFileName : WideString) : WordBool; safecall;
    function TryFindByGUID(const AVersionGUID : TGUID; out APkgAsDll : ISANPkgAsDll) : WordBool; safecall;
    function FindByGUID(const AVersionGUID : TGUID) : ISANPkgAsDll; safecall;
    function ExistsByGUID(const AVersionGUID : TGUID) : WordBool; safecall;
    function RemoveByGUID(const AVersionGUID : TGUID) : WordBool; safecall;

  public
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property Items[const AIndex : integer] : ISANPkgAsDll  read GetItems; default;
  end;

  /// <summary>
  /// Серверный объект, управляющий набором оберток для общения с "пакетом-как-DLL".
  /// Если задан владелец FOwner, то QueryInterface перенаправляется к нему
  /// (для неподдерживаемых интерфейсов)
  /// </summary>
  TSANPkgAsDllsServer = class(TInterfacedObject, IInterface, ISANPkgAsDllsServer)
  private
    FPackages : ISANPkgAsDlls;
    FOwnerIntf : Pointer;// weak link
    FDllEntryPointName : WideString;
    FEvents : ISANPkgAsDllsServerEvents;

  protected
    function GetOwnerIntf: IInterface; virtual;

    procedure DoDuplicatePackageGUID(const AOldPackage, ANewPackage : ISANPkgAsDll;
      var AAction : TSANPkgAsDllDuplicateGUIDAction);

    procedure ValidatePackageGUID(const ANewPackage : ISANPkgAsDll; out AContinue : boolean);

    // IInterface
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;

    // ISANPkgAsDllsServer
    function GetDllEntryPointName : WideString; virtual; safecall;
    function GetPackages : ISANPkgAsDlls; safecall;
    function GetEvents : ISANPkgAsDllsServerEvents; safecall;
    procedure SetEvents(const AEvents : ISANPkgAsDllsServerEvents); safecall;
    function LoadPackage(const AFileName : WideString) : ISANPkgAsDll; safecall;

  public
    constructor Create(const ADllEntryPointName : string); virtual;
    constructor CreateOwned(const ADllEntryPointName : string; const AOwnerIntf : IInterface);
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    property OwnerIntf : IInterface  read GetOwnerIntf;

    property DllEntryPointName : WideString  read GetDllEntryPointName;

    property Packages : ISANPkgAsDlls  read GetPackages;

    property Events : ISANPkgAsDllsServerEvents  read GetEvents  write SetEvents;
  end;

IMPLEMENTATION

uses
  ComObj,
  SANPkgAsDllMessages;

//==============================================================================
// TSANPkgAsDll
// protected
function TSANPkgAsDll.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  Result := inherited;

  if (Result = E_NOINTERFACE) and Assigned(FEntryIntf) then
    Result := FEntryIntf.QueryInterface(IID, Obj);
end;

function TSANPkgAsDll.GetEntry: ISANPkgAsDllEntry;
begin
  Result := FEntryIntf;
end;

function TSANPkgAsDll.GetFileName: WideString;
begin
  Result := FFileName;
end;

function TSANPkgAsDll.GetHandle: HMODULE;
begin
  Result := FHandle;
end;

function TSANPkgAsDll.GetServer: ISANPkgAsDllsServer;
begin
  Result := ISANPkgAsDllsServer(FServer);
end;

// public
constructor TSANPkgAsDll.Create(const APackageFileName: WideString; const AServer : ISANPkgAsDllsServer);
var
  vEntryPoint : TSANPkgAsDllExportEntryPointAltProc;
  HR: HResult;
begin
  inherited Create;

  FFileName := APackageFileName;
  FServer   := Pointer(AServer);

  FHandle := SafeLoadLibrary(APackageFileName);
  if FHandle = 0 then
    RaiseLastOSError;

  @vEntryPoint := GetProcAddress(FHandle, PAnsiChar(AnsiString(AServer.DllEntryPointName)));
  if not Assigned(vEntryPoint) then
    RaiseLastOSError;

  try
    HR := vEntryPoint(AServer, FEntryIntf);
  except
    HR := HResultFromWin32(ERROR_DLL_INIT_FAILED);
  end;


  if Failed(HR) then
  begin
    FEntryIntf := nil;
    if Assigned(SafeCallErrorProc) then
      SafeCallErrorProc(HR, Pointer(-1));  // loses error address
    RunError(ORD(reSafeCallError));
  end;

  if not Assigned(FEntryIntf) then
    raise ESANPkgAsDllServerError.CreateResFmt(@sServer_NotGetEntryInterfaceFmtError, [APackageFileName]);
end;

procedure TSANPkgAsDll.BeforeDestruction;
begin
  if Assigned(FEntryIntf) then
    FEntryIntf.Done(Server);

  inherited;
end;

destructor TSANPkgAsDll.Destroy;
begin
  FEntryIntf := nil;

  FreeLibrary(FHandle);

  inherited;
end;

function TSANPkgAsDll.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANPkgAsDll,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANPkgAsDllsList
function TSANPkgAsDllsList.GetCount: integer;
begin
  Result := inherited Count;
end;

function TSANPkgAsDllsList.GetItems(const AIndex: integer): ISANPkgAsDll;
begin
  Result := (inherited Items[AIndex]) as ISANPkgAsDll;
end;

procedure TSANPkgAsDllsList.Clear;
begin
  inherited Clear;
end;

function TSANPkgAsDllsList.TryFindByFile(const AFileName: WideString;
  out APkgAsDll: ISANPkgAsDll): WordBool;
var
  i : integer;
  vItem : ISANPkgAsDll;
begin
  APkgAsDll  := nil;
  Result     := False;

  for i := 0 to Count - 1 do
  begin
    vItem := Items[i];

    if not WideSameText(AFileName, vItem.FileName) then
      Continue;

    APkgAsDll := vItem;
    Exit(True);
  end;

end;

function TSANPkgAsDllsList.FindByFile(const AFileName: WideString): ISANPkgAsDll;
begin
  if not TryFindByFile(AFileName, Result) then
    raise ESANPkgAsDllServerError.CreateResFmt
    (
      @sServer_PackageNotFoundByFileNameFmtError,
      [AFileName]
    );
end;

function TSANPkgAsDllsList.ExistsByFile(const AFileName: WideString): WordBool;
var
  vDummy : ISANPkgAsDll;
begin
  Result := TryFindByFile(AFileName, vDummy);
end;

function TSANPkgAsDllsList.RemoveByFile(const AFileName: WideString): WordBool;
var
  i : integer;
  vItem : ISANPkgAsDll;
begin
  Result := False;

  for i := 0 to Count - 1 do
    if WideSameText(AFileName, Items[i].FileName) then
    begin
      Delete(i);
      Exit(True);
    end;
end;

function TSANPkgAsDllsList.TryFindByGUID(const AVersionGUID: TGUID;
  out APkgAsDll: ISANPkgAsDll): WordBool;
var
  i : integer;
  vItem : ISANVersionInfo;
begin
  APkgAsDll  := nil;
  Result     := False;

  for i := 0 to Count - 1 do
    if Supports(inherited Items[i], ISANVersionInfo, vItem)
    and (vItem.GUID = AVersionGUID)
    then
    begin
      APkgAsDll := vItem as ISANPkgAsDll;
      Exit(True);
    end;

end;

function TSANPkgAsDllsList.FindByGUID(const AVersionGUID: TGUID): ISANPkgAsDll;
begin
  if not TryFindByGUID(AVersionGUID, Result) then
    raise ESANPkgAsDllServerError.CreateResFmt
    (
      @sServer_PackageNotFoundByVersionGUIDFmtError,
      [GUIDToString(AVersionGUID)]
    );
end;

function TSANPkgAsDllsList.ExistsByGUID(const AVersionGUID: TGUID): WordBool;
var
  vDummy : ISANPkgAsDll;
begin
  Result := TryFindByGUID(AVersionGUID, vDummy);
end;

function TSANPkgAsDllsList.RemoveByGUID(const AVersionGUID: TGUID): WordBool;
var
  i : integer;
  vItem : ISANVersionInfo;
begin
  Result := False;

  for i := 0 to Count - 1 do
    if Supports(inherited Items[i], ISANVersionInfo, vItem)
    and (vItem.GUID = AVersionGUID)
    then
    begin
      Delete(i);
      Exit(True);
    end;
end;

// public
function TSANPkgAsDllsList.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANPkgAsDlls,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================

//==============================================================================
// TSANPkgAsDllsServer
// protected
function TSANPkgAsDllsServer.GetOwnerIntf: IInterface;
begin
  Result := IInterface(FOwnerIntf);
end;

procedure TSANPkgAsDllsServer.DoDuplicatePackageGUID(const AOldPackage,
  ANewPackage: ISANPkgAsDll; var AAction: TSANPkgAsDllDuplicateGUIDAction);
begin
  if Assigned(FEvents) then
    FEvents.PackageGUIDsDuplicated(Self, AOldPackage, ANewPackage, AAction);
end;

procedure TSANPkgAsDllsServer.ValidatePackageGUID(const ANewPackage : ISANPkgAsDll;
  out AContinue : boolean);
var
  vVrsInfo : ISANVersionInfo;
  vOldPackage : ISANPkgAsDll;
  vAction : TSANPkgAsDllDuplicateGUIDAction;
begin
  AContinue := True;

  if not
  (
    Supports(ANewPackage, ISANVersionInfo, vVrsInfo)
    and
    FPackages.TryFindByGUID(vVrsInfo.GUID, vOldPackage)
  )
  then
    Exit;

  vAction := dgaError;

  DoDuplicatePackageGUID(vOldPackage, ANewPackage, vAction);

  case vAction of

    dgaError:
      raise ESANPkgAsDllServerError.CreateResFmt
      (
        @sServer_DuplicatePackageVersionGUIDFmtError,
        [ANewPackage.FileName, vVrsInfo.Caption, GUIDToString(vVrsInfo.GUID)]
      );

    dgaReplace:
      Assert(FPackages.RemoveByGUID(vVrsInfo.GUID));

    dgaIgnore:
      AContinue := False;
  end;

end;

function TSANPkgAsDllsServer.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  Result := inherited QueryInterface(IID, Obj);

  if (Result = E_NOINTERFACE) and Assigned(FOwnerIntf) then
    if Supports(OwnerIntf, IID, Obj) then
      Result := S_OK;
end;

function TSANPkgAsDllsServer.GetDllEntryPointName: WideString;
begin
  Result := FDllEntryPointName;
end;

function TSANPkgAsDllsServer.GetPackages: ISANPkgAsDlls;
begin
  Result := FPackages;
end;

function TSANPkgAsDllsServer.GetEvents: ISANPkgAsDllsServerEvents;
begin
  Result := FEvents;
end;

procedure TSANPkgAsDllsServer.SetEvents(const AEvents: ISANPkgAsDllsServerEvents);
begin
  FEvents := AEvents;
end;

function TSANPkgAsDllsServer.LoadPackage(const AFileName: WideString): ISANPkgAsDll;
var
  vPkgAsDll : TSANPkgAsDll;
  vContinue : boolean;
begin

  if FPackages.ExistsByFile(AFileName) then
    raise ESANPkgAsDllServerError.CreateResFmt
    (
      @sServer_DuplicatePackageFileNameFmtError,
      [AFileName]
    );

  vPkgAsDll := TSANPkgAsDll.Create(AFileName, Self);
  Result := vPkgAsDll;

  ValidatePackageGUID(Result, vContinue);

  if not vContinue then
    Exit;

  vPkgAsDll.Entry.Init(Self);

  (FPackages as IInterfaceList).Add(Result);
end;

// public
constructor TSANPkgAsDllsServer.Create(const ADllEntryPointName : string);
begin
  inherited Create;

  FDllEntryPointName := ADllEntryPointName;

  FPackages := TSANPkgAsDllsList.Create;
end;

constructor TSANPkgAsDllsServer.CreateOwned(const ADllEntryPointName : string; const AOwnerIntf : IInterface);
begin
  FOwnerIntf := Pointer(AOwnerIntf);

  Create(ADllEntryPointName);
end;

destructor TSANPkgAsDllsServer.Destroy;
begin
  FPackages := nil;

  inherited;
end;

function TSANPkgAsDllsServer.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    ISANPkgAsDllsServer,
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;
//==============================================================================


END.
