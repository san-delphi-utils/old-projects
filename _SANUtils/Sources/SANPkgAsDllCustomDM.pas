{*******************************************************}
{                                                       }
{       SANPkgAsDllCustomDM                             }
{       модуль данных, поддерживающий                   }
{       ISANVersionInfo.                                }
{       Используется для визуального проектирования     }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPkgAsDllCustomDM;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf;

TYPE
  EVersionInitDoneDataModuleError = class(Exception);

  /// <summary>
  /// Прототип обработчика инициализации
  /// </summary>
  TSANIntfNotifyEvent = procedure (Sender : TObject; const AParams: IInterface) of object;

  /// <summary>
  /// Часть версии
  /// </summary>
  TSANVersionPart = LongWord;
  /// <summary>
  /// Объект свойства версии
  /// </summary>
  TSANVersionPropObj = class(TPersistent)
  private
    FOwner : TPersistent;
    FMinor: TSANVersionPart;
    FRelease: TSANVersionPart;
    FMajor: TSANVersionPart;
    FBuild: TSANVersionPart;

    procedure SetBuild(const Value: TSANVersionPart);
    procedure SetMajor(const Value: TSANVersionPart);
    procedure SetMinor(const Value: TSANVersionPart);
    procedure SetRelease(const Value: TSANVersionPart);

    function GetVersion: LongWord;
    procedure SetVersion(Value: LongWord);

  protected
    function GetOwner : TPersistent; override;
    procedure AssignTo(Dest : TPersistent); override;
    procedure ValidatePart(const Value: TSANVersionPart);

  public
    constructor Create(const AOwner : TPersistent);

    property Version : LongWord  read GetVersion  write SetVersion;

  published
    property V1Major    : TSANVersionPart  read FMajor    write SetMajor;
    property V2Minor    : TSANVersionPart  read FMinor    write SetMinor;
    property V3Release  : TSANVersionPart  read FRelease  write SetRelease;
    property V4Build    : TSANVersionPart  read FBuild    write SetBuild;
  end;

  /// <summary>
  /// Модуль данных поддерщивающий св-ва версии, OnInit, OnDone.
  /// </summary>
  TSANVersionInitDoneDataModule = class(TDataModule, ISANVersionInfo)
  private
    FOnInit, FOnDone: TSANIntfNotifyEvent;
    FVersion: TSANVersionPropObj;
    FAuthor, FCaption, FDescription, FGUID, FURL: string;

    procedure SetVersion(const Value: TSANVersionPropObj);
    procedure SetGUID(const Value: string);

  protected
    procedure DoInit(const AParams: IInterface); dynamic;
    procedure DoDone(const AParams: IInterface); dynamic;

    // ISANVersionInfo
    function GetGUID: TGUID; safecall;
    function GetCaption: WideString; safecall;
    function GetDescription: WideString; safecall;
    function GetURL: WideString; safecall;
    function GetAuthor: WideString; safecall;
    function GetVersion: Longword; safecall;

    property GUID         : string    read FGUID         write SetGUID;
    property Caption      : string    read FCaption      write FCaption;
    property Description  : string    read FDescription  write FDescription;
    property URL          : string    read FURL          write FURL;
    property Author       : string    read FAuthor       write FAuthor;

    property Version      : TSANVersionPropObj  read FVersion      write SetVersion;

    property OnInit : TSANIntfNotifyEvent  read FOnInit  write FOnInit;
    property OnDone : TSANIntfNotifyEvent  read FOnDone  write FOnDone;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

  end;


IMPLEMENTATION

USES
  ComObj, ActiveX,
  SANPkgAsDllMessages;

//==============================================================================
// TSANVersionPropObj
// private
procedure TSANVersionPropObj.SetMajor(const Value: TSANVersionPart);
begin
  ValidatePart(Value);
  FMajor := Value;
end;

procedure TSANVersionPropObj.SetMinor(const Value: TSANVersionPart);
begin
  ValidatePart(Value);
  FMinor := Value;
end;

procedure TSANVersionPropObj.SetRelease(const Value: TSANVersionPart);
begin
  ValidatePart(Value);
  FRelease := Value;
end;

procedure TSANVersionPropObj.SetBuild(const Value: TSANVersionPart);
begin
  ValidatePart(Value);
  FBuild := Value;
end;

function TSANVersionPropObj.GetVersion: LongWord;
begin
  Result := FMajor    * $FFFFFF +
            FMinor    * $00FFFF +
            FRelease  * $0000FF +
            FBuild;
end;

procedure TSANVersionPropObj.SetVersion(Value: LongWord);

  procedure ExtractVerPart(var APart : LongWord; const ADiv : LongWord);
  begin
    APart := Value div ADiv;
    Value := Value mod ADiv;
  end;

begin
  ExtractVerPart(FMajor,   $FFFFFF);
  ExtractVerPart(FMinor,   $00FFFF);
  ExtractVerPart(FRelease, $0000FF);
  FBuild := Value;
end;

// protected
function TSANVersionPropObj.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

procedure TSANVersionPropObj.AssignTo(Dest: TPersistent);
begin
  if Dest is TSANVersionPropObj then
    (Dest as TSANVersionPropObj).Version := Self.Version
  else
    inherited;
end;

procedure TSANVersionPropObj.ValidatePart(const Value: TSANVersionPart);
begin
  if Value > 255 then
    raise EVersionInitDoneDataModuleError.CreateRes(@sCustomDM_VersionPartOwerflowError);
end;

// public
constructor TSANVersionPropObj.Create(const AOwner: TPersistent);
begin
  inherited Create;

  FOwner := AOwner;
end;
//==============================================================================

//==============================================================================
// TSANVersionInitDoneDataModule
// private
procedure TSANVersionInitDoneDataModule.SetVersion(const Value: TSANVersionPropObj);
begin
  FVersion.Assign(Value);
end;

procedure TSANVersionInitDoneDataModule.SetGUID(const Value: string);
var
  vGUID : TGUID;
begin

  if FGUID = Value then
    Exit;

  if (Value <> '') and (CLSIDFromString(PWideChar(WideString(Value)), vGUID) <> S_OK) then
    raise EVersionInitDoneDataModuleError.CreateResFmt(@sCustomDM_IncorrectGUIDFmtError, [Value]);

  FGUID := Value;
end;

// protected
procedure TSANVersionInitDoneDataModule.DoInit(const AParams: IInterface);
begin
  if Assigned(FOnInit) then
    FOnInit(Self, AParams);
end;

procedure TSANVersionInitDoneDataModule.DoDone(const AParams: IInterface);
begin
  if Assigned(FOnDone) then
    FOnDone(Self, AParams);
end;

function TSANVersionInitDoneDataModule.GetGUID: TGUID;
begin
  Result := StringToGUID(FGUID);
end;

function TSANVersionInitDoneDataModule.GetCaption: WideString;
begin
  Result := FCaption;
end;

function TSANVersionInitDoneDataModule.GetDescription: WideString;
begin
  Result := FDescription;
end;

function TSANVersionInitDoneDataModule.GetURL: WideString;
begin
  Result := FURL;
end;

function TSANVersionInitDoneDataModule.GetAuthor: WideString;
begin
  Result := FAuthor;
end;

function TSANVersionInitDoneDataModule.GetVersion: Longword;
begin
  Result := FVersion.Version;
end;

// public
constructor TSANVersionInitDoneDataModule.Create(AOwner: TComponent);
begin
  FVersion := TSANVersionPropObj.Create(Self);

  inherited;
end;

destructor TSANVersionInitDoneDataModule.Destroy;
begin
  FreeAndNil(FVersion);
  inherited;
end;
//==============================================================================

END.
