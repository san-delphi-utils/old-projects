// JCL_DEBUG_EXPERT_INSERTJDBG OFF
program MailSlotSingle;

uses
  Forms,
  SANProtocol,
  SANProtocolCodecBase,
  SANMailSlots,
  MSSingleTypes_u in 'MSSingleTypes_u.pas',
  frmMSSingleMain_u in 'frmMSSingleMain_u.pas' {frmMSSingleMain};

{$R *.res}

begin

  System.ReportMemoryLeaksOnShutdown := True;

  if TryConnectAsClient then
  begin
    ExitCode := 1;
    Exit;
  end;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMSSingleMain, frmMSSingleMain);
  Application.Run;
end.
