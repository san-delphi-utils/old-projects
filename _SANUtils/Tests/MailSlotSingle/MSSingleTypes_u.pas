UNIT MSSingleTypes_u;

INTERFACE

function TryConnectAsClient : boolean;

IMPLEMENTATION

USES
  SysUtils, Classes, Forms, StdCtrls, Dialogs,
  SANProtocol,
  SANProtocolCodecBase,
  SANMailSlots,
  SANVCLMisc;

const
  MailslotName  = '\\.\mailslot\MSSingle';
  EventName     = 'MSSingle_Command_Event';

type
//------------------------------------------------------------------------------
  PMSSDataRec = ^TMSSDataRec;
  TMSSDataRec = record
    /// <summary>
    /// Передатчик данных
    /// </summary>
    Transporter : TSANServerMailSlot;

    /// <summary>
    /// Соединение передачи данных
    /// </summary>
    Connection : Pointer;

    /// <summary>
    /// Пакет данных
    /// </summary>
    DataPackage : TStrings;
  end;
//------------------------------------------------------------------------------

var
  vIPC : ISANProtocolCodec;
  vSrvMS    : TSANServerMailSlot        = nil;
  vClntMS   : TSANClientMailSlot        = nil;
  vRouter   : TSANProtocolQueuedRouter  = nil;

procedure RouterCommand(const Self, ASender : TObject; const ADataRec : PMSSDataRec);
begin
  (Application.MainForm.FindComponent('mmMain') as TMemo).Lines.AddStrings(ADataRec^.DataPackage);
  AppGoToForeground;
end;

function TryConnectAsClient : boolean;
var
  i : integer;
  vPkg : TStrings;
begin
  Result := Assigned(vClntMS) and not Assigned(vSrvMS);

  if not Result then
    Exit;

  vIPC.NewDataPackage(vPkg);
  try

    for i := 0 to ParamCount do
      vPkg.Add(ParamStr(i));

    SANProtocolSend(vIPC, vClntMS, vPkg);
  finally
    SANProtocolCodecFreeAndNilDataPackage(vIPC, vPkg);
  end;
end;

procedure InitMailSlots;
begin

  if TSANServerMailSlot.TryCreate(vSrvMS, MailslotName, EventName) then
    begin
      vRouter := TSANProtocolQueuedRouter.Create;
      vRouter.ChangeProtocolCodec(vIPC);
      @vRouter.OnUnHandledCommand := @RouterCommand;

      vSrvMS.ProtocolRouter := vRouter;
    end
  else
    vClntMS := TSANClientMailSlot.Create(MailslotName, EventName);

end;

INITIALIZATION
  vIPC := TSANProtocolCodecStrings.Create;

  InitMailSlots;


FINALIZATION
  FreeAndNil(vClntMS);
  FreeAndNil(vSrvMS);
  FreeAndNil(vRouter);

  vIPC := nil;
END.
