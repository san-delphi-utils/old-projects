// JCL_DEBUG_EXPERT_INSERTJDBG OFF
program DynClasses;

uses
  Forms,
  frmDynClassesMain_u in 'frmDynClassesMain_u.pas' {frmDynClassesMain};

{$R *.res}

begin
  System.ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmDynClassesMain, frmDynClassesMain);
  Application.Run;
end.
