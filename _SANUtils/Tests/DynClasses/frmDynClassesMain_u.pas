unit frmDynClassesMain_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,
  SANDynamicClasses;

type
  ICIntf = interface(IInterface)
    ['{18EA0033-FBE9-4037-864F-8055818B4F54}']
    function GetStr : string;
  end;

  TCls1Class = class of TCls1;
  TCls1 = class(TInterfacedPersistent, ICIntf)
  function GetStr : string; virtual;
  end;

  TCls2 = class(TCls1)
  function GetStr : string; override;
  end;

  TC1Class = class of TCls1;
  TC1 = class(TInterfacedPersistent, ICIntf)
  function GetStr : string; virtual;
  end;

  TC2 = class(TC1)
  function GetStr : string; override;
  end;

  TC3 = class(TC2)
  function GetStr : string; override;
  end;

  TfrmDynClassesMain = class(TForm)
    btnShow: TButton;
    btnReg: TButton;
    btnUnReg: TButton;
    gbSimple: TGroupBox;
    gbList: TGroupBox;
    gbCls: TGroupBox;
    btnClsAdd: TButton;
    btnClsRemove: TButton;
    btnClsReg: TButton;
    btnClsUnReg: TButton;
    gbC: TGroupBox;
    btnCAdd: TButton;
    btnCRemove: TButton;
    btnCReg: TButton;
    btnCUnReg: TButton;
    tvList: TTreeView;
    procedure btnShowClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnRegClick(Sender: TObject);
    procedure btnUnRegClick(Sender: TObject);
    procedure btnClsAddClick(Sender: TObject);
    procedure btnClsRemoveClick(Sender: TObject);
    procedure btnClsRegClick(Sender: TObject);
    procedure btnClsUnRegClick(Sender: TObject);
    procedure btnCAddClick(Sender: TObject);
    procedure btnCRemoveClick(Sender: TObject);
    procedure btnCRegClick(Sender: TObject);
    procedure btnCUnRegClick(Sender: TObject);
  private
    FDCI : TSANDynamicClsInfo;
    FDCIL : TSANDynamicClsInfoList;

  protected
    procedure DCILEnumItemDynClss(const AClass : TClass; var AParam : Pointer);
    procedure DCILEnumInfos(const ADynClassInfo : TSANDynamicClsInfo; var AParam : Pointer);
    procedure FillTree;

  public
    { Public declarations }
  end;

var
  frmDynClassesMain: TfrmDynClassesMain;

implementation

uses
  SANInfoXMLTree;

{$R *.dfm}

{ TCls1 }

function TCls1.GetStr: string;
begin
  Result := '1';
end;

{ TCls2 }

function TCls2.GetStr: string;
begin
  Result := '2';
end;

{ TC1 }

function TC1.GetStr: string;
begin
  Result := '3';
end;

{ TC2 }

function TC2.GetStr: string;
begin
  Result := '4';
end;

{ TC3 }

function TC3.GetStr: string;
begin
  Result := '5';
end;


// protected
procedure TfrmDynClassesMain.DCILEnumItemDynClss(const AClass: TClass;
  var AParam: Pointer);
begin
  tvList.Items.AddChildObject( TTreeNode(AParam), AClass.ClassName, AClass);
end;

procedure TfrmDynClassesMain.DCILEnumInfos(const ADynClassInfo: TSANDynamicClsInfo;
  var AParam: Pointer);
var
  vNode : TTreeNode;
  vIntf : ICIntf;
  vObj : TObject;
  S : string;
begin
  vNode := tvList.Items.AddObject( TTreeNode(AParam), ADynClassInfo.BaseClass.ClassName, ADynClassInfo);

  ADynClassInfo.EnumDynClasses(DCILEnumItemDynClss, vNode);

  vObj := ADynClassInfo.TopClass.Create;
  try

    if vObj.GetInterface(ICIntf, vIntf) then
      S := vIntf.GetStr

    else
      S := '-'

  finally
    FreeAndNil(vObj)
  end;

  vNode.Text := vNode.Text + ' {' + S + '}';
end;

procedure TfrmDynClassesMain.FillTree;
begin

  with tvList, Items do
  begin

    BeginUpdate;
    try
      Clear;

      Self.FDCIL.EnumDynClsInfos(DCILEnumInfos, nil);

    finally
      EndUpdate;
    end;

    FullExpand;
  end;

end;

// public
procedure TfrmDynClassesMain.FormCreate(Sender: TObject);
begin
  FDCI := TSANDynamicClsInfo.Create(TCls1);

  FDCIL := TSANDynamicClsInfoList.Create;
end;

procedure TfrmDynClassesMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FDCI);
  FreeAndNil(FDCIL);
end;

procedure TfrmDynClassesMain.btnRegClick(Sender: TObject);
begin
  FDCI.TopClass := TCls2;
end;

procedure TfrmDynClassesMain.btnUnRegClick(Sender: TObject);
begin
  FDCI.RemoveDynClass(TCls2)
end;

procedure TfrmDynClassesMain.btnShowClick(Sender: TObject);
var
  C : TCls1Class;
  O : TCls1;
begin
  C := TCls1Class(FDCI.TopClass);
  O := C.Create;
  try
    ShowMessage(O.GetStr);
  finally
    FreeAndNil(O);
  end;

end;

procedure TfrmDynClassesMain.btnClsAddClick(Sender: TObject);
begin
  FDCIL.Add(TCls1);
  FillTree;
end;

procedure TfrmDynClassesMain.btnClsRemoveClick(Sender: TObject);
begin
  FDCIL.RemoveByClass(TCls1);
  FillTree;
end;

procedure TfrmDynClassesMain.btnClsRegClick(Sender: TObject);
begin
  FDCIL[TCls1].TopClass := TCls2;
  FillTree;
end;

procedure TfrmDynClassesMain.btnClsUnRegClick(Sender: TObject);
begin
  FDCIL[TCls1].RemoveDynClass(TCls2);
  FillTree;
end;

procedure TfrmDynClassesMain.btnCAddClick(Sender: TObject);
begin
  FDCIL.Add(TC1);
  FillTree;
end;

procedure TfrmDynClassesMain.btnCRemoveClick(Sender: TObject);
begin
  FDCIL.RemoveByClass(TC1);
  FillTree;
end;

procedure TfrmDynClassesMain.btnCRegClick(Sender: TObject);
begin
  FDCIL[TC1].TopClass := TC2;
  FDCIL[TC1].TopClass := TC3;
  FillTree;
end;

procedure TfrmDynClassesMain.btnCUnRegClick(Sender: TObject);
begin
  FDCIL[TC1].RemoveDynClass(TC3);
  FDCIL[TC1].RemoveDynClass(TC2);
  FillTree;
end;

END.
