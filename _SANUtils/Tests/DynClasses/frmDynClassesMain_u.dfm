object frmDynClassesMain: TfrmDynClassesMain
  Left = 0
  Top = 0
  Caption = 'frmDynClassesMain'
  ClientHeight = 255
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object gbSimple: TGroupBox
    Left = 8
    Top = 8
    Width = 89
    Height = 110
    Caption = 'gbSimple'
    TabOrder = 0
    object btnReg: TButton
      Left = 7
      Top = 46
      Width = 75
      Height = 25
      Caption = 'btnReg'
      TabOrder = 0
      OnClick = btnRegClick
    end
    object btnShow: TButton
      Left = 7
      Top = 15
      Width = 75
      Height = 25
      Caption = 'btnShow'
      TabOrder = 1
      OnClick = btnShowClick
    end
    object btnUnReg: TButton
      Left = 7
      Top = 77
      Width = 75
      Height = 25
      Caption = 'btnUnReg'
      TabOrder = 2
      OnClick = btnUnRegClick
    end
  end
  object gbList: TGroupBox
    Left = 128
    Top = 8
    Width = 337
    Height = 227
    Caption = 'gbList'
    TabOrder = 1
    object gbCls: TGroupBox
      Left = 144
      Top = 15
      Width = 185
      Height = 82
      Caption = 'gbCls'
      TabOrder = 0
      object btnClsAdd: TButton
        Left = 7
        Top = 16
        Width = 75
        Height = 25
        Caption = 'btnClsAdd'
        TabOrder = 0
        OnClick = btnClsAddClick
      end
      object btnClsRemove: TButton
        Left = 88
        Top = 16
        Width = 89
        Height = 25
        Caption = 'btnClsRemove'
        TabOrder = 1
        OnClick = btnClsRemoveClick
      end
      object btnClsReg: TButton
        Left = 7
        Top = 47
        Width = 75
        Height = 25
        Caption = 'btnClsReg'
        TabOrder = 2
        OnClick = btnClsRegClick
      end
      object btnClsUnReg: TButton
        Left = 88
        Top = 47
        Width = 89
        Height = 25
        Caption = 'btnClsUnReg'
        TabOrder = 3
        OnClick = btnClsUnRegClick
      end
    end
    object gbC: TGroupBox
      Left = 149
      Top = 103
      Width = 185
      Height = 82
      Caption = 'gbCls'
      TabOrder = 1
      object btnCAdd: TButton
        Left = 7
        Top = 16
        Width = 75
        Height = 25
        Caption = 'btnCAdd'
        TabOrder = 0
        OnClick = btnCAddClick
      end
      object btnCRemove: TButton
        Left = 88
        Top = 16
        Width = 89
        Height = 25
        Caption = 'btnCRemove'
        TabOrder = 1
        OnClick = btnCRemoveClick
      end
      object btnCReg: TButton
        Left = 7
        Top = 47
        Width = 75
        Height = 25
        Caption = 'btnCReg'
        TabOrder = 2
        OnClick = btnCRegClick
      end
      object btnCUnReg: TButton
        Left = 88
        Top = 47
        Width = 89
        Height = 25
        Caption = 'btnCUnReg'
        TabOrder = 3
        OnClick = btnCUnRegClick
      end
    end
    object tvList: TTreeView
      Left = 6
      Top = 15
      Width = 132
      Height = 194
      Indent = 19
      TabOrder = 2
    end
  end
end
