object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'frmMain'
  ClientHeight = 305
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object btnSLUFindEx: TBitBtn
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'btnSLUFindEx'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 0
    OnClick = btnSLUFindExClick
  end
  object btnCalcSize: TBitBtn
    Left = 8
    Top = 39
    Width = 75
    Height = 25
    Caption = 'btnCalcSize'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 1
    OnClick = btnCalcSizeClick
  end
  object btnLogs: TBitBtn
    Left = 8
    Top = 70
    Width = 75
    Height = 25
    Caption = 'btnLogs'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 2
    OnClick = btnLogsClick
  end
  object mmText: TMemo
    Left = 112
    Top = 8
    Width = 393
    Height = 89
    Lines.Strings = (
      '123'
      '456'
      '789')
    TabOrder = 3
  end
  object seVIndent: TSpinEdit
    Left = 16
    Top = 103
    Width = 57
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 2
  end
  object seHIndent: TSpinEdit
    Left = 79
    Top = 103
    Width = 57
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 1
  end
  object seSubIndent: TSpinEdit
    Left = 142
    Top = 103
    Width = 57
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 6
    Value = 2
  end
  object seLeftIndent: TSpinEdit
    Left = 205
    Top = 103
    Width = 57
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 7
    Value = 2
  end
  object btnFiles: TBitBtn
    Left = 8
    Top = 131
    Width = 75
    Height = 25
    Caption = 'btnFiles'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 8
    OnClick = btnFilesClick
  end
  object btn1: TBitBtn
    Left = 8
    Top = 162
    Width = 75
    Height = 25
    Caption = 'btn1'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 9
    OnClick = btn1Click
  end
  object btn2: TBitBtn
    Left = 89
    Top = 162
    Width = 75
    Height = 25
    Caption = 'btn2'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 10
    OnClick = btn2Click
  end
  object btn3: TBitBtn
    Left = 170
    Top = 162
    Width = 75
    Height = 25
    Caption = 'btn3'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 11
    OnClick = btn3Click
  end
  object btnRUS: TButton
    Left = 8
    Top = 193
    Width = 75
    Height = 25
    Caption = 'btnRUS'
    TabOrder = 12
    OnClick = btnRUSClick
  end
  object btn4: TButton
    Left = 8
    Top = 224
    Width = 75
    Height = 25
    Caption = 'btn4'
    TabOrder = 13
    OnClick = btn4Click
  end
  object btn5: TButton
    Left = 8
    Top = 255
    Width = 75
    Height = 25
    Caption = 'btn5'
    TabOrder = 14
    OnClick = btn5Click
  end
  object tmrAF: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tmrAFTimer
    Left = 352
    Top = 160
  end
end
