UNIT frmMain_u;

INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Spin, ExtCtrls, TypInfo,
  SANMisc,
  SANRus,
  SANListUtils,
  SANCalcSize,
//  SANLogs, {SANLogsStorageFiles,} SANLogsTable, SANLogsTree,
  SANWinUtils,
  SANProtocol, SANProtocolCodecBase,
  SANVCLMisc;

type
  TfrmMain = class(TForm)
    btnSLUFindEx: TBitBtn;
    btnCalcSize: TBitBtn;
    btnLogs: TBitBtn;
    mmText: TMemo;
    seVIndent: TSpinEdit;
    seHIndent: TSpinEdit;
    seSubIndent: TSpinEdit;
    seLeftIndent: TSpinEdit;
    btnFiles: TBitBtn;
    tmrAF: TTimer;
    btn1: TBitBtn;
    btn2: TBitBtn;
    btn3: TBitBtn;
    btnRUS: TButton;
    btn4: TButton;
    btn5: TButton;
    procedure btnSLUFindExClick(Sender: TObject);
    procedure btnCalcSizeClick(Sender: TObject);
    procedure btnLogsClick(Sender: TObject);
    procedure btnFilesClick(Sender: TObject);
    procedure tmrAFTimer(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnRUSClick(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

IMPLEMENTATION

{$R *.dfm}

procedure TfrmMain.btnSLUFindExClick(Sender: TObject);
var
  vList : TList;
  vItem : Pointer;
  i : integer;
begin
  vList := TList.Create;
  try

    vList.Add( Pointer(3) );
    vList.Add( Pointer(5) );
    vList.Add( Pointer(7) );
    vList.Add( Pointer(9) );
    vList.Add( Pointer(11) );

//    SANListFind(vItem, vList, SANListFF_EqualPointers, SANListParams(Pointer(9), -1, @i));

    ShowMessageFmt('%d %d', [Integer(vItem), i]);

  finally
    FreeAndNil(vList)
  end;

end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
//  LogsManager.StoragesDLL_Add('D:\DELPHI\ActiveProjects\LogStoragesDLL\bin\LSFiles.dll')
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
//  LogsManager.StoragesDLL_Clear;
end;

procedure TfrmMain.btnCalcSizeClick(Sender: TObject);
var
  S : string;
  W : WideString;
  A : AnsiString;
  U : UnicodeString;
begin
  S := 'ABCD';
  W := 'ABCD';
  A := 'ABCD';
  U := 'ABCD';

  ShowMessageFmt('%d %d %d %d ', [SANCalcSizeString(S), SANCalcSizeWideString(W), SANCalcSizeAnsiString(A), SANCalcSizeUnicodeString(U)]);
end;

procedure TfrmMain.btnLogsClick(Sender: TObject);
//
//  procedure TableWrk(const ALog : ICustomLog);
//  var
//    T : TSLTable;
//  begin
//
//    T := TSLTable.Create(ALog);
//    try
//
//      T.TableBeginWrite(lmkWarning, 'Table');
//      try
//
//        T.InitColsByWidth([10, 20, 30]);
//
//        T.Write_HrzLine(False);
//
//        T.Write_Header(taCenter, ['Таблица', 'тест1']);
//        T.Write_HrzLine;
//
//        mmText.Text := 'Таблица'#13#10'тест2';
//        T.Write_Header(taCenter, mmText.Lines);
//        T.Write_HrzLine;
//
//        T.Write_Header(taCenter, 'Таблица'#13#10'тест3');
//        T.Write_HrzLine;
//
//
//        T.Write_SepStr('A|B|C|B');
//
//        T.Write_HrzLine;
//
//      finally
//        T.TableEndWrite;
//      end;
//
//    finally
//      FreeAndNil(T);
//    end;
//
//  end;
//
//var
//  L : ILog;
//  R : TSLTree;
//
begin
//  LogsManager.AttachLog(L, 'l1', ExtractFilePath(Application.ExeName) + 'TestLog.txt');
//
//  L.Write(lmkInfo, 'ABCD');
//
//  R := TSLTree.Create(L);
//  try
//
//    R.VIndent := seVIndent.Value;
//    R.HIndent := seHIndent.Value;
//    R.SubIndent := seSubIndent.Value;
//    R.LeftIndent := seLeftIndent.Value;
//
//    R.TreeBeginWrite(lmkError, 'Tree');
//    try
//
////      R.SimpleNode('123', True);
////      WriteStr('123');
//
//      R.NewNode(False, 0);
//      try
//
//        R.WriteStr('zxcv');
//
//        R.NewNode(True, 1);
//        try
//          R.WriteStr('hjkl');
//        finally
//          R.EndNode
//        end;
//
//        R.NewNode(True, 1);
//        try
//          R.WriteStr('8975');
//
//          R.NewNode(False, 2);
//          try
//
//            TableWrk(R);
//
//          finally
//            R.EndNode
//          end;
//
//        finally
//          R.EndNode
//        end;
//
//        R.SimpleNode('%^#', False, 1);
//
//      finally
//        R.EndNode
//      end;
//
//    finally
//      R.TreeEndWrite;
//    end;
//
//  finally
//    FreeAndNil(R);
//  end;
//
end;

procedure TfrmMain.btnFilesClick(Sender: TObject);
//var
//  i : integer;
//  S, vSub : string;
begin
//  S := 'asdfghjksfzcvxxbwdzxazsfxcvbesxzcwxs';
//
//  i := 1;
//  while ReadSubStr(S, i, vSub, 'x') do
//    mmText.Lines.Add(vSub)


//  ComPortsStrings(mmText.Lines);

//  DirCopy('D:\DELPHI\ActiveProjects\_SANUtils\BPL', 'E:\test');
//  CopyFile('D:\DELPHI\ActiveProjects\_SANUtils\BPL', 'E:\test', False);
//  raise Exception.Create( SysErrorMessage(GetLastError));



//  ShowMessage( StrArrToStr(['A', 'B', 'C'], '', '+') );

//  mmText.Lines.Add( IntToStr(

//  raise Exception.Create( SysErrorMessage());
//SHFODelete(0, 'E:\Temp\фыва');
//  SHFOCopy(Handle, ['E:\Temp\фыва\*.*'], 'E:\Temp\фыва+');
end;

procedure TfrmMain.tmrAFTimer(Sender: TObject);
begin
  AppGoToForeground;
end;

{
  TSANProtocolDecodingHeaderItem = record
    /// <summary>
    /// Куда скопировать
    /// </summary>
    Dest : Pointer;
    /// <summary>
    /// Сколько байт
    /// </summary>
    Size : integer;
  end;
  //----------------------------------------------------------------------------
  TSANProtocolDecodingHeaderItems = array [0..1024] of TSANProtocolDecodingHeaderItem;
  PSANProtocolDecodingHeaderItems = ^TSANProtocolDecodingHeaderItems;
  //----------------------------------------------------------------------------
  /// <summary>
  /// Заголовок управляющий копированием при декодировании
  /// </summary>
  TSANProtocolDecodingHeader = record
    /// <summary>
    /// Число элементов
    /// </summary>
    Count : integer;
    /// <summary>
    /// Индекс текущего элемента
    /// </summary>
    Index : integer;
    /// <summary>
    /// Смещение для текущего элемента
    /// </summary>
    Offset : integer;
    /// <summary>
    /// Остаток для текущего элемента
    /// </summary>
    Left : integer;

    Items : PSANProtocolDecodingHeaderItems;
  end;

}


procedure TfrmMain.btn1Click(Sender: TObject);
var
  vPC : ISANProtocolCodec;
  vDP : TSANProtocolDecodingRec;
  vBuffer : AnsiString;
  S : string;
  B : boolean;
begin
  S := 'Command1';
  vBuffer := 'GRPP';

  vPC := TSANProtocolCodecCommandName.Create;

  vPC.EncodeDataPackage(@S, vBuffer);

  vDP.Buffer := vBuffer;

  vDP.BeginDecoding(vPC);
  try
    vDP.ResetBuffer;

     B := vPC.DecodeDataPackage(vDP);

    ShowMessage( BoolToStr(B, True) + #13#10 + PString(vDP.DataPackage)^ + #13#10 + String(vDP.Buffer) );

    vPC.FreeDataPackage(vDP.DataPackage);
  finally
    vDP.EndDecoding(vPC);
  end;
end;

procedure TfrmMain.btn2Click(Sender: TObject);
const
  STEP_SIZE = 1;
var
  vPC : ISANProtocolCodec;
  vDP : TSANProtocolDecodingRec;
  vBuffer : AnsiString;
  S : string;
  B : boolean;
  i : integer;
begin
  S := 'Command2';
  vBuffer := 'GRPP';

  vPC := TSANProtocolCodecCommandName.Create;

  vPC.EncodeDataPackage(@S, vBuffer);

  vDP.BeginDecoding(vPC);
  try

    i := 1;
    repeat
      vDP.Buffer := Copy(vBuffer, i, STEP_SIZE);
      Inc(i, STEP_SIZE);

      vDP.ResetBuffer;

      B := vPC.DecodeDataPackage(vDP);
    until B;

    ShowMessage( BoolToStr(B, True) + #13#10 + PString(vDP.DataPackage)^ + #13#10 + String(vDP.Buffer) );

    vPC.FreeDataPackage(vDP.DataPackage);
  finally
    vDP.EndDecoding(vPC);
  end;
end;


procedure TfrmMain.btn3Click(Sender: TObject);
const
  STEP_SIZE = 1;
var
  vPC : ISANProtocolCodec;
  vDP : TSANProtocolDecodingRec;
  vBuffer : AnsiString;
  B : boolean;
  i : integer;
begin
  vBuffer := 'GRPP';

  vPC := TSANProtocolCodecStrings.Create;

  vPC.EncodeDataPackage(mmText.Lines, vBuffer);

  vDP.BeginDecoding(vPC);
  try

    i := 1;
    repeat
      vDP.Buffer := Copy(vBuffer, i, STEP_SIZE);
      Inc(i, STEP_SIZE);

      vDP.ResetBuffer;

      B := vPC.DecodeDataPackage(vDP);
    until B;

    ShowMessage( BoolToStr(B, True));
    ShowMessage( TStrings(vDP.DataPackage).Text);
    ShowMessage( String(vDP.Buffer) );

    vPC.FreeDataPackage(vDP.DataPackage);
  finally
    vDP.EndDecoding(vPC);
  end;
end;

procedure TfrmMain.btnRUSClick(Sender: TObject);
begin
  ShowMessage
  (
    Format
    (
      NumSubstDecode
      (
        '%d человек{0||а|} иногда эффективнее %d танк{1|а|ов|ов}, если у {0|него|них|них} %d извилин{2|а|ы|} и %d гранат{3|а|ы|}!',
        [
          seVIndent.Value,
          seHIndent.Value,
          seSubIndent.Value,
          seLeftIndent.Value
        ]
      ),
      [
        seVIndent.Value,
        seHIndent.Value,
        seSubIndent.Value,
        seLeftIndent.Value
      ]
    )
  );
end;

procedure TfrmMain.btn4Click(Sender: TObject);
begin
  mmText.Text := StrShift(mmText.Text, 2);
end;

procedure TfrmMain.btn5Click(Sender: TObject);

  function BPLFileName : TFileName;
  var
    vFN: array[0..MAX_PATH - 1] of Char;
    l : Cardinal;
  begin
    l := GetModuleFileName(HInstance, vFN, MAX_PATH);
    if l = 0 then
      RaiseLastOSError;

    SetLength(Result, l);

    Move(vFN[0], Result[1], l * SizeOf(Char));
  end;

begin
  ShowMessage(BPLFileName);
end;

END.
