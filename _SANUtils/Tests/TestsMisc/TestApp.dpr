// JCL_DEBUG_EXPERT_INSERTJDBG OFF
program TestApp;

uses
  Forms,
  SANCancelSecondRun,
  frmMain_u in 'frmMain_u.pas' {frmMain};

{$R *.res}
var
  vMappingHandle : THandle;
  vIsSecondRun : boolean;
begin
  System.ReportMemoryLeaksOnShutdown := True;

  SANSecRunMappingCreate(Application.Handle, False, True, vMappingHandle, vIsSecondRun);
  try

    if vIsSecondRun then
      Exit;

    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmMain, frmMain);
  Application.Run;

  finally
    SANSecRunMappingFree(vMappingHandle);
  end;

end.
