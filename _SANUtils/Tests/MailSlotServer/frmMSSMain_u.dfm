object frmMSSMain: TfrmMSSMain
  Left = 0
  Top = 0
  Caption = 'frmMSSMain'
  ClientHeight = 477
  ClientWidth = 848
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object gbServer: TGroupBox
    Left = 8
    Top = 8
    Width = 417
    Height = 461
    Caption = #1057#1077#1088#1074#1077#1088
    TabOrder = 0
    DesignSize = (
      417
      461)
    object shpServer: TShape
      Left = 391
      Top = 16
      Width = 17
      Height = 17
      Anchors = [akTop, akRight]
      Brush.Color = clRed
      Shape = stCircle
      ExplicitLeft = 159
    end
    object btnServerStart: TBitBtn
      Left = 10
      Top = 16
      Width = 75
      Height = 25
      Caption = #1057#1090#1072#1088#1090
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 0
      OnClick = btnServerStartClick
    end
    object btnServerStop: TBitBtn
      Left = 10
      Top = 47
      Width = 75
      Height = 25
      Caption = #1057#1090#1086#1087
      DoubleBuffered = True
      Enabled = False
      ParentDoubleBuffered = False
      TabOrder = 1
      OnClick = btnServerStopClick
    end
    object lstServer: TListBox
      Left = 10
      Top = 78
      Width = 247
      Height = 138
      ItemHeight = 13
      TabOrder = 2
    end
    object rgServer: TRadioGroup
      Left = 16
      Top = 222
      Width = 241
      Height = 153
      Caption = #1057#1077#1088#1074#1077#1088#1085#1072#1103' '#1088#1072#1076#1080#1086#1075#1088#1091#1087#1087#1072
      TabOrder = 3
    end
    object cmbxServer: TComboBox
      Left = 104
      Top = 18
      Width = 273
      Height = 21
      Style = csDropDownList
      TabOrder = 4
    end
  end
  object gbClient: TGroupBox
    Left = 431
    Top = 8
    Width = 409
    Height = 461
    Caption = #1050#1083#1080#1077#1085#1090
    TabOrder = 1
    DesignSize = (
      409
      461)
    object shpClient: TShape
      Left = 383
      Top = 16
      Width = 17
      Height = 17
      Anchors = [akTop, akRight]
      Brush.Color = clRed
      Shape = stCircle
      ExplicitLeft = 159
    end
    object btnClientStart: TBitBtn
      Left = 10
      Top = 16
      Width = 75
      Height = 25
      Caption = #1057#1090#1072#1088#1090
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 0
      OnClick = btnClientStartClick
    end
    object btnClientStop: TBitBtn
      Left = 10
      Top = 47
      Width = 75
      Height = 25
      Caption = #1057#1090#1086#1087
      DoubleBuffered = True
      Enabled = False
      ParentDoubleBuffered = False
      TabOrder = 1
      OnClick = btnClientStopClick
    end
    object rgClientCMD: TRadioGroup
      Left = 91
      Top = 11
      Width = 134
      Height = 61
      Caption = #1053#1086#1084#1077#1088' '#1082#1086#1084#1072#1085#1076#1099
      Columns = 4
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        '0'
        '1'
        '2'
        '3')
      TabOrder = 2
    end
    object mmClient: TMemo
      Left = 10
      Top = 78
      Width = 390
      Height = 107
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      TabOrder = 3
    end
    object btnClientSend: TBitBtn
      Left = 10
      Top = 191
      Width = 75
      Height = 25
      Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100
      DoubleBuffered = True
      Enabled = False
      ParentDoubleBuffered = False
      TabOrder = 4
      OnClick = btnClientSendClick
    end
  end
end
