// JCL_DEBUG_EXPERT_INSERTJDBG OFF
program MailSlotServer;

uses
  Forms,
  frmMSSMain_u in 'frmMSSMain_u.pas' {frmMSSMain};

{$R *.res}

begin
  System.ReportMemoryLeaksOnShutdown := true;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMSSMain, frmMSSMain);
  Application.Run;
end.
