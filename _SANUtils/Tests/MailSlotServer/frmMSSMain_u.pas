unit frmMSSMain_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,
  SANProtocol, SANProtocolCodecBase, SANMailSlots, ExtCtrls;

const
  MailslotName  = '\\.\mailslot\SanMailSlotTests';
  EventName     = 'SanMailSlotTests_Command_Event';

type
//------------------------------------------------------------------------------
  PMSSDataPackage = ^TMSSDataPackage;
  TMSSDataPackage = record
    CmdName : PString;
    Strings : TStrings;
  end;
//------------------------------------------------------------------------------
  PMSSDataRec = ^TMSSDataRec;
  TMSSDataRec = record
    /// <summary>
    /// Передатчик данных
    /// </summary>
    Transporter : TSANServerMailSlot;

    /// <summary>
    /// Соединение передачи данных
    /// </summary>
    Connection : Pointer;

    /// <summary>
    /// Пакет данных
    /// </summary>
    DataPackage : PMSSDataPackage;
  end;
//------------------------------------------------------------------------------
  TfrmMSSMain = class(TForm)
    btnServerStart: TBitBtn;
    btnServerStop: TBitBtn;
    gbServer: TGroupBox;
    gbClient: TGroupBox;
    shpServer: TShape;
    btnClientStart: TBitBtn;
    shpClient: TShape;
    btnClientStop: TBitBtn;
    rgClientCMD: TRadioGroup;
    mmClient: TMemo;
    btnClientSend: TBitBtn;
    lstServer: TListBox;
    rgServer: TRadioGroup;
    cmbxServer: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnServerStartClick(Sender: TObject);
    procedure btnServerStopClick(Sender: TObject);
    procedure btnClientStartClick(Sender: TObject);
    procedure btnClientStopClick(Sender: TObject);
    procedure btnClientSendClick(Sender: TObject);
  private
    FPCStack : TSANProtocolStack;
    FRouter : TSANProtocolQueuedRouter;

    FServerMailSlot : TSANServerMailSlot;
    FClientMailSlot : TSANClientMailSlot;

  protected
    procedure RouterUnHandledCommand(const ASender : TObject;  const ADataRec : PSANProtocolDataRec);
    procedure RouterCommandError(const ASender, ACommander : TObject;
      const AException : Exception; const ACommandName : string;
      const ADataRec : PSANProtocolDataRec);

  public
    { Public declarations }

  published
    procedure CMD_Command1(const ADataRec : PMSSDataRec; var AHandled : boolean);
    procedure CMD_Command2(const ADataRec : PMSSDataRec; var AHandled : boolean);
    procedure CMD_Command3(const ADataRec : PMSSDataRec; var AHandled : boolean);
  end;

var
  frmMSSMain: TfrmMSSMain;

implementation

{$R *.dfm}

// protected
procedure TfrmMSSMain.RouterUnHandledCommand(const ASender: TObject;
  const ADataRec: PSANProtocolDataRec);
begin
  MessageBox(Handle, PChar( (FPCStack as ISANProtocolCodec).ReadCommandName(ADataRec.DataPackage) + ' - неизвестная команда.' ), 'Неизвестная команда', MB_ICONWARNING);
end;

procedure TfrmMSSMain.RouterCommandError(const ASender, ACommander: TObject;
  const AException: Exception; const ACommandName: string;
  const ADataRec: PSANProtocolDataRec);
begin
  MessageBox(Handle, PChar(AException.Message), 'Ошибка', MB_ICONWARNING);
end;

// published
procedure TfrmMSSMain.FormCreate(Sender: TObject);
begin
  FPCStack := TSANProtocolStack.Create;
  FPCStack.Add( TSANProtocolCodecCommandName.Create );
  FPCStack.Add( TSANProtocolCodecStrings.Create );


  FRouter := TSANProtocolQueuedRouter.Create;
  FRouter.ChangeProtocolCodec(FPCStack);
  FRouter.Commanders.Add(Self);
  FRouter.OnUnHandledCommand  := RouterUnHandledCommand;
  FRouter.OnError             := RouterCommandError;
end;

procedure TfrmMSSMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FClientMailSlot);
  FreeAndNil(FServerMailSlot);

  FreeAndNil(FRouter);
  FreeAndNil(FPCStack);
end;

procedure TfrmMSSMain.btnServerStartClick(Sender: TObject);
begin
  FServerMailSlot := TSANServerMailSlot.Create(MailslotName, EventName);
  FServerMailSlot.ProtocolRouter := FRouter;

  btnServerStart.Enabled  := False;
  btnServerStop.Enabled   := True;
  shpServer.Brush.Color   := clLime;
end;

procedure TfrmMSSMain.btnServerStopClick(Sender: TObject);
begin
  FreeAndNil(FServerMailSlot);

  btnServerStart.Enabled  := True;
  btnServerStop.Enabled   := False;
  shpServer.Brush.Color   := clRed;
end;

procedure TfrmMSSMain.btnClientStartClick(Sender: TObject);
begin
  FClientMailSlot := TSANClientMailSlot.Create(MailslotName, EventName);

  btnClientStart.Enabled  := False;
  btnClientStop.Enabled   := True;
  rgClientCMD.Enabled     := True;
  mmClient.Enabled        := True;
  btnClientSend.Enabled   := True;
  shpClient.Brush.Color   := clLime;
end;

procedure TfrmMSSMain.btnClientStopClick(Sender: TObject);
begin
  FreeAndNil(FClientMailSlot);

  btnClientStart.Enabled  := True;
  btnClientStop.Enabled   := False;
  rgClientCMD.Enabled     := False;
  mmClient.Enabled        := False;
  btnClientSend.Enabled   := False;
  shpClient.Brush.Color   := clRed;
end;

procedure TfrmMSSMain.btnClientSendClick(Sender: TObject);
var
  vIntf : ISANProtocolCodec;
  vPkg : PMSSDataPackage;
//  vSL : TStrings;
begin
  vIntf := (FPCStack as ISANProtocolCodec);

  vIntf.NewDataPackage(vPkg);
  try

    vPkg^.CmdName^ := 'Command' + IntToStr(rgClientCMD.ItemIndex);
    vPkg^.Strings.Assign(mmClient.Lines);

    SANProtocolSend(vIntf, FClientMailSlot, vPkg);

//    ShowMessage(vIntf.ReadCommandName(vPkg));
  finally
    SANProtocolCodecFreeAndNilDataPackage(vIntf, vPkg);
  end;

//  FPSC.NewDataPackage(vSL);
//  try
//    vSL.Add('Command' + IntToStr(rgClientCMD.ItemIndex));
//    vSL.AddStrings(mmClient.Lines);
//    FClientMailSlot.SendData(vSL);
//  finally
//    FPSC.FreeDataPackage(vSL);
//  end;

end;

procedure TfrmMSSMain.CMD_Command1(const ADataRec: PMSSDataRec;
  var AHandled: boolean);
begin
  lstServer.Items.Assign( ADataRec^.DataPackage^.Strings );

  AHandled := True;
end;

procedure TfrmMSSMain.CMD_Command2(const ADataRec: PMSSDataRec;
  var AHandled: boolean);
begin
  rgServer.Items.Assign( ADataRec^.DataPackage^.Strings );

  AHandled := True;
end;

procedure TfrmMSSMain.CMD_Command3(const ADataRec: PMSSDataRec;
  var AHandled: boolean);
begin
  cmbxServer.Items.Assign( ADataRec^.DataPackage^.Strings );

  AHandled := True;
end;


end.
