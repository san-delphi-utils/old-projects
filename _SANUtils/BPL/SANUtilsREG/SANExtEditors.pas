{*******************************************************}
{                                                       }
{       SANExtEditors                                   }
{       Внешние редакторы на онове "пакетов-как-Dll".   }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExtEditors;
{$I NX.INC}
INTERFACE

USES
  ToolsAPI,
  Windows, SysUtils, Classes,
//  Dialogs,
  SANWinUtils,
  SANPkgAsDllServer,
  SANExtEditorsIntf;

TYPE
  /// <summary>
  /// Класс управления внешними редакторами.
  /// </summary>
  TSANExtEditorsManager = class(TSANPkgAsDllsServer, ISANExtEditorsManager)
  private
    FExtEditors : IInterfaceList;

  protected
    // ISANExtEditorsManager
    function RegEditor(const AEditor : ISANExtEditor) : WordBool; safecall;
    function UnRegEditor(const AEditorName : WideString) : WordBool; safecall;
    function GetAppHandle : THandle; safecall;

    function EditorFindFunc(const AItem, AValue : Pointer) : boolean;

  public
    constructor Create(const ADllEntryPointName : string); override;
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HResult; override;

    function FindEditor(out AEditor : ISANExtEditor; const AName : WideString;
      const AIndexPtr : PInteger = nil) : boolean;
    function ExistsEditor(const AName : WideString; const AIndexPtr : PInteger = nil) : boolean;

    function ExecuteEditor(const AName, ACaption : WideString; var AValue : OleVariant) : boolean;
  end;

procedure ExtEditorsManagerInit;
procedure ExtEditorsManagerFinal;
function ExtEditorsManager : TSANExtEditorsManager;

IMPLEMENTATION

uses
  ComObj,
  SANListUtils,
  SANIntfUtils;

//==============================================================================
// TSANExtEditorsManager
// protected
function TSANExtEditorsManager.RegEditor(const AEditor: ISANExtEditor): WordBool;
begin
  Result := not ExistsEditor(AEditor.Name);

  if Result then
    FExtEditors.Add(AEditor);
end;

function TSANExtEditorsManager.UnRegEditor(const AEditorName: WideString): WordBool;
var
  i : integer;
begin
  Result := ExistsEditor(AEditorName, @i);

  if Result then
    FExtEditors.Delete(i);
//  Result := SANListRemove(FExtEditors, EditorFindFunc, @AEditorName);
end;

function TSANExtEditorsManager.GetAppHandle: THandle;
begin
  Result := (BorlandIDEServices as IOTAServices).GetParentHandle;
end;

function TSANExtEditorsManager.EditorFindFunc(const AItem, AValue: Pointer): boolean;
begin
  Result := CompareText(ISANExtEditor(AItem).Name, PWideString(AValue)^) = 0;
end;

// public
constructor TSANExtEditorsManager.Create(const ADllEntryPointName: string);
begin
  inherited;

  FExtEditors := TInterfaceList.Create;
end;

destructor TSANExtEditorsManager.Destroy;
begin
  FExtEditors := nil;

  inherited;
end;

function TSANExtEditorsManager.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  Result := HandleSafeCallException
  (
    ExceptObject,
    ExceptAddr,
    StringToGUID('{76AC4820-5208-468F-914F-08840D95112A}'),
    String(ExceptObject.ClassName),
    '' //файл справки
  );
end;

function TSANExtEditorsManager.FindEditor(out AEditor: ISANExtEditor;
  const AName: WideString; const AIndexPtr : PInteger): boolean;
var
  i : integer;
  vEE : ISANExtEditor;
begin

  for i := 0 to FExtEditors.Count - 1 do
    if  Supports(FExtEditors[i], ISANExtEditor, vEE)
    and (CompareText(vEE.Name, AName) = 0)
    then
    begin
      AEditor := vEE;

      if Assigned(AIndexPtr) then
        AIndexPtr^ := i;

      Exit(True);
    end;

  Result := False;
//  Result := SANListFind(AEditor, FExtEditors, EditorFindFunc, @AName);
end;

function TSANExtEditorsManager.ExistsEditor(const AName: WideString; const AIndexPtr : PInteger): boolean;
var
  vDummy : ISANExtEditor;
begin
  Result := FindEditor(vDummy, AName, AIndexPtr);
//  Result := SANListExists(FExtEditors, EditorFindFunc, @AName);
end;

function TSANExtEditorsManager.ExecuteEditor(const AName, ACaption: WideString;
  var AValue: OleVariant): boolean;
var
  vEE : ISANExtEditor;
begin
  Result := FindEditor(vEE, AName)
        and vEE.Execute(ACaption, AValue);
end;
//==============================================================================

const
  ENTRY_POINT_NAME = '_7147A73047FC4C968792D000812B92A8';

var
  vExtEditorsManager : TSANExtEditorsManager;

procedure ExtEditorsManagerInit;

  procedure LoadExtEditors;
  var
    vPath : string;
    vSR : TSearchRec;
  begin
    vPath := ExtractFilePath( ModuleFileName(HInstance));

//    ShowMessage(vPath);

    if FindFirst(vPath + '*.eed', faAnyFile AND NOT faDirectory, vSR) = 0 then
      try

        repeat
//          ShowMessage(vPath + vSR.Name);

          vExtEditorsManager.Packages.Add(vPath + vSR.Name);

        until FindNext(vSR) > 0;

      finally
        FindClose(vSR);
      end;

  end;

begin
  vExtEditorsManager := TSANExtEditorsManager.Create(ENTRY_POINT_NAME);

  LoadExtEditors;
end;

procedure ExtEditorsManagerFinal;
begin
  vExtEditorsManager.Packages.Clear;

  FreeAndNil(vExtEditorsManager);
end;

function ExtEditorsManager : TSANExtEditorsManager;
begin
  Result := vExtEditorsManager;
end;

END.
