{*******************************************************}
{                                                       }
{       SANUtilsReg                                     }
{       Регистрация SANUtils в IDE                      }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANUtilsReg;
{$I NX.INC}
INTERFACE

USES
  ToolsAPI, DesignIntf, DesignEditors,

  Windows, SysUtils, Classes, TypInfo,
//  Dialogs,

  SANPkgAsDllCustomDM, SANPluginsServerDM, SANPkgAsDllClient, SANPkgAsDllClientDM, SANPluginsClientDM;

TYPE
  /// <summary>
  /// Редактор свойств для версии.
  /// </summary>
  TSANVersionPropertyEditor = class(TClassProperty)
  public
    function GetValue: string; override;
  end;

  /// <summary>
  /// Редактор свойств GUID в виде строки.
  /// </summary>
  TSANGUIDPropertyEditor = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;


procedure Register;

IMPLEMENTATION

uses
  SANExtEditors;

{$R _autogenres\SANUtilsRegBitmaps.RES}

procedure Register;
begin
//  ShowMessage('Init');
  ExtEditorsManagerInit;

  RegisterCustomModule(TSANPluginsServerDataModule, TCustomModule);
  RegisterCustomModule(TSANPkgAsDllEntryDataModule, TCustomModule);
  RegisterCustomModule(TSANPluginDataModule, TCustomModule);

  RegisterPropertyEditor(TypeInfo(TSANVersionPropObj),  TSANVersionInitDoneDataModule,  'Version',  TSANVersionPropertyEditor);
  RegisterPropertyEditor(TypeInfo(string),              TSANVersionInitDoneDataModule,  'GUID',     TSANGUIDPropertyEditor);

  SplashScreenServices.AddPluginBitmap
  (
    'SAN Utils',
    LoadBitmap(hInstance, 'SAN_UTILS_BMP_LOGO'),
    False,
    '',
    {$I SANUtilsREGBPL.textversion}
  );

end;

//==============================================================================
// TSANVersionPropertyEditor
// public
function TSANVersionPropertyEditor.GetValue: string;
var
  vVPObj : TSANVersionPropObj;
begin
  vVPObj := (GetObjectProp(GetComponent(0), GetPropInfo) as TSANVersionPropObj);

  Result := Format('%d.%d.%d.%d', [vVPObj.V1Major, vVPObj.V2Minor, vVPObj.V3Release, vVPObj.V4Build]);
end;
//==============================================================================

//==============================================================================
// TSANGUIDPropertyEditor
// public
function TSANGUIDPropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := inherited;
  Include(Result, paReadOnly);
  Include(Result, paDialog);
  Exclude(Result, paMultiSelect);
end;

procedure TSANGUIDPropertyEditor.Edit;
var
  vName, S : string;
  vValue : OleVariant;
begin

  if GetComponent(0) is TComponent then
    vName := (GetComponent(0) as TComponent).Name + ':'
  else
    vName := '';

  vValue := WideString(GetStrValue);

  if ExtEditorsManager.ExecuteEditor('GUIDEditor', WideString(vName + GetName), vValue) then
    SetStrValue(vValue);
end;
//==============================================================================

INITIALIZATION

FINALIZATION
  ExtEditorsManagerFinal;
//  ShowMessage('Final');

END.
