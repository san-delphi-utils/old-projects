{*******************************************************}
{                                                       }
{       SANExtEditorsIntf                               }
{       Интерфейсы внешних редакторов.                  }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANExtEditorsIntf;
{$I NX.INC}
INTERFACE

CONST
  EXT_EDITORS_ENTRY_POINT_NAME = '_7147A73047FC4C968792D000812B92A8';

TYPE
  /// <summary>
  /// Интерфейс внешнего редактора.
  /// </summary>
  ISANExtEditor = interface(IInterface)
    ['{F60E2BD9-F204-4ECC-B965-0F55552473BF}']
    function Get_Name : WideString; safecall;
    /// <summary>
    /// Уникальное имя внешнего редактора.
    /// </summary>
    property Name : WideString  read Get_Name;

    /// <summary>
    /// Вызов внешнего редактора.
    /// </summary>
    /// <param name="ACaption">
    /// Заголовок редактора.
    /// </param>
    /// <param name="AValue">
    /// Изменяемое значение.
    /// </param>
    /// <returns>
    /// WordBool. True - редактирование подтверждено.
    /// </returns>
    function Execute(const ACaption : WideString; var AValue : OleVariant) : WordBool; safecall;
  end;

  /// <summary>
  /// Интерфейс управления внешними редакторами.
  /// </summary>
  ISANExtEditorsManager = interface(IInterface)
    ['{225784A9-0990-4879-AE6F-4AF8D4DF4A9F}']

    /// <summary>
    /// Регистрация внешнего редактора.
    /// </summary>
    /// <param name="AEditor">
    /// Интерфейс внешнего редактора для регистрации.
    /// </param>
    /// <returns>
    /// WordBool. True - успешно.
    /// </returns>
    function RegEditor(const AEditor : ISANExtEditor) : WordBool; safecall;

    /// <summary>
    /// Разрегистрация внешнего редактора.
    /// </summary>
    /// <param name="AEditorName">
    /// Уникальное имя внешнего редактора.
    /// </param>
    /// <returns>
    /// WordBool. True - успешно.
    /// </returns>
    function UnRegEditor(const AEditorName : WideString) : WordBool; safecall;

    function GetAppHandle : THandle; safecall;
    /// <summary>
    /// Описатель приложения
    /// </summary>
    property AppHandle : THandle  read GetAppHandle;
  end;

IMPLEMENTATION

END.
