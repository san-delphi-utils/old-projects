unit $(ModuleIdent);

interface

uses
  Windows, SysUtils, Classes,
  SANPkgAsDllIntf, SANPkgAsDllCustomDM, SANPkgAsDllClient, SANPkgAsDllClientDM;

type
  T$(FormIdent) = class(TSANPkgAsDllEntryDataModule)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  PkgAsDllEntryDataModuleClass := T$(FormIdent);
end.
