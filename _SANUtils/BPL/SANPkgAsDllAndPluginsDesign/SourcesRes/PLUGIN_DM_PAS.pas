unit $(ModuleIdent);

interface

uses
  Windows, SysUtils, Classes,
  SANPluginsIntf, SANPkgAsDllCustomDM, SANPluginsClient, SANPluginsClientDM;

type
  T$(FormIdent) = class(TSANPluginDataModule)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  SANPluginRegister(T$(FormIdent));
end.
