UNIT frmSANGUIDDLG_u;

INTERFACE

USES
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, StdCtrls, ComObj, ActiveX, Mask;

TYPE
  TfrmSANGUIDDLG = class(TForm)
    lblGUID: TLabel;
    sbtnGUID: TSpeedButton;
    shpGUID: TShape;
    medtGUID: TMaskEdit;
    pnlButtons: TPanel;
    btnCancel: TBitBtn;
    btnOK: TBitBtn;
    pnlCustomData: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure sbtnGUIDClick(Sender: TObject);
    procedure edtGUIDChange(Sender: TObject);
    procedure edtGUIDKeyPress(Sender: TObject; var Key: Char);

  private
    FFlagGUID : boolean;

  protected
    procedure SetWarnShape(const AShape : TShape; var AFlag : boolean;
      const AValidateCondition : boolean; const AWarningHint : string);

  public

  end;

function SANGUIDDLG(const ACaption : TCaption; var AGUIDStr : string) : boolean;

IMPLEMENTATION

{$R *.dfm}

function SANGUIDDLG(const ACaption : TCaption; var AGUIDStr : string) : boolean;
begin
  with TfrmSANGUIDDLG.Create(nil) do
  try
    Caption := ACaption;

    medtGUID.Text := AGUIDStr;

    Result := (ShowModal = mrOk);

    if Result then
      AGUIDStr := medtGUID.Text;

  finally
    Free
  end;
end;

procedure TfrmSANGUIDDLG.SetWarnShape(const AShape: TShape; var AFlag: boolean;
  const AValidateCondition: boolean; const AWarningHint: string);
begin
  AFlag := AValidateCondition;

  with AShape do
    if AValidateCondition then
      begin
        Brush.Color := clLime;
        Hint := '';
      end
    else
      begin
        Brush.Color := clRed;
        Hint := AWarningHint;
      end;

  btnOK.Enabled := FFlagGUID;
end;

procedure TfrmSANGUIDDLG.FormCreate(Sender: TObject);
begin
  FFlagGUID := True;
end;

procedure TfrmSANGUIDDLG.sbtnGUIDClick(Sender: TObject);
var
  vGUID : TGUID;
begin
  OleCheck(CoCreateGuid(vGUID));

  medtGUID.Text := GUIDToString(vGUID);
end;

procedure TfrmSANGUIDDLG.edtGUIDChange(Sender: TObject);
var
  vGUID : TGUID;
  vHR : HRESULT;
begin
  vHR := CLSIDFromString
  (
    PWideChar
    (
      WideString
      (
        Trim
        (
          (Sender as TMaskEdit).Text
        )
      )
    ),
    vGUID
  );

  SetWarnShape(shpGUID, FFlagGUID, vHR = S_OK, 'Введен не корректный GUID');
end;

procedure TfrmSANGUIDDLG.edtGUIDKeyPress(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', 'A'..'F', 'a'..'f', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)]) then
    Key := #0;
end;

END.
