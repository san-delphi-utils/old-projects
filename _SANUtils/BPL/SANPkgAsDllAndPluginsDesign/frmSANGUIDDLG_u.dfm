object frmSANGUIDDLG: TfrmSANGUIDDLG
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'frmSANGUIDDLG'
  ClientHeight = 79
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object pnlButtons: TPanel
    Left = 0
    Top = 41
    Width = 408
    Height = 38
    Align = alTop
    Caption = 'pnlButtons'
    ShowCaption = False
    TabOrder = 1
    DesignSize = (
      408
      38)
    object btnCancel: TBitBtn
      Left = 325
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      DoubleBuffered = True
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333000033333333333333333FF33333FF330000333003333300
        3333733F333733F3000033300033300033337333F37333F30000333300030003
        3333373337333F330000333330000033333333733333F3330000333333000333
        33333337333F33330000333330000033333333733333F3330000333300030003
        3333373337333F33000033300033300033337333F37333F30000333003333300
        3333733F333733F3000033333333333333333773333377330000333333333333
        33333333333333330000}
      ModalResult = 2
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 0
    end
    object btnOK: TBitBtn
      Left = 244
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1050
      Default = True
      DoubleBuffered = True
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333333330000333333333333333333F3333333330000333303333333
        33333F3F3333333300003330003333333333F333F33333330000330000033333
        333F33333F33333300003000300033333373333333F333330000300333000333
        337333F7333F33330000333333300033333777337333F3330000333333330003
        3333333337333F33000033333333300033333333337333F30000333333333300
        3333333333373373000033333333333333333333333377330000333333333333
        33333333333333330000}
      ModalResult = 1
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 1
    end
  end
  object pnlCustomData: TPanel
    Left = 0
    Top = 0
    Width = 408
    Height = 41
    Align = alTop
    Caption = 'pnlCustomData'
    TabOrder = 0
    DesignSize = (
      408
      41)
    object lblGUID: TLabel
      Left = 8
      Top = 11
      Width = 33
      Height = 16
      Caption = 'GUID:'
    end
    object sbtnGUID: TSpeedButton
      Left = 359
      Top = 8
      Width = 25
      Height = 24
      Hint = #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
      Anchors = [akTop, akRight]
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333FFFFF3333000033333707333333333FF3337733330000333700733333
        3333733337333333000033300733333333337333733333330000337073333333
        3337333F3333333300003300333333333337333F3FFFFFF30000330033300000
        3337333F733333F300003300733370003337333F373333F30000337007370000
        33373333F33333F3000033300000007033337333333373F30000333370007330
        33333733333773F3000033333333333333333377777337330000333333333333
        33333333333333330000}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = sbtnGUIDClick
      ExplicitLeft = 488
    end
    object shpGUID: TShape
      Left = 391
      Top = 17
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
      ExplicitLeft = 520
    end
    object medtGUID: TMaskEdit
      Left = 47
      Top = 8
      Width = 311
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      EditMask = '{AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA};1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 38
      ParentFont = False
      TabOrder = 0
      Text = '{00000000-0000-0000-0000-000000000000}'
      OnChange = edtGUIDChange
      OnKeyPress = edtGUIDKeyPress
    end
  end
end
