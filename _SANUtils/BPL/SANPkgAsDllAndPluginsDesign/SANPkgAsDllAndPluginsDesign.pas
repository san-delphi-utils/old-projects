{*******************************************************}
{                                                       }
{       SANPkgAsDllDesign                               }
{       Взаимодействие с "пакетом-как-DLL".             }
{       Визуальное проектирование                       }
{                                                       }
{       Copyright (C) 2012 SAN                          }
{                                                       }
{*******************************************************}
UNIT SANPkgAsDllAndPluginsDesign;
{$I NX.INC}
INTERFACE

USES
  Windows, SysUtils, Classes, Forms, ComObj, Controls, TypInfo,
  ToolsAPI, DesignIntf, DesignEditors,
  JclFileUtils,
  SANRepositoryExpertsUtils,
  SANPkgAsDllCustomDM, SANPluginsServerDM, SANPkgAsDllClient, SANPkgAsDllClientDM, SANPluginsClientDM;

TYPE
  ESANPkgAsDllClientDesignError = class(ESANPkgAsDllClientError);

  TSANVersionPropertyEditor = class(TClassProperty)
  public
    function GetValue: string; override;
  end;

  TSANGUIDPropertyEditor = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;

procedure Register;

IMPLEMENTATION

uses
  frmSANVersionDMDLG_u,
  frmSANGUIDDLG_u;

{$R *.res}
{$R SourcesRes.res}

TYPE
  TSANCustomVerDMRepositoryExpert = class(TSAN_RepositoryResource_Expert)
  protected
    function GetComponentName : string; virtual; abstract;
    function GetCaption : string; virtual; abstract;
    function GetDfmResName : string; virtual; abstract;
    function GetPasResName : string; virtual; abstract;
    procedure Execute; override;
  end;

  TSANPkgAsDllEntryDataModuleRepositoryExpert = class(TSANCustomVerDMRepositoryExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetPage: string; override;
    function GetGlyphResName : string; override;
    function GetComponentName : string; override;
    function GetCaption : string; override;
    function GetDfmResName : string; override;
    function GetPasResName : string; override;
    procedure Execute; override;
  end;

  TSANPluginsDataModuleRepositoryExpert = class(TSANCustomVerDMRepositoryExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetPage: string; override;
    function GetGlyphResName : string; override;
    function GetComponentName : string; override;
    function GetCaption : string; override;
    function GetDfmResName : string; override;
    function GetPasResName : string; override;
  end;

  TSANPluginsServerDataModuleRepositoryExpert = class(TSANCustomVerDMRepositoryExpert)
  protected
    function GetIDString: string; override;
    function GetName: string; override;
    function GetComment: string; override;
    function GetPage: string; override;
    function GetGlyphResName : string; override;
    function GetComponentName : string; override;
    function GetCaption : string; override;
    function GetDfmResName : string; override;
    function GetPasResName : string; override;
  end;
//==============================================================================


procedure Register;
begin
  RegisterCustomModule(TSANPluginsServerDataModule, TCustomModule);
  RegisterCustomModule(TSANPkgAsDllEntryDataModule, TCustomModule);
  RegisterCustomModule(TSANPluginDataModule, TCustomModule);

  RegisterPackageWizard(TSANPluginsServerDataModuleRepositoryExpert.Create(HInstance));
  RegisterPackageWizard(TSANPkgAsDllEntryDataModuleRepositoryExpert.Create(HInstance));
  RegisterPackageWizard(TSANPluginsDataModuleRepositoryExpert.Create(HInstance));

  RegisterPropertyEditor(TypeInfo(TSANVersionPropObj),  TSANVersionInitDoneDataModule,  'Version',  TSANVersionPropertyEditor);
  RegisterPropertyEditor(TypeInfo(string),              TSANVersionInitDoneDataModule,  'GUID',     TSANGUIDPropertyEditor);
end;

function GetModuleRootComponent(out ARootComponent; const AModule : IOTAModule) : boolean;
var
  i : integer;
  vFIOTAFormEditor : IOTAFormEditor;
  vFIOTAComponent : IOTAComponent;
begin

  for i := 0 to AModule.GetModuleFileCount - 1 do
    if Supports(AModule.GetModuleFileEditor(i), IOTAFormEditor, vFIOTAFormEditor) then
    begin
      vFIOTAComponent := vFIOTAFormEditor.GetRootComponent;
      if vFIOTAComponent = nil then
        Continue;

      Pointer(ARootComponent) := vFIOTAComponent.GetComponentHandle;
      Exit(True);
    end;

  Result := False;
end;

function FindRootComponentByClass(out AModule : IOTAModule; const AClass : TComponentClass) : boolean;
var
  i : integer;
  vProject : IOTAProject;
  vServices : IOTAModuleServices;
  vFIOTAModuleInfo : IOTAModuleInfo;
  vModule : IOTAModule;
  vRootComponent : TComponent;
begin
  Result := False;

  if not Assigned(AClass) then
    Exit;

  vProject := GetActiveProject;
  if vProject = nil then
    Exit;

  vServices := BorlandIDEServices as IOTAModuleServices;

  for i := 0 to vProject.GetModuleCount - 1 do
  begin
    vFIOTAModuleInfo := vProject.GetModule(i);

    if vFIOTAModuleInfo.GetFormName = '' then
      Continue;

    vModule := vFIOTAModuleInfo.OpenModule;

    if GetModuleRootComponent(vRootComponent, vModule)
    and vRootComponent.ClassType.InheritsFrom(AClass)
    then
    begin
      AModule := vModule;
      Exit(True);
    end;

  end;//for

end;

function ActiveProjectName : string;
begin
  Result := PathExtractFileNameNoExt((BorlandIDEServices as IOTAModuleServices).GetActiveProject.FileName);
end;

function DFMString(const S : string) : string;
var
  I: Integer;
  vQuoted : boolean;
begin

  if S = '' then
  begin
    Result := #39#39;
    Exit;
  end;

  vQuoted := False;

  SetLength(Result, 0);

  for I := 1 to Length(S) do
    if (S[i] = #39) or (ORD(S[i]) > 255) then
      begin

        if vQuoted then
        begin
          Result := Result + #39;
          vQuoted := False;
        end;

        if S[i] = #39 then
          Result := Result + '#39'
        else
          Result := Result + '#' + IntToStr(Ord(S[i]));
      end
    else
      begin

        if not vQuoted then
        begin
          Result := Result + #39;
          vQuoted := True;
        end;

        Result := Result + S[i];
      end;

  if vQuoted then
    Result := Result + #39;

end;

//==============================================================================
// TSANVersionPropertyEditor
// public
function TSANVersionPropertyEditor.GetValue: string;
var
  vVPObj : TSANVersionPropObj;
begin
  vVPObj := (GetObjectProp(GetComponent(0), GetPropInfo) as TSANVersionPropObj);

  Result := Format('%d.%d.%d.%d', [vVPObj.V1Major, vVPObj.V2Minor, vVPObj.V3Release, vVPObj.V4Build]);
end;
//==============================================================================

//==============================================================================
// TSANGUIDPropertyEditor
// public
function TSANGUIDPropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := inherited;
  Include(Result, paReadOnly);
  Include(Result, paDialog);
  Exclude(Result, paMultiSelect);
end;

procedure TSANGUIDPropertyEditor.Edit;
var
  vName, S : string;
begin

  if GetComponent(0) is TComponent then
    vName := (GetComponent(0) as TComponent).Name + ':'
  else
    vName := '';

  S := GetStrValue;

  if not SANGUIDDLG(vName + GetName, S) then
    Exit;

  SetStrValue(S);
end;
//==============================================================================

//==============================================================================
// TSANCustomVerDMRepositoryExpert
// protected
procedure TSANCustomVerDMRepositoryExpert.Execute;
var
  vForm : TfrmSANVersionDMDLG;
  vModule : IOTAModule;
  vParams : TStrings;
begin

  vForm := TfrmSANVersionDMDLG.Create(nil);
  try

    with vForm do
    begin
      Icon.LoadFromResourceName(HInstance, Self.GetGlyphResName);

      edtComponentName.Text  := Self.GetComponentName;
      edtUnitNameFmt.Text    := '%s_u';
      edtCaption.Text        := Self.GetCaption;
      mmDescription.Text     := '';
      edtURL.Text            := '';
      edtAuthor.Text         := '';

      seMajor.Value    := 1;
      seMinor.Value    := 0;
      seRelease.Value  := 0;
      seBuild.Value    := 0;


      if ShowModal <> mrOK then
        Exit;

    end;

    vParams := TStringList.Create;
    try
      vParams.LineBreak := '{A5A082B3-A1BA-4760-9919-F06CD0EDDBC8}';

      vParams.Add('GUID='         + Trim(vForm.medtGUID.Text));
      vParams.Add('Caption='      + DFMString(Trim(vForm.edtCaption.Text)));
      vParams.Add('Description='  + DFMString(Trim(vForm.mmDescription.Text)));
      vParams.Add('URL='          + DFMString(Trim(vForm.edtURL.Text)));
      vParams.Add('Author='       + DFMString(Trim(vForm.edtAuthor.Text)));
      vParams.Add('V1Major='      + IntToStr(vForm.seMajor.Value));
      vParams.Add('V2Minor='      + IntToStr(vForm.seMinor.Value));
      vParams.Add('V3Release='    + IntToStr(vForm.seRelease.Value));
      vParams.Add('V4Build='      + IntToStr(vForm.seBuild.Value));

      vModule := TSAN_Static_Creator.CreateModule(
        ctForm,
        vForm.edtComponentName.Text,
        '',
        '%s',
        vForm.edtUnitNameFmt.Text,
        ReadTextFromRes( Self.GetDfmResName ),
        ReadTextFromRes( Self.GetPasResName ),
        vParams
      );

      (BorlandIDEServices as IOTAModuleServices).GetActiveProject.AddFile(vModule.FileName, True);

    finally
      FreeAndNil(vParams);
    end;

  finally
    FreeAndNil(vForm);
  end;

end;
//==============================================================================

//==============================================================================
// TSANPkgAsDllEntryDataModuleRepositoryExpert
// protected
function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetIDString: string;
begin
  Result := 'SAN.Repository.PkgAsDllEntryDataModule';
end;

function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetName: string;
begin
  Result := 'SAN PkgAsDllEntryDataModule';
end;

function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetComment: string;
begin
  Result := 'Создание модуля данных точки входа "пакета-как-Dll"';
end;

function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetGlyphResName: string;
begin
  Result := 'SAN_PKG_AS_DLL_ICON';
end;

function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetPage: string;
begin
  Result := 'SANPkgAsDllAndPlugins';
end;

function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetComponentName: string;
begin
  Result := ActiveProjectName + 'PkgAsDllEntry';
end;

function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetCaption: string;
begin
  Result := ActiveProjectName + 'Entry';
end;

function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetDfmResName: string;
begin
  Result := 'SAN_PKG_AS_DLL_ENTRY_DM_DFM';
end;

function TSANPkgAsDllEntryDataModuleRepositoryExpert.GetPasResName: string;
begin
  Result := 'SAN_PKG_AS_DLL_ENTRY_DM_PAS';
end;

procedure TSANPkgAsDllEntryDataModuleRepositoryExpert.Execute;
var
  vModule : IOTAModule;
begin

  if  FindRootComponentByClass(vModule, TSANPkgAsDllEntryDataModule)
  and
  (
    MessageBox
    (
      Application.MainForm.Handle,
      PChar
      (
        Format
        (
          'В проекте %s уже есть модуль точки входа, это %s. Продолжить?',
          [ActiveProjectName, vModule.FileName]
        )
      ),
      'Внимание',
      MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON1
    ) = ID_NO
  )
  then
    Exit;

  inherited;
end;
//==============================================================================

//==============================================================================
// TSANPluginsDataModuleRepositoryExpert
// protected
function TSANPluginsDataModuleRepositoryExpert.GetIDString: string;
begin
  Result := 'SAN.Repository.PluginDataModule';
end;

function TSANPluginsDataModuleRepositoryExpert.GetName: string;
begin
  Result := 'SAN PluginDataModule';
end;

function TSANPluginsDataModuleRepositoryExpert.GetComment: string;
begin
  Result := 'Создание модуля данных плагина в "пакете-как-Dll"';
end;

function TSANPluginsDataModuleRepositoryExpert.GetGlyphResName: string;
begin
  Result := 'SAN_PLUGINS_ICON';
end;

function TSANPluginsDataModuleRepositoryExpert.GetPage: string;
begin
  Result := 'SANPkgAsDllAndPlugins';
end;

function TSANPluginsDataModuleRepositoryExpert.GetComponentName: string;
begin
  Result := ActiveProjectName + 'Plugin';
end;

function TSANPluginsDataModuleRepositoryExpert.GetCaption: string;
begin
  Result := 'Plugin';
end;

function TSANPluginsDataModuleRepositoryExpert.GetDfmResName: string;
begin
  Result := 'SAN_PKG_AS_DLL_PLUGIN_DM_DFM';
end;

function TSANPluginsDataModuleRepositoryExpert.GetPasResName: string;
begin
  Result := 'SAN_PKG_AS_DLL_PLUGIN_DM_PAS';
end;
//==============================================================================

//==============================================================================
// TSANPluginsServerDataModuleRepositoryExpert
// protected
function TSANPluginsServerDataModuleRepositoryExpert.GetIDString: string;
begin
  Result := 'SAN.Repository.PluginsServerDataModule';
end;

function TSANPluginsServerDataModuleRepositoryExpert.GetName: string;
begin
  Result := 'SAN PluginsServerDataModule';
end;


function TSANPluginsServerDataModuleRepositoryExpert.GetComment: string;
begin
  Result := 'Создание модуля данных сервера плагинов, загружающего плагины из "пакетов-как-Dll"';
end;

function TSANPluginsServerDataModuleRepositoryExpert.GetGlyphResName: string;
begin
  Result := 'SAN_PLUGINS_SERVER_ICON';
end;

function TSANPluginsServerDataModuleRepositoryExpert.GetPage: string;
begin
  Result := 'SANPkgAsDllAndPlugins';
end;

function TSANPluginsServerDataModuleRepositoryExpert.GetComponentName: string;
begin
  Result := ActiveProjectName + 'PluginsServer';
end;

function TSANPluginsServerDataModuleRepositoryExpert.GetCaption: string;
begin
  Result := 'PluginsServer';
end;

function TSANPluginsServerDataModuleRepositoryExpert.GetDfmResName: string;
begin
  Result := 'SAN_PKG_AS_DLL_SERVER_DM_DFM';
end;

function TSANPluginsServerDataModuleRepositoryExpert.GetPasResName: string;
begin
  Result := 'SAN_PKG_AS_DLL_SERVER_DM_PAS';
end;
//==============================================================================

END.
