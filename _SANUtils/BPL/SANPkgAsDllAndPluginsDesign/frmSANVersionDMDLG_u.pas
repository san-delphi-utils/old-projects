unit frmSANVersionDMDLG_u;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ActiveX, ComObj, ExtCtrls, Spin, Mask;

type
  TfrmSANVersionDMDLG = class(TForm)
    lblCaption: TLabel;
    lblGUID: TLabel;
    lblAuthor: TLabel;
    lblURL: TLabel;
    lblDescription: TLabel;
    lblVersion: TLabel;
    edtCaption: TEdit;
    mmDescription: TMemo;
    edtURL: TEdit;
    edtAuthor: TEdit;
    sbtnGUID: TSpeedButton;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    lblMajor: TLabel;
    seMajor: TSpinEdit;
    lblMinor: TLabel;
    seMinor: TSpinEdit;
    lblRelease: TLabel;
    seRelease: TSpinEdit;
    lblBuild: TLabel;
    seBuild: TSpinEdit;
    shpGUID: TShape;
    shpCaption: TShape;
    shpDescription: TShape;
    shpURL: TShape;
    shpAuthor: TShape;
    shpVersion: TShape;
    medtGUID: TMaskEdit;
    pnlCustomData: TPanel;
    pnlButtons: TPanel;
    pnlCompNames: TPanel;
    lbComponentlName: TLabel;
    lblUnitNameFmt: TLabel;
    edtComponentName: TEdit;
    edtUnitNameFmt: TEdit;
    shpComponentName: TShape;
    shpUnitNameFmt: TShape;
    procedure FormCreate(Sender: TObject);
    procedure edtComponentNameChange(Sender: TObject);
    procedure edtComponentNameKeyPress(Sender: TObject; var Key: Char);
    procedure edtUnitNameFmtChange(Sender: TObject);
    procedure edtUnitNameFmtKeyPress(Sender: TObject; var Key: Char);
    procedure sbtnGUIDClick(Sender: TObject);
    procedure edtGUIDChange(Sender: TObject);
    procedure edtGUIDKeyPress(Sender: TObject; var Key: Char);
    procedure edtCaptionChange(Sender: TObject);
    procedure edtURLChange(Sender: TObject);
  private
    FFlagComponentName, FFlagUnitNameFmt, FFlagGUID, FFlagCaption, FFlagURL : boolean;
    function GetVersion: LongWord;

  protected
    procedure SetWarnShape(const AShape : TShape; var AFlag : boolean;
      const AValidateCondition : boolean; const AWarningHint : string);

  public
    property Version : LongWord  read GetVersion;
  end;

implementation

{$R *.dfm}

function TfrmSANVersionDMDLG.GetVersion: LongWord;
begin
  Result := seMajor.Value    * $FFFFFF +
            seMinor.Value    * $00FFFF +
            seRelease.Value  * $0000FF +
            seBuild.Value;
end;

procedure TfrmSANVersionDMDLG.SetWarnShape(const AShape: TShape; var AFlag : boolean;
  const AValidateCondition: boolean; const AWarningHint: string);
begin
  AFlag := AValidateCondition;

  with AShape do
    if AValidateCondition then
      begin
        Brush.Color := clLime;
        Hint := '';
      end
    else
      begin
        Brush.Color := clRed;
        Hint := AWarningHint;
      end;

  btnOK.Enabled := FFlagComponentName and FFlagUnitNameFmt and FFlagGUID and FFlagCaption and FFlagURL;
end;

procedure TfrmSANVersionDMDLG.FormCreate(Sender: TObject);
begin
  FFlagComponentName  := True;
  FFlagUnitNameFmt    := True;
  FFlagGUID           := True;
  FFlagCaption        := True;
  FFlagURL            := True;

  sbtnGUID.Click;
end;

procedure TfrmSANVersionDMDLG.edtComponentNameChange(Sender: TObject);
var
  S : string;
  vValidName : Boolean;
begin
  S := Trim((Sender as TEdit).Text);

  vValidName := (Length(S) > 0) and not CharInSet(S[1], ['0'..'9']);

  SetWarnShape(shpComponentName, FFlagComponentName, vValidName, 'Введено не корректное имя компонента');
end;

procedure TfrmSANVersionDMDLG.edtComponentNameKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z', '_', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)]) then
  begin
    MessageBeep(MB_OK);
    Key := #0;
  end;
end;

procedure TfrmSANVersionDMDLG.edtUnitNameFmtChange(Sender: TObject);
var
  S : string;
  vValidFmt : Boolean;
begin
  S := Trim((Sender as TEdit).Text);

  vValidFmt := (Length(S) > 0) and not CharInSet(S[1], ['0'..'9']);

  SetWarnShape(shpUnitNameFmt, FFlagUnitNameFmt, vValidFmt, 'Введен не корректный формат имени модуля');
end;

procedure TfrmSANVersionDMDLG.edtUnitNameFmtKeyPress(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z', '_', '%', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)]) then
  begin
    MessageBeep(MB_OK);
    Key := #0;
  end;
end;

procedure TfrmSANVersionDMDLG.sbtnGUIDClick(Sender: TObject);
var
  vGUID : TGUID;
begin
  OleCheck(CoCreateGuid(vGUID));

  medtGUID.Text := GUIDToString(vGUID);
end;

procedure TfrmSANVersionDMDLG.edtGUIDChange(Sender: TObject);
var
  vGUID : TGUID;
  vHR : HRESULT;
begin
  vHR := CLSIDFromString
  (
    PWideChar
    (
      WideString
      (
        Trim
        (
          (Sender as TMaskEdit).Text
        )
      )
    ),
    vGUID
  );

  SetWarnShape(shpGUID, FFlagGUID, vHR = S_OK, 'Введен не корректный GUID');
end;

procedure TfrmSANVersionDMDLG.edtGUIDKeyPress(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', 'A'..'F', 'a'..'f', #3, #22, #26, #24, CHR(VK_BACK), CHR(VK_INSERT)]) then
    Key := #0;
end;

procedure TfrmSANVersionDMDLG.edtCaptionChange(Sender: TObject);
begin
  SetWarnShape(shpCaption, FFlagCaption, Trim((Sender as TEdit).Text) <> '', 'Введено пустое внешнее имя');
end;

procedure TfrmSANVersionDMDLG.edtURLChange(Sender: TObject);
begin
  // TODO: RegExp
  SetWarnShape(shpURL, FFlagURL, True, 'Введен не корректный URL');
end;


end.
