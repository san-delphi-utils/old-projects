object frmSANVersionDMDLG: TfrmSANVersionDMDLG
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1090#1086#1095#1082#1080' '#1074#1093#1086#1076#1072' "'#1055#1072#1082#1077#1090#1072'-'#1082#1072#1082'-Dll"'
  ClientHeight = 353
  ClientWidth = 450
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object pnlCustomData: TPanel
    Left = 0
    Top = 74
    Width = 450
    Height = 241
    Align = alTop
    Caption = 'pnlCustomData'
    ShowCaption = False
    TabOrder = 1
    DesignSize = (
      450
      241)
    object lblAuthor: TLabel
      Left = 8
      Top = 160
      Width = 43
      Height = 16
      Caption = 'Author:'
    end
    object lblBuild: TLabel
      Left = 245
      Top = 192
      Width = 26
      Height = 13
      Caption = 'Build:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblCaption: TLabel
      Left = 8
      Top = 45
      Width = 48
      Height = 16
      Caption = 'Caption:'
    end
    object lblDescription: TLabel
      Left = 8
      Top = 75
      Width = 68
      Height = 16
      Caption = 'Description:'
    end
    object lblGUID: TLabel
      Left = 8
      Top = 13
      Width = 33
      Height = 16
      Caption = 'GUID:'
    end
    object lblMajor: TLabel
      Left = 89
      Top = 192
      Width = 31
      Height = 13
      Caption = 'Major:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblMinor: TLabel
      Left = 141
      Top = 192
      Width = 30
      Height = 13
      Caption = 'Minor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblRelease: TLabel
      Left = 193
      Top = 192
      Width = 42
      Height = 13
      Caption = 'Release:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblURL: TLabel
      Left = 8
      Top = 130
      Width = 27
      Height = 16
      Caption = 'URL:'
    end
    object lblVersion: TLabel
      Left = 8
      Top = 190
      Width = 48
      Height = 16
      Caption = 'Version:'
    end
    object sbtnGUID: TSpeedButton
      Left = 401
      Top = 10
      Width = 25
      Height = 24
      Hint = #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
      Anchors = [akTop, akRight]
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333FFFFF3333000033333707333333333FF3337733330000333700733333
        3333733337333333000033300733333333337333733333330000337073333333
        3337333F3333333300003300333333333337333F3FFFFFF30000330033300000
        3337333F733333F300003300733370003337333F373333F30000337007370000
        33373333F33333F3000033300000007033337333333373F30000333370007330
        33333733333773F3000033333333333333333377777337330000333333333333
        33333333333333330000}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = sbtnGUIDClick
    end
    object shpAuthor: TShape
      Left = 433
      Top = 166
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
    end
    object shpCaption: TShape
      Left = 433
      Top = 51
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
    end
    object shpDescription: TShape
      Left = 433
      Top = 94
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
    end
    object shpGUID: TShape
      Left = 433
      Top = 19
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
    end
    object shpURL: TShape
      Left = 433
      Top = 135
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
    end
    object shpVersion: TShape
      Left = 433
      Top = 209
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
    end
    object edtAuthor: TEdit
      Left = 88
      Top = 157
      Width = 338
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      Text = 'edtAuthor'
    end
    object edtCaption: TEdit
      Left = 88
      Top = 42
      Width = 338
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = 'edtCaption'
      OnChange = edtCaptionChange
    end
    object edtURL: TEdit
      Left = 88
      Top = 127
      Width = 338
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Text = 'edtURL'
      OnChange = edtURLChange
    end
    object medtGUID: TMaskEdit
      Left = 89
      Top = 10
      Width = 312
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      EditMask = '{AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA};1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 38
      ParentFont = False
      TabOrder = 0
      Text = '{00000000-0000-0000-0000-000000000000}'
      OnChange = edtGUIDChange
      OnKeyPress = edtGUIDKeyPress
    end
    object mmDescription: TMemo
      Left = 88
      Top = 72
      Width = 338
      Height = 49
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        'mmDescription')
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 2
    end
    object seBuild: TSpinEdit
      Left = 245
      Top = 206
      Width = 46
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 3
      MaxValue = 255
      MinValue = 0
      ParentFont = False
      TabOrder = 8
      Value = 0
    end
    object seMajor: TSpinEdit
      Left = 88
      Top = 206
      Width = 46
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 3
      MaxValue = 255
      MinValue = 0
      ParentFont = False
      TabOrder = 5
      Value = 0
    end
    object seMinor: TSpinEdit
      Left = 141
      Top = 206
      Width = 46
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 3
      MaxValue = 255
      MinValue = 0
      ParentFont = False
      TabOrder = 6
      Value = 0
    end
    object seRelease: TSpinEdit
      Left = 193
      Top = 206
      Width = 46
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 3
      MaxValue = 255
      MinValue = 0
      ParentFont = False
      TabOrder = 7
      Value = 0
    end
  end
  object pnlButtons: TPanel
    Left = 0
    Top = 315
    Width = 450
    Height = 38
    Align = alTop
    Caption = 'pnlButtons'
    ShowCaption = False
    TabOrder = 2
    DesignSize = (
      450
      38)
    object btnCancel: TBitBtn
      Left = 367
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      DoubleBuffered = True
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333000033333333333333333FF33333FF330000333003333300
        3333733F333733F3000033300033300033337333F37333F30000333300030003
        3333373337333F330000333330000033333333733333F3330000333333000333
        33333337333F33330000333330000033333333733333F3330000333300030003
        3333373337333F33000033300033300033337333F37333F30000333003333300
        3333733F333733F3000033333333333333333773333377330000333333333333
        33333333333333330000}
      ModalResult = 2
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 0
    end
    object btnOK: TBitBtn
      Left = 286
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1050
      Default = True
      DoubleBuffered = True
      Glyph.Data = {
        46010000424D460100000000000076000000280000001C0000000D0000000100
        040000000000D000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333333330000333333333333333333F3333333330000333303333333
        33333F3F3333333300003330003333333333F333F33333330000330000033333
        333F33333F33333300003000300033333373333333F333330000300333000333
        337333F7333F33330000333333300033333777337333F3330000333333330003
        3333333337333F33000033333333300033333333337333F30000333333333300
        3333333333373373000033333333333333333333333377330000333333333333
        33333333333333330000}
      ModalResult = 1
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 1
    end
  end
  object pnlCompNames: TPanel
    Left = 0
    Top = 0
    Width = 450
    Height = 74
    Align = alTop
    Caption = 'pnlCompNames'
    ShowCaption = False
    TabOrder = 0
    DesignSize = (
      450
      74)
    object lbComponentlName: TLabel
      Left = 8
      Top = 11
      Width = 121
      Height = 16
      Caption = #1048#1084#1103' '#1084#1086#1076#1091#1083#1103' '#1076#1072#1085#1085#1099#1093':'
    end
    object lblUnitNameFmt: TLabel
      Left = 8
      Top = 43
      Width = 128
      Height = 16
      Caption = #1060#1086#1088#1084#1072#1090' '#1080#1084#1077#1085#1080' Unit-'#1072':'
    end
    object shpComponentName: TShape
      Left = 433
      Top = 19
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
    end
    object shpUnitNameFmt: TShape
      Left = 433
      Top = 51
      Width = 7
      Height = 7
      Anchors = [akTop, akRight]
      Brush.Color = clLime
      ParentShowHint = False
      Shape = stCircle
      ShowHint = True
    end
    object edtComponentName: TEdit
      Left = 142
      Top = 10
      Width = 284
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Text = 'edtComponentName'
      OnChange = edtComponentNameChange
      OnKeyPress = edtComponentNameKeyPress
    end
    object edtUnitNameFmt: TEdit
      Left = 142
      Top = 40
      Width = 284
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = 'edtUnitNameFmt'
      OnChange = edtUnitNameFmtChange
      OnKeyPress = edtUnitNameFmtKeyPress
    end
  end
end
