///	<summary>
///	  Сообщения локализации.  
///	</summary>
///	<remarks>
///	  Формат имен констант:
///	  <code>
///	     Сообщение = 's' Модуль '_' Имя_сообщения ['Fmt'] [Постфикс]
///	     Постфикс = 'Error' | 'Warning' | 'Hint' | 'Caption'
///	     'Fmt'      - признак форматированного сообщения.
///	     'Error'    - сообщение об ошибке.
///	     'Warning'  - предупреждение.
///	     'Hint'     - подсказка.
///	     'Caption'  - заголовок.
///
///	  </code>
///	</remarks>
unit SANPkgAsDllMessages;

interface

resourcestring
  // Server
  sServer_NotGetEntryInterfaceFmtError
    = 'Не удалось загрузить пакет "%s", не получен интерфейс инициализации';

  sServer_DuplicatePackageFileNameFmtError
    = 'Не удалось загрузить пакет "%s", т.к. пакет с таким именем файла уже загружен';

  sServer_DuplicatePackageVersionGUIDFmtError
    = 'Не удалось загрузить пакет "%s" (%s) с GUID: %s, т.к. пакет с таким GUID уже загружен';

  sServer_PackageNotFoundByFileNameFmtError
    = 'Пакет с именем файла "%s" не найден';

  sServer_PackageNotFoundByVersionGUIDFmtError
    = 'Пакет с GUID "%s" не найден';

  // CustomDM
  sCustomDM_VersionPartOwerflowError
    = 'Часть версии должна быть в диапазоне 0..255';

  sCustomDM_IncorrectGUIDFmtError
    = 'Строка %s - представляет собой некорректный GUID';

implementation

end.
