///	<summary>
///	  Сообщения локализации.  
///	</summary>
///	<remarks>
///	  Формат имен констант:
///	  <code>
///	     Сообщение = 's' Модуль '_' Имя_сообщения ['Fmt'] [Постфикс]
///	     Постфикс = 'Error' | 'Warning' | 'Hint' | 'Caption'
///	     'Fmt'      - признак форматированного сообщения.
///	     'Error'    - сообщение об ошибке.
///	     'Warning'  - предупреждение.
///	     'Hint'     - подсказка.
///	     'Caption'  - заголовок.
///
///	  </code>
///	</remarks>
unit SANPluginsMessages;

interface

resourcestring
  // Server
  sServer_DuplicatePluginVersionGUIDFmtError
    = 'Не удалось загрузить плагин "%s" с GUID: %s, т.к. пакет с таким GUID ("%s") уже загружен';

  sServer_PluginNotFoundByGUIDFmtError
    = 'Плагин с GUID "%s" не найден';

  // ClientDM
  sClientDM_MultiEntryNotSupported
    = 'Создание нескольких точек входа для плагинов не поддерживается';

implementation

end.
